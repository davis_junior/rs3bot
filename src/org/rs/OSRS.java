package org.rs;

/**
 * This annotation is used for a post processor.
 * Anything annotated with @OSRS will only be included in the OSRS build.
 * The RS3 build will exclude anything annotated with @OSRS.
 * 
 * @author User
 * 
 */
public @interface OSRS {

}
