package org.rs;

/**
 * This annotation is used for a post processor.
 * Anything annotated with @RS3 will only be included in the RS3 build.
 * The OSRS build will exclude anything annotated with @RS3.
 * 
 * @author User
 * 
 */
public @interface RS3 {

}
