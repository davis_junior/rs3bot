package org.rs3.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.rs3.api.Nodes;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.ItemTable;
import org.rs3.api.wrappers.Node;

public class ItemTableCallback {
	
	private static List<ItemTableListener> listeners = new ArrayList<>();
	
	public static void addListener(ItemTableListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}
	
	public static void removeListener(ItemTableListener listener) {
		listeners.remove(listener);
	}
	
	public static void itemTablePut_Before(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
		//System.out.println("itemTablePut_Before[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		
		for (ItemTableListener listener : listeners) {
			listener.put(itemTableId, itemTablePosition, itemId, itemStackSize);
		}
		
		Node tableNode = Nodes.lookup(Client.getItemTables(), itemTableId);
		if (tableNode != null) {
			ItemTable table = tableNode.forceCast(ItemTable.class);
			int originalId = table.getItemIdAt(itemTablePosition);
			int originalStackSize = table.getStackSize(itemTablePosition);
			
			for (ItemTableListener listener : listeners) {
				listener.put(itemTableId, itemTablePosition, itemId, itemStackSize, originalId, originalStackSize);
			}
			
			if (originalId != itemId) {
				if (itemId == -1) {
					for (ItemTableListener listener : listeners) {
						listener.remove(itemTableId, itemTablePosition, itemId, itemStackSize);
					}
				} else if (originalId == -1) {
					for (ItemTableListener listener : listeners) {
						listener.add(itemTableId, itemTablePosition, itemId, itemStackSize);
					}
				} else {
					for (ItemTableListener listener : listeners) {
						listener.replace(itemTableId, itemTablePosition, itemId, itemStackSize, originalId, originalStackSize);
					}
				}
			} else if (originalStackSize != itemStackSize) {
				if (itemStackSize < originalStackSize) {
					for (ItemTableListener listener : listeners) {
						listener.decrease(itemTableId, itemTablePosition, itemId, itemStackSize);
					}
				} else {
					for (ItemTableListener listener : listeners) {
						listener.increase(itemTableId, itemTablePosition, itemId, itemStackSize);
					}
				}
			}
		}
	}
	
	public static void itemTablePut_After(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
		//System.out.println("itemTablePut_After[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		
		for (org.rs3.api.ItemTable table : org.rs3.api.ItemTable.values()) {
			if (table.snapshotsEnabled() && table.getId() == itemTableId) {
				ItemTable wrapperTable = table.getTable();
				if (wrapperTable != null)
					table.getSnapshot().setTable(wrapperTable.getId(), wrapperTable.getItemIds(), wrapperTable.getItemStackSizes());
			}
		}
	}

	public static void itemTablePut_Before(int itemTableId, int itemTablePosition, int itemId, int itemStackSize, boolean orIdWithIntegerMin) {
		long l = itemTableId | (orIdWithIntegerMin ? Integer.MIN_VALUE : 0);
		itemTablePut_Before(l, itemTablePosition, itemId, itemStackSize);
	}
	
	public static void itemTablePut_After(int itemTableId, int itemTablePosition, int itemId, int itemStackSize, boolean orIdWithIntegerMin) {
		long l = itemTableId | (orIdWithIntegerMin ? Integer.MIN_VALUE : 0);
		itemTablePut_After(l, itemTablePosition, itemId, itemStackSize);
	}
}
