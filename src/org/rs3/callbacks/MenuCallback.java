package org.rs3.callbacks;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.accessors.Reflection;
import org.rs3.api.Menu;
import org.rs3.api.wrappers.MenuGroupNode;
import org.rs3.api.wrappers.MenuItemNode;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.NodeSubQueue;

public class MenuCallback {
	
	private static List<MenuListener> listeners = new ArrayList<>();
	
	public static void addListener(MenuListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}
	
	public static void removeListener(MenuListener listener) {
		listeners.remove(listener);
	}
	
	/**
	 * First, second, and cancel menu items are never recreated, only action and option changed or are cached
	 * So this method will not be called for them. 
	 * All is speculation.
	 * 
	 * @param menuItemNode
	 */
	public static void menuItemAppend(Object menuItemNode) {
		//MenuItemNode min = MenuItemNode.create(menuItemNode);
		//menuItemAppend(min);
		
		for (MenuListener listener : listeners) {
			listener.append(menuItemNode);
		}
	}
	
	/*public static void menuItemAppend(MenuItemNode menuItemNode) {
		System.out.println("menuItemAppend(" + menuItemNode.getAction() + " " + menuItemNode.getOption() + ")");
		
		for (MenuListener listener : listeners) {
			listener.append((MenuItemNode)menuItemNode);
		}
	}*/
}
