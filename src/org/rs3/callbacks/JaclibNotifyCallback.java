package org.rs3.callbacks;

import org.rs3.boot.Frame;

public class JaclibNotifyCallback {
	
	public static long lastNotified = 0;
	
	public static void jaclibNotify1(int arg0) {
		//System.out.println("jaclibNotify1(" + arg0 + ")");
	}
	
	public static void jaclibNotify2(int arg0, int arg1, int arg2) {
		/*System.out.println("jaclibNotify2(" + arg0 + ", " + arg1 + ", " + arg2 + ")");
		int x = arg0 >> 16;
		int y = arg0 & 0xFFFF;
		System.out.println(" -- coords: " + x + ", " + y);*/
		
		//System.out.println(System.currentTimeMillis() - lastNotified);
		
		lastNotified = System.currentTimeMillis();
		
		Frame.recorder.jaclib_notify(arg0, arg1, arg2);
	}
}
