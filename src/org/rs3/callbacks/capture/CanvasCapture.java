package org.rs3.callbacks.capture;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.OpenGLRender;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.SDRender;
import org.rs3.boot.BootType;
import org.rs3.boot.Frame;
import org.rs3.boot.OfficialClientInject;
import org.rs3.debug.Paint;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class CanvasCapture {
	public static volatile boolean readPixels = false;
	public static volatile boolean screenshotReadPixels = false;

	public static volatile boolean screenshotReadPixelsSignal = false;

	//static BufferedImage capturedImage = null;
	public static Object capturedLock = new Object();
	public static int[] capturedPixels = null;
	public static int capturedWidth = 0;
	public static int capturedHeight = 0;

	private static Object screenshot_capturedLock = new Object();
	private static int[] screenshot_capturedPixels = null;
	private static int screenshot_capturedWidth = 0;
	private static int screenshot_capturedHeight = 0;

	/**
	 * Callback
	 */
	public static void smSwapBuffers(Image image) {
		//System.out.println("smSwapBuffers(" + image + ")");

		if (BootType.get().equals(BootType.NORMAL) && Frame.window.paint)
			Paint.draw(image.getGraphics());
		else if (BootType.get().equals(BootType.OFFICIAL_CLIENT) && OfficialClientInject.paint)
			Paint.draw(image.getGraphics());
	}

	/**
	 * Callback
	 */
	public static void glSwapBuffers(long pointer) {
		//System.out.println("glSwapBuffers(" + pointer + ")");

		//System.out.println(((OpenGLRender) Client.getRender()).getOpenGL().getClass().getSuperclass().getName());

		glReadPixels();

		if (BootType.get().equals(BootType.NORMAL) && Frame.window.paint) {
			try {
				Paint.setup((GL2) null, Client.getCanvasWidth(), Client.getCanvasHeight());
				Paint.render((GL2) null);
				Paint.reset((GL2) null);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		} else if (BootType.get().equals(BootType.OFFICIAL_CLIENT) && OfficialClientInject.paint) {
			try {
				Paint.setup((GL2) null, Client.getCanvasWidth(), Client.getCanvasHeight());
				Paint.render((GL2) null);
				Paint.reset((GL2) null);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}

		/*OpenGLRender render = (OpenGLRender) Client.getRender();
		if (render != null) {
			Object adq = Reflection.getDeclaredField("dv", render.getObject(), "f");
			if (adq != null) {
				Canvas canvas = (Canvas) Reflection.getDeclaredField(adq, "e");
				if (canvas != null) {
					Graphics g = canvas.getGraphics();
					//g.setColor(Color.red);
					//g.fillRect(0, 0, 300, 300);
					Paint.draw(g);
				}
			}
		}*/
	}

	/**
	 * Callback
	 */
	public static void glSwapBuffersAfter() {

	}


	public static void glReadPixels() {
		//ViewportCallback.tempViewportUpdate();

		if (!readPixels && !screenshotReadPixels)
			return;

		//Object abq = Reflection.getStaticField("client", "cg");
		//Object abf = Reflection.getField(abq, "b");

		//long bufferPointer = (long) Reflection.getField(abf, "i");

		Render render = Client.getRender();

		Object glo = null;

		if (render != null) {
			if (render instanceof OpenGLRender) 
				glo = ((OpenGLRender) render).getOpenGL();
		}

		if (glo != null) {	
			try {
				//java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getIAccessor().getObject();
				int width = Client.getCanvasWidth();
				int height = Client.getCanvasHeight();

				int[] pixels = new int[width*height];
				int[] screenshot_pixels = null;

				//Method getString = CustomClassLoader.get.loadClass("jaggl.OpenGL").getMethod("glGetString", int.class);
				//String string = (String) getString.invoke(glo, GL2.GL_RENDERER);
				//System.out.println(string);

				//Method getError = CustomClassLoader.get.loadClass("jaggl.OpenGL").getMethod("glGetError");

				//Method bindFrameBuffer = CustomClassLoader.get.loadClass("jaggl.OpenGL").getMethod("glBindFramebufferEXT", int.class, int.class);
				//bindFrameBuffer.invoke(glo, GL2.GL_READ_FRAMEBUFFER, buffer);

				Method readBuffer = CustomClassLoader.get.loadClass("jaggl.OpenGL").getMethod("glReadBuffer", int.class);
				readBuffer.invoke(glo, GL.GL_BACK);

				Method pixelStorei = CustomClassLoader.get.loadClass("jaggl.OpenGL").getMethod("glPixelStorei", int.class, int.class);
				pixelStorei.invoke(glo, GL.GL_PACK_ALIGNMENT, 1);
				//pixelStorei.invoke(glo, 3317, 1); // 3317 = GL_UNPACK_ALIGNMENT


				//int type = (int) Reflection.getField(abq, "hc");

				Method readPixels = CustomClassLoader.get.loadClass("jaggl.OpenGL").getMethod("glReadPixelsi", int.class, int.class, int.class, int.class, int.class, int.class, int[].class, int.class);
				//readPixels.invoke(glo, 0, 0, 800, 600, 32993, type, pixels, 800*600); // format = 32993 = GL_BGRA; type = 5121 = unsigned byte
				//readPixels.invoke(glo, 0, 0, 800, 600, GL2.GL_RGB, GL2.GL_UNSIGNED_BYTE, pixels, 800*600);

				/*for (int j = 0; j < 600; j++) {
					readPixels.invoke(glo, 0, 600 - 0 - j - 1, 800, 1, GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, pixels, j*800);
					//readPixels.invoke(glo, 0, 600 - 0 - j - 1, 800, 1, 32993, type, pixels, j*800);
					//readPixels.invoke(glo, 0, height - 0 - j - 1, 800, 1, 32993, type, pixels, j*800);
				}*/
				//readPixels.invoke(glo, 0, 0, 800, 600, GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, pixels, 0);
				//readPixels.invoke(glo, 0, 0, width, height, GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, pixels, 0);

				if (CanvasCapture.readPixels) {
					readPixels.invoke(glo, 0, 0, width, height, (Frame.window.debugGL) ? GL.GL_RGBA : GL.GL_BGRA, GL.GL_UNSIGNED_BYTE, pixels, 0);

					synchronized (capturedLock) {
						capturedPixels = pixels;
						capturedWidth = width;
						capturedHeight = height;
					}
				}

				if (screenshotReadPixels) {
					screenshot_pixels = new int[pixels.length];

					readPixels.invoke(glo, 0, 0, width, height, GL.GL_BGRA, GL.GL_UNSIGNED_BYTE, screenshot_pixels, 0);

					synchronized (screenshot_capturedLock) {
						screenshot_capturedPixels = screenshot_pixels;
						screenshot_capturedWidth = width;
						screenshot_capturedHeight = height;

						screenshotReadPixelsSignal = true;
					}
				}

			} catch (SecurityException | IllegalArgumentException | NoSuchMethodException | ClassNotFoundException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	public static int[] flipPixels(int[] imgPixels, int imgw, int imgh) {
		int[] flippedPixels = null;

		if (imgPixels != null) {
			flippedPixels = new int[imgw * imgh];

			for (int y = 0; y < imgh; y++) {
				for (int x = 0; x < imgw; x++) {
					flippedPixels[ ( (imgh - y - 1) * imgw) + x] = imgPixels[ (y * imgw) + x];
				}
			}
		}
		return flippedPixels;
	}

	public static BufferedImage getGlImage(boolean screenshot) {
		if (!screenshot) {
			synchronized (capturedLock) {
				if (capturedPixels != null) {
					BufferedImage bufferedImage = new BufferedImage(capturedWidth, capturedHeight, BufferedImage.TYPE_INT_ARGB);

					int[] flipped = flipPixels(capturedPixels, capturedWidth, capturedHeight);
					bufferedImage.setRGB(0, 0, capturedWidth, capturedHeight, flipped, 0, capturedWidth);
					//bufferedImage.setRGB(0, 0, capturedWidth, capturedHeight, capturedPixels, 0, capturedWidth);

					return bufferedImage;
				}
			}
		} else {
			synchronized (screenshot_capturedLock) {
				if (screenshot_capturedPixels != null) {
					BufferedImage bufferedImage = new BufferedImage(screenshot_capturedWidth, screenshot_capturedHeight, BufferedImage.TYPE_INT_ARGB);

					int[] flipped = flipPixels(screenshot_capturedPixels, screenshot_capturedWidth, screenshot_capturedHeight);
					bufferedImage.setRGB(0, 0, screenshot_capturedWidth, screenshot_capturedHeight, flipped, 0, screenshot_capturedWidth);
					//bufferedImage.setRGB(0, 0, screenshot_capturedWidth, screenshot_capturedHeight, screenshot_capturedPixels, 0, screenshot_capturedWidth);

					screenshot_capturedPixels = null;

					return bufferedImage;
				}
			}
		}

		return null;
	}


	public static BufferedImage getScreenshot(boolean paint) {
		java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();

		int width = Client.getCanvasWidth();
		int height = Client.getCanvasHeight();

		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // sun's jpg encoder/decoder doesn't support alpha, although using png will work
		Graphics g = bi.getGraphics();

		Render render = Client.getRender();

		if (render != null) {
			if (render instanceof SDRender)
				Paint.blit(canvas.getGraphics(), g);
			else if (render instanceof OpenGLRender) {
				// Wait for gl screen capture...
				screenshotReadPixelsSignal = false;
				screenshotReadPixels = true;

				Timer timer = new Timer(1000);
				while (timer.isRunning()) {
					if (screenshotReadPixelsSignal)
						break;

					Time.sleep(50);
				}

				screenshotReadPixels = false;

				if (!screenshotReadPixelsSignal)
					return null;

				BufferedImage bufferedImage = getGlImage(true);

				if (bufferedImage != null)
					//g.drawImage(bufferedImage, 0, 0, null);
					Paint.blit(bufferedImage.getGraphics(), g);
				else {
					g.setColor(Color.black);
					g.fillRect(0, 0, width, height);
				}
			} else
				Paint.blit(canvas.getGraphics(), g);
		} else {
			g.setColor(Color.black);
			g.fillRect(0, 0, width, height);
		}

		if (paint)
			Paint.draw(g);

		return bi;
	}

	public static boolean saveScreenshot(boolean paint, String name) {
		return saveScreenshot(paint, null, name);
	}

	public static boolean saveScreenshot(boolean paint, File directory, String name) {
		BufferedImage screenshot = getScreenshot(paint);

		if (screenshot == null)
			return false;

		File file;

		if (directory == null)
			file = new File(name);
		else {
			file = new File(directory, name);
		}

		if (file.getParentFile().exists() || file.getParentFile().mkdirs()) {
			boolean fileExists = false;
			if (!file.exists()) {
				try {
					file.createNewFile();
					fileExists = true;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else
				fileExists = true;

			if (fileExists) {
				OutputStream out = null;
				try {
					out = Files.newOutputStream(Paths.get(file.getAbsolutePath()));

					//if (ImageIO.write(screenshot, "jpg", file)) {
						if (ImageIO.write(screenshot, "jpg", out)) {
							out.flush(); // probably unnecessary
							screenshot.flush(); // probably unnecessary
							return true;
						}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (out != null) {
						try {
							out.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		screenshot.flush(); // probably unnecessary

		return false;
	}
}
