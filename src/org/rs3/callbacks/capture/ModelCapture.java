package org.rs3.callbacks.capture;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.ObjectComposite;
import org.rs3.api.wrappers.OpenGLRender;
import org.rs3.util.Timer;

public class ModelCapture {
	public static Set<Npc> npcs = Collections.newSetFromMap(new IdentityHashMap<Npc, Boolean>());
	public static Map<Object, Model> animableObjectModels = new WeakHashMap<>();
	public static Map<Object, Model> boundaryObjectModels = new WeakHashMap<>();
	public static Map<Object, Model> floorObjectModels = new WeakHashMap<>();
	public static Map<Object, Model> wallObjectModels = new WeakHashMap<>();

	public static Timer rotateTimer;

	/**
	 * Removes Npc in set with the specified npcObj.
	 * @param npcObj
	 * @return true if removal was performed, false otherwise
	 */
	public static boolean removeNpc(Object npcObj) {
		Npc toRemove = null;

		synchronized (npcs) {
			Iterator<Npc> itr = npcs.iterator();

			while (itr.hasNext()) {
				Npc npc = itr.next();
				if (npc == null)
					continue;

				Object obj = npc.getObject();

				if (npcObj == obj)
					toRemove = npc;
			}
		}

		if (toRemove != null) {
			return npcs.remove(toRemove);
		}

		return false;
	}

	/**
	 * Scans the Npc set for an Npc with the specified object.
	 * 
	 * @param npcObj
	 * @return Npc with specefied npcObj, null otherwise.
	 */
	public static Npc findNpc(Object npcObj) {
		synchronized (npcs) {
			Iterator<Npc> itr = npcs.iterator();

			while (itr.hasNext()) {
				Npc npc = itr.next();
				if (npc == null)
					continue;

				Object obj = npc.getObject();
				if (npcObj == obj)
					return npc;
			}
		}

		return null;
	}

	public static void updateModel(Object npcObj) {
		synchronized (npcs) {
			Iterator<Npc> itr = npcs.iterator();

			while (itr.hasNext()) {
				Npc npc = itr.next();
				if (npc == null)
					continue;

				Object obj = npc.getObject();
				if (npcObj == obj) {
					Array1D<? extends Model> models = npc.getModels();

					if (models != null && models.getLength() > 0) {
						Model model = models.get(0);

						if (model != null) {
							synchronized (npc) {
								int orientation = npc.getOrientation();
								//int orientation = (int) (rotateTimer.getElapsed() / 5L);

								npc.curModel = Model.create(true, model.getObject(), orientation, 0);
							}

							break;
						}
					}
				}
			}
		}
	}

	public static void updateAnimableObjectModel(Object composite, Object animableObj) {
		if (!(Client.getRender() instanceof OpenGLRender)) {
			animableObjectModels.clear();
			return;
		}
		
		ObjectComposite oc = ObjectComposite.create(composite);
		Model model = null;
		if (oc != null) {
			model = oc.getModel();
		}

		/*if (model == null) {
			System.out.println("update model: null: " + I_ObjectComposite.getModel(composite));
			return;
		}*/

		synchronized (animableObjectModels) {
			if (model == null)
				animableObjectModels.remove(animableObj);
			else
				animableObjectModels.put(animableObj, model);
		}
	}

	public static void updateBoundaryObjectModel(Object composite, Object boundaryObj) {
		if (!(Client.getRender() instanceof OpenGLRender)) {
			boundaryObjectModels.clear();
			return;
		}
		
		ObjectComposite oc = ObjectComposite.create(composite);
		Model model = null;
		if (oc != null) {
			model = oc.getModel();
		}

		/*if (model == null) {
			System.out.println("update model: null: " + I_ObjectComposite.getModel(composite));
			return;
		}*/

		//System.out.println(composite + ": " + model);
		synchronized (boundaryObjectModels) {
			if (model == null)
				boundaryObjectModels.remove(boundaryObj);
			else
				boundaryObjectModels.put(boundaryObj, model);
		}
	}

	public static void updateFloorObjectModel(Object composite, Object floorObj) {
		if (!(Client.getRender() instanceof OpenGLRender)) {
			floorObjectModels.clear();
			return;
		}
		
		ObjectComposite oc = ObjectComposite.create(composite);
		Model model = null;
		if (oc != null) {
			model = oc.getModel();
		}

		/*if (model == null) {
			System.out.println("update model: null: " + I_ObjectComposite.getModel(composite));
			return;
		}*/

		synchronized (floorObjectModels) {
			if (model == null)
				floorObjectModels.remove(floorObj);
			else
				floorObjectModels.put(floorObj, model);
		}
	}

	public static void updateWallObjectModel(Object composite, Object wallObj) {
		if (!(Client.getRender() instanceof OpenGLRender)) {
			wallObjectModels.clear();
			return;
		}
		
		ObjectComposite oc = ObjectComposite.create(composite);
		Model model = null;
		if (oc != null) {
			model = oc.getModel();
		}

		/*if (model == null) {
			System.out.println("update model: null: " + I_ObjectComposite.getModel(composite));
			return;
		}*/

		synchronized (wallObjectModels) {
			if (model == null)
				wallObjectModels.remove(wallObj);
			else
				wallObjectModels.put(wallObj, model);
		}
	}
}
