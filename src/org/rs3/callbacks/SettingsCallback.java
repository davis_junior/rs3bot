package org.rs3.callbacks;

import org.eclipse.swt.widgets.Display;
import org.rs3.tools.SettingsExplorer;

public class SettingsCallback {
	
	public static SettingsExplorer listener = null;
	
	public static void changeSetting(Object arg0, int arg1) {
		//System.out.println("changeSetting(" + arg0 + ", " + arg1 + ")");
	}
	
	public static void changeSetting(final int index, final int value) {
		//System.out.println("SETTINGS[" + index + "] = " + value);
		
		if (listener != null) {
			synchronized (SettingsExplorer.lock) {
				Display.getDefault().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	listener.changeSetting(index, value);
				    }
				});
			}
		}
	}
}
