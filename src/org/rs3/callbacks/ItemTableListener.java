package org.rs3.callbacks;

public interface ItemTableListener {
	
	public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize);
	public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize, int originalItemId, int originalItemStackSize);
	
	// these are based on item id (-1 if removed)
	public void add(long itemTableId, int itemTablePosition, int itemId, int itemStackSize);
	public void remove(long itemTableId, int itemTablePosition, int itemId, int itemStackSize);
	public void replace(long itemTableId, int itemTablePosition, int itemId, int itemStackSize, int originalItemId, int originalItemStackSize);
	
	// these are based on stack size
	public void increase(long itemTableId, int itemTablePosition, int itemId, int itemStackSize);
	public void decrease(long itemTableId, int itemTablePosition, int itemId, int itemStackSize);
	
}
