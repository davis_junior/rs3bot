package org.rs3.callbacks;

import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.Viewport;

public class ViewportCallback {
	
	public static float[] floats = null;
	
	public static float[] debugFloats1 = null;
	public static float[] debugFloats2 = null;
	
	public static void smViewportUpdate(int arg0, int arg1, int arg2, int arg3, int arg4, int arg5) {
		System.out.println("smViewportUpdate(" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + ", " + arg4 + ", " + arg5 + ")");
	}
	
	public static void glViewportUpdate(int arg0, int arg1, int arg2, int arg3, int arg4, int arg5) {
		//System.out.println("glViewportUpdate(" + arg0 + ", " + arg1 + ", " + arg2 + ", " + arg3 + ", " + arg4 + ", " + arg5 + ")");
	}
	
	public static void smViewportUpdate(float[] floats) {
		ViewportCallback.floats = floats.clone();
	}
	
	public static void glViewportUpdate(float[] floats) {
		ViewportCallback.floats = floats.clone();
	}
	
	// for opengl until the correct callback is obtained
	/*public static void tempViewportUpdate() {
		Render render = Client.getRender();
		Viewport viewport = null;
		
		if (render != null)
			viewport = render.getClientViewport();
		
		if (viewport != null)
			floats = viewport.getFloats().clone();
	}*/
	
}
