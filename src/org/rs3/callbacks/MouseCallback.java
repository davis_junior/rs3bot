package org.rs3.callbacks;

import org.rs3.api.Menu;
import org.rs3.api.wrappers.MenuItemNode;
import org.rs3.util.TextUtils;

public class MouseCallback {
	
	public static MenuClickFilter menuClickFilter = null;
	
	public static MenuItemNode lastActionClickMin;
	public static int lastActionClickX;
	public static int lastActionClickY;
	public static boolean lastActionClickUnknownBoolean;
	
	static {
		resetLastActionClickValues();
	}
	
	public static void resetLastActionClickValues() {
		lastActionClickMin = null;
		lastActionClickX = -1;
		lastActionClickY = -1;
		lastActionClickUnknownBoolean = false;
	}
	
	/**
	 * the object returned will replace the actionClicked menu item node
	 * 
	 * @param obj_origMenuItemNode
	 * @return
	 */
	public static Object forceClickedMenuItem(Object obj_origMenuItemNode) {
		if (menuClickFilter != null)
			return menuClickFilter.setMenuItem(obj_origMenuItemNode);
		
		return obj_origMenuItemNode;
	}
	
	/**
	 * Occurs when the mouse is clicked with a selected menu action other than "Cancel" and null.
	 * 
	 * @param obj_MenuItemNode MenuItemNode unwrapped object
	 * @param x mouse x
	 * @param y mouse y
	 * @param unknownBoolean
	 */
	public static void actionClicked(Object obj_MenuItemNode, int x, int y, boolean unknownBoolean) {
		
		MenuItemNode min = MenuItemNode.create(obj_MenuItemNode);
		
		lastActionClickMin = min;
		lastActionClickX = x;
		lastActionClickY = y;
		lastActionClickUnknownBoolean = unknownBoolean;
		
		if (min != null) {
			String action = min.getAction();
			String option = min.getOption();
			
			action = TextUtils.removeHtml(action).trim();
			option = TextUtils.removeHtml(option).trim();
			
			String menuString = (option.isEmpty()) ? action : action + ", " + option;
			
			System.out.println("actionClicked([" + menuString + "], " + x + ", " + y + ", " + unknownBoolean + ")");
		} else {
			System.out.println("actionClicked(" + obj_MenuItemNode + ", " + x + ", " + y + ", " + unknownBoolean + ")");
		}
	}
	
}
