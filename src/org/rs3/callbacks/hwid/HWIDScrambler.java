package org.rs3.callbacks.hwid;

import org.rs3.util.Random;

public class HWIDScrambler {

	private static int[] cpuInfo = null;
	private static int[] rawCpuInfo = null;
	private static long totalMemory = -1;
	private static long maxMemory = -1;
	private static long freeMemory = -1;
	private static int availableProcessors = -1;
	
	private static void setup() {
		Random.randomOrgReseed();
		
		// getCPUInfo
		// 0 = threads (logical cores) - 8 on this system
		// 1 = processor base freq in mhz - 3400 on this sytem
		// 2 = total physical memory as seen by the OS in task manager in MB - 32637 on this system
		
		cpuInfo = new int[3];
		switch (Random.nextInt(0, 4)) {
		case 0:
			cpuInfo[0] = 1; // threads (logical cores)
			break;
		case 1:
			cpuInfo[0] = 2; // threads (logical cores)
			break;
		case 2:
			cpuInfo[0] = 4; // threads (logical cores)
			break;
		case 3:
			cpuInfo[0] = 8; // threads (logical cores)
			break;
		}
		
		cpuInfo[1] = 1800 + Random.nextInt(0, 20)*100; // base clock speed
		cpuInfo[2] = 512 + Random.nextInt(0, 8)*512; // total physical memory as seen by OS
		
		//rawCpuInfo = new int[115];
		
		// these 3 measured in bytes
		//totalMemory = -1;
		maxMemory = 266634176; // 256m
		//freeMemory = -1;
		
		switch (cpuInfo[0]) {
		case 0:
			availableProcessors = 1;
			break;
		case 1:
			if (Random.nextBoolean())
				availableProcessors = 1;
			else
				availableProcessors = 2;
			break;
		case 2:
			switch (Random.nextInt(0, 3)) {
			case 0:
				availableProcessors = 1;
				break;
			case 1:
				availableProcessors = 2;
				break;
			case 2:
				availableProcessors = 4;
				break;
			}
			break;
		case 3:
			switch (Random.nextInt(0, 4)) {
			case 0:
				availableProcessors = 1;
				break;
			case 1:
				availableProcessors = 2;
				break;
			case 2:
				availableProcessors = 4;
				break;
			case 3:
				availableProcessors = 8;
				break;
			}
			break;
		}
		
		rawCpuInfo = new int[115];
		
		rawCpuInfo[0] = 0;
		rawCpuInfo[1] = 13;
		rawCpuInfo[2] = 1970169159;
		rawCpuInfo[3] = 1818588270;
		rawCpuInfo[4] = 1231384169;
		
		rawCpuInfo[5] = 1;
		rawCpuInfo[6] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[7] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[8] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[8] = rawCpuInfo[8] & 0x7FFFFFFE; // clear hypervisor present bit (31, not sure if first or last bit)
		rawCpuInfo[9] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[10] = 2;
		rawCpuInfo[11] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[12] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[13] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[14] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[15] = 3;
		rawCpuInfo[16] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[17] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[18] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[19] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[20] = 4;
		rawCpuInfo[21] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[22] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[23] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[24] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[25] = 5;
		rawCpuInfo[26] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[27] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[28] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[29] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[30] = 6;
		rawCpuInfo[31] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[32] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[33] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[34] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[35] = 7;
		rawCpuInfo[36] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[37] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[38] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[39] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[40] = 8;
		rawCpuInfo[41] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[42] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[43] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[44] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[45] = 9;
		rawCpuInfo[46] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[47] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[48] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[49] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[50] = 10;
		rawCpuInfo[51] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[52] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[53] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[54] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[55] = 11;
		rawCpuInfo[56] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[57] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[58] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[59] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[60] = 12;
		rawCpuInfo[61] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[62] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[63] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[64] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[65] = 13;
		rawCpuInfo[66] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[67] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[68] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[69] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[70] = 0x80000000;
		rawCpuInfo[71] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[72] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[73] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[74] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[75] = 0x80000001;
		rawCpuInfo[76] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[77] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[78] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[79] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[80] = 0x80000002;
		rawCpuInfo[81] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[82] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[83] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[84] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[85] = 0x80000003;
		rawCpuInfo[86] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[87] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[88] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[89] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[90] = 0x80000004;
		rawCpuInfo[91] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[92] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[93] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[94] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[95] = 0x80000005;
		rawCpuInfo[96] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[97] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[98] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[99] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[100] = 0x80000006;
		rawCpuInfo[101] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[102] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[103] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[104] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[105] = 0x80000007;
		rawCpuInfo[106] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[107] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[108] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[109] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		
		rawCpuInfo[110] = 0x80000008;
		rawCpuInfo[111] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[112] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[113] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
		rawCpuInfo[114] = (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2)) + (Random.nextInt(0, Integer.MAX_VALUE) + Random.nextInt(0, 2));
	}
	
	public static int[] getCPUInfo(int[] orig) {
		System.out.println("getCPUInfo(" + orig + ")");
		
		if (cpuInfo == null)
			setup();
		
		return cpuInfo;
	}
	
	public static int[] getRawCPUInfo(int[] orig) {
		System.out.println("getRawCPUInfo(" + orig + ")");
		
		if (rawCpuInfo == null)
			setup();
		
		return rawCpuInfo;
	}
	
	public static long totalMemory(long orig) {
		System.out.println("totalMemory(" + orig + ")");
		/*if (totalMemory == -1)
			setup();
		
		return totalMemory;*/
		return orig;
	}
	
	public static long maxMemory(long orig) {
		System.out.println("maxMemory(" + orig + ")");
		if (maxMemory == -1)
			setup();
		
		return maxMemory;
		//return orig;
	}
	
	public static long freeMemory(long orig) {
		System.out.println("freeMemory(" + orig + ")");
		/*if (freeMemory == -1)
			setup();
		
		return freeMemory;*/
		return orig;
	}
	
	public static int availableProcessors(int orig) {
		System.out.println("availableProcessors(" + orig + ")");
		if (availableProcessors == -1)
			setup();
		
		return availableProcessors;
	}
	
	public static String getHostName(String orig) {
		System.out.println("getHostName(" + orig + ")");
		return orig;
	}
}
