package org.rs3.callbacks.proxy;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

// mainly used for runescape analytics, not sure how to proxify it in client: socket://api.segment.io:443
public class CustomProxySelector extends ProxySelector {

    private final ProxySelector def;

    CustomProxySelector(ProxySelector def) {
        this.def = def;
    }
    
    @Override
    public List<Proxy> select(URI uri) {
		//System.out.println("select for URL : " + uri);
		//System.out.println("select for URL host : " + uri.getHost());
		
		if (uri.getHost().equals("localhost") || uri.getHost().equals("127.0.0.1") || uri.getHost().equals("192.168.5.11") /*|| uri.getHost().startsWith("192.168.")*/) {
			//System.out.println("no proxy");
			List<Proxy> proxyList = new ArrayList<Proxy>();
			proxyList.add(Proxy.NO_PROXY);
			return proxyList;
		}

		//System.out.println("using proxy");
		List<Proxy> proxyList = new ArrayList<Proxy>();
		proxyList.add(ProxySetup.getProxy());
		return proxyList;
    }

    @Override
    public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
