package org.rs3.callbacks.proxy;

import java.applet.AppletContext;
import java.io.IOException;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.net.Proxy.Type;
import java.net.Socket;
import java.net.URL;

import org.rs3.database.ServerInfo;
import org.rs3.database.ServerRow;

import proxy.ProxyAuthenticator;

public class ProxySetup {

	private static Proxy proxy = null;
	private static ProxySelector proxySelector = null;
	
	public static Proxy getProxy() {
		if (proxy == null) {
			/*proxy = new Proxy(Type.SOCKS, new InetSocketAddress("104.148.11.196", 61336));
			Authenticator.setDefault(new ProxyAuthenticator("davisjunior11", "E25lEePY"));
			if (true) return proxy;*/
			
			if (ServerInfo.cur == null)
				ServerInfo.loadServerInfo();
			
			if (ServerInfo.cur != null) {
				ServerRow serverRow = ServerInfo.cur.getRow();
				if (serverRow != null) {
					proxy = new Proxy(Type.SOCKS, new InetSocketAddress(serverRow.getProxyHost(), serverRow.getProxyPort()));
					Authenticator.setDefault(new ProxyAuthenticator(serverRow.getProxyUsername(), serverRow.getProxyPassword()));
				}
			}
		}
		
		return proxy;
	}
	
	public static ProxySelector getProxySelector() {
		if (proxySelector == null) {
			proxySelector = new CustomProxySelector(ProxySelector.getDefault());
		}
		
		return proxySelector;
	}
	
	public static Socket getSocket(InetSocketAddress socketAddress) throws IOException {
		System.out.println("getSocket(" + socketAddress + ")");
		
		Socket socket = new Socket(getProxy());
		socket.connect(socketAddress, 2000);
		return socket;
	}
	
	// TODO unused so far so not necessary to implement proxy yet
	public static void showDocument(AppletContext appContext, URL url, String target) {
		System.out.println("showDocument(" + appContext + ", " + url + ", " + target + ")");
		appContext.showDocument(url, target);
	}
}
