package org.rs3.eventqueue;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;

import org.rs3.accessors.CustomClassLoader;

/**
 * Smart blocks events to the client by overriding the AWTEventQueue. This 
 * queue is responsible for sending events to anything and everything in java.
 * Therefore, by checking the "source" of the event, we can determin whether or 
 * not to block it. Also, we can inject events and distribute them exactly as 
 * AWT would, so it is no different to the client. 
 *
 * This class was written for another project, and is easily portable through 
 * the provided methods. Feel free to use this class with credit.
 *
 * @author Benjamin J. Land
 */
public class BlockingEventQueue extends EventQueue {
    
    /**
     * This one single instance is all that will ever be used in this instance
     * of Java. Makes sure we don't mess ourselves up.
     */
    private static final BlockingEventQueue instance = new BlockingEventQueue();
    /**
     * Maintains a list of all components added to the queue's blocking list
     */
    private static final LinkedList<Component> blockedComponents = new LinkedList<Component>();
    /**
     * Keeps track of whether or not the component is being blocked at the 
     * moment.
     */
    private static final HashMap<Component,Boolean> blocking = new HashMap<Component,Boolean>();
    
    private static final HashMap<Component,EventRedirect> redirects = new HashMap<Component,EventRedirect>();
    
    // not used
    /*public static void addMouseListener(Component comp, MouseListener listener) {
    	if (comp != null) {
    		comp.addMouseListener(listener);
    	}
    }
    
    public static void addKeyListener(Component comp, KeyListener listener) {
    	if (comp != null) {
    		comp.addKeyListener(listener);
    	}
    }*/
    
    /**
     * Adds a component to the queue and set it to start blocking
     * @param comp The component to be blocked
     */
    public static void addComponent(Component comp, EventRedirect redirect) {
        if (!blockedComponents.contains(comp)) {
            blockedComponents.add(comp);
            redirects.put(comp,redirect);
            setBlocking(comp, true);
        }
    }
    
    /**
     * Removes a component to the queue and set it to stop blocking
     * @param comp The component to be removed
     */
    public static void removeComponent(Component comp) {
        if (blockedComponents.contains(comp)) {
            setBlocking(comp, false);
            blockedComponents.remove(comp);
            blocking.remove(comp);
        }
    }
    
    public static void removeAllComponents() {
    	if (blockedComponents != null) {
    		for (java.awt.Component comp : blockedComponents) {
                setBlocking(comp, false);
                blockedComponents.remove(comp);
                blocking.remove(comp);
    		}
    	}
    }
    
    /**
     * Changes the blocking status of a component. Creates an event nazi if 
     * nessessary.
     * @param comp The component to be changed
     * @param isBlocking Whether or not the component should be blocking
     */
    public static void setBlocking(Component comp, boolean isBlocking) {
        if (!blockedComponents.contains(comp))
            throw new RuntimeException("Component not found: " + comp.getName());
        if (isBlocking) {
            blocking.put(comp, isBlocking);
            if (EventNazi.getNazi(comp) == null) 
                EventNazi.createNazi(comp);
        } else {
            if (EventNazi.getNazi(comp) != null) EventNazi.getNazi(comp).destroy();
            blocking.put(comp, isBlocking);
        }
        ensureBlocking();
    }
    
    /**
     * Checks to see if a component is blocking
     * @param comp The component to be checked
     * @result True if blocking
     */
    public static boolean isBlocking(Component comp) {
        if (!blockedComponents.contains(comp))
            return false;
        return blocking.get(comp);
    }
    
    // added
    private static void setQueue() {
        EventQueue eventQueue = Toolkit.getDefaultToolkit().getSystemEventQueue();
        
        try {
        	Class<?> clazz = CustomClassLoader.get.loadClass(data.Client.get.eventQueue.getInternalClassName());
        	Field field = clazz.getDeclaredField(data.Client.get.eventQueue.getInternalName());
        	field.setAccessible(true);
        	field.set(null, BlockingEventQueue.instance);
        	
        	eventQueue.push(BlockingEventQueue.instance);
		} catch (SecurityException | ClassNotFoundException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
    }

    // added
    private static boolean isJava7Like() {
        return Float.parseFloat(System.getProperty("java.specification.version")) > 1.6;
    }

    
    /**
     * Call this method whenever you want to make sure something is being blocked.
     * Needs to be called at least once per program...
     */
    public static void ensureBlocking() {
        if (!(Toolkit.getDefaultToolkit().getSystemEventQueue() instanceof BlockingEventQueue)) {
        	//Toolkit.getDefaultToolkit().getSystemEventQueue().push(instance);
        	
        	if (!isJava7Like()) setQueue();
        	
            try {
    			EventQueue.invokeAndWait(new Runnable() {
    			    @Override
					public void run() {
    	                if (isJava7Like()) setQueue();
    			    }
    			});
    		} catch (InvocationTargetException | InterruptedException e1) {
    			e1.printStackTrace();
    		}
        }
    }
    
    /**
     * Send an event skiping blocking. Just wraps the event and unwraps it in
     * the queue.
     * @param e The event to send unblocked
     */
    public static void sendUnblocked(AWTEvent e) {
        instance.postEvent(new UnblockedEvent(e));
    }
    
    /**
     * If it gets called more than once, you've pretty much screwed yourself.
     */
    private BlockingEventQueue() {
    	
    }
    
    /**
     * The backbone to event blocking. Simple concept really.
     * @param event The next event in the queue
     */
    @Override
	protected void dispatchEvent(AWTEvent event) {
    	//System.out.println("dsfsfsfs");
    	
        if (event instanceof UnblockedEvent) {
            AWTEvent e = ((UnblockedEvent)event).getEvent();
            ((Component)e.getSource()).dispatchEvent(e);
        } else if (blockedComponents.contains(event.getSource()) && blocking.get(event.getSource())) {
            if (event instanceof MouseEvent || event instanceof KeyEvent || event instanceof WindowEvent || event instanceof FocusEvent) {
                redirects.get(event.getSource()).dispatched(event);
                redirects.get(event.getSource()).dispatchToListener(event);
                return;
            }
            super.dispatchEvent(event);
        } else {
            super.dispatchEvent(event);
        }
    }
    
}
