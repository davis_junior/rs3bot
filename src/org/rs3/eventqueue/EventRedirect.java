package org.rs3.eventqueue;
import java.awt.AWTEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.EventListener;

public abstract class EventRedirect {
    
	MouseListener mouseListener = null;
	MouseMotionListener mouseMotionListener = null;
	KeyListener keyListener = null;
	
    public abstract void dispatched(AWTEvent e);
    
    public MouseListener getMouseListener() {
    	return this.mouseListener;
    }
    
    public MouseMotionListener getMouseMotionListener() {
    	return this.mouseMotionListener;
    }
    
    public KeyListener getKeyListener() {
    	return this.keyListener;
    }
    
    public void setMouseListener(MouseListener listener) {
    	this.mouseListener = listener;
    }
    
    public void setMouseMotionListener(MouseMotionListener listener) {
    	this.mouseMotionListener = listener;
    }
    
    public void setKeyListener(KeyListener listener) {
    	this.keyListener = listener;
    }
    
    public void setupListeners(MouseListener mouseListener, MouseMotionListener mouseMotionListener, KeyListener keyListener) {
        this.mouseListener = mouseListener;
        this.mouseMotionListener = mouseMotionListener;
        this.keyListener = keyListener;
    }
    
	public void setupListeners(EventListener listener) {
		if (listener instanceof MouseListener)
			this.mouseListener = (MouseListener) listener;
		else
			this.mouseListener = null;
		
		if (listener instanceof MouseMotionListener)
			this.mouseMotionListener = (MouseMotionListener) listener;
		else
			this.mouseMotionListener = null;
		
		if (listener instanceof KeyListener)
			this.keyListener = (KeyListener) listener;
		else
			this.keyListener = null;
	}
	
    public void resetListeners() {
        this.mouseListener = null;
        this.mouseMotionListener = null;
        this.keyListener = null;
    }
    
    public void dispatchToListener(AWTEvent e) {
    	if (mouseMotionListener != null && e instanceof MouseEvent) {
    		MouseEvent me = (MouseEvent) e;
    		
    		switch (me.getID()) {
    		case MouseEvent.MOUSE_MOVED:
    			mouseMotionListener.mouseMoved(me);
    			return;
    		case MouseEvent.MOUSE_DRAGGED:
    			mouseMotionListener.mouseDragged(me);
    			return;
    		}
    	}
    	
    	if (mouseListener != null && e instanceof MouseEvent) {
    		MouseEvent me = (MouseEvent) e;
    		
    		switch (me.getID()) {
    		case MouseEvent.MOUSE_PRESSED:
    			mouseListener.mousePressed(me);
    			return;
    		case MouseEvent.MOUSE_RELEASED:
    			mouseListener.mouseReleased(me);
    			return;
    		case MouseEvent.MOUSE_CLICKED:
    			mouseListener.mouseClicked(me);
    			return;
    		case MouseEvent.MOUSE_ENTERED:
    			mouseListener.mouseEntered(me);
    			return;
    		case MouseEvent.MOUSE_EXITED:
    			mouseListener.mouseExited(me);
    			return;
    		}
    	}
    	
    	if (keyListener != null && e instanceof KeyEvent) {
    		KeyEvent ke = (KeyEvent) e;
    		
    		switch (ke.getID()) {
    		case KeyEvent.KEY_PRESSED:
    			keyListener.keyPressed(ke);
    			return;
    		case KeyEvent.KEY_RELEASED:
    			keyListener.keyReleased(ke);
    			return;
    		case KeyEvent.KEY_TYPED:
    			keyListener.keyTyped(ke);
    			return;
    		}
    	}
    }
}