package org.rs3.eventqueue;

import java.awt.AWTEvent;

/**
* Lets me pass events through the blockade in a Java-like manner.
* @author Benjamin J. Land
*/
public class UnblockedEvent extends AWTEvent {
    
    private AWTEvent e;
    
    public UnblockedEvent(AWTEvent e) {
        super(e.getSource(), e.getID());
        this.e = e;
    }
    
    public AWTEvent getEvent() {
        return e;
    }
    
}