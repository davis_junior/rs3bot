package org.rs3.api;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Constants {

	public enum Animation {
		MINING("Mining", 624, 625, 626, 627, 628, 629), // TODO: add dragon pick animation, bronze = 625, iron = 626, steel = 627, mithril = 629, adamant = 628, rune = 624
		WOODCUTTING("Wood Cutting", 21177, 21191),
		LODESTONE_TELEPORT_START("Lodestone Teleport Start", 16385),
		LODESTONE_TELEPORT_END("Lodestone Teleport End", 16386),
		LODESTONE_TELEPORT_END_WALK("Lodestone Teleport End Walk", 16393),
		RING_OF_KINSHIP_TELEPORT_START("Ring of Kinship Teleport Start", 13652),
		RING_OF_KINSHIP_TELEPORT_END("Ring of Kinship Teleport End", 13654),
		RUNESPAN_SIPHONING("Runespan Siphoning", 16596),
		RUNESPAN_SIPHON_END("Runespan Siphon End", 16599),
		;
		
		private String name;
		private Set<Integer> ids;
		
		Animation(String name, Integer... ids) {
			this.name = name;
			this.ids = new HashSet<>(Arrays.asList(ids));
		}
		
		public String getName() {
			return name;
		}
		
		public Set<Integer> getIds() {
			return ids;
		}
		
	}
	
}
