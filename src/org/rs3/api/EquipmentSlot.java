package org.rs3.api;

import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.WornEquipmentWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Time;

public enum EquipmentSlot {

	HEAD(0, 0),
	CAPE(1, 1),
	NECK(2, 2),
	AMMUNITION(13, 13),
	MAINHAND(3, 3),
	TORSO(4, 4),
	OFFHAND(5, 5),
	LEGS(7, 7),
	HANDS(9, 9),
	FEET(10, 10),
	RING(12, 12),
	AURA(14, 14), // TODO: check itemTableIndex
	POCKET(15, 15), // TODO: check itemTableIndex
	;
	
	int tabIndex; // component index
	int itemTableIndex;
	
	private EquipmentSlot(int tabIndex, int itemTableIndex) {
		this.tabIndex = tabIndex;
		this.itemTableIndex = itemTableIndex;
	}
	
	public int getItemId() {
		return ItemTable.EQUIPMENT.getItemIdAt(this.itemTableIndex);
	}
	
	public Component getSlot() {
		Component equipment = WornEquipmentWidget.equipmentData.getComponent();
		if (equipment != null) {
			Array1D<Component> slots = equipment.getComponents();
			if (slots != null && this.tabIndex < slots.getLength())
				return slots.get(this.tabIndex);
		}
		
		return null;
	}
	
	public boolean interact(String action) {
		return interact(action, null);
	}
	
	public boolean interact(String action, String option) {
		if (Ribbon.Tab.WORN_EQUIPMENT.open(true)) {
			Time.sleepQuick();
			
			Component slot = getSlot();
			if (slot != null && slot.isVisible())
				return slot.interact(action, option);
		}
		
		return false;
	}
	
	public boolean remove() {
		return interact("Remove");
	}
}
