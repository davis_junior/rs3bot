package org.rs3.api;

import org.rs3.api.wrappers.ItemTable;

/**
 * Used for caching ItemTables. E.g., the bank item table is cleared when the bank is closed. This should be used so its cache is always accessible.
 * 
 * Should only be used in the ItemTable enum, where callbacks set the data.
 * 
 * @author User
 *
 */
public class ItemTableSnapShot extends ItemTable {
	
	private long id = -1;
	private int[] itemIds = null;
	private int[] itemStackSizes = null;

	protected ItemTableSnapShot(long id) {
		super(null);
		this.id = id;
	}
	
	@Override
	public long getId() {
		return this.id;
	}
	
	@Override
	public int[] getItemIds() {
		return this.itemIds;
	}
	
	@Override
	public int[] getItemStackSizes() {
		return this.itemStackSizes;
	}
	
	public void setTable(long id, int[] itemIds, int[] itemStackSizes) {
		this.id = id;
		this.itemIds = itemIds;
		this.itemStackSizes = itemStackSizes;
	}
}
