package org.rs3.api.input.realmouse;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;
import org.jnativehook.mouse.NativeMouseMotionListener;
import org.rs3.api.input.HardwareInput;
import org.rs3.boot.Frame;
import org.rs3.callbacks.JaclibNotifyCallback;
import org.rs3.debug.PaintListenter;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

public class MouseRecorder implements PaintListenter, MouseMotionListener, MouseListener, NativeMouseMotionListener, NativeMouseListener {
	
	//boolean printed = false;
	
	Block srcBlock = Block.BLOCK_261;
	Block dstBlock = Block.BLOCK_176;
	
	Block prevSrcBlock = srcBlock;
	Block prevDstBlock = dstBlock;
	
	public boolean recording = false;
	boolean discarded = false;
	
	Object lock = new Object();
	Object fileLock = new Object();
	List<MouseData> curMouseData = new ArrayList<>();
	Block recordedSrcBlock = srcBlock;
	Block recordedDstBlock = dstBlock;
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 1920, 1080);
		
		/*int i = 0;
		for (int x = 0; x < 1100; x += 50) {
			for (int y = 0; y < 800; y += 50) {
				if (!printed)
					System.out.println("BLOCK_" + i + "(" + i + ", " + x + ", " + y + "),");
				
				g.setColor(Color.RED);
				g.drawRect(x, y, 50, 50);
				
				g.setColor(Color.BLUE);
				g.drawString(Integer.toString(i), x + 25, y + 25);
				
				i++;
			}
		}
		
		printed = true;*/
		
		Rectangle srcBounds = srcBlock.getBounds();
		Rectangle dstBounds = dstBlock.getBounds();
		
		g.setColor(Color.GREEN);
		g.drawString(Integer.toString(srcBlock.getId()), srcBlock.getX() + srcBounds.width / 2, srcBlock.getY() + srcBounds.height / 2);
		
		g.setColor(Color.RED);
		g.drawString(Integer.toString(dstBlock.getId()), dstBlock.getX() + dstBounds.width / 2, dstBlock.getY() + dstBounds.height / 2);
		
		g.setColor(Color.WHITE);
		g.drawRect(srcBlock.getX(), srcBlock.getY(), srcBounds.width, srcBounds.height);
		g.drawRect(dstBlock.getX(), dstBlock.getY(), dstBounds.width, dstBounds.height);
		
		g.setColor(Color.BLUE);
		g.setFont(g.getFont().deriveFont(20f));
		if (discarded)
			g.drawString("Discarded", 200, 50);
		else
			g.drawString(recording ? "Recording" : "Stopped", 200, 50);
		
		if (!recording) {
			synchronized (lock) {
				for (int i = 0; i < curMouseData.size() - 1; i++) {
					g.setColor(Color.RED);
					g.fillOval(curMouseData.get(i).getX() - 3, curMouseData.get(i).getY() - 3, 6, 6);
					
					g.setColor(Color.GREEN);
					g.drawLine(curMouseData.get(i).getX(), curMouseData.get(i).getY(), curMouseData.get(i + 1).getX(), curMouseData.get(i + 1).getY());
				}
			}
		}
		
		/*for (Block block : Block.values()) {
			Rectangle bounds = block.getBounds();
			
			g.setColor(Color.RED);
			g.drawRect(block.getX(), block.getY(), bounds.width, bounds.height);
			
			g.setColor(Color.BLUE);
			g.drawString(Integer.toString(block.getId()), block.getX() + bounds.width / 2, block.getY() + bounds.height / 2);
		}*/
	}

	public void _mousePressed(int button, int x, int y) {
		if (button == MouseEvent.BUTTON1) {
			discarded = false;
			
			if (!recording) { // going to start recording
				recording = true;
				
				rebaseMouseDataTime();
				synchronized (fileLock) {
					flushToFile();
				}
				
				synchronized (lock) {
					curMouseData.clear();
				}
				recordedSrcBlock = srcBlock;
				recordedDstBlock = dstBlock;
			} else if (recording) { // going to stop recording
				recording = false;
				
				prevSrcBlock = srcBlock;
				prevDstBlock = dstBlock;
				
				dstBlock = dstBlock.getNext();
				
				if (dstBlock == null) {
					srcBlock = srcBlock.getNext();
					dstBlock = Block.BLOCK_0;
				}
				
				if (srcBlock == null) {
					srcBlock = Block.BLOCK_0;
					dstBlock = Block.BLOCK_1;
				}
				
				
				if (dstBlock.equals(srcBlock)) {
					dstBlock = dstBlock.getNext();
				}
				
				// check again
				if (dstBlock == null) {
					srcBlock = srcBlock.getNext();
					dstBlock = Block.BLOCK_0;
				}
				
				// check again
				if (srcBlock == null) {
					srcBlock = Block.BLOCK_0;
					dstBlock = Block.BLOCK_1;
				}
				
				/*synchronized (lock) {
					for (int i = 0; i < curMouseData.size() - 1; i++) {
						System.out.println(curMouseData.get(i + 1).getTime() - curMouseData.get(i).getTime());
					}
				}*/
			}
			
			//recording = !recording;
			//discarded = false;
		} else if (button == MouseEvent.BUTTON3) {
			discarded = true;
			recording = false;
			
			synchronized (lock) {
				curMouseData.clear();
			}
			
			if (!recording) {
				srcBlock = prevSrcBlock;
				dstBlock = prevDstBlock;
			}
		}
	}
	
	public void _mouseMoved(int x, int y) {
		if (recording) {
			synchronized (lock) {
				curMouseData.add(new MouseData(x, y, System.currentTimeMillis()));
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		_mousePressed(e.getButton(), e.getX(), e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		_mouseMoved(e.getX(), e.getY());
	}
	
	public void jaclib_notify(int arg0, int arg1, int arg2) {
		//System.out.println("jaclib_notify(" + arg0 + ", " + arg1 + ", " + arg2 + ")");
		int x = arg0 >> 16;
		int y = arg0 & 0xFFFF;
		//System.out.println(" -- coords: " + x + ", " + y);
		
		if (arg1 == 512) { // move mouse
			//curMouseData.add(new MouseData(x, y, System.currentTimeMillis()));
			//System.out.println("curMouseData.add(new MouseData(" + x + ", " + y + ", " + System.currentTimeMillis() + "L));");
		}
	}
	
	public void rebaseMouseDataTime() {
		synchronized (lock) {
			if (curMouseData.size() > 0) {
				long initialTime = curMouseData.get(0).getTime();
				if (initialTime == 0)
					return;
				
				List<MouseData> newMouseData = new ArrayList<>();
				
				newMouseData.add(new MouseData(curMouseData.get(0).getX(), curMouseData.get(0).getY(), 0));
				
				for (int i = 1; i < curMouseData.size(); i++) {
					MouseData prev = curMouseData.get(i - 1);
					MouseData cur = curMouseData.get(i);
					newMouseData.add(new MouseData(cur.getX(), cur.getY(), cur.getTime() - prev.getTime()));
				}
				
				curMouseData = newMouseData;
			}
		}
	}
	
	public void flushToFile() {
		if (curMouseData.size() == 0)
			return;
		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter("mouse.dat", true));
			bw.write(recordedSrcBlock.getId() + "->" + recordedDstBlock.getId() + "=");
			
			synchronized (lock) {
				for (MouseData mouseData : curMouseData) {
					bw.write(mouseData.getX() + "," + mouseData.getY() + "," + mouseData.getTime() + ";");
				}
			}
			
			bw.newLine();
			bw.flush();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException ioe2) {
					ioe2.printStackTrace();
				}
			}
		}
	}
	   
	public void loadMouseData() {
		curMouseData.clear();

curMouseData.add(new MouseData(38, 94, 1455692190880L));
curMouseData.add(new MouseData(56, 94, 1455692190886L));
curMouseData.add(new MouseData(68, 95, 1455692190894L));
curMouseData.add(new MouseData(83, 96, 1455692190902L));
curMouseData.add(new MouseData(98, 98, 1455692190910L));
curMouseData.add(new MouseData(115, 99, 1455692190918L));
curMouseData.add(new MouseData(131, 101, 1455692190926L));
curMouseData.add(new MouseData(148, 102, 1455692190932L));
curMouseData.add(new MouseData(163, 103, 1455692190940L));
curMouseData.add(new MouseData(177, 105, 1455692190948L));
curMouseData.add(new MouseData(192, 105, 1455692190956L));
curMouseData.add(new MouseData(208, 106, 1455692190964L));
curMouseData.add(new MouseData(221, 108, 1455692190972L));
curMouseData.add(new MouseData(237, 109, 1455692190980L));
curMouseData.add(new MouseData(251, 110, 1455692190986L));
curMouseData.add(new MouseData(266, 112, 1455692190994L));
curMouseData.add(new MouseData(281, 113, 1455692191002L));
curMouseData.add(new MouseData(297, 115, 1455692191010L));
curMouseData.add(new MouseData(312, 116, 1455692191018L));
curMouseData.add(new MouseData(328, 117, 1455692191026L));
curMouseData.add(new MouseData(341, 119, 1455692191034L));
curMouseData.add(new MouseData(355, 120, 1455692191040L));
curMouseData.add(new MouseData(367, 123, 1455692191048L));
curMouseData.add(new MouseData(374, 124, 1455692191056L));
curMouseData.add(new MouseData(383, 125, 1455692191064L));
curMouseData.add(new MouseData(385, 126, 1455692191072L));
curMouseData.add(new MouseData(388, 128, 1455692191080L));
curMouseData.add(new MouseData(391, 131, 1455692191088L));
curMouseData.add(new MouseData(397, 137, 1455692191094L));
curMouseData.add(new MouseData(405, 145, 1455692191102L));
curMouseData.add(new MouseData(415, 155, 1455692191110L));
curMouseData.add(new MouseData(423, 163, 1455692191118L));
curMouseData.add(new MouseData(431, 171, 1455692191126L));
curMouseData.add(new MouseData(435, 174, 1455692191134L));
curMouseData.add(new MouseData(438, 177, 1455692191142L));
curMouseData.add(new MouseData(440, 179, 1455692191148L));
curMouseData.add(new MouseData(440, 180, 1455692191172L));
curMouseData.add(new MouseData(440, 181, 1455692191180L));
curMouseData.add(new MouseData(440, 183, 1455692191188L));
curMouseData.add(new MouseData(439, 185, 1455692191196L));
curMouseData.add(new MouseData(437, 188, 1455692191202L));
curMouseData.add(new MouseData(435, 190, 1455692191210L));
curMouseData.add(new MouseData(433, 193, 1455692191218L));
curMouseData.add(new MouseData(430, 198, 1455692191226L));
curMouseData.add(new MouseData(428, 202, 1455692191234L));
curMouseData.add(new MouseData(425, 208, 1455692191242L));
curMouseData.add(new MouseData(420, 214, 1455692191250L));
curMouseData.add(new MouseData(413, 219, 1455692191256L));
curMouseData.add(new MouseData(409, 223, 1455692191264L));
curMouseData.add(new MouseData(405, 226, 1455692191272L));
curMouseData.add(new MouseData(399, 229, 1455692191280L));
curMouseData.add(new MouseData(396, 230, 1455692191288L));
curMouseData.add(new MouseData(393, 231, 1455692191296L));
curMouseData.add(new MouseData(390, 231, 1455692191304L));
curMouseData.add(new MouseData(385, 233, 1455692191310L));
curMouseData.add(new MouseData(374, 235, 1455692191318L));
curMouseData.add(new MouseData(361, 236, 1455692191326L));
curMouseData.add(new MouseData(341, 239, 1455692191334L));
curMouseData.add(new MouseData(319, 242, 1455692191342L));
curMouseData.add(new MouseData(289, 246, 1455692191350L));
curMouseData.add(new MouseData(263, 249, 1455692191358L));
curMouseData.add(new MouseData(236, 253, 1455692191364L));
curMouseData.add(new MouseData(218, 254, 1455692191372L));
curMouseData.add(new MouseData(204, 257, 1455692191380L));
curMouseData.add(new MouseData(193, 258, 1455692191388L));
curMouseData.add(new MouseData(186, 261, 1455692191396L));
curMouseData.add(new MouseData(183, 262, 1455692191404L));
curMouseData.add(new MouseData(181, 263, 1455692191412L));
curMouseData.add(new MouseData(181, 263, 1455692191426L));
curMouseData.add(new MouseData(181, 265, 1455692191434L));
curMouseData.add(new MouseData(181, 267, 1455692191442L));
curMouseData.add(new MouseData(181, 268, 1455692191450L));
curMouseData.add(new MouseData(181, 271, 1455692191458L));
curMouseData.add(new MouseData(180, 274, 1455692191466L));
curMouseData.add(new MouseData(179, 278, 1455692191472L));
curMouseData.add(new MouseData(179, 282, 1455692191480L));
curMouseData.add(new MouseData(179, 285, 1455692191488L));
curMouseData.add(new MouseData(180, 291, 1455692191496L));
curMouseData.add(new MouseData(182, 297, 1455692191504L));
curMouseData.add(new MouseData(186, 303, 1455692191512L));
curMouseData.add(new MouseData(191, 309, 1455692191520L));
curMouseData.add(new MouseData(195, 312, 1455692191526L));
curMouseData.add(new MouseData(201, 317, 1455692191534L));
curMouseData.add(new MouseData(211, 322, 1455692191542L));
curMouseData.add(new MouseData(223, 329, 1455692191550L));
curMouseData.add(new MouseData(239, 338, 1455692191558L));
curMouseData.add(new MouseData(261, 347, 1455692191566L));
curMouseData.add(new MouseData(290, 356, 1455692191574L));
curMouseData.add(new MouseData(318, 363, 1455692191580L));
curMouseData.add(new MouseData(351, 369, 1455692191588L));
curMouseData.add(new MouseData(384, 373, 1455692191596L));
curMouseData.add(new MouseData(421, 377, 1455692191604L));
curMouseData.add(new MouseData(460, 381, 1455692191612L));
curMouseData.add(new MouseData(500, 388, 1455692191620L));
curMouseData.add(new MouseData(541, 394, 1455692191626L));
curMouseData.add(new MouseData(582, 405, 1455692191634L));
curMouseData.add(new MouseData(624, 416, 1455692191642L));
curMouseData.add(new MouseData(676, 430, 1455692191650L));
curMouseData.add(new MouseData(727, 444, 1455692191658L));
curMouseData.add(new MouseData(773, 458, 1455692191666L));
curMouseData.add(new MouseData(811, 471, 1455692191674L));
curMouseData.add(new MouseData(839, 478, 1455692191680L));
curMouseData.add(new MouseData(853, 484, 1455692191688L));
curMouseData.add(new MouseData(863, 489, 1455692191696L));
curMouseData.add(new MouseData(869, 494, 1455692191704L));
curMouseData.add(new MouseData(872, 499, 1455692191712L));
curMouseData.add(new MouseData(874, 501, 1455692191720L));
curMouseData.add(new MouseData(876, 505, 1455692191728L));
curMouseData.add(new MouseData(877, 509, 1455692191734L));
curMouseData.add(new MouseData(877, 512, 1455692191742L));
curMouseData.add(new MouseData(877, 515, 1455692191750L));
curMouseData.add(new MouseData(877, 517, 1455692191758L));
curMouseData.add(new MouseData(875, 519, 1455692191766L));
curMouseData.add(new MouseData(873, 524, 1455692191774L));
curMouseData.add(new MouseData(869, 528, 1455692191782L));
curMouseData.add(new MouseData(866, 532, 1455692191788L));
curMouseData.add(new MouseData(862, 537, 1455692191796L));
curMouseData.add(new MouseData(858, 543, 1455692191806L));
curMouseData.add(new MouseData(853, 549, 1455692191812L));
curMouseData.add(new MouseData(846, 554, 1455692191820L));
curMouseData.add(new MouseData(836, 561, 1455692191828L));
curMouseData.add(new MouseData(827, 569, 1455692191836L));
curMouseData.add(new MouseData(816, 576, 1455692191842L));
curMouseData.add(new MouseData(803, 582, 1455692191850L));
curMouseData.add(new MouseData(787, 587, 1455692191858L));
curMouseData.add(new MouseData(770, 592, 1455692191866L));
curMouseData.add(new MouseData(749, 595, 1455692191874L));
curMouseData.add(new MouseData(719, 598, 1455692191882L));
curMouseData.add(new MouseData(690, 600, 1455692191890L));
curMouseData.add(new MouseData(661, 602, 1455692191896L));
curMouseData.add(new MouseData(637, 602, 1455692191904L));
curMouseData.add(new MouseData(615, 604, 1455692191912L));
curMouseData.add(new MouseData(601, 605, 1455692191920L));
curMouseData.add(new MouseData(589, 605, 1455692191928L));
curMouseData.add(new MouseData(582, 605, 1455692191936L));
curMouseData.add(new MouseData(575, 605, 1455692191944L));
curMouseData.add(new MouseData(570, 605, 1455692191950L));
curMouseData.add(new MouseData(564, 606, 1455692191958L));
curMouseData.add(new MouseData(555, 609, 1455692191966L));
curMouseData.add(new MouseData(543, 613, 1455692191974L));
curMouseData.add(new MouseData(526, 617, 1455692191982L));
curMouseData.add(new MouseData(503, 624, 1455692191990L));
curMouseData.add(new MouseData(477, 627, 1455692191998L));
curMouseData.add(new MouseData(447, 631, 1455692192004L));
curMouseData.add(new MouseData(418, 634, 1455692192012L));
curMouseData.add(new MouseData(392, 636, 1455692192020L));
curMouseData.add(new MouseData(364, 636, 1455692192028L));
curMouseData.add(new MouseData(339, 638, 1455692192036L));
curMouseData.add(new MouseData(313, 640, 1455692192044L));
curMouseData.add(new MouseData(296, 640, 1455692192052L));
curMouseData.add(new MouseData(286, 640, 1455692192058L));
curMouseData.add(new MouseData(281, 640, 1455692192066L));
curMouseData.add(new MouseData(278, 640, 1455692192074L));
curMouseData.add(new MouseData(276, 640, 1455692192082L));
curMouseData.add(new MouseData(275, 640, 1455692192098L));
curMouseData.add(new MouseData(274, 640, 1455692192106L));
curMouseData.add(new MouseData(273, 640, 1455692192120L));
curMouseData.add(new MouseData(273, 640, 1455692192136L));
curMouseData.add(new MouseData(272, 640, 1455692192144L));
curMouseData.add(new MouseData(271, 640, 1455692192152L));
curMouseData.add(new MouseData(271, 640, 1455692192166L));
curMouseData.add(new MouseData(272, 641, 1455692193154L));
curMouseData.add(new MouseData(276, 643, 1455692193162L));
curMouseData.add(new MouseData(285, 647, 1455692193170L));
curMouseData.add(new MouseData(294, 651, 1455692193178L));
curMouseData.add(new MouseData(305, 656, 1455692193186L));
curMouseData.add(new MouseData(318, 663, 1455692193192L));
curMouseData.add(new MouseData(332, 671, 1455692193200L));
curMouseData.add(new MouseData(350, 678, 1455692193208L));
curMouseData.add(new MouseData(370, 688, 1455692193216L));
curMouseData.add(new MouseData(396, 699, 1455692193224L));
curMouseData.add(new MouseData(424, 708, 1455692193232L));
curMouseData.add(new MouseData(457, 723, 1455692193238L));
curMouseData.add(new MouseData(492, 735, 1455692193246L));
curMouseData.add(new MouseData(532, 751, 1455692193254L));
curMouseData.add(new MouseData(577, 767, 1455692193262L));
curMouseData.add(new MouseData(626, 781, 1455692193270L));
curMouseData.add(new MouseData(675, 797, 1455692193278L));
curMouseData.add(new MouseData(730, 811, 1455692193286L));
curMouseData.add(new MouseData(785, 823, 1455692193292L));
curMouseData.add(new MouseData(840, 838, 1455692193300L));
curMouseData.add(new MouseData(895, 850, 1455692193308L));
curMouseData.add(new MouseData(949, 859, 1455692193316L));
curMouseData.add(new MouseData(1003, 866, 1455692193324L));
curMouseData.add(new MouseData(1057, 873, 1455692193332L));
curMouseData.add(new MouseData(1110, 878, 1455692193342L));
curMouseData.add(new MouseData(1159, 880, 1455692193346L));
curMouseData.add(new MouseData(1204, 880, 1455692193354L));
curMouseData.add(new MouseData(1253, 880, 1455692193362L));
curMouseData.add(new MouseData(1302, 879, 1455692193370L));
curMouseData.add(new MouseData(1352, 874, 1455692193378L));
curMouseData.add(new MouseData(1402, 867, 1455692193386L));
curMouseData.add(new MouseData(1449, 860, 1455692193394L));
curMouseData.add(new MouseData(1562, 838, 1455692193402L));
curMouseData.add(new MouseData(1599, 832, 1455692193408L));
curMouseData.add(new MouseData(1637, 823, 1455692193416L));
curMouseData.add(new MouseData(1667, 818, 1455692193424L));
curMouseData.add(new MouseData(1693, 814, 1455692193432L));
curMouseData.add(new MouseData(1717, 809, 1455692193440L));
curMouseData.add(new MouseData(1731, 806, 1455692193448L));
curMouseData.add(new MouseData(1742, 804, 1455692193454L));
curMouseData.add(new MouseData(1743, 802, 1455692193462L));
curMouseData.add(new MouseData(1743, 801, 1455692193470L));
curMouseData.add(new MouseData(1743, 800, 1455692193478L));
curMouseData.add(new MouseData(1742, 798, 1455692193556L));
curMouseData.add(new MouseData(1736, 795, 1455692193562L));
curMouseData.add(new MouseData(1732, 793, 1455692193570L));
curMouseData.add(new MouseData(1730, 793, 1455692193578L));
curMouseData.add(new MouseData(1729, 793, 1455692193624L));
curMouseData.add(new MouseData(1728, 793, 1455692193632L));
curMouseData.add(new MouseData(1724, 793, 1455692193640L));
curMouseData.add(new MouseData(1719, 793, 1455692193648L));
curMouseData.add(new MouseData(1710, 793, 1455692193656L));
curMouseData.add(new MouseData(1700, 793, 1455692193664L));
curMouseData.add(new MouseData(1689, 795, 1455692193670L));
curMouseData.add(new MouseData(1682, 797, 1455692193678L));
curMouseData.add(new MouseData(1676, 799, 1455692193686L));
curMouseData.add(new MouseData(1674, 800, 1455692193694L));
curMouseData.add(new MouseData(1673, 800, 1455692193702L));
curMouseData.add(new MouseData(1671, 800, 1455692193710L));
curMouseData.add(new MouseData(1671, 800, 1455692193724L));
curMouseData.add(new MouseData(1670, 800, 1455692193732L));
curMouseData.add(new MouseData(1668, 800, 1455692193741L));
curMouseData.add(new MouseData(1663, 800, 1455692193748L));
curMouseData.add(new MouseData(1656, 800, 1455692193756L));
curMouseData.add(new MouseData(1648, 800, 1455692193764L));
curMouseData.add(new MouseData(1642, 800, 1455692193770L));
curMouseData.add(new MouseData(1639, 800, 1455692193778L));
curMouseData.add(new MouseData(1637, 800, 1455692193786L));
curMouseData.add(new MouseData(1634, 799, 1455692193794L));
curMouseData.add(new MouseData(1631, 799, 1455692193802L));
curMouseData.add(new MouseData(1628, 798, 1455692193810L));
curMouseData.add(new MouseData(1627, 798, 1455692193818L));
curMouseData.add(new MouseData(1625, 797, 1455692193826L));
curMouseData.add(new MouseData(1623, 797, 1455692193832L));
curMouseData.add(new MouseData(1622, 797, 1455692193840L));
curMouseData.add(new MouseData(1621, 797, 1455692193848L));
curMouseData.add(new MouseData(1620, 797, 1455692193864L));
curMouseData.add(new MouseData(1620, 796, 1455692193880L));
curMouseData.add(new MouseData(1619, 796, 1455692193902L));
curMouseData.add(new MouseData(1619, 796, 1455692193910L));
curMouseData.add(new MouseData(1617, 796, 1455692193918L));
curMouseData.add(new MouseData(1612, 797, 1455692193926L));
curMouseData.add(new MouseData(1606, 799, 1455692193934L));
curMouseData.add(new MouseData(1602, 800, 1455692193940L));
curMouseData.add(new MouseData(1599, 801, 1455692193948L));
curMouseData.add(new MouseData(1598, 801, 1455692193956L));
curMouseData.add(new MouseData(1596, 801, 1455692193964L));
curMouseData.add(new MouseData(1596, 801, 1455692193986L));
curMouseData.add(new MouseData(1596, 801, 1455692194142L));
curMouseData.add(new MouseData(1596, 800, 1455692194156L));
curMouseData.add(new MouseData(1596, 799, 1455692194164L));
curMouseData.add(new MouseData(1595, 798, 1455692194196L));
curMouseData.add(new MouseData(1595, 798, 1455692194210L));
curMouseData.add(new MouseData(1595, 797, 1455692194226L));
curMouseData.add(new MouseData(1595, 796, 1455692194242L));
curMouseData.add(new MouseData(1594, 795, 1455692194264L));
curMouseData.add(new MouseData(1594, 795, 1455692194412L));
	}
	
	public void printMouseData() {
		synchronized (lock) {
			for (MouseData mouseData : curMouseData) {
				System.out.println("curMouseData.add(new MouseData(" + mouseData.getX() + ", " + mouseData.getY() + ", " + mouseData.getTime() + "L));");
			}
		}
	}

	@Override
	public void nativeMouseDragged(NativeMouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nativeMouseMoved(NativeMouseEvent arg0) {
		Point p = new Point (arg0.getX(), arg0.getY());
		p = HardwareInput.hardwareToClient(p);
		
		_mouseMoved(p.x, p.y);
	}

	@Override
	public void nativeMouseClicked(NativeMouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void nativeMousePressed(NativeMouseEvent arg0) {
		Point p = new Point (arg0.getX(), arg0.getY());
		p = HardwareInput.hardwareToClient(p);
		
		_mousePressed(arg0.getButton(), p.x, p.y);
	}

	@Override
	public void nativeMouseReleased(NativeMouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
