package org.rs3.api.input.realmouse;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.boot.Frame;

public class MouseData {

	short x;
	short y;
	long time;
	
	private static Map<String, List<MouseData>> mdMap = null;
	
	public MouseData(int x, int y, long time) {
		this.x = (short) x;
		this.y = (short) y;
		this.time = time;
	}
	
	public MouseData(short x, short y, long time) {
		this.x = x;
		this.y = y;
		this.time = time;
	}
	
	public short getX() {
		return x;
	}
	
	public short getY() {
		return y;
	}
	
	public long getTime() {
		return time;
	}
	
	public static Map<String, List<MouseData>> getMouseData() {
		if (mdMap != null)
			return mdMap;
		
		System.out.println("Loading mouse data...");
		
		mdMap = new HashMap<>();
		
		//System.out.println("Path " + MouseData.class.getClassLoader().getResource("."));
		
		String path = MouseData.class.getClassLoader().getResource(".").toString().endsWith("/") ? "mouse/mouse.dat" : "/mouse/mouse.dat";
		URL url = MouseData.class.getClassLoader().getResource(path);
		
		try (BufferedReader br = new BufferedReader(new FileReader(url.getPath()))) {
		    String line = br.readLine();
		    
		    while (line != null) {
		    	String[] keyValue = line.split("=");
		    	String key = keyValue[0];
		    	String value = keyValue[1];
		    	
		    	List<MouseData> mdList = new ArrayList<>();
		    	for (String mdStr : value.split(";")) {
		    		String[] mdStrSep = mdStr.split(",");
		    		short xCoord = Short.parseShort(mdStrSep[0]);
		    		short yCoord = Short.parseShort(mdStrSep[1]);
		    		long time = Long.parseLong(mdStrSep[2]);
		    		
		    		MouseData mouseData = new MouseData(xCoord, yCoord, time);
		    		mdList.add(mouseData);
		    	}
		    	
		    	mdMap.put(key, mdList);
		    	
		        line = br.readLine();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Successfully loaded " + mdMap.size() + " mouse records.");
		//System.gc();
		
		// rebase coords
		for (String key : mdMap.keySet()) {
			List<MouseData> mdList = mdMap.get(key);
			short initialX = mdList.get(0).x;
			short initialY = mdList.get(0).y;
			
			for (MouseData md : mdList) {
				md.x = (short) (md.x - initialX);
				md.y = (short) (md.y - initialY);
			}
		}
		
		return mdMap;
	}
}
