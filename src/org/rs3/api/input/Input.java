package org.rs3.api.input;

public class Input {
	private static boolean useHardwareInput = true; // set in Frame and OfficialClientInject using setter
	
	public static boolean useHardWareInput() {
		return Input.useHardwareInput;
	}
	
	public static void setUseHardWareInput(boolean useHardwareInput) {
		Input.useHardwareInput = useHardwareInput;
	}
}
