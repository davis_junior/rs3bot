package org.rs3.api.input;

import org.rs3.eventqueue.EventNazi;
import org.rs3.util.Condition;

public class Keyboard {
	private static EventNazi nazi = null;
	private static HardwareInput hardwareInput = null;
	
	public static void setup(EventNazi nazi) {
		Keyboard.nazi = nazi;
	}
	
	public static void setup(HardwareInput hardwareInput) {
		Keyboard.hardwareInput = hardwareInput;
	}
	
	
    public static synchronized void pressKey(int code) {
    	if (Input.useHardWareInput())
    		hardwareInput.pressKey(code);
    	else
    		nazi.pressKey(code);
    }
    
    public static synchronized void releaseKey(int code) {
    	if (Input.useHardWareInput())
    		hardwareInput.releaseKey(code);
    	else
    		nazi.releaseKey(code);
    }
    
    /*public static synchronized void passKeyEvent(KeyEvent e) {
    	if (Input.useHardWareInput())
    	// TODO
    	else
    	nazi.passKeyEvent(e);
    }*/
    
    public static boolean isKeyDown(int code) {
    	if (Input.useHardWareInput())
    		return hardwareInput.isKeyDown(code);
    	else
    		return nazi.isKeyDown(code);
    }
    
    public static synchronized void sendKeys(String text, int keywait, int keymodwait) {
    	if (Input.useHardWareInput())
    		hardwareInput.sendKeys(text, keywait, keymodwait);
    	else
    		nazi.sendKeys(text, keywait, keymodwait);
    }
    
    public static synchronized void sendKeys(String text) {
    	if (Input.useHardWareInput())
    		hardwareInput.sendKeys(text, 90, 60);
    	else
    		nazi.sendKeys(text, 90, 60);
    }
    
    public static synchronized void holdKey(int code, int wait, Condition breakCondition) {
    	if (Input.useHardWareInput())
    		hardwareInput.holdKey(code, wait, breakCondition);
    	else
    		nazi.holdKey(code, wait, breakCondition);
    }
    
    public static synchronized void holdKey(int code, int wait) {
    	if (Input.useHardWareInput())
    		hardwareInput.holdKey(code, wait);
    	else
    		nazi.holdKey(code, wait);
    }
    
    public static synchronized void sendKey(int code) {
    	if (Input.useHardWareInput())
    		hardwareInput.sendKey(code);
    	else
    		nazi.sendKey(code);
    }
}
