package org.rs3.api.input;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.swt.widgets.Display;
import org.rs3.accessors.CustomClassLoader;
import org.rs3.api.Calculations;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.realmouse.Block;
import org.rs3.api.input.realmouse.MouseData;
import org.rs3.api.wrappers.Client;
import org.rs3.boot.BootType;
import org.rs3.boot.Frame;
import org.rs3.boot.OfficialClientInject;
import org.rs3.callbacks.capture.CanvasCapture;
import org.rs3.debug.Paint;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class HardwareInput {

	private static int mouseSpeed = 20;
	
	private static Robot robot = null;
	
	public static boolean createRobot() {
		try {
			robot = new Robot();
			//robot.setAutoDelay(0);
			return true;
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static Robot getRobot() {
		return robot;
	}

	public static int getMouseSpeed() {
		return mouseSpeed;
	}

	public static void setMouseSpeed(int speed) {
		HardwareInput.mouseSpeed = speed;
	}
	
	private int cx, cy;
	private Set<int[]> keysHeld;
	private boolean mousein;
	private boolean leftDown;
	private boolean midDown;
	private boolean rightDown;
	private boolean shiftDown;
	
	public HardwareInput() {
		createRobot();
		
		updateMouseCoords();
		mousein = true;
		
		leftDown = false;
		midDown = false;
		rightDown = false;
		shiftDown = false;
		keysHeld = Collections.synchronizedSet(new HashSet<int[]>());
	}
	
	private void updateMouseCoords() {
		cx = MouseInfo.getPointerInfo().getLocation().x;
		cy = MouseInfo.getPointerInfo().getLocation().y;
	}
	
	public static Point hardwareToClient(Point point) {
		Point newPoint = new Point();
		
		//Point p = CustomClassLoader.get.rs2app.getLocationOnScreen();
		Point p;
		if (!Frame.recordMouseMode || BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
			java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();
			if (!canvas.isShowing())
				return null;
			p = canvas.getLocationOnScreen();
		} else {
			if (!Frame.window.panel.isShowing())
				return null;
			p = Frame.window.panel.getLocationOnScreen();
		}
		
		newPoint.x = point.x - p.x;
		newPoint.y = point.y - p.y;
		
		return newPoint;
		
		//r.mouseMove(p.x + Client.getCanvasWidth() - 1, p.y + Client.getCanvasHeight() - 6);
	}
	
	public static Point clientToHardware(Point point) {
		Point newPoint = new Point();
		
		//Point p = CustomClassLoader.get.rs2app.getLocationOnScreen();
		Point p;
		if (!Frame.recordMouseMode || BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
			java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();
			if (!canvas.isShowing())
				return null;
			p = canvas.getLocationOnScreen();
		} else {
			if (!Frame.window.panel.isShowing())
				return null;
			p = Frame.window.panel.getLocationOnScreen();
		}
		
		newPoint.x = p.x + point.x;
		newPoint.y = p.y + point.y;
		
		return newPoint;
		
		//r.mouseMove(p.x + Client.getCanvasWidth() - 1, p.y + Client.getCanvasHeight() - 6);
	}

	public void wait(int min, int max) {
		int time = min == max ? min : (int)((Math.random() * Math.abs(max - min)) + Math.min(min, max));
		try { Thread.sleep(time); } catch (Exception e) { e.printStackTrace(); }
	}

	public void wait(int mills) {
		wait(mills, mills);
	}

	public Point getMousePos() {
		return hardwareToClient(MouseInfo.getPointerInfo().getLocation());
	}

	private synchronized Point moveMouseImpl(int x, int y) {
		if (cx == x && cy == y)
			return new Point(cx, cy);
		
		/*int btnMask = (leftDown ? InputEvent.BUTTON1_DOWN_MASK : 0) | (rightDown ? (InputEvent.BUTTON3_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0);
		if (isDragging()) {
			BlockingEventQueue.sendUnblocked(new MouseEvent(comp,MouseEvent.MOUSE_DRAGGED,System.currentTimeMillis(),btnMask,x,y,0,false,0));
		} else if (x >= 0 && x < comp.getWidth() && y >= 0 && y < comp.getHeight()) {
			if (mousein) {
				BlockingEventQueue.sendUnblocked(new MouseEvent(comp,MouseEvent.MOUSE_MOVED,System.currentTimeMillis(),btnMask,x,y,0,false,0));
			} else {
				mousein = true;
				BlockingEventQueue.sendUnblocked(new MouseEvent(comp,MouseEvent.MOUSE_ENTERED,System.currentTimeMillis(),btnMask,x,y,0,false,0));
			}
		} else {
			if (mousein) {
				BlockingEventQueue.sendUnblocked(new MouseEvent(comp,MouseEvent.MOUSE_EXITED,System.currentTimeMillis(),btnMask,x,y,0,false,0));
				mousein = false;
			} else {
				//Mouse outside and still out, no event.
			}
		}*/
		robot.mouseMove(x, y);
		updateMouseCoords();
		return new Point(cx, cy);
	}

	/**
	 * Internal mouse movement algorithm. Do not use this without credit to either
	 * Benjamin J. Land or BenLand100. This is synchronized to prevent multiple
	 * motions and bannage.
	 * @param xs The x start
	 * @param ys The y start
	 * @param xe The x destination
	 * @param ye The y destination
	 * @param gravity Strength pulling the position towards the destination
	 * @param wind Strength pulling the position in random directions
	 * @param minWait Minimum relative time per step
	 * @param maxWait Maximum relative time per step
	 * @param maxStep Maximum size of a step, prevents out of control motion
	 * @param targetArea Radius of area around the destination that should
	 *                   trigger slowing, prevents spiraling
	 * @result The actual end point
	 */
	/*private synchronized Point windMouseImpl(double xs, double ys, double xe, double ye, double gravity, double wind, double minWait, double maxWait, double maxStep, double targetArea) {
        //System.out.println(targetArea);
        final double sqrt3 = Math.sqrt(3);
        final double sqrt5 = Math.sqrt(5);


        double dist, veloX = 0, veloY = 0, windX = 0, windY = 0;
        while ((dist = Math.hypot(xs - xe,ys - ye)) >= 1) {
            wind = Math.min(wind, dist);
            if (dist >= targetArea) {
                windX = windX / sqrt3 + (Math.random() * (wind * 2D + 1D) - wind) / sqrt5;
                windY = windY / sqrt3 + (Math.random() * (wind * 2D + 1D) - wind) / sqrt5;
            } else {
                windX /= sqrt3;
                windY /= sqrt3;
                if (maxStep < 3) {
                    maxStep = Math.random() * 3 + 3D;
                } else {
                    maxStep /= sqrt5;
                }
                //System.out.println(maxStep + ":" + windX + ";" + windY);
            }
            veloX += windX + gravity * (xe - xs) / dist;
            veloY += windY + gravity * (ye - ys) / dist;
            double veloMag = Math.hypot(veloX, veloY);
            if (veloMag > maxStep) {
                double randomDist = maxStep / 2D + Math.random() * maxStep / 2D;
                veloX = (veloX / veloMag) * randomDist;
                veloY = (veloY / veloMag) * randomDist;
            }
            xs += veloX;
            ys += veloY;
            int mx = (int) Math.round(xs);
            int my = (int) Math.round(ys);
            if (cx != mx || cy != my) {
                //Scratch
	 *//*g.drawLine(cx,cy,mx,my);
                frame.repaint();*/
	//MouseJacking
	/*try {
                    Robot r = new Robot();
                    r.mouseMove(mx,my);
                } catch (Exception e) { } *//*
                moveMouseImpl(mx, my);
            }
            double step = Math.hypot(xs - cx, ys - cy);
            try {
                Thread.sleep(Math.round((maxWait - minWait) * (step / maxStep) + minWait));
            } catch (InterruptedException ex) {  }
        }
        //System.out.println(Math.abs(xe - cx) + ", " + Math.abs(ye - cy));
        return new Point(cx, cy);
    }*/

	// finished port from gist.github (from srl)
	/*private synchronized Point _humanWindMouse(int xs, int ys, int xe, int ye) {
        double targetArea = ((Math.random() * MOUSESPEED) / 2.0 + MOUSESPEED);
        int gravity = 7;
        int wind = 5;

        double x = xs;
        double y = ys;
        double veloX = 0, veloY = 0;
        double veloMag, dist, randomDist, d;
        double windX = 0, windY = 0;
        int lastX, lastY, w, tDist;
        long timeOut;
        double sqrt2, sqrt3, sqrt5, maxStep, rCnc;
        Point startPoint = new Point(xs, ys);
        Point endPoint = new Point(xe, ye);

        sqrt2 = Math.sqrt(2);
        sqrt3 = Math.sqrt(3);
        sqrt5 = Math.sqrt(5);

        //tDist = startPoint.distance(endPoint);
        tDist = (int) Calculations.distance(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
        timeOut = System.currentTimeMillis() + 10000;

        //log.debug("Moving mouse from ($xs, $ys) to ($xe, $ye)");

        while (System.currentTimeMillis() < timeOut) {
            dist = Math.hypot(x - xe, y - ye);
            wind = (int) Math.min(wind, dist);

            dist = Math.max(dist, 1);
            d = Math.round((Math.round(tDist) * 0.3) / 7);
            d = Math.min(d, 25);
            d = Math.max(d, 5);

            rCnc = Math.round(Math.random() * 6);
            if (rCnc == 1) {
                d = Math.random() + 2;
            }

            maxStep = Math.min(d, Math.round(dist));

            if (dist >= targetArea) {
                windX = windX / sqrt3 + (Math.random() * (Math.round(wind) * 2 + 1) - wind) / sqrt5;
                windY = windY / sqrt3 + (Math.random() * (Math.round(wind) * 2 + 1) - wind) / sqrt5;
            } else {
                windX = windX / sqrt2;
                windY = windY / sqrt2;
            }

            veloX += windX;
            veloY += windY;

            veloX += gravity * (xe - x) / dist;
            veloY += gravity * (ye - y) / dist;

            if (Math.hypot(veloX, veloY) > maxStep) {
                randomDist = maxStep / 2.0 + (Math.random() * (Math.round(maxStep) / 2));
                veloMag = Math.sqrt(veloX * veloX + veloY * veloY);
                veloX = (veloX / veloMag) * randomDist;
                veloY = (veloY / veloMag) * randomDist;
            }

            lastX = (int) Math.round(x);
            lastY = (int) Math.round(y);

            x = x + veloX;
            y = y + veloY;

            if (lastX != Math.round(x) || (lastY != Math.round(y))) {
                moveMouse((int)Math.round(x), (int)Math.round(y));
            }

            w = (int) (Math.random() * (Math.round(100 / MOUSESPEED)) * 6);
            w = Math.max(w, 5);
            w = (int) Math.round(w * 0.9);
            Time.sleep(w);

            if (Math.hypot(x - xe, y - ye) < 1) {
                break;
            }
        }

        if (Math.round(xe) != Math.round(x) || (Math.round(ye) != Math.round(y))) {
            moveMouse(xe, ye);
        }

        return new Point (cx, cy);
    }*/

	/**
	 * Ported by me from SRL. (Almost exact)
	 * @param xs The x start
	 * @param ys The y start
	 * @param xe The x destination
	 * @param ye The y destination
	 * @param gravity Strength pulling the position towards the destination
	 * @param wind Strength pulling the position in random directions
	 * 		@param minWait Minimum relative time per step
	 * 		@param maxWait Maximum relative time per step
	 * 		@param maxStep  (only used internally, this is not a used param anymore) Maximum size of a step, prevents out of control motion
	 * @param targetArea Radius of area around the destination that should
	 *                   trigger slowing, prevents spiraling
	 * @result The actual end point
	 */
	private synchronized Point _humanWindMouse(double xs, double ys, double xe, double ye, double gravity, double wind, double minWait, double maxWait, double targetArea) {
		int MSP = mouseSpeed;
		final double sqrt2 = Math.sqrt(2);
		final double sqrt3 = Math.sqrt(3);
		final double sqrt5 = Math.sqrt(5);

		int TDist = (int) Calculations.distance(Math.round(xs), Math.round(ys), Math.round(xe), Math.round(ye));
		long t = System.currentTimeMillis() + 10000;

		double windX = 0;
		double windY = 0;
		double veloX = 0;
		double veloY = 0;

		double lastDist = 0; // unused?

		do {
			if (System.currentTimeMillis() > t)
				break;

			double dist = Math.hypot(xs - xe, ys - ye);
			wind = Math.min(wind, dist);

			if (dist < 1)
				dist = 1;

			double D = (Math.round((Math.round(TDist) * 0.3D)) / 7D);

			if (D > 25)
				D = 25;
			if (D < 5)
				D = 5;

			//double rCnc = Math.random() * 6 + 6D; // TODO: double check random(6)
			double rCnc = Random.nextDouble(0D, 6D);
			if (rCnc == 1)
				D = Random.nextDouble(2, 3);

			double maxStep;
			if (D <= Math.round(dist))
				maxStep = D;
			else
				maxStep = Math.round(dist);

			if (dist >= targetArea) {
				//windX = windX / sqrt3 + (Math.random() * (wind * 2D + 1D) - wind) / sqrt5; // TODO: double check, this is almost exactly like Benland's method
				//windY = windY / sqrt3 + (Math.random() * (wind * 2D + 1D) - wind) / sqrt5; // TODO: double check, this is almost exactly like Benland's method
				windX = windX / sqrt3 + (Random.nextInt(0, (int) Math.round(wind) * 2 + 1) - wind) / sqrt5;
				windY = windY / sqrt3 + (Random.nextInt(0, (int) Math.round(wind) * 2 + 1) - wind) / sqrt5;
			} else {
				windX /= sqrt2;
				windY /= sqrt2;
			}

			veloX += windX + gravity * (xe - xs) / dist;
			veloY += windY + gravity * (ye - ys) / dist;

			if (Math.hypot(veloX, veloY) > maxStep) {
				//double randomDist = maxStep / 2D + Math.random() * Math.round(maxStep) / 2D; // TODO: double check, this is almost exactly like Benland's method
				double randomDist = maxStep / 2D + Random.nextInt(0, (int) Math.round(maxStep) / 2); // the last / is div in pascal, which truncates the result
				double veloMag = Math.sqrt(veloX * veloX + veloY * veloY);
				veloX = (veloX / veloMag) * randomDist;
				veloY = (veloY / veloMag) * randomDist;
			}

			int lastX = (int) Math.round(xs);
			int lastY = (int) Math.round(ys);
			xs += veloX;
			ys += veloY;

			// ADDED to prevent mouse from spazzing
			if (dist >= 1D && lastDist != 0D && dist > lastDist) {
				//System.out.println("Mouse spaz detected; trying again...");

				int W = Random.nextInt(0, Math.round(100F / MSP)) + 6;
				W = Math.round(W * 1.1F);
				if (W < 7)
					W = 7;
				Time.sleep(W);

				return _humanWindMouse(lastX, lastY, xe, ye, gravity, wind, minWait, maxWait, targetArea);
			}

			if (lastX != (int)Math.round(xs) || lastY != (int)Math.round(ys)) {
				moveMouseImpl((int) Math.round(xs), (int) Math.round(ys));
				
				if (CanvasCapture.readPixels) {
					synchronized (Paint.mousePoints) {
						
						Paint.mousePoints.put(System.currentTimeMillis(), new Point[] {hardwareToClient(new Point(lastX, lastY)), hardwareToClient(new Point((int) Math.round(xs), (int) Math.round(ys)))});
					}
				}
			}

			int W = Random.nextInt(0, Math.round(100F / MSP)) + 6;
			W = Math.round(W * 1.1F);
			if (W < 7)
				W = 7;
			Time.sleep(W);

			lastDist = dist;

			//System.out.println(Math.hypot(xs - xe, ys - ye));

		} while (Math.hypot(xs - xe, ys - ye) >= 1D);


		if (Math.round(xe) != Math.round(xs) || Math.round(ye) != Math.round(ys))
			moveMouseImpl((int) Math.round(xe), (int) Math.round(ye));

		mouseSpeed = MSP; // MSP is never changed?

		//return getMousePos();
		return new Point(cx, cy);
	}

	/**
	 * Moves the mouse from the current position to the specified position.
	 * Approximates human movement in a way where smoothness and accuracy are
	 * relative to speed, as it should be.
	 * @param x The x destination
	 * @param y The y destination
	 * @result The actual end point
	 */
	/*public synchronized Point windMouse(int x, int y) {
        if (canInteract()) {
            double speed = (Math.random() * 15D + 15D) / 10D;
            return windMouseImpl(cx,cy,x,y,9D,3D,5D/speed,10D/speed,10D*speed,8D*speed);
        }
        return null;
    }*/

	// partial port from gist.github (from srl)
	/*public synchronized Point mouse(int x, int y, int button) {
        if (canInteract()) {
            //double speed = (Math.random() * MOUSESPEED / 2D + MOUSESPEED) / 10D;
           _humanWindMouse(cx,cy,x,y);
           return fastClick(button);
        }
        return null;
    }*/

	private synchronized Point _sampleMoveMouse(int xs, int ys, int xe, int ye) {
		Point clientStart = hardwareToClient(new Point(xs, ys));
		Point clientEnd = hardwareToClient(new Point(xe, ye));
		
		Block srcBlock = null;
		for (Block block : Block.values()) {
			if (block.getBounds().contains(clientStart.x, clientStart.y)) {
				srcBlock = block;
				break;
			}
		}
		
		Block dstBlock = null;
		for (Block block : Block.values()) {
			if (block.getBounds().contains(clientEnd.x, clientEnd.y)) {
				dstBlock = block;
				break;
			}
		}
		
		if (srcBlock != null && dstBlock != null) {
			if (srcBlock.getId() == dstBlock.getId())
				return _infinityMoveMouse(xs, ys, xe, ye);
			
			List<MouseData> mdList = MouseData.getMouseData().get(srcBlock.getId() + "->" + dstBlock.getId());
			if (mdList != null) {
				for (MouseData md : mdList) {
					Time.sleep(md.getTime());
					moveMouseImpl(xs + md.getX(), ys + md.getY());
				}
				Time.sleep(8);
				return _infinityMoveMouse(cx, cy, xe, ye);
			}
		} else {
			System.out.println("Using inifinity bot mouse movement algo!");
			return _infinityMoveMouse(xs, ys, xe, ye);
		}
		
		return null;
	}
	
	private synchronized Point _infinityMoveMouse(int xs, int ys, int xe, int ye) {
		Point origin = new Point(xs,ys);
		Point [] points = InfinityBotMouse.generatePath(origin, new Point(xe,ye));
		for (int i = 0; i < points.length; i++){
			moveMouseImpl(points[i].x, points[i].y);
			
			// added
			/*int W = Random.nextInt(0, Math.round(100F / mouseSpeed)) * 2;
			if (W < 3)
				W = 3;
			W = Math.round(W * 0.9F);
			Time.sleep(W);*/
			int W = Random.nextInt(0, Math.round(100F / mouseSpeed)) * 4;
			if (W < 8)
				W = 9;
			W = Math.round(W * 0.9F);
			Time.sleep(W);
			// end added
			//System.out.println(W);
		}
		return points[points.length - 1];
	}
	
	// ported by me from SRL (almost exact), along with some of SMART's code.
	public synchronized Point mouse(int x, int y, int button) {
		if (canInteract()) {
			int ms = mouseSpeed;
			double randSpeed = (Random.nextInt(0, mouseSpeed) / 2D + mouseSpeed) / 10D;
			//double speed = (Math.random() * MOUSESPEED / 2D + MOUSESPEED) / 10D;

			int MOUSE_HUMAN = 7;
			updateMouseCoords();
			Point hardwarePoint = clientToHardware(new Point (x, y));
			if (hardwarePoint == null)
				return null;
			
			//_humanWindMouse(cx, cy, hardwarePoint.x, hardwarePoint.y, MOUSE_HUMAN, 5, 10D / randSpeed, 15D / randSpeed, 10D * randSpeed);
			//_infinityMoveMouse(cx, cy, hardwarePoint.x, hardwarePoint.y);
			_sampleMoveMouse(cx, cy, hardwarePoint.x, hardwarePoint.y);
			
			mouseSpeed = ms; // ms never changed?

			Time.sleep(0.9F, 16, 70, 16, 100);
			return fastClick(button);
		}

		return null;
	}

	public synchronized Point mouse(int x, int y) {
		return mouse(x, y, -1);
	}
	
	// ADDED: about same as mouse(...)
	public synchronized Point drag(int x, int y, int button) {
		if (BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
			// TODO
		} else {
			if (!Frame.window.activated)
				return null;
		}
		
		if (canInteract()) {
			int ms = mouseSpeed;
			double randSpeed = (Random.nextInt(0, mouseSpeed) / 2D + mouseSpeed) / 10D;
			//double speed = (Math.random() * MOUSESPEED / 2D + MOUSESPEED) / 10D;

			int MOUSE_HUMAN = 7;
			updateMouseCoords();
			Point hardwarePoint = clientToHardware(new Point (x, y));
			if (hardwarePoint == null)
				return null;
			
			long cur = System.currentTimeMillis();
			if (button != -1) {
				if (Mouse.lastClickMs != 0L)
					System.out.println(cur - Mouse.lastClickMs);
				
				Mouse.lastClickMs = cur;
				
				pressMouse(cx, cy, button);
				Time.sleep(30, 75);
			}
			
			//_humanWindMouse(cx, cy, hardwarePoint.x, hardwarePoint.y, MOUSE_HUMAN, 5, 10D / randSpeed, 15D / randSpeed, 10D * randSpeed);
			//_infinityMoveMouse(cx, cy, hardwarePoint.x, hardwarePoint.y);
			_sampleMoveMouse(cx, cy, hardwarePoint.x, hardwarePoint.y);
			
			if (button != -1) {
				Time.sleep(30, 75);
				releaseMouse(cx, cy, button);
				
				cur = System.currentTimeMillis();
				Mouse.lastClickMs = cur;
			}
			
			mouseSpeed = ms; // ms never changed?

			return new Point(cx, cy);
		}

		return null;
	}

	/**
	 * Moves the mouse from the current position to the specified position.
	 * Approximates human movement in a way where smoothness and accuracy are
	 * relative to speed, as it should be.
	 * @param x The x destination
	 * @param y The y destination
	 * @result The actual end point
	 */
	public synchronized Point moveMouse(int x, int y) {
		if (canInteract()) {
			return moveMouseImpl(x,y);
		}
		return null;
	}

	/**
	 * Holds the mouse at the specified position after moving from the current
	 * position to the specified position.
	 * @param x The x destination
	 * @param y The y destination
	 * @result The actual end point
	 */
	public synchronized Point pressMouse(int x, int y, int button) {
		if (canHold(button)) {
			int btnMask = ((leftDown || button==1) ? InputEvent.BUTTON1_DOWN_MASK : 0) | ((midDown || button==2) ? (InputEvent.BUTTON2_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0) | ((rightDown || button==3) ? (InputEvent.BUTTON3_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0);
			int btn = 0;
			switch (button) {
			case 1: btn = InputEvent.BUTTON1_DOWN_MASK; break;
			case 2: btn = InputEvent.BUTTON2_DOWN_MASK; break;
			case 3: btn = InputEvent.BUTTON3_DOWN_MASK; break;
			}
			Point end = moveMouse(x,y);
			if (mousein) {
				robot.mousePress(btn);
				/*if (!isFocused()) {
					wait(25,50);
					getFocus();
				}*/
				switch (button) {
				case 1: leftDown = true; break;
				case 2: midDown = true; break;
				case 3: rightDown = true; break;
				}
			}
			return end;
		}
		return null;
	}

	/**
	 * Releases the mouse at the specified position after moving from the current
	 * position to the specified position.
	 * @param x The x destination
	 * @param y The y destination
	 * @result The actual end point
	 */
	public synchronized Point releaseMouse(int x, int y, int button) {
		if (canRelease(button)) {
			int btnMask = ((leftDown || button==1) ? InputEvent.BUTTON1_DOWN_MASK : 0) | ((midDown || button==2) ? (InputEvent.BUTTON2_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0) | ((rightDown || button==3) ? (InputEvent.BUTTON3_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0);
			int btn = 0;
			switch (button) {
			case 1: btn = InputEvent.BUTTON1_DOWN_MASK; break;
			case 2: btn = InputEvent.BUTTON2_DOWN_MASK; break;
			case 3: btn = InputEvent.BUTTON3_DOWN_MASK; break;
			}
			Point end = moveMouse(x,y);
			if (mousein) {
				//long time = System.currentTimeMillis();
				robot.mouseRelease(btn);
				switch (button) {
				case 1: leftDown = false; break;
				case 2: midDown = false; break;
				case 3: rightDown = false; break;
				}
			} else {
				loseFocus(false);
			}
			return end;
		}
		return null;
	}

	// ported from SRL
	public synchronized Point fastClick(int button) {
		if (BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
			// TODO
		} else {
			if (!Frame.window.activated)
				return null;
		}
		
		if (button != -1) {
			long cur = System.currentTimeMillis();
			if (Mouse.lastClickMs != 0L)
				System.out.println(cur - Mouse.lastClickMs);
			
			Mouse.lastClickMs = cur;

			pressMouse(cx, cy, button);
			Time.sleep(60, 150);
			releaseMouse(cx, cy, button);
		}

		return new Point(cx, cy);
	}

	/**
	 * Clicks the mouse at the specified position after moving from the current
	 * position to the specified position.
	 * @param x The x destination
	 * @param y The y destination
	 * @result The actual end point
	 */
	public synchronized Point clickMouse(int x, int y, int button) {
		if (canClick(button)) {
			int btnMask = ((leftDown || button==1) ? InputEvent.BUTTON1_DOWN_MASK : 0) | ((midDown || button==2) ? (InputEvent.BUTTON2_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0) | ((rightDown || button==3) ? (InputEvent.BUTTON3_DOWN_MASK | InputEvent.META_DOWN_MASK) : 0);
			int btn = 0;
			switch (button) {
			case 1: btn = InputEvent.BUTTON1_MASK; break;
			case 2: btn = InputEvent.BUTTON2_MASK; break;
			case 3: btn = InputEvent.BUTTON3_MASK; break;
			}
			Point end = moveMouse(x,y);
			if (mousein) {
				robot.mousePress(btn);
				switch (button) {
				case 1: leftDown = true; break;
				case 2: midDown = true; break;
				case 3: rightDown = true; break;
				}
				/*if (!isFocused()) {
					wait(25,50);
					getFocus();
				}*/
				try { Thread.sleep((int)(Math.random() * 56 + 90)); } catch (Exception ex) { }
				//long time = System.currentTimeMillis();
				robot.mouseRelease(btn);
				switch (button) {
				case 1: leftDown = false; break;
				case 2: midDown = false; break;
				case 3: rightDown = false; break;
				}
			} else {
				loseFocus(false);
			}
			return end;
		}
		return null;
	}

	public boolean isMouseButtonHeld(int button) {
		switch (button) {
		case 1: return leftDown;
		case 2: return midDown;
		case 3: return rightDown;
		}
		return false;
	}

	public synchronized Point scrollMouse(int lines) {
		if (canInteract()) {
			if (mousein) {
				robot.mouseWheel(lines);
				return new Point(cx, cy);
			}
		}
		return null;
	}
	
	public synchronized Point scrollMouse(int x, int y, int lines) {
		if (canInteract()) {
			Point end = mouse(x, y);
			Time.sleepQuickest();
			if (mousein) {
				robot.mouseWheel(lines);
				return new Point(x, y);
			}
			return end;
		}
		return null;
	}

	/**
	 * Tests if a character requires the shift key to be pressed.
	 * @param c Char to check for
	 * @result True if shift is required
	 */
	private boolean isShiftChar(char c) {
		String special = "~!@#$%^&*()_+|{}:\"<>?";
		return special.indexOf(c) != -1 || (c - 'A' >= 0 && c - 'A' <= 25);
	}

	public static int[] typable_vk_keycode = new int[0xff];
	static {
		typable_vk_keycode[32] = 32;
		for (int c = 'A'; c <= 'Z'; c++) typable_vk_keycode[c] = c;
		for (int c = '0'; c <= '9'; c++) typable_vk_keycode[c] = c;
		typable_vk_keycode[186] = ';'; //  ;:
		typable_vk_keycode[187] = '='; //  =+
		typable_vk_keycode[188] = ','; // hack: ,
		typable_vk_keycode[189] = '-'; //  -_
		typable_vk_keycode[190] = '.'; //  .>
		typable_vk_keycode[191] = '/'; //  /?
		typable_vk_keycode[192] = '`'; //  `~
		typable_vk_keycode[219] = '['; //  [{
		typable_vk_keycode[220] = '\\';//  \|
		typable_vk_keycode[221] = ']'; //  ]}
		typable_vk_keycode[222] = '\'';//  '"
		typable_vk_keycode[226] = ','; // hack: <
	}


	/**
	 * Converts a char into a KeyCode value for KeyEvent
	 * @param c Char to convert
	 * @result c's KeyCode
	 */
	private int toKeyCode(char c) {
		//if (c == ' ') // special space
		//	return KeyEvent.VK_SPACE;
		
		if (c == '\'')
			return KeyEvent.VK_QUOTE;
		
		final String special = "~!@#$%^&*()_+|{}:\"<>?";
		final String normal = "`1234567890-=\\[];',./";
		int index = special.indexOf(c);
		return Character.toUpperCase(index == -1 ? c : normal.charAt(index));
	}

	/**
	 * Converts a vk code into a char
	 * @param code KeyCode to convert
	 * @result the char
	 */
	private char toChar(int vk, boolean shift) {
		int code = typable_vk_keycode[vk];
		final String special = "~!@#$%^&*()_+|{}:\"<>?";
		final String normal = "`1234567890-=\\[];',./";
		int index = normal.indexOf((char)code);
		if (index == -1) {
			return shift ? Character.toUpperCase((char)code) : Character.toLowerCase((char)code);
		} else {
			return shift ? special.charAt(index) : (char)code;
		}
	}

	/**
	 * Returns true if the vk code is typable
	 */
	private boolean isTypableCode(int vk) {
		return vk < 0xff && typable_vk_keycode[vk] != 0;
	}

	/**
	 * Holds a key. Should be used for any key that needs to be held, not
	 * sending text.
	 * @param code KeyCode for the key
	 */
	public synchronized void pressKey(int code) {
		if (canInteract()) {
			if (!isFocused()) getFocus();
			long startTime = System.currentTimeMillis();
			int[] dat = new int[] {code, (int) (startTime & 0xFFFFFFFF)};
			if (!isKeyHeld(dat)) {
				if (KeyEvent.VK_SHIFT == code) shiftDown = true;
				robot.keyPress(code);
				setKeyHeld(dat, true);
			}
		}
	}

	/**
	 * Release a key. Should be used for any key that needs to be held, not
	 * sending text. Will only release it if its already held.
	 * @param code KeyCode for the key
	 */
	public synchronized void releaseKey(int code) {
		if (canInteract()) {
			if (!isFocused()) getFocus();
			long startTime = System.currentTimeMillis();
			int[] dat = new int[] {code};
			if (isKeyHeld(dat)) {
				setKeyHeld(dat, false);
				robot.keyRelease(code);
				if (KeyEvent.VK_SHIFT == code) shiftDown = false;
			}
		}
	}

	/**
	 * Allows KeyEvents to be easily and safely passed through the blocking 
	 * mechanism. Also ensures the target component is the target of this nazi
	 */
	/*public synchronized void passKeyEvent(KeyEvent e) {
		if (canInteract()) {
			if (!focused) getFocus();  
			//System.out.println("Passing Event: " + e.getKeyChar());
			BlockingEventQueue.sendUnblocked(new KeyEvent(comp, e.getID(), e.getWhen(), e.getModifiers(), e.getKeyCode(), e.getKeyChar(), e.getKeyLocation()));
		}
	}*/


	public boolean isKeyDown(int code) {
		int[] dat = new int[] {code};
		return isKeyHeld(dat);
	}

	/**
	 * Sends a string to the client like a person would type it.
	 * In Scar you can use Chr(10) for enter, not Chr(13)
	 * Not to be used for arrow keys, but can be used with F keys or the like
	 * @param text String to send to the client
	 */
	public synchronized void sendKeys(String text, int keywait, int keymodwait) {
		if (canInteract()) {
			char[] chars = text.toCharArray();
			if (!isFocused()) getFocus();
			if (!isFocused()) return;
			for (char c : chars) {
				int code = toKeyCode(c);
				int keyLoc = Character.isDigit(c) ? Math.random() > 0.5D ? KeyEvent.KEY_LOCATION_NUMPAD : KeyEvent.KEY_LOCATION_STANDARD : KeyEvent.KEY_LOCATION_STANDARD;
				if (isShiftChar(c)) {
					int shiftLoc = Math.random() > 0.5D ? KeyEvent.KEY_LOCATION_RIGHT : KeyEvent.KEY_LOCATION_LEFT;
					robot.keyPress(KeyEvent.VK_SHIFT);
					try { Thread.sleep((int)((Math.random() * 0.1 + 1) * keymodwait)); } catch (Exception e) { e.printStackTrace(); }
					long time = System.currentTimeMillis();
					robot.keyPress(code);
					try { Thread.sleep((int)((Math.random() * 0.1 + 1) * keywait)); } catch (Exception e) { e.printStackTrace(); }
					robot.keyRelease(code);
					try { Thread.sleep((int)((Math.random() * 0.1 + 1) * keymodwait)); } catch (Exception e) { e.printStackTrace(); }
					robot.keyRelease(KeyEvent.VK_SHIFT);
				} else {
					long time = System.currentTimeMillis();
					robot.keyPress(code);
					try { Thread.sleep((int)((Math.random() * 0.1 + 1) * keywait)); } catch (Exception e) { e.printStackTrace(); }
					robot.keyRelease(code);
				}
			}
		}
	}
	
	/**
	 * added
	 * Presses and releases key every 100ms for the specified time.
	 * 
	 * @param code
	 * @param wait
	 */
	public synchronized void holdKey(int code, int wait) {
		holdKey(code, wait, null);
	}
	
	/**
	 * added
	 * Presses and releases key every 100ms for the specified time or until condition is met.
	 * 
	 * @param code
	 * @param wait
	 * @param breakCondition
	 */
	public synchronized void holdKey(int code, int wait, Condition breakCondition) {
		if (canInteract()) {
			if (!isFocused()) getFocus();
			
			Timer timer = new Timer(wait);
			while (timer.isRunning()) {
				if (breakCondition != null && breakCondition.evaluate())
					return;
				
				robot.keyPress(code);
				try { Thread.sleep(100); } catch (Exception e) { e.printStackTrace(); }
				robot.keyRelease(code);
			}
		}
	}
	
	/**
	 * added
	 * Presses and releases key
	 * 
	 * @param code
	 */
	public synchronized void sendKey(int code) {
		if (canInteract()) {
			if (!isFocused()) getFocus();
			
			robot.keyPress(code);
			try { Thread.sleep((int)((Math.random() * 0.1 + 1) * 90)); } catch (Exception e) { e.printStackTrace(); }
			robot.keyRelease(code);
		}
	}

	/**
	 * Sends an event that emulates a user alt+tabbing into RuneScape.
	 */
	private synchronized void getFocus() {
		if (BootType.get().equals(BootType.OFFICIAL_CLIENT))
			return;
		
		if (!isFocused()) {
			//BlockingEventQueue.sendUnblocked(new FocusEvent(comp, FocusEvent.FOCUS_GAINED, false, null));
			//focused = true;
			
			final AtomicBoolean executed = new AtomicBoolean(false);
			final AtomicBoolean compNotFocusControl = new AtomicBoolean(false);
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					if (Display.getDefault().getFocusControl() != Frame.window.comp) {
						System.out.println("SWT composite is not focused! Attempting to focus...");
						compNotFocusControl.set(true);
					}
					
					executed.set(true);
				}
			});
			
			Timer waitForExecTimer = new Timer(100);
			while (waitForExecTimer.isRunning()) {
				if (executed.get()) {
					//System.out.println("Exec time: " + waitForExecTimer.getElapsed());
					break;
				}
				
				Time.sleep(5);
			}
			
			if (compNotFocusControl.get()) {
				if (Client.getCanvas() == null) {
					System.out.println("ERROR: HardwareInput.getFocus():  Canvas is null!");
					return;
				}
				
				Antiban.randomClick(Mouse.LEFT);
				Time.sleepQuick();
			}
			
			/*if (!isFocused()) {
				System.out.println("RS Canvas is not focused! Attempting to focus...");
				Antiban.randomClick(Mouse.RIGHT);
				Time.sleepQuick();
			}*/
		}
	}

	/**
	 * Sends an event that emulates a user alt+tabbing out of RuneScape.
	 * tabbed = true will send a tab key, so avoid that.
	 */
	private synchronized void loseFocus(boolean tabbed) {
		/*if (focused) {
			if (tabbed) {
				BlockingEventQueue.sendUnblocked(new KeyEvent(comp, KeyEvent.KEY_PRESSED,System.currentTimeMillis(),InputEvent.ALT_DOWN_MASK,KeyEvent.VK_ALT,KeyEvent.CHAR_UNDEFINED,KeyEvent.KEY_LOCATION_LEFT));
				wait(100,200);
				BlockingEventQueue.sendUnblocked(new KeyEvent(comp, KeyEvent.KEY_PRESSED,System.currentTimeMillis(),InputEvent.ALT_DOWN_MASK,KeyEvent.VK_TAB,KeyEvent.CHAR_UNDEFINED,KeyEvent.KEY_LOCATION_STANDARD));
				wait(10,50);
			}
			BlockingEventQueue.sendUnblocked(new FocusEvent(comp, FocusEvent.FOCUS_LOST, false, null));
			BlockingEventQueue.sendUnblocked(new FocusEvent(comp, FocusEvent.FOCUS_LOST, false, null));
			focused = false;
			wait(100,200);
		}*/
	}

	private void setKeyHeld(int[] dat, boolean held) {
		synchronized (keysHeld) {
			if (held) {
				keysHeld.add(dat);
			} else {
				HashSet<int[]> remove = new HashSet<int[]>();
				for (int[] entry : keysHeld) {
					if (entry[0] == dat[0]) {
						remove.add(entry);
					}
				}
				keysHeld.removeAll(remove);
			}
		}
	}

	private boolean isKeyHeld(int[] dat) {
		synchronized (keysHeld) {
			for (int[] entry : keysHeld) {
				if (entry[0] == dat[0]) {
					return true;
				}
			}
			return false;
		}
	}

	 public synchronized void destroy() {
		robot = null;
	 }

	 public boolean isActive() {
		 //if (!BlockingEventQueue.isBlocking(comp)) destroy();
		 //return active;
		 
		if (BootType.get().equals(BootType.OFFICIAL_CLIENT))
			return true; // TODO
		else
			return Frame.window.activated;
	 }

	 public boolean isFocused() {
		 if (BootType.get().equals(BootType.OFFICIAL_CLIENT))
			 return true; // TODO
		 else
			 return Frame.window.isRsCanvasFocused();
	 }

	 public boolean isDragging() {
		 return leftDown || midDown || rightDown;
	 }

	 public boolean isDown(int button) {
		 switch (button) {
		 case 1: return leftDown;
		 case 2: return midDown;
		 case 3: return rightDown;
		 }
		 return false;
	 }

	 public boolean canInteract() {
		 return isActive();
	 }

	 public boolean canClick(int button) {
		 switch (button) {
		 case 1: return canInteract() && !leftDown;
		 case 2: return canInteract() && !midDown;
		 case 3: return canInteract() && !rightDown;
		 }
		 return false;
	 }

	 public boolean canHold(int button) {
		 switch (button) {
		 case 1: return canInteract() && !leftDown;
		 case 2: return canInteract() && !midDown;
		 case 3: return canInteract() && !rightDown;
		 }
		 return false;
	 }

	 public boolean canRelease(int button) {
		 switch (button) {
		 case 1: return canInteract() && leftDown;
		 case 2: return canInteract() && midDown;
		 case 3: return canInteract() && rightDown;
		 }
		 return false;
	 }

}