package org.rs3.api.input;

import java.awt.Point;

import org.rs3.api.wrappers.Client;
import org.rs3.callbacks.JaclibNotifyCallback;
import org.rs3.database.ServerInfo;
import org.rs3.database.ServerRow;
import org.rs3.eventqueue.EventNazi;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Mouse {
	
	public static int LEFT = 1;
	public static int RIGHT = 3;
	public static int MIDDLE = 2;

	private static EventNazi nazi = null;
	private static HardwareInput hardwareInput = null;
	
	public static volatile long lastClickMs = 0L;
	
	public static void setup(EventNazi nazi) {
		Mouse.nazi = nazi;
	}
	
	public static void setup(HardwareInput hardwareInput) {
		Mouse.hardwareInput = hardwareInput;
	}
	
	private static int jaclibFailureCount = 0;
	private static boolean checkJaclibNotified() {
		Time.sleep(5);
		if (System.currentTimeMillis() - JaclibNotifyCallback.lastNotified < 500)
			return true;
		
		Time.sleep(50);
		if (System.currentTimeMillis() - JaclibNotifyCallback.lastNotified < 500)
			return true;
		
		Time.sleep(250);
		if (System.currentTimeMillis() - JaclibNotifyCallback.lastNotified < 500)
			return true;
		
		Timer timer = new Timer(10000);
		while (timer.isRunning()) {
			if (System.currentTimeMillis() - JaclibNotifyCallback.lastNotified < timer.getElapsed() + 500)
				return true;
			
			Time.sleep(50);
		}
		
		jaclibFailureCount++;
		//if (jaclibFailureCount <= 5) {
		//if (jaclibFailureCount <= 20) {
		if (jaclibFailureCount <= 99) {
			System.out.println("Jaclib notify() method not called! Failure " + jaclibFailureCount);
			return true;
		}
		
		if (ScriptHandler.currentScript != null) {
			//System.out.println("Jaclib notify() method not called " + jaclibFailureCount + " times! Stopping script...");
			//ScriptHandler.currentScript.interruptStop();
			
			System.out.println("Jaclib notify() method not called " + jaclibFailureCount + " times! Stopping script and restarting bot...");
			ScriptHandler.currentScript.interruptStop();
			
			if (ServerInfo.cur == null)
				System.out.println("ServerInfo is null!");
			else {
				Thread thread = new Thread() {
					public void run() {
						System.out.println("Waiting 5 minutes before restart...");
						Time.sleep(300000);
						ServerRow row = ServerInfo.cur.getRow();
						row.setKillBot(false);
						row.setRestartBot(true);
						row.executeUpdate();
					}
				};
				thread.start();
			}
		}
		
		return false;
	}
	
	public static boolean outOfBounds(int x, int y) {
		if (Input.useHardWareInput()) {
			/*if (x < 0 || x > Client.getCanvasWidth() - 2)
				return true;
			
			if (y < 0 || y > Client.getCanvasHeight() - 6)
				return true;*/
			
			// TODO allow points inside hardware screen but not outside
			
			return false;
		} else {
			if (x < 0 || x > Client.getCanvasWidth() - 1)
				return true;
			
			if (y < 0 || y > Client.getCanvasHeight() - 1)
				return true;
		}
		
		return false;
	}
	
	private static int randomize(int coord, int rand) {
		return Random.nextInt(coord - rand, coord + rand + 1);
	}
	
	
	protected static Point getPos() {
		if (Input.useHardWareInput())
			return hardwareInput != null ? hardwareInput.getMousePos() : null;
		else
			return nazi != null ? nazi.getMousePos() : null;
	}
	
	public static Point getClientPos() {
		return getPos();
	}
	
	public static boolean isMouseButtonHeld(int button) {
		if (Input.useHardWareInput())
			return hardwareInput.isMouseButtonHeld(button);
		else
			return nazi.isMouseButtonHeld(button);
	}
	
	
	public static synchronized Point mouse(int x, int y, int button) {
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.mouse(x, y, button);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.mouse(x, y, button);
		
		return null;
	}
	
	public static synchronized Point mouse(int x, int y, int rx, int ry, int button) {
		x = randomize(x, rx);
		y = randomize(y, ry);
		
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.mouse(x, y, button);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.mouse(x, y, button);
		
		return null;
	}
	
	
	public static synchronized Point mouse(int x, int y) {
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.mouse(x, y);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.mouse(x, y);
		
		return null;
	}
	
	public static synchronized Point mouse(int x, int y, int rx, int ry) {
		x = randomize(x, rx);
		y = randomize(y, ry);
		
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.mouse(x, y);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.mouse(x, y);
		
		return null;
	}
	
	public static synchronized Point drag(int x, int y, int button) {
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.drag(x, y, button);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.drag(x, y, button);
		
		return null;
	}
	
	public static synchronized Point drag(int x, int y, int rx, int ry, int button) {
		x = randomize(x, rx);
		y = randomize(y, ry);
		
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.drag(x, y, button);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.drag(x, y, button);
		
		return null;
	}
	
	public static synchronized Point fastClick(int button) {
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.fastClick(button);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.fastClick(button);
		
		return null;
	}
	
	/**
	 * Negative numbers scroll up, positive scroll down
	 * 
	 * @param lines
	 * @return
	 */
	private static synchronized Point scrollMouse(int lines) {
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.scrollMouse(lines);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.scrollMouse(lines);
		
		return null;
	}
	
	/**
	 * Negative numbers scroll up, positive scroll down
	 * 
	 * @param x
	 * @param y
	 * @param lines
	 * @return
	 */
	@SuppressWarnings("unused")
	private static synchronized Point scrollMouse(int x, int y, int lines) {
		if (outOfBounds(x, y))
			return null;
		
		if (Input.useHardWareInput()) {
			Point p = hardwareInput.scrollMouse(x, y, lines);
			if (checkJaclibNotified())
				return p;
		} else
			return nazi.scrollMouse(x, y, lines);
		
		return null;
	}
	
	// TODO: add to mouse classes as humanScrollMouse
	public static synchronized Point scrollMouse(boolean up) {
		return scrollMouse(-1, -1, up);
	}
	
	// TODO: add to mouse classes as humanScrollMouse
	public static synchronized Point scrollMouse(int x, int y, boolean up) {
		if (x != -1 && y != -1) {
			if (outOfBounds(x, y))
				return null;
			else {
				if (mouse(x, y) == null)
					return null;
				else
					Time.sleepQuickest();
			}
		}
		
		int numLines = (Random.nextInt(0, 100) < 75) ? Random.nextInt(5, 8) : Random.nextInt(3, 9);
		
		while (numLines > 0) {
			if (up)
				scrollMouse(-1);
			else
				scrollMouse(1);
			
			Time.sleep(15, 30);
			numLines--;
		}
		
		return getClientPos();
	}
}
