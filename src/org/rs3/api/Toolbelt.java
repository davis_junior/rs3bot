package org.rs3.api;

import org.rs3.api.wrappers.Client;

public class Toolbelt {

	public enum Pickaxe {
		BRONZE(0), IRON(1), STEEL(2), MITHRIL(3), ADAMANT(4), RUNE(5);
		
		private int settingValue;
		
		private Pickaxe(int settingValue) {
			this.settingValue = settingValue;
		}
		
		public boolean equipped() {
			int setting = Client.getSettingData().getSettings().get(1102, 20, 0x7);
			return setting == settingValue;
		}
		
		public static Pickaxe getEquipped() {
			for (Pickaxe pick : values()) {
				if (pick.equipped())
					return pick;
			}
			
			return null;
		}
	}
	
	public enum Hatchet {
		BRONZE(0), IRON(1), STEEL(2), MITHRIL(3), ADAMANT(4), RUNE(5);
		
		private int settingValue;
		
		private Hatchet(int settingValue) {
			this.settingValue = settingValue;
		}
		
		public boolean equipped() {
			int setting = Client.getSettingData().getSettings().get(1102, 24, 0x7);
			return setting == settingValue;
		}
		
		public static Hatchet getEquipped() {
			for (Hatchet hatchet : values()) {
				if (hatchet.equipped())
					return hatchet;
			}
			
			return null;
		}
	}
	
}
