package org.rs3.api;

import java.awt.event.KeyEvent;

import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.objects.BasicInteractable;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Player;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Camera {
	
	public static double getX() {
		return Client.getCameraX();
	}

	public static double getY() {
		return Client.getCameraY();
	}

	public static int getZ() {
		return Client.getCameraZ();
	}

	public static int getPitch() {
		return Client.getCameraPitch();
	}
	
	public static int getYaw() {
		return Client.getCameraYaw();
	}
	
	public static int getZoomPercent() {
		return Client.getZoomPercent();
	}
	
	public synchronized static boolean setDefaultPitch() {
		return setPitch(Random.nextInt(50, 74));
	}
	
	public synchronized static boolean setPitch(final boolean up) {
		/*if (up) {
			return setPitch(100);
		}
		return setPitch(0);*/
		
		if (up)
			return setPitch(74);
		
		//return setPitch(10);
		
		// 10 if zoomed all the way out, 25 if zoomed all the way in
		return setPitch(10 + (int) Math.ceil(15*(getZoomPercent() / 100D))); // TODO: test
	}

	public synchronized static boolean setPitch(final int percent) {
		int curAlt = getPitch();
		int lastAlt = 0;
		if (curAlt == percent) {
			return true;
		}

		final boolean up = curAlt < percent;
		Keyboard.pressKey(up ? KeyEvent.VK_UP : KeyEvent.VK_DOWN);

		final Timer breakTimer = new Timer(3000); // TODO: only necessary because I don't have the max pitch fields or calculations for them
		final Timer timer = new Timer(100);
		while (breakTimer.isRunning() && timer.isRunning()) {
			if (lastAlt != curAlt) {
				timer.reset();
			}

			lastAlt = curAlt;
			Time.sleep(Random.nextInt(5, 10));
			curAlt = getPitch();

			if (up && curAlt >= percent) {
				break;
			} else if (!up && curAlt <= percent) {
				break;
			}
		}

		Keyboard.releaseKey(up ? (char) KeyEvent.VK_UP : (char) KeyEvent.VK_DOWN);
		return curAlt == percent;
	}

	public synchronized static void setAngle(final char direction) {
		switch (direction) {
		case 'n':
			setAngle(0);
			break;
		case 'w':
			setAngle(90);
			break;
		case 's':
			setAngle(180);
			break;
		case 'e':
			setAngle(270);
			break;
		default:
			setAngle(0);
			break;
		}
	}

	/*public synchronized static boolean setNorth() {
		return WidgetComposite.getCompass().click(true);
	}

	public synchronized static boolean setNorth(final int up) {
		return WidgetComposite.getCompass().click(true) && setPitch(up);
	}*/

	public synchronized static void setAngle(int degrees) {
		degrees %= 360;
		if (getAngleTo(degrees) > 5) {
			Keyboard.pressKey(KeyEvent.VK_LEFT);
			final Timer timer = new Timer(500);
			int ang, prev = -1;
			while ((ang = getAngleTo(degrees)) > 5 && Client.getClientState() == Client.LOADED_MAP && timer.isRunning()) {
				if (ang != prev) {
					timer.reset();
				}
				prev = ang;
				Time.sleep(10);
			}
			Keyboard.releaseKey((char) KeyEvent.VK_LEFT);
		} else if (getAngleTo(degrees) < -5) {
			Keyboard.pressKey((char) KeyEvent.VK_RIGHT);
			final Timer timer = new Timer(500);
			int ang, prev = -1;
			while ((ang = getAngleTo(degrees)) < -5 && Client.getClientState() == Client.LOADED_MAP && timer.isRunning()) {
				if (ang != prev) {
					timer.reset();
				}
				prev = ang;
				Time.sleep(10);
			}
			Keyboard.releaseKey((char) KeyEvent.VK_RIGHT);
		}
	}
	
	/**
	 * 
	 * @param zoomPercent 0 to 100
	 */
	public synchronized static boolean setZoomPercent(int zoomPercent) {
		if (zoomPercent < 0)
			zoomPercent = 0;
		if (zoomPercent > 100)
			zoomPercent = 100;
		
		if (Math.abs(Client.getZoomPercent() - zoomPercent) <= 5)
			return true;
		
		if (!MainWidget.get.isPointOnScreen(true, Mouse.getClientPos())) {
			if (!Antiban.randomMoveMouseOnScreen())
				return false;
			Time.sleepVeryQuick();
		}
		
		Timer timer = new Timer(10000);
		while (timer.isRunning()) {
			int curZoom = Client.getZoomPercent();
			if (Math.abs(curZoom - zoomPercent) <= 5)
				return true;
			
			if (curZoom < zoomPercent)
				Mouse.scrollMouse(true);
			else
				Mouse.scrollMouse(false);
			
			Time.sleepQuickest();
		}
		
		return false;
	}

	public static int getAngleTo(final int degrees) {
		int ca = getYaw();
		if (ca < degrees) {
			ca += 360;
		}
		int da = ca - degrees;
		if (da > 180) {
			da -= 360;
		}
		return da;
	}

	public synchronized static void turnTo(BasicInteractable i) {
		Tile t = i.getTile();
		turnTo(t.getX(), t.getY());
	}
	
	public synchronized static void turnTo(Tile t) {
		turnTo(t.getX(), t.getY());
	}
	
	public synchronized static void turnTo(int absX, int absY) {
		turnTo(absX, absY, 0);
	}
	
	public synchronized static void turnTo(BasicInteractable i, int dev) {
		Tile t = i.getTile();
		turnTo(t.getX(), t.getY(), dev);
	}

	public synchronized static void turnTo(int absX, int absY, int dev) {
		int angle = getMobileAngle(absX, absY);
		angle = Random.nextInt(angle - dev, angle + dev + 1);
		setAngle(angle);
	}
	
	public static int getMobileAngle(BasicInteractable i) {
		Tile t = i.getTile();
		return getMobileAngle(t.getX(), t.getY());
	}

	public static int getMobileAngle(int absX, int absY) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return -1;
		
		Tile me = player.getTile();
		
		int angle = ((int) Math.toDegrees(Math.atan2(absY - me.getY(), absX - me.getX()))) - 90;
		if (angle < 0)
			angle = 360 + angle;
		
		return angle % 360;
	}
}
