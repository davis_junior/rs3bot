package org.rs3.api;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.rs3.api.objects.AreaPath;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.Player;

public class GroundSearch {
	// result[0] = x, result[1] = y
	protected static List<int[]> getGroundIndices(int maxDistance) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		Location myLoc = player.getData().getCenterLocation();
		
		int relX = myLoc.getRelativeX();
		int relY = myLoc.getRelativeY();
		
		List<int[]> result = new ArrayList<>();
		
		for (int xi = 0; xi < 104; xi++) {
			for (int yi = 0; yi < 104; yi++) {
				
				if (maxDistance == -1) {
					result.add(new int[] {xi, yi});
				} else if (Calculations.distance(relX, relY, xi, yi) <= maxDistance) {
					result.add(new int[] {xi, yi});
				}
			}
		}
		
		if (result.size() > 0)
			return result;
		
		return null;
	}
	
	// inclusive
	protected static List<Point> removeOutOfBounds(List<Point> points, int tlX, int tlY, int brX, int brY) {
		
		if (points == null)
			return null;
		
		List<Point> newPoints = new ArrayList<>();
		for (Point p : points) {
			if (p.x >= tlX && p.x <= brX && p.y >= tlY && p.y <= brY)
				newPoints.add(p);
		}
		
		if (newPoints.size() == 0)
			return null;
		
		return newPoints;
	}
	
	// result[0] = x, result[1] = y
	protected static List<int[]> getSprialSquareIndicesAt(Tile absoluteTile, int squareIndex) {
		int x = absoluteTile.x;
		int y = absoluteTile.y;
		
		if (x > 103 || y > 103) {
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			x -= base.getX();
			y -= base.getY();
		}
		
		if (x >= 0 && y >= 0 && x < 104 && y < 104) {
			return getSprialSquareIndicesAtLocal(new Tile(x, y, absoluteTile.plane), squareIndex);
		}
		
		return null;
	}
	
	// result[0] = x, result[1] = y
	protected static List<int[]> getSprialSquareIndicesAtLocal(Tile relativeTile, int squareIndex) {
		
		if (squareIndex < 0)
			return null;
		
		int relX = relativeTile.x;
		int relY = relativeTile.y;
		
		List<int[]> result = new ArrayList<>();
		
		if (squareIndex == 0) {
			result.add(new int[] {relX, relY});
			return result;
		}
		
		Point myPoint = new Point(relX, relY);
		
		Point cornerTL = AreaPath.derive(myPoint, -squareIndex, -squareIndex);
		Point cornerTR = AreaPath.derive(myPoint, squareIndex, -squareIndex);
		Point cornerBR = AreaPath.derive(myPoint, squareIndex, squareIndex);
		Point cornerBL = AreaPath.derive(myPoint, -squareIndex, squareIndex);
		
		List<Point> lineTL_to_TR = AreaPath.getLine(cornerTL.x, cornerTL.y, cornerTR.x - 1, cornerTR.y);
		List<Point> lineTR_to_BR = AreaPath.getLine(cornerTR.x, cornerTR.y, cornerBR.x, cornerBR.y - 1);
		List<Point> lineBR_to_BL = AreaPath.getLine(cornerBR.x, cornerBR.y, cornerBL.x + 1, cornerBL.y);
		List<Point> lineBL_to_TL = AreaPath.getLine(cornerBL.x, cornerBL.y, cornerTL.x, cornerTL.y + 1);
		
		lineTL_to_TR = removeOutOfBounds(lineTL_to_TR, 0, 0, 103, 103);
		lineTR_to_BR = removeOutOfBounds(lineTR_to_BR, 0, 0, 103, 103);
		lineBR_to_BL = removeOutOfBounds(lineBR_to_BL, 0, 0, 103, 103);
		lineBL_to_TL = removeOutOfBounds(lineBL_to_TL, 0, 0, 103, 103);
		
		if (lineTL_to_TR != null) {
			for (Point p : lineTL_to_TR) {
				result.add(new int[] {p.x, p.y});
			}
		}
		
		if (lineTR_to_BR != null) {
			for (Point p : lineTR_to_BR) {
				result.add(new int[] {p.x, p.y});
			}
		}
		
		if (lineBR_to_BL != null) {
			for (Point p : lineBR_to_BL) {
				result.add(new int[] {p.x, p.y});
			}
		}
		
		if (lineBL_to_TL != null) {
			for (Point p : lineBL_to_TL) {
				result.add(new int[] {p.x, p.y});
			}
		}
		
		if (result.size() > 0)
			return result;
		
		return null;
	}
}
