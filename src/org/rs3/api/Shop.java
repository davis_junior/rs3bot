package org.rs3.api;

import org.rs3.api.interfaces.widgets.ShopWidget;
import org.rs3.api.objects.BasicInteractable;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.util.Random;
import org.rs3.util.Time;

public enum Shop {
	
	BURTHORPE_GNOME_SHOP(14921, 90958, "Gnome Shopkeeper", "Gnome Shop"),
	;
	
	int npcId;
	int groundObjectId;
	String npcName;
	String groundObjectName;
	
	private Shop(int npcId, int groundObjectId, String npcName, String groundObjectName) {
		this.npcId = npcId;
		this.groundObjectId = groundObjectId;
		this.npcName = npcName;
		this.groundObjectName = groundObjectName;
	}
	
	private Shop(int npcId, String npcName) {
		this(npcId, -1, npcName, null);
	}
	
	
	private static final TilePath burthorpeLStoGnomeShop = new TilePath( new Tile(2899, 3544, 0), new Tile(2899, 3543, 0), new Tile(2898, 3543, 0), new Tile(2898, 3542, 0), new Tile(2898, 3541, 0), new Tile(2898, 3540, 0), new Tile(2897, 3540, 0), new Tile(2897, 3539, 0), new Tile(2897, 3538, 0), new Tile(2897, 3537, 0), new Tile(2896, 3537, 0), new Tile(2896, 3536, 0), new Tile(2896, 3535, 0), new Tile(2895, 3535, 0), new Tile(2895, 3534, 0), new Tile(2895, 3533, 0), new Tile(2895, 3532, 0), new Tile(2895, 3531, 0), new Tile(2894, 3531, 0), new Tile(2894, 3530, 0), new Tile(2894, 3529, 0), new Tile(2893, 3529, 0), new Tile(2893, 3528, 0), new Tile(2893, 3527, 0), new Tile(2892, 3527, 0), new Tile(2892, 3526, 0), new Tile(2892, 3525, 0), new Tile(2891, 3525, 0), new Tile(2891, 3524, 0), new Tile(2890, 3524, 0), new Tile(2889, 3524, 0) );
	
	public boolean walkTo() {
		switch (this) {
		case BURTHORPE_GNOME_SHOP:
			if (burthorpeLStoGnomeShop.getDistanceToNearest() < 25 || Lodestone.BURTHORPE.teleport(true)) {
				// manually takes about 10 seconds
				return burthorpeLStoGnomeShop.loopWalk(20000, null, true);
			}
			
			break;
		default:
			break;
		}
		
		return false;
	}
	
	public boolean open() {
		return open(false);
	}
	
	public boolean open(boolean waitAfter) {
		if (ShopWidget.get.isOpen())
			return true;
		
		if (walkTo()) {
			Time.sleepQuick();
			
			BasicInteractable shop = null;
			if (this.npcId != -1 && this.groundObjectId != -1) {
				if (Random.nextBoolean())
					shop = Npcs.getNearest(null, this.npcId);
				else
					shop = GroundObjects.getNearest(Type.All, this.groundObjectId);
			} else if (this.npcId != -1) {
				shop = Npcs.getNearest(null, this.npcId);
			} else if (this.groundObjectId != -1) {
				shop = GroundObjects.getNearest(Type.All, this.groundObjectId);
			}
			
			if (shop != null) {
				boolean result = shop.loopInteract(null, "Trade");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
}
