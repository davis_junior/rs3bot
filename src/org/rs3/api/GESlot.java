package org.rs3.api;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Array2D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.WorldInfo;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public enum GESlot {
	
	FIRST(false, 0, GrandExchangeWidget.offer1BuyButtonData, GrandExchangeWidget.offer1SellButtonData, GrandExchangeWidget.offer1BorderData, ItemTable.GESLOT1),
	SECOND(false, 1, GrandExchangeWidget.offer2BuyButtonData, GrandExchangeWidget.offer2SellButtonData, GrandExchangeWidget.offer2BorderData, ItemTable.GESLOT2),
	THIRD(false, 2, GrandExchangeWidget.offer3BuyButtonData, GrandExchangeWidget.offer3SellButtonData, GrandExchangeWidget.offer3BorderData, ItemTable.GESLOT3),
	FOURTH(true, 3, GrandExchangeWidget.offer4BuyButtonData, GrandExchangeWidget.offer4SellButtonData, GrandExchangeWidget.offer4BorderData, ItemTable.GESLOT4),
	FIFTH(true, 4, GrandExchangeWidget.offer5BuyButtonData, GrandExchangeWidget.offer5SellButtonData, GrandExchangeWidget.offer5BorderData, ItemTable.GESLOT5),
	SIXTH(true, 5, GrandExchangeWidget.offer6BuyButtonData, GrandExchangeWidget.offer6SellButtonData, GrandExchangeWidget.offer6BorderData, ItemTable.GESLOT6),
	SEVEN(true, 6, GrandExchangeWidget.offer7BuyButtonData, GrandExchangeWidget.offer7SellButtonData, GrandExchangeWidget.offer7BorderData, ItemTable.GESLOT7),
	EIGHT(true, 7, GrandExchangeWidget.offer8BuyButtonData, GrandExchangeWidget.offer8SellButtonData, GrandExchangeWidget.offer8BorderData, ItemTable.GESLOT8),
	;
	
	public static GESlot getNextEmptySlot() {
		for (GESlot slot : values()) {
			if (slot.isValid()) {
				GEOfferStatus status = slot.getStatus();
				if (status != null) {
					if (status.isEmpty())
						return slot;
				}
			}
		}
		
		return null;
	}
	
	public static GESlot getRandomEmptySlot() {
		List<GESlot> emptySlots = new ArrayList<>();
		for (GESlot slot : values()) {
			if (slot.isValid()) {
				GEOfferStatus status = slot.getStatus();
				if (status != null) {
					if (status.isEmpty())
						emptySlots.add(slot);
				}
			}
		}
		
		if (emptySlots.size() > 0)
			return emptySlots.get(Random.nextInt(0, emptySlots.size()));
		
		return null;
	}
	
	public static GESlot findByItemId(int itemId) {
		for (GESlot slot : values()) {
			if (slot.isValid()) {
				if (slot.getItemId() == itemId)
					return slot;
			}
		}
		
		return null;
	}
	
	public static boolean anyContainItem(int itemId) {
		return findByItemId(itemId) != null;
	}
	
	public static enum GEOfferStatus {
		
		EMPTY(false, false, (byte) 0),
		
		BUYING_PROCESSING_ENTERING		(true, true, (byte) 1), // buying - processing - putting in offer
		BUYING_IDLE_ACTIVE				(true, false, (byte) 2), // buying - bought some or none
		BUYING_PROCESSING_TRADE			(true, true, (byte) 3), // buying - processing - a trade (even right before completion)
		BUYING_PROCESSING_CANCELLING	(true, true, (byte) 4), // buying - processing - cancellation
		BUYING_FINALIZED				(true, false, (byte) 5), // buying - cancelled or complete
		
		SELLING_PROCESSING_ENTERING		(false, true, (byte) 9), // selling - processing - putting in offer
		SELLING_IDLE_ACTIVE				(false, false, (byte) 10), // selling - sold some or none
		SELLING_PROCESSING_TRADE		(false, true, (byte) 11), // selling - processing - a trade (even right before completion)
		SELLING_PROCESSING_CANCELLING	(false, true, (byte) 12), // selling - processing - cancellation
		SELLING_FINALIZED				(false, false, (byte) 13), // selling - cancelled or complete
		;
		
		boolean buying;
		boolean processing;
		byte statusId;
		
		GEOfferStatus(boolean buying, boolean processing, byte statusId) {
			this.buying = buying;
			this.processing = processing;
			this.statusId = statusId;
		}
		
		public byte getStatusId() {
			return statusId;
		}
		
		public boolean isBuying() {
			return buying;
		}
		
		public boolean isSelling() {
			return !buying;
		}
		
		public boolean isProcessing() {
			return processing;
		}
		
		public boolean isEmpty() {
			return this.equals(EMPTY);
		}
		
		public boolean isIdleActive() {
			return this.equals(BUYING_IDLE_ACTIVE) || this.equals(SELLING_IDLE_ACTIVE);
		}
		
		public boolean isFinalized() {
			return this.equals(BUYING_FINALIZED) || this.equals(SELLING_FINALIZED);
		}
	}
	
	boolean members;
	int arrayId;
	ComponentData buyButtonData;
	ComponentData sellButtonData;
	ComponentData offerBorderData;
	ItemTable itemTable;
	
	private GESlot(boolean members, int arrayId, ComponentData buyButtonData, ComponentData sellButtonData, ComponentData offerBorderData, ItemTable itemTable) {
		this.members = members;
		this.arrayId = arrayId;
		this.buyButtonData = buyButtonData;
		this.sellButtonData = sellButtonData;
		this.offerBorderData = offerBorderData;
		this.itemTable = itemTable;
	}
	
	public org.rs3.api.wrappers.GEOffer getGEOffer() {
		Array2D<org.rs3.api.wrappers.GEOffer> offersArray = Client.getGEOffers();
		if (offersArray == null)
			return null;
		
		return offersArray.get(0, arrayId);
	}
	
	public ItemTable getItemTable() {
		return itemTable;
	}
	
	public GESlot getNext() {
		switch (this) {
		case FIRST:
			return SECOND;
		case SECOND:
			return THIRD;
		case THIRD:
			return FOURTH;
		case FOURTH:
			return FIFTH;
		case FIFTH:
			return SIXTH;
		case SIXTH:
			return FIRST;
		default:
			return null;
		}
	}
	
	public boolean isOpen() {
		return GrandExchangeWidget.get.getOpenOfferSetting() == arrayId;
	}
	
	public boolean isClosed() {
		return !isOpen();
	}
	
	/**
	 * @return true if is a members slot and currently on a members world, false otherwise; true if a f2p slot
	 */
	public boolean isValid() {
		if (members) {
			WorldInfo worldInfo = Client.getWorldInfo();
			if (worldInfo == null)
				return false;
			
			return Worlds.isMembers(worldInfo.getWorld());
		} else
			return true;
	}
	
	public GEOfferStatus getStatus() {
		org.rs3.api.wrappers.GEOffer offer = getGEOffer();
		if (offer == null)
			return null;
		
		byte status = offer.getStatus();
		for (GEOfferStatus statusEnum : GEOfferStatus.values()) {
			if (statusEnum.getStatusId() == status)
				return statusEnum;
		}
		
		return null;
	}
	
	// wrapper
	public int getItemId() {
		org.rs3.api.wrappers.GEOffer offer = getGEOffer();
		if (offer == null)
			return -1;
		
		return offer.getId();
	}
	
	// wrapper
	/**
	 * E.g., sold a armor set for 200k though the asking price was 170k.
	 * @return
	 */
	public int getAskingPriceEach() {
		org.rs3.api.wrappers.GEOffer offer = getGEOffer();
		if (offer == null)
			return -1;
		
		return offer.getPrice();
	}
	
	// wrapper
	/**
	 * E.g., sold 10 out of 200 items. Asking quantity would be 200.
	 * @return
	 */
	public int getAskingQuantity() {
		org.rs3.api.wrappers.GEOffer offer = getGEOffer();
		if (offer == null)
			return -1;
		
		return offer.getTotal();
	}
	
	// wrapper
	/**
	 * E.g., sold 20 out of 50 items. Quantity transferred would be 20.
	 * @return
	 */
	public int getQuantityTransferred() {
		org.rs3.api.wrappers.GEOffer offer = getGEOffer();
		if (offer == null)
			return -1;
		
		return offer.getTransfered();
	}
	
	// wrapper
	/**
	 * E.g., sold 200 items at 10 gp each. Total spent would be 2000 gp.
	 * @return
	 */
	public int getTotalSpent() {
		org.rs3.api.wrappers.GEOffer offer = getGEOffer();
		if (offer == null)
			return -1;
		
		return offer.getSpend();
	}
	
	/**
	 * E.g., sold 5 items for 1000 gp. Average price each would be 200 gp.
	 * @return
	 */
	public int getAveragePriceEach() {
		int quantity = getQuantityTransferred();
		if (quantity == 0)
			return 0;
		
		return Math.round((float) getTotalSpent() / quantity);
	}
	
	/**
	 * Checks prices at 3 to 5 5% increments above or below market price.
	 * 
	 * @param itemId
	 * @param itemName
	 * @param sellPrice
	 * @param buyPrice
	 * @param maxWaitEach
	 * @return
	 */
	public int[] checkPrices(int itemId, String itemName, int maxWaitEach) {
		return checkPrices(itemId, itemName, -1, -1, maxWaitEach);
	}

	/**
	 * Normally you should use the other method, it checks prices using market price increments.
	 * 
	 * @param itemId
	 * @param itemName
	 * @param sellPrice
	 * @param buyPrice
	 * @param maxWaitEach
	 * @return
	 */
	public int[] checkPrices(int itemId, String itemName, int sellPrice, int buyPrice, int maxWaitEach) {
		if (!GrandExchangeWidget.get.open(true))
			return null;
		
		if ((buyPrice != -1 && buy(itemId, itemName, 1, buyPrice))
				|| (buyPrice == -1 && buyByMarketPrice(itemId, itemName, 1, Random.nextInt(3, 6)))) {
			Time.sleepQuick();
			
			int highPrice = -1;
			Timer timer = new Timer(maxWaitEach);
			while (timer.isRunning()) {
				GEOfferStatus status = getStatus();
				if (status != null && status.equals(GEOfferStatus.BUYING_FINALIZED)) {
					highPrice = getAveragePriceEach();
					collect(true);
					Time.sleepMedium();
					Time.sleepQuick();
					break;
				}
				
				Time.sleep(50);
			}
			
			if (highPrice != -1) {
				if ((sellPrice != -1 && sell(itemId, 1, sellPrice))
						|| (sellPrice == -1 && sellByMarketPrice(itemId, 1, -1*Random.nextInt(3, 6)))) {
					Time.sleepQuick();
					
					int lowPrice = -1;
					timer.reset();
					while (timer.isRunning()) {
						GEOfferStatus status = getStatus();
						if (status != null && status.equals(GEOfferStatus.SELLING_FINALIZED)) {
							lowPrice = getAveragePriceEach();
							collect(true);
							Time.sleepMedium();
							Time.sleepQuick();
							break;
						}
						
						Time.sleep(50);
					}
					
					if (lowPrice != -1)
						return new int[] { lowPrice, highPrice };
				}
			}
		}
		
		return null;
	}
	
	public boolean buyByMarketPrice(int itemId, String itemName, int quantity, int priceFivePercentIncrements) {
		return offerByMarketPrice(false, itemId, itemName, quantity, priceFivePercentIncrements);
	}
	
	public boolean sellByMarketPrice(int itemId, int quantity, int priceFivePercentIncrements) {
		return offerByMarketPrice(true, itemId, null, quantity, priceFivePercentIncrements);
	}
	
	public boolean buy(int itemId, String itemName, int quantity, int priceEach) {
		return offer(false, itemId, itemName, quantity, priceEach);
	}
	
	public boolean sell(int itemId, int quantity, int priceEach) {
		return offer(true, itemId, null, quantity, priceEach);
	}
	
	private boolean offerByMarketPrice(boolean sell, int itemId, String itemName, int quantity, int priceFivePercentIncrements) {
		return offer(sell, itemId, itemName, quantity, -1, priceFivePercentIncrements);
	}
	
	private boolean offer(boolean sell, int itemId, String itemName, int quantity, int priceEach) {
		return offer(sell, itemId, itemName, quantity, priceEach, -1);
	}
	
	private boolean offer(boolean sell, int itemId, String itemName, int quantity, int priceEach, int priceFivePercentIncrements) {
		if (ItemTable.MONEYPOUCH.getCoinCount() < quantity*priceEach) {
			System.out.println("ERROR: offer(): you do not have enough money to complete the offer!");
			return false;
		}
		
		if (!isValid())
			return false;
		
		if (!GrandExchangeWidget.get.open(true))
			return false;
		
		Component.pauseAntiban();
		
		GEOfferStatus status = getStatus();
		if (status != null && !status.isEmpty()) {
			if (!abort()) {
				Component.resumeAntiban();
				return false;
			}
			
			Time.sleepMedium();
			Time.sleepQuick();
		}
		
		if (!GrandExchangeWidget.get.gotoOffersScreen(true)) {
			Component.resumeAntiban();
			return false;
		}
		
		GrandExchangeWidget.Screen screen = GrandExchangeWidget.Screen.getOpen();
		if (screen != null && screen.equals(GrandExchangeWidget.Screen.OFFERS)) {
			Component offerButton = sell ? sellButtonData.getComponent() : buyButtonData.getComponent();
			if (offerButton != null && offerButton.interact("Select")) {
				Time.sleepQuick();
				
				// wait for offer screen
				boolean success = false;
				Timer timer = new Timer(5000);
				while (timer.isRunning()) {
					screen = GrandExchangeWidget.Screen.getOpen();
					if (screen != null && screen.equals(sell ? GrandExchangeWidget.Screen.SELL : GrandExchangeWidget.Screen.BUY)) {
						success = true;
						break;
					}
					
					Time.sleep(50);
				}
				
				if (success) {
					if (selectItem(sell, itemId, itemName)) {
						Time.sleepMedium(); // give some time to load initial quantity and price
						
						if (GrandExchangeWidget.get.getOfferSelectedItemMarketPriceSetting() <= 0)
							Time.sleepMedium();
						
						// if market price not shown (item not selected)
						if (GrandExchangeWidget.get.getOfferSelectedItemMarketPriceSetting() <= 0)
							return false; 
						
						if (priceEach == -1) {
							if (ItemTable.MONEYPOUCH.getCoinCount() < quantity*adjustPrice(GrandExchangeWidget.get.getOfferSelectedItemMarketPriceSetting(), priceFivePercentIncrements)) {
								System.out.println("ERROR: offer(): you do not have enough money to complete the offer!");
								return false;
							}
						}
						
						if (selectQuantity(sell, itemId, quantity)) {
							Time.sleepQuick();
							
							if ((priceEach != -1 && selectPrice(sell, priceEach))
									|| (priceEach == -1 && selectPriceByMarket(sell, priceFivePercentIncrements))) {
								Time.sleepQuick();
								
								Component confirmButton = GrandExchangeWidget.confirmOfferButtonData.getComponent();
								if (confirmButton != null && confirmButton.isVisible()) {
									boolean clicked = confirmButton.interact("Select");
									Component.resumeAntiban();
									return clicked;
								}
							}
						}
					}
				}
			}
		}
		
		Component.resumeAntiban();
		return false;
	}
	
	private boolean selectItem(boolean sell, int itemId, String itemName) {
		boolean success = false;
		
		if (sell) {
			// select inventory item
			Component[] invItems = GrandExchangeWidget.get.getInvItems();
			if (invItems != null) {
				Component interItem = null;
				for (Component invItem : invItems) {
					if (invItem.getComponentId() == itemId) {
						interItem = invItem;
						break;
					}
				}
				
				// account for noted item in the case the non-noted item was not found
				if (interItem == null) {
					for (Component invItem : invItems) {
						if (invItem.getComponentId() == itemId + 1) {
							interItem = invItem;
							break;
						}
					}
				}
				
				if (interItem != null && interItem.interact("Offer")) {
					Time.sleepQuick();
					success = true;
				}
			}
		} else {
			// wait for item search widget
			success = false;
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				if (GrandExchangeWidget.get.isSearchOpen()) {
					success = true;
					break;
				}
				
				Time.sleep(50);
			}
			
			if (success) {
				// TODO clear text
				
				// keep typing item name until it is a visible result
				success = false;
				Component result = null;
				int curIndex = 0;
				Timer typeTimer = new Timer(0);
				Timer mainTimer = new Timer(20000);
				while (mainTimer.isRunning()) {
					result = (itemId != -1) ? GrandExchangeWidget.get.getSearchResult(itemId) : GrandExchangeWidget.get.getSearchResult(itemName);
					if (result != null && GrandExchangeWidget.get.isSearchResultVisible(result)) {
						success = true;
						break;
					}
					
					if (GrandExchangeWidget.get.isSearchOpen() && curIndex < itemName.length() && !typeTimer.isRunning()) {
						int endIndex = curIndex + Random.nextInt(3, 6);
						if (endIndex > itemName.length())
							endIndex = itemName.length();
						
						String toType = itemName.substring(curIndex, endIndex);
						curIndex = endIndex;
						Keyboard.sendKeys(toType);
						
						typeTimer = new Timer(Random.nextInt(1500, 2500));
					}
					
					Time.sleep(50);
				}
				
				if (success) {
					Time.sleepQuick();
					
					if (success = result.click(Mouse.LEFT))
						Time.sleepQuick();
				}
			}
		}
		
		if (success) {
			// wait for item to be selected
			success = false;
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				if (GrandExchangeWidget.get.getOfferSelectedItemIdSetting() == itemId) {
					success = true;
					break;
				}
				
				Time.sleep(50);
			}
		}
		
		return success;
	}
	
	private boolean selectQuantity(boolean sell, int itemId, int quantity) {
		int maxQuantity = 0;
		if (sell) {
			// verify there are enough items
			int stack = ItemTable.INVENTORY.countItems(true, itemId);
			int notedStack = ItemTable.INVENTORY.getStackSize(itemId + 1);
			
			if (stack != -1)
				maxQuantity = stack;
			if (notedStack != -1)
				maxQuantity += notedStack;
			
			System.out.println("Max quantity: " + maxQuantity);
			
			if (maxQuantity == 0 || maxQuantity < quantity)
				return false;	
			
			if (quantity == -1)
				quantity = maxQuantity;
		}
		
		// wait for correct quantity
		boolean success = false;
		Timer mainTimer = new Timer(sell ? 15000 : 30000);
		while (mainTimer.isRunning()) {
			int curQuantity = GrandExchangeWidget.get.getOfferQuantitySetting();
			if (curQuantity == quantity) {
				// double check
				Time.sleepMedium();
				curQuantity = GrandExchangeWidget.get.getOfferQuantitySetting();
				if (curQuantity == quantity) {
					success = true;
					break;
				}
			} else
				success = false;
			
			boolean enterCustomQuantity = false;
			
			if (sell) {
				//
				// sell offer
				//
				int diff = quantity - curQuantity;
				if (quantity == maxQuantity) {
					Component add = GrandExchangeWidget.quantityAdd1000OrAllButtonData.getComponent();
					if (add != null) {
						add.interact("Sell All");
						Time.sleepQuick();
					}
				} else if (quantity == 1) {
					Component add = GrandExchangeWidget.quantityAdd1ButtonData.getComponent();
					if (add != null) {
						add.interact("Sell 1");
						Time.sleepQuick();
					}
				} else if (diff > 0 && diff < 10) {
					Component add = GrandExchangeWidget.quantityAddButtonData.getComponent();
					if (add != null) {
						add.interact("Select");
						Time.sleepQuick();
					}
				} else if (quantity == 10) {
					Component add = GrandExchangeWidget.quantityAdd10ButtonData.getComponent();
					if (add != null) {
						add.interact("Sell 10");
						Time.sleepQuick();
					}
				} else if (quantity == 100) {
					Component add = GrandExchangeWidget.quantityAdd100ButtonData.getComponent();
					if (add != null) {
						add.interact("Sell 100");
						Time.sleepQuick();
					}
				} else if (diff < 0) {
					if (diff >= -10) {
						Component subtract = GrandExchangeWidget.quantitySubtractButtonData.getComponent();
						if (subtract != null) {
							subtract.interact("Select");
							Time.sleepQuick();
						}
					} else
						enterCustomQuantity = true;
				} else
					enterCustomQuantity = true;
			} else {
				//
				// buy offer
				//
				int diff = quantity - curQuantity;
				if (diff < 0) {
					if (diff >= -10) {
						Component subtract = GrandExchangeWidget.quantitySubtractButtonData.getComponent();
						if (subtract != null) {
							subtract.interact("Select");
							Time.sleepMedium();
						}
					} else
						enterCustomQuantity = true;
				} else if (quantity <= 5000 && quantity % 1000 == 0) {
					Component add = GrandExchangeWidget.quantityAdd1000OrAllButtonData.getComponent();
					if (add != null) {
						add.interact("Add 1000");
						Time.sleepMedium();
					}
				} else if (quantity <= 500 && quantity % 100 == 0) {
					Component add = GrandExchangeWidget.quantityAdd100ButtonData.getComponent();
					if (add != null) {
						add.interact("Add 100");
						Time.sleepMedium();
					}
				} else if (quantity <= 50 && quantity % 10 == 0) {
					Component add = GrandExchangeWidget.quantityAdd10ButtonData.getComponent();
					if (add != null) {
						add.interact("Add 10");
						Time.sleepMedium();
					}
				} else if (quantity == 1) {
					Component add = GrandExchangeWidget.quantityAdd1ButtonData.getComponent();
					if (add != null) {
						add.interact("Add 1");
						Time.sleepMedium();
					}
				} else if (quantity <= 5) {
					Component add = GrandExchangeWidget.quantityAddButtonData.getComponent();
					if (add != null) {
						add.interact("Select");
						Time.sleepMedium();
					}
				} else
					enterCustomQuantity = true;
				
				// uses buttons only
				/*
				int diff = quantity - curQuantity;
				if (diff < 0) {
					if (diff >= -10) {
						Component subtract = GrandExchangeWidget.quantitySubtractButtonData.getComponent();
						if (subtract != null) {
							subtract.interact("Select");
							Time.sleepQuick();
						}
					} else
						enterCustomQuantity = true;
				} else if (diff > 0 && diff < 10) {
					Component add = GrandExchangeWidget.quantityAddButtonData.getComponent();
					if (add != null) {
						add.interact("Select");
						Time.sleepQuick();
					}
				} else if (diff < 100) {
					Component add = GrandExchangeWidget.quantityAdd10ButtonData.getComponent();
					if (add != null) {
						add.interact("Add 10");
						Time.sleepQuick();
					}
				} else if (diff < 1000) {
					Component add = GrandExchangeWidget.quantityAdd100ButtonData.getComponent();
					if (add != null) {
						add.interact("Add 100");
						Time.sleepQuick();
					}
				} else if (diff <= 20000) {
					Component add = GrandExchangeWidget.quantityAdd1000OrAllButtonData.getComponent();
					if (add != null) {
						add.interact("Add 1000");
						Time.sleepQuick();
					}
				} else
					enterCustomQuantity = true;*/
			}
			
			if (enterCustomQuantity) {
				Component enterred = GrandExchangeWidget.quantityEnterredData.getComponent();
				if (enterred != null) {
					if (enterred.interact("Enter Number")) {
						Time.sleepQuick();
						
						Component inputComp = GrandExchangeWidget.quantityInputData.getComponent();
						
						// wait for input comp
						success = false;
						Timer timer = new Timer(5000);
						while (timer.isRunning()) {
							if (inputComp != null && inputComp.isVisible()) {
								success = true;
								break;
							}
							
							Time.sleep(50);
						}
						
						if (success) {
							Time.sleepQuick();
							
							Keyboard.sendKeys(Integer.toString(quantity));
							Time.sleepQuick();
							
							if (inputComp != null && inputComp.getText().equals(Integer.toString(quantity))) {
								Keyboard.pressKey(KeyEvent.VK_ENTER);
								Time.sleepVeryQuick();
								Keyboard.releaseKey(KeyEvent.VK_ENTER);
								Time.sleepVeryQuick();
								
								// wait for input comp to disappear
								success = false;
								timer = new Timer(5000);
								while (timer.isRunning()) {
									if (inputComp != null && !inputComp.isVisible()) {
										success = true;
										break;
									}
									
									Time.sleep(50);
								}
								
								Time.sleepQuick();
							}
						}
					}
				}
			}
			
			//Time.sleep(50);
			Time.sleepMedium(); // wait for quantity to update
		}
		
		return success;
	}
	
	private static int adjustPriceFivePercent(int price, boolean increase) {
		if (increase)
			return (int) Math.floor(price*(1 + 0.05));
		else
			return (int) Math.floor(price*(1 - 0.05));
	}
	
	/**
	 * Calculates based on how the GE adjusts prices based on increment
	 * 
	 * Note: The GE adjustment is done based on the current input price, not the market price. This accounts for that.
	 * 
	 * TODO: test
	 * 
	 * @param price
	 * @param priceFivePercentIncrements
	 * @return
	 */
	private static int adjustPrice(int price, int priceFivePercentIncrements) {
		int newPrice = price;
		boolean increase = priceFivePercentIncrements > 0;
		for (int i = 0; i < Math.abs(priceFivePercentIncrements); i++) {
			newPrice = adjustPriceFivePercent(newPrice, increase);
		}
		
		return newPrice;
	}
	
	private boolean selectPriceByMarket(boolean sell, int priceFivePercentIncrements) {
		// wait for correct price
		boolean success = false;
		Timer mainTimer = new Timer(10000);
		while (mainTimer.isRunning()) {
			int curPrice = GrandExchangeWidget.get.getOfferPriceSetting();
			if (curPrice == adjustPrice(GrandExchangeWidget.get.getOfferSelectedItemMarketPriceSetting(), priceFivePercentIncrements)) {
				// double check
				Time.sleepMedium();
				curPrice = GrandExchangeWidget.get.getOfferPriceSetting();
				if (curPrice == adjustPrice(GrandExchangeWidget.get.getOfferSelectedItemMarketPriceSetting(), priceFivePercentIncrements)) {
					success = true;
					break;
				}
			} else
				success = false;
			
			// set to guide price
			if (GrandExchangeWidget.get.getOfferPriceSetting() != GrandExchangeWidget.get.getOfferSelectedItemMarketPriceSetting()) {
				Component setGuidePriceButton = GrandExchangeWidget.priceSetToGuidePriceButtonData.getComponent();
				if (setGuidePriceButton != null && setGuidePriceButton.isVisible()) {
					setGuidePriceButton.interact("Select");
					Time.sleepMedium();
				}
				
				Time.sleep(50);
				continue;
			}
			
			// previous block ensures price is set to guide price, no need for a conditional
			
			Component incrementButton = (priceFivePercentIncrements > 0) ? GrandExchangeWidget.priceAdd5PercentButtonData.getComponent() : GrandExchangeWidget.priceSubtract5PercentButtonData.getComponent();
			if (incrementButton != null && incrementButton.isVisible()) {
				for (int i = 0; i < Math.abs(priceFivePercentIncrements); i++) {
					if (!incrementButton.loopInteract(null, "Select"))
						break;
					Time.sleepQuick();
				}
				Time.sleepMedium();
				Time.sleepMedium();
			}
			
			Time.sleep(50);
		}
		
		return success;
	}
	
	private boolean selectPrice(boolean sell, int priceEach) {
		// wait for correct price
		boolean success = false;
		Timer mainTimer = new Timer(15000);
		while (mainTimer.isRunning()) {
			int curPrice = GrandExchangeWidget.get.getOfferPriceSetting();
			if (curPrice == priceEach) {
				// double check
				Time.sleepMedium();
				curPrice = GrandExchangeWidget.get.getOfferPriceSetting();
				if (curPrice == priceEach) {
					success = true;
					break;
				}
			} else
				success = false;
			
			boolean enterCustomPrice = true;
			
			if (enterCustomPrice) {
				Component enterred = GrandExchangeWidget.priceEnterredData.getComponent();
				if (enterred != null) {
					if (enterred.interact("Enter Number")) {
						Time.sleepQuick();
						
						Component inputComp = GrandExchangeWidget.priceInputData.getComponent();
						
						// wait for input comp
						success = false;
						Timer timer = new Timer(5000);
						while (timer.isRunning()) {
							if (inputComp != null && inputComp.isVisible()) {
								success = true;
								break;
							}
							
							Time.sleep(50);
						}
						
						if (success) {
							Time.sleepQuick();
							
							String input;
							if (priceEach >= 1000000 && priceEach % 1000000 == 0)
								input = Integer.toString(priceEach / 1000000) + "m";
							else if (priceEach % 1000 == 0)
								input = Integer.toString(priceEach / 1000) + "k";
							else
								input = Integer.toString(priceEach);
							
							Keyboard.sendKeys(input);
							Time.sleepQuick();
							
							if (inputComp != null && inputComp.getText().equals(input)) {
								Keyboard.pressKey(KeyEvent.VK_ENTER);
								Time.sleepVeryQuick();
								Keyboard.releaseKey(KeyEvent.VK_ENTER);
								Time.sleepVeryQuick();
								
								// wait for input comp to disappear
								success = false;
								timer = new Timer(5000);
								while (timer.isRunning()) {
									if (inputComp != null && !inputComp.isVisible()) {
										success = true;
										break;
									}
									
									Time.sleep(50);
								}
								
								Time.sleepQuick();
							}
						}
					}
				}
			}
			
			//Time.sleep(50);
			Time.sleepMedium(); // wait for price to update
		}
		
		return success;
	}
	
	public boolean viewOffer() {
		if (!isValid())
			return false;
		
		if (isOpen())
			return true;
		
		if (!GrandExchangeWidget.get.gotoOffersScreen(true))
			return false;
		
		GEOfferStatus status = getStatus();
		if (status == null || status.isEmpty())
			return false;
		
		GrandExchangeWidget.Screen screen = GrandExchangeWidget.Screen.getOpen();
		if (screen != null && screen.equals(GrandExchangeWidget.Screen.OFFERS)) {
			Component offerBorder = offerBorderData.getComponent();
			if (offerBorder != null && offerBorder.interact("View Offer")) {
				Time.sleepQuick();
				
				// wait for buy or sell screen
				Timer timer = new Timer(5000);
				while (timer.isRunning()) {
					screen = GrandExchangeWidget.Screen.getOpen();
					if (screen != null && !screen.equals(GrandExchangeWidget.Screen.OFFERS)) {
						Time.sleepQuick();
						return true;
					}
					
					Time.sleep(50);
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Note: does not abort offer
	 * 
	 * @param noted
	 * @return
	 */
	public boolean collect(boolean noted) {
		if (!getStatus().isFinalized())
			return false;
		
		if (!viewOffer())
			return false;
		
		GrandExchangeWidget.Screen screen = GrandExchangeWidget.Screen.getOpen();
		if (screen != null && !screen.equals(GrandExchangeWidget.Screen.OFFERS)) { // buy or sell screen
			boolean item1success = true;
			Component item1 = GrandExchangeWidget.collectionItem1Data.getComponent();
			if (item1 != null && item1.isVisible() && item1.getComponentId() != -1) {
				boolean hasCollect = false;
				boolean hasCollectNotes = false;
				boolean hasCollectItems = false;
				
				String[] actions = item1.getActions();
				if (actions != null) {
					for (String action : actions) {
						if (action.equals("Collect"))
							hasCollect = true;
						else if (action.equals("Collect-notes"))
							hasCollectNotes = true;
						else if (action.equals("Collect-items"))
							hasCollectItems = true;
					}
				}
				
				if (hasCollectNotes && noted) {
					item1success = item1.interact("Collect-notes");
				} else if (hasCollectItems) {
					item1success = item1.interact("Collect-items");
				} else if (hasCollect) {
					item1success = item1.interact("Collect");
				}
				
				Time.sleepQuick();
			}
			
			boolean item2success = true;
			Component item2 = GrandExchangeWidget.collectionItem2Data.getComponent();
			if (item2 != null && item2.isVisible() && item2.getComponentId() != -1) {
				boolean hasCollect = false;
				boolean hasCollectNotes = false;
				boolean hasCollectItems = false;
				
				String[] actions = item2.getActions();
				if (actions != null) {
					for (String action : actions) {
						if (action.equals("Collect"))
							hasCollect = true;
						else if (action.equals("Collect-notes"))
							hasCollectNotes = true;
						else if (action.equals("Collect-items"))
							hasCollectItems = true;
					}
				}
				
				if (hasCollectNotes && noted) {
					item2success = item2.interact("Collect-notes");
				} else if (hasCollectItems) {
					item2success = item2.interact("Collect-items");
				} else if (hasCollect) {
					item2success = item2.interact("Collect");
				}
				
				Time.sleepQuick();
			}
			
			return item1success && item2success;
		}
		
		return false;
	}
	
	public boolean abort() {
		return abort(true, true);
	}
	
	public boolean abort(boolean collect, boolean noted) {
		if (!viewOffer())
			return false;
		
		GrandExchangeWidget.Screen screen = GrandExchangeWidget.Screen.getOpen();
		if (screen != null && !screen.equals(GrandExchangeWidget.Screen.OFFERS)) { // buy or sell screen
			Component abortButton = GrandExchangeWidget.abortOfferButtonData.getComponent();
			if (getStatus().isFinalized() || (abortButton != null && abortButton.isVisible())) {
				if (getStatus().isFinalized() || abortButton.interact("Abort Offer")) {
					if (collect) {
						Time.sleepQuick();
						// wait for offer to finalize
						Timer timer = new Timer(5000);
						while (timer.isRunning()) {
							if (getStatus().isFinalized())
								break;
							
							Time.sleep(50);
						}
						Time.sleepQuick();
						return collect(noted);
					} else
						return true;
				}
			}
		}
		
		return false;
	}
}
