package org.rs3.api;

import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.HashTable;
import org.rs3.api.wrappers.Node;

public class Nodes {
	
	public static Node lookup(HashTable nc, long id) {
		if (nc == null || id < 0)
			return null;
		
		Array1D<Node> buckets = nc.getBuckets();
		
		if (buckets == null)
			return null;
		
		final Node n = buckets.get((int) (id & buckets.getLength() - 1));
		
		if (n == null)
			return null;
		
		for (Node node = n.getPrevious(); (node != null) && (node.getObject() != n.getObject()); node = node.getPrevious()) {
			if (node.getId() == id)
				return node;
		}
		
		return null;
	}
}
