package org.rs3.api;

import org.rs3.api.interfaces.widgets.AbilitySubTab;
import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.MagicAbilitiesMainTabWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.WorldInfo;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public enum Spell {

	HOME_TELEPORT				("Home Teleport", 				false, 0, false, 155, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	AIR_STRIKE					("Air Strike", 					false, 1, true, 14, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	CONFUSE						("Confuse", 					false, 3, false, 15, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENCHANT_CROSSBOW_BOLT		("Enchant Crossbow Bolt", 		true, 4, false, 156, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	WATER_STRIKE				("Water Strike", 				false, 5, true, 16, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENCHANT_LEVEL_1_JEWELLERY	("Lvl-1 Enchant", 				false, 7, false, 17, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	EARTH_STRIKE				("Earth Strike", 				false, 9, true, 18, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	MOBILISING_ARMIES_TELEPORT	("Mobilising Armies Teleport", 	true, 10, false, 19, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	WEAKEN						("Weaken", 						false, 11, false, 20, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	FIRE_STRIKE					("Fire Strike", 				false, 13, true, 21, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	BONES_TO_BANANAS			("Bones To Bananas", 			false, 15, false, 22, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	AIR_BOLT					("Air Bolt", 					false, 17, true, 23, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	CURSE						("Curse", 						false, 19, false, 24, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	BIND						("Bind", 						false, 20, false, 25, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	LOW_LEVEL_ALCHEMY			("Low Level Alchemy", 			false, 21, false, 26, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	WATER_BOLT					("Water Bolt", 					false, 23, true, 27, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	VARROCK_TELEPORT			("Varrock Teleport", 			false, 25, false, 28, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	ENCHANT_LEVEL_2_JEWELLERY	("Lvl-2 Enchant", 				false, 27, false, 29, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	EARTH_BOLT					("Earth Bolt", 					false, 29, true, 30, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	LUMBRIDGE_TELEPORT			("Lumbridge Teleport", 			false, 31, false, 31, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	TELEKINETIC_GRAB			("Telekinetic Grab", 			false, 33, false, 32, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	FIRE_BOLT					("Fire Bolt", 					false, 35, true, 33, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	FALADOR_TELEPORT			("Falador Teleport", 			false, 37, false, 34, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	HOUSE_TELEPORT				("House Teleport", 				true, 40, false, 36, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	AIR_BLAST					("Air Blast", 					false, 41, true, 37, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	SUPERHEAT_ITEM				("Superheat Item", 				false, 43, false, 38, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	CAMELOT_TELEPORT			("Camelot Teleport", 			true, 45, false, 39, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	WATER_BLAST					("Water Blast", 				false, 47, true, 40, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENCHANT_LEVEL_3_JEWELLERY	("Lvl-3 Enchant", 				false, 49, false, 41, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	SNARE						("Snare", 						true, 50, false, 43, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	SLAYER_DART					("Slayer Dart", 				true, 50, true, 44, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ARDOUGNE_TELEPORT			("Ardougne Teleport", 			true, 51, false, 45, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	EARTH_BLAST					("Earth Blast", 				false, 53, true, 46, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	HIGH_LEVEL_ALCHEMY			("High Level Alchemy", 			false, 55, false, 47, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	CHARGE_WATER_ORB			("Charge Water Orb", 			true, 56, false, 48, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	ENCHANT_LEVEL_4_JEWELLERY	("Lvl-4 Enchant", 				false, 57, false, 49, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	WATCHTOWER_TELEPORT			("Watchtower Teleport", 		true, 58, false, 50, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	FIRE_BLAST					("Fire Blast", 					false, 59, true, 51, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	DIVINE_STORM				("Divine Storm", 				true, 60, true, 54, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	CHARGE_EARTH_ORB			("Charge Earth Orb", 			true, 60, false, 52, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	BONES_TO_PEACHES			("Bones To Peaches", 			true, 60, false, 53, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	TROLLHIEM_TELEPORT			("Trollheim Teleport", 			true, 61, false, 57, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	GOD_WARS_DUNGEON_TELEPORT	("God Wars Dungeon Teleport", 	true, 61, false, 170, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	AIR_WAVE					("Air Wave", 					true, 62, true, 58, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	CHARGE_FIRE_ORB				("Charge Fire Orb", 			true, 63, false, 59, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	APE_ATOLL_TELEPORT			("Ape Atoll Teleport", 			true, 64, false, 60, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	WATER_WAVE					("Water Wave", 					true, 65, true, 61, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	CHARGE_AIR_ORB				("Charge Air Orb", 				true, 66, false, 62, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	VULNERABILITY				("Vulnerability", 				true, 66, false, 63, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENCHANT_LEVEL_5_JEWELLERY	("Lvl-5 Enchant", 				true, 68, false, 64, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	EARTH_WAVE					("Earth Wave", 					true, 70, true, 65, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENFEEBLE					("Enfeeble", 					true, 73, false, 66, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	TELEOTHER_LUMBRIDGE			("Tele-other Lumbridge", 		true, 74, false, 67, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	FIRE_WAVE					("Fire Wave", 					true, 75, true, 68, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	STORM_OF_ARMADYL			("Storm of Armadyl", 			true, 77, true, 69, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENTANGLE					("Entangle", 					true, 79, false, 70, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	POLYPORE_STRIKE				("Polypore Strike", 			true, 80, true, 162, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	STAGGER						("Stagger", 					true, 80, false, 71, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	AIR_SURGE					("Air Surge", 					true, 81, true, 73, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	TELEOTHER_FALADAR			("Tele-other Falador", 			true, 82, false, 74, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	TELEPORT_BLOCK				("Teleport Block", 				false, 85, false, 75, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	WATER_SURGE					("Water Surge", 				true, 85, true, 78, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	ENCHANT_LEVEL_6_JEWELLERY	("Lvl-6 Enchant", 				true, 87, false, 77, AbilitySubTab.MAIN_MAGIC_SKILLING, AbilitySubTab.SUB_MAGIC_SKILLING),
	EARTH_SURGE					("Earth Surge", 				true, 90, true, 76, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	TELEOTHER_CAMELOT			("Tele-other Camelot", 			true, 90, false, 79, AbilitySubTab.MAIN_MAGIC_TELEPORT, AbilitySubTab.SUB_MAGIC_TELEPORT),
	FIRE_SURGE					("Fire Surge", 					true, 95, true, 80, AbilitySubTab.MAIN_MAGIC_COMBAT, AbilitySubTab.SUB_MAGIC_COMBAT),
	;
	
	String name;
	boolean members;
	int levelReq;
	boolean autocastable;
	int tabComponentIndex; // component index of spell interface, used to check if spell was selected
	AbilitySubTab mainSubTab;
	AbilitySubTab subInterfaceSubTab;
	
	private Ability ability = null; // use getter - cannot reference ability in constructor because ability requires spell, so it is set when retrieval is necessary
	
	private Spell(String name, boolean members, int levelReq, boolean autocastable, int tabComponentIndex, AbilitySubTab mainSubTab, AbilitySubTab subInterfaceSubTab) {
		this.name = name;
		this.members = members;
		this.levelReq = levelReq;
		this.autocastable = autocastable;
		this.tabComponentIndex = tabComponentIndex;
		this.mainSubTab = mainSubTab;
		this.subInterfaceSubTab = subInterfaceSubTab;
	}
	
	public Ability getAbility() {
		if (ability != null)
			return ability;
		
		switch (this) {
		case AIR_BLAST:
			ability = Ability.AIR_BLAST;
			break;
		case AIR_BOLT:
			ability = Ability.AIR_BOLT;
			break;
		case AIR_STRIKE:
			ability = Ability.AIR_STRIKE;
			break;
		case AIR_SURGE:
			ability = Ability.AIR_SURGE;
			break;
		case AIR_WAVE:
			ability = Ability.AIR_WAVE;
			break;
		case APE_ATOLL_TELEPORT:
			ability = Ability.APE_ATOLL_TELEPORT;
			break;
		case ARDOUGNE_TELEPORT:
			ability = Ability.ARDOUGNE_TELEPORT;
			break;
		case BIND:
			ability = Ability.BIND;
			break;
		case BONES_TO_BANANAS:
			ability = Ability.BONES_TO_BANANAS;
			break;
		case BONES_TO_PEACHES:
			ability = Ability.BONES_TO_PEACHES;
			break;
		case CAMELOT_TELEPORT:
			ability = Ability.CAMELOT_TELEPORT;
			break;
		case CHARGE_AIR_ORB:
			ability = Ability.CHARGE_AIR_ORB;
			break;
		case CHARGE_EARTH_ORB:
			ability = Ability.CHARGE_EARTH_ORB;
			break;
		case CHARGE_FIRE_ORB:
			ability = Ability.CHARGE_FIRE_ORB;
			break;
		case CHARGE_WATER_ORB:
			ability = Ability.CHARGE_WATER_ORB;
			break;
		case CONFUSE:
			ability = Ability.CONFUSE;
			break;
		case CURSE:
			ability = Ability.CURSE;
			break;
		case DIVINE_STORM:
			ability = Ability.DIVINE_STORM;
			break;
		case EARTH_BLAST:
			ability = Ability.EARTH_BLAST;
			break;
		case EARTH_BOLT:
			ability = Ability.EARTH_BOLT;
			break;
		case EARTH_STRIKE:
			ability = Ability.EARTH_STRIKE;
			break;
		case EARTH_SURGE:
			ability = Ability.EARTH_SURGE;
			break;
		case EARTH_WAVE:
			ability = Ability.EARTH_WAVE;
			break;
		case ENCHANT_CROSSBOW_BOLT:
			ability = Ability.ENCHANT_CROSSBOW_BOLT;
			break;
		case ENCHANT_LEVEL_1_JEWELLERY:
			ability = Ability.ENCHANT_LEVEL_1_JEWELLERY;
			break;
		case ENCHANT_LEVEL_2_JEWELLERY:
			ability = Ability.ENCHANT_LEVEL_2_JEWELLERY;
			break;
		case ENCHANT_LEVEL_3_JEWELLERY:
			ability = Ability.ENCHANT_LEVEL_3_JEWELLERY;
			break;
		case ENCHANT_LEVEL_4_JEWELLERY:
			ability = Ability.ENCHANT_LEVEL_4_JEWELLERY;
			break;
		case ENCHANT_LEVEL_5_JEWELLERY:
			ability = Ability.ENCHANT_LEVEL_5_JEWELLERY;
			break;
		case ENCHANT_LEVEL_6_JEWELLERY:
			ability = Ability.ENCHANT_LEVEL_6_JEWELLERY;
			break;
		case ENFEEBLE:
			ability = Ability.ENFEEBLE;
			break;
		case ENTANGLE:
			ability = Ability.ENTANGLE;
			break;
		case FALADOR_TELEPORT:
			ability = Ability.FALADOR_TELEPORT;
			break;
		case FIRE_BLAST:
			ability = Ability.FIRE_BLAST;
			break;
		case FIRE_BOLT:
			ability = Ability.FIRE_BOLT;
			break;
		case FIRE_STRIKE:
			ability = Ability.FIRE_STRIKE;
			break;
		case FIRE_SURGE:
			ability = Ability.FIRE_SURGE;
			break;
		case FIRE_WAVE:
			ability = Ability.FIRE_WAVE;
			break;
		case GOD_WARS_DUNGEON_TELEPORT:
			ability = Ability.GOD_WARS_DUNGEON_TELEPORT;
			break;
		case HIGH_LEVEL_ALCHEMY:
			ability = Ability.HIGH_LEVEL_ALCHEMY;
			break;
		case HOME_TELEPORT:
			ability = Ability.HOME_TELEPORT;
			break;
		case HOUSE_TELEPORT:
			ability = Ability.HOUSE_TELEPORT;
			break;
		case LOW_LEVEL_ALCHEMY:
			ability = Ability.LOW_LEVEL_ALCHEMY;
			break;
		case LUMBRIDGE_TELEPORT:
			ability = Ability.LUMBRIDGE_TELEPORT;
			break;
		case MOBILISING_ARMIES_TELEPORT:
			ability = Ability.MOBILISING_ARMIES_TELEPORT;
			break;
		case POLYPORE_STRIKE:
			ability = Ability.POLYPORE_STRIKE;
			break;
		case SLAYER_DART:
			ability = Ability.SLAYER_DART;
			break;
		case SNARE:
			ability = Ability.SNARE;
			break;
		case STAGGER:
			ability = Ability.STAGGER;
			break;
		case STORM_OF_ARMADYL:
			ability = Ability.STORM_OF_ARMADYL;
			break;
		case SUPERHEAT_ITEM:
			ability = Ability.SUPERHEAT_ITEM;
			break;
		case TELEKINETIC_GRAB:
			ability = Ability.TELEKINETIC_GRAB;
			break;
		case TELEOTHER_CAMELOT:
			ability = Ability.TELEOTHER_CAMELOT;
			break;
		case TELEOTHER_FALADAR:
			ability = Ability.TELEOTHER_FALADOR;
			break;
		case TELEOTHER_LUMBRIDGE:
			ability = Ability.TELEOTHER_LUMBRIDGE;
			break;
		case TELEPORT_BLOCK:
			ability = Ability.TELEPORT_BLOCK;
			break;
		case TROLLHIEM_TELEPORT:
			ability = Ability.TROLLHEIM_TELEPORT;
			break;
		case VARROCK_TELEPORT:
			ability = Ability.VARROCK_TELEPORT;
			break;
		case VULNERABILITY:
			ability = Ability.VULNERABILITY;
			break;
		case WATCHTOWER_TELEPORT:
			ability = Ability.WATCHTOWER_TELEPORT;
			break;
		case WATER_BLAST:
			ability = Ability.WATER_BLAST;
			break;
		case WATER_BOLT:
			ability = Ability.WATER_BOLT;
			break;
		case WATER_STRIKE:
			ability = Ability.WATER_STRIKE;
			break;
		case WATER_SURGE:
			ability = Ability.WATER_SURGE;
			break;
		case WATER_WAVE:
			ability = Ability.WATER_WAVE;
			break;
		case WEAKEN:
			ability = Ability.WEAKEN;
			break;
		default:
			System.out.println("ERROR: Spell.getAbility() - unimplemented ability!");
			break;
		}
		
		return ability;
	}
	
	public String getName() {
		return name;
	}
	
	public int getLevelReq() {
		return levelReq;
	}
	
	/**
	 * Checks if on a members world if spell is members and if skill level requirement is met
	 * @return
	 */
	public boolean isSelectable() {
		if (members) {
			WorldInfo worldInfo = Client.getWorldInfo();
			if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
				return false;
		}
		
		if (Skill.MAGIC.getCurrentLevel() < levelReq) {
			System.out.println("Can't cast spell " + this.getName() + " because magic level is not high enough!");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Is selected to be cast (selected from widget)
	 * @return
	 */
	public boolean isSelected() {
		if (Client.isUsableComponentSelected()
				// lastSelectedUsableComponentId is -1 if selected from action bar, the other conditions will verify if spell is selected
				&& (Client.getLastSelectedUsableComponentId() == -1 || Client.getLastSelectedUsableComponentId() == this.tabComponentIndex)
				&& Client.getLastSelectedUsableComponentAction().equals("Cast")
				&& TextUtils.removeHtml(Client.getLastSelectedUsableComponentOption()).equals(this.name)) {
			return true;
		}
		
		return false;
	}
	
	public boolean select() {
		return select(false);
	}
	
	public boolean select(boolean waitAfter) {
		if (isSelected())
			return true;
		
		if (!isSelectable())
			return false;
		
		if (Client.isUsableComponentSelected()) {
			cancelUsableComponentSelection();
			Time.sleepQuick();
		}
			
		if (!Client.isUsableComponentSelected()) {
			// if on action bar, cast from there
			for (ActionSlot actionSlot : ActionBar.getCurrentSlots()) {
				if (actionSlot.getAbility().equals(getAbility()))
					return actionSlot.interact(waitAfter, "Cast", name);
			}
			
			if (mainSubTab.open()) {
				Time.sleepQuick();
				Component[] spells = MagicAbilitiesMainTabWidget.get.getAbilities();
				if (spells != null && tabComponentIndex < spells.length) {
					Component button = spells[tabComponentIndex];
					if (button != null && !button._isHidden()) {
						boolean result = button.interact("Cast", name);
						if (waitAfter)
							Time.sleepQuick();
						return result;
					}
				}
			}
		}
		
		return false;
	}
	
	public boolean isAutocastSelected() {
		return this.tabComponentIndex == getSelectedAutocastSpellSetting();
	}
	
	public boolean autocast() {
		return autocast(false);
	}
	
	public boolean autocast(boolean waitAfter) {
		if (isAutocastSelected())
			return true;
		
		if (!isSelectable())
			return false;
		
		if (!autocastable)
			return false;
		
		// if on action bar, auto-cast from there
		for (ActionSlot actionSlot : ActionBar.getCurrentSlots()) {
			if (actionSlot.getAbility().equals(getAbility())) {
				if (MainWidget.get.maximiseActionBar(true) && ActionBarWidget.get.isOpen()) {
					Component mainSlot = actionSlot.getMainSlot();
					if (mainSlot != null && mainSlot.isVisible()) {
						boolean result = mainSlot.interact("Auto-cast", name);
						if (waitAfter)
							Time.sleepQuick();
						return result;
					}
				}
				
				return false;
			}
		}
		
		if (mainSubTab.open()) {
			Time.sleepQuick();
			Component[] spells = MagicAbilitiesMainTabWidget.get.getAbilities();
			if (spells != null && tabComponentIndex < spells.length) {
				Component button = spells[tabComponentIndex];
				if (button != null && !button._isHidden()) {
					boolean result = button.interact("Auto-cast", name);
					if (waitAfter)
						Time.sleepQuick();
					return result;
				}
			}
		}
		
		return false;
	}
	
	public static int getSelectedAutocastSpellSetting() {
		int componentId = Client.getSettingData().getSettings().get(0, 1, 0xFF);
		return componentId;
	}
	
	public static Spell getSelectedAutocastSpell() {
		for (Spell spell : values()) {
			if (spell.autocastable && spell.isAutocastSelected())
				return spell;
		}
		
		return null;
	}
	
	public static boolean cancelUsableComponentSelection() {
		if (!Client.isUsableComponentSelected())
			return true;
		else
			return Menu.select("Cancel");
	}
}
