package org.rs3.api;

import java.util.ArrayList;
import java.util.List;

import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.HashTable;
import org.rs3.api.wrappers.Node;

public enum ItemTable {

	BANK(95, true),
	BOB(530, false), // TODO: check if should enable snapshots (if accessible while tab is closed)
	EQUIPMENT(94, false), // or 670
	GESLOT1(523, true),
	GESLOT2(524, true),
	GESLOT3(525, true),
	GESLOT4(526, true),
	GESLOT5(527, true),
	GESLOT6(528, true),
	GESLOT7(529, true),
	GESLOT8(530, true),
	INVENTORY(93, false),
	MONEYPOUCH(623, false),
	LOOT(773, false),
	ACQUIRED_ITEMS(787, false), // items acquired (drops, mined, taken (wine), etc.)
	SHOP_BUY_ITEMS(655, false),
	SHOP_BUY_FREE_ITEMS(657, false),
	;
	
	long id;
	boolean enableSnapshots;
	ItemTableSnapShot snapshot;
	
	private ItemTable(long id, boolean enableSnapshots) {
		this.id = id;
		this.enableSnapshots = enableSnapshots;
		if (enableSnapshots)
			this.snapshot = new ItemTableSnapShot(id);
		else
			this.snapshot = null;
	}
	
	public long getId() {
		return this.id;
	}
	
	public boolean snapshotsEnabled() {
		return enableSnapshots;
	}
	
	public ItemTableSnapShot getSnapshot() {
		return this.snapshot;
	}
	
	public org.rs3.api.wrappers.ItemTable getTable() {
		return getTable(this.id);
	}
	
	public int countAllItems(boolean countStackSizes) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.countAllItems(countStackSizes);
		
		//return -1;
		return 0;
	}
	
	public int countAllItems(boolean countStackSizes, int... exceptionIds) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.countAllItems(countStackSizes, exceptionIds);
		
		//return -1;
		return 0;
	}
	
	public int countItems(boolean countStackSizes, int itemId) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.countItems(countStackSizes, itemId);
		
		//return -1;
		return 0;
	}
	
	public boolean containsAny(int... ids) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.containsAny(ids);
		
		return false;
	}
	
	public boolean containsAll(int... ids) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.containsAll(ids);
		
		return false;
	}
	
	public boolean containsUselessItems() {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.containsUselessItems();
		
		return false;
	}
	
	public int getStackSize(int itemId) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.getStackSize(itemId);
		
		//return -1;
		return 0;
	}
	
	public int getItemIdAt(int index) {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			return table.getItemIdAt(index);
		
		return -1;
	}
	
	public void printItems() {
		org.rs3.api.wrappers.ItemTable table = getTable();
		if (table != null)
			table.printItems();
	}
	
	public int getCoinCount() {
		return getStackSize(995); // coins
	}
	
	public static void printCurrentTables() {
		HashTable nc = Client.getItemTables();
		StringBuilder sb = new StringBuilder("Item Tables = [ ");
		
		if (nc != null) {
			Node[] nodes = nc.getAll();
			if (nodes != null) {
				for (Node node : nodes) {
					sb.append(Long.toString(node.getId()) + ", ");
				}
			}
		}
		int lastIndex = sb.lastIndexOf(", ");
		if (lastIndex != -1)
			sb = new StringBuilder(sb.substring(0, lastIndex));
		sb.append(" ]");
		System.out.println(sb.toString());
	}
	
	public static org.rs3.api.wrappers.ItemTable getTable(long id) {
		HashTable nc = Client.getItemTables();
		
		if (nc != null) {
			Node node = Nodes.lookup(nc, id);
			if (node != null)
				return node.forceCast(org.rs3.api.wrappers.ItemTable.class);
		}
		
		return null;
	}
}
