package org.rs3.api;

import org.rs3.api.wrappers.Client;

public class Stats {
	
	public static final LifePoints HEALTH = new LifePoints();
	public static final Prayer PRAYER = new Prayer();
	
	public static final Antifire ANTIFIRE = new Antifire();
	
	public static class LifePoints {
		
		private LifePoints () {
			super();
		}
		
		public int getCurrent() {
			int currentHealth = Client.getSettingData().getSettings().get(659, 0xFFFF) / 2;
			return currentHealth;
		}
		
		public int getMax() {
			int maxHealth = Client.getSettingData().getSettings().get(659, 16, 0xFFFF);
			return maxHealth;
		}
		
		public int getPercentage() {
			float percentage = (float) getCurrent() / getMax();
			return Math.round(percentage * 100);
		}
	}
	
	public static class Prayer {
		
		private Prayer () {
			super();
		}
		
		public int getCurrent() {
			int curPrayer = Client.getSettingData().getSettings().get(3274, 0x3FFF);
			
			int prayerPoints;
			if (curPrayer > 0 && curPrayer < 20)
				prayerPoints = 1;
			else
				prayerPoints = curPrayer/10;
			
			return prayerPoints;
		}
		
		public int getMax() {
			int maxPrayerPoints = Client.getSettingData().getSettings().get(3274, 16, 0x7F);
			maxPrayerPoints *= 10;
			
			return maxPrayerPoints;
		}
		
		public int getPercentage() {
			float percentage = (float) getCurrent() / getMax();
			return Math.round(percentage * 100);
		}
	}
	
	/**
	 * 
	 * A normal anti-fire dose will start the setting at 24, representing 6 minutes. It will decrease by 1 every 15 seconds.
	 * E.g.:
	 * 		- 24 -> 6 min. left
	 * 		- 20 -> 5 min. left
	 * 		- 4 -> 60 sec. left
	 * 		- 1 -> 15 sec. left
	 */
	public static class Antifire {
		
		private Antifire () {
			super();
		}
		
		private int getCurrent() {
			int current = Client.getSettingData().getSettings().get(1299, 0x3F);
			return current;
		}
		
		public boolean isActive() {
			return getCurrent() != 0;
		}
		
		public int getSecondsLeft() {
			return getCurrent() * 15;
		}
		
		public float getMinutesLeft() {
			float minutes = (float) getSecondsLeft() / 60;
			return minutes;
		}
	}
}
