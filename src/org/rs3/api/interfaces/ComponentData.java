package org.rs3.api.interfaces;

import org.rs3.api.Interfaces;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;

public class ComponentData {
	
	String interfacedName;
	int widgetId;
	int componentId;
	int[] nextComponentIndices;
	
	int fullId; // calculated with widgetId and componentId
	
	/**
	 * 
	 * @param interfacedName Interfaced name
	 * @param widgetId Widget id
	 * @param id Component id
	 * @param nextComponentIndices next dimension Component ids
	 */
	public ComponentData(String interfacedName, int widgetId, int componentId, int... nextComponentIndices) {
		this.interfacedName = interfacedName;
		this.widgetId = widgetId;
		this.componentId = componentId;
		this.nextComponentIndices = nextComponentIndices;
		
		this.fullId = Interfaces.getFullId(widgetId, componentId);
	}
	
	/**
	 * 
	 * @param interfacedName Interfaced name
	 * @param widgetId Widget id
	 * @param id Component id
	 * @param index 2nd dimension Component id
	 */
	public ComponentData(String interfacedName, int widgetId, int componentId, int index) {
		this(interfacedName, widgetId, componentId, new int[] {index});
	}
	
	/**
	 * 
	 * @param interfacedName Interfaced name
	 * @param widgetId Widget id
	 * @param id Component id
	 */
	public ComponentData(String interfacedName, int widgetId, int componentId) {
		this(interfacedName, widgetId, componentId, null);
	}
	
	public Component getComponent() {
		Component parent = Interfaces.getChild(fullId);
		
		if (nextComponentIndices == null)
			return parent;
		else {
			if (parent != null) {
				Component comp = parent;
				for (int index : nextComponentIndices) {
					Array1D<Component> comps = comp.getComponents();
					if (comps != null) {
						if (index >= comps.getLength())
							return null;
						
						comp = comps.get(index);
						if (comp == null)
							return null;
					}
				}
				
				return comp;
			}
		}
		
		return null;
	}
	
	public int getFullId() {
		return fullId;
	}
}
