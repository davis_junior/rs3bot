package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.Openable;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;

public class MeleeAbilitiesSubInterfaceTabWidget extends WidgetData implements Openable {
	
	public static final MeleeAbilitiesSubInterfaceTabWidget get = new MeleeAbilitiesSubInterfaceTabWidget(1450);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1450, 3);
	public static final ComponentData tabAttackButtonData = new ComponentData("Attack Tab Button", 1450, 0, 7);
	public static final ComponentData tabStrengthButtonData = new ComponentData("Strength Tab Button", 1450, 0, 8);
	
	
	public MeleeAbilitiesSubInterfaceTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1450);
		
		if (widget != null && widget.validate(1450)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return SubInterfaceTab.POWERS_MELEE.open(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
