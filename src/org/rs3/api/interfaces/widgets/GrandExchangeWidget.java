package org.rs3.api.interfaces.widgets;

import org.rs3.api.Calculations;
import org.rs3.api.GESlot;
import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundObjects;
import org.rs3.api.Interfaces;
import org.rs3.api.Npcs;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Widget;
import org.rs3.debug.Paint;
import org.rs3.util.Random;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class GrandExchangeWidget extends OpenableClosableWidget {
	
	public static final GrandExchangeWidget get = new GrandExchangeWidget(105);
	
	//public static final ComponentData closeButtonData = new ComponentData("Close Button", 105, 89);
	
	//
	// Offers screen
	//
	public static final ComponentData invItemsData = new ComponentData("Inventory Items", 107, 7);
	public static final ComponentData mainTextData = new ComponentData("Main Text", 105, 13); // Text: Choose an item to exchange
	public static final ComponentData offer1BorderData = new ComponentData("Offer 1 Border", 105, 17);
	public static final ComponentData offer2BorderData = new ComponentData("Offer 2 Border", 105, 18);
	public static final ComponentData offer3BorderData = new ComponentData("Offer 3 Border", 105, 19);
	public static final ComponentData offer4BorderData = new ComponentData("Offer 4 Border", 105, 20);
	public static final ComponentData offer5BorderData = new ComponentData("Offer 5 Border", 105, 21);
	public static final ComponentData offer6BorderData = new ComponentData("Offer 6 Border", 105, 22);
	public static final ComponentData offer7BorderData = new ComponentData("Offer 7 Border", 105, 23);
	public static final ComponentData offer8BorderData = new ComponentData("Offer 8 Border", 105, 24);
	public static final ComponentData offer1BuyButtonData = new ComponentData("Offer 1 Buy Button", 105, 175);
	public static final ComponentData offer1SellButtonData = new ComponentData("Offer 1 Sell Button", 105, 180);
	public static final ComponentData offer2BuyButtonData = new ComponentData("Offer 2 Buy Button", 105, 186);
	public static final ComponentData offer2SellButtonData = new ComponentData("Offer 2 Sell Button", 105, 192);
	public static final ComponentData offer3BuyButtonData = new ComponentData("Offer 3 Buy Button", 105, 198);
	public static final ComponentData offer3SellButtonData = new ComponentData("Offer 3 Sell Button", 105, 204);
	public static final ComponentData offer4BuyButtonData = new ComponentData("Offer 4 Buy Button", 105, 208);
	public static final ComponentData offer4SellButtonData = new ComponentData("Offer 4 Sell Button", 105, 214);
	public static final ComponentData offer5BuyButtonData = new ComponentData("Offer 5 Buy Button", 105, 224);
	public static final ComponentData offer5SellButtonData = new ComponentData("Offer 5 Sell Button", 105, 230);
	public static final ComponentData offer6BuyButtonData = new ComponentData("Offer 6 Buy Button", 105, 240);
	public static final ComponentData offer6SellButtonData = new ComponentData("Offer 6 Sell Button", 105, 246);
	public static final ComponentData offer7BuyButtonData = new ComponentData("Offer 7 Buy Button", 105, 240);
	public static final ComponentData offer7SellButtonData = new ComponentData("Offer 7 Sell Button", 105, 246);
	public static final ComponentData offer8BuyButtonData = new ComponentData("Offer 8 Buy Button", 105, 240);
	public static final ComponentData offer8SellButtonData = new ComponentData("Offer 8 Sell Button", 105, 246);
	public static final ComponentData collectAllToInventoryButtonData = new ComponentData("Collect all to Inventory Button", 651, 6);
	public static final ComponentData collectAllToBankButtonData = new ComponentData("Collect all to bank Button", 651, 14);
	
	//
	// Offer (Buy/Sell) screen
	//
	// close button is also 87
	public static final ComponentData offerTextData = new ComponentData("Offer Text", 105, 80); // "Buy Offer" or "Sell Offer"
	public static final ComponentData selectedItemData = new ComponentData("Selected Item", 105, 85);
	public static final ComponentData backButtonData = new ComponentData("Back Button", 105, 15);
	public static final ComponentData abortOfferButtonData = new ComponentData("Abort Offer Button", 105, 61);
	public static final ComponentData collectionItem1Data = new ComponentData("Collection Item 1", 105, 63);
	public static final ComponentData collectionItem2Data = new ComponentData("Collection Item 2", 105, 65);
	public static final ComponentData quantitySubtractButtonData = new ComponentData("Quantity - Button", 105, 121);
	public static final ComponentData quantityAddButtonData = new ComponentData("Quantity + Button", 105, 124);
	public static final ComponentData quantityAdd1ButtonData = new ComponentData("Quantity +1 or 1 Button", 105, 311);
	public static final ComponentData quantityAdd10ButtonData = new ComponentData("Quantity +10 or 10 Button", 105, 128);
	public static final ComponentData quantityAdd100ButtonData = new ComponentData("Quantity +100 or 100 Button", 105, 134);
	public static final ComponentData quantityAdd1000OrAllButtonData = new ComponentData("Quantity +1000 or ALL Button", 105, 140);
	public static final ComponentData quantityEnterredData = new ComponentData("Quantity Enterred", 105, 292);
	public static final ComponentData quantityInputData = new ComponentData("Quantity Input", 105, 290);
	public static final ComponentData priceSubtractButtonData = new ComponentData("Price - Button", 105, 145);
	public static final ComponentData priceAddButtonData = new ComponentData("Price + Button", 105, 148);
	public static final ComponentData priceSubtract5PercentButtonData = new ComponentData("Price -5% Button", 105, 152);
	public static final ComponentData priceSetToGuidePriceButtonData = new ComponentData("Price Set to Guide Price Button", 105, 158);
	public static final ComponentData priceEnterredData = new ComponentData("Price Enterred", 105, 298);
	public static final ComponentData priceInputData = new ComponentData("Price Input", 105, 296);
	public static final ComponentData priceAdd5PercentButtonData = new ComponentData("Price +5% Button", 105, 163);
	public static final ComponentData confirmOfferButtonData = new ComponentData("Confirm Offer Button", 105, 169);
	
	
	//
	// Buy Offer Search
	//
	public static final ComponentData searchResultsData = new ComponentData("Search Results", 105, 53);
	public static final ComponentData searchTextData = new ComponentData("Search Text", 105, 302);
	/**
	 * 0 - entire scroll bar border without buttons (does not consider current scroll bar position/size)
	 * 1 - currently visible scroll bar without buttons (yellow bar)
	 * 2 - top position
	 * 3 - bottom position
	 * 4 - top button
	 * 5 - bottom button
	 */
	public static final ComponentData searchScrollBarData = new ComponentData("Search Scroll Bar", 105, 54);
	
	// ? Text: 
	
	// ? Action: null
	// ? Text: 
	
	public enum Screen {
		OFFERS, BUY, SELL;
		
		boolean isOpen() {
			return this.equals(getOpen());
		}
		
		boolean isClosed() {
			return !isOpen();
		}
		
		public static Screen getOpen() {
			if (GrandExchangeWidget.get.isOpen()) {
				switch (GrandExchangeWidget.get.getCreateOfferTypeSetting()) {
				case -1:
					Component mainText = mainTextData.getComponent();
					if (mainText == null || !mainText.isVisible())
						return OFFERS;
					
					Component offerText = offerTextData.getComponent();
					if (offerText != null && offerText.isVisible()) {
						String text = offerText.getText();
						if (text != null) {
							if (text.toLowerCase().contains("buy")) // "Buy Offer"
								return BUY;
							else if (text.toLowerCase().contains("sell")) // "Sell Offer"
								return SELL;
						}
					}
				case 0:
					return BUY;
				case 1:
					return SELL;
				}
			}
			
			return null;
		}
	}
	
	public GrandExchangeWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(105);
		if (widget != null && widget.validate(105))
			return true;
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}


	@Override
	public boolean open(boolean waitAfter) {
		if (!GrandExchangeWidget.get.isTutorialCompletedSetting()) {
			System.out.println("Grand Exchange Tutorial not completed! Attempting to complete...");
			
			if (!GrandExchange.completeTutorial())
				return false;
			else
				System.out.println("Successfully completed GE tutorial.");
			
			Time.sleepMedium();
		}
		
		if (isOpen())
			return true;
		
		Interactable geClerk = getNearest();
		
		if (geClerk != null) {
			if (Calculations.distanceTo(geClerk.getTile()) > 3)
				if (!geClerk.getTile().loopWalkTo(10000)) {
					if (waitAfter)
						Time.sleepQuick();
					
					return false;
				}
			
			Paint.updatePaintObject(geClerk);
			
			if (geClerk.loopInteract(getIsOpenCondition(), "Exchange") || isOpen()) {
				Timer timer = new Timer(7000);
				while (timer.isRunning()) {
					if (isOpen()) {
						if (waitAfter)
							Time.sleepQuick();
						
						return true;
					}
					
					Time.sleep(50);
				}
			}
		}
		
		if (waitAfter)
			Time.sleepQuick();
		
		return false;
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		//Component closeButton = closeButtonData.getComponent();
		
		//return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
		
		//return false;
		
		return MainWidget.get.closeSubInterface(waitAfter);
	}
	
	public Npc getNearest() {
		return Npcs.getNearest(null, 2240, 2241, 1419, 2593);
	}
	

	public Component[] getInvItems() {
		Component invItems = invItemsData.getComponent();
		
		if (invItems != null) {
			Array1D<Component> items = invItems.getComponents();
			if (items == null)
				return null;
			
			if (items.getLength() != 28) {
				System.out.println("Error: GrandExchange.getInvItems() does not have 28 children!");
				return null;
			}
			
			return items.getAll();
		}
		
		return null;
	}
	
	public int countInv() {
		return countInv(null);
	}
	
	public int countInv(int... exceptionIds) {
		Component[] items = getInvItems();
			
		if (items != null) {
			int count = 0;
			
			for (Component item : items) {
				if (item != null) {
					int componentId = item.getComponentId();
					
					if (componentId != -1) {
						boolean exception = false;
						
						if (exceptionIds != null) {
							for (int exceptionId : exceptionIds) {
								if (componentId == exceptionId) {
									exception = true;
									break;
								}
							}
						}
						
						if (!exception)
							count++;
					}
				}
			}
			
			return count;
		}
		
		return -1;
	}
	
	/**
	 * used only in buy or sell screen
	 */
	public int getOfferSelectedItemIdSetting() {
		int itemId = Client.getSettingData().getSettings().get(135, 0xFFFFFFFF); // mask guess
		return itemId;
	}
	
	/**
	 * used only in buy or sell screen
	 */
	public int getOfferQuantitySetting() {
		int quantity = Client.getSettingData().getSettings().get(136, 0xFFFFFFFF); // mask guess
		return quantity;
	}
	
	/**
	 * used only in buy or sell screen
	 */
	public int getOfferPriceSetting() {
		int price = Client.getSettingData().getSettings().get(137, 0xFFFFFFFF); // mask guess
		return price;
	}
	
	/**
	 * used only in buy or sell screen <br>
	 * 
	 * 0 - first offer <br>
	 * 1 - second offer <br>
	 * etc. <br>
	 */
	public int getOpenOfferSetting() {
		int offerIndex = Client.getSettingData().getSettings().get(138, 0xFFFFFFFF);
		return offerIndex;
	}
	
	/**
	 * used only in buy or sell screen during offer creation <br>
	 * 
	 * 0 - buy, 1 - sell
	 */
	public int getCreateOfferTypeSetting() {
		int offerIndex = Client.getSettingData().getSettings().get(139, 0xFFFFFFFF);
		return offerIndex;
	}
	
	/**
	 * used only in buy or sell screen
	 */
	public int getOfferSelectedItemMarketPriceSetting() {
		int price = Client.getSettingData().getSettings().get(140, 0xFFFFFFFF); // mask guess
		return price;
	}
	
	/**
	 * Not 20!
	 * (06:09:39) [144]: 196609 -> 196610
	 * Could be first bit instead == 0
	 * @return true if tutorial by "Grand Exchange Tutor" is completed
	 */
	public boolean isTutorialCompletedSetting() {
		int activated = Client.getSettingData().getSettings().get(144, 1, 0x1);
		return activated == 1;
	}
	
	public boolean gotoOffersScreen(boolean waitOpen) {
		Screen screen = Screen.getOpen();
		if (screen == null) // GE not open
			return false;
		else if (screen.equals(Screen.OFFERS))
			return true;
		
		if (InputPane.get.isOpen()) {
			InputPane.get.enter("0");
			Time.sleepQuick();
		}
		
		Component backButton = backButtonData.getComponent();
		if (backButton != null && backButton.isVisible()) {
			boolean clicked = backButton.interact("Back");
			if (!clicked)
				return false;
			
			if (waitOpen) {
				Timer timer = new Timer(3000);
				while (timer.isRunning()) {
					screen = Screen.getOpen();
					if (screen.equals(Screen.OFFERS)) {
						Time.sleepVeryQuick();
						return clicked;
					}
					
					Time.sleep(50);
				}
			}
			
			if (clicked)
				return true;
		}
		
		return false;
	}
	
	public boolean collectAllToBank() {
		boolean allEmpty = true;
		for (GESlot slot : GESlot.values()) {
			if (slot.isValid()) {
				GEOfferStatus status = slot.getStatus();
				if (status != null && !status.isEmpty()) {
					allEmpty = false;
					break;
				}
			}
		}
		
		if (allEmpty)
			return true;
		
		if (gotoOffersScreen(true)) {
			Component bankButton = collectAllToBankButtonData.getComponent();
			if (bankButton != null && bankButton.isVisible()) {
				return bankButton.interact("Select");
			}
		}
		
		return false;
	}
	
	public boolean collectAllToInventory() {
		boolean allEmpty = true;
		for (GESlot slot : GESlot.values()) {
			if (slot.isValid()) {
				GEOfferStatus status = slot.getStatus();
				if (status != null && !status.isEmpty()) {
					allEmpty = false;
					break;
				}
			}
		}
		
		if (allEmpty)
			return true;
		
		if (gotoOffersScreen(true)) {
			Component invButton = collectAllToInventoryButtonData.getComponent();
			if (invButton != null && invButton.isVisible()) {
				return invButton.interact("Select");
			}
		}
		
		return false;
	}
	
	public String getSearchText() {
		if (isOpen()) {
			Component textComp = searchTextData.getComponent();
			if (textComp != null)
				return textComp.getText();
		}
		
		return null;
	}
	
	public boolean isSearchOpen() {
		if (isOpen()) {
			Component resultComp = searchResultsData.getComponent();
			if (resultComp != null)
				return resultComp.isVisible();
		}
		
		return false;
	}
	
	public Component getSearchResult(String itemFullName) {
		if (isSearchOpen()) {
			Component resultComp = searchResultsData.getComponent();
			if (resultComp != null && resultComp.isVisible()) {
				Array1D<Component> array = resultComp.getComponents();
				if (array != null) {
					Component[] results = array.getAll();
					if (results != null) {
						for (int i = 0; i < results.length; i++) {
							if (results[i].getType() == 4) {
								if (itemFullName.equals(results[i].getText())) {
									return results[i - 2]; // entire result border, type should be 3
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public Component getSearchResult(int itemId) {
		if (isSearchOpen()) {
			Component resultComp = searchResultsData.getComponent();
			if (resultComp != null && resultComp.isVisible()) {
				Array1D<Component> array = resultComp.getComponents();
				if (array != null) {
					Component[] results = array.getAll();
					if (results != null) {
						for (int i = 0; i < results.length; i++) {
							if (results[i].getType() == 5) {
								if (itemId == results[i].getComponentId()) {
									return results[i - 1]; // entire result border, type should be 3
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean isSearchResultVisible(Component resultItem) {
		Component resultComp = searchResultsData.getComponent();
		if (resultComp != null && resultComp.isVisible())
			return resultComp.contains(resultItem);
		else
			return false;
	}
}
