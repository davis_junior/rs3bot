package org.rs3.api.interfaces.widgets;

import org.rs3.api.Menu;
import org.rs3.api.Misc;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.ArrayUtils;

public class MiniMap extends WidgetData {
	
	public static final MiniMap get = new MiniMap(1465);
	
	public static final ComponentData mapBounds = new ComponentData("Map Bounds", 1465, 1);
	public static final ComponentData worldMapButton = new ComponentData("World Map Button", 1465, 44);
	public static final ComponentData runEnergyButton = new ComponentData("Run Energy Button", 1465, 46);
	public static final ComponentData runEnergyText = new ComponentData("Run Energy Text", 1465, 51);
	public static final ComponentData resetCameraButton = new ComponentData("Reset Camera Button", 1465, 55);
	public static final ComponentData homeTeleportButton = new ComponentData("Home Teleport Button", 1465, 57);
	
	// ? Action:
	
	public MiniMap(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1465);
		
		if (widget != null && widget.validate(1465)) {
			// widget validation is all that is needed
			//Component map = mapBounds.getComponent();
			//return map != null && map.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
}
