package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public class LogoutWidget extends ClosableWidget {
	
	public static final LogoutWidget get = new LogoutWidget(26);
	
	public static final ComponentData textData = new ComponentData("Text", 26, 3, 0);
	public static final ComponentData headerData = new ComponentData("Header", 26, 21, 14);
	public static final ComponentData logoutButtonData = new ComponentData("Logout Button", 26, 11);
	public static final ComponentData exitToLobbyButtonData = new ComponentData("Exit to Lobby Button", 26, 18);
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 26, 22);
	
	// Action: null
	// Text: ""
	
	public LogoutWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(26);
		
		if (widget != null && widget.validate(26)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	
	public boolean clickExitToLobbyButton() {
		return clickExitToLobbyButton(false);
	}
	
	public boolean clickExitToLobbyButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component exitToLobbyButton = exitToLobbyButtonData.getComponent();
		if (exitToLobbyButton != null && exitToLobbyButton.isVisible()) {
			boolean result = exitToLobbyButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public boolean clickLogoutButton() {
		return clickLogoutButton(false);
	}
	
	public boolean clickLogoutButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component logoutButton = logoutButtonData.getComponent();
		if (logoutButton != null && logoutButton.isVisible()) {
			boolean result = logoutButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public String getText(boolean removeHtml) {
		if (isOpen()) {
			Component textComp = textData.getComponent();
			if (textComp != null) {
				String text = textComp.getText();
				if (text != null) {
					if (removeHtml) {
						// replace <br> with space: TODO: test more cases
						text = text.replace("<br>", " ");
						return TextUtils.removeHtml(text);
					} else
						return text;
				}
			}
		}
		
		return null;
	}
	
	public String getHeader() {
		if (isOpen()) {
			Component textComp = textData.getComponent();
			if (textComp != null) {
				return textComp.getText();
			}
		}
		
		return null;
	}
}
