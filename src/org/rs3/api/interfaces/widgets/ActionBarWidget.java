package org.rs3.api.interfaces.widgets;

import org.rs3.api.Menu;
import org.rs3.api.Misc;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Time;

public class ActionBarWidget extends WidgetData {
	
	public static final ActionBarWidget get = new ActionBarWidget(1430);
	
	public static final ComponentData previousButtonData = new ComponentData("Previous Button Data", 1430, 250);
	public static final ComponentData nextButtonData = new ComponentData("Next Button Data", 1430, 251);
	
	public static final ComponentData slot1Data = new ComponentData("Slot 1 Data", 1430, 57);
	public static final ComponentData slot2Data = new ComponentData("Slot 2 Data", 1430, 70);
	public static final ComponentData slot3Data = new ComponentData("Slot 3 Data", 1430, 83);
	public static final ComponentData slot4Data = new ComponentData("Slot 4 Data", 1430, 96);
	public static final ComponentData slot5Data = new ComponentData("Slot 5 Data", 1430, 109);
	public static final ComponentData slot6Data = new ComponentData("Slot 6 Data", 1430, 122);
	public static final ComponentData slot7Data = new ComponentData("Slot 7 Data", 1430, 135);
	public static final ComponentData slot8Data = new ComponentData("Slot 8 Data", 1430, 148);
	public static final ComponentData slot9Data = new ComponentData("Slot 9 Data", 1430, 161);
	public static final ComponentData slot10Data = new ComponentData("Slot 10 Data", 1430, 174);
	public static final ComponentData slot11Data = new ComponentData("Slot 11 Data", 1430, 187);
	public static final ComponentData slot12Data = new ComponentData("Slot 12 Data", 1430, 200);
	public static final ComponentData slot13Data = new ComponentData("Slot 13 Data", 1430, 213);
	public static final ComponentData slot14Data = new ComponentData("Slot 14 Data", 1430, 228);
	
	public static final ComponentData autoRetalitateButtonData = new ComponentData("Auto-retaliate Button", 1430, 49);
	public static final ComponentData combatSettingsButtonData = new ComponentData("Combat settings Button", 1430, 256);
	public static final ComponentData lockButtonData = new ComponentData("Lock Button", 1430, 254);
	
	// 49 Action: Toggle
	
	public ActionBarWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component actionBarBounds = MainWidget.actionBarBoundsData.getComponent();
		return actionBarBounds != null && actionBarBounds.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	
	public boolean clickPrevious() {
		return clickPrevious(false);
	}
	
	public boolean clickPrevious(boolean waitAfter) {
		if (MainWidget.get.maximiseActionBar(true) && isOpen()) {
			Component button = previousButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Previous");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public boolean clickNext() {
		return clickNext(false);
	}
	
	public boolean clickNext(boolean waitAfter) {
		if (MainWidget.get.maximiseActionBar(true) && isOpen()) {
			Component button = nextButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Next");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public static boolean isAutoRetaliateEnabled() {
		int autoRetaliate = Client.getSettingData().getSettings().get(462, 0x1);
		return autoRetaliate == 0;
	}
	
	public boolean setAutoRetaliate(boolean enabled) {
		return setAutoRetaliate(enabled, false);
	}
	
	public boolean setAutoRetaliate(boolean enabled, boolean waitAfter) {
		if (enabled == isAutoRetaliateEnabled())
			return true;
		
		if (isOpen()) {
			Component button = autoRetalitateButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Toggle");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public static boolean isActionBarLocked() {
		int lockSetting = Client.getSettingData().getSettings().get(682, 4, 0x1);
		return lockSetting == 1;
	}
	
	public boolean lockActionBar() {
		return lockActionBar(false);
	}
	
	public boolean lockActionBar(boolean waitAfter) {
		if (isActionBarLocked())
			return true;
		
		if (MainWidget.get.maximiseActionBar(true) && isOpen()) {
			Component button = lockButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Lock action bar");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public boolean unlockActionBar() {
		return unlockActionBar(false);
	}
	
	public boolean unlockActionBar(boolean waitAfter) {
		if (!isActionBarLocked())
			return true;
		
		if (MainWidget.get.maximiseActionBar(true) && isOpen()) {
			Component button = lockButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Unlock action bar");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public boolean interactCombatSettingsButton(String action) {
		return interactCombatSettingsButton(action, false);
	}
	
	public boolean interactCombatSettingsButton(String action, boolean waitAfter) {
		if (MainWidget.get.maximiseActionBar(true) && isOpen()) {
			Component combatSettingsButton = combatSettingsButtonData.getComponent();
			if (combatSettingsButton != null && combatSettingsButton.isVisible()) {
				boolean result = combatSettingsButton.interact(action);
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
}
