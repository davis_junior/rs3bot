package org.rs3.api.interfaces.widgets;

import java.awt.event.KeyEvent;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Condition;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class InputPane extends WidgetData {
	
	public static final InputPane get = new InputPane(1469);
	
	public static final ComponentData textData = new ComponentData("Text", 1469, 2);
	public static final ComponentData inputData = new ComponentData("Input", 1469, 3);
	
	// 2 Text: Enter the price you wish to sell for:
	// 3 Text: 222
	
	public InputPane(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		// widget validation is successful even if not open while in ge screen
		Component text = textData.getComponent();
		return text != null && text.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	/**
	 * Note: This method sends enter. A quick wait is performed.
	 * 
	 * @param input
	 * @return
	 */
	public boolean enter(String input) {
		if (isClosed())
			return false;
		
		// widget has global focus, so focus check is unnecessary
		
		// clear anything already entered
		String compInput = getInput();
		if (compInput != null && !compInput.isEmpty()) {
			Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 3000, new Condition() {
				@Override
				public boolean evaluate() {
					String compInput = getInput();
					if (compInput != null && compInput.isEmpty())
						return true;
					
					return false;
				}
			});
			Time.sleepVeryQuick();
		}
		
		compInput = getInput();
		if (compInput != null && compInput.isEmpty()) {
			Keyboard.sendKeys(input);
			Time.sleepQuick();
			
			compInput = getInput();
			if (compInput != null && compInput.equals(input)) {
				Keyboard.pressKey(KeyEvent.VK_ENTER);
				Time.sleepVeryQuick();
				Keyboard.releaseKey(KeyEvent.VK_ENTER);
				Time.sleepVeryQuick();
				
				return true;
			}
		}
		
		return false;
	}
	
	public String getText() {
		if (isOpen()) {
			Component textComp = textData.getComponent();
			if (textComp != null)
				return textComp.getText();
		}
		
		return null;
	}
	
	public String getInput() {
		if (isOpen()) {
			Component inputComp = inputData.getComponent();
			if (inputComp != null)
				return inputComp.getText();
		}
		
		return null;
	}
}
