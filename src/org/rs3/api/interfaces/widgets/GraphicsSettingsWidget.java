package org.rs3.api.interfaces.widgets;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.DropMenu;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class GraphicsSettingsWidget extends OpenableClosableWidget {
	
	public static final GraphicsSettingsWidget get = new GraphicsSettingsWidget(1513);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1513, 8);
	public static final ComponentData minButtonData = new ComponentData("MIN Button", 1513, 24);
	public static final ComponentData lowButtonData = new ComponentData("LOW Button", 1513, 26);
	public static final ComponentData midButtonData = new ComponentData("MID Button", 1513, 28);
	public static final ComponentData highButtonData = new ComponentData("HIGH Button", 1513, 30);
	public static final ComponentData customButtonData = new ComponentData("CUSTOM Button", 1513, 33);
	public static final ComponentData advancedOptionsData = new ComponentData("Advanced Options", 1513, 66);
	public static final ComponentData optionCpuUsageComboData = new ComponentData("Cpu Usage Option Combo", 1513, 66, 19);
	
	
	public GraphicsSettingsWidget(int widgetId) {
		super(widgetId);
	}
	
	
	public static enum GraphicsLevel {
		MIN("MIN", minButtonData, 1),
		LOW("LOW", lowButtonData, 2),
		MID("MID", midButtonData, 3),
		HIGH("HIGH", highButtonData, 4),
		CUSTOM("CUSTOM", customButtonData, 0),
		;
		
		String name;
		ComponentData buttonData;
		int settingValue;
		
		private GraphicsLevel(String name, ComponentData buttonData, int settingValue) {
			this.name = name;
			this.buttonData = buttonData;
			this.settingValue = settingValue;
		}
		
		private Component getButton() {
			if (GraphicsSettingsWidget.get.isOpen())
				return buttonData.getComponent();
			
			return null;
		}
		
		public boolean isSelected() {
			return Client.getGraphicsSettingsContainer().getLevel().getValue() == settingValue;
		}
		
		public boolean select() {
			return select(false);
		}
		
		public boolean select(boolean waitAfter) {
			if (isSelected())
				return true;
			
			if (GraphicsSettingsWidget.get.open(true)) {
				Time.sleepQuick();
				
				Component button = getButton();
				if (button != null && button.isVisible()) {
					boolean result = button.interact(this.name);
					if (waitAfter)
						Time.sleepQuick();
					return result;
				}
			}
			
			return false;
		}
		
		
		public static GraphicsLevel getSelected() {
			for (GraphicsLevel graphicsLevel : values()) {
				if (graphicsLevel.isSelected())
					return graphicsLevel;
			}
			
			return null;
		}
	}
	
	public static enum GraphicsCpuUsage {
		VERY_LOW("Very low", 0),
		LOW("Low", 1),
		NORMAL("Normal", 2),
		HIGH("High", 3),
		MAXIMUM("Maximum", 4),
		;
		
		String name;
		int settingValue;
		
		private GraphicsCpuUsage(String name, int settingValue) {
			this.name = name;
			this.settingValue = settingValue;
		}
		
		public boolean isSelected() {
			return Client.getGraphicsSettingsContainer().getCpuUsage().getValue() == settingValue;
		}
		
		public boolean select() {
			return select(false);
		}
		
		public boolean select(boolean waitAfter) {
			if (isSelected())
				return true;
			
			if (GraphicsSettingsWidget.get.open(true)) {
				Time.sleepQuick();
				
				boolean dropMenuOpen = false;
				
				Component combo = optionCpuUsageComboData.getComponent();
				if (combo != null && combo.isVisible()) {
					if (combo.click(Mouse.LEFT)) {
						Time.sleepQuick();
						
						Timer timer = new Timer(3000);
						while (timer.isRunning()) {
							if (DropMenu.isOpen()) {
								Time.sleepQuick();
								dropMenuOpen = true;
								break;
							}
							
							Time.sleep(50);
						}
					}
				}
				
				if (dropMenuOpen)
					return DropMenu.chooseOption(this.name, waitAfter);
			}
			
			return false;
		}
		
		
		public static GraphicsCpuUsage getSelected() {
			for (GraphicsCpuUsage cpuUsage : values()) {
				if (cpuUsage.isSelected())
					return cpuUsage;
			}
			
			return null;
		}
	}
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1513);
		
		if (widget != null && widget.validate(1513)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			if (LobbyScreen.get.isOpen())
				return LobbyScreen.LobbyTab.OPTIONS.isSelected();
			else if (Client.isLoggedIn())
				return Ribbon.SubInterfaceTab.SETTINGS_GRAPHICS.isOpen();
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		if (LobbyScreen.get.isOpen())
			return LobbyScreen.LobbyTab.OPTIONS.select(waitAfter);
		else if (Client.isLoggedIn())
			return Ribbon.SubInterfaceTab.SETTINGS_GRAPHICS.open(waitAfter);
		
		return false;
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		if (Client.isLoggedIn())
			return MainWidget.get.closeSubInterface(waitAfter);
		else if (LobbyScreen.get.isOpen()) // can't really close this widget in lobby screen; can just select different tab
			return LobbyScreen.LobbyTab.WORLD_SELECT.select(waitAfter);
		
		return false;
	}
}
