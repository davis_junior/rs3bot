package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.Openable;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;

public class PrayersSubInterfaceTabWidget extends WidgetData implements Openable {
	
	public static final PrayersSubInterfaceTabWidget get = new PrayersSubInterfaceTabWidget(1457);
	
	public static final ComponentData prayersData = new ComponentData("Prayers", 1457, 11);
	
	
	public PrayersSubInterfaceTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1457);
		
		if (widget != null && widget.validate(1457)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return SubInterfaceTab.POWERS_PRAYERS.open(waitAfter);
	}
	
	
	public Component[] getPrayers() {
		Component prayers = prayersData.getComponent();
		
		if (prayers != null) {
			Array1D<Component> array = prayers.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
