package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;

public class DefenceAbilitiesMainTabWidget extends OpenableClosableWidget {
	
	public static final DefenceAbilitiesMainTabWidget get = new DefenceAbilitiesMainTabWidget(1449);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1449, 1);
	public static final ComponentData tabDefenceButtonData = new ComponentData("Defence Tab Button", 1449, 7, 7);
	public static final ComponentData tabConstitutionButtonData = new ComponentData("Constitution Tab Button", 1449, 7, 8);

	
	public DefenceAbilitiesMainTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainDefenseAbilitiesBounds = MainWidget.tabDefenseAbilitiesBounds.getComponent();
		return mainDefenseAbilitiesBounds != null && mainDefenseAbilitiesBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.DEFENSE_ABILITIES.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.DEFENSE_ABILITIES.close(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
