package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Component;

/**
 * @see Ribbon for uses
 */
public class HoveredRibbonTabs extends WidgetData {
	
	public static final HoveredRibbonTabs get = new HoveredRibbonTabs(1432);
	
	public static final ComponentData tab1ButtonData = new ComponentData("Tab 1 Button", 1432, 4, 0);
	public static final ComponentData tab2ButtonData = new ComponentData("Tab 2 Button", 1432, 4, 2);
	public static final ComponentData tab3ButtonData = new ComponentData("Tab 3 Button", 1432, 4, 4);
	public static final ComponentData tab4ButtonData = new ComponentData("Tab 4 Button", 1432, 4, 6);
	public static final ComponentData tab5ButtonData = new ComponentData("Tab 5 Button", 1432, 4, 8);
	
	// 4, 0 Action: <blank> but not null
	// 4, 0 Component name: Prayer Abilities
	
	public HoveredRibbonTabs(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component hoveredRibbonTabsBounds = MainWidget.hoveredRibbonTabsBounds.getComponent();
		return hoveredRibbonTabsBounds != null && !hoveredRibbonTabsBounds._isHidden();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
}
