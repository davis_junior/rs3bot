package org.rs3.api.interfaces.widgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class WornEquipmentWidget extends OpenableClosableWidget {
	
	public static final WornEquipmentWidget get = new WornEquipmentWidget(1464);
	
	public static final ComponentData equipmentData = new ComponentData("Equipment", 1464, 15);
	
	public WornEquipmentWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainWornEquipmentBounds = MainWidget.tabWornEquipmentBounds.getComponent();
		return mainWornEquipmentBounds != null && mainWornEquipmentBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.WORN_EQUIPMENT.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.WORN_EQUIPMENT.close(waitAfter);
	}
	
	
	public Component[] getEquipment() {
		Component equipment = equipmentData.getComponent();
		
		if (equipment != null) {
			Array1D<Component> array = equipment.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
