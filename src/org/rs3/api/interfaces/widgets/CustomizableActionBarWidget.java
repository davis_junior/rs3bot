package org.rs3.api.interfaces.widgets;

import org.rs3.api.Ability;
import org.rs3.api.ActionBar;
import org.rs3.api.ActionSlot;
import org.rs3.api.Menu;
import org.rs3.api.Misc;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class CustomizableActionBarWidget extends WidgetData {
	
	public static final CustomizableActionBarWidget get = new CustomizableActionBarWidget(1436);
	
	public static final ComponentData trashcanButtonData = new ComponentData("Trashcan Button Data", 1436, 6);
	
	public static final ComponentData previousButtonData = new ComponentData("Previous Button Data", 1436, 10);
	public static final ComponentData nextButtonData = new ComponentData("Next Button Data", 1436, 11);

	public static final ComponentData slot1Data = new ComponentData("Slot 1 Data", 1436, 17);
	public static final ComponentData slot2Data = new ComponentData("Slot 2 Data", 1436, 30);
	public static final ComponentData slot3Data = new ComponentData("Slot 3 Data", 1436, 43);
	public static final ComponentData slot4Data = new ComponentData("Slot 4 Data", 1436, 56);
	public static final ComponentData slot5Data = new ComponentData("Slot 5 Data", 1436, 69);
	public static final ComponentData slot6Data = new ComponentData("Slot 6 Data", 1436, 82);
	public static final ComponentData slot7Data = new ComponentData("Slot 7 Data", 1436, 95);
	public static final ComponentData slot8Data = new ComponentData("Slot 8 Data", 1436, 108);
	public static final ComponentData slot9Data = new ComponentData("Slot 9 Data", 1436, 121);
	public static final ComponentData slot10Data = new ComponentData("Slot 10 Data", 1436, 134);
	public static final ComponentData slot11Data = new ComponentData("Slot 11 Data", 1436, 147);
	public static final ComponentData slot12Data = new ComponentData("Slot 12 Data", 1436, 160);
	public static final ComponentData slot13Data = new ComponentData("Slot 13 Data", 1436, 173);
	public static final ComponentData slot14Data = new ComponentData("Slot 14 Data", 1436, 186);
	
	public CustomizableActionBarWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1436);
		
		if (widget != null && widget.validate(1436)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	public boolean clickPrevious() {
		return clickPrevious(false);
	}
	
	public boolean clickPrevious(boolean waitAfter) {
		if (isOpen()) {
			Component button = previousButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Previous");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public boolean clickNext() {
		return clickNext(false);
	}
	
	public boolean clickNext(boolean waitAfter) {
		if (isOpen()) {
			Component button = nextButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Next");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	@SuppressWarnings("unused")
	private boolean clickClearAll() {
		return clickClearAll(false);
	}
	
	private boolean clickClearAll(boolean waitAfter) {
		if (isOpen()) {
			Component button = trashcanButtonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Clear-all");
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public boolean clearAll() {
		return clearAll(false);
	}
	
	public boolean clearAll(boolean waitAfter) {
		// check if already cleared
		boolean alreadyClear = true;
		for (ActionSlot slot : ActionBar.getCurrentSlots()) {
			if (!slot.getAbility().equals(Ability.EMPTY)) {
				alreadyClear = false;
				break;
			}
		}
		if (alreadyClear)
			return true;
		
		if (isOpen()) {
			if (ChatOptionPane.get.isOpen() || clickClearAll(true)) {
				Time.sleepQuick();
				
				Timer timer = new Timer(4000);
				while (timer.isRunning()) {
					if (ChatOptionPane.get.isOpen()) {
						// Header Text: Are you sure you want to clear your actionbar?
						if (ChatOptionPane.get.getHeader().contains("clear")) {
							if (ChatOptionPane.get.chooseYes()) {
								if (waitAfter)
									Time.sleepQuick();
								return true;
							}
							Time.sleepQuick();
						}
					}
					
					Time.sleep(50);
				}
			}
		}
		
		return false;
	}
}
