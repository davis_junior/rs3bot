package org.rs3.api.interfaces.widgets;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class DeathItemReclaimingWidget extends ClosableWidget {
	
	public static final DeathItemReclaimingWidget get = new DeathItemReclaimingWidget(1626);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1626, 43);
	public static final ComponentData confirmButtonData = new ComponentData("Confirm Button", 1626, 47);
	public static final ComponentData confirmationScreenYesButtonData = new ComponentData("Confirmation Screen Yes Button", 1626, 59); // main api visible method is accurate, actions are null
	public static final ComponentData confirmationScreenNoButtonData = new ComponentData("Confirmation Screen No Button", 1626, 68); // main api visible method is accurate, actions are null
	
	public DeathItemReclaimingWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1626);
		
		if (widget != null && widget.validate(1626)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean clickConfirm() {
		return clickConfirm(false);
	}
	
	public boolean clickConfirm(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component confirmButton = confirmButtonData.getComponent();
		if (confirmButton != null && confirmButton.isVisible()) {
			boolean result = confirmButton.interact("Confirm");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean isConfirmationScreenOpen() {
		if (isClosed())
			return false;
		
		Component yesButton = confirmationScreenYesButtonData.getComponent();
		if (yesButton != null && yesButton.isVisible())
			return true;
		
		Component noButton = confirmationScreenNoButtonData.getComponent();
		if (noButton != null && noButton.isVisible())
			return true;
		
		return false;
	}
	
	public boolean clickConfirmationScreenYesButton() {
		return clickConfirmationScreenYesButton(false);
	}
	
	public boolean clickConfirmationScreenYesButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (!isConfirmationScreenOpen())
			return false;
		
		Component yesButton = confirmationScreenYesButtonData.getComponent();
		if (yesButton != null && yesButton.isVisible()) {
			boolean result = yesButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean clickConfirmationScreenNoButton() {
		return clickConfirmationScreenNoButton(false);
	}
	
	public boolean clickConfirmationScreenNoButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (!isConfirmationScreenOpen())
			return false;
		
		Component noButton = confirmationScreenNoButtonData.getComponent();
		if (noButton != null && noButton.isVisible()) {
			boolean result = noButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
