package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class DungeoneeringFloorSettingsWidget extends ClosableWidget {
	
	public static final DungeoneeringFloorSettingsWidget get = new DungeoneeringFloorSettingsWidget(1049);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1049, 50);
	public static final ComponentData enterDungeonButtonData = new ComponentData("Enter Dungeon Button", 1049, 55);
	
	public DungeoneeringFloorSettingsWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1049);
		
		if (widget != null && widget.validate(1049)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	
	public boolean clickEnterDungeonButton() {
		return clickEnterDungeonButton(false);
	}
	
	public boolean clickEnterDungeonButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component enterButton = enterDungeonButtonData.getComponent();
		if (enterButton != null && enterButton.isVisible()) {
			boolean result = enterButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
}
