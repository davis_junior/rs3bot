package org.rs3.api.interfaces.widgets;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class MakeItemsWidget extends ClosableWidget {
	
	public static final MakeItemsWidget get = new MakeItemsWidget(1370);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1370, 31);
	public static final ComponentData makeButtonData = new ComponentData("Make Button", 1370, 37);
	
	public MakeItemsWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1370);
		
		if (widget != null && widget.validate(1370)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean clickMakeButton() {
		return clickMakeButton(false);
	}
	
	public boolean clickMakeButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component makeButton = makeButtonData.getComponent();
		if (makeButton != null && makeButton.isVisible()) {
			boolean result = makeButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
