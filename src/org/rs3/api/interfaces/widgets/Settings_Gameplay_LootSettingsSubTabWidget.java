package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class Settings_Gameplay_LootSettingsSubTabWidget extends OpenableClosableWidget {
	
	public static final Settings_Gameplay_LootSettingsSubTabWidget get = new Settings_Gameplay_LootSettingsSubTabWidget(1623);
	
	public static final ComponentData useLootInventoryButtonData = new ComponentData("Use Loot inventory Button", 1623, 30);
	
	public Settings_Gameplay_LootSettingsSubTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1623);
		
		if (widget != null && widget.validate(1623)) {
			// widget validation is all that is needed
			//Component collectionBox = closeButtonData.getComponent();
			//return collectionBox != null && collectionBox.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}
	
	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Settings_GameplaySubTabButtonsWidget.GameplaySettingsSubTab.LOOT_SETTINGS.select(waitAfter);
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		return MainWidget.get.closeSubInterface(waitAfter);
	}
	
	
	public boolean isLootInventoryEnabled() {
		int useLootInventory = Client.getSettingData().getSettings().get(5413, 0x1);
		return useLootInventory == 1;
	}
	
	@SuppressWarnings("unused")
	private boolean toggleUseLootInventoryButton() {
		return toggleUseLootInventoryButton(false);
	}
	
	private boolean toggleUseLootInventoryButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component button = useLootInventoryButtonData.getComponent();
		if (button != null && button.isVisible()) {
			boolean result = button.interact("Toggle");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean enableLootInventory() {
		return enableLootInventory(false);
	}
	
	public boolean enableLootInventory(boolean waitAfter) {
		if (isLootInventoryEnabled())
			return true;
		
		if (open(true)) {
			Time.sleepQuick();
			return toggleUseLootInventoryButton(waitAfter);
		}
		
		return false;
	}
	
	public boolean disableLootInventory() {
		return disableLootInventory(false);
	}
	
	public boolean disableLootInventory(boolean waitAfter) {
		if (!isLootInventoryEnabled())
			return true;
		
		if (open(true)) {
			Time.sleepQuick();
			return toggleUseLootInventoryButton(waitAfter);
		}
		
		return false;
	}
}
