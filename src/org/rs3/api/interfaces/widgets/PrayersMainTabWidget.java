package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;

public class PrayersMainTabWidget extends OpenableClosableWidget {
	
	public static final PrayersMainTabWidget get = new PrayersMainTabWidget(1458);
	
	public static final ComponentData prayersData = new ComponentData("Prayers", 1458, 33);
	
	public PrayersMainTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainPrayersBounds = MainWidget.tabPrayersBounds.getComponent();
		return mainPrayersBounds != null && mainPrayersBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.PRAYERS.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.PRAYERS.close(waitAfter);
	}
	
	
	public Component[] getPrayers() {
		Component prayers = prayersData.getComponent();
		
		if (prayers != null) {
			Array1D<Component> array = prayers.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
