package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public class ItemSelectInfoWidget extends ClosableWidget {
	
	public static final ItemSelectInfoWidget get = new ItemSelectInfoWidget(1370);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1370, 31);
	public static final ComponentData doActionButtonData = new ComponentData("Do Action Button", 1370, 37);
	public static final ComponentData selectedItemIdData = new ComponentData("Selected Item Id", 1370, 55);
	public static final ComponentData selectedItemNameData = new ComponentData("Selected Item Name", 1370, 56);
	public static final ComponentData selectedItemAmountData = new ComponentData("Selected Item Amount", 1370, 74);
	
	// ? Text: 
	
	// ? Action: null
	// ? Text: 
	
	public ItemSelectInfoWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1370);
		
		if (widget != null && widget.validate(1370))
			return true;
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
	
	
	public boolean clickDoActionButton(boolean waitAfter) {
		if (isOpen()) {
			Component doAction = doActionButtonData.getComponent();
			if (doAction != null) {
				boolean result = doAction.click(Mouse.LEFT);
				
				if (waitAfter)
					Time.sleepQuick();
				
				return result;
			}
		}
		
		return false;
	}
	
	public int getSelectedItemId() {
		if (isOpen()) {
			Component idComp = selectedItemIdData.getComponent();
			if (idComp != null)
				return idComp.getComponentId();
		}
		
		return -1;
	}
	
	public String getSelectedItemName() {
		if (isOpen()) {
			Component textComp = selectedItemNameData.getComponent();
			if (textComp != null)
				return textComp.getText();
		}
		
		return null;
	}
	
	public int getSelectedItemAmount() {
		if (isOpen()) {
			Component textComp = selectedItemAmountData.getComponent();
			if (textComp != null) {
				String text = textComp.getText();
				if (text != null) {
					try {
						return Integer.parseInt(text);
					} catch (NumberFormatException nfe) {}
				}
			}
		}
		
		return -1;
	}
}
