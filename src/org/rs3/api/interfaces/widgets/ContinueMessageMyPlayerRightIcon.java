package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public class ContinueMessageMyPlayerRightIcon extends ClosableWidget {
	
	public static final ContinueMessageMyPlayerRightIcon get = new ContinueMessageMyPlayerRightIcon(1191);
	
	public static final ComponentData textData = new ComponentData("Text", 1191, 6);
	public static final ComponentData headerData = new ComponentData("Header", 1191, 2);
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1191, 15);
	
	// Action: null
	// Text: ""
	
	public ContinueMessageMyPlayerRightIcon(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component continueMessage = closeButtonData.getComponent();
		return continueMessage != null && continueMessage.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component continueButton = closeButtonData.getComponent();
		if (continueButton != null && continueButton.isVisible()) {
			boolean result = continueButton.click(Mouse.LEFT);
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	
	public String getText(boolean removeHtml) {
		if (isOpen()) {
			Component textComp = textData.getComponent();
			if (textComp != null) {
				String text = textComp.getText();
				if (text != null) {
					if (removeHtml) {
						// replace <br> with space: TODO: test more cases
						text = text.replace("<br>", " ");
						return TextUtils.removeHtml(text);
					} else
						return text;
				}
			}
		}
		
		return null;
	}
	
	public String getHeader() {
		if (isOpen()) {
			Component textComp = headerData.getComponent();
			if (textComp != null)
				return textComp.getText();
		}
		
		return null;
	}
}
