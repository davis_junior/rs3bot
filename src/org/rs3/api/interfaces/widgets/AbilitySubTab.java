package org.rs3.api.interfaces.widgets;

import org.rs3.api.ActionBar.AbilityActionType;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.Openable;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Time;

public enum AbilitySubTab {
	// TODO: unknown bit shifts: 8, 12 (likely both for ranged though they only have 1 tab)
	
	//
	// Main Tabs
	//
	MAIN_MELEE_ATTACK(true, AbilityActionType.MELEE, "Attack", MeleeAbilitiesMainTabWidget.get, MeleeAbilitiesMainTabWidget.tabAttackButtonData, 4, 0),
	MAIN_MELEE_STRENGTH(true, AbilityActionType.MELEE, "Strength", MeleeAbilitiesMainTabWidget.get, MeleeAbilitiesMainTabWidget.tabSrengthButtonData, 4, 1),
	
	MAIN_RANGED_RANGED(true, AbilityActionType.RANGED, "Ranged", RangedAbilitiesMainTabWidget.get, RangedAbilitiesMainTabWidget.tabRangedButtonData, 12, 0), // bitshift might be 8
	
	MAIN_MAGIC_ABILITIES(true, AbilityActionType.MAGIC, "Abilities", MagicAbilitiesMainTabWidget.get, MagicAbilitiesMainTabWidget.tabAbilitiesButtonData, 20, 0),
	MAIN_MAGIC_COMBAT(true, AbilityActionType.MAGIC, "Combat", MagicAbilitiesMainTabWidget.get, MagicAbilitiesMainTabWidget.tabCombatButtonData, 20, 1),
	MAIN_MAGIC_TELEPORT(true, AbilityActionType.MAGIC, "Teleport", MagicAbilitiesMainTabWidget.get, MagicAbilitiesMainTabWidget.tabTeleportButtonData, 20, 2),
	MAIN_MAGIC_SKILLING(true, AbilityActionType.MAGIC, "Skilling", MagicAbilitiesMainTabWidget.get, MagicAbilitiesMainTabWidget.tabSkillingButtonData, 20, 3),
	
	MAIN_DEFENSIVE_DEFENCE(true, AbilityActionType.DEFENSIVE, "Defence", DefenceAbilitiesMainTabWidget.get, DefenceAbilitiesMainTabWidget.tabDefenceButtonData, 28, 0),
	MAIN_DEFENSIVE_CONSTITUTION(true, AbilityActionType.DEFENSIVE, "Constitution", DefenceAbilitiesMainTabWidget.get, DefenceAbilitiesMainTabWidget.tabConstitutionButtonData, 28, 1),
	
	//
	// Subinterface Tabs
	//
	SUB_MELEE_ATTACK(false, AbilityActionType.MELEE, "Attack", MeleeAbilitiesSubInterfaceTabWidget.get, MeleeAbilitiesSubInterfaceTabWidget.tabAttackButtonData, 0, 0),
	SUB_MELEE_STRENGTH(false, AbilityActionType.MELEE, "Strength", MeleeAbilitiesSubInterfaceTabWidget.get, MeleeAbilitiesSubInterfaceTabWidget.tabStrengthButtonData, 0, 1),
	
	SUB_RANGED_RANGED(false, AbilityActionType.RANGED, "Ranged", RangedAbilitiesSubInterfaceTabWidget.get, RangedAbilitiesSubInterfaceTabWidget.tabRangedButtonData, 8, 0), // bitshift might be 12
	
	SUB_MAGIC_ABILITIES(false, AbilityActionType.MAGIC, "Abilities", MagicAbilitiesSubInterfaceTabWidget.get, MagicAbilitiesSubInterfaceTabWidget.tabAbilitiesButtonData, 16, 0),
	SUB_MAGIC_COMBAT(false, AbilityActionType.MAGIC, "Combat", MagicAbilitiesSubInterfaceTabWidget.get, MagicAbilitiesSubInterfaceTabWidget.tabCombatButtonData, 16, 1),
	SUB_MAGIC_TELEPORT(false, AbilityActionType.MAGIC, "Teleport", MagicAbilitiesSubInterfaceTabWidget.get, MagicAbilitiesSubInterfaceTabWidget.tabTeleportButtonData, 16, 2),
	SUB_MAGIC_SKILLING(false, AbilityActionType.MAGIC, "Skilling", MagicAbilitiesSubInterfaceTabWidget.get, MagicAbilitiesSubInterfaceTabWidget.tabSkillingButtonData, 16, 3),
	
	SUB_DEFENSIVE_DEFENCE(false, AbilityActionType.DEFENSIVE, "Defence", DefenceAbilitiesSubInterfaceTabWidget.get, DefenceAbilitiesSubInterfaceTabWidget.tabDefenceButtonData, 24, 0),
	SUB_DEFENSIVE_CONSTITUTION(false, AbilityActionType.DEFENSIVE, "Constitution", DefenceAbilitiesSubInterfaceTabWidget.get, DefenceAbilitiesSubInterfaceTabWidget.tabConstitutionButtonData, 24, 1),
	;
	
	boolean mainTab;
	AbilityActionType actionType; // tab type
	String name;
	Openable widgetData;
	ComponentData tabButtonData;
	int bitsToShift;
	int tabSettingValue;
	
	private AbilitySubTab(boolean mainTab, AbilityActionType actionType, String name, Openable widgetData, ComponentData tabButtonData, int bitsToShift, int tabSettingValue) {
		this.mainTab = mainTab;
		this.actionType = actionType;
		this.name = name;
		this.widgetData = widgetData;
		this.tabButtonData = tabButtonData;
		this.bitsToShift = bitsToShift;
		this.tabSettingValue = tabSettingValue;
	}
	
	public boolean isOpen() {
		if (widgetData.isOpen()) {
			int tabSetting = Client.getSettingData().getSettings().get(3705, this.bitsToShift, 0x3);
			return tabSetting == this.tabSettingValue;
		}
		
		return false;
	}
	
	public boolean open() {
		return open(false);
	}
	
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		if (widgetData.open(true)) {
			Time.sleepMedium();
			
			if (isOpen())
				return true;
			
			Component tabButton = tabButtonData.getComponent();
			if (tabButton != null) {
				boolean result = tabButton.interact(name);
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public static AbilitySubTab getOpenMainTab(AbilityActionType actionType) {
		for (AbilitySubTab subTab : AbilitySubTab.values()) {
			if (subTab.mainTab && subTab.actionType.equals(actionType) && subTab.isOpen())
				return subTab;
		}
		
		return null;
	}
	
	public static AbilitySubTab getOpenSubInterfaceTab(AbilityActionType actionType) {
		for (AbilitySubTab subTab : AbilitySubTab.values()) {
			if (!subTab.mainTab && subTab.actionType.equals(actionType) && subTab.isOpen())
				return subTab;
		}
		
		return null;
	}
}
