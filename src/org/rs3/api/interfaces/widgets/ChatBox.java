package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class ChatBox extends WidgetData {
	
	public static final ChatBox get = new ChatBox(137);
	
	public static final ComponentData alwaysOnModeButton = new ComponentData("Always-On Mode Button", 137, 103); // visible always false
	
	// ? Action:
	
	public ChatBox(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(137);
		
		if (widget != null && widget.validate(137)) {
			// widget validation is all that is needed
			//Component map = mapBounds.getComponent();
			//return map != null && map.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	
	public static boolean isAlwaysOnModeEnabled() {
		int alwaysOn = Client.getSettingData().getSettings().get(1775, 3, 0x1);
		return alwaysOn == 1;
	}
	
	public boolean setAlwaysOnMode(boolean enabled) {
		return setAlwaysOnMode(enabled, false);
	}
	
	public boolean setAlwaysOnMode(boolean enabled, boolean waitAfter) {
		if (enabled == isAlwaysOnModeEnabled())
			return true;
		
		if (isOpen()) {
			Component button = alwaysOnModeButton.getComponent();
			if (button != null && button.validate()) {
				String action = enabled ? "Turn ON" : "Filter";
				boolean result = button.interact(action);
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
}
