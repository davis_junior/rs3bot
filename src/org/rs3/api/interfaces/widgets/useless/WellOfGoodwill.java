package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;

public class WellOfGoodwill extends ClosableWidget {

	public static final WellOfGoodwill get = new WellOfGoodwill(1316);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1316, 42);
	
	// ? Text
	
	// 42 Action: Close
	
	public WellOfGoodwill(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component wellOfGoodwill = closeButtonData.getComponent();
		return wellOfGoodwill != null && wellOfGoodwill.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
	
}
