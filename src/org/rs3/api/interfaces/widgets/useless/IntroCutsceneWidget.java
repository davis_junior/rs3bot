package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class IntroCutsceneWidget extends ClosableWidget {
	
	public static final IntroCutsceneWidget get = new IntroCutsceneWidget(548);
	
	public static final ComponentData skipButtonData = new ComponentData("Skip Button", 548, 4); // Action: Skip
	
	public IntroCutsceneWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(548);
		
		if (widget != null && widget.validate(548)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = skipButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Skip");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
