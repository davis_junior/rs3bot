package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Random;

public class TaskComplete extends ClosableWidget {
	
	public static final TaskComplete get = new TaskComplete(1223);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1223, 11, 1);
	public static final ComponentData continueButtonData = new ComponentData("Continue Button", 1223, 19);
	
	// 7 Text: Increasing your skills gives you access to more abilities, training methods and things to make.
	
	// 11 Action: Close
	// 19 Action: Select
	// 18 Text: Continue
	
	// 10, 14 Text: Task Complete
	
	public TaskComplete(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component taskComplete1 = closeButtonData.getComponent();
		boolean taskComplete1Visible = taskComplete1 != null && taskComplete1.isVisible();
		if (taskComplete1Visible)
			return true;
		
		Component taskComplete2 = continueButtonData.getComponent();
		boolean taskComplete2Visible = taskComplete2 != null && taskComplete2.isVisible();
		if (taskComplete2Visible)
			return true;
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		if (!MainWidget.get.closeSubInterface(true))
			return false;
		
		Component closeButton = null;
		
		if (Random.nextBoolean())
			closeButton = closeButtonData.getComponent();
		else
			closeButton = continueButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
}
