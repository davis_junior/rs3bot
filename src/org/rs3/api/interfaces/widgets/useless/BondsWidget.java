package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Random;
import org.rs3.util.Time;

public class BondsWidget extends ClosableWidget {
	
	public static final BondsWidget get = new BondsWidget(245);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 245, 4);
	public static final ComponentData closeButton2Data = new ComponentData("Close Button 2", 245, 13);
	
	public BondsWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(245);
		
		if (widget != null && widget.validate(245)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton;
		String action;
		
		if (Random.nextBoolean()) {
			closeButton = closeButtonData.getComponent();
			action = "Close";
		} else {
			closeButton = closeButton2Data.getComponent();
			action = "Select";
		}
		
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact(action);
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
