package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Random;

public class ExaminePlayer extends ClosableWidget {
	
	public static final ExaminePlayer get = new ExaminePlayer(1560);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1560, 22);
	
	// ? Action:
	// ? Text:
	
	public ExaminePlayer(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1560);
		
		if (widget != null && widget.validate(1560)) {
			// widget validation is all that is needed
			//Component closeButton = closeButtonData.getComponent();
			//return closeButton != null && closeButton.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
}
