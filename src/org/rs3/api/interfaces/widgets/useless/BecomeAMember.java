package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Random;

public class BecomeAMember extends ClosableWidget {
	
	public static final BecomeAMember get = new BecomeAMember(149);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 149, 246);
	public static final ComponentData noThanksButtonData = new ComponentData("No Thanks Button", 149, 235);
	
	// Causes: when you reach level 10 in any? skill
	
	// ? Text: TODO
	
	// 246 Action: Close
	// 235 Action: Select
	// 237 Text: No thanks
	
	// ?, ? (Title) Text: TODO
	
	public BecomeAMember(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component becomeAMemberClose = closeButtonData.getComponent();
		boolean closeVisible = becomeAMemberClose != null && becomeAMemberClose.isVisible();
		if (closeVisible)
			return true;
		
		Component becomeAMemberNoThanks = noThanksButtonData.getComponent();
		boolean noThanksVisible = becomeAMemberNoThanks != null && becomeAMemberNoThanks.isVisible();
		if (noThanksVisible)
			return true;
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = null;
		
		if (Random.nextBoolean())
			closeButton = closeButtonData.getComponent();
		else
			closeButton = noThanksButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
}
