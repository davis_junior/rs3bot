package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.wrappers.Component;

public class TreasureHunterChest extends ClosableWidget {
	
	public static final TreasureHunterChest get = new TreasureHunterChest(1252);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1252, 5);
	
	// TODO
	// ?, ? Text:
	// ? Action:
	// ? Text:
	
	public TreasureHunterChest(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component treasureHunterChest = closeButtonData.getComponent();
		return treasureHunterChest != null && treasureHunterChest.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		if (GrandExchangeWidget.get.isOpen() || Bank.get.isOpen() || DepositBox.get.isOpen()) // do not close if some other interfaces are open since it's impossible
			return false;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
}
