package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;

public class BeginnerHintMessages extends ClosableWidget {
	
	public static final BeginnerHintMessages get = new BeginnerHintMessages(669);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 669, 18);
	
	// ?, ? Text: Your backup is almost full! // TODO: get html version
	// ?, ? Text: You might want to think about depositing some items in a bank. TODO: get html version
	
	// 17, 0 Text: You can safely store items in any bank in Gielinor.<br>Banks are marked on both the world map and the minimap.
	
	// 17, 0 Text: Alternatively, if you want to get rid of an item, you can <col=FFcb05>right-click</col> it and select <col=FFcb05>drop</col> or <col=FFcb05>destroy</col>.
	
	// 17, 0 Text: A new poll has been added! Click on the 'Community' button to vote now.
	
	// 18 Action: Close
	// 19 Text: OK
	
	public BeginnerHintMessages(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component beginnerHintMessages = closeButtonData.getComponent();
		return beginnerHintMessages != null && beginnerHintMessages.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
	
}
