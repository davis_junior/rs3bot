package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Component;

public class WorldMap extends OpenableClosableWidget {
	
	public static final WorldMap get = new WorldMap(1422);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1422, 167);
	
	// 167 Action: Close
	// ? Text: 
	
	public WorldMap(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component worldMap = closeButtonData.getComponent();
		return worldMap != null && worldMap.isVisible(true, true); // TODO: Note: only visible when isVisible() defaults to true on unknown
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean open(boolean waitAfter) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
	
}
