package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;

public class MembersOnlyContent extends ClosableWidget {

	public static final MembersOnlyContent get = new MembersOnlyContent(1401);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1401, 31, 1);
	
	// 17 Text: Board a ship to the island sanctuary of Entrana, where no weapons are allowed but a boss still awaits in the Lost City quest.
	
	// 31 Action: Close
	// 30, 14 (Title) Text: Members Only Content
	
	public MembersOnlyContent(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component membersOnlyContent = closeButtonData.getComponent();
		return membersOnlyContent != null && membersOnlyContent.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
	
}
