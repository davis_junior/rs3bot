package org.rs3.api.interfaces.widgets.useless;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Random;

public class BecomeAMember2 extends ClosableWidget {
	
	public static final BecomeAMember2 get = new BecomeAMember2(1155);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1155, 49);
	public static final ComponentData noThanksButtonData = new ComponentData("No Thanks Button", 1155, 37);
	
	// Causes: when you reach level 5 in magic skill (possibly others)
	
	// ? Text: TODO
	
	// 49 Action: Close
	// 37 Action: Select
	// 39 Text: No thanks
	
	// ?, ? (Title) Text: TODO
	
	public BecomeAMember2(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		// TODO: widget is not validated when not open; use widget validation instead to check if open
		
		Component becomeAMemberClose = closeButtonData.getComponent();
		boolean closeVisible = becomeAMemberClose != null && becomeAMemberClose.isVisible();
		if (closeVisible)
			return true;
		
		Component becomeAMemberNoThanks = noThanksButtonData.getComponent();
		boolean noThanksVisible = becomeAMemberNoThanks != null && becomeAMemberNoThanks.isVisible();
		if (noThanksVisible)
			return true;
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = null;
		
		if (Random.nextBoolean())
			closeButton = closeButtonData.getComponent();
		else
			closeButton = noThanksButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
}
