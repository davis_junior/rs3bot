package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;

public class RangedAbilitiesMainTabWidget extends OpenableClosableWidget {
	
	public static final RangedAbilitiesMainTabWidget get = new RangedAbilitiesMainTabWidget(1452);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1452, 1);
	public static final ComponentData tabRangedButtonData = new ComponentData("Attack Tab Button", 1452, 7, 7);

	
	public RangedAbilitiesMainTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainRangedAbilitiesBounds = MainWidget.tabRangedAbilitiesBounds.getComponent();
		return mainRangedAbilitiesBounds != null && mainRangedAbilitiesBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.RANGED_ABILITIES.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.RANGED_ABILITIES.close(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
