package org.rs3.api.interfaces.widgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Calculations;
import org.rs3.api.Interfaces;
import org.rs3.api.GroundObjects;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class DepositBox extends OpenableClosableWidget {
	
	public static final DepositBox get = new DepositBox(11);
	
	public static final ComponentData itemsData = new ComponentData("Items", 11, 1);
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 11, 41);
	public static final ComponentData depositCarriedItemsButtonData = new ComponentData("Deposit carried items Button", 11, 11);
	
	public DepositBox(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component depositBox = itemsData.getComponent();
		return depositBox != null && depositBox.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}
	
	// TODO: waitAfter is unused because a wait is forced after click
	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		RSAnimable deposit = getNearest();
		
		if (deposit != null) {
			if (Calculations.distanceTo(deposit.getData().getCenterLocation().getTile(true)) > 3) {
			//if (!deposit.getData().getCenterLocation().getTile(true).isOnScreen()) {
				boolean walkSucess = false;
				Timer walkTimer = new Timer(10000);
				while (walkTimer.isRunning()) {
					if (deposit.getTile().loopWalkTo(10000))
						walkSucess = true;
					
					Time.sleepMedium();
					
					if (walkSucess)
						break;
				}
				
				if (!walkSucess)
					return false;
			}
			
			
			Paint.updatePaintObject(deposit);
			
			if (deposit.loopInteract(getIsOpenCondition(), "Deposit") || isOpen()) {
				Timer timer = new Timer(5000);
				while (timer.isRunning()) {
					if (isOpen())
						return true;
					
					Time.sleep(50);
				}
			}
		}
		
		return false;
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitOnClose) {
		if (!isOpen())
			return true;
		
		Component closeComp = closeButtonData.getComponent();
		if (closeComp != null) {
			Component closeButton = closeComp;
			
			if (closeButton != null && closeButton.isVisible()) {
				boolean result = closeButton.interact("Close");
				
				if (waitOnClose)
					Time.sleepVeryQuick();
				
				return result;
			}
		}
		
		return false;
	}
	
	public RSAnimable getNearest() {
		return (RSAnimable) GroundObjects.getNearest(GroundObject.Type.Animable, 36788);
	}
	
	public Component[] getItems() {
		Component depositBox = itemsData.getComponent();
		
		if (depositBox != null) {
			Array1D<Component> items = depositBox.getComponents();
			
			if (items == null)
				return null;
			
			if (items.getLength() != 29) {
				System.out.println("Error: DepositBox.getItems() does not have 29 children!");
				return null;
			}
			
			return items.getAll();
		}
		
		return null;
	}
	
	public int count() {
		return count(null);
	}
	
	public int count(int... exceptionIds) {
		Component[] items = getItems();
			
		if (items != null) {
			int count = 0;
			
			for (Component item : items) {
				if (item != null) {
					int componentId = item.getComponentId();
					
					if (componentId != -1) {
						boolean exception = false;
						
						if (exceptionIds != null) {
							for (int exceptionId : exceptionIds) {
								if (componentId == exceptionId) {
									exception = true;
									break;
								}
							}
						}
						
						if (!exception)
							count++;
					}
				}
			}
			
			return count;
		}
		
		return -1;
	}
	
	// TODO: add deposit all component support
	public boolean depositAllItems() {
		return depositAllItems(null);
	}
	
	public boolean depositAllItems(int... exceptionIds) {
		if (isClosed())
			return false;
		
		// Internal exceptions
		if (exceptionIds == null || exceptionIds.length == 0)
			exceptionIds = Interfaces.internalItemExceptionIds;
		else
			exceptionIds = ArrayUtils.combine(exceptionIds, Interfaces.internalItemExceptionIds);
		
		if (count(exceptionIds) == 0)
			return true;
		
		Timer timer = new Timer(25000);
		while (timer.isRunning()) {
			if (isClosed())
				return false;
			
			Component[] items = getItems();
			
			if (items == null) {
				Time.sleep(50);
				continue;
			}
			
			int itemCountInclExceptions = 0;
			Map<Integer, Integer> idsToDeposit = new HashMap<>();
			
			// add ids to hashmap
			for (int i = 0; i < items.length; i++) {
				int componentId = items[i].getComponentId();
				if (componentId != -1) {
					itemCountInclExceptions++;
					
					boolean exception = false;
					if (exceptionIds != null) {
						for (int exceptionId : exceptionIds) {
							if (componentId == exceptionId) {
								exception = true;
								break;
							}
						}
					}
					
					if (!exception)
						idsToDeposit.put(i, componentId);
				}
			}
			
			boolean depositAll = false;
			if (idsToDeposit.size() == itemCountInclExceptions) {
				if (Random.nextInt(0, 100) < 90)
					depositAll = true;
			}
			
			if (depositAll) {
				Component depositAllButton = depositCarriedItemsButtonData.getComponent();
				
				if (depositAllButton != null) {
					depositAllButton.interact("Deposit carried items");
					Time.sleepQuick();
				}
			} else {
				List<Integer> keySet = new ArrayList<>(idsToDeposit.keySet());
				Collections.shuffle(keySet); // shuffles all non-exception item indexes
				
				List<Integer> depositedIds = new ArrayList<>(); // used in loop to only allow one item of the same id to be deposited through deposit-all
				
				boolean originalAntiban = Component.interactionAntiban;
				Component.interactionAntiban = false;
				
				// deposit non-duplicate items in random order
				for (int i : keySet) {
					int componentId = idsToDeposit.get(i);
					if (!depositedIds.contains(componentId)) {
						boolean containsDepositAll = false;
						String[] actions = items[i].getActions();
						if (actions == null)
							continue;
						
						for (String action : actions) {
							if (action.contains("Deposit-All"))
								containsDepositAll = true;
						}
						
						if (containsDepositAll)
							items[i].interact("Deposit-All");
						else
							items[i].interact("Deposit");
						
						Time.sleepQuick();
						
						depositedIds.add(componentId);
					}
				}
				
				Component.interactionAntiban = originalAntiban;
			}
			
			Time.sleepMedium();
			
			if (count(exceptionIds) == 0)
				return true;
		}
		
		return false;
	}
	
}
