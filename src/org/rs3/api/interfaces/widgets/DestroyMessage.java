package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Time;

public class DestroyMessage extends ClosableWidget {
	
	public static final DestroyMessage get = new DestroyMessage(1183);
	
	public static final ComponentData yesButtonData = new ComponentData("Yes Button", 1183, 6);
	
	// 9 Text: Are you sure you want to destroy this token?<br>You cannot reclaim it.
	
	// 6 Tooltip ("Yes" Button): Destroy
	// 7 Tooltip ("No" Button): Stop
	// 16 Text: Yes
	// 20 Text: No
	
	// 4 Text: Key token
	// 12 Text: Are you sure you want to destroy this object?
	
	public DestroyMessage(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component destroyMessage = yesButtonData.getComponent();
		return destroyMessage != null && destroyMessage.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		return chooseYes(waitAfter);
	}
	
	public boolean chooseYes() {
		return chooseYes(false);
	}
	
	public boolean chooseYes(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component yesButton = yesButtonData.getComponent();
		if (yesButton != null && yesButton.isVisible()) {
			boolean result = yesButton.click(Mouse.LEFT);
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
}
