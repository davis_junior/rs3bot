package org.rs3.api.interfaces.widgets;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class ProcessingItemsWidget extends ClosableWidget {
	
	public static final ProcessingItemsWidget get = new ProcessingItemsWidget(1251);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1251, -1); // TODO: done button, though it closes automatically after a couple seconds
	
	public ProcessingItemsWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1251);
		
		if (widget != null && widget.validate(1251)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close"); // TODO: action might be "Done"
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
