package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.Openable;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;

public class MagicAbilitiesSubInterfaceTabWidget extends WidgetData implements Openable {
	
	public static final MagicAbilitiesSubInterfaceTabWidget get = new MagicAbilitiesSubInterfaceTabWidget(1459);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1459, 1);
	public static final ComponentData tabAbilitiesButtonData = new ComponentData("Abilities Tab Button", 1459, 7, 7);
	public static final ComponentData tabCombatButtonData = new ComponentData("Combat Tab Button", 1459, 7, 8);
	public static final ComponentData tabTeleportButtonData = new ComponentData("Teleport Tab Button", 1459, 7, 9);
	public static final ComponentData tabSkillingButtonData = new ComponentData("Skilling Tab Button", 1459, 7, 10);

	
	public MagicAbilitiesSubInterfaceTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1459);
		
		if (widget != null && widget.validate(1459)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return SubInterfaceTab.POWERS_MAGIC.open(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
