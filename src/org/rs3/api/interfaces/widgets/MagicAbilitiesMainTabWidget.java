package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;

public class MagicAbilitiesMainTabWidget extends OpenableClosableWidget {
	
	public static final MagicAbilitiesMainTabWidget get = new MagicAbilitiesMainTabWidget(1461);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1461, 1);
	public static final ComponentData tabAbilitiesButtonData = new ComponentData("Abilities Tab Button", 1461, 7, 7);
	public static final ComponentData tabCombatButtonData = new ComponentData("Combat Tab Button", 1461, 7, 8);
	public static final ComponentData tabTeleportButtonData = new ComponentData("Teleport Tab Button", 1461, 7, 9);
	public static final ComponentData tabSkillingButtonData = new ComponentData("Skilling Tab Button", 1461, 7, 10);

	
	public MagicAbilitiesMainTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainMagicAbilitiesBounds = MainWidget.tabMagicAbilitiesBounds.getComponent();
		return mainMagicAbilitiesBounds != null && mainMagicAbilitiesBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.MAGIC_ABILITIES.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.MAGIC_ABILITIES.close(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
