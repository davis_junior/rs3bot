package org.rs3.api.interfaces.widgets;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Calculations;
import org.rs3.api.Interfaces;
import org.rs3.api.GroundObjects;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.Widget;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Settings_GameplaySubTabButtonsWidget extends WidgetData {
	
	public static final Settings_GameplaySubTabButtonsWidget get = new Settings_GameplaySubTabButtonsWidget(1443);
	
	/*
	 * We must override the width, height of all sub tab buttons to use scrollbar thumb sizes
	 */
	public static final ComponentData gameplaySettingsButtonData = new ComponentData("Gameplay Settings Button", 1443, 9);
	public static final ComponentData lootSettingsButtonData = new ComponentData("Loot Settings Button", 1443, 18);
	public static final ComponentData deathsStoreSettingsButtonData = new ComponentData("Death's Store Settings Button", 1443, 27);
	public static final ComponentData playerOwnedHouseSettingsButtonData = new ComponentData("Player Owned House Settings Button", 1443, 36);
	public static final ComponentData actionBarSettingsButtonData = new ComponentData("Action Bar Settings Button", 1443, 45);
	public static final ComponentData doomsayerWarningSettingsButtonData = new ComponentData("Doomsayer Warning Settings Button", 1443, 56);
	public static final ComponentData miscellaneousSettingsButtonData = new ComponentData("Miscellaneous Settings Button", 1443, 64);
	public static final ComponentData aidSettingsButtonData = new ComponentData("Aid Settings Button", 1443, 73);

	
	public Settings_GameplaySubTabButtonsWidget(int widgetId) {
		super(widgetId);
	}
	
	
	public static enum GameplaySettingsSubTab {
		GAMEPLAY_SETTINGS(0, gameplaySettingsButtonData),
		LOOT_SETTINGS(1, lootSettingsButtonData),
		DEATH_STORE_SETTINGS(2, deathsStoreSettingsButtonData),
		PLAYER_OWNED_HOUSE_SETTINGS(3, playerOwnedHouseSettingsButtonData),
		ACTION_BAR_SETTINGS(4, actionBarSettingsButtonData),
		DOOM_SAYER_WARNING_SETTINGS(5, doomsayerWarningSettingsButtonData),
		MISCELLANEOUS_SETTINGS(6, miscellaneousSettingsButtonData),
		AID_SETTINGS(7, aidSettingsButtonData),
		;
		
		int settingValue;
		private ComponentData buttonData;
		
		private GameplaySettingsSubTab(int settingValue, ComponentData buttonData) {
			this.settingValue = settingValue;
			this.buttonData = buttonData;
		}
		
		public Component getButton() {
			if (Settings_GameplaySubTabButtonsWidget.get.isClosed())
				return null;
			
			Component button = buttonData.getComponent();
			if (button != null) {
				button.setWidth(button.getHorizontalScrollbarThumbSize());
				button.setHeight(button.getVerticalScrollbarThumbSize());
			}
			
			return button;
		}
		
		public boolean isLastSelected() {
			int tabSetting = Client.getSettingData().getSettings().get(5839, 0x7);
			return tabSetting == this.settingValue;
		}
		
		public boolean select() {
			return select(false);
		}
		
		// TODO: implement scrollbar
		public boolean select(boolean waitAfter) {
			//if (Settings_GameplaySubTabButtonsWidget.get.isClosed())
			//	return false;
			
			if (SubInterfaceTab.SETTINGS_GAMEPLAY.open(true)) {
				Time.sleepQuick();
				
				if (isLastSelected())
					return true;
				
				Component button = getButton();
				if (button != null) { // visibility always false
					boolean result = button.interact("Select");
					if (waitAfter)
						Time.sleepQuick();
					return result;
				}
			}
			
			return false;
		}
		
		public static GameplaySettingsSubTab getLastSelected() {
			for (GameplaySettingsSubTab tab : values()) {
				if (tab.isLastSelected())
					return tab;
			}
			
			return null;
		}
	}
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1443);
		
		if (widget != null && widget.validate(1443)) {
			// widget validation is all that is needed
			//Component collectionBox = closeButtonData.getComponent();
			//return collectionBox != null && collectionBox.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
}
