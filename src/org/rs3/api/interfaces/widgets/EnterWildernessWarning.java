package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class EnterWildernessWarning extends ClosableWidget {
	
	public static final EnterWildernessWarning get = new EnterWildernessWarning(382);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 382, 14);
	public static final ComponentData enterButtonData = new ComponentData("Enter Wilderness Button", 382, 21);
	public static final ComponentData dontEnterButtonData = new ComponentData("Don't Enter Button", 382, 22);
	public static final ComponentData headerData = new ComponentData("Header", 382, 25);
	public static final ComponentData messageData = new ComponentData("Message", 382, 26);
	
	// 25 Text: WARNING!
	
	// 21 Text: Enter Wilderness
	// 22 Text: Don't Enter
	
	public EnterWildernessWarning(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(382);
		
		if (widget != null && widget.validate(382)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean chooseEnter() {
		return chooseEnter(false);
	}
	
	public boolean chooseEnter(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component yesButton = enterButtonData.getComponent();
		if (yesButton != null && yesButton.isVisible()) {
			boolean result = yesButton.interact("Proceed");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public boolean chooseDontEnter() {
		return chooseDontEnter(false);
	}
	
	public boolean chooseDontEnter(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component noButton = dontEnterButtonData.getComponent();
		if (noButton != null && noButton.isVisible()) {
			boolean result = noButton.interact("Cancel");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public String getHeader() {
		if (isOpen()) {
			Component headerComp = headerData.getComponent();
			if (headerComp != null)
				return headerComp.getText();
		}
		
		return null;
	}
	
	public String getMessage() {
		if (isOpen()) {
			Component msgComp = messageData.getComponent();
			if (msgComp != null)
				return msgComp.getText();
		}
		
		return null;
	}
}
