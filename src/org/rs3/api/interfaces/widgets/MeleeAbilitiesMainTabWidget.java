package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;

public class MeleeAbilitiesMainTabWidget extends OpenableClosableWidget {
	
	public static final MeleeAbilitiesMainTabWidget get = new MeleeAbilitiesMainTabWidget(1460);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1460, 1);
	public static final ComponentData tabAttackButtonData = new ComponentData("Attack Tab Button", 1460, 5, 7);
	public static final ComponentData tabSrengthButtonData = new ComponentData("Strength Tab Button", 1460, 5, 8);

	
	public MeleeAbilitiesMainTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainMeleeAbilitiesBounds = MainWidget.tabMeleeAbilitiesBounds.getComponent();
		return mainMeleeAbilitiesBounds != null && mainMeleeAbilitiesBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.MELEE_ABILITIES.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.MELEE_ABILITIES.close(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
