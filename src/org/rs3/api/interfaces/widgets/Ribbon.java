package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Ribbon extends WidgetData {
	
	public static final Ribbon get = new Ribbon(1431);
	
	// all have misleading "Close" as action
	public static final ComponentData heroButtonData = new ComponentData("Hero Button", 1431, 7);
	public static final ComponentData customisationsButtonData = new ComponentData("Customisations Button", 1431, 8);
	public static final ComponentData adventuresButtonData = new ComponentData("Adventures Button", 1431, 9);
	public static final ComponentData powersButtonData = new ComponentData("Powers Button", 1431, 10);
	public static final ComponentData communityButtonData = new ComponentData("Community Button", 1431, 11);
	public static final ComponentData extrasButtonData = new ComponentData("Extras Button", 1431, 13);
	//public static final ComponentData groupingSystemButtonData = new ComponentData("Grouping System Button", 1431, 14);
	public static final ComponentData christmas2015ButtonData = new ComponentData("Christmas 2015 Button", 1431, 14);
	public static final ComponentData settingsButtonData = new ComponentData("Settings Button", 1431, 12);
	
	// ? Action: 
	// ? Text: 
	
	public Ribbon(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component ribbonBounds = MainWidget.ribbonBoundsData.getComponent();
		return ribbonBounds != null && ribbonBounds.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	
	public enum SubInterface {
		HERO("Hero", heroButtonData, 0), 
		CUSTOMISATIONS("Customisations", customisationsButtonData, 1), 
		ADVENTURES("Adventures", adventuresButtonData, 3), 
		POWERS("Powers", powersButtonData, 2), 
		COMMUNITY("Community", communityButtonData, 4), 
		EXTRAS("Upgrades & Extras", extrasButtonData, 7), 
		//GROUPING_SYSTEM("Community", groupingSystemButtonData, 4), // was also Christmas 2015, there is no grouping system sub interface only a tab in Community 
		CHRISTMAS_2015("Christmas 2015", christmas2015ButtonData, 9),
		SETTINGS("Settings", settingsButtonData, 8),
		
		GRAND_EXCHANGE("Grand Exchange", null, 5),
		;
		
		String name;
		ComponentData buttonData;
		int settingValue;
		
		private SubInterface(String name, ComponentData buttonData, int settingValue) {
			this.name = name;
			this.buttonData = buttonData;
			this.settingValue = settingValue;
		}
		
		public String getName() {
			return name;
		}
		
		public boolean isLastOpened() {
			int lastOpenSubInterface = Client.getSettingData().getSettings().get(3708, 0, 0xF);
			return lastOpenSubInterface == settingValue;
		}
		
		public boolean isOpen() {
			return isLastOpened() && MainWidget.get.isSubInterfaceOpen();
		}
		
		public boolean isClosed() {
			return !isOpen();
		}
		
		public boolean hover() {
			Component button = this.buttonData.getComponent();
			return button != null && button.isVisible() && button.hover();
		}
		
		public boolean open() {
			return open(false);
		}
		
		public boolean open(boolean waitAfter) {
			if (isOpen())
				return true;
			
			// close other subinterface if opened
			if (MainWidget.get.isSubInterfaceOpen()) {
				MainWidget.get.closeSubInterface(true);
				Time.sleepQuick();
			}
			
			if (this.equals(GRAND_EXCHANGE))
				return GrandExchangeWidget.get.open(waitAfter);	
			
			Component button = this.buttonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Close"); // "Close" action also opens sub interface for some reason, can also be used to close if open
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
			
			return false;
		}
		
		public boolean close() {
			return close(false);
		}
		
		public boolean close(boolean waitAfter) {
			if (isClosed())
				return true;
			
			if (isOpen())
				return MainWidget.get.closeSubInterface(waitAfter);
			
			return false;
		}
		
		public static SubInterface getLastOpened() {
			for (SubInterface subInterface : values()) {
				if (subInterface.isLastOpened())
					return subInterface;
			}
			
			return null;
		}
		
		public static SubInterface getOpened() {
			if (MainWidget.get.isSubInterfaceOpen()) {
				for (SubInterface subInterface : values()) {
					if (subInterface.isLastOpened()) // we check if sub interface is opened before this
						return subInterface;
				}
			}
			
			return null;
		}
	}
	
	public enum SubInterfaceTab {
		
		HERO_SUMMARY("Summary", SubInterface.HERO, MainWidget.subInterfaceTabButton1Data, 3708, 9, 1),
		HERO_SKILLS("Skills", SubInterface.HERO, MainWidget.subInterfaceTabButton2Data, 3708, 9, 2),
		HERO_LOADOUT("Loadout", SubInterface.HERO, MainWidget.subInterfaceTabButton3Data, 3708, 9, 3),
		HERO_PRESETS("Presets", SubInterface.HERO, MainWidget.subInterfaceTabButton4Data, 3708, 9, 4),
		HERO_ACHIEVEMENTS("Achievements", SubInterface.HERO, MainWidget.subInterfaceTabButton5Data, 3708, 9, 5),
		HERO_COMPLETIONIST("Completionist", SubInterface.HERO, MainWidget.subInterfaceTabButton6Data, 3708, 9, 6),
		
		CUSTOMISATIONS_WARDROBE("Wardrobe", SubInterface.CUSTOMISATIONS, MainWidget.subInterfaceTabButton1Data, 3708, 13, 1),
		CUSTOMISATIONS_ANIMATIONS("Animations", SubInterface.CUSTOMISATIONS, MainWidget.subInterfaceTabButton3Data, 3708, 13, 3),
		CUSTOMISATIONS_APPEARANCE("Appearance", SubInterface.CUSTOMISATIONS, MainWidget.subInterfaceTabButton4Data, 3708, 13, 4),
		CUSTOMISATIONS_TITLES("Titles", SubInterface.CUSTOMISATIONS, MainWidget.subInterfaceTabButton5Data, 3708, 13, 5),
		CUSTOMISATIONS_PETS("Pets", SubInterface.CUSTOMISATIONS, MainWidget.subInterfaceTabButton6Data, 3708, 13, 6),
		
		ADVENTURES_FEATURED("Featured", SubInterface.ADVENTURES, MainWidget.subInterfaceTabButton1Data, 3708, 21, 1),
		ADVENTURES_QUESTS("Quests", SubInterface.ADVENTURES, MainWidget.subInterfaceTabButton2Data, 3708, 21, 2),
		ADVENTURES_CHALLENGES("Challenges", SubInterface.ADVENTURES, MainWidget.subInterfaceTabButton3Data, 3708, 21, 3),
		ADVENTURES_MINIGAMES("Minigames", SubInterface.ADVENTURES, MainWidget.subInterfaceTabButton4Data, 3708, 21, 4),
		ADVENTURES_PATH("Path", SubInterface.ADVENTURES, MainWidget.subInterfaceTabButton5Data, 3708, 21, 5),
		ADVENTURES_BEASTS("Beasts", SubInterface.ADVENTURES, MainWidget.subInterfaceTabButton6Data, 3708, 21, 6),
		
		POWERS_MELEE("Melee", SubInterface.POWERS, MainWidget.subInterfaceTabButton1Data, 3708, 17, 1),
		POWERS_RANGED("Ranged", SubInterface.POWERS, MainWidget.subInterfaceTabButton2Data, 3708, 17, 2),
		POWERS_MAGIC("Magic", SubInterface.POWERS, MainWidget.subInterfaceTabButton3Data, 3708, 17, 3),
		POWERS_DEFENSIVE("Defensive", SubInterface.POWERS, MainWidget.subInterfaceTabButton4Data, 3708, 17, 4),
		POWERS_PRAYERS("Prayers", SubInterface.POWERS, MainWidget.subInterfaceTabButton5Data, 3708, 17, 5),
		POWERS_COMBAT_SETTINGS("Combat Settings", SubInterface.POWERS, MainWidget.subInterfaceTabButton6Data, 3708, 17, 6),
		
		COMMUNITY_VOTE_NOW("Vote Now!", SubInterface.COMMUNITY, MainWidget.subInterfaceTabButton1Data, 3708, 25, 1),
		COMMUNITY_HISCORES("Hiscores", SubInterface.COMMUNITY, MainWidget.subInterfaceTabButton2Data, 3708, 25, 2),
		COMMUNITY_CHAT_SETTINGS("Chat Settings", SubInterface.COMMUNITY, MainWidget.subInterfaceTabButton3Data, 3708, 25, 3),
		COMMUNITY_GROUPING_SYSTEM("Grouping System", SubInterface.COMMUNITY, MainWidget.subInterfaceTabButton4Data, 3708, 25, 4),
		COMMUNITY_TWITCH("Twitch", SubInterface.COMMUNITY, MainWidget.subInterfaceTabButton5Data, 3708, 25, 5),
		
		EXTRAS_OVERVIEW("Overview", SubInterface.EXTRAS, MainWidget.subInterfaceTabButton1Data, 3709, 8, 1), // bits to shift is either 8 or 12 (likely 8)
		
		CHRISTMAS_2015_CHRISTMAS("Christmas", SubInterface.CHRISTMAS_2015, MainWidget.subInterfaceTabButton1Data, 3709, 16, 1),
		CHRISTMAS_2015_ADVENT_CALENDAR("Advent Calendar", SubInterface.CHRISTMAS_2015, MainWidget.subInterfaceTabButton2Data, 3709, 16, 2),
		CHRISTMAS_2015_QUEST_EPISODES("Quest Episodes", SubInterface.CHRISTMAS_2015, MainWidget.subInterfaceTabButton3Data, 3709, 16, 3),
		CHRISTMAS_2015_WINTER_WEEKENDS("Winter Weekends", SubInterface.CHRISTMAS_2015, MainWidget.subInterfaceTabButton4Data, 3709, 16, 4),
		CHRISTMAS_2015_COMMUNITY("Community", SubInterface.CHRISTMAS_2015, MainWidget.subInterfaceTabButton5Data, 3709, 16, 5),
		
		SETTINGS_GAMEPLAY("Gameplay", SubInterface.SETTINGS, MainWidget.subInterfaceTabButton1Data, 3709, 4, 1),
		SETTINGS_INTERFACE("Interface", SubInterface.SETTINGS, MainWidget.subInterfaceTabButton2Data, 3709, 4, 2),
		SETTINGS_CONTROLS("Controls", SubInterface.SETTINGS, MainWidget.subInterfaceTabButton3Data, 3709, 4, 3),
		SETTINGS_GRAPHICS("Graphics", SubInterface.SETTINGS, MainWidget.subInterfaceTabButton4Data, 3709, 4, 4),
		SETTINGS_AUDIO("Audio", SubInterface.SETTINGS, MainWidget.subInterfaceTabButton5Data, 3709, 4, 5),
		SETTINGS_COMBAT_SETTINGS("Combat Settings", SubInterface.SETTINGS, MainWidget.subInterfaceTabButton6Data, 3709, 4, 6),
		
		GE_GRAND_EXCHANGE("Grand Exchange", SubInterface.GRAND_EXCHANGE, MainWidget.subInterfaceTabButton1Data, 3709, 0, 1),
		GE_SALE_HISTORY("Sale History", SubInterface.GRAND_EXCHANGE, MainWidget.subInterfaceTabButton2Data, 3709, 0, 2),
		;
		
		String name;
		SubInterface subInterface;
		ComponentData tabButtonData;
		int settingId;
		int bitsToShift;
		int settingValue;
		
		private SubInterfaceTab(String name, SubInterface subInterface, ComponentData tabButtonData, int settingId, int bitsToShift, int settingValue) {
			this.name = name;
			this.subInterface = subInterface;
			this.tabButtonData = tabButtonData;
			this.settingId = settingId;
			this.bitsToShift = bitsToShift;
			this.settingValue = settingValue;
		}
		
		public boolean isLastSelected() {
			int lastSelectedTab = Client.getSettingData().getSettings().get(settingId, bitsToShift, 0x7);
			return lastSelectedTab == settingValue;
		}
		
		public boolean isOpen() {
			return subInterface.isOpen() && isLastSelected();
		}
		
		public boolean open() {
			return open(false);
		}
		
		public boolean open(boolean waitAfter) {
			if (isOpen())
				return true;
			
			if (!subInterface.isOpen() && this.equals(SubInterfaceTab.POWERS_COMBAT_SETTINGS)) {
				if (!ActionBarWidget.get.interactCombatSettingsButton("Combat settings", true))
					return false;
				Time.sleepQuick();
			}
			
			if (!subInterface.isOpen() && this.equals(SubInterfaceTab.SETTINGS_COMBAT_SETTINGS)) {
				OptionsMenuWidget.get.close(true);
				
				// this brings you to the Gameplay tab but the rest of the code will select the correct tab
				if (!ActionBarWidget.get.interactCombatSettingsButton("Action bar settings", true))
					return false;
				Time.sleepMedium();
			}
			
			if ((this.equals(SETTINGS_COMBAT_SETTINGS) && subInterface.isOpen())
					|| (!this.equals(SETTINGS_COMBAT_SETTINGS) && subInterface.open(true))
					|| (subInterface.equals(SubInterface.SETTINGS) && !this.equals(SETTINGS_COMBAT_SETTINGS) && OptionsMenuWidget.get.isOpen())) {
				Time.sleepQuick();
				
				if ((!subInterface.isOpen() && subInterface.equals(SubInterface.SETTINGS) && !this.equals(SETTINGS_COMBAT_SETTINGS))
						|| (subInterface.equals(SubInterface.SETTINGS) && !this.equals(SETTINGS_COMBAT_SETTINGS) && OptionsMenuWidget.get.isOpen())) {
					if (OptionsMenuWidget.get.open(true)) {
						Time.sleepQuick();
						
						switch (this) {
						case SETTINGS_GAMEPLAY:
							return OptionsMenuWidget.get.clickButton(OptionsMenuWidget.gameSettingsButtonData, "Select", waitAfter);
						case SETTINGS_INTERFACE:
							return OptionsMenuWidget.get.clickButton(OptionsMenuWidget.interfaceSettingsButtonData, "Select", waitAfter);
						case SETTINGS_CONTROLS:
							return OptionsMenuWidget.get.clickButton(OptionsMenuWidget.controlsButtonData, "Select", waitAfter);
						case SETTINGS_GRAPHICS:
							return OptionsMenuWidget.get.clickButton(OptionsMenuWidget.graphicsSettingsButtonData, "Select", waitAfter);
						case SETTINGS_AUDIO:
							return OptionsMenuWidget.get.clickButton(OptionsMenuWidget.audioSettingsButtonData, "Select", waitAfter);
						//case SETTINGS_COMBAT_SETTINGS: // handles above
						//	break;
						default:
							System.out.println("SubInterface open() unknown tab: " + this);
							return false;
						}
					} else
						return false;
				}
				
				if (subInterface.isOpen()) {
					if (isLastSelected())
						return true;
					
					Component tabButton = tabButtonData.getComponent();
					if (tabButton != null && tabButton.isVisible()) {
						boolean result = tabButton.interact(name);
						if (waitAfter)
							Time.sleepQuick();
						return result;
					}
				}
			}
			
			return false;
		}
		
		public static SubInterfaceTab getLastSelected() {
			for (SubInterfaceTab tab : values()) {
				if (tab.isLastSelected())
					return tab;
			}
			
			return null;
		}
		
		public static SubInterfaceTab getOpened() {
			if (MainWidget.get.isSubInterfaceOpen()) {
				for (SubInterfaceTab tab : values()) {
					if (tab.isOpen())
						return tab;
				}
			}
			
			return null;
		}
	}
	
	public enum Tab {
		SKILLS("Skills", SubInterface.HERO, MainWidget.tabSkillsBounds, MainWidget.tabSkillsCloseButton), 
		BACKPACK("Backpack", SubInterface.HERO, MainWidget.tabBackpackBounds, MainWidget.tabBackPackCloseButton), 
		WORN_EQUIPMENT("Worn Equipment", SubInterface.HERO, MainWidget.tabWornEquipmentBounds, MainWidget.tabWornEquipmentCloseButton), 
		
		ACTIVE_TASK("Active Task", SubInterface.ADVENTURES, MainWidget.tabActiveTaskBounds, MainWidget.tabActiveTaskCloseButton), 

		PRAYERS("Prayer Abilities", SubInterface.POWERS, MainWidget.tabPrayersBounds, MainWidget.tabPrayersCloseButton), 
		MAGIC_ABILITIES("Magic Abilities", SubInterface.POWERS, MainWidget.tabMagicAbilitiesBounds, MainWidget.tabMagicAbilitiesCloseButton), 
		MELEE_ABILITIES("Melee Abilities", SubInterface.POWERS, MainWidget.tabMeleeAbilitiesBounds, MainWidget.tabMeleeAbilitiesCloseButton), 
		RANGED_ABILITIES("Ranged Abilities", SubInterface.POWERS, MainWidget.tabRangedAbilitiesBounds, MainWidget.tabRangedAbilitiesCloseButton), 
		DEFENSE_ABILITIES("Defence Abilities", SubInterface.POWERS, MainWidget.tabDefenseAbilitiesBounds, MainWidget.tabDefenseAbilitiesCloseButton), 
		
		FRIENDS("Friends", SubInterface.COMMUNITY, MainWidget.tabFriendsBounds, MainWidget.tabFriendsCloseButton), 
		FRIENDS_CHAT_INFO("Friends Chat Info", SubInterface.COMMUNITY, MainWidget.tabFriendsChatInfoBounds, MainWidget.tabFriendsChatInfoCloseButton), 
		CLAN("Clan", SubInterface.COMMUNITY, MainWidget.tabClanBounds, MainWidget.tabClanCloseButton), 
		GROUP_MEMBERS("Group Members", SubInterface.COMMUNITY, MainWidget.tabGroupMembersBounds, MainWidget.tabGroupMembersCloseButton), 
		// TODO: TWITCH_STREAM, 
		
		NOTES("Notes", SubInterface.SETTINGS, MainWidget.tabNotesBounds, MainWidget.tabNotesCloseButton), 
		MUSIC_PLAYER("Music Player", SubInterface.SETTINGS, MainWidget.tabMusicPlayerBounds, MainWidget.tabMusicPlayerCloseButton);
		
		String componentName;
		SubInterface subInterface;
		ComponentData tabBounds;
		ComponentData closeButton;
		
		private Tab(String componentName, SubInterface subInterface, ComponentData tabBounds, ComponentData closeButton) {
			this.componentName = componentName;
			this.subInterface = subInterface;
			this.tabBounds = tabBounds;
			this.closeButton = closeButton;
		}
		
		public boolean isOpen() {
			Component tabBounds = this.tabBounds.getComponent();
			return tabBounds != null && tabBounds.isVisible();
		}
		
		public boolean isClosed() {
			return !isOpen();
		}
		
		public boolean open() {
			return open(false);
		}
		
		public boolean open(boolean waitAfter) {
			if (isOpen())				
				return true;
			
			if (subInterface.hover()) {
				Time.sleepQuickest();
				
				// Wait for tab button component data to be updated (usually takes < 150ms)
				// HoveredRibbonTabs.get.isOpen() should not be used here
				Timer timer = new Timer(1000);
				while (timer.isRunning()) {
					Component tabButton = HoveredRibbonTabs.tab1ButtonData.getComponent();
					if (tabButton != null && tabButton.isVisible())
						break;
					
					Time.sleep(50);
				}
				Time.sleepQuickest();
				
				if (HoveredRibbonTabs.get.isOpen()) {
					ComponentData[] tabButtons = new ComponentData[5];
					tabButtons[0] = HoveredRibbonTabs.tab1ButtonData;
					tabButtons[1] = HoveredRibbonTabs.tab2ButtonData;
					tabButtons[2] = HoveredRibbonTabs.tab3ButtonData;
					tabButtons[3] = HoveredRibbonTabs.tab4ButtonData;
					tabButtons[4] = HoveredRibbonTabs.tab5ButtonData;
					
					for (ComponentData tabButtonData : tabButtons) {
						Component button = tabButtonData.getComponent();
						if (button != null && button.isVisible()) { // visibility is checked previously
							if (this.componentName.equals(button.getComponentName().trim())) {
								boolean clicked = button.click(Mouse.LEFT);
								
								if (clicked && Random.nextInt(0, 100) < 99) {
									Time.sleepVeryQuick();
									Antiban.randomMoveMouse();
								}
								
								if (waitAfter)
									Time.sleepQuick();
								
								return clicked;
							}
						}
					}
				}
			}
			
			return false;
		}
		
		public boolean close() {
			return close(false);
		}
		
		public boolean close(boolean waitAfter) {
			if (isClosed())
				return true;
			
			Component button = closeButton.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact("Close Window");
				
				if (result && Random.nextInt(0, 100) < 99) {
					Time.sleepVeryQuick();
					Antiban.randomMoveMouse();
				}
				
				if (waitAfter)
					Time.sleepQuick();
				
				return result;
			}
			
			return false;
		}
		
		public static boolean closeAll() {
			return closeAll((Tab[]) null);
		}
		
		public static boolean closeAll(Tab...exceptions) {
			boolean result = true;
			
			try {
				Component.interactionAntiban = false;
				
				for (Tab tab : Tab.values()) {
					if (exceptions != null) {
						boolean skip = false;
						for (Tab exception : exceptions) {
							if (tab.equals(exception)) {
								skip = true;
								break;
							}
						}
						
						if (skip)
							continue;
					}
					
					if (tab.isOpen()) {
						if (!tab.close(true))
							result = false;
						Time.sleepQuick();
					}
				}
			} finally {
				Component.interactionAntiban = true;
			}
			
			return result;
		}
	}
}
