package org.rs3.api.interfaces.widgets;

import java.awt.event.KeyEvent;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class AccountCreateWidget extends WidgetData {
	
	public static final AccountCreateWidget get = new AccountCreateWidget(1420);
	
	//
	// all components - visible always false, except checkboxes
	//

	// Design screen
	public static final ComponentData femaleButtonData = new ComponentData("Female Button", 1420, 18); // Action: Female
	public static final ComponentData maleButtonData = new ComponentData("Male Button", 1420, 19); // Action: Male
	public static final ComponentData randomiseButtonData = new ComponentData("Randomise Button", 1420, 102); // Actions: Randomise, Advanced gameplay modes
	public static final ComponentData doneButtonData = new ComponentData("Done Button", 1420, 159); // Action: null, Text: null
	
	// Enlist screen
	public static final ComponentData sendMeNewsCheckboxData = new ComponentData("Send Me News Checkbox", 1420, 97); // hidden flag true when not checked, false when checked
	public static final ComponentData showPasswordCheckboxData = new ComponentData("Show Password Checkbox", 1420, 198); // hidden flag true when not checked, false when checked
	public static final ComponentData emailAddressTextboxData = new ComponentData("Email Address Textbox", 1420, 5);
	public static final ComponentData passwordTextboxData = new ComponentData("Password Textbox", 1420, 258);
	public static final ComponentData ageTextboxData = new ComponentData("Age Textbox", 1420, 265);
	public static final ComponentData characterNameTextboxData = new ComponentData("Character Name Textbox", 1420, 272);
	public static final ComponentData backButtonData = new ComponentData("Back Button", 1420, 45); // Action: null, Text: null
	public static final ComponentData randomiseNameButtonData = new ComponentData("Randomise Name Button", 1420, 208); // Action: null, Text: null
	public static final ComponentData playNowButtonData = new ComponentData("Play Now Button", 1420, 452); // Action: null, Text: null
	
	// Both screens
	public static final ComponentData screenTitleData = new ComponentData("Screen Title", 1420, 158); // Text: design your hero, enlist your hero
	
	// ? Text:
	
	// Action: null
	// Text: 
	
	public AccountCreateWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1420);
		
		if (widget != null && widget.validate(1420)) {
			// widget validation is all that is needed
			//Component loginScreen = loginButtonData.getComponent();
			//return loginScreen != null && loginScreen.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	public String getScreenTitleText() {
		if (isClosed())
			return null;
		
		Component screenTitle = screenTitleData.getComponent();
		if (screenTitle != null && screenTitle.validate())
			return screenTitle.getText();
		
		return null;
	}
	
	public boolean isDesignScreenVisible() {
		if (isClosed())
			return false;
		
		String text = getScreenTitleText();
		if (text != null)
			return text.trim().toLowerCase().equals("design your hero");
		
		return false;
	}
	
	public boolean isEnlistScreenVisible() {
		if (isClosed())
			return false;
		
		String text = getScreenTitleText();
		if (text != null)
			return text.trim().toLowerCase().equals("enlist your hero");
		
		return false;
	}
	
	public boolean clickFemaleButton() {
		return clickFemaleButton(false);
	}
	
	public boolean clickFemaleButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isDesignScreenVisible()) {
			Component femaleButton = femaleButtonData.getComponent();
			if (femaleButton != null) {
				boolean clicked = femaleButton.interact("Female");
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public boolean clickMaleButton() {
		return clickMaleButton(false);
	}
	
	public boolean clickMaleButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isDesignScreenVisible()) {
			Component maleButton = maleButtonData.getComponent();
			if (maleButton != null) {
				boolean clicked = maleButton.interact("Male");
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public boolean clickRandomiseButton() {
		return clickRandomiseButton(false);
	}
	
	public boolean clickRandomiseButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isDesignScreenVisible()) {
			Component randomiseButton = randomiseButtonData.getComponent();
			if (randomiseButton != null) {
				boolean clicked = randomiseButton.interact("Randomise");
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public boolean clickDoneButton() {
		return clickDoneButton(false);
	}
	
	public boolean clickDoneButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isDesignScreenVisible()) {
			Component doneButton = doneButtonData.getComponent();
			if (doneButton != null) {
				boolean clicked = doneButton.click(Mouse.LEFT);
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	
	public boolean isSendMeNewsChecked() {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component sendMeNewsCheckbox = sendMeNewsCheckboxData.getComponent();
			if (sendMeNewsCheckbox != null && sendMeNewsCheckbox.validate())
				return !sendMeNewsCheckbox._isHidden();
		}
		
		return false;
	}
	
	public boolean isShowPasswordChecked() {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component showPasswordCheckbox = showPasswordCheckboxData.getComponent();
			if (showPasswordCheckbox != null && showPasswordCheckbox.validate())
				return !showPasswordCheckbox._isHidden();
		}
		
		return false;
	}
	
	public boolean setSendMeNewsCheckbox(boolean checked) {
		return setSendMeNewsCheckbox(false, checked);
	}
	
	public boolean setSendMeNewsCheckbox(boolean waitAfter, boolean checked) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component sendMeNewsCheckbox = sendMeNewsCheckboxData.getComponent();
			if (sendMeNewsCheckbox != null) {
				if (sendMeNewsCheckbox._isHidden() && !checked)
					return true;
				else if (!sendMeNewsCheckbox._isHidden() && checked)
					return true;
				
				boolean clicked = sendMeNewsCheckbox.click(Mouse.LEFT);
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public boolean setShowPasswordCheckbox(boolean checked) {
		return setShowPasswordCheckbox(false, checked);
	}
	
	public boolean setShowPasswordCheckbox(boolean waitAfter, boolean checked) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component showPasswordCheckbox = showPasswordCheckboxData.getComponent();
			if (showPasswordCheckbox != null) {
				if (showPasswordCheckbox._isHidden() && !checked)
					return true;
				else if (!showPasswordCheckbox._isHidden() && checked)
					return true;
				
				boolean clicked = showPasswordCheckbox.click(Mouse.LEFT);
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public String getCharacterNameText() {
		if (isClosed())
			return null;
		
		Component characterName = characterNameTextboxData.getComponent();
		if (characterName != null && characterName.validate())
			return characterName.getText();
		
		return null;
	}
	
	public String getAgeText() {
		if (isClosed())
			return null;
		
		Component age = ageTextboxData.getComponent();
		if (age != null && age.validate())
			return age.getText();
		
		return null;
	}
	
	public String getEmailAddressText() {
		if (isClosed())
			return null;
		
		Component emailAddress = emailAddressTextboxData.getComponent();
		if (emailAddress != null && emailAddress.validate())
			return emailAddress.getText();
		
		return null;
	}
	
	public String getPasswordText() {
		if (isClosed())
			return null;
		
		Component password = passwordTextboxData.getComponent();
		if (password != null && password.validate())
			return password.getText();
		
		return null;
	}
	
	public boolean enterCharacterName(String characterName) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			final Component charNameComp = characterNameTextboxData.getComponent();
			if (charNameComp != null && charNameComp.validate()) {
				if (charNameComp.getText().toLowerCase().equals(characterName.toLowerCase()))
					return true;
				
				if (charNameComp.click(Mouse.LEFT)) {
					Time.sleepQuick();
					
					//
					// clear
					//
					if (!charNameComp.getText().isEmpty()) {
						Keyboard.holdKey(KeyEvent.VK_RIGHT, 4000 + Random.nextInt(200, 500));
						Time.sleepVeryQuick();
						
						Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 10000 + Random.nextInt(200, 500), new Condition() {
							@Override
							public boolean evaluate() {
								return charNameComp.getText().isEmpty();
							}
						});
						Time.sleepVeryQuick();
					}
					
					Keyboard.sendKeys(characterName);
					Time.sleepQuick();
					
					return charNameComp.getText().equals(characterName);
				}
			}
		}
		
		return false;
	}
	
	public boolean enterAge(String age) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			final Component ageComp = ageTextboxData.getComponent();
			if (ageComp != null && ageComp.validate()) {
				if (ageComp.getText().toLowerCase().equals(age.toLowerCase()))
					return true;
				
				if (ageComp.click(Mouse.LEFT)) {
					Time.sleepQuick();
					
					//
					// clear
					//
					if (!ageComp.getText().isEmpty()) {
						Keyboard.holdKey(KeyEvent.VK_RIGHT, 4000 + Random.nextInt(200, 500));
						Time.sleepVeryQuick();
						
						Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 10000 + Random.nextInt(200, 500), new Condition() {
							@Override
							public boolean evaluate() {
								return ageComp.getText().isEmpty();
							}
						});
						Time.sleepVeryQuick();
					}
					
					Keyboard.sendKeys(age);
					Time.sleepQuick();
					
					return ageComp.getText().equals(age);
				}
			}
		}
		
		return false;
	}
	
	public boolean enterEmailAddress(String emailAddress) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			final Component emailAddressComp = emailAddressTextboxData.getComponent();
			if (emailAddressComp != null && emailAddressComp.validate()) {
				if (emailAddressComp.getText().toLowerCase().equals(emailAddress.toLowerCase()))
					return true;
				
				if (emailAddressComp.click(Mouse.LEFT)) {
					Time.sleepQuick();
					
					//
					// clear
					//
					if (!emailAddressComp.getText().isEmpty()) {
						Keyboard.holdKey(KeyEvent.VK_RIGHT, 4000 + Random.nextInt(200, 500));
						Time.sleepVeryQuick();
						
						Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 10000 + Random.nextInt(200, 500), new Condition() {
							@Override
							public boolean evaluate() {
								return emailAddressComp.getText().isEmpty();
							}
						});
						Time.sleepVeryQuick();
					}
					
					Keyboard.sendKeys(emailAddress);
					Time.sleepQuick();
					
					return emailAddressComp.getText().equals(emailAddress);
				}
			}
		}
		
		return false;
	}
	
	public boolean enterPassword(String password) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			if (!setShowPasswordCheckbox(true, true))
				return false;
			Time.sleepQuick();
			
			final Component passwordComp = passwordTextboxData.getComponent();
			if (passwordComp != null && passwordComp.validate()) {
				if (passwordComp.getText().toLowerCase().equals(password.toLowerCase()))
					return true;
				
				if (passwordComp.click(Mouse.LEFT)) {
					Time.sleepQuick();
					
					//
					// clear
					//
					if (!passwordComp.getText().isEmpty()) {
						Keyboard.holdKey(KeyEvent.VK_RIGHT, 4000 + Random.nextInt(200, 500));
						Time.sleepVeryQuick();
						
						Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 10000 + Random.nextInt(200, 500), new Condition() {
							@Override
							public boolean evaluate() {
								return passwordComp.getText().isEmpty();
							}
						});
						Time.sleepVeryQuick();
					}
					
					Keyboard.sendKeys(password);
					Time.sleepQuick();
					
					return passwordComp.getText().equals(password);
				}
			}
		}
		
		return false;
	}
	
	public boolean clickBackButton() {
		return clickBackButton(false);
	}
	
	public boolean clickBackButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component backButton = backButtonData.getComponent();
			if (backButton != null) {
				boolean clicked = backButton.click(Mouse.LEFT);
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public boolean clickRandomiseNameButton() {
		return clickRandomiseNameButton(false);
	}
	
	public boolean clickRandomiseNameButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component randomiseNameButton = randomiseNameButtonData.getComponent();
			if (randomiseNameButton != null) {
				boolean clicked = randomiseNameButton.click(Mouse.LEFT);
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
	
	public boolean clickPlayNowButton() {
		return clickPlayNowButton(false);
	}
	
	public boolean clickPlayNowButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isEnlistScreenVisible()) {
			Component playNowButton = playNowButtonData.getComponent();
			if (playNowButton != null) {
				boolean clicked = playNowButton.click(Mouse.LEFT);
				if (waitAfter)
					Time.sleepQuick();
				return clicked;
			}
		}
		
		return false;
	}
}
