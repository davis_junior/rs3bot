package org.rs3.api.interfaces.widgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class InventoryWidget extends OpenableClosableWidget {
	
	public static final InventoryWidget get = new InventoryWidget(1473);
	
	public static final ComponentData itemsData = new ComponentData("Items", 1473, 5);
	
	public InventoryWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component mainInventoryBounds = MainWidget.tabBackpackBounds.getComponent();
		return mainInventoryBounds != null && mainInventoryBounds.isVisible();
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.Tab.BACKPACK.open(waitAfter);
	}

	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		return Ribbon.Tab.BACKPACK.close(waitAfter);
	}
	
	
	public Component[] getItems() {
		Component inventory = itemsData.getComponent();
		
		if (inventory != null) {
			Array1D<Component> items = inventory.getComponents();
			
			if (items == null)
				return null;
			
			if (items.getLength() != 28) {
				System.out.println("Error: Inventory.getItems() does not have 28 children!");
				return null;
			}
			
			return items.getAll();
		}
		
		return null;
	}
	
	/**
	 * Returns first item in item array
	 * @param itemId
	 * @return
	 */
	public Component getItem(int itemId) {
		Component[] items = getItems();
		if (items != null) {
			for (Component item : items) {
				if (item != null && item.getComponentId() == itemId)
					return item;
			}
		}
		
		return null;
	}
	
	public int countAllItems() {
		return countAllItems(null);
	}
	
	public int countAllItems(int... exceptionIds) {
		Component[] items = getItems();
			
		if (items != null) {
			int count = 0;
			
			for (Component item : items) {
				if (item != null) {
					int componentId = item.getComponentId();
					
					if (componentId != -1) {
						boolean exception = false;
						
						if (exceptionIds != null) {
							for (int exceptionId : exceptionIds) {
								if (componentId == exceptionId) {
									exception = true;
									break;
								}
							}
						}
						
						if (!exception)
							count++;
					}
				}
			}
			
			return count;
		}
		
		return -1;
	}
	
	public void dropAllItems() {
		dropAllItems(null);
	}
	
	public boolean dropAllItems(int... exceptionIds) {
		if (isClosed())
			return false;
		
		// Internal exceptions
		if (exceptionIds == null || exceptionIds.length == 0)
			exceptionIds = Interfaces.internalItemExceptionIds;
		else
			exceptionIds = ArrayUtils.combine(exceptionIds, Interfaces.internalItemExceptionIds);
		
		if (countAllItems(exceptionIds) == 0)
			return true;
		
		boolean origInteractionAntiban = Component.interactionAntiban;
		Component.interactionAntiban = false;
		
		Timer timer = new Timer(25000);
		while (timer.isRunning()) {
			if (isClosed())
				return false;
			
			Component[] items = getItems();
			
			if (items == null) {
				Time.sleep(50);
				
				continue;
			}
			
			for (Component item : items) {
				int componentId = item.getComponentId();
				if (componentId != -1) {
					boolean exception = false;
					if (exceptionIds != null) {
						for (int exceptionId : exceptionIds) {
							if (componentId == exceptionId) {
								exception = true;
								break;
							}
						}
					}
					
					if (!exception) {
						item.interact("Drop");
						Time.sleepQuick();
					}
				}
			}
			
			Time.sleepMedium();
			
			if (countAllItems(exceptionIds) == 0) {
				Component.interactionAntiban = origInteractionAntiban;
				return true;
			}
		}
		
		Component.interactionAntiban = origInteractionAntiban;
		return false;
	}
	
	public boolean containsAny(int... ids) {
		Component[] items = getItems();
		
		if (items == null)
			return false;
		
		for (Component item : items) {
			int componentId = item.getComponentId();
			
			if (componentId != -1) {
				for (int checkId : ids) {
					if (componentId == checkId)
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean containsUselessItems() {
		return containsAny(Interfaces.internalItemExceptionIds);
	}
	
	public void destroyItem(Component item) {
		if (item.interact("Destroy")) {
			Time.sleepMedium();
			
			Timer waitForDestroyMessage = new Timer(2000);
			while (waitForDestroyMessage.isRunning()) {
				if (DestroyMessage.get.isOpen())
					break;
				
				Time.sleep(50);
			}
			
			if (DestroyMessage.get.isOpen())
				DestroyMessage.get.chooseYes();
		}
	}
	
	public void destroyUselessItems() {
		Map<Integer, Integer> uselessIds = new HashMap<>();
		
		Component[] items = getItems();
		
		if (items == null)
			return;
		
		for (int i = 0; i < items.length; i++) {
			int componentId = items[i].getComponentId();
			
			if (componentId != -1) {
				for (int uselessId : Interfaces.internalItemExceptionIds) {
					if (componentId == uselessId) {
						uselessIds.put(i, componentId);
						break;
					}
				}
			}
		}
		
		if (uselessIds.size() > 0) {
			List<Integer> keySet = new ArrayList<>(uselessIds.keySet());
			Collections.shuffle(keySet); // shuffles all item indexes
			
			List<Integer> destroyedIds = new ArrayList<>(); // used in loop to only allow one item of the same id to be destroyed through destroy all
			
			// destroy non-duplicate items in random order
			for (int i : keySet) {
				int componentId = uselessIds.get(i);
				if (!destroyedIds.contains(componentId)) {
					destroyItem(items[i]);
					Time.sleepQuick();
					
					destroyedIds.add(componentId);
				}
			}
		}
	}
}
