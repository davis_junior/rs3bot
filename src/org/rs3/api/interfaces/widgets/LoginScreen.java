package org.rs3.api.interfaces.widgets;

import java.awt.event.KeyEvent;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class LoginScreen extends WidgetData {
	
	public static final LoginScreen get = new LoginScreen(596);
	
	public static final ComponentData usernameData = new ComponentData("Username", 596, 38);
	public static final ComponentData passwordData = new ComponentData("Password", 596, 63);
	public static final ComponentData loginButtonData = new ComponentData("Login Button", 596, 71);
	public static final ComponentData createAccountButtonData = new ComponentData("Create Account Button", 596, 86);
	public static final ComponentData loginMessageData = new ComponentData("Login Message", 596, 138); // visible always false
	public static final ComponentData tryAgainButtonData = new ComponentData("Try Again Button", 596, 144); // visible always false
	public static final ComponentData forgotPasswordScreenBorderData = new ComponentData("Forgot Password Screen Border", 596, 97); // only used to test visibility - use _hidden flag, true when hidden and false when visible -- hard to re-identify because there's no text or actions, trace back a couple from back button if necessary
	public static final ComponentData forgotPasswordBackButtonData = new ComponentData("Forgot Password Back Button", 596, 99); // visible always false, actions are null
	
	// ? Text:
	
	// Action: null
	// Text: "Login"\
	
	public LoginScreen(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(596);
		
		if (widget != null && widget.validate(596)) {
			// widget validation is all that is needed
			//Component loginScreen = loginButtonData.getComponent();
			//return loginScreen != null && loginScreen.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	public String getLoginMessageText() {
		Component loginMessage = loginMessageData.getComponent();
		if (loginMessage != null && loginMessage.validate())
			return loginMessage.getText();
		
		return null;
	}
	
	public boolean isLoginMessageVisible() {
		String msg = getLoginMessageText();
		return msg != null && !msg.trim().isEmpty();
	}
	
	public boolean clickLoginButton() {
		return clickLoginButton(false);
	}
	
	public boolean clickLoginButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isLoginMessageVisible())
			return false;
		
		if (isForgotPasswordScreenOpen())
			return false;
		
		Component loginButton = loginButtonData.getComponent();
		if (loginButton != null && loginButton.isVisible()) {
			boolean clicked = loginButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return clicked;
		}
		
		return false;
	}
	
	public boolean clickCreateAccountButton() {
		return clickCreateAccountButton(false);
	}
	
	public boolean clickCreateAccountButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (isLoginMessageVisible())
			return false;
		
		if (isForgotPasswordScreenOpen())
			return false;
		
		Component createAccountButton = createAccountButtonData.getComponent();
		if (createAccountButton != null && createAccountButton.isVisible()) {
			boolean clicked = createAccountButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return clicked;
		}
		
		return false;
	}
	
	public boolean clickTryAgainButton() {
		return clickTryAgainButton(false);
	}
	
	public boolean clickTryAgainButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (!isLoginMessageVisible())
			return false;
		
		Component tryAgainButton = tryAgainButtonData.getComponent();
		if (tryAgainButton != null) {
			boolean clicked = tryAgainButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return clicked;
		}
		
		return false;
	}
	
	public boolean enterUsername(String username) {
		if (isClosed())
			return false;

		if (isLoginMessageVisible())
			return false;
		
		if (isForgotPasswordScreenOpen())
			return false;
		
		final Component usernameComp = usernameData.getComponent();
		if (usernameComp != null && usernameComp.isVisible()) {
			if (usernameComp.getText().toLowerCase().equals(username.toLowerCase()))
				return true;
			
			if (usernameComp.click(Mouse.LEFT)) {
				Time.sleepQuick();
				
				//
				// clear
				//
				if (!usernameComp.getText().isEmpty()) {
					Keyboard.holdKey(KeyEvent.VK_RIGHT, 4000 + Random.nextInt(200, 500));
					Time.sleepVeryQuick();
					
					Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 10000 + Random.nextInt(200, 500), new Condition() {
						@Override
						public boolean evaluate() {
							return usernameComp.getText().isEmpty();
						}
					});
					Time.sleepVeryQuick();
				}
				
				Keyboard.sendKeys(username);
				Time.sleepQuick();
				
				return usernameComp.getText().equals(username);
			}
		}
		
		return false;
	}
	
	public boolean enterPassword(String password) {
		if (isClosed())
			return false;
		
		if (isLoginMessageVisible())
			return false;
		
		if (isForgotPasswordScreenOpen())
			return false;
		
		final Component passwordComp = passwordData.getComponent();
		if (passwordComp != null && passwordComp.isVisible()) {
			if (passwordComp.getText().toLowerCase().equals(password.toLowerCase()))
				return true;
			
			if (passwordComp.click(Mouse.LEFT)) {
				Time.sleepQuick();
				
				//
				// clear
				//
				if (!passwordComp.getText().isEmpty()) {
					Keyboard.holdKey(KeyEvent.VK_RIGHT, 4000 + Random.nextInt(200, 500));
					Time.sleepVeryQuick();
					
					Keyboard.holdKey(KeyEvent.VK_BACK_SPACE, 10000 + Random.nextInt(200, 500), new Condition() {
						@Override
						public boolean evaluate() {
							return passwordComp.getText().isEmpty();
						}
					});
					Time.sleepVeryQuick();
				}
				
				Keyboard.sendKeys(password);
				Time.sleepQuick();
				
				return passwordComp.getText().length() == password.length();
			}
		}
		
		return false;
	}
	
	public boolean isForgotPasswordScreenOpen() {
		if (isClosed())
			return false;
		
		Component border = forgotPasswordScreenBorderData.getComponent();
		if (border != null && border.validate()) {
			return !border._isHidden();
		}
		
		return false;
	}
	
	public boolean clickForgotPasswordBackButton() {
		return clickForgotPasswordBackButton(false);
	}
	
	public boolean clickForgotPasswordBackButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (!isForgotPasswordScreenOpen())
			return false;
		
		Component backButton = forgotPasswordBackButtonData.getComponent();
		if (backButton != null) {
			boolean clicked = backButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return clicked;
		}
		
		return false;
	}
}
