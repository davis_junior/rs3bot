package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class OptionPane extends ClosableWidget {
	
	public static final OptionPane get = new OptionPane(1578);
	
	public static final ComponentData headerData = new ComponentData("Header", 1578, 3);
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1578, 18);
	public static final ComponentData option1ButtonData = new ComponentData("Option 1 Button", 1578, 1);
	public static final ComponentData option2ButtonData = new ComponentData("Option 2 Button", 1578, 20);
	public static final ComponentData option3ButtonData = new ComponentData("Option 3 Button", 1578, 23);
	public static final ComponentData option4ButtonData = new ComponentData("Option 4 Button", 1578, 26);
	public static final ComponentData option1TextData = new ComponentData("Option 1 Text", 1578, 14);
	public static final ComponentData option2TextData = new ComponentData("Option 2 Text", 1578, 21);
	public static final ComponentData option3TextData = new ComponentData("Option 3 Text", 1578, 24);
	public static final ComponentData option4TextData = new ComponentData("Option 4 Text", 1578, 27);

	
	// 31 Text: Teleport to the Wilderness?
	
	// 0 Text: Yes.
	// 32 Text: No.
	
	public OptionPane(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1578);
		
		if (widget != null && widget.validate(1578)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	
	/**
	 * 
	 * @param index 1 to 4?
	 * @return
	 */
	public boolean chooseOption(int index) {
		return chooseOption(index, false);
	}
	
	/**
	 * 
	 * @param index 1 to 4?
	 * @param waitAfter
	 * @return
	 */
	public boolean chooseOption(int index, boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component optionButton = null;
		switch (index) {
		case 1:
			optionButton = option1ButtonData.getComponent();
			break;
		case 2:
			optionButton = option2ButtonData.getComponent();
			break;
		case 3:
			optionButton = option3ButtonData.getComponent();
			break;
		case 4:
			optionButton = option4ButtonData.getComponent();
			break;
		}
		
		if (optionButton != null && optionButton.isVisible()) {
			boolean result = optionButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public boolean chooseOption(String containsText) {
		return chooseOption(containsText, false);
	}
	
	public boolean chooseOption(String containsText, boolean waitAfter) {
		int index = getOptionIndex(containsText);
		if (index != -1)
			return chooseOption(index, waitAfter);
		
		return false;
	}
	
	/**
	 * 
	 * @param index 1 to 4?
	 * @return
	 */
	public String getOptionText(int index) {
		if (isClosed())
			return null;
		
		Component optionText = null;
		switch (index) {
		case 1:
			optionText = option1TextData.getComponent();
			break;
		case 2:
			optionText = option2TextData.getComponent();
			break;
		case 3:
			optionText = option3TextData.getComponent();
			break;
		case 4:
			optionText = option4TextData.getComponent();
			break;
		}
		
		if (optionText != null)
			return optionText.getText();
		
		return null;
	}
	
	/**
	 * 
	 * @return index of option 1 to 4?
	 */
	public int getOptionIndex(String containsText) {
		if (isClosed())
			return -1;
		
		for (int i = 1; i <= 4; i++) {
			String option = getOptionText(i);
			if (option != null) {
				if (option.contains(containsText))
					return i;
			}
		}
		
		return -1;
	}
	
	public String getHeader() {
		if (isOpen()) {
			Component headerComp = headerData.getComponent();
			if (headerComp != null)
				return headerComp.getText();
		}
		
		return null;
	}
}
