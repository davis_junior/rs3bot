package org.rs3.api.interfaces.widgets;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.LobbyScreen.LobbyTab;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class WorldSelect extends WidgetData {
	
	public static final WorldSelect get = new WorldSelect(910);
	
	public static final ComponentData worldBoundsData = new ComponentData("World Bounds", 910, 68);
	public static final ComponentData worldsData = new ComponentData("Worlds", 910, 75); // visible always false
	public static final ComponentData worldButtonsData = new ComponentData("World Buttons", 910, 83); // visible always false
	public static final ComponentData worldsScrollBarData = new ComponentData("Worlds Scroll Bar Buttons", 910, 92);
	
	// ? Text:
	
	// Action:
	// Text:
	
	public WorldSelect(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		return LobbyTab.WORLD_SELECT.isSelected();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	
	/*public boolean clickPlayButton() {
		return clickPlayButton(false);
	}
	
	public boolean clickPlayButton(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component playButton = playButtonData.getComponent();
		if (playButton != null && playButton.isVisible()) {
			boolean result = playButton.interact("Play");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public String getMessageText() {
		Component messageComp = messageData.getComponent();
		if (messageComp != null && messageComp.validate()) {
			return messageComp.getText();
		}
		
		return null;
	}
	
	// TODO: visible is false, find another method besides checking if text is empty
	public boolean isMessageVisible() {
		String msg = getMessageText();
		return msg != null && !msg.trim().isEmpty();
	}*/
	
	public Component[] getWorlds() {
		Component worlds = worldsData.getComponent();
		
		if (worlds != null) {
			Array1D<Component> items = worlds.getComponents();
			if (items == null)
				return null;
			
			//return items.getAll(); // >1000 items, should break once component id is -1 in iteration
			
			List<Component> result = new ArrayList<>();
			for (int i = 0; i < items.getLength(); i++) {
				Component item = items.get(i);
				if (item == null || item.getText() == null || item.getText().trim().isEmpty())
					break;
				
				result.add(item);
			}
			
			if (result.size() != 0)
				return result.toArray(new Component[0]);
		}
		
		return null;
	}
	
	public Component[] getWorldButtons() {
		Component worldButtons = worldButtonsData.getComponent();
		
		if (worldButtons != null) {
			Array1D<Component> items = worldButtons.getComponents();
			if (items == null)
				return null;
			
			//return items.getAll(); // >1000 items, should break once component id is -1 in iteration
			
			List<Component> result = new ArrayList<>();
			for (int i = 0; i < items.getLength(); i++) {
				Component item = items.get(i);
				if (item == null || item.getComponentName() == null || item.getComponentName().trim().isEmpty())
					break;
				
				result.add(item);
			}
			
			if (result.size() != 0)
				return result.toArray(new Component[0]);
		}
		
		return null;
	}
	
	/**
	 * Returns how much space is available to scroll up. -1 is returned if scroll bar does not exist.
	 * 
	 * @return Scroll bar top gap height (in y coords)
	 */
	public int getScrollBarTopGapHeight() {
		Component scrollBar = worldsScrollBarData.getComponent();
		
		if (scrollBar != null && scrollBar.isVisible()) {
			Array1D<Component> comps = scrollBar.getComponents();
			
			if (comps != null) {
				Component topPosition = comps.get(2);
				int topY = topPosition.getAbsoluteLocation().y;
				
				Component yellowBar = comps.get(0);
				int minY = yellowBar.getAbsoluteLocation().y;
				
				return topY - minY;
			}
		}
		
		return -1;
	}
	
	/**
	 * Returns how much space is available to scroll down. -1 is returned if scroll bar does not exist.
	 * 
	 * @return Scroll bar bottom gap height (in y coords)
	 */
	public int getScrollBarBottomGapHeight() {
		Component scrollBar = worldsScrollBarData.getComponent();
		
		if (scrollBar != null && scrollBar.isVisible()) {
			Array1D<Component> comps = scrollBar.getComponents();
			
			if (comps != null) {
				Component bottomPosition = comps.get(3);
				int bottomY = bottomPosition.getAbsoluteLocation().y + bottomPosition.getHeight();
				
				Component yellowBar = comps.get(0);
				int maxY = yellowBar.getAbsoluteLocation().y + yellowBar.getHeight();
				
				return maxY - bottomY;
			}
		}
		
		return -1;
	}
	
	/**
	 * If world is within scroll view.
	 * 
	 * @param id
	 * @return
	 */
	public boolean isWorldDisplayed(int world) {
		Component comp = getWorld(world);
		return comp != null ? isWorldDisplayed(comp) : false;
	}
	
	/**
	 * If world is within scroll view.
	 * 
	 * @param item
	 * @return
	 */
	public boolean isWorldDisplayed(Component comp) {
		Component border = worldBoundsData.getComponent();
		if (border != null)
			return border.contains(comp);
		
		return false;
	}
	
	/**
	 * 
	 * @param world
	 * @return null if world is not found
	 */
	public Component getWorld(int world) {
		Component[] items = getWorlds();
		if (items != null) {
			for (Component item : items) {
				if (Integer.parseInt(item.getText()) == world)
					return item;
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param world
	 * @return null if world is not found
	 */
	public Component getWorldButton(int world) {
		Component[] items = getWorldButtons();
		if (items != null) {
			for (Component item : items) {
				if (item.getComponentName().equals("World " + world))
					return item;
			}
		}
		
		return null;
	}
	
	public boolean scrollToWorld(int world) {
		Component comp = getWorld(world);
		return comp != null ? scrollToWorld(comp) : false;
	}
	
	// vertical scrolling is all that's necessary
	public boolean scrollToWorld(Component comp) {
		if (isWorldDisplayed(comp))
			return true;
		
		if (comp != null) {
			Component worldBounds = worldBoundsData.getComponent();
			if (worldBounds != null) {
				int borderMinY = worldBounds.getAbsoluteLocation().y;
				int borderMaxY = borderMinY + worldBounds.getHeight();
				
				Point randPoint = worldBounds.getRandomPoint();
				if (randPoint == null)
					return false;
				
				Mouse.mouse(randPoint.x, randPoint.y);
				Time.sleepQuickest();
				
				Timer timer = new Timer(10000);
				while (timer.isRunning()) {
					int minY = comp.getAbsoluteLocation().y;
					int maxY = minY + comp.getHeight();
					
					if (minY <= borderMinY) {
						if (getScrollBarTopGapHeight() != 0) // make sure scroll bar isn't already scrolled to the top
							Mouse.scrollMouse(true);
						else
							return false;
					} else if (maxY >= borderMaxY) {
						if (getScrollBarBottomGapHeight() != 0) // make sure scroll bar isn't already scrolled to the bottom
							Mouse.scrollMouse(false);
						else
							return false;
					}
					
					Time.sleepQuickest();
					
					if (isWorldDisplayed(comp))
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean select(int world) {
		if (Client.getWorldInfo().getWorld() == world)
			return true;
		
		if (!isOpen()) {
			if (!LobbyTab.WORLD_SELECT.select(true))
				return false;
		}
		
		if (!isOpen())
			return false;

		if (!LobbyScreen.get.handleMessage())
			return false;
		
		if (scrollToWorld(world)) {
			Time.sleepVeryQuick();
			
			Component button = getWorldButton(world);
			if (button != null && button.validate()) {
				boolean result = button.interact("Select", "World " + world);
				Time.sleepQuick();
				
				if (result) {
					return Client.getWorldInfo().getWorld() == world;
				}
			}
		}
		
		return false;
	}
}
