package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Time;

public class OptionsMenuWidget extends OpenableClosableWidget {
	
	public static final OptionsMenuWidget get = new OptionsMenuWidget(1433);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1433, 29);
	public static final ComponentData gameSettingsButtonData = new ComponentData("Game Settings Button", 1433, 43);
	public static final ComponentData graphicsSettingsButtonData = new ComponentData("Graphics Settings Button", 1433, 51);
	public static final ComponentData audioSettingsButtonData = new ComponentData("Audio Settings Button", 1433, 59);
	public static final ComponentData interfaceSettingsButtonData = new ComponentData("Interface Settings Button", 1433, 83);
	public static final ComponentData controlsButtonData = new ComponentData("Controls Button", 1433, 91);
	public static final ComponentData skipTutorialButtonData = new ComponentData("Skip Tutorial Button", 1433, 132); // api visible is true, Actions: Skip Tutorial
	public static final ComponentData hopWorldsButtonData = new ComponentData("Hop Worlds Button", 1433, 145);
	
	public OptionsMenuWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component optionsMenuBounds = MainWidget.optionsMenuBoundsData.getComponent();
		return optionsMenuBounds != null && !optionsMenuBounds._isHidden();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return Ribbon.SubInterface.SETTINGS.open(waitAfter);
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	
	public boolean clickButton(ComponentData buttonData, String action) {
		return clickButton(buttonData, action, false);
	}
	
	public boolean clickButton(ComponentData buttonData, String action, boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component button = buttonData.getComponent();
		if (button != null && button.isVisible()) {
			boolean result = button.interact(action);
			if (waitAfter)
				Time.sleepMedium();
			return result;
		}
		
		return false;
	}
	
	public boolean clickSkipTutorialButton() {
		return clickSkipTutorialButton(false);
	}
	
	public boolean clickSkipTutorialButton(boolean waitAfter) {
		if (!MainWidget.isTutorialActive())
			return false;
		
		return OptionsMenuWidget.get.clickButton(OptionsMenuWidget.skipTutorialButtonData, "Skip Tutorial", waitAfter);
	}
}
