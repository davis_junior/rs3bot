package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class LootScreen extends ClosableWidget {
	
	public static final LootScreen get = new LootScreen(1622);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1622, 8);
	public static final ComponentData itemsData = new ComponentData("Items", 1622, 10);
	public static final ComponentData lootAllButtonData = new ComponentData("Loot All Button", 1622, 21);
	
	public LootScreen(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1622);
		
		if (widget != null && widget.validate(1622)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	
	public Component[] getItems() {
		Component inventory = itemsData.getComponent();
		
		if (inventory != null) {
			Array1D<Component> items = inventory.getComponents();
			
			if (items == null)
				return null;
			
			if (items.getLength() != 28) {
				System.out.println("Error: Inventory.getItems() does not have 28 children!");
				return null;
			}
			
			return items.getAll();
		}
		
		return null;
	}
	
	/**
	 * Returns first item in item array
	 * @param itemId
	 * @return
	 */
	public Component getItem(int itemId) {
		Component[] items = getItems();
		if (items != null) {
			for (Component item : items) {
				if (item != null && item.getComponentId() == itemId)
					return item;
			}
		}
		
		return null;
	}
	
	public int countAllItems() {
		return countAllItems(null);
	}
	
	public int countAllItems(int... exceptionIds) {
		Component[] items = getItems();
			
		if (items != null) {
			int count = 0;
			
			for (Component item : items) {
				if (item != null) {
					int componentId = item.getComponentId();
					
					if (componentId != -1) {
						boolean exception = false;
						
						if (exceptionIds != null) {
							for (int exceptionId : exceptionIds) {
								if (componentId == exceptionId) {
									exception = true;
									break;
								}
							}
						}
						
						if (!exception)
							count++;
					}
				}
			}
			
			return count;
		}
		
		return -1;
	}
	
	public void lootAllItems() {
		lootAllItems(null);
	}
	
	public boolean lootAllItems(int... exceptionIds) {
		if (isClosed())
			return false;
		
		// Internal exceptions
		if (exceptionIds == null || exceptionIds.length == 0)
			exceptionIds = Interfaces.internalItemExceptionIds;
		else
			exceptionIds = ArrayUtils.combine(exceptionIds, Interfaces.internalItemExceptionIds);
		
		if (countAllItems(exceptionIds) == 0)
			return true;
		
		if (countAllItems(exceptionIds) == countAllItems())
			return clickLootAllButton(true);
		
		boolean origInteractionAntiban = Component.interactionAntiban;
		Component.interactionAntiban = false;
		
		Timer timer = new Timer(25000);
		while (timer.isRunning()) {
			if (isClosed())
				return false;
			
			Component[] items = getItems();
			
			if (items == null) {
				Time.sleep(50);
				
				continue;
			}
			
			for (Component item : items) {
				int componentId = item.getComponentId();
				if (componentId != -1) {
					boolean exception = false;
					if (exceptionIds != null) {
						for (int exceptionId : exceptionIds) {
							if (componentId == exceptionId) {
								exception = true;
								break;
							}
						}
					}
					
					if (!exception) {
						item.interact("Take");
						Time.sleepQuick();
					}
				}
			}
			
			Time.sleepMedium();
			
			if (countAllItems(exceptionIds) == 0) {
				Component.interactionAntiban = origInteractionAntiban;
				return true;
			}
		}
		
		Component.interactionAntiban = origInteractionAntiban;
		return false;
	}
	
	public boolean containsAny(int... ids) {
		Component[] items = getItems();
		
		if (items == null)
			return false;
		
		for (Component item : items) {
			int componentId = item.getComponentId();
			
			if (componentId != -1) {
				for (int checkId : ids) {
					if (componentId == checkId)
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean containsUselessItems() {
		return containsAny(Interfaces.internalItemExceptionIds);
	}
	
	public boolean clickLootAllButton() {
		return clickLootAllButton(false);
	}
	
	public boolean clickLootAllButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component lootAllButton = lootAllButtonData.getComponent();
		if (lootAllButton != null && lootAllButton.isVisible()) {
			boolean result = lootAllButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
}
