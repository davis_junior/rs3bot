package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public class ItemSelectWidget extends WidgetData {
	
	public static final ItemSelectWidget get = new ItemSelectWidget(1371);
	
	public static final ComponentData headerData = new ComponentData("Header", 1371, 49);
	public static final ComponentData increaseAmountButtonData = new ComponentData("Increase Amount Button", 1371, 29);
	public static final ComponentData decreaseAmountButtonData = new ComponentData("Decrease Amount Button", 1371, 31);
	public static final ComponentData itemsData = new ComponentData("Items", 1371, 44);
	
	// ? Text: 
	
	// ? Action: null
	// ? Text: 
	
	public ItemSelectWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1371);
		
		if (widget != null && widget.validate(1371))
			return true;
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	
	public String getHeader() {
		if (isOpen()) {
			Component textComp = headerData.getComponent();
			if (textComp != null)
				return textComp.getText();
		}
		
		return null;
	}
	
	public boolean increaseAmount(boolean waitAfter) {
		if (isOpen()) {
			Component increaseComp = increaseAmountButtonData.getComponent();
			if (increaseComp != null) {
				boolean result = increaseComp.interact("Increase");
				
				if (waitAfter)
					Time.sleepQuick();
				
				return result;
			}
		}
		
		return false;
	}
	
	public boolean decreaseAmount(boolean waitAfter) {
		if (isOpen()) {
			Component decreaseComp = decreaseAmountButtonData.getComponent();
			if (decreaseComp != null) {
				boolean result = decreaseComp.interact("Decrease");
				
				if (waitAfter)
					Time.sleepQuick();
				
				return result;
			}
		}
		
		return false;
	}
	
	public boolean selectItem(int itemId, boolean waitAfter) {
		if (isOpen()) {
			Component itemsComp = itemsData.getComponent();
			if (itemsComp != null) {
				Component[] items = itemsComp.getComponents().getAll();
				if (items != null) {
					for (Component item : items) {
						if (item == null)
							continue;
						
						if (item.getComponentId() == itemId) {
							// TODO: get prev component instead: item bounds (full)
							boolean result = item.interact("Select");
							
							if (waitAfter)
								Time.sleepQuick();
							
							return result;
						}
					}
				}
			}
		}
		
		return false;
	}
}
