package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.Openable;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;

public class RangedAbilitiesSubInterfaceTabWidget extends WidgetData implements Openable {
	
	public static final RangedAbilitiesSubInterfaceTabWidget get = new RangedAbilitiesSubInterfaceTabWidget(1456);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1456, 1);
	public static final ComponentData tabRangedButtonData = new ComponentData("Ranged Tab Button", 1456, 7, 7);
	
	
	public RangedAbilitiesSubInterfaceTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1456);
		
		if (widget != null && widget.validate(1456)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return SubInterfaceTab.POWERS_RANGED.open(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
