package org.rs3.api.interfaces.widgets;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Calculations;
import org.rs3.api.Interfaces;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.ObjectDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.Widget;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class ShopWidget extends ClosableWidget {
	
	public static final ShopWidget get = new ShopWidget(1265);
	
	public static final ComponentData itemButtonsData = new ComponentData("Item Buttons", 1265, 20);
	public static final ComponentData freeItemButtonsData = new ComponentData("Free Item Buttons", 1265, 21);
	
	public static final ComponentData itemCostsData = new ComponentData("Item Costs", 1265, 23); // text
	public static final ComponentData itemNamesData = new ComponentData("Item Names", 1265, 25); // text
	public static final ComponentData itemIdsData = new ComponentData("Item Ids", 1265, 26); // comp id
	
	public static final ComponentData freeItemCostsData = new ComponentData("Free Item Costs", 1265, 97); // text
	public static final ComponentData freeItemNamesData = new ComponentData("Free Item Names", 1265, 98); // text
	public static final ComponentData freeItemIdsData = new ComponentData("Free Item Ids", 1265, 99); // comp id
	
	public static final ComponentData allItemBounds = new ComponentData("All Item Bounds", 1265, 58);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1265, 89);
	
	/**
	 * 0 - entire scroll bar border without buttons (does not consider current scroll bar position/size)
	 * 1 - currently visible scroll bar without buttons (yellow bar)
	 * 2 - top position
	 * 3 - bottom position
	 * 4 - top button
	 * 5 - bottom button
	 */
	public static final ComponentData scrollBarData = new ComponentData("Bank Scroll Bar", 1265, 57);
	
	public ShopWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1265);
		
		if (widget != null && widget.validate(1265)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitOnClose) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			if (waitOnClose)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public Component[] getItemButtons() {
		List<Component> result = new ArrayList<>();
		
		Component freeItemButtonsComp = freeItemButtonsData.getComponent();
		if (freeItemButtonsComp != null) {
			Array1D<Component> freeItemButtons = freeItemButtonsComp.getComponents();
			if (freeItemButtons != null) {
				//return freeItemButtons.getAll(); // 39 items, should break once component name is blank in interation
				
				for (int i = 0; i < freeItemButtons.getLength(); i++) {
					Component item = freeItemButtons.get(i);
					if (item == null || item.getComponentName() == null || item.getComponentName().trim().isEmpty())
						break;
					
					result.add(item);
				}
			}
		}
		
		Component itemButtonsComp = itemButtonsData.getComponent();
		if (itemButtonsComp != null) {
			Array1D<Component> itemButtons = itemButtonsComp.getComponents();
			if (itemButtons != null) {
				//return itemButtons.getAll(); // 39 items, should break once component name is blank in interation
				
				for (int i = 0; i < itemButtons.getLength(); i++) {
					Component item = itemButtons.get(i);
					if (item == null || item.getComponentName() == null || item.getComponentName().trim().isEmpty())
						break;
					
					result.add(item);
				}
			}
		}
		
		if (result.size() > 0)
			return result.toArray(new Component[0]);
		
		return null;
	}
	
	public Integer[] getItemCosts() {
		List<Integer> result = new ArrayList<>();
		
		Component freeItemCostsComp = freeItemCostsData.getComponent();
		if (freeItemCostsComp != null) {
			Array1D<Component> freeItemCosts = freeItemCostsComp.getComponents();
			if (freeItemCosts != null) {
				for (Component item : freeItemCosts.getAll()) {
					if (item != null) {
						String cost = item.getText();
						if (cost != null && !cost.trim().isEmpty()) {
							if (cost.contains("Free"))
								result.add(0);
							else
								result.add(Integer.parseInt(cost));
						} else
							break;
					} else
						break;
				}
			}
		}
		
		Component itemCostsComp = itemCostsData.getComponent();
		if (itemCostsComp != null) {
			Array1D<Component> itemCosts = itemCostsComp.getComponents();
			if (itemCosts != null) {
				//return itemButtons.getAll(); // 39 items, should break once text is blank in interation
				
				for (int i = 0; i < itemCosts.getLength(); i++) {
					Component item = itemCosts.get(i);
					if (item != null) {
						String cost = item.getText();
						if (cost != null && !cost.trim().isEmpty()) {
							if (cost.contains("Free"))
								result.add(0);
							else
								result.add(Integer.parseInt(cost));
						} else
							break;
					} else
						break;
				}
			}
		}
		
		if (result.size() > 0)
			return result.toArray(new Integer[0]);
		
		return null;
	}
	
	public String[] getItemNames() {
		List<String> result = new ArrayList<>();
		
		Component freeItemNamesComp = freeItemNamesData.getComponent();
		if (freeItemNamesComp != null) {
			Array1D<Component> freeItemNames = freeItemNamesComp.getComponents();
			if (freeItemNames != null) {
				for (Component item : freeItemNames.getAll()) {
					if (item != null) {
						String name = item.getText();
						if (name != null && !name.trim().isEmpty())
							result.add(name);
						else
							break;
					} else
						break;
				}
			}
		}
		
		Component itemNamesComp = itemNamesData.getComponent();
		if (itemNamesComp != null) {
			Array1D<Component> itemNames = itemNamesComp.getComponents();
			if (itemNames != null) {
				//return itemButtons.getAll(); // 39 items, should break once text is blank in interation
				
				for (int i = 0; i < itemNames.getLength(); i++) {
					Component item = itemNames.get(i);
					if (item != null) {
						String name = item.getText();
						if (name != null && !name.trim().isEmpty())
							result.add(name);
						else
							break;
					} else
						break;
				}
			}
		}
		
		if (result.size() > 0)
			return result.toArray(new String[0]);
		
		return null;
	}
	
	public Integer[] getItemIds() {
		List<Integer> result = new ArrayList<>();
		
		Component freeItemIdsComp = freeItemIdsData.getComponent();
		if (freeItemIdsComp != null) {
			Array1D<Component> freeItemIds = freeItemIdsComp.getComponents();
			if (freeItemIds != null) {
				for (Component item : freeItemIds.getAll()) {
					if (item != null) {
						int id = item.getComponentId();
						if (id != -1) {
							result.add(id);
						} else
							break;
					} else
						break;
				}
			}
		}
		
		Component itemIdsComp = itemIdsData.getComponent();
		if (itemIdsComp != null) {
			Array1D<Component> itemIds = itemIdsComp.getComponents();
			if (itemIds != null) {
				//return itemButtons.getAll(); // 39 items, should break once comp id is -1 in interation
				
				for (int i = 0; i < itemIds.getLength(); i++) {
					Component item = itemIds.get(i);
					if (item != null) {
						int id = item.getComponentId();
						if (id != -1)
							result.add(id);
						else
							break;
					} else
						break;
				}
			}
		}
		
		if (result.size() > 0)
			return result.toArray(new Integer[0]);
		
		return null;
	}
	
	
	/**
	 * Returns how much space is available to scroll up. -1 is returned if scroll bar does not exist.
	 * 
	 * @return Scroll bar top gap height (in y coords)
	 */
	public int getScrollBarTopGapHeight() {
		Component scrollBar = scrollBarData.getComponent();
		
		if (scrollBar != null && scrollBar.isVisible()) {
			Array1D<Component> comps = scrollBar.getComponents();
			
			if (comps != null) {
				Component topPosition = comps.get(2);
				int topY = topPosition.getAbsoluteLocation().y;
				
				Component yellowBar = comps.get(0);
				int minY = yellowBar.getAbsoluteLocation().y;
				
				return topY - minY;
			}
		}
		
		return -1;
	}
	
	/**
	 * Returns how much space is available to scroll down. -1 is returned if scroll bar does not exist.
	 * 
	 * @return Scroll bar bottom gap height (in y coords)
	 */
	public int getScrollBarBottomGapHeight() {
		Component scrollBar = scrollBarData.getComponent();
		
		if (scrollBar != null && scrollBar.isVisible()) {
			Array1D<Component> comps = scrollBar.getComponents();
			
			if (comps != null) {
				Component bottomPosition = comps.get(3);
				int bottomY = bottomPosition.getAbsoluteLocation().y + bottomPosition.getHeight();
				
				Component yellowBar = comps.get(0);
				int maxY = yellowBar.getAbsoluteLocation().y + yellowBar.getHeight();
				
				return maxY - bottomY;
			}
		}
		
		return -1;
	}
	
	/**
	 * If item is within scroll view.
	 * 
	 * @param id
	 * @return
	 */
	public boolean isItemDisplayed(int id) {
		Component item = getItemButton(id);
		return item != null ? isItemDisplayed(item) : false;
	}
	
	/**
	 * If item is within scroll view.
	 * 
	 * @param item
	 * @return
	 */
	public boolean isItemDisplayed(Component item) {
		Component border = allItemBounds.getComponent();
		if (border != null)
			return border.contains(item);
		
		return false;
	}
	
	/**
	 * 
	 * @param id
	 * @return null if store does not contain item
	 */
	public Component getItemButton(int id) {
		Component[] itemButtons = getItemButtons();
		Integer[] itemIds = getItemIds();
		if (itemButtons != null && itemIds != null) {
			for (int i = 0; i < itemIds.length; i++) {
				if (itemIds[i] == id)
					return itemButtons[i];
			}
		}
		
		return null;
	}
	
	public boolean scrollToItemButton(int id) {
		Component item = getItemButton(id);
		return item != null ? scrollToItem(item) : false;
	}
	
	// vertical scrolling is all that's necessary
	public boolean scrollToItem(Component item) {
		if (isClosed())
			return false;
		
		if (isItemDisplayed(item))
			return true;
		
		if (item != null) {
			Component itemsBorder = allItemBounds.getComponent();
			if (itemsBorder != null) {
				int borderMinY = itemsBorder.getAbsoluteLocation().y;
				int borderMaxY = borderMinY + itemsBorder.getHeight();
				
				Point randPoint = itemsBorder.getRandomPoint();
				if (randPoint == null)
					return false;
				
				Mouse.mouse(randPoint.x, randPoint.y);
				Time.sleepQuickest();
				
				Timer timer = new Timer(10000);
				while (timer.isRunning()) {
					int minY = item.getAbsoluteLocation().y;
					int maxY = minY + item.getHeight();
					
					if (minY <= borderMinY) {
						if (getScrollBarTopGapHeight() != 0) // make sure scroll bar isn't already scrolled to the top
							Mouse.scrollMouse(true);
						else
							return false;
					} else if (maxY >= borderMaxY) {
						if (getScrollBarBottomGapHeight() != 0) // make sure scroll bar isn't already scrolled to the bottom
							Mouse.scrollMouse(false);
						else
							return false;
					}
					
					Time.sleepQuickest();
					
					if (isItemDisplayed(item))
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean buy1(int itemId) {
		return buy(itemId, 1);
	}
	
	public boolean buy5(int itemId) {
		return buy(itemId, 5);
	}
	
	public boolean buy10(int itemId) {
		return buy(itemId, 10);
	}
	
	public boolean buy50(int itemId) {
		return buy(itemId, 50);
	}
	
	public boolean buy500(int itemId) {
		return buy(itemId, 500);
	}
	
	public boolean buyAll(int itemId) {
		return buy(itemId, -1);
	}
	
	// TODO: !!! check current tab is on buy items
	public boolean buy(int itemId, int amount) {
		if (isClosed())
			return false;
		
		if (ItemTable.INVENTORY.countAllItems(false) >= 28)
			return false;
		
		if (ItemTable.SHOP_BUY_FREE_ITEMS.countItems(true, itemId) < amount
				&& ItemTable.SHOP_BUY_ITEMS.countItems(true, itemId) < amount) {
			
			System.out.println("Shop does not contain enough stock of item!");
			return false;
		}
		
		Component[] itemButtons = getItemButtons();
		Integer[] itemCosts = getItemCosts();
		String[] itemNames = getItemNames();
		Integer[] itemIds = getItemIds();
		
		int itemIndex = -1;
		for (int i = 0; i < itemIds.length; i++) {
			if (itemIds[i] == itemId) {
				itemIndex = i;
				break;
			}
		}
		
		if (itemIndex == -1)
			return false;
		
		Component itemButton = itemButtons[itemIndex];
		Integer itemCost = itemCosts[itemIndex];
		String itemName = itemNames[itemIndex];
		
		if (ItemTable.MONEYPOUCH.getCoinCount() < amount*itemCost) {
			System.out.println("Not enough coins to make shop purchase!");
			return false;
		}
		
		if (scrollToItem(itemButton)) {
			Time.sleepQuick();
			
			String action = itemCost == 0 ? "Take" : "Buy";
			
			switch (amount) {
			case -1:
				return itemButton.interact(action + " All", itemName);
			case 1:
				return itemButton.interact(action + " 1", itemName);
			case 5:
				return itemButton.interact(action + " 5", itemName);
			case 10:
				return itemButton.interact(action + " 10", itemName);
			case 50:
				return itemButton.interact(action + " 50", itemName);
			case 500:
				return itemButton.interact(action + " 500", itemName);
			default:
				// TODO: implement amount selection
			}
		}
		
		return false;
	}
}
