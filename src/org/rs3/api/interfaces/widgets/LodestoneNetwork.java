package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class LodestoneNetwork extends OpenableClosableWidget {
	
	public static final LodestoneNetwork get = new LodestoneNetwork(1092);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 1092, 49);
	
	public static final ComponentData teleportBanditCampData = new ComponentData("Teleport Bandit Camp", 1092, 8);
	public static final ComponentData teleportLunarIsleData = new ComponentData("Teleport Lunar Isle", 1092, 9);
	public static final ComponentData teleportAlKharidData = new ComponentData("Teleport Al Kharid", 1092, 10);
	public static final ComponentData teleportArdougneData = new ComponentData("Teleport Ardougne", 1092, 11);
	public static final ComponentData teleportBurthorpeData = new ComponentData("Teleport Burthorpe", 1092, 12);
	public static final ComponentData teleportCatherbyData = new ComponentData("Teleport Catherby", 1092, 13);
	public static final ComponentData teleportDraynorVillageData = new ComponentData("Teleport Draynor Village", 1092, 14);
	public static final ComponentData teleportEdgevilleData = new ComponentData("Teleport Edgeville", 1092, 15);
	public static final ComponentData teleportFaladorData = new ComponentData("Teleport Falador", 1092, 16);
	public static final ComponentData teleportLumbridgeData = new ComponentData("Teleport Lumbridge", 1092, 17);
	public static final ComponentData teleportPortSarimData = new ComponentData("Teleport Port Sarim", 1092, 18);
	public static final ComponentData teleportSeersVillageData = new ComponentData("Teleport Seers' Village", 1092, 19);
	public static final ComponentData teleportTaverleyData = new ComponentData("Teleport Taverley", 1092, 20);
	public static final ComponentData teleportVarrockData = new ComponentData("Teleport Varrock", 1092, 21);
	public static final ComponentData teleportYanilleData = new ComponentData("Teleport Yanille", 1092, 22);
	public static final ComponentData teleportCanifisData = new ComponentData("Teleport Canifis", 1092, 23);
	public static final ComponentData teleportEaglesPeakData = new ComponentData("Teleport Eagles' Peak", 1092, 24);
	public static final ComponentData teleportFremennikProvinceData = new ComponentData("Teleport Fremennik Province", 1092, 25);
	public static final ComponentData teleportKaramjaData = new ComponentData("Teleport Karamja", 1092, 26);
	public static final ComponentData teleportOoGlogData = new ComponentData("Teleport Oo'glog", 1092, 27);
	public static final ComponentData teleportTirannwnData = new ComponentData("Teleport Tirannwn", 1092, 28);
	public static final ComponentData teleportWildernessVolcanoData = new ComponentData("Teleport Wilderness Volcano", 1092, 29);
	public static final ComponentData teleportAshdaleData = new ComponentData("Teleport Ashdale", 1092, 30);
	public static final ComponentData teleportPrifddinasData = new ComponentData("Teleport Prifddinas", 1092, 31);
	
	
	
	// 47 Action: Close
	// 45, 14 Text: Lodestone Network
	
	public LodestoneNetwork(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1092);
		
		if (widget != null && widget.validate(1092)) {
			// widget validation is all that is needed
			//Component lodestoneNetwork = closeButtonData.getComponent();
			//return lodestoneNetwork != null && lodestoneNetwork.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}
	
	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		if (GrandExchangeWidget.get.isOpen() || Bank.get.isOpen() || DepositBox.get.isOpen()) // do not open if some other interfaces are open since it's impossible
			return false;
		
		Component mapHomeTeleport = MiniMap.homeTeleportButton.getComponent();
		if (mapHomeTeleport != null) {
			if (mapHomeTeleport.loopInteract(null, "Teleport")) {
				if (waitAfter)
					Time.sleepQuick();
				
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public boolean close() {
		return close(false);
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (!isOpen())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		
		return Interfaces.defaultCloseInterface(waitAfter, getIsOpenCondition(), closeButton);
	}
	
}
