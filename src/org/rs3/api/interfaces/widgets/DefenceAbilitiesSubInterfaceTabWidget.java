package org.rs3.api.interfaces.widgets;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.Openable;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;

public class DefenceAbilitiesSubInterfaceTabWidget extends WidgetData implements Openable {
	
	public static final DefenceAbilitiesSubInterfaceTabWidget get = new DefenceAbilitiesSubInterfaceTabWidget(1453);
	
	public static final ComponentData abilitiesData = new ComponentData("Abilities", 1453, 1);
	public static final ComponentData tabDefenceButtonData = new ComponentData("Defence Tab Button", 1453, 7, 7);
	public static final ComponentData tabConstitutionButtonData = new ComponentData("Constitution Tab Button", 1453, 7, 8);
	
	
	public DefenceAbilitiesSubInterfaceTabWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1453);
		
		if (widget != null && widget.validate(1453)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean open() {
		return open(false);
	}

	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		return SubInterfaceTab.POWERS_DEFENSIVE.open(waitAfter);
	}
	
	
	public Component[] getAbilities() {
		Component abilities = abilitiesData.getComponent();
		
		if (abilities != null) {
			Array1D<Component> array = abilities.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
}
