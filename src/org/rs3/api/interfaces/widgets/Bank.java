package org.rs3.api.interfaces.widgets;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Calculations;
import org.rs3.api.Interfaces;
import org.rs3.api.GroundObjects;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.ObjectDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.Widget;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Bank extends OpenableClosableWidget {
	
	public static final Bank get = new Bank(762);
	
	public static final ComponentData bankItemsData = new ComponentData("Bank Items", 762, 243); // bounds of item rectangle; components are items
	public static final ComponentData invItemsData = new ComponentData("Inventory Items", 762, 10);
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 762, 331);
	public static final ComponentData noteWithdrawToggleButtonData = new ComponentData("Note Withdraw Toggle Button", 762, 64);
	public static final ComponentData depositCarriedItemsButtonData = new ComponentData("Deposit carried items Button", 762, 91);
	public static final ComponentData depositWornItemsButtonData = new ComponentData("Deposit worn items Button", 762, 99);
	public static final ComponentData depositBobInvButtonData = new ComponentData("Deposit BoB inventory Button", 762, 107);
	public static final ComponentData depositMoneyPouchButtonData = new ComponentData("Deposit money pouch Button", 762, 115);
	
	/**
	 * 0 - entire scroll bar border without buttons (does not consider current scroll bar position/size)
	 * 1 - currently visible scroll bar without buttons (yellow bar)
	 * 2 - top position
	 * 3 - bottom position
	 * 4 - top button
	 * 5 - bottom button
	 */
	public static final ComponentData bankScrollBarData = new ComponentData("Bank Scroll Bar", 762, 238);
	
	public Bank(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(762);
		
		if (widget != null && widget.validate(762)) {
			// widget validation is all that is needed
			//Component bank = bankItemsData.getComponent();
			//return bank != null && bank.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}
	
	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		Interactable bank = getNearest();
		
		if (bank != null) {
			if (Calculations.distanceTo(bank.getTile()) > 3)
				if (!bank.getTile().loopWalkTo(10000)) {
					if (waitAfter)
						Time.sleepQuick();
					
					return false;
				}
			
			Paint.updatePaintObject(bank);
			
			String action = "Bank";
			
			if (bank instanceof GroundObject) {
				GroundObject go = (GroundObject) bank;
				ObjectDefinition def = go.getDefinition();
				if (def != null) {
					String[] actions = def.getActions();
					if (actions != null) {
						for (String str : actions) {
							if (str != null && str.equals("Use")) {
								action = str;
								break;
							}
						}
					}
				}
			}
			
			if (bank.loopInteract(getIsOpenCondition(), action) || isOpen()) {
				Timer timer = new Timer(7000);
				while (timer.isRunning()) {
					if (isOpen()) {
						if (waitAfter)
							Time.sleepQuick();
						
						return true;
					}
					
					Time.sleep(50);
				}
			}
		}
		
		if (waitAfter)
			Time.sleepQuick();
		
		return false;
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitOnClose) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close");
			
			if (waitOnClose)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	/**
	 * Finds the nearest banker or bank booth. If the banker is very close to a bank booth (which is usual), then either is returned randomly.
	 * 
	 * @return nearest Interactable
	 */
	public Interactable getNearest() {
		Npc npc = Npcs.getNearest(null, 
				496, // Al Kharid
				14923, 14924, 14925, // Taverley
				19086, // Burthorpe "Gnome Banker"
				2718, 3418, 3293, 3416, // Grand Exchange
				9710 // Daemonheim "Fremennik Banker"
				);
		
		Interactable go = (Interactable) GroundObjects.getNearest(GroundObject.Type.Animable, 
				76274, // Al Kharid
				66665, 66666, 66667, // Taverley
				25688, // Burthorpe
				42192, // Clan camp bank chest
				83634 // Death area bank chest
				);
		
		if (npc == null)
			return go;
		
		if (go == null)
			return npc;
		
		double npcDist = Calculations.distanceTo(npc.getData().getCenterLocation().getTile(true));
		double goDist = Calculations.distanceTo(go.getData().getCenterLocation().getTile(true));
		
		if (Math.abs(npcDist - goDist) <= 2)
			return Random.nextBoolean() ? npc : go;
		else if (npcDist < goDist)
			return npc;
		else
			return go;
	}
	
	public Component[] getBankItems() {
		Component bankItems = bankItemsData.getComponent();
		
		if (bankItems != null) {
			Array1D<Component> items = bankItems.getComponents();
			
			if (items == null)
				return null;
			
			//return items.getAll(); // >1000 items, should break once component id is -1 in iteration
			
			List<Component> result = new ArrayList<>();
			for (int i = 0; i < items.getLength(); i++) {
				Component item = items.get(i);
				if (item == null || item.getComponentId() == -1)
					break;
				
				result.add(item);
			}
			
			if (result.size() != 0)
				return result.toArray(new Component[0]);
		}
		
		return null;
	}
	
	public Component[] getInvItems() {
		Component invItems = invItemsData.getComponent();
		
		if (invItems != null) {
			Array1D<Component> items = invItems.getComponents();
			if (items == null)
				return null;
			
			if (items.getLength() != 28) {
				System.out.println("Error: Bank.getInvItems() does not have 28 children!");
				return null;
			}
			
			return items.getAll();
		}
		
		return null;
	}
	
	public int countInv() {
		return countInv(null);
	}
	
	public int countInv(int... exceptionIds) {
		Component[] items = getInvItems();
			
		if (items != null) {
			int count = 0;
			
			for (Component item : items) {
				if (item != null) {
					int componentId = item.getComponentId();
					
					if (componentId != -1) {
						boolean exception = false;
						
						if (exceptionIds != null) {
							for (int exceptionId : exceptionIds) {
								if (componentId == exceptionId) {
									exception = true;
									break;
								}
							}
						}
						
						if (!exception)
							count++;
					}
				}
			}
			
			return count;
		}
		
		return -1;
	}
	
	public boolean depositAllInvItems() {
		return depositAllInvItems(null);
	}
	
	/**
	 * Waits after click.
	 * 
	 * @param exceptionIds
	 * @return
	 */
	public boolean depositAllInvItems(int... exceptionIds) {
		if (isClosed())
			return false;
		
		// Internal exceptions
		if (exceptionIds == null || exceptionIds.length == 0)
			exceptionIds = Interfaces.internalItemExceptionIds;
		else
			exceptionIds = ArrayUtils.combine(exceptionIds, Interfaces.internalItemExceptionIds);
		
		if (countInv(exceptionIds) == 0)
			return true;
		
		Timer timer = new Timer(25000);
		while (timer.isRunning()) {
			if (isClosed())
				return false;
			
			Component[] items = getInvItems();
			
			if (items == null) {
				Time.sleep(50);
				continue;
			}
			
			int itemCountInclExceptions = 0;
			Map<Integer, Integer> idsToDeposit = new HashMap<>();
			
			// add ids to hashmap
			for (int i = 0; i < items.length; i++) {
				int componentId = items[i].getComponentId();
				if (componentId != -1) {
					itemCountInclExceptions++;
					
					boolean exception = false;
					if (exceptionIds != null) {
						for (int exceptionId : exceptionIds) {
							if (componentId == exceptionId) {
								exception = true;
								break;
							}
						}
					}
					
					if (!exception)
						idsToDeposit.put(i, componentId);
				}
			}
			
			boolean depositAll = false;
			if (idsToDeposit.size() == itemCountInclExceptions) {
				if (Random.nextInt(0, 100) < 90)
					depositAll = true;
			}
			
			if (depositAll) {
				clickDepositCarriedItemsButton(true);
			} else {
				List<Integer> keySet = new ArrayList<>(idsToDeposit.keySet());
				Collections.shuffle(keySet); // shuffles all non-exception item indexes
				
				List<Integer> depositedIds = new ArrayList<>(); // used in loop to only allow one item of the same id to be deposited through deposit-all
				
				boolean originalAntiban = Component.interactionAntiban;
				Component.interactionAntiban = false;
				
				// deposit non-duplicate items in random order
				for (int i : keySet) {
					int componentId = idsToDeposit.get(i);
					if (!depositedIds.contains(componentId)) {
						boolean containsDepositAll = false;
						String[] actions = items[i].getActions();
						if (actions == null)
							continue;
						
						for (String action : actions) {
							if (action.contains("Deposit-All"))
								containsDepositAll = true;
						}
						
						if (containsDepositAll)
							items[i].interact("Deposit-All");
						else
							items[i].interact("Deposit");
						
						Time.sleepQuick();
						
						depositedIds.add(componentId);
					}
				}
				
				Component.interactionAntiban = originalAntiban;
			}
			
			Time.sleepMedium();
			
			if (countInv(exceptionIds) == 0)
				return true;
		}
		
		return false;
	}
	
	public boolean clickDepositCarriedItemsButton() {
		return clickDepositCarriedItemsButton(false);
	}
	
	public boolean clickDepositCarriedItemsButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component depositButton = depositCarriedItemsButtonData.getComponent();
		if (depositButton != null && depositButton.isVisible()) {
			boolean result = depositButton.interact("Deposit carried items");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public boolean clickDepositWornItemsButton() {
		return clickDepositWornItemsButton(false);
	}
	
	public boolean clickDepositWornItemsButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component depositButton = depositWornItemsButtonData.getComponent();
		if (depositButton != null && depositButton.isVisible()) {
			boolean result = depositButton.interact("Deposit worn items");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public boolean clickDepositBoBItemsButton() {
		return clickDepositBoBItemsButton(false);
	}
	
	public boolean clickDepositBoBItemsButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component depositButton = depositBobInvButtonData.getComponent();
		if (depositButton != null && depositButton.isVisible()) {
			boolean result = depositButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	public boolean clickDepositMoneyPouchButton() {
		return clickDepositMoneyPouchButton(false);
	}
	
	public boolean clickDepositMoneyPouchButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component depositButton = depositMoneyPouchButtonData.getComponent();
		if (depositButton != null && depositButton.isVisible()) {
			boolean result = depositButton.interact("Select");
			
			if (waitAfter)
				Time.sleepQuick();
			
			return result;
		}
		
		return false;
	}
	
	/**
	 * Returns how much space is available to scroll up. -1 is returned if scroll bar does not exist.
	 * 
	 * @return Scroll bar top gap height (in y coords)
	 */
	public int getScrollBarTopGapHeight() {
		Component scrollBar = bankScrollBarData.getComponent();
		
		if (scrollBar != null && scrollBar.isVisible()) {
			Array1D<Component> comps = scrollBar.getComponents();
			
			if (comps != null) {
				Component topPosition = comps.get(2);
				int topY = topPosition.getAbsoluteLocation().y;
				
				Component yellowBar = comps.get(0);
				int minY = yellowBar.getAbsoluteLocation().y;
				
				return topY - minY;
			}
		}
		
		return -1;
	}
	
	/**
	 * Returns how much space is available to scroll down. -1 is returned if scroll bar does not exist.
	 * 
	 * @return Scroll bar bottom gap height (in y coords)
	 */
	public int getScrollBarBottomGapHeight() {
		Component scrollBar = bankScrollBarData.getComponent();
		
		if (scrollBar != null && scrollBar.isVisible()) {
			Array1D<Component> comps = scrollBar.getComponents();
			
			if (comps != null) {
				Component bottomPosition = comps.get(3);
				int bottomY = bottomPosition.getAbsoluteLocation().y + bottomPosition.getHeight();
				
				Component yellowBar = comps.get(0);
				int maxY = yellowBar.getAbsoluteLocation().y + yellowBar.getHeight();
				
				return maxY - bottomY;
			}
		}
		
		return -1;
	}
	
	/**
	 * If bank item is within scroll view.
	 * 
	 * @param id
	 * @return
	 */
	public boolean isBankItemDisplayed(int id) {
		Component item = getBankItem(id);
		return item != null ? isBankItemDisplayed(item) : false;
	}
	
	/**
	 * If bank item is within scroll view.
	 * 
	 * @param item
	 * @return
	 */
	public boolean isBankItemDisplayed(Component item) {
		Component border = bankItemsData.getComponent();
		if (border != null)
			return border.contains(item);
		
		return false;
	}
	
	/**
	 * 
	 * @param id
	 * @return null if bank does not contain item
	 */
	public Component getBankItem(int id) {
		Component[] items = getBankItems();
		if (items != null) {
			for (Component item : items) {
				if (item.getComponentId() == id)
					return item;
			}
		}
		
		return null;
	}
	
	public boolean scrollToBankItem(int id) {
		Component item = getBankItem(id);
		return item != null ? scrollToBankItem(item) : false;
	}
	
	// vertical scrolling is all that's necessary
	public boolean scrollToBankItem(Component item) {
		if (isClosed())
			return false;
		
		if (isBankItemDisplayed(item))
			return true;
		
		if (item != null) {
			Component itemsBorder = bankItemsData.getComponent();
			if (itemsBorder != null) {
				int borderMinY = itemsBorder.getAbsoluteLocation().y;
				int borderMaxY = borderMinY + itemsBorder.getHeight();
				
				Point randPoint = itemsBorder.getRandomPoint();
				if (randPoint == null)
					return false;
				
				Mouse.mouse(randPoint.x, randPoint.y);
				Time.sleepQuickest();
				
				Timer timer = new Timer(10000);
				while (timer.isRunning()) {
					int minY = item.getAbsoluteLocation().y;
					int maxY = minY + item.getHeight();
					
					if (minY <= borderMinY) {
						if (getScrollBarTopGapHeight() != 0) // make sure scroll bar isn't already scrolled to the top
							Mouse.scrollMouse(true);
						else
							return false;
					} else if (maxY >= borderMaxY) {
						if (getScrollBarBottomGapHeight() != 0) // make sure scroll bar isn't already scrolled to the bottom
							Mouse.scrollMouse(false);
						else
							return false;
					}
					
					Time.sleepQuickest();
					
					if (isBankItemDisplayed(item))
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean withdrawAll(int id, boolean noted) {
		return withdraw(id, -2, noted);
	}
	
	public boolean withdrawAll(Component item, boolean noted) {
		return withdraw(item, -2, noted);
	}
	
	public boolean withdrawAllButOne(int id, boolean noted) {
		return withdraw(id, -1, noted);
	}
	
	public boolean withdrawAllButOne(Component item, boolean noted) {
		return withdraw(item, -1, noted);
	}
	
	public boolean withdraw(int id, int amount, boolean noted) {
		Component item = getBankItem(id);
		return item != null ? withdraw(item, amount, noted) : false;
	}
	
	// TODO: !!! check current tab items, then switch to all tabs if item is not listed
	public boolean withdraw(Component item, int amount, boolean noted) {
		if (isClosed())
			return false;
		
		if (countInv() >= 28)
			return false;
		
		if (isNoteWithdrawMode() != noted) {
			if (!setNoteWithdrawMode(noted))
				return false;
			Time.sleepQuick();
		}
		
		if (scrollToBankItem(item)) {
			Time.sleepQuick();
			
			switch (amount) {
			case -2:
				return item.interact("Withdraw-All");
			case -1:
				return item.interact("Withdraw-All but one");
			case 1:
				return item.interact("Withdraw-1");
			case 5:
				return item.interact("Withdraw-5");
			case 10:
				return item.interact("Withdraw-10");
			default:
				if (item.interact("Withdraw-X")) {
					Time.sleep(2000, 3000);
					
					// TODO: verify enter menu/input dialog is open, and wait for it instead of static sleep
					
					Keyboard.sendKeys(Integer.toString(amount));
					Time.sleepQuickest();
					Keyboard.pressKey(KeyEvent.VK_ENTER);
					Time.sleepQuickest();
					Keyboard.releaseKey(KeyEvent.VK_ENTER);
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean isNoteWithdrawMode() {
		int noteId = Client.getSettingData().getSettings().get(160);
		return noteId == 1;
	}
	
	public boolean setNoteWithdrawMode(boolean enable) {
		if (enable == isNoteWithdrawMode())
			return true;
		
		Component noteWithdrawToggle = noteWithdrawToggleButtonData.getComponent();
		
		if (noteWithdrawToggle != null) {
			boolean result = noteWithdrawToggle.interact("Switch to note withdrawal mode");
			Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
