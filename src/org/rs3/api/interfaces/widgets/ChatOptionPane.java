package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public class ChatOptionPane extends WidgetData {
	
	public static final ChatOptionPane get = new ChatOptionPane(1188);
	
	public static final ComponentData headerData = new ComponentData("Header", 1188, 31);
	public static final ComponentData option1ButtonData = new ComponentData("Option 1 Button", 1188, 0);
	public static final ComponentData option2ButtonData = new ComponentData("Option 2 Button", 1188, 32);
	public static final ComponentData option3ButtonData = new ComponentData("Option 3 Button", 1188, 34);
	public static final ComponentData option4ButtonData = new ComponentData("Option 4 Button", 1188, 36);
	public static final ComponentData option5ButtonData = new ComponentData("Option 5 Button", 1188, 38);
	
	// 31 Text: Teleport to the Wilderness?
	
	// 0 Text: Yes.
	// 32 Text: No.
	
	public ChatOptionPane(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1188);
		
		if (widget != null && widget.validate(1188)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	/**
	 * 
	 * @param index 1 to 5?
	 * @return
	 */
	public boolean chooseOption(int index) {
		return chooseOption(index, false);
	}
	
	/**
	 * 
	 * @param index 1 to 5?
	 * @param waitAfter
	 * @return
	 */
	public boolean chooseOption(int index, boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component optionButton = null;
		switch (index) {
		case 1:
			optionButton = option1ButtonData.getComponent();
			break;
		case 2:
			optionButton = option2ButtonData.getComponent();
			break;
		case 3:
			optionButton = option3ButtonData.getComponent();
			break;
		case 4:
			optionButton = option4ButtonData.getComponent();
			break;
		case 5:
			optionButton = option5ButtonData.getComponent();
			break;
		}
		
		if (optionButton != null && optionButton.isVisible()) {
			boolean result = optionButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean chooseOption(String containsText) {
		return chooseOption(containsText, false);
	}
	
	public boolean chooseOption(String containsText, boolean waitAfter) {
		int index = getOptionIndex(containsText);
		if (index != -1)
			return chooseOption(index, waitAfter);
		
		return false;
	}
	
	public boolean chooseYes() {
		return chooseYes(false);
	}
	
	public boolean chooseYes(boolean waitAfter) {
		return chooseOption("Yes", waitAfter);
	}
	
	public boolean chooseNo() {
		return chooseNo(false);
	}
	
	public boolean chooseNo(boolean waitAfter) {
		return chooseOption("No", waitAfter);
	}
	
	/**
	 * 
	 * @param index 1 to 5?
	 * @return
	 */
	public String getOptionText(int index) {
		if (isClosed())
			return null;
		
		Component optionButton = null;
		switch (index) {
		case 1:
			optionButton = option1ButtonData.getComponent();
			break;
		case 2:
			optionButton = option2ButtonData.getComponent();
			break;
		case 3:
			optionButton = option3ButtonData.getComponent();
			break;
		case 4:
			optionButton = option4ButtonData.getComponent();
			break;
		case 5:
			optionButton = option5ButtonData.getComponent();
			break;
		}
		
		if (optionButton != null)
			return optionButton.getText();
		
		return null;
	}
	
	/**
	 * 
	 * @return index of option 1 to 5?
	 */
	public int getOptionIndex(String containsText) {
		if (isClosed())
			return -1;
		
		for (int i = 1; i <= 5; i++) {
			String option = getOptionText(i);
			if (option != null) {
				if (option.contains(containsText))
					return i;
			}
		}
		
		return -1;
	}
	
	public String getHeader() {
		if (isOpen()) {
			Component headerComp = headerData.getComponent();
			if (headerComp != null)
				return headerComp.getText();
		}
		
		return null;
	}
}
