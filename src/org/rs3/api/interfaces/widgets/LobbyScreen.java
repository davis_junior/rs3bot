package org.rs3.api.interfaces.widgets;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.database.ServerInfo;
import org.rs3.database.ServerRow;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Time;

public class LobbyScreen extends ClosableWidget {
	
	public static final LobbyScreen get = new LobbyScreen(906);
	
	public static final ComponentData messageData = new ComponentData("Message", 906, 599); // visible always false
	public static final ComponentData messageBackButtonData = new ComponentData("Message Back Button", 906, 605); // visible always false
	public static final ComponentData playerInfoTabButtonData = new ComponentData("Player Info Tab Button", 906, 104, 3);
	public static final ComponentData worldSelectTabButtonData = new ComponentData("World Select Tab Button", 906, 104, 7);
	public static final ComponentData friendsTabButtonData = new ComponentData("Friends Tab Button", 906, 104, 11);
	public static final ComponentData friendsChatTabButtonData = new ComponentData("Friends Chat Tab Button", 906, 104, 15);
	public static final ComponentData clanChatTabButtonData = new ComponentData("Clan Chat Tab Button", 906, 104, 19);
	public static final ComponentData optionsTabButtonData = new ComponentData("Options Tab Button", 906, 104, 23);
	public static final ComponentData selectedTabData = new ComponentData("Selected Tab", 906, 104, 27);
	public static final ComponentData playButtonData = new ComponentData("Play Button", 906, 137);
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 906, 313, 1);
	
	public static final ComponentData subWidgetTitleData = new ComponentData("Sub Widget Title", 906, 338); // Text: VALIDATE YOUR EMAIL
	public static final ComponentData resendEmailButtonData = new ComponentData("Resend E-mail Button", 906, 350); // Actions: Resend E-mail
	
	public enum LobbyTab {
		
		PLAYER_INFO("Player Info", playerInfoTabButtonData),
		WORLD_SELECT("World Select", worldSelectTabButtonData),
		FRIENDS("Friends", friendsTabButtonData),
		FRIENDS_CHAT("Friends Chat", friendsChatTabButtonData),
		CLAN_CHAT("Clan Chat", clanChatTabButtonData),
		OPTIONS("Options", optionsTabButtonData),
		;
		
		String name;
		ComponentData buttonData;
		
		private LobbyTab(String name, ComponentData buttonData) {
			this.name = name;
			this.buttonData = buttonData;
		}
		
		public String getName() {
			return name;
		}
		
		public Component getComponent() {
			return buttonData.getComponent();
		}
		
		public boolean isSelected() {
			return this.equals(getSelected());
		}
		
		public boolean select() {
			return select(false);
		}
		
		public boolean select(boolean waitAfter) {
			if (isSelected())
				return true;
			
			Component button = buttonData.getComponent();
			if (button != null && button.isVisible()) {
				boolean result = button.interact(this.name);
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
			
			return false;
		}
		
		public static LobbyTab getSelected() {
			Component selectedComp = selectedTabData.getComponent();
			if (selectedComp != null && selectedComp.isVisible()) {
				String selectedName = selectedComp.getText();
				for (LobbyTab tab : values()) {
					if (selectedName.equals(tab.name))
						return tab;
				}
			}
			
			return null;
		}
	}
	
	// ? Text:
	
	// Action: "Play"
	// Text:
	
	public LobbyScreen(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(906);
		
		if (widget != null && widget.validate(906)) {
			// widget validation is all that is needed
			//Component lobbyScreen = playButtonData.getComponent();
			//return lobbyScreen != null && lobbyScreen.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}

	@Override
	public boolean close(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component closeButton = closeButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Logout");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}

	@Override
	public boolean close() {
		return close(false);
	}
	
	public boolean clickPlayButton() {
		return clickPlayButton(false);
	}
	
	public boolean clickPlayButton(boolean waitAfter) {
		if (isClosed())
			return true;
		
		Component playButton = playButtonData.getComponent();
		if (playButton != null && playButton.isVisible()) {
			boolean result = playButton.interact("Play");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean clickMessageBackButton() {
		return clickMessageBackButton(false);
	}
	
	public boolean clickMessageBackButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		if (!isMessageVisible())
			return false;
		
		Component backButton = messageBackButtonData.getComponent();
		if (backButton != null && backButton.validate()) {
			String[] actions = backButton.getActions();
				if (actions != null) {
				boolean result = backButton.interact(actions[0]);
				if (waitAfter)
					Time.sleepQuick();
				return result;
			}
		}
		
		return false;
	}
	
	public String getMessageText() {
		Component messageComp = messageData.getComponent();
		if (messageComp != null && messageComp.validate()) {
			return messageComp.getText();
		}
		
		return null;
	}
	
	// TODO: visible is false, find another method besides checking if text is empty
	public boolean isMessageVisible() {
		String msg = getMessageText();
		return msg != null && !msg.trim().isEmpty();
	}
	
	public boolean handleMessage() {
		if (LobbyScreen.get.isMessageVisible()) {
			String msg = LobbyScreen.get.getMessageText();
			if (msg.toLowerCase().contains("you need a member's account")) {
				System.out.println("Not a member and choosing a member's world! Stopping script...");
				ScriptHandler.currentScript.interruptStop();
				LobbyScreen.get.clickMessageBackButton(true);
				return false;
			} else if (msg.toLowerCase().contains("click continue")) {
				LobbyScreen.get.clickMessageBackButton(true);
			} else if (msg.toLowerCase().contains("error connecting to server")) {
				LobbyScreen.get.clickMessageBackButton(true);
			} else if (msg.toLowerCase().contains("login limit exceeded")) {
				System.out.println("Login limit exceeded, try a different world! Stopping script...");
				ScriptHandler.currentScript.interruptStop();
				LobbyScreen.get.clickMessageBackButton(true);
				return false;
			} else if (msg.toLowerCase().contains("too many logins from your address")) {
				Time.sleep(10000);
				LobbyScreen.get.clickMessageBackButton(true);
				return false;
			} else if (msg.toLowerCase().contains("game session has now ended")) {
				System.out.println("Game session ended! Stopping script and restarting bot...");
				ScriptHandler.currentScript.interruptStop();
				Time.sleepMedium();
				
				if (ServerInfo.cur == null)
					System.out.println("ServerInfo is null!");
				else {
					ServerRow row = ServerInfo.cur.getRow();
					row.setKillBot(false);
					row.setRestartBot(true);
					row.executeUpdate();
				}
				
				return false;
			} else if (msg.toLowerCase().contains("logging in")) {
				
			} else {
				System.out.println("Unknown lobby message: " + msg);
				Time.sleep(10000);
			}
		}
		
		return true;
	}
	
	public String getSubWidgetTitleText() {
		Component subWidgetTitleComp = subWidgetTitleData.getComponent();
		if (subWidgetTitleComp != null && subWidgetTitleComp.isVisible()) {
			return subWidgetTitleComp.getText();
		}
		
		return null;
	}
	
	public boolean isSubWidgetVisible() {
		return getSubWidgetTitleText() != null;
	}
	
	public boolean isValidateEmailScreenVisible() {
		if (isSubWidgetVisible()) {
			String subWidgetTitle = getSubWidgetTitleText();
			if (subWidgetTitle != null) {
				return subWidgetTitle.toUpperCase().equals("VALIDATE YOUR EMAIL");
			}
		}
		
		return false;
	}
	
	public boolean clickResendEmailButton() {
		return clickResendEmailButton(false);
	}
	
	public boolean clickResendEmailButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component resendEmailButton = resendEmailButtonData.getComponent();
		if (resendEmailButton != null && resendEmailButton.isVisible()) {
			boolean result = resendEmailButton.interact("Resend E-mail");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
}
