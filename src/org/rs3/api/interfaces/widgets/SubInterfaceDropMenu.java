package org.rs3.api.interfaces.widgets;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Time;

// Same as LobbyScreenDropMenu
public class SubInterfaceDropMenu extends WidgetData {
	
	public static final SubInterfaceDropMenu get = new SubInterfaceDropMenu(742);
	
	public static final ComponentData optionsData = new ComponentData("Options", 742, 5);
	
	public SubInterfaceDropMenu(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Component options = optionsData.getComponent();
		return options != null && options.isVisible();
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	
	public Component[] getOptions() {
		Component options = optionsData.getComponent();
		if (options != null && options.isVisible()) {
			Array1D<Component> array = options.getComponents();
			if (array != null)
				return array.getAll();
		}
		
		return null;
	}
	
	public boolean chooseOption(int index) {
		return chooseOption(index, false);
	}
	
	public boolean chooseOption(int index, boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component[] options = getOptions();
		
		if (options != null && index >= 0 && index < options.length) {
			Component option = options[index];
			if (option != null && option.isVisible()) {
				boolean result = option.click(Mouse.LEFT);
				
				if (waitAfter)
					Time.sleepQuick();
				
				return result;
			}
		}
		
		return false;
	}
	
	public boolean chooseOption(String containsText) {
		return chooseOption(containsText, false);
	}
	
	public boolean chooseOption(String containsText, boolean waitAfter) {
		int index = getOptionIndex(containsText);
		if (index != -1)
			return chooseOption(index, waitAfter);
		
		return false;
	}
	
	public String getOptionText(int index) {
		if (isClosed())
			return null;
		
		Component[] options = getOptions();
		
		if (options != null && index >= 0 && index < options.length) {
			Component option = options[index];
			if (option != null && option.isVisible())
				return option.getText();
		}
		
		return null;
	}
	
	public int getOptionIndex(String containsText) {
		if (isClosed())
			return -1;
		
		Component[] options = getOptions();
		if (options != null) {
			for (int i = 0; i < options.length; i++) {
				String option = getOptionText(i);
				if (option != null) {
					if (option.contains(containsText))
						return i;
				}
			}
		}
		
		return -1;
	}
}
