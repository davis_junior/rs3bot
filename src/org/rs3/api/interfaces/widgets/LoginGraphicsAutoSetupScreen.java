package org.rs3.api.interfaces.widgets;

import java.awt.event.KeyEvent;

import org.rs3.api.Interfaces;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ClosableWidget;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class LoginGraphicsAutoSetupScreen extends WidgetData {
	
	public static final LoginGraphicsAutoSetupScreen get = new LoginGraphicsAutoSetupScreen(976);
	
	public static final ComponentData autoSetupButtonData = new ComponentData("Auto Setup Button Data", 976, 25); // actions are null
	public static final ComponentData textData = new ComponentData("Text Data", 976, 26);

	
	public LoginGraphicsAutoSetupScreen(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(976);
		
		if (widget != null && widget.validate(976)) {
			// widget validation is all that is needed
			//Component loginScreen = loginButtonData.getComponent();
			//return loginScreen != null && loginScreen.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	public String getText() {
		Component text = textData.getComponent();
		if (text != null && text.validate())
			return text.getText();
		
		return null;
	}
	
	public boolean clickAutoSetupButton() {
		return clickAutoSetupButton(false);
	}
	
	public boolean clickAutoSetupButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component autoSetupButton = autoSetupButtonData.getComponent();
		if (autoSetupButton != null && autoSetupButton.isVisible()) {
			boolean clicked = autoSetupButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return clicked;
		}
		
		return false;
	}
}
