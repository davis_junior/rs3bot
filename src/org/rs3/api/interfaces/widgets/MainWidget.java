package org.rs3.api.interfaces.widgets;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.rs3.api.Interfaces;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

public class MainWidget extends WidgetData {
	
	public static final MainWidget get = new MainWidget(1477);
	
	public static final ComponentData ribbonBoundsData = new ComponentData("Ribbon Bounds", 1477, 38);
	public static final ComponentData actionBarBoundsData = new ComponentData("Action Bar Bounds", 1477, 44);
	public static final ComponentData minimapBoundsData = new ComponentData("Minimap Bounds", 1477, 69);
	public static final ComponentData chatBoxBoundsData = new ComponentData("Chat Box Bounds", 1477, 98);
	
	public static final ComponentData minimapLogOutButton = new ComponentData("Minimap Logout Button", 1477, 74, 1);
	public static final ComponentData actionBarMinimizeButton = new ComponentData("Action Bar Minimize Button", 1477, 48, 1);
	
	/**
	 * To test visibility: is visible when hidden flag is false, and hidden when hidden flag is true.
	 */
	public static final ComponentData hoveredRibbonTabsBounds = new ComponentData("Hovered Ribbon Tabs Bounds", 1477, 566);
	
	//
	// Tabs
	// 		- These components are only used to test tab visibility, each has its own separate widget for other uses.
	//
	public static final ComponentData tabBackpackBounds = new ComponentData("Backpack Tab Bounds", 1477, 87);
	public static final ComponentData tabWornEquipmentBounds = new ComponentData("Worn Equipment Tab Bounds", 1477, 162);
	public static final ComponentData tabPrayersBounds = new ComponentData("Prayers Tab Bounds", 1477, 184);
	public static final ComponentData tabMeleeAbilitiesBounds = new ComponentData("Melee Abilities Tab Bounds", 1477, 195);
	public static final ComponentData tabRangedAbilitiesBounds = new ComponentData("Ranged Abilities Tab Bounds", 1477, 206);
	public static final ComponentData tabMagicAbilitiesBounds = new ComponentData("Magic Abilities Tab Bounds", 1477, 217);
	public static final ComponentData tabDefenseAbilitiesBounds = new ComponentData("Defense Abilities Tab Bounds", 1477, 228);
	public static final ComponentData tabSkillsBounds = new ComponentData("Skills Tab Bounds", 1477, 249);
	public static final ComponentData tabActiveTaskBounds = new ComponentData("Active Task Tab Bounds", 1477, 260);
	public static final ComponentData tabMusicPlayerBounds = new ComponentData("Music Player Tab Bounds", 1477, 271);
	public static final ComponentData tabFriendsBounds = new ComponentData("Friends Tab Bounds", 1477, 282);
	public static final ComponentData tabClanBounds = new ComponentData("Clan Tab Bounds", 1477, 293);
	public static final ComponentData tabGroupMembersBounds = new ComponentData("Group Members Tab Bounds", 1477, 304);
	public static final ComponentData tabNotesBounds = new ComponentData("Notes Tab Bounds", 1477, 315);
	public static final ComponentData tabFriendsChatInfoBounds = new ComponentData("Friends Chat Info Tab Bounds", 1477, 326);
	// TODO: Twitch Stream tab
	
	//
	// Tab close buttons
	//	- action is "Close Window"
	//	- can also be used to test visibility (if tab is opened)
	//
	public static final ComponentData tabBackPackCloseButton = new ComponentData("Backpack Tab Close Button", 1477, 93, 1);
	public static final ComponentData tabWornEquipmentCloseButton = new ComponentData("Worn Equipment Tab Close Button", 1477, 168, 1);
	public static final ComponentData tabPrayersCloseButton = new ComponentData("Prayers Tab CloseButton", 1477, 190, 1);
	public static final ComponentData tabMeleeAbilitiesCloseButton = new ComponentData("Melee Abilities Tab Close Button", 1477, 201, 1);
	public static final ComponentData tabRangedAbilitiesCloseButton = new ComponentData("Ranged Abilities Tab Close Button", 1477, 212, 1);
	public static final ComponentData tabMagicAbilitiesCloseButton = new ComponentData("Magic Abilities Tab Close Close Button", 1477, 223, 1);
	public static final ComponentData tabDefenseAbilitiesCloseButton = new ComponentData("Defense Abilities Tab Close Button", 1477, 234, 1);
	public static final ComponentData tabSkillsCloseButton = new ComponentData("Skills Tab Close Button", 1477, 255, 1);
	public static final ComponentData tabActiveTaskCloseButton = new ComponentData("Active Task Tab Close Button", 1477, 266, 1);
	public static final ComponentData tabMusicPlayerCloseButton = new ComponentData("Music Player Tab Close Button", 1477, 277, 1);
	public static final ComponentData tabFriendsCloseButton = new ComponentData("Friends Tab Close Button", 1477, 288, 1);
	public static final ComponentData tabClanCloseButton = new ComponentData("Clan Tab Bounds", 1477, 300, 1);
	public static final ComponentData tabGroupMembersCloseButton = new ComponentData("Group Members Tab Close Button", 1477, 310, 1);
	public static final ComponentData tabNotesCloseButton = new ComponentData("Notes Tab Bounds", 1477, 321, 1);
	public static final ComponentData tabFriendsChatInfoCloseButton = new ComponentData("Friends Chat Info Tab Close Button", 1477, 332, 1);
	
	public static final ComponentData subInterfaceNameData = new ComponentData("Sub Interface Name", 1477, 501, 16);
	public static final ComponentData subInterfaceBoundsData = new ComponentData("Sub Interface Bounds", 1477, 499); // testable for visibility also
	public static final ComponentData subInterfaceCloseButtonData = new ComponentData("Sub Interface Close Button", 1477, 506, 1);
	
	public static final ComponentData subInterfaceTabButton1Data = new ComponentData("Sub Interface Tab Button 1", 1477, 503, 3);
	public static final ComponentData subInterfaceTabButton2Data = new ComponentData("Sub Interface Tab Button 2", 1477, 503, 7);
	public static final ComponentData subInterfaceTabButton3Data = new ComponentData("Sub Interface Tab Button 3", 1477, 503, 11);
	public static final ComponentData subInterfaceTabButton4Data = new ComponentData("Sub Interface Tab Button 4", 1477, 503, 15);
	public static final ComponentData subInterfaceTabButton5Data = new ComponentData("Sub Interface Tab Button 5", 1477, 503, 19);
	public static final ComponentData subInterfaceTabButton6Data = new ComponentData("Sub Interface Tab Button 6", 1477, 503, 23);
	
	// Use to test visibility: _hidden()
	public static final ComponentData optionsMenuBoundsData = new ComponentData("Options Menu Bounds", 1477, 584);

	
	private static Rectangle[] cachedBounds = null;
	
	public MainWidget(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(1477);
		
		if (widget != null && widget.validate(1477)) {
			// widget validation is all that is needed
			//Component destroyMessage = yesButtonData.getComponent();
			//return destroyMessage != null && destroyMessage.isVisible();
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	/**
	 * Gets all open/visible tabs bound components.
	 * 
	 * @return
	 */
	public Component[] getAllOpenTabBounds() {
		Component[] tabs = new Component[15];
		tabs[0] = tabBackpackBounds.getComponent();
		tabs[1] = tabWornEquipmentBounds.getComponent();
		tabs[2] = tabPrayersBounds.getComponent();
		tabs[3] = tabMeleeAbilitiesBounds.getComponent();
		tabs[4] = tabRangedAbilitiesBounds.getComponent();
		tabs[5] = tabMagicAbilitiesBounds.getComponent();
		tabs[6] = tabDefenseAbilitiesBounds.getComponent();
		tabs[7] = tabSkillsBounds.getComponent();
		tabs[8] = tabActiveTaskBounds.getComponent();
		tabs[9] = tabMusicPlayerBounds.getComponent();
		tabs[10] = tabFriendsBounds.getComponent();
		tabs[11] = tabClanBounds.getComponent();
		tabs[12] = tabGroupMembersBounds.getComponent();
		tabs[13] = tabNotesBounds.getComponent();
		tabs[14] = tabFriendsChatInfoBounds.getComponent();
		
		List<Component> visibleTabs = new ArrayList<Component>();
		for (Component tab : tabs) {
			if (tab != null && tab.isVisible())
				visibleTabs.add(tab);
		}
		
		if (visibleTabs.size() > 0)
			return visibleTabs.toArray(new Component[0]);
		
		return null;
	}
	
	/**
	 * Creates a Rectangle[] containing all main bounds (ribbon, action bar, minimap, chat box, tabs, subinterface, etc.)
	 */
	public void updateBoundsCache() {
		boolean orig_useCachedComponentNodes = Interfaces.useCachedComponentNodes;
		boolean orig_useWidgetComponentCache = Interfaces.useWidgetComponentCache;
		
		Interfaces.useCachedComponentNodes = true;
		Interfaces.useWidgetComponentCache = true;
		Interfaces.updateCache_componentNodes();
		Interfaces.updateCache_widgetCache();
		
		Component ribbonBounds = ribbonBoundsData.getComponent();
		Component actionBarBounds = actionBarBoundsData.getComponent();
		Component minimapBounds = minimapBoundsData.getComponent();
		Component chatBoxBounds = chatBoxBoundsData.getComponent();
		
		if (ribbonBounds != null && actionBarBounds != null && minimapBounds != null && chatBoxBounds != null) {
			List<Rectangle> rects = new ArrayList<>();
			rects.add(ribbonBounds.getBoundingRectangle());
			rects.add(actionBarBounds.getBoundingRectangle());
			rects.add(minimapBounds.getBoundingRectangle());
			rects.add(chatBoxBounds.getBoundingRectangle());
			
			Component subInterfaceBounds = subInterfaceBoundsData.getComponent();
			if (subInterfaceBounds != null && subInterfaceBounds.isVisible())
				rects.add(subInterfaceBounds.getBoundingRectangle());
			
			Component[] openTabs = getAllOpenTabBounds();
			if (openTabs != null) {
				for (Component tab : openTabs)
					rects.add(tab.getBoundingRectangle());
			}
			
			cachedBounds = rects.toArray(new Rectangle[0]);
		} else
			cachedBounds = null;
		
		Interfaces.useCachedComponentNodes = orig_useCachedComponentNodes;
		Interfaces.useWidgetComponentCache = orig_useWidgetComponentCache;
	}
	
	/**
	 * Checks whether any of the main bounds (ribbon, action bar, minimap, chat box, tabs, subinterface, etc.) contain the specified point.
	 * 
	 * @param updateBoundsCache
	 * @param p
	 * @return
	 */
	public boolean contains(boolean updateBoundsCache, Point p) {
		if (updateBoundsCache || cachedBounds == null)
			updateBoundsCache();
		
		if (cachedBounds != null) {
			for (Rectangle rect : cachedBounds) {
				if (rect != null && rect.contains(p))
					return true;
			}
		}
		
		return false;
	}
	
	/**
	 * False if out of client bounds or in main bound points (ribbon, action bar, minimap, chat box, tabs, subinterface, etc.)
	 * 
	 * @return
	 */
	public boolean isPointOnScreen(boolean updateBoundsCache, Point p) {
		if (Mouse.outOfBounds(p.x, p.y))
			return false;
		
		return !contains(updateBoundsCache, p);
	}
	
	/**
	 * Skips main bound points (ribbon, action bar, minimap, chat box, tabs, subinterface, etc.)
	 * 
	 * @return
	 */
	public Point getRandomOnScreenPoint() {
		updateBoundsCache();
		
		Point p = Antiban.getRandomClientPoint();
		while (contains(false, p)) {
			p = Antiban.getRandomClientPoint();
		}
		
		return p;
	}
	
	public boolean isSubInterfaceOpen() {
		Component bounds = subInterfaceBoundsData.getComponent();
		return bounds != null && bounds.isVisible();
	}
	
	public String getSubInterfaceName() {
		if (isSubInterfaceOpen()) {
			Component nameComp = subInterfaceNameData.getComponent();
			if (nameComp != null) {
				return nameComp.getText();
			}
		}
		
		return null;
	}
	
	public boolean closeSubInterface() {
		return closeSubInterface(false);
	}
	
	public boolean closeSubInterface(boolean waitAfter) {
		if (!isSubInterfaceOpen()) {
			if (OptionsMenuWidget.get.isOpen())
				return OptionsMenuWidget.get.close(waitAfter);
			
			return true;
		}
		
		Component closeButton = subInterfaceCloseButtonData.getComponent();
		if (closeButton != null && closeButton.isVisible()) {
			boolean result = closeButton.interact("Close Window");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean clickMinimapLogoutButton() {
		return clickMinimapLogoutButton(false);
	}
	
	public boolean clickMinimapLogoutButton(boolean waitAfter) {
		if (LogoutWidget.get.isOpen())
			return true;
		
		Component logoutButton = minimapLogOutButton.getComponent();
		if (logoutButton != null && logoutButton.isVisible()) {
			boolean result = logoutButton.interact("Logout");
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean isActionBarMinimized() {
		Component minimizeButton = actionBarMinimizeButton.getComponent();
		if (minimizeButton != null && minimizeButton.isVisible()) {
			String[] actions = minimizeButton.getActions();
			if (actions != null) {
				for (String action : actions) {
					if (action.equals("Maximise"))
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean isActionBarMaximised() {
		Component minimizeButton = actionBarMinimizeButton.getComponent();
		if (minimizeButton != null && minimizeButton.isVisible()) {
			String[] actions = minimizeButton.getActions();
			if (actions != null) {
				for (String action : actions) {
					if (action.equals("Minimise"))
						return true;
				}
			}
		}
		
		return false;
	}
	
	@SuppressWarnings("unused")
	private boolean interactActionBarMinimizeButton(String action) {
		return interactActionBarMinimizeButton(action, false);
	}
	
	private boolean interactActionBarMinimizeButton(String action, boolean waitAfter) {
		Component minimizeButton = actionBarMinimizeButton.getComponent();
		if (minimizeButton != null && minimizeButton.isVisible()) {
			boolean result = minimizeButton.interact(action);
			if (waitAfter)
				Time.sleepQuick();
			return result;
		}
		
		return false;
	}
	
	public boolean minimiseActionBar() {
		return minimiseActionBar(false);
	}
	
	public boolean minimiseActionBar(boolean waitAfter) {
		if (isActionBarMinimized())
			return true;
		
		return interactActionBarMinimizeButton("Minimise");
	}
	
	public boolean maximiseActionBar() {
		return maximiseActionBar(false);
	}
	
	public boolean maximiseActionBar(boolean waitAfter) {
		if (isActionBarMaximised())
			return true;
		
		return interactActionBarMinimizeButton("Maximise");
	}
	
	public boolean isWeaponSheathed() {
		int sheathedSetting = Client.getSettingData().getSettings().get(627, 8, 0x1);
		return sheathedSetting == 0; // 1 when unsheathed
	}
	
	public boolean sheatheWeapon() {
		return sheatheWeapon(false);
	}
	
	public boolean sheatheWeapon(boolean waitAfter) {
		if (isWeaponSheathed())
			return true;
		
		return interactActionBarMinimizeButton("Sheathe/Unsheathe Weapon");
	}
	
	public boolean unsheatheWeapon() {
		return unsheatheWeapon(false);
	}
	
	public boolean unsheatheWeapon(boolean waitAfter) {
		if (!isWeaponSheathed())
			return true;
		
		return interactActionBarMinimizeButton("Sheathe/Unsheathe Weapon");
	}
	
	// tutorial: 3926 = 1, 3927 = 2, both are 0 when tutorial is finished
	public static boolean isTutorialActive() {
		int tutorialSetting = Client.getSettingData().getSettings().get(3926, 0x1);
		return tutorialSetting == 1; // 1 when tutorial active
	}
}
