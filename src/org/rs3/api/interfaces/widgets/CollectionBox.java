package org.rs3.api.interfaces.widgets;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.rs3.api.Calculations;
import org.rs3.api.Interfaces;
import org.rs3.api.GroundObjects;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.OpenableClosableWidget;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.Widget;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class CollectionBox extends OpenableClosableWidget {
	
	public static final CollectionBox get = new CollectionBox(109);
	
	public static final ComponentData closeButtonData = new ComponentData("Close Button", 109, 42);
	public static final ComponentData collectAllToBankButtonData = new ComponentData("Collect All to Bank Button", 109, 47);
	public static final ComponentData collectAllToInventoryButtonData = new ComponentData("Collect All to Inventory Button", 109, 55);
	
	public CollectionBox(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(109);
		
		if (widget != null && widget.validate(109)) {
			// widget validation is all that is needed
			//Component collectionBox = closeButtonData.getComponent();
			//return collectionBox != null && collectionBox.isVisible();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	@Override
	public boolean open() {
		return open(false);
	}
	
	// about the same as Bank#open
	@Override
	public boolean open(boolean waitAfter) {
		if (isOpen())
			return true;
		
		Interactable bank = Bank.get.getNearest();
		
		if (bank != null) {
			if (Calculations.distanceTo(bank.getTile()) > 3)
				if (!bank.getTile().loopWalkTo(10000)) {
					if (waitAfter)
						Time.sleepQuick();
					
					return false;
				}
			
			Paint.updatePaintObject(bank);
			
			if (bank.loopInteract(getIsOpenCondition(), "Collect") || isOpen()) {
				Timer timer = new Timer(7000);
				while (timer.isRunning()) {
					if (isOpen()) {
						if (waitAfter)
							Time.sleepQuick();
						
						return true;
					}
					
					Time.sleep(50);
				}
			}
		}
		
		if (waitAfter)
			Time.sleepQuick();
		
		return false;
	}
	
	@Override
	public boolean close() {
		return close(false);
	}
	
	@Override
	public boolean close(boolean waitOnClose) {
		if (!isOpen())
			return true;
		
		Component closeComp = closeButtonData.getComponent();
		if (closeComp != null) {
			Component closeButton = closeComp;
			
			if (closeButton != null && closeButton.isVisible()) {
				boolean result = closeButton.interact("Close");
				
				if (waitOnClose)
					Time.sleepVeryQuick();
				
				return result;
			}
		}
		
		return false;
	}
	
	public boolean collectAllToBank() {
		Component bankButton = collectAllToBankButtonData.getComponent();
		if (bankButton != null && bankButton.isVisible()) {
			return bankButton.interact("Select");
		}
		
		return false;
	}
	
	public boolean collectAllToInventory() {
		Component invButton = collectAllToInventoryButtonData.getComponent();
		if (invButton != null && invButton.isVisible()) {
			return invButton.interact("Select");
		}
		
		return false;
	}
}
