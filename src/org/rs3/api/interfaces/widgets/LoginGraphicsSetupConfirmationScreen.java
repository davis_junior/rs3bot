package org.rs3.api.interfaces.widgets;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.WidgetData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Time;

// likely used for other screens too
public class LoginGraphicsSetupConfirmationScreen extends WidgetData {
	
	public static final LoginGraphicsSetupConfirmationScreen get = new LoginGraphicsSetupConfirmationScreen(744);
	
	public static final ComponentData yesButtonData = new ComponentData("Yes Button Data", 744, 121); // actions are null

	
	public LoginGraphicsSetupConfirmationScreen(int widgetId) {
		super(widgetId);
	}
	
	
	@Override
	public boolean isOpen() {
		Widget widget = Client.getWidgetCache().get(744);
		
		if (widget != null && widget.validate(744)) {
			Component yesButton = yesButtonData.getComponent();
			return yesButton != null && yesButton.isVisible();
		}
		
		return false;
	}
	
	@Override
	public boolean isClosed() {
		return !isOpen();
	}
	
	
	public boolean clickYesButton() {
		return clickYesButton(false);
	}
	
	public boolean clickYesButton(boolean waitAfter) {
		if (isClosed())
			return false;
		
		Component yesButton = yesButtonData.getComponent();
		if (yesButton != null && yesButton.isVisible()) {
			boolean clicked = yesButton.click(Mouse.LEFT);
			if (waitAfter)
				Time.sleepQuick();
			return clicked;
		}
		
		return false;
	}
}
