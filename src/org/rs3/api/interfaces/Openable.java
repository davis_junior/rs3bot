package org.rs3.api.interfaces;

public interface Openable {
	
	public boolean isOpen();
	public boolean open();
	public boolean open(boolean waitAfter);
	
}
