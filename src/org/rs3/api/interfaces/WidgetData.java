package org.rs3.api.interfaces;

import org.rs3.util.Condition;

public abstract class WidgetData {
	
	int widgetId;
	
	final Condition isOpenCondition;
	final Condition isClosedCondition;
	
	public WidgetData(int widgetId) {
		this.widgetId = widgetId;
		
		isOpenCondition = new Condition() {
			@Override
			public boolean evaluate() {
				return isOpen();
			}
		};
		
		isClosedCondition = new Condition() {
			@Override
			public boolean evaluate() {
				return isClosed();
			}
		};
	}
	
	public abstract boolean isOpen();
	public abstract boolean isClosed();
	
	public Condition getIsOpenCondition() {
		return isOpenCondition;
	}
	
	public Condition getIsClosedCondition() {
		return isClosedCondition;
	}
}
