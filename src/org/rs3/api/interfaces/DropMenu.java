package org.rs3.api.interfaces;

import org.rs3.api.interfaces.widgets.LobbyScreenDropMenu;
import org.rs3.api.interfaces.widgets.SubInterfaceDropMenu;
import org.rs3.api.wrappers.Component;

public class DropMenu {

	public static boolean isOpen() {
		return SubInterfaceDropMenu.get.isOpen() || LobbyScreenDropMenu.get.isOpen();
	}
	
	public static boolean isClosed() {
		return !isOpen();
	}
	
	public static Component[] getOptions() {
		if (SubInterfaceDropMenu.get.isOpen())
			return SubInterfaceDropMenu.get.getOptions();
		else if (LobbyScreenDropMenu.get.isOpen())
			return LobbyScreenDropMenu.get.getOptions();
		
		return null;
	}
	
	public static boolean chooseOption(int index) {
		return chooseOption(index, false);
	}
	
	public static boolean chooseOption(int index, boolean waitAfter) {
		if (SubInterfaceDropMenu.get.isOpen())
			return SubInterfaceDropMenu.get.chooseOption(index, waitAfter);
		else if (LobbyScreenDropMenu.get.isOpen())
			return LobbyScreenDropMenu.get.chooseOption(index, waitAfter);
		
		return false;
	}
	
	public static boolean chooseOption(String containsText) {
		return chooseOption(containsText, false);
	}
	
	public static boolean chooseOption(String containsText, boolean waitAfter) {
		if (SubInterfaceDropMenu.get.isOpen())
			return SubInterfaceDropMenu.get.chooseOption(containsText, waitAfter);
		else if (LobbyScreenDropMenu.get.isOpen())
			return LobbyScreenDropMenu.get.chooseOption(containsText, waitAfter);
		
		return false;
	}
	
	public static String getOptionText(int index) {
		if (SubInterfaceDropMenu.get.isOpen())
			return SubInterfaceDropMenu.get.getOptionText(index);
		else if (LobbyScreenDropMenu.get.isOpen())
			return LobbyScreenDropMenu.get.getOptionText(index);
		
		return null;
	}
	
	public static int getOptionIndex(String containsText) {
		if (SubInterfaceDropMenu.get.isOpen())
			return SubInterfaceDropMenu.get.getOptionIndex(containsText);
		else if (LobbyScreenDropMenu.get.isOpen())
			return LobbyScreenDropMenu.get.getOptionIndex(containsText);
		
		return -1;
	}
}
