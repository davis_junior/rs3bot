package org.rs3.api.interfaces;

public interface Closeable {
	
	public boolean isClosed();
	public boolean close();
	public boolean close(boolean waitAfter);
	
}
