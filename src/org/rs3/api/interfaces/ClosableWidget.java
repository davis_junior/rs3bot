package org.rs3.api.interfaces;

public abstract class ClosableWidget extends WidgetData implements Closeable {

	public ClosableWidget(int widgetId) {
		super(widgetId);
	}

}
