package org.rs3.api.interfaces;

public abstract class OpenableClosableWidget extends WidgetData implements Openable, Closeable {

	public OpenableClosableWidget(int widgetId) {
		super(widgetId);
	}
	
}
