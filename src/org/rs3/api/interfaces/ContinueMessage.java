package org.rs3.api.interfaces;

import org.rs3.api.interfaces.widgets.ContinueMessageGame;
import org.rs3.api.interfaces.widgets.ContinueMessageGameLeftIcon;
import org.rs3.api.interfaces.widgets.ContinueMessageMyPlayerRightIcon;
import org.rs3.api.interfaces.widgets.ContinueMessageNpcLeftIcon;

public class ContinueMessage {

	public static boolean isOpen() {
		return ContinueMessageGame.get.isOpen() || ContinueMessageMyPlayerRightIcon.get.isOpen() || ContinueMessageNpcLeftIcon.get.isOpen() || ContinueMessageGameLeftIcon.get.isOpen();
	}
	
	public static boolean isClosed() {
		return !isOpen();
	}
	
	public static boolean close() {
		return close(false);
	}
	
	public static boolean close(boolean waitAfter) {
		if (ContinueMessageGame.get.isOpen())
			return ContinueMessageGame.get.close(waitAfter);
		else if (ContinueMessageMyPlayerRightIcon.get.isOpen())
			return ContinueMessageMyPlayerRightIcon.get.close(waitAfter);
		else if (ContinueMessageNpcLeftIcon.get.isOpen())
			return ContinueMessageNpcLeftIcon.get.close(waitAfter);
		else if (ContinueMessageGameLeftIcon.get.isOpen())
			return ContinueMessageGameLeftIcon.get.close(waitAfter);
		
		return true;
	}
	
	public static String getText(boolean removeHtml) {
		if (ContinueMessageGame.get.isOpen())
			return ContinueMessageGame.get.getText(removeHtml);
		else if (ContinueMessageMyPlayerRightIcon.get.isOpen())
			return ContinueMessageMyPlayerRightIcon.get.getText(removeHtml);
		else if (ContinueMessageNpcLeftIcon.get.isOpen())
			return ContinueMessageNpcLeftIcon.get.getText(removeHtml);
		else if (ContinueMessageGameLeftIcon.get.isOpen())
			return ContinueMessageGameLeftIcon.get.getText(removeHtml);
		
		return null;
	}
	
	public static String getHeader() {
		if (ContinueMessageGame.get.isOpen())
			return "";
		else if (ContinueMessageMyPlayerRightIcon.get.isOpen())
			return ContinueMessageMyPlayerRightIcon.get.getHeader();
		else if (ContinueMessageNpcLeftIcon.get.isOpen())
			return ContinueMessageNpcLeftIcon.get.getHeader();
		else if (ContinueMessageGameLeftIcon.get.isOpen())
			return "";
		
		return null;
	}
}
