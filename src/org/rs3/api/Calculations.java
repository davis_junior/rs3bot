package org.rs3.api;

import java.awt.Point;

import org.rs3.accessors.I_Client;
import org.rs3.accessors.Reflection;
import org.rs3.api.interfaces.widgets.MiniMap;
import org.rs3.api.objects.BasicInteractable;
import org.rs3.api.objects.PreciseTile;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.TileData;
import org.rs3.api.wrappers.Viewport;


public class Calculations {

	public static int internalCoordToRelative(int coord) {
		return (coord >> 9);
	}
	
	public static int internalCoordToRelative(float coord) {
		return (Math.round(coord) >> 9);
	}
	
	public static Point groundToScreen(int x, int y, int plane) {
		return groundToScreen(x, y, plane, 0);
	}
	
	public static Point groundToScreen(int x, int y, int plane, int heightOff) {
		try {
			int z = tileHeight(x, y, plane) - heightOff;
			return worldToScreen(x, z, y);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Point locationToScreen(int x, int y, int plane) {
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		x -= base.getX();
		y -= base.getY();
		
		return groundToScreen((int) ((x + 0.5) * 512), (int) ((y + 0.5) * 512), plane);
	}
	
	public static int tileHeight(int internalX, int internalY, int plane) {
		try {
			int x1 = internalX >> 9;
			int y1 = internalY >> 9;
			
			byte[][][] settings = Client.getGameInfo().getGroundBytes().getBytes();
			
			if (settings != null && x1 >= 0 && x1 < 104 && y1 >= 0 && y1 < 104) {
				if (plane <= 3 && (settings[1][x1][y1] & 2) != 0)
					++plane;
				
				Array1D<TileData> planes = Client.getGameInfo().getGroundInfo().getTileData();
				
				if (planes != null && plane < planes.getLength()) {
					TileData planeData = planes.get(plane);
					
					if (planeData != null) {
						int[][] heights = planeData.getHeights();
						
						if (heights != null) {
							int x2 = internalX & 0x200 - 1;
							int y2 = internalY & 0x200 - 1;
							int start_h = heights[x1][y1] * (0x200 - x2) + heights[x1 + 1][y1] * x2 >> 9;
							int end_h = heights[x1][1 + y1] * (0x200 - x2) + heights[x1 + 1][y1 + 1] * x2 >> 9;
				
							return start_h * (512 - y2) + end_h * y2 >> 9;
						}
					}
				}
			}
		} catch(Exception e) { 
			
		}
		
		return 0;
	}
	
	// old; powerbot method modified
	// TODO: verify; minimap angle is not used in rs3 so we calculate with Camera.getYaw (camera angle); calc used from powerbot
	/*public static Point worldToMap(double x, double y) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		x -= base.getX();
		y -= base.getY();
		
		Location localLoc = player.getData().getCenterLocation();
		PreciseTile localTile = localLoc.getLocalPreciseTile(true);
		int calculatedX = (int) (x * 4 + 2) - ((int) localTile.getX()) / 0x80;
		int calculatedY = (int) (y * 4 + 2) - ((int) localTile.getY()) / 0x80;
		
		Component mm2 = Interfaces.getMap();
		if (mm2 == null)
			return null;
		
		int actDistSq = calculatedX * calculatedX + calculatedY * calculatedY;
		int mmDist = Math.max(mm2.getWidth() / 2, mm2.getHeight() / 2) - 8;
		
		if (mmDist * mmDist >= actDistSq) {
			double angle = Camera.getYaw() * 0.0174532925199433D * 2607.5945876176133D;
			System.out.println(angle);
			boolean setting4 = Client.getMinimapSetting() == data.Client.get.minimapSetting_check.getMultiplier().intValue();	
			
			if (!setting4)
				angle = 0x3fff & (Client.getMinimapOffset()) + (int) angle;
			
			int newAngle = 0x3fff & (int) angle;
			
			int cs = Calculations.SIN_TABLE[newAngle];
			int cc = Calculations.COS_TABLE[newAngle];
			
			if (!setting4) {
				int fact = 0x100 + (Client.getMinimapScale());
				cs = 0x100 * cs / fact;
				cc = 0x100 * cc / fact;
			}
			
			int calcCenterX = cc * calculatedX + cs * calculatedY >> 0xf;
			int calcCenterY = cc * calculatedY - cs * calculatedX >> 0xf;
			
			Point mmAbsolute = mm2.getAbsoluteLocation();
			int screen_x = calcCenterX + mmAbsolute.x + mm2.getWidth() / 2;
			int screen_y = -calcCenterY + mmAbsolute.y + mm2.getHeight() / 2;
			//System.out.println(calcCenterX + ", " + calcCenterY);
			//System.out.println(screen_x + ", " + screen_y);
			return new Point(screen_x, screen_y);
		}
		
		return null;
	}*/
	
	// referenced to own - 836 - ay.bo(...)
	// all verified besides the TODO
	public static Point worldToMap(int absX, int absY) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		int x = absX - base.getX();
		int y = absY - base.getY();
		
		// if minimap setting is "4" (own - 836), two static ints are used rather than player location floats (though they're likely the same)
		PreciseTile playerITile = player.getData().getCenterLocation().getLocalPreciseTile(true);
		// the following coords are based on a quarter of the internal coords  (128 units, not 512)
		x = (int) (x * 4 + 2) - (int)(playerITile.getX()) / 128; // + 2 is in the client, just not in every w2mm method
		y = (int) (y * 4 + 2) - (int)(playerITile.getY()) / 128; // + 2 is in the client, just not in every w2mm method
		
		Component mmBorder = MiniMap.mapBounds.getComponent();
		if (mmBorder == null)
			return null;
		
		int maxDist = Math.max(mmBorder.getHorizontalScrollbarThumbSize() / 2, mmBorder.getVerticalScrollbarThumbSize() / 2) + 10; // original from client; max draw dist
		int distSq = x * x + y * y;
		
		if (distSq > maxDist * maxDist)
			return null;
		
		final int minimapSetting = Client.getMinimapSetting();
		int angle;
		
		if (minimapSetting == data.Client.get.minimapSetting_check.getMultiplier().intValue()) // standard setting
			//angle = (int) (Camera.getYaw() * 0.0174532925199433D * 2607.5945876176133D) + Client.getMinimapOffset() & 0x3FFF; // workaround without using Camera.getMinimapAngle()
			angle = (int) (Client.getCamera().getMinimapAngle() * 2607.5945876176133D) + Client.getMinimapOffset() & 0x3FFF;
		else if (minimapSetting == 4) // TODO: hook "4": own - 836
			angle = (int) Client.getMinimapAngle() & 0x3FFF;
		else
			angle = (int) Client.getMinimapAngle() + Client.getMinimapOffset() & 0x3FFF;
		
		int aSin = Calculations.SIN_TABLE[angle];
		int aCos = Calculations.COS_TABLE[angle];
		
		if (minimapSetting != 4) { // TODO: hook "4": own - 836
			final int scale = Client.getMinimapScale();
			aSin = 256 * aSin / (256 + scale);
			aCos = 256 * aCos / (256 + scale);
			
			// alternative
			/*final int scale = Client.getMinimapScale();
			aSin = (256 + scale) * aSin >> 8;
			aCos = (256 + scale) * aCos >> 8;*/
		}
		
		// the offset coords from the center of the minimap
		int xOff = aCos * x + aSin * y >> 14;
		int yOff = aCos * y - aSin * x >> 14;
		
		// TODO: temp fix, more division is necessary?
		xOff /= 32; // same as >>= 5
		yOff /= 32; // same as >>= 5
		
		// the client offsets these values a bit (at least ground items), based on some more calculations, normally 2 is subtracted from screen_x and 2.5 (though 2 since it's truncated) is subtracted from screen_y
		Point mmPoint = mmBorder.getAbsoluteLocation();
		if (mmPoint == null)
			return null;
		
		int screen_x = mmPoint.x + mmBorder.getHorizontalScrollbarThumbSize() / 2 + xOff;
		int screen_y = mmPoint.y + mmBorder.getVerticalScrollbarThumbSize() / 2 - yOff;
		
		return new Point(screen_x, screen_y);
	}
	
	public static Point worldToScreen(int x, int y, int z) {
		Render render = Client.getRender();
		if (render == null)
			return null;
		
		Viewport viewport = render.getClientViewport();
		
		float[] data;
		
		float[] callBackFloats = Viewport.getCallBackFloats();
		if (callBackFloats == null)
			data = viewport.getFloats().clone();
		else
			data = callBackFloats.clone();
		
		final float checkOff = data[14];
		final float xOff = data[12];
		final float yOff = data[13];
		final float zOff = data[15];
		final float checkX = data[2];
		final float checkY = data[6];
		final float checkZ = data[10];
		final float xX = data[0];
		final float xY = data[4];
		final float xZ = data[8];
		final float yX = data[1];
		final float yY = data[5];
		final float yZ = data[9];
		final float zX = data[3];
		final float zY = data[7];
		final float zZ = data[11];
		
		final float render_absoluteX = render.getAbsoluteX();
		final float render_absoluteY = render.getAbsoluteY();
		final float render_xMultiplier = render.getXMultiplier();
		final float render_yMultiplier = render.getYMultiplier();
		
		float _check = (checkOff + (checkX * x + checkY * y + checkZ * z));
		float _z = (zOff + (zX * x + zY * y + zZ * z));
		
		if (_check >= -_z) {
			float _x = (xOff + (xX * x + xY * y + xZ * z));
			float _y = (yOff + (yX * x + yY * y + yZ * z));
			
			if (_x >= -_z && _x <= _z && _y >= -_z && _y <= _z) {
				return new Point(
						(int)(render_absoluteX + render_xMultiplier * _x / _z), 
						(int)(render_absoluteY + render_yMultiplier * _y / _z));
			} else
				return null;
		} else
			return null;
	}
	
	public static final int[] SIN_TABLE = new int[0x4000];
	public static final int[] COS_TABLE = new int[0x4000];
	
	static {
		final double d = 0.00038349519697141029D;
		for (int i = 0; i < 0x4000; i++) {
			Calculations.SIN_TABLE[i] = (int) (0x8000D * Math.sin(i * d));
			Calculations.COS_TABLE[i] = (int) (0x8000D * Math.cos(i * d));
		}	
	}
	
	public static boolean isOnScreen(Point point) {
		if (point == null)
			return false;
		
		return isOnScreen(point.x, point.y);
	}
	
	public static boolean isOnScreen(int x, int y) {
		int canvasWidth = Client.getCanvasWidth();
		int canvasHeight = Client.getCanvasHeight();
		
		return x >= 0 && y >= 0 && x < canvasWidth && y < canvasHeight;
	}
	
	public static double distance(BasicInteractable interactable1, BasicInteractable interactable2) {
		if (interactable1 == null || interactable2 == null)
			return -1D;
		
		Tile tile1 = interactable1.getTile(), tile2 = interactable2.getTile();
		return Math.sqrt(Math.pow(tile1.getX() - tile2.getX(), 2) + Math.pow(tile1.getY() - tile2.getY(), 2));
	}
	
	public static double distance(Location loc1, Location loc2) {
		return Math.sqrt(Math.pow(loc1.getRelativeX() - loc2.getRelativeX(), 2) + Math.pow(loc1.getRelativeY() - loc2.getRelativeY(), 2));
	}
	
	public static double distance(Tile tile1, Tile tile2) {
		return Math.sqrt(Math.pow(tile1.getX() - tile2.getX(), 2) + Math.pow(tile1.getY() - tile2.getY(), 2));
	}
	
	public static double distance(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
	
	public static double distance(long x1, long y1, long x2, long y2) {
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
	
	public static double distanceTo(BasicInteractable interactable) {
		return distance(interactable, Client.getMyPlayer());
	}
	
	public static double distanceTo(Location loc) {
		Player player = Client.getMyPlayer();
		if (player == null)
			return -1D;
		
		return distance(loc, player.getData().getCenterLocation());
	}
	
	public static double distanceTo(Tile tile) {
		Player player = Client.getMyPlayer();
		if (player == null)
			return -1D;
		
		return distance(tile, player.getTile());
	}
	
	public static double distanceTo(int x, int y) {
		Player player = Client.getMyPlayer();
		if (player == null)
			return -1D;
		
		Tile tile = player.getTile();
		return distance(x, y, tile.getX(), tile.getY());
	}
}
