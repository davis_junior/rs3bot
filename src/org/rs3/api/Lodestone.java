package org.rs3.api;

import java.awt.event.KeyEvent;
import java.util.EnumSet;

import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.ContinueMessageGame;
import org.rs3.api.interfaces.widgets.EnterWildernessWarning;
import org.rs3.api.interfaces.widgets.LodestoneNetwork;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Player;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public enum Lodestone {
	//
	//					keyShortcutModifier	keyShortcut			membersOnly		aaoId	componentData									isActivatedSettingShift
	//
	LUMBRIDGE			(-1, 				KeyEvent.VK_L, 		false, 			69836, 	LodestoneNetwork.teleportLumbridgeData, 		2), // isActivatedSettingShift guess, otherwise 7 or 22
	BURTHORPE			(-1, 				KeyEvent.VK_B, 		false, 			69831, 	LodestoneNetwork.teleportBurthorpeData, 		7), // isActivatedSettingShift guess, otherwise 2 or 22
	LUNAR_ISLE			(KeyEvent.VK_ALT, 	KeyEvent.VK_L, 		true, 			-1, 	LodestoneNetwork.teleportLunarIsleData, 		-1),
	BANDIT_CAMP			(KeyEvent.VK_ALT, 	KeyEvent.VK_B, 		true, 			-1, 	LodestoneNetwork.teleportBanditCampData, 		-1),
	TAVERLEY			(-1, 				KeyEvent.VK_T, 		false, 			69839, 	LodestoneNetwork.teleportTaverleyData, 			10),
	AL_KHARID			(-1, 				KeyEvent.VK_A, 		false, 			69829, 	LodestoneNetwork.teleportAlKharidData, 			0),
	VARROCK				(-1, 				KeyEvent.VK_V, 		false, 			69840, 	LodestoneNetwork.teleportVarrockData, 			11),
	EDGEVILLE			(-1, 				KeyEvent.VK_E, 		false, 			69834, 	LodestoneNetwork.teleportEdgevilleData, 		5),
	FALADOR				(-1, 				KeyEvent.VK_F, 		false, 			69835, 	LodestoneNetwork.teleportFaladorData, 			6),
	PORT_SARIM			(-1, 				KeyEvent.VK_P, 		false, 			69837, 	LodestoneNetwork.teleportPortSarimData, 		8),
	DRAYNOR_VILLAGE		(-1, 				KeyEvent.VK_D, 		false, 			69833, 	LodestoneNetwork.teleportDraynorVillageData, 	4),
	ARDOUGNE			(KeyEvent.VK_ALT, 	KeyEvent.VK_A, 		true, 			-1, 	LodestoneNetwork.teleportArdougneData, 			-1),
	CATHERBY			(-1, 				KeyEvent.VK_C, 		true, 			-1, 	LodestoneNetwork.teleportCatherbyData, 			-1),
	YANILLE				(-1, 				KeyEvent.VK_Y, 		true, 			-1, 	LodestoneNetwork.teleportYanilleData, 			-1),
	SEERS_VILLAGE		(-1, 				KeyEvent.VK_S, 		true, 			-1, 	LodestoneNetwork.teleportSeersVillageData, 		-1),
	EAGLES_PEAK			(KeyEvent.VK_ALT, 	KeyEvent.VK_E, 		true, 			-1, 	LodestoneNetwork.teleportEaglesPeakData, 		-1),
	TIRANNWN			(KeyEvent.VK_ALT, 	KeyEvent.VK_T, 		true, 			-1, 	LodestoneNetwork.teleportTirannwnData, 			-1),
	OOGLOG				(-1, 				KeyEvent.VK_O, 		true, 			-1, 	LodestoneNetwork.teleportOoGlogData, 			-1),
	KARAMJA				(-1, 				KeyEvent.VK_K, 		true, 			-1, 	LodestoneNetwork.teleportKaramjaData, 			-1),
	CANIFIS				(KeyEvent.VK_ALT, 	KeyEvent.VK_C, 		true, 			-1, 	LodestoneNetwork.teleportCanifisData, 			-1),
	WILDERNESS_VOLCANO	(-1, 				KeyEvent.VK_W, 		false, 			84754, 	LodestoneNetwork.teleportWildernessVolcanoData, 21),
	FREMENNIK_PROVINCE	(KeyEvent.VK_ALT, 	KeyEvent.VK_F, 		true, 			-1, 	LodestoneNetwork.teleportFremennikProvinceData, -1),
	ASHDALE				(KeyEvent.VK_SHIFT, KeyEvent.VK_A, 		false, 			66532, 	LodestoneNetwork.teleportAshdaleData, 			23),
	PRIFDDINAS			(KeyEvent.VK_ALT, 	KeyEvent.VK_P, 		true, 			-1, 	LodestoneNetwork.teleportPrifddinasData, 		-1),
	;

	int keyShortcutModifier;
	int keyShortcut;
	boolean membersOnly;
	int aaoId;
	ComponentData componentData;
	int isActivatedSettingShift; // bits to right shift
	
	private static TilePath burthopeLStoTaverlyLS = new TilePath( new Tile(2899, 3544, 0), new Tile(2899, 3543, 0), new Tile(2899, 3542, 0), new Tile(2899, 3541, 0), new Tile(2898, 3541, 0), new Tile(2898, 3540, 0), new Tile(2898, 3539, 0), new Tile(2898, 3538, 0), new Tile(2898, 3537, 0), new Tile(2898, 3536, 0), new Tile(2899, 3536, 0), new Tile(2899, 3535, 0), new Tile(2899, 3534, 0), new Tile(2899, 3533, 0), new Tile(2899, 3532, 0), new Tile(2899, 3531, 0), new Tile(2899, 3530, 0), new Tile(2899, 3529, 0), new Tile(2899, 3528, 0), new Tile(2899, 3527, 0), new Tile(2899, 3526, 0), new Tile(2899, 3525, 0), new Tile(2899, 3524, 0), new Tile(2899, 3523, 0), new Tile(2899, 3522, 0), new Tile(2899, 3521, 0), new Tile(2899, 3520, 0), new Tile(2899, 3519, 0), new Tile(2899, 3518, 0), new Tile(2899, 3517, 0), new Tile(2899, 3516, 0), new Tile(2899, 3515, 0), new Tile(2899, 3514, 0), new Tile(2898, 3514, 0), new Tile(2898, 3513, 0), new Tile(2898, 3512, 0), new Tile(2897, 3512, 0), new Tile(2897, 3511, 0), new Tile(2897, 3510, 0), new Tile(2896, 3510, 0), new Tile(2896, 3509, 0), new Tile(2896, 3508, 0), new Tile(2895, 3508, 0), new Tile(2895, 3507, 0), new Tile(2895, 3506, 0), new Tile(2894, 3506, 0), new Tile(2894, 3505, 0), new Tile(2894, 3504, 0), new Tile(2893, 3504, 0), new Tile(2893, 3503, 0), new Tile(2892, 3503, 0), new Tile(2892, 3502, 0), new Tile(2892, 3501, 0), new Tile(2892, 3500, 0), new Tile(2892, 3499, 0), new Tile(2892, 3498, 0), new Tile(2892, 3497, 0), new Tile(2892, 3496, 0), new Tile(2892, 3495, 0), new Tile(2892, 3494, 0), new Tile(2892, 3493, 0), new Tile(2892, 3492, 0), new Tile(2892, 3491, 0), new Tile(2892, 3490, 0), new Tile(2892, 3489, 0), new Tile(2892, 3488, 0), new Tile(2892, 3487, 0), new Tile(2892, 3486, 0), new Tile(2892, 3485, 0), new Tile(2892, 3484, 0), new Tile(2892, 3483, 0), new Tile(2892, 3482, 0), new Tile(2891, 3482, 0), new Tile(2891, 3481, 0), new Tile(2891, 3480, 0), new Tile(2891, 3479, 0), new Tile(2891, 3478, 0), new Tile(2891, 3477, 0), new Tile(2891, 3476, 0), new Tile(2891, 3475, 0), new Tile(2892, 3475, 0), new Tile(2892, 3474, 0), new Tile(2892, 3473, 0), new Tile(2893, 3473, 0), new Tile(2893, 3472, 0), new Tile(2894, 3472, 0), new Tile(2894, 3471, 0), new Tile(2894, 3470, 0), new Tile(2894, 3469, 0), new Tile(2894, 3468, 0), new Tile(2894, 3467, 0), new Tile(2893, 3467, 0), new Tile(2893, 3466, 0), new Tile(2893, 3465, 0), new Tile(2893, 3464, 0), new Tile(2893, 3463, 0), new Tile(2893, 3462, 0), new Tile(2893, 3461, 0), new Tile(2893, 3460, 0), new Tile(2892, 3460, 0), new Tile(2892, 3459, 0), new Tile(2891, 3459, 0), new Tile(2891, 3458, 0), new Tile(2891, 3457, 0), new Tile(2890, 3457, 0), new Tile(2890, 3456, 0), new Tile(2889, 3456, 0), new Tile(2888, 3456, 0), new Tile(2888, 3455, 0), new Tile(2887, 3455, 0), new Tile(2887, 3454, 0), new Tile(2887, 3453, 0), new Tile(2886, 3453, 0), new Tile(2886, 3452, 0), new Tile(2885, 3452, 0), new Tile(2885, 3451, 0), new Tile(2885, 3450, 0), new Tile(2884, 3450, 0), new Tile(2883, 3450, 0), new Tile(2883, 3449, 0), new Tile(2883, 3448, 0), new Tile(2882, 3448, 0), new Tile(2882, 3447, 0), new Tile(2881, 3447, 0), new Tile(2881, 3446, 0), new Tile(2881, 3445, 0), new Tile(2880, 3445, 0), new Tile(2880, 3444, 0), new Tile(2880, 3443, 0), new Tile(2879, 3443, 0) );
	private static TilePath tavleryLStoFaladorGate = new TilePath( new Tile(2878, 3442, 0), new Tile(2878, 3441, 0), new Tile(2878, 3440, 0), new Tile(2879, 3440, 0), new Tile(2879, 3439, 0), new Tile(2880, 3439, 0), new Tile(2880, 3438, 0), new Tile(2881, 3438, 0), new Tile(2881, 3437, 0), new Tile(2882, 3437, 0), new Tile(2883, 3437, 0), new Tile(2883, 3436, 0), new Tile(2883, 3435, 0), new Tile(2884, 3435, 0), new Tile(2884, 3434, 0), new Tile(2884, 3433, 0), new Tile(2884, 3432, 0), new Tile(2884, 3431, 0), new Tile(2884, 3430, 0), new Tile(2884, 3429, 0), new Tile(2884, 3428, 0), new Tile(2884, 3427, 0), new Tile(2884, 3426, 0), new Tile(2884, 3425, 0), new Tile(2884, 3424, 0), new Tile(2884, 3423, 0), new Tile(2884, 3422, 0), new Tile(2884, 3421, 0), new Tile(2884, 3420, 0), new Tile(2884, 3419, 0), new Tile(2885, 3419, 0), new Tile(2885, 3418, 0), new Tile(2886, 3418, 0), new Tile(2886, 3417, 0), new Tile(2887, 3417, 0), new Tile(2888, 3417, 0), new Tile(2888, 3416, 0), new Tile(2888, 3415, 0), new Tile(2889, 3415, 0), new Tile(2890, 3415, 0), new Tile(2891, 3415, 0), new Tile(2892, 3415, 0), new Tile(2893, 3415, 0), new Tile(2894, 3415, 0), new Tile(2895, 3415, 0), new Tile(2896, 3415, 0), new Tile(2897, 3415, 0), new Tile(2898, 3415, 0), new Tile(2899, 3415, 0), new Tile(2900, 3415, 0), new Tile(2901, 3415, 0), new Tile(2902, 3415, 0), new Tile(2903, 3415, 0), new Tile(2904, 3415, 0), new Tile(2905, 3415, 0), new Tile(2906, 3415, 0), new Tile(2906, 3416, 0), new Tile(2907, 3416, 0), new Tile(2908, 3416, 0), new Tile(2908, 3417, 0), new Tile(2908, 3418, 0), new Tile(2908, 3419, 0), new Tile(2909, 3419, 0), new Tile(2909, 3420, 0), new Tile(2910, 3420, 0), new Tile(2911, 3420, 0), new Tile(2911, 3421, 0), new Tile(2912, 3421, 0), new Tile(2912, 3422, 0), new Tile(2913, 3422, 0), new Tile(2913, 3423, 0), new Tile(2914, 3423, 0), new Tile(2915, 3423, 0), new Tile(2915, 3424, 0), new Tile(2916, 3424, 0), new Tile(2917, 3424, 0), new Tile(2918, 3424, 0), new Tile(2919, 3424, 0), new Tile(2919, 3425, 0), new Tile(2920, 3425, 0), new Tile(2920, 3426, 0), new Tile(2921, 3426, 0), new Tile(2921, 3427, 0), new Tile(2921, 3428, 0), new Tile(2922, 3428, 0), new Tile(2922, 3429, 0), new Tile(2922, 3430, 0), new Tile(2922, 3431, 0), new Tile(2922, 3432, 0), new Tile(2922, 3433, 0), new Tile(2922, 3434, 0), new Tile(2922, 3435, 0), new Tile(2922, 3436, 0), new Tile(2922, 3437, 0), new Tile(2922, 3438, 0), new Tile(2923, 3438, 0), new Tile(2923, 3439, 0), new Tile(2924, 3439, 0), new Tile(2925, 3439, 0), new Tile(2926, 3439, 0), new Tile(2927, 3439, 0), new Tile(2928, 3439, 0), new Tile(2929, 3439, 0), new Tile(2930, 3439, 0), new Tile(2931, 3439, 0), new Tile(2932, 3439, 0), new Tile(2933, 3439, 0), new Tile(2934, 3439, 0), new Tile(2935, 3439, 0), new Tile(2936, 3439, 0), new Tile(2937, 3439, 0), new Tile(2938, 3439, 0), new Tile(2939, 3439, 0), new Tile(2940, 3439, 0), new Tile(2941, 3439, 0), new Tile(2942, 3439, 0) );
	private static TilePath faladorGateTofaladorLS = new TilePath( new Tile(2943, 3440, 0), new Tile(2944, 3440, 0), new Tile(2945, 3440, 0), new Tile(2946, 3440, 0), new Tile(2946, 3439, 0), new Tile(2946, 3438, 0), new Tile(2947, 3438, 0), new Tile(2947, 3437, 0), new Tile(2947, 3436, 0), new Tile(2947, 3435, 0), new Tile(2948, 3435, 0), new Tile(2948, 3434, 0), new Tile(2948, 3433, 0), new Tile(2948, 3432, 0), new Tile(2948, 3431, 0), new Tile(2948, 3430, 0), new Tile(2948, 3429, 0), new Tile(2948, 3428, 0), new Tile(2948, 3427, 0), new Tile(2948, 3426, 0), new Tile(2948, 3425, 0), new Tile(2948, 3424, 0), new Tile(2949, 3424, 0), new Tile(2949, 3423, 0), new Tile(2950, 3423, 0), new Tile(2950, 3422, 0), new Tile(2951, 3422, 0), new Tile(2951, 3421, 0), new Tile(2952, 3421, 0), new Tile(2952, 3420, 0), new Tile(2953, 3420, 0), new Tile(2954, 3420, 0), new Tile(2954, 3419, 0), new Tile(2955, 3419, 0), new Tile(2955, 3418, 0), new Tile(2956, 3418, 0), new Tile(2957, 3418, 0), new Tile(2957, 3417, 0), new Tile(2958, 3417, 0), new Tile(2959, 3417, 0), new Tile(2959, 3416, 0), new Tile(2960, 3416, 0), new Tile(2960, 3415, 0), new Tile(2961, 3415, 0), new Tile(2962, 3415, 0), new Tile(2962, 3414, 0), new Tile(2963, 3414, 0), new Tile(2963, 3413, 0), new Tile(2964, 3413, 0), new Tile(2964, 3412, 0), new Tile(2965, 3412, 0), new Tile(2965, 3411, 0), new Tile(2965, 3410, 0), new Tile(2965, 3409, 0), new Tile(2966, 3409, 0), new Tile(2966, 3408, 0), new Tile(2966, 3407, 0), new Tile(2967, 3407, 0), new Tile(2967, 3406, 0), new Tile(2967, 3405, 0) );
	private static TilePath faladorLStoEdgvilleLS = new TilePath( new Tile(2967, 3405, 0), new Tile(2967, 3406, 0), new Tile(2967, 3407, 0), new Tile(2967, 3408, 0), new Tile(2967, 3409, 0), new Tile(2968, 3409, 0), new Tile(2968, 3410, 0), new Tile(2968, 3411, 0), new Tile(2969, 3411, 0), new Tile(2969, 3412, 0), new Tile(2970, 3412, 0), new Tile(2970, 3413, 0), new Tile(2971, 3413, 0), new Tile(2972, 3413, 0), new Tile(2973, 3413, 0), new Tile(2973, 3414, 0), new Tile(2974, 3414, 0), new Tile(2975, 3414, 0), new Tile(2976, 3414, 0), new Tile(2976, 3415, 0), new Tile(2977, 3415, 0), new Tile(2978, 3415, 0), new Tile(2978, 3416, 0), new Tile(2979, 3416, 0), new Tile(2980, 3416, 0), new Tile(2980, 3417, 0), new Tile(2981, 3417, 0), new Tile(2982, 3417, 0), new Tile(2982, 3418, 0), new Tile(2983, 3418, 0), new Tile(2983, 3419, 0), new Tile(2984, 3419, 0), new Tile(2985, 3419, 0), new Tile(2985, 3420, 0), new Tile(2986, 3420, 0), new Tile(2987, 3420, 0), new Tile(2988, 3420, 0), new Tile(2988, 3421, 0), new Tile(2988, 3422, 0), new Tile(2989, 3422, 0), new Tile(2989, 3423, 0), new Tile(2989, 3424, 0), new Tile(2989, 3425, 0), new Tile(2989, 3426, 0), new Tile(2989, 3427, 0), new Tile(2989, 3428, 0), new Tile(2989, 3429, 0), new Tile(2990, 3429, 0), new Tile(2991, 3429, 0), new Tile(2991, 3430, 0), new Tile(2992, 3430, 0), new Tile(2992, 3431, 0), new Tile(2993, 3431, 0), new Tile(2994, 3431, 0), new Tile(2994, 3432, 0), new Tile(2995, 3432, 0), new Tile(2995, 3433, 0), new Tile(2996, 3433, 0), new Tile(2997, 3433, 0), new Tile(2998, 3433, 0), new Tile(2999, 3433, 0), new Tile(3000, 3433, 0), new Tile(3000, 3432, 0), new Tile(3001, 3432, 0), new Tile(3002, 3432, 0), new Tile(3003, 3432, 0), new Tile(3004, 3432, 0), new Tile(3005, 3432, 0), new Tile(3006, 3432, 0), new Tile(3007, 3432, 0), new Tile(3008, 3432, 0), new Tile(3009, 3432, 0), new Tile(3010, 3432, 0), new Tile(3011, 3432, 0), new Tile(3012, 3432, 0), new Tile(3013, 3432, 0), new Tile(3014, 3432, 0), new Tile(3014, 3433, 0), new Tile(3015, 3433, 0), new Tile(3016, 3433, 0), new Tile(3017, 3433, 0), new Tile(3018, 3433, 0), new Tile(3018, 3434, 0), new Tile(3018, 3435, 0), new Tile(3019, 3435, 0), new Tile(3020, 3435, 0), new Tile(3021, 3435, 0), new Tile(3021, 3436, 0), new Tile(3022, 3436, 0), new Tile(3023, 3436, 0), new Tile(3024, 3436, 0), new Tile(3025, 3436, 0), new Tile(3025, 3437, 0), new Tile(3025, 3438, 0), new Tile(3026, 3438, 0), new Tile(3027, 3438, 0), new Tile(3028, 3438, 0), new Tile(3028, 3439, 0), new Tile(3029, 3439, 0), new Tile(3030, 3439, 0), new Tile(3031, 3439, 0), new Tile(3032, 3439, 0), new Tile(3032, 3440, 0), new Tile(3033, 3440, 0), new Tile(3034, 3440, 0), new Tile(3034, 3441, 0), new Tile(3034, 3442, 0), new Tile(3035, 3442, 0), new Tile(3035, 3443, 0), new Tile(3035, 3444, 0), new Tile(3036, 3444, 0), new Tile(3036, 3445, 0), new Tile(3037, 3445, 0), new Tile(3037, 3446, 0), new Tile(3038, 3446, 0), new Tile(3039, 3446, 0), new Tile(3040, 3446, 0), new Tile(3040, 3447, 0), new Tile(3040, 3448, 0), new Tile(3040, 3449, 0), new Tile(3040, 3450, 0), new Tile(3041, 3450, 0), new Tile(3042, 3450, 0), new Tile(3043, 3450, 0), new Tile(3044, 3450, 0), new Tile(3044, 3451, 0), new Tile(3044, 3452, 0), new Tile(3044, 3453, 0), new Tile(3044, 3454, 0), new Tile(3045, 3454, 0), new Tile(3046, 3454, 0), new Tile(3047, 3454, 0), new Tile(3048, 3454, 0), new Tile(3048, 3455, 0), new Tile(3048, 3456, 0), new Tile(3048, 3457, 0), new Tile(3049, 3457, 0), new Tile(3049, 3458, 0), new Tile(3049, 3459, 0), new Tile(3049, 3460, 0), new Tile(3049, 3461, 0), new Tile(3050, 3461, 0), new Tile(3051, 3461, 0), new Tile(3052, 3461, 0), new Tile(3053, 3461, 0), new Tile(3053, 3462, 0), new Tile(3053, 3463, 0), new Tile(3053, 3464, 0), new Tile(3054, 3464, 0), new Tile(3055, 3464, 0), new Tile(3056, 3464, 0), new Tile(3057, 3464, 0), new Tile(3058, 3464, 0), new Tile(3058, 3465, 0), new Tile(3058, 3466, 0), new Tile(3058, 3467, 0), new Tile(3058, 3468, 0), new Tile(3058, 3469, 0), new Tile(3059, 3469, 0), new Tile(3059, 3470, 0), new Tile(3059, 3471, 0), new Tile(3060, 3471, 0), new Tile(3061, 3471, 0), new Tile(3062, 3471, 0), new Tile(3062, 3472, 0), new Tile(3062, 3473, 0), new Tile(3062, 3474, 0), new Tile(3063, 3474, 0), new Tile(3063, 3475, 0), new Tile(3063, 3476, 0), new Tile(3063, 3477, 0), new Tile(3064, 3477, 0), new Tile(3065, 3477, 0), new Tile(3065, 3478, 0), new Tile(3065, 3479, 0), new Tile(3065, 3480, 0), new Tile(3065, 3481, 0), new Tile(3066, 3481, 0), new Tile(3067, 3481, 0), new Tile(3067, 3482, 0), new Tile(3067, 3483, 0), new Tile(3067, 3484, 0), new Tile(3068, 3484, 0), new Tile(3069, 3484, 0), new Tile(3069, 3485, 0), new Tile(3069, 3486, 0), new Tile(3069, 3487, 0), new Tile(3069, 3488, 0), new Tile(3069, 3489, 0), new Tile(3069, 3490, 0), new Tile(3069, 3491, 0), new Tile(3069, 3492, 0), new Tile(3069, 3493, 0), new Tile(3069, 3494, 0), new Tile(3069, 3495, 0), new Tile(3069, 3496, 0), new Tile(3069, 3497, 0), new Tile(3069, 3498, 0), new Tile(3069, 3499, 0), new Tile(3069, 3500, 0), new Tile(3069, 3501, 0), new Tile(3069, 3502, 0), new Tile(3069, 3503, 0), new Tile(3068, 3503, 0), new Tile(3067, 3503, 0), new Tile(3067, 3504, 0), new Tile(3067, 3505, 0) );
	private static TilePath edgvilleLStoWildernessWall = new TilePath( new Tile(3067, 3505, 0), new Tile(3068, 3505, 0), new Tile(3069, 3505, 0), new Tile(3070, 3505, 0), new Tile(3070, 3506, 0), new Tile(3070, 3507, 0), new Tile(3070, 3508, 0), new Tile(3071, 3508, 0), new Tile(3072, 3508, 0), new Tile(3072, 3509, 0), new Tile(3072, 3510, 0), new Tile(3073, 3510, 0), new Tile(3073, 3511, 0), new Tile(3073, 3512, 0), new Tile(3073, 3513, 0), new Tile(3073, 3514, 0), new Tile(3073, 3515, 0), new Tile(3073, 3516, 0), new Tile(3074, 3516, 0), new Tile(3074, 3517, 0), new Tile(3074, 3518, 0), new Tile(3074, 3519, 0), new Tile(3074, 3520, 0), new Tile(3074, 3521, 0) );
	private static TilePath wildernessWalltoWildernessVolcanoLS = new TilePath( new Tile(3074, 3522, 0), new Tile(3074, 3523, 0), new Tile(3074, 3524, 0), new Tile(3074, 3525, 0), new Tile(3075, 3525, 0), new Tile(3076, 3525, 0), new Tile(3076, 3526, 0), new Tile(3076, 3527, 0), new Tile(3076, 3528, 0), new Tile(3077, 3528, 0), new Tile(3078, 3528, 0), new Tile(3079, 3528, 0), new Tile(3079, 3529, 0), new Tile(3079, 3530, 0), new Tile(3079, 3531, 0), new Tile(3079, 3532, 0), new Tile(3079, 3533, 0), new Tile(3080, 3533, 0), new Tile(3081, 3533, 0), new Tile(3082, 3533, 0), new Tile(3082, 3534, 0), new Tile(3082, 3535, 0), new Tile(3082, 3536, 0), new Tile(3083, 3536, 0), new Tile(3084, 3536, 0), new Tile(3085, 3536, 0), new Tile(3085, 3537, 0), new Tile(3085, 3538, 0), new Tile(3085, 3539, 0), new Tile(3085, 3540, 0), new Tile(3085, 3541, 0), new Tile(3086, 3541, 0), new Tile(3087, 3541, 0), new Tile(3088, 3541, 0), new Tile(3089, 3541, 0), new Tile(3089, 3542, 0), new Tile(3089, 3543, 0), new Tile(3089, 3544, 0), new Tile(3089, 3545, 0), new Tile(3090, 3545, 0), new Tile(3091, 3545, 0), new Tile(3092, 3545, 0), new Tile(3093, 3545, 0), new Tile(3094, 3545, 0), new Tile(3094, 3546, 0), new Tile(3094, 3547, 0), new Tile(3094, 3548, 0), new Tile(3094, 3549, 0), new Tile(3094, 3550, 0), new Tile(3095, 3550, 0), new Tile(3096, 3550, 0), new Tile(3097, 3550, 0), new Tile(3098, 3550, 0), new Tile(3099, 3550, 0), new Tile(3099, 3551, 0), new Tile(3099, 3552, 0), new Tile(3099, 3553, 0), new Tile(3099, 3554, 0), new Tile(3100, 3554, 0), new Tile(3100, 3555, 0), new Tile(3100, 3556, 0), new Tile(3100, 3557, 0), new Tile(3101, 3557, 0), new Tile(3102, 3557, 0), new Tile(3103, 3557, 0), new Tile(3104, 3557, 0), new Tile(3105, 3557, 0), new Tile(3106, 3557, 0), new Tile(3106, 3558, 0), new Tile(3106, 3559, 0), new Tile(3106, 3560, 0), new Tile(3106, 3561, 0), new Tile(3107, 3561, 0), new Tile(3108, 3561, 0), new Tile(3109, 3561, 0), new Tile(3110, 3561, 0), new Tile(3111, 3561, 0), new Tile(3111, 3562, 0), new Tile(3111, 3563, 0), new Tile(3111, 3564, 0), new Tile(3111, 3565, 0), new Tile(3112, 3565, 0), new Tile(3113, 3565, 0), new Tile(3113, 3566, 0), new Tile(3113, 3567, 0), new Tile(3113, 3568, 0), new Tile(3113, 3569, 0), new Tile(3113, 3570, 0), new Tile(3113, 3571, 0), new Tile(3113, 3572, 0), new Tile(3114, 3572, 0), new Tile(3115, 3572, 0), new Tile(3116, 3572, 0), new Tile(3116, 3573, 0), new Tile(3116, 3574, 0), new Tile(3116, 3575, 0), new Tile(3116, 3576, 0), new Tile(3116, 3577, 0), new Tile(3116, 3578, 0), new Tile(3116, 3579, 0), new Tile(3116, 3580, 0), new Tile(3116, 3581, 0), new Tile(3116, 3582, 0), new Tile(3117, 3582, 0), new Tile(3118, 3582, 0), new Tile(3118, 3583, 0), new Tile(3118, 3584, 0), new Tile(3118, 3585, 0), new Tile(3119, 3585, 0), new Tile(3120, 3585, 0), new Tile(3120, 3586, 0), new Tile(3120, 3587, 0), new Tile(3121, 3587, 0), new Tile(3122, 3587, 0), new Tile(3122, 3588, 0), new Tile(3122, 3589, 0), new Tile(3122, 3590, 0), new Tile(3123, 3590, 0), new Tile(3124, 3590, 0), new Tile(3124, 3591, 0), new Tile(3124, 3592, 0), new Tile(3124, 3593, 0), new Tile(3124, 3594, 0), new Tile(3124, 3595, 0), new Tile(3124, 3596, 0), new Tile(3124, 3597, 0), new Tile(3125, 3597, 0), new Tile(3126, 3597, 0), new Tile(3126, 3598, 0), new Tile(3126, 3599, 0), new Tile(3126, 3600, 0), new Tile(3126, 3601, 0), new Tile(3126, 3602, 0), new Tile(3126, 3603, 0), new Tile(3126, 3604, 0), new Tile(3126, 3605, 0), new Tile(3127, 3605, 0), new Tile(3128, 3605, 0), new Tile(3128, 3606, 0), new Tile(3128, 3607, 0), new Tile(3128, 3608, 0), new Tile(3129, 3608, 0), new Tile(3129, 3609, 0), new Tile(3129, 3610, 0), new Tile(3129, 3611, 0), new Tile(3130, 3611, 0), new Tile(3131, 3611, 0), new Tile(3132, 3611, 0), new Tile(3132, 3612, 0), new Tile(3132, 3613, 0), new Tile(3132, 3614, 0), new Tile(3132, 3615, 0), new Tile(3132, 3616, 0), new Tile(3133, 3616, 0), new Tile(3134, 3616, 0), new Tile(3135, 3616, 0), new Tile(3136, 3616, 0), new Tile(3136, 3617, 0), new Tile(3136, 3618, 0), new Tile(3136, 3619, 0), new Tile(3137, 3619, 0), new Tile(3138, 3619, 0), new Tile(3139, 3619, 0), new Tile(3139, 3620, 0), new Tile(3139, 3621, 0), new Tile(3139, 3622, 0), new Tile(3139, 3623, 0), new Tile(3139, 3624, 0), new Tile(3140, 3624, 0), new Tile(3141, 3624, 0), new Tile(3141, 3625, 0), new Tile(3141, 3626, 0), new Tile(3141, 3627, 0), new Tile(3141, 3628, 0), new Tile(3141, 3629, 0), new Tile(3141, 3630, 0), new Tile(3142, 3630, 0), new Tile(3142, 3631, 0), new Tile(3142, 3632, 0), new Tile(3142, 3633, 0), new Tile(3143, 3633, 0), new Tile(3143, 3634, 0), new Tile(3143, 3635, 0) );
	private static TilePath lumbridgeLStoDraynorVillageLS = new TilePath( new Tile(3233, 3221, 0), new Tile(3234, 3221, 0), new Tile(3235, 3221, 0), new Tile(3235, 3222, 0), new Tile(3235, 3223, 0), new Tile(3235, 3224, 0), new Tile(3235, 3225, 0), new Tile(3235, 3226, 0), new Tile(3234, 3226, 0), new Tile(3234, 3227, 0), new Tile(3233, 3227, 0), new Tile(3233, 3228, 0), new Tile(3233, 3229, 0), new Tile(3232, 3229, 0), new Tile(3231, 3229, 0), new Tile(3231, 3230, 0), new Tile(3231, 3231, 0), new Tile(3230, 3231, 0), new Tile(3229, 3231, 0), new Tile(3228, 3231, 0), new Tile(3228, 3232, 0), new Tile(3228, 3233, 0), new Tile(3227, 3233, 0), new Tile(3226, 3233, 0), new Tile(3226, 3234, 0), new Tile(3226, 3235, 0), new Tile(3225, 3235, 0), new Tile(3225, 3236, 0), new Tile(3225, 3237, 0), new Tile(3224, 3237, 0), new Tile(3223, 3237, 0), new Tile(3223, 3238, 0), new Tile(3223, 3239, 0), new Tile(3223, 3240, 0), new Tile(3223, 3241, 0), new Tile(3223, 3242, 0), new Tile(3223, 3243, 0), new Tile(3222, 3243, 0), new Tile(3222, 3244, 0), new Tile(3222, 3245, 0), new Tile(3221, 3245, 0), new Tile(3221, 3246, 0), new Tile(3221, 3247, 0), new Tile(3220, 3247, 0), new Tile(3220, 3248, 0), new Tile(3219, 3248, 0), new Tile(3219, 3249, 0), new Tile(3219, 3250, 0), new Tile(3219, 3251, 0), new Tile(3219, 3252, 0), new Tile(3219, 3253, 0), new Tile(3219, 3254, 0), new Tile(3219, 3255, 0), new Tile(3219, 3256, 0), new Tile(3219, 3257, 0), new Tile(3219, 3258, 0), new Tile(3218, 3258, 0), new Tile(3218, 3259, 0), new Tile(3218, 3260, 0), new Tile(3217, 3260, 0), new Tile(3217, 3261, 0), new Tile(3217, 3262, 0), new Tile(3217, 3263, 0), new Tile(3217, 3264, 0), new Tile(3217, 3265, 0), new Tile(3217, 3266, 0), new Tile(3217, 3267, 0), new Tile(3217, 3268, 0), new Tile(3217, 3269, 0), new Tile(3216, 3269, 0), new Tile(3215, 3269, 0), new Tile(3215, 3270, 0), new Tile(3214, 3270, 0), new Tile(3213, 3270, 0), new Tile(3213, 3271, 0), new Tile(3212, 3271, 0), new Tile(3211, 3271, 0), new Tile(3211, 3272, 0), new Tile(3211, 3273, 0), new Tile(3211, 3274, 0), new Tile(3210, 3274, 0), new Tile(3209, 3274, 0), new Tile(3208, 3274, 0), new Tile(3208, 3275, 0), new Tile(3208, 3276, 0), new Tile(3207, 3276, 0), new Tile(3206, 3276, 0), new Tile(3205, 3276, 0), new Tile(3204, 3276, 0), new Tile(3204, 3277, 0), new Tile(3204, 3278, 0), new Tile(3203, 3278, 0), new Tile(3202, 3278, 0), new Tile(3201, 3278, 0), new Tile(3200, 3278, 0), new Tile(3199, 3278, 0), new Tile(3199, 3279, 0), new Tile(3198, 3279, 0), new Tile(3197, 3279, 0), new Tile(3197, 3280, 0), new Tile(3196, 3280, 0), new Tile(3195, 3280, 0), new Tile(3194, 3280, 0), new Tile(3194, 3281, 0), new Tile(3193, 3281, 0), new Tile(3193, 3282, 0), new Tile(3192, 3282, 0), new Tile(3192, 3283, 0), new Tile(3191, 3283, 0), new Tile(3190, 3283, 0), new Tile(3189, 3283, 0), new Tile(3188, 3283, 0), new Tile(3187, 3283, 0), new Tile(3187, 3284, 0), new Tile(3186, 3284, 0), new Tile(3185, 3284, 0), new Tile(3184, 3284, 0), new Tile(3184, 3285, 0), new Tile(3183, 3285, 0), new Tile(3183, 3286, 0), new Tile(3182, 3286, 0), new Tile(3181, 3286, 0), new Tile(3180, 3286, 0), new Tile(3179, 3286, 0), new Tile(3178, 3286, 0), new Tile(3177, 3286, 0), new Tile(3176, 3286, 0), new Tile(3175, 3286, 0), new Tile(3174, 3286, 0), new Tile(3173, 3286, 0), new Tile(3172, 3286, 0), new Tile(3171, 3286, 0), new Tile(3170, 3286, 0), new Tile(3170, 3287, 0), new Tile(3169, 3287, 0), new Tile(3168, 3287, 0), new Tile(3167, 3287, 0), new Tile(3166, 3287, 0), new Tile(3165, 3287, 0), new Tile(3164, 3287, 0), new Tile(3163, 3287, 0), new Tile(3162, 3287, 0), new Tile(3161, 3287, 0), new Tile(3160, 3287, 0), new Tile(3159, 3287, 0), new Tile(3159, 3288, 0), new Tile(3158, 3288, 0), new Tile(3157, 3288, 0), new Tile(3157, 3289, 0), new Tile(3156, 3289, 0), new Tile(3155, 3289, 0), new Tile(3155, 3290, 0), new Tile(3154, 3290, 0), new Tile(3153, 3290, 0), new Tile(3152, 3290, 0), new Tile(3151, 3290, 0), new Tile(3151, 3291, 0), new Tile(3150, 3291, 0), new Tile(3149, 3291, 0), new Tile(3148, 3291, 0), new Tile(3148, 3292, 0), new Tile(3147, 3292, 0), new Tile(3146, 3292, 0), new Tile(3145, 3292, 0), new Tile(3144, 3292, 0), new Tile(3144, 3293, 0), new Tile(3143, 3293, 0), new Tile(3142, 3293, 0), new Tile(3141, 3293, 0), new Tile(3140, 3293, 0), new Tile(3140, 3294, 0), new Tile(3139, 3294, 0), new Tile(3138, 3294, 0), new Tile(3137, 3294, 0), new Tile(3137, 3295, 0), new Tile(3136, 3295, 0), new Tile(3135, 3295, 0), new Tile(3134, 3295, 0), new Tile(3133, 3295, 0), new Tile(3133, 3296, 0), new Tile(3132, 3296, 0), new Tile(3131, 3296, 0), new Tile(3130, 3296, 0), new Tile(3129, 3296, 0), new Tile(3128, 3296, 0), new Tile(3127, 3296, 0), new Tile(3126, 3296, 0), new Tile(3126, 3297, 0), new Tile(3125, 3297, 0), new Tile(3124, 3297, 0), new Tile(3123, 3297, 0), new Tile(3122, 3297, 0), new Tile(3121, 3297, 0), new Tile(3120, 3297, 0), new Tile(3119, 3297, 0), new Tile(3118, 3297, 0), new Tile(3117, 3297, 0), new Tile(3116, 3297, 0), new Tile(3115, 3297, 0), new Tile(3114, 3297, 0), new Tile(3113, 3297, 0), new Tile(3112, 3297, 0), new Tile(3111, 3297, 0), new Tile(3110, 3297, 0), new Tile(3109, 3297, 0), new Tile(3108, 3297, 0), new Tile(3107, 3297, 0), new Tile(3106, 3297, 0), new Tile(3105, 3297, 0), new Tile(3105, 3298, 0) );
	private static TilePath draynorVillageLStoPortSarimLS = new TilePath( new Tile(3105, 3298, 0), new Tile(3105, 3297, 0), new Tile(3105, 3296, 0), new Tile(3104, 3296, 0), new Tile(3104, 3295, 0), new Tile(3103, 3295, 0), new Tile(3102, 3295, 0), new Tile(3102, 3294, 0), new Tile(3101, 3294, 0), new Tile(3100, 3294, 0), new Tile(3099, 3294, 0), new Tile(3098, 3294, 0), new Tile(3097, 3294, 0), new Tile(3096, 3294, 0), new Tile(3095, 3294, 0), new Tile(3095, 3293, 0), new Tile(3095, 3292, 0), new Tile(3094, 3292, 0), new Tile(3093, 3292, 0), new Tile(3093, 3291, 0), new Tile(3092, 3291, 0), new Tile(3092, 3290, 0), new Tile(3091, 3290, 0), new Tile(3090, 3290, 0), new Tile(3089, 3290, 0), new Tile(3088, 3290, 0), new Tile(3087, 3290, 0), new Tile(3086, 3290, 0), new Tile(3085, 3290, 0), new Tile(3085, 3289, 0), new Tile(3084, 3289, 0), new Tile(3083, 3289, 0), new Tile(3083, 3288, 0), new Tile(3082, 3288, 0), new Tile(3081, 3288, 0), new Tile(3080, 3288, 0), new Tile(3080, 3287, 0), new Tile(3080, 3286, 0), new Tile(3080, 3285, 0), new Tile(3079, 3285, 0), new Tile(3078, 3285, 0), new Tile(3078, 3284, 0), new Tile(3078, 3283, 0), new Tile(3078, 3282, 0), new Tile(3078, 3281, 0), new Tile(3077, 3281, 0), new Tile(3076, 3281, 0), new Tile(3076, 3280, 0), new Tile(3076, 3279, 0), new Tile(3075, 3279, 0), new Tile(3074, 3279, 0), new Tile(3073, 3279, 0), new Tile(3073, 3278, 0), new Tile(3073, 3277, 0), new Tile(3072, 3277, 0), new Tile(3071, 3277, 0), new Tile(3070, 3277, 0), new Tile(3069, 3277, 0), new Tile(3069, 3276, 0), new Tile(3068, 3276, 0), new Tile(3067, 3276, 0), new Tile(3066, 3276, 0), new Tile(3065, 3276, 0), new Tile(3064, 3276, 0), new Tile(3063, 3276, 0), new Tile(3063, 3275, 0), new Tile(3063, 3274, 0), new Tile(3063, 3273, 0), new Tile(3062, 3273, 0), new Tile(3061, 3273, 0), new Tile(3061, 3272, 0), new Tile(3061, 3271, 0), new Tile(3061, 3270, 0), new Tile(3060, 3270, 0), new Tile(3059, 3270, 0), new Tile(3058, 3270, 0), new Tile(3057, 3270, 0), new Tile(3057, 3269, 0), new Tile(3057, 3268, 0), new Tile(3057, 3267, 0), new Tile(3056, 3267, 0), new Tile(3055, 3267, 0), new Tile(3054, 3267, 0), new Tile(3053, 3267, 0), new Tile(3052, 3267, 0), new Tile(3051, 3267, 0), new Tile(3051, 3266, 0), new Tile(3050, 3266, 0), new Tile(3049, 3266, 0), new Tile(3048, 3266, 0), new Tile(3048, 3265, 0), new Tile(3047, 3265, 0), new Tile(3046, 3265, 0), new Tile(3045, 3265, 0), new Tile(3045, 3264, 0), new Tile(3044, 3264, 0), new Tile(3043, 3264, 0), new Tile(3042, 3264, 0), new Tile(3042, 3263, 0), new Tile(3041, 3263, 0), new Tile(3040, 3263, 0), new Tile(3040, 3262, 0), new Tile(3040, 3261, 0), new Tile(3040, 3260, 0), new Tile(3040, 3259, 0), new Tile(3040, 3258, 0), new Tile(3039, 3258, 0), new Tile(3039, 3257, 0), new Tile(3039, 3256, 0), new Tile(3039, 3255, 0), new Tile(3039, 3254, 0), new Tile(3039, 3253, 0), new Tile(3039, 3252, 0), new Tile(3038, 3252, 0), new Tile(3038, 3251, 0), new Tile(3038, 3250, 0), new Tile(3037, 3250, 0), new Tile(3037, 3249, 0), new Tile(3036, 3249, 0), new Tile(3036, 3248, 0), new Tile(3035, 3248, 0), new Tile(3035, 3247, 0), new Tile(3035, 3246, 0), new Tile(3034, 3246, 0), new Tile(3033, 3246, 0), new Tile(3033, 3245, 0), new Tile(3032, 3245, 0), new Tile(3032, 3244, 0), new Tile(3031, 3244, 0), new Tile(3031, 3243, 0), new Tile(3030, 3243, 0), new Tile(3030, 3242, 0), new Tile(3029, 3242, 0), new Tile(3028, 3242, 0), new Tile(3027, 3242, 0), new Tile(3026, 3242, 0), new Tile(3025, 3242, 0), new Tile(3024, 3242, 0), new Tile(3024, 3241, 0), new Tile(3023, 3241, 0), new Tile(3022, 3241, 0), new Tile(3022, 3240, 0), new Tile(3022, 3239, 0), new Tile(3021, 3239, 0), new Tile(3021, 3238, 0), new Tile(3021, 3237, 0), new Tile(3021, 3236, 0), new Tile(3021, 3235, 0), new Tile(3021, 3234, 0), new Tile(3021, 3233, 0), new Tile(3021, 3232, 0), new Tile(3020, 3232, 0), new Tile(3020, 3231, 0), new Tile(3020, 3230, 0), new Tile(3020, 3229, 0), new Tile(3020, 3228, 0), new Tile(3020, 3227, 0), new Tile(3020, 3226, 0), new Tile(3020, 3225, 0), new Tile(3020, 3224, 0), new Tile(3020, 3223, 0), new Tile(3020, 3222, 0), new Tile(3020, 3221, 0), new Tile(3019, 3221, 0), new Tile(3019, 3220, 0), new Tile(3018, 3220, 0), new Tile(3018, 3219, 0), new Tile(3017, 3219, 0), new Tile(3017, 3218, 0), new Tile(3016, 3218, 0), new Tile(3016, 3217, 0), new Tile(3015, 3217, 0), new Tile(3014, 3217, 0), new Tile(3013, 3217, 0), new Tile(3013, 3216, 0), new Tile(3012, 3216, 0) );
	private static TilePath draynorVillageLStoVarrockLS = new TilePath( new Tile(3105, 3298, 0), new Tile(3106, 3298, 0), new Tile(3107, 3298, 0), new Tile(3108, 3298, 0), new Tile(3109, 3298, 0), new Tile(3110, 3298, 0), new Tile(3111, 3298, 0), new Tile(3111, 3299, 0), new Tile(3111, 3300, 0), new Tile(3112, 3300, 0), new Tile(3112, 3301, 0), new Tile(3113, 3301, 0), new Tile(3114, 3301, 0), new Tile(3115, 3301, 0), new Tile(3115, 3302, 0), new Tile(3116, 3302, 0), new Tile(3117, 3302, 0), new Tile(3118, 3302, 0), new Tile(3119, 3302, 0), new Tile(3120, 3302, 0), new Tile(3121, 3302, 0), new Tile(3122, 3302, 0), new Tile(3122, 3303, 0), new Tile(3123, 3303, 0), new Tile(3124, 3303, 0), new Tile(3125, 3303, 0), new Tile(3125, 3304, 0), new Tile(3126, 3304, 0), new Tile(3127, 3304, 0), new Tile(3128, 3304, 0), new Tile(3129, 3304, 0), new Tile(3129, 3305, 0), new Tile(3129, 3306, 0), new Tile(3130, 3306, 0), new Tile(3131, 3306, 0), new Tile(3131, 3307, 0), new Tile(3131, 3308, 0), new Tile(3132, 3308, 0), new Tile(3133, 3308, 0), new Tile(3133, 3309, 0), new Tile(3134, 3309, 0), new Tile(3135, 3309, 0), new Tile(3135, 3310, 0), new Tile(3136, 3310, 0), new Tile(3137, 3310, 0), new Tile(3138, 3310, 0), new Tile(3139, 3310, 0), new Tile(3140, 3310, 0), new Tile(3140, 3311, 0), new Tile(3141, 3311, 0), new Tile(3142, 3311, 0), new Tile(3143, 3311, 0), new Tile(3144, 3311, 0), new Tile(3145, 3311, 0), new Tile(3146, 3311, 0), new Tile(3147, 3311, 0), new Tile(3148, 3311, 0), new Tile(3149, 3311, 0), new Tile(3150, 3311, 0), new Tile(3151, 3311, 0), new Tile(3152, 3311, 0), new Tile(3153, 3311, 0), new Tile(3154, 3311, 0), new Tile(3155, 3311, 0), new Tile(3156, 3311, 0), new Tile(3157, 3311, 0), new Tile(3158, 3311, 0), new Tile(3159, 3311, 0), new Tile(3160, 3311, 0), new Tile(3161, 3311, 0), new Tile(3162, 3311, 0), new Tile(3162, 3312, 0), new Tile(3163, 3312, 0), new Tile(3164, 3312, 0), new Tile(3164, 3313, 0), new Tile(3165, 3313, 0), new Tile(3166, 3313, 0), new Tile(3167, 3313, 0), new Tile(3168, 3313, 0), new Tile(3169, 3313, 0), new Tile(3170, 3313, 0), new Tile(3171, 3313, 0), new Tile(3172, 3313, 0), new Tile(3173, 3313, 0), new Tile(3174, 3313, 0), new Tile(3175, 3313, 0), new Tile(3176, 3313, 0), new Tile(3176, 3314, 0), new Tile(3176, 3315, 0), new Tile(3176, 3316, 0), new Tile(3176, 3317, 0), new Tile(3176, 3318, 0), new Tile(3176, 3319, 0), new Tile(3177, 3319, 0), new Tile(3177, 3320, 0), new Tile(3177, 3321, 0), new Tile(3177, 3322, 0), new Tile(3177, 3323, 0), new Tile(3177, 3324, 0), new Tile(3177, 3325, 0), new Tile(3177, 3326, 0), new Tile(3177, 3327, 0), new Tile(3178, 3327, 0), new Tile(3178, 3328, 0), new Tile(3178, 3329, 0), new Tile(3178, 3330, 0), new Tile(3178, 3331, 0), new Tile(3178, 3332, 0), new Tile(3178, 3333, 0), new Tile(3178, 3334, 0), new Tile(3178, 3335, 0), new Tile(3178, 3336, 0), new Tile(3178, 3337, 0), new Tile(3178, 3338, 0), new Tile(3178, 3339, 0), new Tile(3178, 3340, 0), new Tile(3178, 3341, 0), new Tile(3177, 3341, 0), new Tile(3177, 3342, 0), new Tile(3177, 3343, 0), new Tile(3177, 3344, 0), new Tile(3177, 3345, 0), new Tile(3177, 3346, 0), new Tile(3176, 3346, 0), new Tile(3176, 3347, 0), new Tile(3176, 3348, 0), new Tile(3176, 3349, 0), new Tile(3176, 3350, 0), new Tile(3176, 3351, 0), new Tile(3176, 3352, 0), new Tile(3176, 3353, 0), new Tile(3176, 3354, 0), new Tile(3176, 3355, 0), new Tile(3176, 3356, 0), new Tile(3176, 3357, 0), new Tile(3176, 3358, 0), new Tile(3176, 3359, 0), new Tile(3177, 3359, 0), new Tile(3177, 3360, 0), new Tile(3177, 3361, 0), new Tile(3177, 3362, 0), new Tile(3178, 3362, 0), new Tile(3178, 3363, 0), new Tile(3178, 3364, 0), new Tile(3179, 3364, 0), new Tile(3180, 3364, 0), new Tile(3180, 3365, 0), new Tile(3180, 3366, 0), new Tile(3181, 3366, 0), new Tile(3182, 3366, 0), new Tile(3182, 3367, 0), new Tile(3182, 3368, 0), new Tile(3183, 3368, 0), new Tile(3184, 3368, 0), new Tile(3184, 3369, 0), new Tile(3184, 3370, 0), new Tile(3185, 3370, 0), new Tile(3186, 3370, 0), new Tile(3187, 3370, 0), new Tile(3188, 3370, 0), new Tile(3189, 3370, 0), new Tile(3190, 3370, 0), new Tile(3191, 3370, 0), new Tile(3192, 3370, 0), new Tile(3193, 3370, 0), new Tile(3194, 3370, 0), new Tile(3195, 3370, 0), new Tile(3195, 3371, 0), new Tile(3196, 3371, 0), new Tile(3197, 3371, 0), new Tile(3197, 3372, 0), new Tile(3198, 3372, 0), new Tile(3199, 3372, 0), new Tile(3200, 3372, 0), new Tile(3201, 3372, 0), new Tile(3202, 3372, 0), new Tile(3202, 3373, 0), new Tile(3203, 3373, 0), new Tile(3204, 3373, 0), new Tile(3205, 3373, 0), new Tile(3206, 3373, 0), new Tile(3207, 3373, 0), new Tile(3207, 3374, 0), new Tile(3208, 3374, 0), new Tile(3209, 3374, 0), new Tile(3210, 3374, 0), new Tile(3210, 3375, 0), new Tile(3211, 3375, 0), new Tile(3211, 3376, 0), new Tile(3212, 3376, 0), new Tile(3213, 3376, 0), new Tile(3214, 3376, 0) );
	private static TilePath lumbridgeLStoAlKharidLS = new TilePath( new Tile(3233, 3221, 0), new Tile(3234, 3221, 0), new Tile(3235, 3221, 0), new Tile(3235, 3222, 0), new Tile(3235, 3223, 0), new Tile(3235, 3224, 0), new Tile(3236, 3224, 0), new Tile(3236, 3225, 0), new Tile(3236, 3226, 0), new Tile(3237, 3226, 0), new Tile(3238, 3226, 0), new Tile(3239, 3226, 0), new Tile(3240, 3226, 0), new Tile(3240, 3225, 0), new Tile(3241, 3225, 0), new Tile(3242, 3225, 0), new Tile(3243, 3225, 0), new Tile(3244, 3225, 0), new Tile(3245, 3225, 0), new Tile(3246, 3225, 0), new Tile(3247, 3225, 0), new Tile(3248, 3225, 0), new Tile(3249, 3225, 0), new Tile(3250, 3225, 0), new Tile(3251, 3225, 0), new Tile(3252, 3225, 0), new Tile(3253, 3225, 0), new Tile(3254, 3225, 0), new Tile(3255, 3225, 0), new Tile(3256, 3225, 0), new Tile(3257, 3225, 0), new Tile(3258, 3225, 0), new Tile(3258, 3226, 0), new Tile(3259, 3226, 0), new Tile(3260, 3226, 0), new Tile(3261, 3226, 0), new Tile(3262, 3226, 0), new Tile(3262, 3227, 0), new Tile(3263, 3227, 0), new Tile(3264, 3227, 0), new Tile(3265, 3227, 0), new Tile(3266, 3227, 0), new Tile(3266, 3228, 0), new Tile(3267, 3228, 0), new Tile(3268, 3228, 0), new Tile(3269, 3228, 0), new Tile(3270, 3228, 0), new Tile(3271, 3228, 0), new Tile(3272, 3228, 0), new Tile(3273, 3228, 0), new Tile(3274, 3228, 0), new Tile(3275, 3228, 0), new Tile(3276, 3228, 0), new Tile(3276, 3227, 0), new Tile(3277, 3227, 0), new Tile(3277, 3226, 0), new Tile(3277, 3225, 0), new Tile(3278, 3225, 0), new Tile(3278, 3224, 0), new Tile(3278, 3223, 0), new Tile(3279, 3223, 0), new Tile(3279, 3222, 0), new Tile(3279, 3221, 0), new Tile(3279, 3220, 0), new Tile(3280, 3220, 0), new Tile(3280, 3219, 0), new Tile(3280, 3218, 0), new Tile(3281, 3218, 0), new Tile(3281, 3217, 0), new Tile(3282, 3217, 0), new Tile(3282, 3216, 0), new Tile(3282, 3215, 0), new Tile(3283, 3215, 0), new Tile(3283, 3214, 0), new Tile(3283, 3213, 0), new Tile(3283, 3212, 0), new Tile(3283, 3211, 0), new Tile(3283, 3210, 0), new Tile(3283, 3209, 0), new Tile(3283, 3208, 0), new Tile(3283, 3207, 0), new Tile(3283, 3206, 0), new Tile(3283, 3205, 0), new Tile(3283, 3204, 0), new Tile(3283, 3203, 0), new Tile(3283, 3202, 0), new Tile(3283, 3201, 0), new Tile(3283, 3200, 0), new Tile(3284, 3200, 0), new Tile(3284, 3199, 0), new Tile(3284, 3198, 0), new Tile(3285, 3198, 0), new Tile(3286, 3198, 0), new Tile(3287, 3198, 0), new Tile(3288, 3198, 0), new Tile(3288, 3197, 0), new Tile(3289, 3197, 0), new Tile(3290, 3197, 0), new Tile(3290, 3196, 0), new Tile(3291, 3196, 0), new Tile(3291, 3195, 0), new Tile(3292, 3195, 0), new Tile(3292, 3194, 0), new Tile(3292, 3193, 0), new Tile(3292, 3192, 0), new Tile(3292, 3191, 0), new Tile(3292, 3190, 0), new Tile(3292, 3189, 0), new Tile(3292, 3188, 0), new Tile(3293, 3188, 0), new Tile(3293, 3187, 0), new Tile(3293, 3186, 0), new Tile(3294, 3186, 0), new Tile(3294, 3185, 0), new Tile(3295, 3185, 0), new Tile(3296, 3185, 0) );
	
	private static int[] faladorGateClosedIds = new int[] { 28690, 28691 };
	private static int[] faladorGateOpenedIds = new int[] { 28692, 28693 };
	private static int[] wildernessWallIds = new int[] { 65076, 65077, 65078, 65079, 65080, 65081, 65082, 65083, 65084, 65085, 65086, 65087  };
	
	private Lodestone(int keyShortcutModifier, int keyShortcut, boolean membersOnly, int aaoId, ComponentData componentData, int isActivatedSettingShift) {
		this.keyShortcutModifier = keyShortcutModifier;
		this.keyShortcut = keyShortcut;
		this.membersOnly = membersOnly;
		this.aaoId = aaoId;
		this.componentData = componentData;
		this.isActivatedSettingShift = isActivatedSettingShift;
	}

	public boolean isActivated() {
		int lodestone = Client.getSettingData().getSettings().get(3, isActivatedSettingShift, 0x1); // mask for all: FF FFFF
		return lodestone == 1;
	}

	public boolean teleport(boolean waitUntilWalkOff) {
		if (!isActivated()) {
			System.out.println("Lodestone " + this + " not activated! Attempting to activate...");
			
			if (!activate())
				return false;
			else {
				System.out.println("Successfully activated " + this + " lodestone.");
				Time.sleepMedium();
			}
		}
		
		GroundObject lodestone = GroundObjects.getNearest(Type.AnimatedAnimableObject, aaoId);
		if (lodestone != null && Calculations.distanceTo(lodestone) < 25)
			return true;
		
		if (LodestoneNetwork.get.open(true)) {
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				if (LodestoneNetwork.get.isOpen()) {
					break;
				}
				
				Time.sleep(50);
			}
			Time.sleepVeryQuick();
			
			if (LodestoneNetwork.get.isOpen()) {
				Component comp = componentData.getComponent();
				if (comp != null) {
					if (comp.loopInteract(null, "Teleport")) {
						if (this.equals(WILDERNESS_VOLCANO)) { // BEGIN Wilderness Volcano chat option chooser
							// close ContinueMessage
							boolean success = false;
							timer.reset();
							while (timer.isRunning()) {
								if (ContinueMessage.isOpen()) {
									String text = ContinueMessage.getText(true);
									if (text != null && text.contains("lodestone")) {
										success = true;
										break;
									}
								}
								
								Time.sleep(50);
							}
							
							if (success) { // no need to return false if not true; the message may not pop up depending on some other things
								if (ContinueMessage.close(true)) {
									// choose yes on chat option pane
									success = false;
									timer.reset();
									while (timer.isRunning()) {
										if (ChatOptionPane.get.isOpen()) {
											String header = ChatOptionPane.get.getHeader();
											if (header != null && header.toLowerCase().contains("teleport")) {
												success = true;
												break;
											}
										}
										
										Time.sleep(50);
									}
									
									if (success) {
										if (!ChatOptionPane.get.chooseYes(true))
											return false;
									} else
										return false; // if the continue message came up, then the chat option pane must
								}
							}
						} // END Wilderness Volcano chat option chooser
						
						Player player = Players.getMyPlayer();
						if (player == null)
							return false;
						
						boolean success = false;
						timer.reset();
						while (timer.isRunning()) {
							if (Constants.Animation.LODESTONE_TELEPORT_START.getIds().contains(player.getAnimationId())) {
								// double check to make sure player isn't already at lodestone teleport destination
								lodestone = GroundObjects.getNearest(Type.AnimatedAnimableObject, aaoId);
								if (lodestone != null && Calculations.distanceTo(lodestone) < 25)
									return true;
								
								success = true;
								break;
							}
							
							Time.sleep(50);
						}
						
						if (success) {
							Timer longTimer = new Timer(12000);
							while (longTimer.isRunning()) {
								if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
									return false;
								
								Time.sleep(50);
							}
							
							success = false;
							timer.reset();
							while (timer.isRunning()) {
								if (Constants.Animation.LODESTONE_TELEPORT_END.getIds().contains(player.getAnimationId())) {
									success = true;
									break;
								}
								
								Time.sleep(50);
							}
							
							if (success) {
								GroundObject go = GroundObjects.getNearest(Type.AnimatedAnimableObject, aaoId);
								if (go != null) {
									if (waitUntilWalkOff) {
										success = false;
										timer.reset();
										while (timer.isRunning()) {
											if (Constants.Animation.LODESTONE_TELEPORT_END_WALK.getIds().contains(player.getAnimationId())) {
												success = true;
												break;
											}
											
											Time.sleep(50);
										}
										
										if (success) {
											success = false;
											timer.reset();
											while (timer.isRunning()) {
												if (player.getAnimationId() == -1) {
													success = true;
													break;
												}
												
												Time.sleep(50);
											}
										}
									}
									
									Time.sleepQuick();
									//System.out.println("success!");
									return true;
								}
							}
						}
					}
				}
			}
		}

		return false;
	}
	
	public boolean activate() {
		if (isActivated())
			return true;
		
		TilePath path;
		
		switch (this) {
		case TAVERLEY:
			path = burthopeLStoTaverlyLS.randomize(4, 4);
			if (path.getDistanceToNearest() < 25.0 || BURTHORPE.teleport(true)) {
				Time.sleepQuick();
				
				// manually completed in about 40 seconds at normal speed
				if (path.loopWalk(80000, null, true)) {
					Time.sleepQuick();
					return loopInteractActivate();
				}
			}
			break;
		case FALADOR:
			if (!TAVERLEY.isActivated() && TAVERLEY.activate()) {
				Time.sleepMedium();
			}
			
			if (TAVERLEY.isActivated()) {
				if (tavleryLStoFaladorGate.getDistanceToNearest() < 25.0 || faladorGateTofaladorLS.getDistanceToNearest() < 25.0 || TAVERLEY.teleport(true)) {
					Time.sleepQuick();
					
					Condition crossedFaladorGate = new Condition() {
						@Override
						public boolean evaluate() {
							Player player = Players.getMyPlayer();
							if (player == null)
								return false;
							
							return player.getTile().getX() >= 2942;
						}
					};
					
					Condition faladorGateOpened = new Condition() {
						@Override
						public boolean evaluate() {
							GroundObject opened = GroundObjects.getNearest(Type.All, faladorGateOpenedIds);
							GroundObject closed = GroundObjects.getNearest(Type.All, faladorGateClosedIds);
							
							if (closed != null)
								return false;
							
							if (opened != null)
								return true;
							
							return false;
						}
					};
					
					if (!crossedFaladorGate.evaluate()) {
						path = tavleryLStoFaladorGate.randomize(4, 4);
						
						// manually completed in about 30 seconds at normal speed
						if (path.loopWalk(60000, null, true)) {
							if (!faladorGateOpened.evaluate()) {
								GroundObject closed = null;
								
								Timer timer = new Timer(3000);
								while (timer.isRunning()) {
									closed = GroundObjects.getNearest(Type.All, faladorGateClosedIds);
									if (closed != null)
										break;
									
									Time.sleep(50);
								}
								 
								if (closed != null) {
									if (closed.loopInteract(faladorGateOpened, "Open")) {
										timer = new Timer(5000);
										while (timer.isRunning()) {
											if (faladorGateOpened.evaluate()) {
												//System.out.println("Opened Falador gate");
												break;
											}
											
											Time.sleep(50);
										}
									}
									Time.sleepQuick();
								}
							}
						} else
							return false;
					}
					
					if (crossedFaladorGate.evaluate() || faladorGateOpened.evaluate()) {
						path = faladorGateTofaladorLS.randomize(4, 4);
						
						// manually completed in about 12 seconds at normal speed
						if (path.loopWalk(24000, null, true)) {
							Time.sleepQuick();
							return loopInteractActivate();
						}
					}
				}
			}
			
			break;
		case EDGEVILLE:
			if (!FALADOR.isActivated() && FALADOR.activate()) {
				Time.sleepMedium();
			}
			
			if (FALADOR.isActivated()) {
				path = faladorLStoEdgvilleLS.randomize(4, 4);
				if (path.getDistanceToNearest() < 25.0 || FALADOR.teleport(true)) {
					Time.sleepQuick();
					
					// manually completed in about 50 seconds at normal speed
					if (path.loopWalk(100000, null, true)) {
						Time.sleepQuick();
						return loopInteractActivate();
					}
				}
			}
			break;
		case WILDERNESS_VOLCANO:
			if (!EDGEVILLE.isActivated() && EDGEVILLE.activate()) {
				Time.sleepMedium();
			}
			
			if (EDGEVILLE.isActivated()) {
				if (edgvilleLStoWildernessWall.getDistanceToNearest() < 25.0 || wildernessWalltoWildernessVolcanoLS.getDistanceToNearest() < 25.0 || EDGEVILLE.teleport(true)) {
					Time.sleepQuick();
					
					Condition crossedWildernessWall = new Condition() {
						@Override
						public boolean evaluate() {
							Player player = Players.getMyPlayer();
							if (player == null)
								return false;
							
							return player.getTile().getY() >= 3520;
						}
					};
					
					if (!crossedWildernessWall.evaluate()) {
						path = edgvilleLStoWildernessWall.randomize(4, 4);
						
						// manually completed in about 6 seconds at normal speed
						if (path.loopWalk(12000, null, true)) {
							Time.sleepQuick();
							
							GroundObject wall = null;
							Timer timer = new Timer(3000);
							while (timer.isRunning()) {
								wall = GroundObjects.getNearest(Type.All, wildernessWallIds);
								if (wall != null)
									break;
								
								Time.sleep(50);
							}
							
							if (wall != null) {
								if (wall.loopInteract(null, "Cross")) {
									Time.sleepQuick();
									
									boolean warningAppeared = false;
									timer = new Timer(8000);
									while (timer.isRunning()) {
										if (EnterWildernessWarning.get.isOpen()) {
											warningAppeared = true;
											
											EnterWildernessWarning.get.chooseEnter(true);
											Time.sleepMedium();
										}
										
										if (warningAppeared && EnterWildernessWarning.get.isClosed()) {
											if (crossedWildernessWall.evaluate()) {
												//System.out.println("Crossed Wilderness Wall");
												break;
											}
										}
										
										Time.sleep(50);
									}
								}
								Time.sleepQuick();
							}
						} else
							return false;
					}
					
					if (crossedWildernessWall.evaluate()) {
						path = wildernessWalltoWildernessVolcanoLS.randomize(4, 4);
						
						// manually completed in about 40 seconds at normal speed
						if (path.loopWalk(80000, null, true)) {
							Time.sleepQuick();
							return loopInteractActivate();
						}
					}
				}
			}
			break;
		case DRAYNOR_VILLAGE:
			path = lumbridgeLStoDraynorVillageLS.randomize(4, 4);
			if (path.getDistanceToNearest() < 25.0 || LUMBRIDGE.teleport(true)) {
				Time.sleepQuick();
				
				// manually completed in about 50 seconds at normal speed
				if (path.loopWalk(100000, null, true)) {
					Time.sleepQuick();
					return loopInteractActivate();
				}
			}
			break;
		case PORT_SARIM:
			if (!DRAYNOR_VILLAGE.isActivated() && DRAYNOR_VILLAGE.activate()) {
				Time.sleepMedium();
			}
			
			if (DRAYNOR_VILLAGE.isActivated()) {
				path = draynorVillageLStoPortSarimLS.randomize(4, 4);
				if (path.getDistanceToNearest() < 25.0 || DRAYNOR_VILLAGE.teleport(true)) {
					Time.sleepQuick();
					
					// manually completed in about 40 seconds at normal speed
					if (path.loopWalk(80000, null, true)) {
						Time.sleepQuick();
						return loopInteractActivate();
					}
				}
			}
			break;
		case VARROCK:
			if (!DRAYNOR_VILLAGE.isActivated() && DRAYNOR_VILLAGE.activate()) {
				Time.sleepMedium();
			}
			
			if (DRAYNOR_VILLAGE.isActivated()) {
				path = draynorVillageLStoVarrockLS.randomize(4, 4);
				if (path.getDistanceToNearest() < 25.0 || DRAYNOR_VILLAGE.teleport(true)) {
					Time.sleepQuick();
					
					// manually completed in about 50 seconds at normal speed
					if (path.loopWalk(100000, null, true)) {
						Time.sleepQuick();
						return loopInteractActivate();
					}
				}
			}
			break;
		case AL_KHARID:
			path = lumbridgeLStoAlKharidLS.randomize(4, 4);
			if (path.getDistanceToNearest() < 25.0 || LUMBRIDGE.teleport(true)) {
				Time.sleepQuick();
				
				// manually completed in about 30 seconds at normal speed
				if (path.loopWalk(60000, null, true)) {
					Time.sleepQuick();
					return loopInteractActivate();
				}
			}
			break;
		default:
			System.out.println("Lodestone " + this + " activate() method unimplemented!");
		}
		
		return false;
	}
	
	/**
	 * Used to reduce boiler plate code in activate() method
	 * @return
	 */
	private boolean loopInteractActivate() {
		GroundObject go = GroundObjects.getNearest(Type.All, aaoId);
		if (go != null) {
			// TODO: cannot interact with model because bot currently cannot obtain model, so we interact with tile
			Tile tile = go.getTile();
			if (tile != null) {
				if (tile.loopInteract(new Condition() {
					@Override
					public boolean evaluate() {
						return isActivated();
					}
				}, "Activate")) {
					
					Timer timer = new Timer(10000);
					while (timer.isRunning()) {
						if (isActivated())
							return true;
						
						Time.sleep(50);
					}
				}
			}
		}
		
		return false;
	}

	public static EnumSet<Lodestone> getAllActivated() {
		EnumSet<Lodestone> set = EnumSet.noneOf(Lodestone.class);
		for (Lodestone lodestone : values()) {
			if (lodestone.isActivated()) {
				set.add(lodestone);
			}
		}

		return set;
	}
}
