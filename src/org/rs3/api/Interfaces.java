package org.rs3.api;

import java.util.HashMap;
import java.util.Map;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.OptionsMenuWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.useless.TreasureHunterChest;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.ComponentNode;
import org.rs3.api.wrappers.HashTable;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.Widget;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Interfaces {
	
	public static Widget[] cache_widgetCache = null;
	public static Component[][] cache_widgetComponentCache = null;
	public static Widget[] cache_validatedWidgetCache = null;
	public static Component[][] cache_validatedWidgetComponentCache = null;
	public static Map<Integer, ComponentNode> cache_componentNodes = null; // Integer key is mainId
	
	public static boolean useWidgetComponentCache = false;
	public static boolean useCachedComponentNodes = false;
	
	public static final int[] internalItemExceptionIds = new int[] { 24154, 36799 }; // Key token, sliskelion piece
	
	public static int getFullId(int widgetId, int componentId) {
		return widgetId << 16 | componentId;
	}
	
	public static Component getChild(final int id) {
		return getChild(id, useWidgetComponentCache);
	}
	
	public static Component getChild(final int id, boolean useCachedWidgetComponents) {
		if (!useCachedWidgetComponents)
			return __getChild(id, Client.getWidgetCache());
		else
			return __getChild(id, cache_widgetComponentCache);
	}
	
	public static Component __getChild(final int id, 
			Component[][] widgetComponentCache) {
		
		if (widgetComponentCache == null)
			return null;
		
		final int x = id >> 16;
		final int y = id & 0xffff;
		
		if (x < 0 || x > widgetComponentCache.length)
			return null;
		
		if (widgetComponentCache[x] == null)
			return null;
		
		if (y < 0 || y > widgetComponentCache[x].length)
			return null;
		
		return widgetComponentCache[x][y]; //return Client.getWidgetCache()[x].getComponents()[y];
	}
	
	public static Component __getChild(final int id, 
			Widget[] widgetCache) {
		
		if (widgetCache == null)
			return null;
		
		final int x = id >> 16;
		final int y = id & 0xffff;
		
		if (x < 0 || x > widgetCache.length)
			return null;
		
		if (widgetCache[x] == null)
			return null;
		
		Array1D<Component> components = widgetCache[x].getComponents();
		
		if (components == null || y < 0 || y > components.getLength())
			return null;
		
		return components.get(y); //return Client.getWidgetCache()[x].getComponents()[y];
	}
	
	public static Component __getChild(final int id, Array1D<Widget> widgetCache) {
		
		if (widgetCache == null)
			return null;
		
		final int x = id >> 16;
		final int y = id & 0xffff;
		
		if (x < 0 || x > widgetCache.getLength())
			return null;
		
		Widget widget = widgetCache.get(x);
		if (widget == null)
			return null;
		
		Array1D<Component> components = widget.getComponents();
		
		if (components == null || y < 0 || y > components.getLength())
			return null;
		
		return components.get(y); //return Client.getWidgetCache()[x].getComponents()[y];
	}
	
	private static void attempToClose(Component component, int... exclusionIds) {
		if (component != null) {
			int id = component.getId();
			if (exclusionIds != null) {
				for (int exclusionId : exclusionIds) {
					if (id == exclusionId)
						return;
				}
			}
			
			if (component.isVisible()) {
				String[] actions = component.getActions();
				if (actions != null) {
					for (String action : actions) {
						if (action == null)
							continue;
						
						if (action.toLowerCase().startsWith("close")) {
							Timer timer = new Timer(5000);
							while (timer.isRunning()) {
								boolean closed = false;
								
								if (component.interact(action)) {
									Time.sleepVeryQuick();
									
									Timer checkIfClosedTimer = new Timer(5000);
									while (checkIfClosedTimer.isRunning()) {
										if (!component.isVisible()) {
											closed = true;
											break;
										}
										
										Time.sleep(50);
									}
								}
								
								Time.sleepVeryQuick();
								//Time.sleep(350, 500);
								
								if (closed)
									break;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * Attempts interaction with all component actions, otherwise clicks component.
	 * 
	 * @param waitAfter Not used, this method always waits after if interaction is performed.
	 * @param isOpenCondition
	 * @param closeButton
	 * @return
	 */
	public static boolean defaultCloseInterface(boolean waitAfter, Condition isOpenCondition, Component comp) {
		return defaultInteractInterface(true, waitAfter, isOpenCondition, comp);
	}
	
	/**
	 * 
	 * Attempts interaction with all component actions, otherwise clicks component.
	 * 
	 * @param shouldClose Whether this interaction is meant to close a component.
	 * @param waitAfter Not used, this method always waits after if interaction is performed.
	 * @param isOpenCondition
	 * @param closeButton
	 * @return
	 */
	public static boolean defaultInteractInterface(boolean shouldClose, boolean waitAfter, Condition isOpenCondition, Component comp) {
		
		if (shouldClose && isOpenCondition != null && !isOpenCondition.evaluate())
			return true;
		
		if (comp != null && comp.isVisible()) {
			String[] actions = comp.getActions();
			if (actions != null) {
				boolean success = false;
				
				for (String action : actions) {
					if (action != null && !action.toLowerCase().equals("cancel")) {
						System.out.println(action);
						if (comp.interact(action)) // should be Close or Select, but this will make sure it's closed
							success = true;
						
						Time.sleepVeryQuick();
					}
				}
				
				return success;
			} else {
				boolean clicked = comp.click(Mouse.LEFT);
				Time.sleepVeryQuick();
				
				if (clicked) {
					if (shouldClose && isOpenCondition != null) {
						Timer timer = new Timer(500);
						while (timer.isRunning()) {
							if (!isOpenCondition.evaluate())
								return true;
							
							Time.sleep(50);
						}
					} else
						return true;
				}
			}
		}
		
		return false;
	}
	
	public static final int[] closeUselessInterfacesExclusionIds = new int[]{
			OptionsMenuWidget.closeButtonData.getFullId(), // only excluded because its visibility is always true
			
			MainWidget.tabBackPackCloseButton.getFullId(),
			MainWidget.tabWornEquipmentCloseButton.getFullId(),
			MainWidget.tabPrayersCloseButton.getFullId(),
			MainWidget.tabMeleeAbilitiesCloseButton.getFullId(),
			MainWidget.tabRangedAbilitiesCloseButton.getFullId(),
			MainWidget.tabMagicAbilitiesCloseButton.getFullId(),
			MainWidget.tabDefenseAbilitiesCloseButton.getFullId(),
			MainWidget.tabSkillsCloseButton.getFullId(),
			MainWidget.tabActiveTaskCloseButton.getFullId(),
			MainWidget.tabMusicPlayerCloseButton.getFullId(),
			MainWidget.tabFriendsCloseButton.getFullId(),
			MainWidget.tabClanCloseButton.getFullId(),
			MainWidget.tabGroupMembersCloseButton.getFullId(),
			MainWidget.tabNotesCloseButton.getFullId(),
			MainWidget.tabFriendsChatInfoCloseButton.getFullId(),
			
			// Ribbon subinterface buttons always have "Close" as action
			Ribbon.adventuresButtonData.getFullId(),
			Ribbon.communityButtonData.getFullId(),
			Ribbon.extrasButtonData.getFullId(),
			Ribbon.customisationsButtonData.getFullId(),
			//Ribbon.groupingSystemButtonData.getFullId(),
			Ribbon.christmas2015ButtonData.getFullId(),
			Ribbon.heroButtonData.getFullId(),
			Ribbon.powersButtonData.getFullId(),
			Ribbon.settingsButtonData.getFullId()
			};
	
	public static void closeUselessInterfaces(int... exclusionIds) {
		boolean orig_useCachedComponentNodes = useCachedComponentNodes;
		boolean orig_useWidgetComponentCache = useWidgetComponentCache;
		
		useCachedComponentNodes = true;
		useWidgetComponentCache = true;
		updateCache_componentNodes();
		updateCache_validatedWidgetCache(); // TODO: should it not be validated? not sure if the Interfaces check uses it or regular cache
		
		if (cache_validatedWidgetComponentCache != null) {
			if (TreasureHunterChest.get.isOpen()) {
				Component treasureHunterChestCloseButton = TreasureHunterChest.closeButtonData.getComponent();
				int closeId = treasureHunterChestCloseButton.getId();
				
				boolean excluded = false;
				if (exclusionIds != null) {
					for (int exclusionId : exclusionIds) {
						if (exclusionId == closeId) {
							excluded = true;
							break;
						}
					}
				}
				
				if (!excluded) {
					TreasureHunterChest.get.close();
				}
			}
			
			for (Component[] parent : cache_validatedWidgetComponentCache) {
				if (parent == null)
					continue;
				
				for (Component parentChild : parent) {
					if (parentChild == null)
						continue;
					
					attempToClose(parentChild, exclusionIds);
					
					
					Array1D<Component> children = parentChild.getComponents();
					if (children != null) {
						for (Component child : children.getAll()) {
							if (child == null)
								continue;
							
							attempToClose(child, exclusionIds);
							
							Array1D<Component> children2 = child.getComponents();
							if (children2 != null) {
								for (Component child2 : children2.getAll()) {
									if (child2 == null)
										continue;
									
									attempToClose(child2, exclusionIds);
								}
							}
						}
					}
				}
			}
		}
		
		useCachedComponentNodes = orig_useCachedComponentNodes;
		useWidgetComponentCache = orig_useWidgetComponentCache;
	}
	
	
    public static void updateCache_componentNodes() {
    	HashTable nc = Client.getComponentNodeCache();
    	
    	if (nc != null) {
    		Node[] nodes = nc.getAll();
    		
    		if (nodes != null) {
	    		//ComponentNode[] newArray = new ComponentNode[nodes.length];
	    		Map<Integer, ComponentNode> map = new HashMap<>();
    			
	    		for (int i = 0; i < nodes.length; i++) {
	    			if (nodes[i] == null)
	    				continue;
	    			
	    			//newArray[i] = nodes[i].forceCast(ComponentNode.class);
	    			ComponentNode cn = nodes[i].forceCast(ComponentNode.class);
	    			map.put(cn.getMainId(), cn);
	    		}
	    		//cache_componentNodes = newArray;
	    		cache_componentNodes = map;
    		}
    	}
    }
    
    public static void updateCache_widgetCache() {
    	Array1D<Widget> wrapper = Client.getWidgetCache();
    	
    	if (wrapper != null)
    		cache_widgetCache = wrapper.getAll();
    	else
    		cache_widgetCache = null;
    	
    	if (cache_widgetCache != null) {
    		cache_widgetComponentCache = new Component[cache_widgetCache.length][];
    		
    		for (int wi = 0; wi < cache_widgetCache.length; wi++) {
    			if (cache_widgetCache[wi] != null) {
    				Array1D<Component> children = cache_widgetCache[wi].getComponents();
    				
    				if (children != null)
    					cache_widgetComponentCache[wi] = children.getAll();
    				else
    					cache_widgetComponentCache[wi] = null;
    			} else
    				cache_widgetComponentCache[wi] = null;
    		}
    	} else
    		cache_widgetComponentCache = null;
    }
    
    public static void updateCache_validatedWidgetCache() {
    	cache_validatedWidgetCache = Client.getValidatedWidgetCache(false);
    	
    	if (cache_validatedWidgetCache != null) {
    		cache_validatedWidgetComponentCache = new Component[cache_validatedWidgetCache.length][];
    		
    		for (int wi = 0; wi < cache_validatedWidgetCache.length; wi++) {
    			if (cache_validatedWidgetCache[wi] != null) {
    				Array1D<Component> children = cache_validatedWidgetCache[wi].getComponents();
    				
    				if (children != null)
    					cache_validatedWidgetComponentCache[wi] = children.getAll();
    				else
    					cache_validatedWidgetComponentCache[wi] = null;
    			} else
    				cache_validatedWidgetComponentCache[wi] = null;
    		}
    	} else
    		cache_validatedWidgetComponentCache = null;
    }
}