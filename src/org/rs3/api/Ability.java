package org.rs3.api;

import org.rs3.api.ActionBar.AbilityRequirement;
import org.rs3.api.ActionBar.AbilitySkillType;
import org.rs3.api.ActionBar.AbilityType;
import org.rs3.api.interfaces.widgets.AbilitySubTab;

// TODO: all abilities settings ids add 1 or 2 to a previous unknown id, might be useless info tho
public enum Ability {
	
	//
	// Other
	//
	EMPTY(null, null, false, -1, AbilityRequirement.NONE, null, null, 0),
	ITEM(null, null, false, -1, AbilityRequirement.NONE, AbilityType.ITEM, null, 10),
	
	//
	// Attack
	//
	SLICE(AbilitySkillType.ATTACK, "Slice", false, 1, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_ATTACK, 17),
	SLAUGHTER(AbilitySkillType.ATTACK, "Slaughter", false, 1, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_ATTACK, 113),
	OVERPOWER(AbilitySkillType.ATTACK, "Overpower", false, 2, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_ATTACK, 161),
	HAVOC(AbilitySkillType.ATTACK, "Havoc", true, 5, AbilityRequirement.DUAL_WIELD, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_ATTACK, 65),
	BACKHAND(AbilitySkillType.ATTACK, "Backhand", false, 10, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_ATTACK, 97),
	FORCEFUL_BACKHAND(AbilitySkillType.ATTACK, "Forceful Backhand", false, 15, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_ATTACK, 257),
	SMASH(AbilitySkillType.ATTACK, "Smash", false, 20, AbilityRequirement.TWO_HAND, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_ATTACK, 81),
	BARGE(AbilitySkillType.ATTACK, "Barge", true, 30, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_ATTACK, 33),
	FLURRY(AbilitySkillType.ATTACK, "Flurry", true, 37, AbilityRequirement.TWO_HAND, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_ATTACK, 129),
	SEVER(AbilitySkillType.ATTACK, "Sever", false, 45, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_ATTACK, 49),
	HURRICANE(AbilitySkillType.ATTACK, "Hurricane", true, 55, AbilityRequirement.TWO_HAND, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_ATTACK, 145),
	MASSACRE(AbilitySkillType.ATTACK, "Massacre", true, 66, AbilityRequirement.DUAL_WIELD, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_ATTACK, 177),
	//BLOOD_TENDRILS(AbilitySkillType.ATTACK, "Blood Tendrils", true, 75, WeaponType.ANY, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_ATTACK, -1),
	METEOR_STRIKE(AbilitySkillType.ATTACK, "Meteor Strike", true, 81, AbilityRequirement.TWO_HAND, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_ATTACK, 193),
	//BALANCED_STRIKE(AbilitySkillType.ATTACK, "Balanced Strike", true, 85, WeaponType.ANY, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_ATTACK, -1),
	
	//
	// Strength
	//
	KICK(AbilitySkillType.STRENGTH, "Kick", false, 3, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_STRENGTH, 34),
	STOMP(AbilitySkillType.STRENGTH, "Stomp", false, 3, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_STRENGTH, 210),
	PUNISH(AbilitySkillType.STRENGTH, "Punish", false, 8, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_STRENGTH, 50),
	DISMEMBER(AbilitySkillType.STRENGTH, "Dismember", true, 14, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_STRENGTH, 18),
	FURY(AbilitySkillType.STRENGTH, "Fury", false, 24, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_STRENGTH, 66),
	DESTROY(AbilitySkillType.STRENGTH, "Destroy", true, 37, AbilityRequirement.DUAL_WIELD, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_STRENGTH, 146),
	QUAKE(AbilitySkillType.STRENGTH, "Quake", true, 37, AbilityRequirement.TWO_HAND, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_STRENGTH, 130),
	BESERK(AbilitySkillType.STRENGTH, "Beserk", true, 42, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_STRENGTH, 162),
	CLEAVE(AbilitySkillType.STRENGTH, "Cleave", false, 48, AbilityRequirement.TWO_HAND, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_STRENGTH, 98),
	ASSAULT(AbilitySkillType.STRENGTH, "Assault", true, 55, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MELEE_STRENGTH, 114),
	DECIMATE(AbilitySkillType.STRENGTH, "Decimate", true, 67, AbilityRequirement.DUAL_WIELD, AbilityType.BASIC, AbilitySubTab.SUB_MELEE_STRENGTH, 82),
	PULVERISE(AbilitySkillType.STRENGTH, "Pulverise", true, 81, AbilityRequirement.TWO_HAND, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_STRENGTH, 194),
	FRENZY(AbilitySkillType.STRENGTH, "Frenzy", true, 86, AbilityRequirement.DUAL_WIELD, AbilityType.ULTIMATE, AbilitySubTab.SUB_MELEE_STRENGTH, 178),
	
	//
	// Ranged
	//
	PIERCING_SHOT(AbilitySkillType.RANGED, "Piercing Shot", false, 1, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 21),
	SNAP_SHOT(AbilitySkillType.RANGED, "Snap Shot", false, 1, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_RANGED_RANGED, 117),
	DEADSHOT(AbilitySkillType.RANGED, "Deadshot", false, 2, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_RANGED_RANGED, 197),
	SNIPE(AbilitySkillType.RANGED, "Snipe", true, 5, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 69),
	DAZING_SHOT(AbilitySkillType.RANGED, "Dazing Shot", false, 8, AbilityRequirement.TWO_HAND, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 245),
	BINDING_SHOT(AbilitySkillType.RANGED, "Binding Shot", false, 10, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 37),
	NEEDLE_STRIKE(AbilitySkillType.RANGED, "Needle Strike", true, 12, AbilityRequirement.DUAL_WIELD, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 261),
	TIGHT_BINDINGS(AbilitySkillType.RANGED, "Tight Bindings", false, 15, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_RANGED_RANGED, 293),
	FRAGMENTATION_SHOT(AbilitySkillType.RANGED, "Fragmentation Shot", false, 20, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 85),
	ESCAPE(AbilitySkillType.RANGED, "Escape", true, 30, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 53),
	RAPID_FIRE(AbilitySkillType.RANGED, "Rapid Fire", true, 37, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_RANGED_RANGED, 133),
	RICOCHET(AbilitySkillType.RANGED, "Ricochet", false, 45, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_RANGED_RANGED, 101),
	BOMBARDMENT(AbilitySkillType.RANGED, "Bombardment", true, 55, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_RANGED_RANGED, 149),
	INCENDIARY_SHOT(AbilitySkillType.RANGED, "Incendiary Shot", true, 66, AbilityRequirement.TWO_HAND, AbilityType.ULTIMATE, AbilitySubTab.SUB_RANGED_RANGED, 165),
	SHADOW_TENDRILS(AbilitySkillType.RANGED, "Shadow Tendrils", true, 75, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_RANGED_RANGED, -1),
	UNLOAD(AbilitySkillType.RANGED, "Unload", true, 81, AbilityRequirement.DUAL_WIELD, AbilityType.ULTIMATE, AbilitySubTab.SUB_RANGED_RANGED, 181),
	//DEATHS_SWIFTNESS(AbilitySkillType.RANGED, "Death's Swiftness", true, 85, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_RANGED_RANGED, -1),
	
	//
	// Magic
	//
	WRACK(AbilitySkillType.MAGIC, "Wrack", false, 1, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 22),
	ASPHYXIATE(AbilitySkillType.MAGIC, "Asphyxiate", false, 1, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MAGIC_ABILITIES, 118),
	OMNIPOWER(AbilitySkillType.MAGIC, "Omnipower", false, 2, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_MAGIC_ABILITIES, 198),
	DRAGON_BREATH(AbilitySkillType.MAGIC, "Dragon Breath", true, 5, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 102),
	SONIC_WAVE(AbilitySkillType.MAGIC, "Sonic Wave", false, 8, AbilityRequirement.TWO_HAND, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 2646),
	IMPACT(AbilitySkillType.MAGIC, "Impact", false, 10, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 54),
	CONCENTRATED_BLAST(AbilitySkillType.MAGIC, "Concentrated Blast", true, 12, AbilityRequirement.DUAL_WIELD, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 2662),
	DEEP_IMPACT(AbilitySkillType.MAGIC, "Deep Impact", false, 15, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MAGIC_ABILITIES, 2742),
	COMBUST(AbilitySkillType.MAGIC, "Combust", false, 20, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 86),
	SURGE(AbilitySkillType.MAGIC, "Surge", true, 30, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 38),
	DETONATE(AbilitySkillType.MAGIC, "Detonate", true, 37, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MAGIC_ABILITIES, 134),
	CHAIN(AbilitySkillType.MAGIC, "Chain", false, 45, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_MAGIC_ABILITIES, 70),
	WILD_MAGIC(AbilitySkillType.MAGIC, "Wild Magic", true, 55, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MAGIC_ABILITIES, 150),
	METAMORPHOSIS(AbilitySkillType.MAGIC, "Metamorphosis", true, 66, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_MAGIC_ABILITIES, 166),
	//SMOKE_TENDRILS(AbilitySkillType.MAGIC, "Smoke Tendrils", true, 75, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_MAGIC_ABILITIES, -1),
	TSUNAMI(AbilitySkillType.MAGIC, "Tsunami", true, 81, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_MAGIC_ABILITIES, 182),
	//SUNSHINE(AbilitySkillType.MAGIC, "Sunshine", true, 85, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_MAGIC_ABILITIES, -1),
	
	//
	// Constitution
	//
	WEAPON_SPECIAL_ATTACK(AbilitySkillType.CONSTITUTION, "Weapon Special attack", true, 10, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, 260),
	REGENERATE(AbilitySkillType.CONSTITUTION, "Regenerate", true, 10, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, 20),
	//SACRIFICE(AbilitySkillType.CONSTITUTION, "Sacrifice", false, 10, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, -1),
	//TRANSFIGURE(AbilitySkillType.CONSTITUTION, "Transfigure", false, 10, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, -1),
	SIPHON(AbilitySkillType.CONSTITUTION, "Siphon", false, 20, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, 244),
	INCITE(AbilitySkillType.CONSTITUTION, "Incite", true, 24, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, 36),
	//GUTHIXS_BLESSING(AbilitySkillType.CONSTITUTION, "Guthix's Blessing", true, 85, AbilityRequirement.THE_WORLD_WAKES, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, -1),
	//ICE_ASYLUM(AbilitySkillType.CONSTITUTION, "Ice Asylum", true, 91, AbilityRequirement.NONE, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_CONSTITUTION, -1),
	
	//
	// Defence
	//
	//DEVOTION(AbilitySkillType.DEFENCE, "Devotion", false, 1, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, -1),
	ANTICIPATION(AbilitySkillType.DEFENCE, "Anticipation", false, 3, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 19),
	BASH(AbilitySkillType.DEFENCE, "Bash", false, 8, AbilityRequirement.SHIELD, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 99),
	REVENGE(AbilitySkillType.DEFENCE, "Revenge", true, 15, AbilityRequirement.SHIELD, AbilityType.THRESHOLD, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 147),
	PROVOKE(AbilitySkillType.DEFENCE, "Provoke", true, 24, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 51),
	IMMORTALITY(AbilitySkillType.DEFENCE, "Immortality", true, 29, AbilityRequirement.SHIELD, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 195),
	FREEDOM(AbilitySkillType.DEFENCE, "Freedom", false, 34, AbilityRequirement.NONE, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 35),
	REFLECT(AbilitySkillType.DEFENCE, "Reflect", true, 37, AbilityRequirement.SHIELD, AbilityType.THRESHOLD, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 115),
	RESONANCE(AbilitySkillType.DEFENCE, "Resonance", true, 48, AbilityRequirement.SHIELD, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 67),
	REJUVENATE(AbilitySkillType.DEFENCE, "Rejuvenate", false, 52, AbilityRequirement.SHIELD, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 179),
	DEBILITATE(AbilitySkillType.DEFENCE, "Debilitate", true, 55, AbilityRequirement.NONE, AbilityType.THRESHOLD, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 131),
	PREPARATION(AbilitySkillType.DEFENCE, "Preparation", true, 67, AbilityRequirement.SHIELD, AbilityType.BASIC, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 83),
	BARRICADE(AbilitySkillType.DEFENCE, "Barricade", true, 81, AbilityRequirement.SHIELD, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, 163),
	//NATURAL_INSTINCT(AbilitySkillType.DEFENCE, "Natural Instinct", true, 85, AbilityRequirement.THE_WORLD_WAKES, AbilityType.ULTIMATE, AbilitySubTab.SUB_DEFENSIVE_DEFENCE, -1),
	
	
	//
	// Spells
	//
	
	// Combat
	AIR_STRIKE(Spell.AIR_STRIKE, 230),
	CONFUSE(Spell.CONFUSE, 246),
	WATER_STRIKE(Spell.WATER_STRIKE, 262),
	EARTH_STRIKE(Spell.EARTH_STRIKE, 294),
	WEAKEN(Spell.WEAKEN, 326),
	FIRE_STRIKE(Spell.FIRE_STRIKE, 342),
	AIR_BOLT(Spell.AIR_BOLT, 374),
	CURSE(Spell.CURSE, 390),
	BIND(Spell.BIND, 406),
	WATER_BOLT(Spell.WATER_BOLT, 438),
	EARTH_BOLT(Spell.EARTH_BOLT, 486),
	FIRE_BOLT(Spell.FIRE_BOLT, 534),
	AIR_BLAST(Spell.AIR_BLAST, 598),
	WATER_BLAST(Spell.WATER_BLAST, 646),
	SNARE(Spell.SNARE, 694),
	SLAYER_DART(Spell.SLAYER_DART, 710),
	EARTH_BLAST(Spell.EARTH_BLAST, 742),
	FIRE_BLAST(Spell.FIRE_BLAST, 822),
	DIVINE_STORM(Spell.DIVINE_STORM, 870),
	AIR_WAVE(Spell.AIR_WAVE, 934),
	WATER_WAVE(Spell.WATER_WAVE, 982),
	VULNERABILITY(Spell.VULNERABILITY, 1014),
	EARTH_WAVE(Spell.EARTH_WAVE, 1046),
	ENFEEBLE(Spell.ENFEEBLE, 1062),
	FIRE_WAVE(Spell.FIRE_WAVE, 1094),
	STORM_OF_ARMADYL(Spell.STORM_OF_ARMADYL, 1110),
	ENTANGLE(Spell.ENTANGLE, 1126),
	STAGGER(Spell.STAGGER, 1142),
	POLYPORE_STRIKE(Spell.POLYPORE_STRIKE, 2598),
	AIR_SURGE(Spell.AIR_SURGE, 1174),
	TELEPORT_BLOCK(Spell.TELEPORT_BLOCK, 1206),
	WATER_SURGE(Spell.WATER_SURGE, 1254),
	EARTH_SURGE(Spell.EARTH_SURGE, 1222),
	FIRE_SURGE(Spell.FIRE_SURGE, 1286),
	
	// Teleportation
	HOME_TELEPORT(Spell.HOME_TELEPORT, 2491),
	MOBILISING_ARMIES_TELEPORT(Spell.MOBILISING_ARMIES_TELEPORT, 310),
	VARROCK_TELEPORT(Spell.VARROCK_TELEPORT, 454),
	LUMBRIDGE_TELEPORT(Spell.LUMBRIDGE_TELEPORT, 502),
	FALADOR_TELEPORT(Spell.FALADOR_TELEPORT, 550),
	HOUSE_TELEPORT(Spell.HOUSE_TELEPORT, 582),
	CAMELOT_TELEPORT(Spell.CAMELOT_TELEPORT, 630),
	ARDOUGNE_TELEPORT(Spell.ARDOUGNE_TELEPORT, 726),
	WATCHTOWER_TELEPORT(Spell.WATCHTOWER_TELEPORT, 806),
	GOD_WARS_DUNGEON_TELEPORT(Spell.GOD_WARS_DUNGEON_TELEPORT, 2726),
	TROLLHEIM_TELEPORT(Spell.TROLLHIEM_TELEPORT, 918),
	APE_ATOLL_TELEPORT(Spell.APE_ATOLL_TELEPORT, 966),
	TELEOTHER_LUMBRIDGE(Spell.TELEOTHER_LUMBRIDGE, 1078),
	TELEOTHER_FALADOR(Spell.TELEOTHER_FALADAR, 1190),
	TELEOTHER_CAMELOT(Spell.TELEOTHER_CAMELOT, 1270),
	
	// Skilling
	ENCHANT_CROSSBOW_BOLT(Spell.ENCHANT_CROSSBOW_BOLT, 2502),
	ENCHANT_LEVEL_1_JEWELLERY(Spell.ENCHANT_LEVEL_1_JEWELLERY, 278),
	BONES_TO_BANANAS(Spell.BONES_TO_BANANAS, 358),
	LOW_LEVEL_ALCHEMY(Spell.LOW_LEVEL_ALCHEMY, 422),
	ENCHANT_LEVEL_2_JEWELLERY(Spell.ENCHANT_LEVEL_2_JEWELLERY, 470),
	TELEKINETIC_GRAB(Spell.TELEKINETIC_GRAB, 518),
	SUPERHEAT_ITEM(Spell.SUPERHEAT_ITEM, 614),
	ENCHANT_LEVEL_3_JEWELLERY(Spell.ENCHANT_LEVEL_3_JEWELLERY, 662),
	HIGH_LEVEL_ALCHEMY(Spell.HIGH_LEVEL_ALCHEMY, 758),
	CHARGE_WATER_ORB(Spell.CHARGE_WATER_ORB, 774),
	ENCHANT_LEVEL_4_JEWELLERY(Spell.ENCHANT_LEVEL_4_JEWELLERY, 790),
	BONES_TO_PEACHES(Spell.BONES_TO_PEACHES, 854),
	CHARGE_EARTH_ORB(Spell.CHARGE_EARTH_ORB, 838),
	CHARGE_FIRE_ORB(Spell.CHARGE_FIRE_ORB, 950),
	CHARGE_AIR_ORB(Spell.CHARGE_AIR_ORB, 998),
	ENCHANT_LEVEL_5_JEWELLERY(Spell.ENCHANT_LEVEL_5_JEWELLERY, 1030),
	ENCHANT_LEVEL_6_JEWELLERY(Spell.ENCHANT_LEVEL_6_JEWELLERY, 1238),
	
	
	//
	// Prayers
	//
	THICK_SKIN(Prayer.THICK_SKIN, 23),
	BURST_OF_STRENGTH(Prayer.BURST_OF_STRENGTH, 39),
	CLARITY_OF_THOUGHT(Prayer.CLARITY_OF_THOUGHT, 55),
	SHARP_EYE(Prayer.SHARP_EYE, 311),
	UNSTOPPABLE_FORCE(Prayer.UNSTOPPABLE_FORCE, 823),
	MYSTIC_WILL(Prayer.MYSTIC_WILL, 327),
	CHARGE(Prayer.CHARGE, 871),
	ROCK_SKIN(Prayer.ROCK_SKIN, THICK_SKIN.getSettingValue()),
	SUPERHUMAN_STRENGTH(Prayer.SUPERHUMAN_STRENGTH, BURST_OF_STRENGTH.settingValue),
	IMPROVED_REFLEXES(Prayer.IMPROVED_REFLEXES, CLARITY_OF_THOUGHT.settingValue),
	RAPID_RESTORE(Prayer.RAPID_RESTORE, 119),
	RAPID_HEAL(Prayer.RAPID_HEAL, 135),
	PROTECT_ITEM(Prayer.PROTECT_ITEM, 151),
	HAWK_EYE(Prayer.HAWK_EYE, SHARP_EYE.settingValue),
	UNRELENTING_FORCE(Prayer.UNRELENTING_FORCE, UNSTOPPABLE_FORCE.settingValue),
	MYSTIC_LORE(Prayer.MYSTIC_LORE, MYSTIC_WILL.settingValue),
	SUPER_CHARGE(Prayer.SUPER_CHARGE, CHARGE.settingValue),
	STEEL_SKIN(Prayer.STEEL_SKIN, THICK_SKIN.settingValue),
	ULTIMATE_STRENGTH(Prayer.ULTIMATE_STRENGTH, BURST_OF_STRENGTH.settingValue),
	INCREDIBLE_REFLEXES(Prayer.INCREDIBLE_REFLEXES, CLARITY_OF_THOUGHT.settingValue),
	PROTECT_FROM_SUMMONING(Prayer.PROTECT_FROM_SUMMONING, 407),
	PROTECT_FROM_MAGIC(Prayer.PROTECT_FROM_MAGIC, 215),
	PROTECT_FROM_MISSLES(Prayer.PROTECT_FROM_MISSLES, 231),
	PROTECT_FROM_MELEE(Prayer.PROTECT_FROM_MELEE, 247),
	EAGLE_EYE(Prayer.EAGLE_EYE, SHARP_EYE.settingValue),
	OVERPOWERING_FORCE(Prayer.OVERPOWERING_FORCE, UNSTOPPABLE_FORCE.settingValue),
	MYSTIC_MIGHT(Prayer.MYSTIC_MIGHT, MYSTIC_WILL.settingValue),
	OVERCHARGE(Prayer.OVERCHARGE, CHARGE.settingValue),
	RETRIBUTION(Prayer.RETRIBUTION, 263),
	REDEMPTION(Prayer.REDEMPTION, 279),
	SMITE(Prayer.SMITE, 295),
	CHIVALRY(Prayer.CHIVALRY, 423),
	RAPID_RENEWAL(Prayer.RAPID_RENEWAL, 455),
	PIETY(Prayer.PIETY, 439),
	RIGOUR(Prayer.RIGOUR, 487),
	AUGURY(Prayer.AUGURY, 471),
	;
	
	AbilitySkillType skillType;
	String name;
	boolean members;
	int levelReq;
	AbilityRequirement requirement;
	AbilityType abilityType;
	AbilitySubTab subTab;
	int settingValue;
	
	Spell spell;
	Prayer prayer;
	
	private Ability(AbilitySkillType skillType, String name, boolean members, int levelReq, AbilityRequirement requirement, AbilityType abilityType, AbilitySubTab subTab, int settingValue) {
		this.skillType = skillType;
		this.name = name;
		this.members = members;
		this.levelReq = levelReq;
		this.requirement = requirement;
		this.abilityType = abilityType;
		this.subTab = subTab;
		this.settingValue = settingValue;
		
		this.spell = null;
		this.prayer = null;
	}
	
	private Ability(Spell spell, int settingValue) {
		this.skillType = AbilitySkillType.MAGIC;
		this.name = spell.name;
		this.members = spell.members;
		this.levelReq = spell.levelReq;
		this.requirement = AbilityRequirement.NONE;
		this.abilityType = AbilityType.SPELL;
		this.subTab = spell.subInterfaceSubTab;
		this.settingValue = settingValue;
		
		this.spell = spell;
		this.prayer = null;
	}
	
	private Ability(Prayer prayer, int settingValue) {
		this.skillType = AbilitySkillType.PRAYER;
		this.name = prayer.name;
		this.members = prayer.members;
		this.levelReq = prayer.levelReq;
		this.requirement = AbilityRequirement.NONE;
		this.abilityType = AbilityType.PRAYER;
		this.subTab = null;
		this.settingValue = settingValue;
		
		this.spell = null;
		this.prayer = prayer;
	}
	
	public AbilitySkillType getSkillType() {
		return skillType;
	}
	
	public String getName() {
		return name;
	}
	
	public int getSettingValue() {
		return settingValue;
	}
	
	public AbilitySubTab getSubTab() {
		return subTab;
	}
	
	public Spell getSpell() {
		return spell;
	}
	
	public Prayer getPrayer() {
		return prayer;
	}
}
