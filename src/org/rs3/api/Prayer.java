package org.rs3.api;

import java.util.EnumSet;

import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.MagicAbilitiesMainTabWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.PrayersMainTabWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Time;

public enum Prayer {
	THICK_SKIN				("Thick Skin", 				false, 1, 0, 0),
	BURST_OF_STRENGTH		("Burst of Strength", 		false, 4, 1, 1),
	CLARITY_OF_THOUGHT		("Clarity of Thought", 		false, 7, 2, 2),
	SHARP_EYE				("Sharp Eye", 				false, 8, 3, 12),
	UNSTOPPABLE_FORCE		("Unstoppable Force", 		false, 8, 4, 14),
	MYSTIC_WILL				("Mystic Will", 			false, 9, 5, 13),
	CHARGE					("Charge", 					false, 9, 6, 15),
	ROCK_SKIN				("Rock Skin", 				false, 10, THICK_SKIN.tabComponentIndex, THICK_SKIN.bitsToShift),
	SUPERHUMAN_STRENGTH		("Superhuman Strength",		false, 13, BURST_OF_STRENGTH.tabComponentIndex, BURST_OF_STRENGTH.bitsToShift),
	IMPROVED_REFLEXES		("Improved Reflexes", 		false, 16, CLARITY_OF_THOUGHT.tabComponentIndex, CLARITY_OF_THOUGHT.bitsToShift),
	RAPID_RESTORE			("Rapid Restore", 			false, 19, 7, 3),
	RAPID_HEAL				("Rapid Heal", 				false, 22, 8, 4),
	PROTECT_ITEM			("Protect Item", 			false, 25, 9, 5),
	HAWK_EYE				("Hawk Eye", 				false, 26, SHARP_EYE.tabComponentIndex, SHARP_EYE.bitsToShift),
	UNRELENTING_FORCE		("Unrelenting Force", 		false, 26, UNSTOPPABLE_FORCE.tabComponentIndex, UNSTOPPABLE_FORCE.bitsToShift),
	MYSTIC_LORE				("Mystic Lore", 			false, 27, MYSTIC_WILL.tabComponentIndex, MYSTIC_WILL.bitsToShift),
	SUPER_CHARGE			("Super Charge", 			false, 27, CHARGE.tabComponentIndex, CHARGE.bitsToShift),
	STEEL_SKIN				("Steel Skin", 				false, 28, THICK_SKIN.tabComponentIndex, THICK_SKIN.bitsToShift),
	ULTIMATE_STRENGTH		("Ultimate Strength", 		false, 31, BURST_OF_STRENGTH.tabComponentIndex, BURST_OF_STRENGTH.bitsToShift),
	INCREDIBLE_REFLEXES		("Incredible Reflexes", 	false, 34, CLARITY_OF_THOUGHT.tabComponentIndex, CLARITY_OF_THOUGHT.bitsToShift),
	PROTECT_FROM_SUMMONING	("Protect from Summoning", 	true, 35, 10, 16),
	PROTECT_FROM_MAGIC		("Protect from Magic", 		false, 37, 11, 6),
	PROTECT_FROM_MISSLES	("Protect from Missles", 	false, 40, 12, 7),
	PROTECT_FROM_MELEE		("Protect from Melee", 		false, 43, 13, 8),
	EAGLE_EYE				("Eagle Eye", 				false, 44, SHARP_EYE.tabComponentIndex, SHARP_EYE.bitsToShift),
	OVERPOWERING_FORCE		("Overpowering Force", 		false, 44, UNSTOPPABLE_FORCE.tabComponentIndex, UNSTOPPABLE_FORCE.bitsToShift),
	MYSTIC_MIGHT			("Mystic Might", 			false, 45, MYSTIC_WILL.tabComponentIndex, MYSTIC_WILL.bitsToShift),
	OVERCHARGE				("Overcharge", 				false, 45, CHARGE.tabComponentIndex, CHARGE.bitsToShift),
	RETRIBUTION				("Retribution", 			true, 46, 14, 9),
	REDEMPTION				("Redemption", 				true, 49, 15, 10),
	SMITE					("Smite", 					true, 52, 16, 11),
	CHIVALRY				("Chivalry", 				true, 60, 17, 17),
	RAPID_RENEWAL			("Rapid Renewal", 			true, 65, 18, 19), // TODO: verify setting bitsToShift, likely 19, but unable to check
	PIETY					("Piety", 					true, 70, 19, 18),
	RIGOUR					("Rigour", 					true, 70, 20, 21),
	AUGURY					("Augury", 					true, 70, 21, 20),
	;
	
	String name;
	boolean members;
	int levelReq;
	int tabComponentIndex; // component index of prayer tab
	int bitsToShift; // prayer selected setting
	
	private Ability ability = null; // use getter - cannot reference ability in constructor because ability requires prayer, so it is set when retrieval is necessary
	
	private Prayer(String name, boolean members, int levelReq, int tabComponentIndex, int bitsToShift) {
		this.name = name;
		this.members = members;
		this.levelReq = levelReq;
		this.tabComponentIndex = tabComponentIndex;
		this.bitsToShift = bitsToShift;
	}
	
	public Ability getAbility() {
		if (ability != null)
			return ability;
		
		switch (this) {
		case AUGURY:
			ability = Ability.AUGURY;
			break;
		case BURST_OF_STRENGTH:
			ability = Ability.BURST_OF_STRENGTH;
			break;
		case CHARGE:
			ability = Ability.CHARGE;
			break;
		case CHIVALRY:
			ability = Ability.CHIVALRY;
			break;
		case CLARITY_OF_THOUGHT:
			ability = Ability.CLARITY_OF_THOUGHT;
			break;
		case EAGLE_EYE:
			ability = Ability.EAGLE_EYE;
			break;
		case HAWK_EYE:
			ability = Ability.HAWK_EYE;
			break;
		case IMPROVED_REFLEXES:
			ability = Ability.IMPROVED_REFLEXES;
			break;
		case INCREDIBLE_REFLEXES:
			ability = Ability.INCREDIBLE_REFLEXES;
			break;
		case MYSTIC_LORE:
			ability = Ability.MYSTIC_LORE;
			break;
		case MYSTIC_MIGHT:
			ability = Ability.MYSTIC_MIGHT;
			break;
		case MYSTIC_WILL:
			ability = Ability.MYSTIC_WILL;
			break;
		case OVERCHARGE:
			ability = Ability.OVERCHARGE;
			break;
		case OVERPOWERING_FORCE:
			ability = Ability.OVERPOWERING_FORCE;
			break;
		case PIETY:
			ability = Ability.PIETY;
			break;
		case PROTECT_FROM_MAGIC:
			ability = Ability.PROTECT_FROM_MAGIC;
			break;
		case PROTECT_FROM_MELEE:
			ability = Ability.PROTECT_FROM_MELEE;
			break;
		case PROTECT_FROM_MISSLES:
			ability = Ability.PROTECT_FROM_MISSLES;
			break;
		case PROTECT_FROM_SUMMONING:
			ability = Ability.PROTECT_FROM_SUMMONING;
			break;
		case PROTECT_ITEM:
			ability = Ability.PROTECT_ITEM;
			break;
		case RAPID_HEAL:
			ability = Ability.RAPID_HEAL;
			break;
		case RAPID_RENEWAL:
			ability = Ability.RAPID_RENEWAL;
			break;
		case RAPID_RESTORE:
			ability = Ability.RAPID_RESTORE;
			break;
		case REDEMPTION:
			ability = Ability.REDEMPTION;
			break;
		case RETRIBUTION:
			ability = Ability.RETRIBUTION;
			break;
		case RIGOUR:
			ability = Ability.RIGOUR;
			break;
		case ROCK_SKIN:
			ability = Ability.ROCK_SKIN;
			break;
		case SHARP_EYE:
			ability = Ability.SHARP_EYE;
			break;
		case SMITE:
			ability = Ability.SMITE;
			break;
		case STEEL_SKIN:
			ability = Ability.STEEL_SKIN;
			break;
		case SUPERHUMAN_STRENGTH:
			ability = Ability.SUPERHUMAN_STRENGTH;
			break;
		case SUPER_CHARGE:
			ability = Ability.SUPER_CHARGE;
			break;
		case THICK_SKIN:
			ability = Ability.THICK_SKIN;
			break;
		case ULTIMATE_STRENGTH:
			ability = Ability.ULTIMATE_STRENGTH;
			break;
		case UNRELENTING_FORCE:
			ability = Ability.UNRELENTING_FORCE;
			break;
		case UNSTOPPABLE_FORCE:
			ability = Ability.UNSTOPPABLE_FORCE;
			break;
		default:
			System.out.println("ERROR: Prayer.getAbility() - unimplemented ability!");
			break;
		}
		
		return ability;
	}
	
	public boolean isActivated() {
		int prayerSetting = Client.getSettingData().getSettings().get(3272, bitsToShift, 0x1); // mask for all: 3F FFFF
		return prayerSetting == 1;
	}
	
	public boolean activate() {
		return activate(false);
	}
	
	public boolean activate(boolean waitAfter) {
		if (isActivated())
			return true;
			
		// if on action bar, cast from there
		for (ActionSlot actionSlot : ActionBar.getCurrentSlots()) {
			if (actionSlot.getAbility().equals(getAbility())) {
				if (MainWidget.get.maximiseActionBar(true) && ActionBarWidget.get.isOpen()) {
					Component mainSlot = actionSlot.getMainSlot();
					if (mainSlot != null && mainSlot.isVisible()) {
						boolean result = mainSlot.interact("Activate", name);
						if (waitAfter)
							Time.sleepQuick();
						return result;
					}
				}
				
				return false;
			}
		}
		
		// TODO: test
		if (Ribbon.Tab.PRAYERS.open(true)) {
			Time.sleepQuick();
			
			Component[] prayers = PrayersMainTabWidget.get.getPrayers();
			if (prayers != null && tabComponentIndex < prayers.length) {
				Component button = prayers[tabComponentIndex];
				if (button != null) {
					boolean result = button.interact("Activate");
					if (waitAfter)
						Time.sleepQuick();
					return result;
				}
			}
		}
		
		return false;
	}
	
	public static boolean inUse() {
		int prayerSettings = Client.getSettingData().getSettings().get(3272, 0x3FFFFF); // mask for all: 3F FFFF
		return prayerSettings != 0;
	}
	
	public static EnumSet<Prayer> getActivated() {
		EnumSet<Prayer> set = EnumSet.noneOf(Prayer.class);
		for (Prayer prayer : values()) {
			if (prayer.isActivated()) {
				set.add(prayer);
			}
		}
		
		return set;
	}
}
