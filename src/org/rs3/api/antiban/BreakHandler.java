package org.rs3.api.antiban;

import org.rs3.api.wrappers.Client;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class BreakHandler {
	
	public static long takeBreak(boolean logout, long timeMs) {
		System.out.println("Taking break for " + Time.format(timeMs) + "...");
		
		if (logout) {
			Client.logout(true);
			Time.sleepQuick();
		}
		
		boolean[] percentages = new boolean[10];
		for (int i = 0; i < percentages.length; i++) {
			percentages[i] = false;
		}
		
		boolean exitDetected = false;
		
		Timer timer = new Timer(timeMs);
		while (timer.isRunning()) {
			if (ScriptHandler.currentScript == null || ScriptHandler.currentScript.stopInterrupt) {
				System.out.println("Exiting break; script stop detected.");
				exitDetected = true;
				break;
			}
			
			int percentDone = (int) Math.round((double)timer.getElapsed() / (double)timeMs * 100D);
			
			//System.out.println(percentDone);
			
			for (int i = 1; i < percentages.length; i++) {
				if (!percentages[i]) {
					if (percentDone >= i * 10) {
						System.out.println("Break: " + i * 10 + "% Remaining: " + timer.toRemainingString());
						percentages[i] = true;
					}
				}
			}
			
			Time.sleep(5000);
		}
		
		if (!exitDetected)
			System.out.println("Break: 100%");
		
		return timer.getElapsed();
		//return timeMs;
		
		//return 0;
	}
	
}
