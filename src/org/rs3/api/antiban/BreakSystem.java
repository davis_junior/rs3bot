package org.rs3.api.antiban;

import org.rs3.util.Random;
import org.rs3.util.Timer;

public class BreakSystem {
	
	//
	// 12 hour average break times
	// water: 54 minutes
	// food: 60 minutes
	// DISABLED - bathroom: 60 minutes
	// DISABLED - fatigue: 60 minutes
	//
	
	private static final int waterTimeBeforeBreak = 2400000; // 40 min.
	private static final int foodTimeBeforeBreak = 10800000; // 3 hr.
	private static final int bathroomTimeBeforeBreak = 3600000; // 1 hr.
	private static final int fatigueTimeBeforeBreak = 25200000; // 7 hr.
	
	// high level means more frequent breaks
	private int waterLevel;
	private int foodLevel;
	private int bathroomLevel;
	private int fatigueLevel;
	
	Timer waterTimer;
	Timer foodTimer;
	Timer bathroomTimer;
	Timer fatigueTimer;
	
	public int getWaterLevel() {
		return waterLevel;
	}
	
	public int getFoodLevel() {
		return foodLevel;
	}
	
	public int getBathroomLevel() {
		return bathroomLevel;
	}
	
	public int getFatigueLevel() {
		return fatigueLevel;
	}
	
	
	public void setFatigueLevel(int level) {
		fatigueLevel = level;
	}
	
	
	public BreakSystem() {
		resetLevels();
		resetTimers();
	}
	
	public void resetLevels() {
		waterLevel = Random.nextInt(0, 100);
		foodLevel = Random.nextInt(0, 100);
		bathroomLevel = Random.nextInt(0, 100);
		fatigueLevel = Random.nextInt(0, 100);
	}
	
	public void resetTimers() {
		resetWaterTimer();
		resetFoodTimer();
		resetBathroomTimer();
		resetFatigueTimer();
	}
	
	public void resetWaterTimer() {
		int waterMod = (100 - waterLevel) * 8000;
		waterTimer = new Timer(Random.nextInt(waterTimeBeforeBreak - waterMod, waterTimeBeforeBreak + waterMod));
	}
	
	public void resetFoodTimer() {
		int foodMod = (100 - foodLevel) * 25000;
		foodTimer = new Timer(Random.nextInt(foodTimeBeforeBreak - foodMod, foodTimeBeforeBreak + foodMod));
	}
	
	public void resetBathroomTimer() {
		int bathroomMod = (100 - bathroomLevel) * 12000;
		bathroomTimer = new Timer(Random.nextInt(bathroomTimeBeforeBreak - bathroomMod, bathroomTimeBeforeBreak + bathroomMod));
	}
	
	public void resetFatigueTimer() {
		int fatigueMod = (100 - fatigueLevel) * 20000;
		fatigueTimer = new Timer(Random.nextInt(fatigueTimeBeforeBreak - fatigueMod, fatigueTimeBeforeBreak + fatigueMod));
	}
	
	
	public long breakCheck() {
		if (!waterTimer.isRunning()) {
			int waterBreakTime = 180000; // 3 min.
			long timeMs = BreakHandler.takeBreak(Random.nextBoolean(), Random.nextInt(waterBreakTime - 60000, waterBreakTime + 60000));  // 2 min. to 4 min.
			resetWaterTimer();
			return timeMs;
		}
		
		if (!foodTimer.isRunning()) {
			int foodBreakTime = 900000; // 15 min.
			long timeMs = BreakHandler.takeBreak(true, Random.nextInt(foodBreakTime - 300000, foodBreakTime + 300000)); // 10 min. to 20 min.
			resetFoodTimer();
			return timeMs;
		}
		
		/*if (!bathroomTimer.isRunning()) {
			int bathroomBreakTime = 300000; // 5 min.
			long timeMs = BreakHandler.takeBreak(Random.nextBoolean(), Random.nextInt(bathroomBreakTime - 120000, bathroomBreakTime + 120000));  // 3 min. to 7 min.
			resetBathroomTimer();
			return timeMs;
		}*/
		
		/*if (!fatigueTimer.isRunning()) {
			int fatigueBreakTime = 3600000; // 1 hr.
			long timeMs = BreakHandler.takeBreak(true, Random.nextInt(fatigueBreakTime - 1800000, fatigueBreakTime + 1800000));  // 30 min. to 1.5 hr.
			resetFatigueTimer();
			return timeMs;
		}*/
		
		return 0;
	}
}
