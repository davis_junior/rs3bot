package org.rs3.api.antiban;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import org.rs3.api.Camera;
import org.rs3.api.GroundObjects;
import org.rs3.api.input.HardwareInput;
import org.rs3.api.input.Input;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.eventqueue.EventNazi;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Antiban {
	
	private static Timer antibanTimer = new Timer(getNextAntibanTime());
	
	public static void resetTimer() {
		antibanTimer.setEndIn(getNextAntibanTime());
	}
	
	private static int getNextAntibanTime() {
		return Random.nextInt(0.75F, 750, 5000, 5000, 15000);
	}
	
	public enum AntibanType {
		RandomMoveMouse, RandomMoveMouseNear, RandomCameraZoom, RandomRightClickItem, RandomRightClickRSAnimable, RandomLongCameraAngle, RandomCameraAngle, RandomCameraPitch, RandomCamera, RandomWait;
		
		public boolean doAction() {
			//System.out.println("Anitban: " + this.name());
			
			switch (this) {
			case RandomMoveMouse:
				return randomMoveMouse();
			case RandomMoveMouseNear:
				return randomMoveMouseNear();
			case RandomCameraZoom:
				return randomCameraZoom();
			case RandomRightClickItem:
				return randomRightClickItem();
			case RandomRightClickRSAnimable:
				return randomRightClickRSAnimable();
			case RandomLongCameraAngle:
				return randomLongCameraAngle();
			case RandomCameraAngle:
				return randomCameraAngle();
			case RandomCameraPitch:
				return randomCameraPitch();
			case RandomCamera:
				return randomCamera();
			case RandomWait:
				return randomWait();
			default:
				System.out.println("Error: AntibanType doAction() not implemented!");
				return false;
			}
		}
		
		public static EnumSet<AntibanType> getAll() {
			return EnumSet.of(AntibanType.RandomMoveMouse, AntibanType.RandomMoveMouseNear, AntibanType.RandomRightClickItem, AntibanType.RandomRightClickRSAnimable, AntibanType.RandomLongCameraAngle, AntibanType.RandomCameraAngle, AntibanType.RandomCameraPitch, AntibanType.RandomCamera, AntibanType.RandomCameraZoom, AntibanType.RandomWait);
		}
		
		public static AntibanType getRandom_noBias() {
			return getRandom_noBias(getAll());
		}
		
		public static AntibanType getRandom_noBias(EnumSet<AntibanType> antibanTypes) {
			AntibanType[] array = antibanTypes.toArray(new AntibanType[0]);
			AntibanType randAntiban = array[Random.nextInt(0, array.length)];
			
			return randAntiban;
		}
		
		public static AntibanType getRandom_biased() {
			int rand = Random.nextInt(0, 100);
			
			if (rand < 99) {
				int rand2 = Random.nextInt(0, 100);
				
				if (rand2 < 75)
					return getRandom_noBias(EnumSet.of(AntibanType.RandomMoveMouseNear, AntibanType.RandomCameraAngle, AntibanType.RandomCameraPitch, AntibanType.RandomCamera));
				else if (rand2 < 99)
					return getRandom_noBias(EnumSet.of(AntibanType.RandomMoveMouse));
				else {
					if (Random.nextBoolean())
						return getRandom_noBias(EnumSet.of(AntibanType.RandomLongCameraAngle, AntibanType.RandomCameraZoom));
					else
						return getRandom_noBias(EnumSet.of(AntibanType.RandomWait));
				}
				
				//return getRandom_noBias(EnumSet.of(AntibanType.RandomMoveMouse, AntibanType.RandomCameraAngle, AntibanType.RandomCameraPitch, AntibanType.RandomCamera, AntibanType.RandomCameraZoom));
			} else
				return getRandom_noBias(EnumSet.of(AntibanType.RandomRightClickItem, AntibanType.RandomRightClickRSAnimable));
		}
	}
	
	
	public static boolean doAntiban() {
		if (!antibanTimer.isRunning()) {
			resetTimer();
			return randomAntiban(100);
		}
		
		return false;
	}
	
	// doesn't check timer
	public static boolean randomAntiban(int percentChance) {
		return randomAntiban(percentChance, AntibanType.getAll());
	}
	
	// TODO: antibanTypes does not work because getRandom_biased() picks random out of all
	// percentChance should be 0-100
	public static boolean randomAntiban(int percentChance, EnumSet<AntibanType> antibanTypes) {
		
		//if (true) return false;
		
		if (!Client.isLoggedIn())
			return false;
		
		int randPercent = percentChance != 100 ? Random.nextInt(0, 100) : 100;
		
		if (randPercent == 100 || randPercent <= percentChance) {
			if (antibanTypes == null)
				antibanTypes = AntibanType.getAll();
			
			//AntibanType randAntiban = AntibanType.getRandom_noBias(antibanTypes);
			AntibanType randAntiban = AntibanType.getRandom_biased();
			return randAntiban.doAction();
			
			//boolean result = randAntiban.doAction();
			//return result;
		}
		
		return false;
	}
	
	
	public static Point getRandomClientPoint() {
		int randX = Random.nextInt(0, Client.getCanvasWidth() - 1);
		int randY = Random.nextInt(0, Client.getCanvasHeight() - 1);
		
		return new Point(randX, randY);
	}
	
	// moves mouse somewhere random in the client bounds
	public static boolean randomMoveMouse() {
		Point randPoint = getRandomClientPoint();
		return Mouse.mouse(randPoint.x, randPoint.y) != null;
	}
	
	public static boolean randomMoveMouseNear() {
		Point randPoint = Mouse.getClientPos();
		randPoint.x = Random.nextInt(randPoint.x - 150, randPoint.x + 150);
		randPoint.y = Random.nextInt(randPoint.y - 150, randPoint.y + 150);
		
		if (randPoint.x < 0)
			randPoint.x = 0;
		if (randPoint.y < 0)
			randPoint.y = 0;
		
		if (randPoint.x >= Client.getCanvasWidth())
			randPoint.x = Client.getCanvasWidth() - 1;
		if (randPoint.y >= Client.getCanvasHeight())
			randPoint.y = Client.getCanvasHeight() - 1;
		
		return Mouse.mouse(randPoint.x, randPoint.y) != null;
	}
	
	/**
	 * Moves mouse to on screen area, skipping main interfaces, tabs, etc.
	 * 
	 * @return
	 */
	public static boolean randomMoveMouseOnScreen() {
		Point randPoint = MainWidget.get.getRandomOnScreenPoint();
		return Mouse.mouse(randPoint.x, randPoint.y) != null;
	}
	
	/**
	 * Moves mouse to on screen area and scrolls, skipping main interfaces, tabs, etc.
	 * 
	 * @return
	 */
	public static boolean randomCameraZoom() {
		int zoomPercent = (Random.nextInt(0, 100) < 75 ? Random.nextInt(0, 40) : Random.nextInt(0, 100)) ;
		return Camera.setZoomPercent(zoomPercent);
	}
	
	// unused in enumset
	public static boolean randomClick(int button) {
		Point randPoint = getRandomClientPoint();
		
		if (Mouse.mouse(randPoint.x, randPoint.y, button) != null) {
			Time.sleepVeryQuick();
			return randomMoveMouse();
		} else
			return false;
	}
	
	// right clicks inventory item and selects examine or cancel or moves mouse to cancel menu
	public static boolean randomRightClickItem() {
		//if (Inventory.countAllItems() <= 0)
		//	return false;
		
		Component[] components = InventoryWidget.get.getItems();
		
		if (components != null) {
			List<Integer> indices = new ArrayList<>();
			
			for (int i = 0; i < components.length; i++) {
				indices.add(i);
			}
			
			Collections.shuffle(indices);
			
			for (int i = 0; i < indices.size(); i++) {
				Component randItem = components[i];
				
				if (randItem != null && randItem.getId() != -1) {
					String randomAction;
					if (Random.nextInt(0, 100) < 75)
						randomAction = "Examine";
					else {
						if (Random.nextBoolean())
							randomAction = "Cancel";
						else {
							if (randItem.click(Mouse.RIGHT)) {
								Time.sleepVeryQuick();
								return randomMoveMouse();
							} else
								return false;
						}
					}
					
					return randItem.interact(randomAction);
				}
			}
		}
		
		return false;
	}
	
	// right clicks inventory item and selects examine or cancel or moves mouse to cancel menu
	public static boolean randomRightClickRSAnimable() {
		//RSAnimable[] animables = Objects.getAllAnimableObjectsOnScreen();
		RSAnimable randAnimable = (RSAnimable) GroundObjects.getRandom(GroundObject.Type.Animable, 15, true);
		
		if (randAnimable != null) {
			AnimableObject ao = null;
			AnimatedAnimableObject aao = null;
			
			if (randAnimable instanceof AnimableObject)
				ao = (AnimableObject) randAnimable;
			else if (randAnimable instanceof AnimatedAnimableObject)
				aao = (AnimatedAnimableObject) randAnimable;
			
			Model model = null;
			
			if (ao != null) {
				//Paint.updatePaintObject(ao);
				
				model = ao.getCachedAnimatedModel();
				
				if (model == null)
					model = ao.getCachedModel();
			} else if (aao != null) {
				//Paint.updatePaintObject(aao);
				
				model = aao.getAnimatedObject().getCachedAnimatedModel();
				
				if (model == null)
					model = aao.getAnimatedObject().getCachedModel();
			}
			
			if (model == null)
				return false;
			
			String randomAction;
			if (Random.nextInt(0, 100) < 75)
				randomAction = "Examine";
			else {
				if (Random.nextBoolean())
					randomAction = "Cancel";
				else {
					if (model.click(Mouse.RIGHT, randAnimable.getLocation(), randAnimable.getPlane())) {
						Time.sleepVeryQuick();
						return randomMoveMouse();
					} else
						return false;
				}
			}
			
			return model.interact(randomAction, randAnimable.getLocation(), randAnimable.getPlane());
		}
		
		return false;
	}
	
	public static boolean randomLongCameraAngle() {
		int keyCode = Random.nextBoolean() ? KeyEvent.VK_LEFT : KeyEvent.VK_RIGHT;
		
		Keyboard.pressKey(keyCode);
		Time.sleep(500, 5000);
		Keyboard.releaseKey(keyCode);
		
		return true;
	}
	
	public static boolean randomCameraAngle() {
		Camera.setAngle(Random.nextInt(0, 360));
		return true;
	}
	
	public static boolean randomCameraPitch() {
		return Camera.setPitch(Random.nextInt(0, 100));
	}
	
	public static boolean randomCamera() {
		boolean result = false;
		if (Random.nextBoolean()) {
			if (randomCameraAngle()) {
				Time.sleepVeryQuick();
				result = randomCameraPitch();
			}
		} else {
			if (randomCameraPitch()) {
				Time.sleepVeryQuick();
				result = randomCameraAngle();
			}
		}
		
		return result;
	}
	
	
	public static void randomMouseSpeed() {
		if (Input.useHardWareInput()) {
			//HardwareInput.setMouseSpeed(Random.nextInt(0.90F, 15, 25, 10, 30));
			HardwareInput.setMouseSpeed(Random.nextInt(0.90F, 30, 50, 15, 50));
		} else {
			//EventNazi.setMouseSpeed(Random.nextInt(0.90F, 15, 25, 10, 30));
			EventNazi.setMouseSpeed(Random.nextInt(0.90F, 30, 50, 15, 50));
		}
	}
	
	public static boolean randomWait() {
		System.out.println("random wait...");
		
		Time.sleep(0.75F, 2000, 4000, 4000, 8000);
		return true;
	}
}
