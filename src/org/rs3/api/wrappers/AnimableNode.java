package org.rs3.api.wrappers;

import org.rs3.accessors.I_AnimableNode;

public class AnimableNode extends AccessorWrapper {
	
	protected AnimableNode(Object obj) {
		super(obj);
	}
	
	public static AnimableNode create(Object obj) {
		return create(AnimableNode.class, obj);
	}
	
	
	public AnimableNode getNext() {
		return create(I_AnimableNode.getNext(obj));
	}
	
	public RSAnimable getAnimable() {
		Object animableObj = I_AnimableNode.getAnimable(obj);
		
		if (animableObj != null) {
			if (animableObj.getClass().getName().equals(data.AnimableObject.get.getInternalName()))
				return AnimableObject.create(animableObj);
			else if (animableObj.getClass().getName().equals(data.AnimatedAnimableObject.get.getInternalName()))
				return AnimatedAnimableObject.create(animableObj);
			else
				return RSAnimable.create(animableObj);
		}
		
		return null;
	}
}
