package org.rs3.api.wrappers;

import org.rs3.accessors.I_Widget;


public class Widget extends AccessorWrapper {
	
	protected Widget(Object obj) {
		super(obj);
	}
	
	public static Widget create(Object obj) {
		return create(Widget.class, obj);
	}
	
	
	public Array1D<Component> getComponents() {
		return Array1D.create(Component.class, I_Widget.getComponents(obj));
	}
	
	/**
	 * Note: This method is very inefficient; it wraps all of the component array.
	 * 
	 * @param index
	 * @return
	 */
	public boolean validate(int index) {
		boolean[] validArray = Client.getValidWidgetArray();
		
		if (validArray != null && index >= 0  && index < validArray.length && validArray[index]) {
			Array1D<Widget> inters = Client.getWidgetCache();
			
			if (index < inters.getLength() && inters.get(index) != null) {
				Component[] children = getComponents().getAll(false);
				int count = 0;
				
				for (Component child : children) {
					if (child.getBoundsArrayIndex() == -1)
						++count;
				}
				
				return count != children.length;
			}
		}
		
		return false;
	}
}
