package org.rs3.api.wrappers;

import org.rs3.accessors.I_Reference;


public class Reference extends NodeSub {
	
	protected Reference(Object obj) {
		super(obj);
	}
	
	public static Reference create(Object obj) {
		return create(Reference.class, obj);
	}
	
	
	public int getIndex() {
		return I_Reference.getIndex(obj);
	}
}
