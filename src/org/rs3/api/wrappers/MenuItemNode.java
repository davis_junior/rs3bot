package org.rs3.api.wrappers;

import org.rs3.accessors.I_MenuItemNode;


public class MenuItemNode extends NodeSub {
	
	protected MenuItemNode(Object obj) {
		super(obj);
	}
	
	public static MenuItemNode create(Object obj) {
		return create(MenuItemNode.class, obj);
	}
	
	
	public String getOption() {
		return I_MenuItemNode.getOption(obj);
	}
	
	public String getAction() {
		return I_MenuItemNode.getAction(obj);
	}
}
