package org.rs3.api.wrappers;

import org.rs3.accessors.I_AbstractGraphicsSetting;

public abstract class AbstractGraphicsSetting extends AccessorWrapper {
	
	protected AbstractGraphicsSetting(Object obj) {
		super(obj);
	}
	
	public static AbstractGraphicsSetting create(Object obj) {
		return create(AbstractGraphicsSetting.class, obj);
	}
	
	
	public int getValue() {
		return I_AbstractGraphicsSetting.getValue(obj);
	}
}
