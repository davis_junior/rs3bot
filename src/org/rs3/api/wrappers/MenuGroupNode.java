package org.rs3.api.wrappers;

import org.rs3.accessors.I_MenuGroupNode;


public class MenuGroupNode extends NodeSub {
	
	protected MenuGroupNode(Object obj) {
		super(obj);
	}
	
	public static MenuGroupNode create(Object obj) {
		return create(MenuGroupNode.class, obj);
	}
	
	
	public NodeSubQueue getItems() {
		return NodeSubQueue.create(I_MenuGroupNode.getItems(obj));
	}
	
	public String getOptions() {
		return I_MenuGroupNode.getOptions(obj);
	}
}
