package org.rs3.api.wrappers;

import org.rs3.accessors.I_ObjectComposite;

public class ObjectComposite extends AccessorWrapper {
	
	protected ObjectComposite(Object obj) {
		super(obj);
	}
	
	public static ObjectComposite create(Object obj) {
		return create(ObjectComposite.class, obj);
	}
	
	
	/*public Object getModel() {
		return iAccessor.getModel();
	}*/
	
	public Model getModel() {
		Render render = Client.getRender();
		
		if (render instanceof SDRender)
			return SDModel.create(I_ObjectComposite.getModel(obj), 0, 0);
		else if (render instanceof OpenGLRender)
			return OpenGLModel.create(I_ObjectComposite.getModel(obj), 0, 0);
		else
			return Model.create(I_ObjectComposite.getModel(obj), 0, 0);
	}
}
