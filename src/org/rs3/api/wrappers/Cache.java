package org.rs3.api.wrappers;

import org.rs3.accessors.I_Cache;


public class Cache extends AccessorWrapper {
	
	protected Cache(Object obj) {
		super(obj);
	}
	
	public static Cache create(Object obj) {
		return create(Cache.class, obj);
	}
	
	
	public HashTable getHashTable() {
		return HashTable.create(I_Cache.getTable(obj));
	}
}
