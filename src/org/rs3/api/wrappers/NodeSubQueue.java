package org.rs3.api.wrappers;

import org.rs3.accessors.I_NodeSubQueue;


public class NodeSubQueue extends AccessorWrapper {
	
	private NodeSub current;
	
	protected NodeSubQueue(Object obj) {
		super(obj);
	}
	
	public static NodeSubQueue create(Object obj) {
		return create(NodeSubQueue.class, obj);
	}
	
	
	private NodeSub _getTail() {
		return NodeSub.create(I_NodeSubQueue.getTail(obj));
	}
	
	
	public int size() {
		try {
			int size = 0;
			NodeSub node = _getTail().getPreviousSub();
			
			//while (node != _getTail()) {
			while (node.getObject() != _getTail().getObject()) {
				node = node.getPreviousSub();
				size++;
			}
			
			return size;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return -1;
	}

	public NodeSub getHead() {
		try {
			NodeSub node = _getTail().getNextSub();
			
			//if (node == _getTail()) {
			if (node.getObject() == _getTail().getObject()) {
				current = null;
				return null;
			}
			
			current = node.getNextSub();
			
			return node;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return null;
	}

	public NodeSub getNext() {
		try {
			NodeSub node = current;
			
			//if (node == _getTail()) {
			if (node.getObject() == _getTail().getObject()) {
				current = null;
				return null;
			}
			
			current = node.getNextSub();
			
			return node;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return null;
	}
}
