package org.rs3.api.wrappers;

import org.rs3.accessors.I_NpcDefinition;


public class NpcDefinition extends AccessorWrapper {
	
	protected NpcDefinition(Object obj) {
		super(obj);
	}
	
	public static NpcDefinition create(Object obj) {
		return create(NpcDefinition.class, obj);
	}
	
	
	public String[] getActions() {
		return I_NpcDefinition.getActions(obj);
	}
	
	public int getId() {
		return I_NpcDefinition.getId(obj);
	}
	
	public String getName() {
		return I_NpcDefinition.getName(obj);
	}
	
	/*public HashTable getNodeTable() {
		return HashTable.create(I_NpcDefinition.getNodeTable(obj));
	}*/
}
