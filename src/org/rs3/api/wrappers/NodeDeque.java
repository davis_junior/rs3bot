package org.rs3.api.wrappers;

import org.rs3.accessors.I_NodeDeque;


public class NodeDeque extends AccessorWrapper {
	
	private Node current;
	
	protected NodeDeque(Object obj) {
		super(obj);
	}
	
	public static NodeDeque create(Object obj) {
		return create(NodeDeque.class, obj);
	}
	
	
	private Node _getTail() {
		return Node.create(I_NodeDeque.getTail(obj));
	}
	
	
	public int size() {
		try {
			int size = 0;
			Node node = _getTail().getPrevious();
			
			//while (node != _getTail()) {
			while (node.getObject() != _getTail().getObject()) {
				node = node.getPrevious();
				size++;
			}
			
			return size;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return -1;
	}

	public Node getHead() {
		try {
			final Node node = _getTail().getNext();
			
			//if (node == _getTail()) {
			if (node.getObject() == _getTail().getObject()) {
				current = null;
				return null;
			}
			
			current = node.getNext();
			
			return node;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return null;
	}

	public Node getTail() {
		try {
			final Node node = _getTail().getPrevious();
			
			//if (node == _getTail()) {
			if (node.getObject() == _getTail().getObject()) {
				current = null;
				return null;
			}
			
			current = node.getPrevious();
			
			return node;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return null;
	}

	public Node getNext() {
		try {
			Node node = current;
			
			//if (node == _getTail()) {
			if (node.getObject() == _getTail().getObject()) {
				current = null;
				return null;
			}
			
			current = node.getNext();
			
			return node;
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return null;
	}
}
