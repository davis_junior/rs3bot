package org.rs3.api.wrappers;

import org.rs3.accessors.I_SoftReference;


public class SoftReference extends Reference {
	
	protected SoftReference(Object obj) {
		super(obj);
	}
	
	public static SoftReference create(Object obj) {
		return create(SoftReference.class, obj);
	}
	

	public java.lang.ref.SoftReference<?> getSoftReference() {
		return I_SoftReference.getSoftReference(obj);
	}
}
