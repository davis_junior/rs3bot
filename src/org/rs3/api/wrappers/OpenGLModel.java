package org.rs3.api.wrappers;

import org.rs3.accessors.I_OpenGLModel;
import org.rs3.api.Calculations;


public class OpenGLModel extends Model {

	int projectionLength;
	int[] projectionChecks;
	short[] projectionIndices;

	protected OpenGLModel(Object obj) {
		super(obj);

		this.type = TYPE_OPENGLMODEL;
	}

	public static OpenGLModel create(Object obj, int orientationMod, int heightOff) {
		OpenGLModel model = create(OpenGLModel.class, obj);

		if (model == null || model.xPoints == null || model.yPoints == null || model.zPoints == null || model.projectionChecks == null)
			return null;
		else {
			model.orientationMod = orientationMod;
			model.heightOff = heightOff;
			return model;
		}
	}

	@Override
	void update() {
		super.update();

		projectionLength = getProjectionLength();
		int[] projectionChecks = getProjectionChecks();
		short[] projectionIndices = getProjectionIndices();

		if (projectionChecks == null || projectionIndices == null)
			return;

		this.projectionChecks = projectionChecks.clone();
		this.projectionIndices = projectionIndices.clone();
	}


	@Override
	public short[] getIndices1() {
		return I_OpenGLModel.getIndices1(obj);
	}

	@Override
	public short[] getIndices2() {
		return I_OpenGLModel.getIndices2(obj);
	}

	@Override
	public short[] getIndices3() {
		return I_OpenGLModel.getIndices3(obj);
	}

	@Override
	public int[] getXPoints() {
		return I_OpenGLModel.getXPoints(obj);
	}

	@Override
	public int[] getYPoints() {
		return I_OpenGLModel.getYPoints(obj);
	}

	@Override
	public int[] getZPoints() {
		return I_OpenGLModel.getZPoints(obj);
	}

	@Override
	public int getNumFaces() {
		return I_OpenGLModel.getNumFaces(obj);
	}

	@Override
	public int getNumVertices() {
		return I_OpenGLModel.getNumVertices(obj);
	}

	public int getProjectionLength() {
		return I_OpenGLModel.getProjectionLength(obj);
	}

	public int[] getProjectionChecks() {
		return I_OpenGLModel.getProjectionChecks(obj);
	}

	public short[] getProjectionIndices() {
		return I_OpenGLModel.getProjectionIndices(obj);
	}


	@Override
	public int[][] projectVertices(double locX, double locY, int plane) {
		Render render = Client.getRender();

		//Viewport viewport = render.getClientViewport();

		float[] data = null;

		float[] callBackFloats = Viewport.getCallBackFloats();
		if (callBackFloats == null)
			data = render.getClientViewport().getFloats().clone();
		else
			data = callBackFloats.clone();

		if (orientationMod != 0) // just for efficiency
			orientationMod = (orientationMod + 360) % 360; // modified for SIN/COS table usage (+ 360 for the known case of negative values)


		int[][] screen = new int[this.projectionLength][2];

		float checkOff = data[14];
		float xOff = data[12];
		float yOff = data[13];
		float zOff = data[15];
		float checkX = data[2];
		float checkY = data[6];
		float checkZ = data[10];
		float xX = data[0];
		float xY = data[4];
		float xZ = data[8];
		float yX = data[1];
		float yY = data[5];
		float yZ = data[9];
		float zX = data[3];
		float zY = data[7];
		float zZ = data[11];

		float render_absoluteX = render.getAbsoluteX();
		float render_absoluteY = render.getAbsoluteY();
		float render_xMultiplier = render.getXMultiplier();
		float render_yMultiplier = render.getYMultiplier();

		int height = Calculations.tileHeight((int)locX, (int)locY, plane) + this.heightOff;

		for (int index = 0; index < this.numVertices; index++) {
			/*int vertexX = (int) (this.xPoints[index] + locX);
			int vertexY = this.yPoints[index] + height;
			int vertexZ = (int) (this.zPoints[index] + locY);*/

			final int x = this.xPoints[index];
			final int y = this.yPoints[index];
			final int z = this.zPoints[index];

			/*double rads = (0.017453292519943295 * rotateDegs); // convert rotateDegs from degrees to radians

			int vertexX = (int) (x * Math.cos(rads) + z * Math.sin(rads));
			int vertexY = y;
			int vertexZ = (int) (-x * Math.sin(rads) + z * Math.cos(rads));*/

			int vertexX = (int) (x * D2R_COS_TABLE[orientationMod] + z * D2R_SIN_TABLE[orientationMod]);
			int vertexY = y;
			int vertexZ = (int) (-x * D2R_SIN_TABLE[orientationMod] + z * D2R_COS_TABLE[orientationMod]);

			vertexX += locX;
			vertexY += height;
			vertexZ += locY;

			float _check = (checkOff + (checkX * vertexX + checkY * vertexY + checkZ * vertexZ));
			float _z = (zOff + (zX * vertexX + zY * vertexY + zZ * vertexZ));

			if (_check >= -_z) {
				float _x = (xOff + (xX * vertexX + xY * vertexY + xZ * vertexZ));
				float _y = (yOff + (yX * vertexX + yY * vertexY + yZ * vertexZ));

				for (int i = this.projectionChecks[index]; i < this.projectionChecks[index + 1]; i++) {
					if (this.projectionIndices[i] == 0)
						break;

					//float fx = Math.round(render_absoluteX + render_xMultiplier * _x / _z);
					//float fy = Math.round(render_absoluteY + render_yMultiplier * _y / _z);

					int newIndex = (this.projectionIndices[i] & 0xFFFF) - 1;

					if (_x >= -_z && _x <= _z && _y >= -_z && _y <= _z) {
						//screen[newIndex][0] = (int)(render_absoluteX + (render_xMultiplier - 35) * _x / _z); // TODO: the multiplier hack might be because of my dpi settings
						//screen[newIndex][1] = (int)(render_absoluteY + (render_yMultiplier - 35) * _y / _z);
						screen[newIndex][0] = (int)(render_absoluteX + render_xMultiplier * _x / _z);
						screen[newIndex][1] = (int)(render_absoluteY + render_yMultiplier * _y / _z);
					} else {
						screen[newIndex][0] = -999999;
						//screen[newIndex][1] = -1; // this is not set in the rs client
					}
				}
			} else {
				for (int i = this.projectionChecks[index]; i < this.projectionChecks[index + 1]; i++) {
					if (this.projectionIndices[i] == 0)
						break;

					int newIndex = (this.projectionIndices[i] & 0xFFFF) - 1;

					screen[newIndex][0] = -999999;
					//screen[newIndex][1] = -1; // this is not set in the rs client
				}
			}
		}

		return screen;
	}
}
