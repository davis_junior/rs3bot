package org.rs3.api.wrappers;

import java.awt.Graphics;

import javax.media.opengl.GL2;

import org.rs3.accessors.I_BoundaryObject;
import org.rs3.api.Nodes;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.callbacks.capture.ModelCapture;

public class BoundaryObject extends AbstractBoundary implements GroundObject {
	
	protected BoundaryObject(Object obj) {
		super(obj);
	}
	
	public static BoundaryObject create(Object obj) {
		return create(BoundaryObject.class, obj);
	}
	
	
	@Override
	public int getId() {
		return I_BoundaryObject.getId(obj);
	}
	
	public Model getModel_field() {
		Render render = Client.getRender();
		
		if (render instanceof SDRender)
			return SDModel.create(I_BoundaryObject.getModel(obj), 0, 0);
		else if (render instanceof OpenGLRender)
			return OpenGLModel.create(I_BoundaryObject.getModel(obj), 0, 0);
		else
			return Model.create(I_BoundaryObject.getModel(obj), 0, 0);
	}
	
	public ObjectDefLoader getDefinitionLoader() {
		return ObjectDefLoader.create(I_BoundaryObject.getDefinitionLoader(obj));
	}
	
	
	@Override
	public Tile getTile() {
		return getData().getCenterLocation().getTile(true);
	}
	
	@Override
	public ObjectDefinition getDefinition() {
		try {
			Node ref = Nodes.lookup(getDefinitionLoader().getDefinitionCache().getHashTable(), getId());
			
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null)
					return ObjectDefinition.create(def);
			
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null)
					return ObjectDefinition.create(def);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public byte getAnimationId() {
		return I_BoundaryObject.getAnimationId(obj);
	}
	
	public byte getFace() {
		return I_BoundaryObject.getFace(obj);
	}
	
	
	// sometimes the object must be hovered first, the composite is most likely not in the cache, most likely need injection for opengl because of this
	// sometimes returns null, i think sometimes it is not in the cache
	public ObjectComposite getComposite() {
		try {
			ObjectDefinition definition = getDefinition();
			
			if (definition == null)
				return null;
			
			long face = getFace();
			long animationId = getAnimationId();
			
			// from rs client
			// (animationId != 11 * -1226570173) ? animationId : (-1226570173 * 10)
			// (-1226570173 * 11 == animationId) ? (4 + face) : face
			
			// TODO: get multiplier?
			if (animationId == 11L * -1226570173L) {
				animationId = -1226570173L * 10L;
				face += 4;
			}
			
			long id = (definition.getId() << 10) + (animationId << 3) + face;
			id |= Client.getRender().getCacheIndex() << 29;
			
			Node ref = Nodes.lookup(definition.getCacheLoader().getCompositeCache().getHashTable(), id);
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null)
					return ObjectComposite.create(def);
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null)
					return ObjectComposite.create(def);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Model getCachedAnimatedModel() {
		ObjectComposite comp = getComposite();
		
		if (comp == null)
			return null;
		
		return comp.getModel();
	}
	
	public Model getCachedModel() {
		try {
			ObjectDefinition definition = getDefinition();
			
			if (definition == null)
				return null;
			
			byte[] byteArray = definition.getModelCache2byteArray().clone();
			int[][] intArray2D = definition.getModelCache2intArray2D().clone();
			
			byte animationId = getAnimationId();
			
			if (animationId == 11)
				animationId = 10;
			
			int index = -1;
			for (int i = 0; i < byteArray.length; i++) {
				if (byteArray[i] == animationId) {
					index = i;
					break;
				}
			}
			
			if (index == -1)
				return null;
			
			int[] intArray = intArray2D[index];
			int length = intArray.length;
			
			if (length <= 0)
				return null;
			
			long id = Client.getRender().getCacheIndex();
			
			for (int i = 0; i < length; i++) {
				id = id * 67783L + intArray[i];
			}
			
			Node ref = Nodes.lookup(definition.getCacheLoader().getModelCache().getHashTable(), id);
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, 0, 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, 0, 0);
					} else {
						return Model.create(def, 0, 0);
					}
				}
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, 0, 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, 0, 0);
					} else {
						return Model.create(def, 0, 0);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	//public Model curModel = null;
	
	/*
	 * Uses model callback to grab model. Adds itself to the BoundaryObject model hashset and waits for a non-null model.
	 * @return
	 */
	/*public Model captureModel() {
		this.curModel = null;
		ModelCapture.boundaryObjects.add(this);
		
		Timer timer = new Timer(1000);
		while (timer.isRunning()) {
			if (this.curModel != null)
				break;
			
			Time.sleep(50);
		}
		
		ModelCapture.boundaryObjects.remove(this);
		
		return this.curModel;
	}*/
	
	@Override
	public Model getModel() {
		Render render = Client.getRender();
		
		if (render instanceof SDRender)
			return getModel_field();
		else if (render instanceof OpenGLRender) {
			Model model = ModelCapture.boundaryObjectModels.get(obj);
			//if (model != null)
			//	model.rotateDegs = getFace();
			
			return model;
		} else
			return getCachedAnimatedModel();
	}
	
	
	@Override
	public void draw(Graphics g) {
		Model model = getModel();
		
		if (model != null)
			model.draw(g, getLocation(), getPlane());
	}
	
	@Override
	public void render(GL2 gl2) {
		Model model = getModel();
		
		if (model != null)
			model.render(gl2, getLocation(), getPlane());
	}
	
	public String getDebugInfo() {
		ObjectDefinition def = getDefinition();
		
		String location = this.getData().getCenterLocation().getAbsoluteX() + ", " + this.getData().getCenterLocation().getAbsoluteY() + ", " + this.getPlane();
		
		if (def != null)
			return def.getName() + "(" + getId() + "): " + location;
		else
			return getId() + ": " + location;
	}
	
	public void printDebugInfo() {
		System.out.println(getDebugInfo());
	}
}
