package org.rs3.api.wrappers;

import java.applet.Applet;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.peer.ComponentPeer;
import java.util.ArrayList;
import java.util.List;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.accessors.I_Client;
import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.GraphicsSettingsWidget.GraphicsCpuUsage;
import org.rs3.api.interfaces.widgets.GraphicsSettingsWidget.GraphicsLevel;
import org.rs3.api.interfaces.widgets.LobbyScreen;
import org.rs3.api.interfaces.widgets.LobbyScreen.LobbyTab;
import org.rs3.api.interfaces.widgets.AccountCreateWidget;
import org.rs3.api.interfaces.widgets.LoginGraphicsAutoSetupScreen;
import org.rs3.api.interfaces.widgets.LoginGraphicsSetupConfirmationScreen;
import org.rs3.api.interfaces.widgets.LoginScreen;
import org.rs3.api.interfaces.widgets.LogoutWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.MiniMap;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.WorldSelect;
import org.rs3.api.objects.PreciseTile;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.Database;
import org.rs3.database.LevelsRow;
import org.rs3.database.ServerInfo;
import org.rs3.database.ServerRow;
import org.rs3.database.StatsRow;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Time;
import org.rs3.util.Timer;


public class Client {

	private static Object obj = null;
	public static java.awt.Graphics g = null;
	public static ComponentPeer peer = null;
	public static Applet applet = null;

	public static Object getObject() {
		return obj;
	}

	public static void setObject(Object obj) {
		Client.obj = obj;
	}

	/*public static Canvas getCanvas() {
		return iAccessor.getCanvas();
	}*/

	public static Canvas getCanvas() {
		return Canvas.create(I_Client.getCanvas(obj));
	}

	public static int getCanvasWidth() {
		return I_Client.getCanvasWidth(obj);
	}

	public static int getCanvasHeight() {
		return I_Client.getCanvasHeight(obj);
	}

	public static Applet getApplet1() {
		return I_Client.getApplet1(obj);
	}

	public static Applet getApplet2() {
		return I_Client.getApplet2(obj);
	}

	public static Applet getApplet3() {
		return I_Client.getApplet3(obj);
	}

	public static Graphics getRSGraphics() {
		return Graphics.create(I_Client.getRSGraphics(obj));
	}

	public static int getPlane() {
		return I_Client.getPlane(obj);
	}
	
	public static Array1D<Player> getPlayers() {
		return Array1D.create(Player.class, I_Client.getPlayers(obj));
	}

	public static Player getMyPlayer() {
		return Player.create(I_Client.getMyPlayer(obj));
	}

	public static Cache getPlayerModelCache() {
		return Cache.create(I_Client.getPlayerModelCache(obj));
	}

	public static Array1D<NpcNode> getNpcNodeArray() {
		return Array1D.create(NpcNode.class, I_Client.getNpcNodeArray(obj));
	}
	
	public static HashTable getNpcNodeCache() {
		return HashTable.create(I_Client.getNpcNodeCache(obj));
	}
	
	public static int[] getNpcNodeIndexArray() {
		return I_Client.getNpcNodeIndexArray(obj);
	}

	public static Array1D<Location> getLocationArray() {
		return Array1D.create(Location.class, I_Client.getLocationArray(obj));
	}

	public static Npc[] getAllNpcs_FromArray() {
		Array1D<NpcNode> npcNodeArray = getNpcNodeArray();
		if (npcNodeArray == null)
			return null;

		Npc[] npcs = new Npc[npcNodeArray.getLength()];
		for (int i = 0; i < npcNodeArray.getLength(); i++) {
			NpcNode npcNode = npcNodeArray.get(i);

			if (npcNode != null)
				npcs[i] = npcNode.getNpc();
			else
				npcs[i] = null;
		}

		return npcs;
	}
	
	public static Npc[] getAllNpcs_FromHashTable() {
		HashTable nc = getNpcNodeCache();
		if (nc == null)
			return null;

		Node[] npcNodes = nc.getAll();
		if (npcNodes == null)
			return null;
		
		Npc[] npcs = new Npc[npcNodes.length];
		for (int i = 0; i < npcNodes.length; i++) {
			Node node = npcNodes[i];
			if (!node.getObject().getClass().getName().equals(data.NpcNode.get.getInternalName())) {
				npcs[i] = null;
				continue;
			}
			
			NpcNode npcNode = node.forceCast(NpcNode.class);
			if (npcNode != null)
				npcs[i] = npcNode.getNpc();
			else
				npcs[i] = null;
		}

		return npcs;
	}

	public static Camera getCamera() {
		return Camera.create(I_Client.getCamera(obj));
	}

	public static GameInfo getGameInfo() {
		return GameInfo.create(I_Client.getGameInfo(obj));
	}

	public static Render getRender() {
		Object render = I_Client.getRender(obj);

		if (render != null) {
			if (render.getClass().getName().equals(data.SDRender.get.getInternalName()))
				return SDRender.create(render);
			else if (render.getClass().getName().equals(data.OpenGLRender.get.getInternalName()))
				return OpenGLRender.create(render);
			else
				return Render.create(render);
		}

		return null;
	}

	public static Rectangle[] getComponentBoundsArray() {
		return I_Client.getComponentBoundsArray(obj);
	}

	public static HashTable getComponentNodeCache() {
		return HashTable.create(I_Client.getComponentNodeCache(obj));
	}

	public static Array1D<Widget> getWidgetCache() {
		return Array1D.create(Widget.class, I_Client.getWidgetCache(obj));
	}

	public static boolean[] getValidWidgetArray() {
		return I_Client.getValidWidgetArray(obj);
	}

	public static Widget[] getValidatedWidgetCache(boolean nullPump) {
		Array1D<Widget> widgetCache = getWidgetCache();

		if (widgetCache == null)
			return null;

		boolean[] validArray = Client.getValidWidgetArray();

		if (validArray != null) {
			List<Widget> validWidgetCache = new ArrayList<>();

			for (int i = 0; i < validArray.length; i++) {
				if (validArray[i]) {
					Widget widget = widgetCache.get(i);

					if (widget != null) {
						Array1D<Component> children = widget.getComponents();

						if (children != null) {
							int count = 0;

							for (Component child : children.getAll()) {
								if (child != null && child.getBoundsArrayIndex() == -1)
									++count;
							}

							if (count != children.getLength()) {
								validWidgetCache.add(widget);
								continue;
							}
						}
					}
				}

				if (nullPump)
					validWidgetCache.add(null);
			}

			return validWidgetCache.toArray(new Widget[0]);
		}

		return null;
	}

	public static boolean isMenuOpen() {
		return I_Client.isMenuOpen(obj);
	}

	public static boolean isMenuCollapsed() {
		return I_Client.isMenuCollapsed(obj);
	}

	public static int getMenuX() {
		return I_Client.getMenuX(obj);
	}

	public static int getMenuY() {
		return I_Client.getMenuY(obj);
	}

	public static int getMenuWidth() {
		return I_Client.getMenuWidth(obj);
	}

	public static int getMenuHeight() {
		return I_Client.getMenuHeight(obj);
	}

	public static int getSubMenuX() {
		return I_Client.getSubMenuX(obj);
	}

	public static int getSubMenuY() {
		return I_Client.getSubMenuY(obj);
	}

	public static int getSubMenuWidth() {
		return I_Client.getSubMenuWidth(obj);
	}

	public static int getSubMenuHeight() {
		return I_Client.getSubMenuHeight(obj);
	}

	public static MenuItemNode getFirstMenuItem() {
		return MenuItemNode.create(I_Client.getFirstMenuItem(obj));
	}

	public static MenuItemNode getSecondMenuItem() {
		return MenuItemNode.create(I_Client.getSecondMenuItem(obj));
	}

	public static NodeDeque getMenuItems() {
		return NodeDeque.create(I_Client.getMenuItems(obj));
	}

	public static NodeSubQueue getCollapsedMenuItems() {
		return NodeSubQueue.create(I_Client.getCollapsedMenuItems(obj));
	}

	public static MenuGroupNode getCurrentMenuGroupNode() {
		return MenuGroupNode.create(I_Client.getCurrentMenuGroupNode(obj));
	}
	
	public static boolean isUsableComponentSelected() {
		return I_Client.isUsableComponentSelected(obj);
	}
	
	public static int getLastSelectedUsableComponentId() {
		return I_Client.getLastSelectedUsableComponentId(obj);
	}

	public static String getLastSelectedUsableComponentAction() {
		return I_Client.getLastSelectedUsableComponentAction(obj);
	}
	
	public static String getLastSelectedUsableComponentOption() {
		return I_Client.getLastSelectedUsableComponentOption(obj);
	}

	public static float getMinimapAngle() {
		return I_Client.getMinimapAngle(obj);
	}

	public static int getMinimapScale() {
		return I_Client.getMinimapScale(obj);
	}

	public static int getMinimapOffset() {
		return I_Client.getMinimapOffset(obj);
	}

	public static int getMinimapSetting() {
		return I_Client.getMinimapSetting(obj);
	}

	public static int getDestX() {
		return I_Client.getDestX(obj);
	}

	public static int getDestY() {
		return I_Client.getDestY(obj);
	}

	public static SettingData getSettingData() {
		return SettingData.create(I_Client.getSettingData(obj));
	}

	public static HashTable getItemTables() {
		return HashTable.create(I_Client.getItemTables(obj));
	}

	public static EventQueue getEventQueue() {
		return I_Client.getEventQueue(obj);
	}
	
	public static HashTable getItemNodeCache() {
		return HashTable.create(I_Client.getItemNodeCache(obj));
	}
	
	public static ItemDefLoader getItemDefLoader() {
		return ItemDefLoader.create(I_Client.getItemDefLoader(obj));
	}
	
	public static Array2D<GEOffer> getGEOffers() {
		return Array2D.create(GEOffer.class, I_Client.getGEOffers(obj));
	}
	
	public static WorldInfo getWorldInfo() {
		return WorldInfo.create(I_Client.getWorldInfo(obj));
	}
	
	public static GraphicsSettingsContainer getGraphicsSettingsContainer() {
		return GraphicsSettingsContainer.create(I_Client.getGraphicsSettingsContainer(obj));
	}


	public static int LOADED_MAP = 11;

	public static int getClientState() {
		return LOADED_MAP; // TODO: add client state hook; this is just a stub
	}

	public static int getCameraX() {
		Camera camera;
		CameraLocationData cameraData;
		
		if ((camera = getCamera()) != null && (cameraData = camera.getCameraLocationData()) != null) {
			PreciseTile cameraLocation = cameraData.getCameraLocation();
			
			if(cameraLocation != null)
				return (int) (Math.round(cameraLocation.getX() / 512 * 100.0) / 100.0);
		}
		
		return -1;
	}

	public static int getCameraY() {
		Camera camera;
		CameraLocationData cameraData;
		
		if ((camera = getCamera()) != null && (cameraData = camera.getCameraLocationData()) != null) {
			PreciseTile cameraLocation = cameraData.getCameraLocation();
			
			if(cameraLocation != null)
				return (int) (Math.round(cameraLocation.getY() / 512 * 100.0) / 100.0);
		}
		
		return -1;
	}

	public static int getCameraZ() {
		Camera camera;
		CameraLocationData cameraData;
		
		if ((camera = getCamera()) != null && (cameraData = camera.getCameraLocationData()) != null) {
			PreciseTile cameraLocation = cameraData.getCameraLocation();
			
			if (cameraLocation != null)
				return (int) (cameraLocation.getHeight());
		}
		
		return -1;
	}

	public static int getCameraPitch() {
		Camera camera;
		CameraLocationData cameraData;
		CentralLocationData centralData;
		
		if ((camera = getCamera()) != null 
				&& (cameraData = camera.getCameraLocationData()) != null 
				&& (centralData = camera.getCentralLocationData()) != null) {
			
			PreciseTile cameraLocation = cameraData.getCameraLocation();
			PreciseTile centralLocation = centralData.getCentralLocation();
	
			//System.out.println("CameraLocation: " + cameraLocation.getX() + ", " + cameraLocation.getY() + ", " + cameraLocation.getHeight());
			//System.out.println("CentralLocation: " + centralLocation.getX() + ", " + centralLocation.getY() + ", " + centralLocation.getHeight());
			
			if (cameraLocation != null && centralLocation != null) {
				float x = Math.abs(cameraLocation.getX() - centralLocation.getX());
				float y = Math.abs(cameraLocation.getY() - centralLocation.getY());
				float z = Math.abs(cameraLocation.getHeight() - centralLocation.getHeight());
				float xy = (float) Math.sqrt((x * x) + (y * y));
	
				return (int) (90 - (Math.atan2(xy, z) * 57.297178369976124249491468687151D));            
			}
		}
		
		return -1;
	}
	
	public static int getCameraYaw() {
		Camera camera;
		CameraLocationData cameraData;
		CentralLocationData centralData;
		
		if ((camera = getCamera()) != null 
				&& (cameraData = camera.getCameraLocationData()) != null 
				&& (centralData = camera.getCentralLocationData()) != null) {
			
			PreciseTile cameraLocation = cameraData.getCameraLocation();
			PreciseTile centralLocation = centralData.getCentralLocation();
			
			if (cameraLocation != null && centralLocation != null) {
				float x = cameraLocation.getX() - centralLocation.getX();
				float y = cameraLocation.getY() - centralLocation.getY();
				
				return (int) (180 - (Math.atan2(x, y) * 57.297178369976124249491468687151D));
			}
		}
		
		return -1;
	}
	
	/**
	 * 
	 * @return Zoom 0-100%
	 */
	public static int getZoomPercent() {
		Camera camera;
		CameraLocationData cameraData;
		
		if ((camera = getCamera()) != null 
				&& (cameraData = camera.getCameraLocationData()) != null) {
			
			Location point1 = cameraData.getPoint1();
			
			if (point1 != null)
				return 100 - Math.round(((point1.getY() - 1860) / (7600 - 1860))*100);
		}
		
		return -1;
	}

	public static boolean isRSDoneLoading() {
		//CustomClassLoader.cacheClassesToAdd();

		try {
			if (data.Component.get.getInternalName() == null)
				return false;
			
			if (CustomClassLoader.get.loadClass(data.Component.get.getInternalName()) == null)
				return false;

			// The following classes aren't loaded until the login screen appears (there may be more - 9/14/14)
			/*if (CustomClassLoader.get.loadClass(data.AnimatedObject.get.getInternalName()) == null)
				return false;

			if (CustomClassLoader.get.loadClass(data.Animation.get.getInternalName()) == null)
				return false;

			if (CustomClassLoader.get.loadClass(data.InteractableData.get.getInternalName()) == null)
				return false;*/
		} catch (ClassNotFoundException e) {
			//e.printStackTrace();
			return false;
		}

		if (Client.getWidgetCache() == null)
			return false;
		else if (LoginGraphicsAutoSetupScreen.get.isOpen() || LoginGraphicsSetupConfirmationScreen.get.isOpen())
			return true;
		else if (LoginScreen.get.isOpen() || LobbyScreen.get.isOpen())
			return true;
		else if (MainWidget.get.isOpen())
			return true;
		else if (AccountCreateWidget.get.isOpen())
			return true;
		else if (MiniMap.get.isOpen() && MiniMap.mapBounds.getComponent() != null && MiniMap.mapBounds.getComponent().isVisible())
			return true;

		return false;
	}

	public static boolean isLoggedIn() {
		if (Client.getWidgetCache() == null)
			return false;
		if (LoginScreen.get.isOpen() || LobbyScreen.get.isOpen())
			return false;
		else if (MainWidget.get.isOpen())
			return true;
		else if (Ribbon.get.isOpen())
			return true;
		else if (ActionBarWidget.get.isOpen())
			return true;
		else if (MiniMap.get.isOpen() && MiniMap.mapBounds.getComponent() != null && MiniMap.mapBounds.getComponent().isVisible())
			return true;

		return false;
	}

	/**
	 * Attempts to login for up to 5 mins.
	 * 
	 * @return
	 */
	public static boolean loopLogin() {
		boolean loggedIn = false;

		//Timer loginTimer = new Timer(120000); // 2 mins
		Timer loginTimer = new Timer(300000); // 5 mins
		while (loginTimer.isRunning()) {
			if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
				return false;
			
			if (Client.isLoggedIn()) {
				loggedIn = true;
				break;
			}

			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Client.login(row.getWorld());
			}
			
			Time.sleepMedium();
		}

		return loggedIn;
	}

	public static boolean login(int world) {
		if (!isLoggedIn()) {
			if (LoginGraphicsAutoSetupScreen.get.isOpen()) {
				LoginGraphicsAutoSetupScreen.get.clickAutoSetupButton(true);
				return false;
			} else if (LoginGraphicsSetupConfirmationScreen.get.isOpen()) {
				LoginGraphicsSetupConfirmationScreen.get.clickYesButton(true);
				return false;
			} else if (LobbyScreen.get.isOpen()) {
				if (!GraphicsCpuUsage.VERY_LOW.isSelected()) {
					if (GraphicsLevel.MIN.select(true)) {
						if (GraphicsCpuUsage.VERY_LOW.select(true))
							Time.sleepQuick();
					}
				}
				
				if (!GraphicsCpuUsage.VERY_LOW.isSelected())
					return false;
				
				if (WorldSelect.get.select(world) && LobbyScreen.get.handleMessage()) {
					String msg = LobbyScreen.get.getMessageText();
					if (msg != null && msg.toLowerCase().contains("logging in")) {
						Time.sleep(500);
						return false;
					}
					
					// play button not visible in options tab, so move to another
					LobbyTab lobbyTab = LobbyTab.getSelected();
					if (lobbyTab != null && lobbyTab.equals(LobbyTab.OPTIONS)) {
						LobbyTab.WORLD_SELECT.select(true);
					}
					
					return LobbyScreen.get.clickPlayButton();
				}
			} else if (LoginScreen.get.isOpen()) {
				if (LoginScreen.get.isForgotPasswordScreenOpen()) {
					LoginScreen.get.clickForgotPasswordBackButton(true);
					Time.sleepQuick();
				}
				
				if (LoginScreen.get.isLoginMessageVisible()) {
					String msg = LoginScreen.get.getLoginMessageText();
					if (msg.toLowerCase().contains("invalid email or password")) {
						LoginScreen.get.clickTryAgainButton(true);
						//return false;
					} else if (msg.toLowerCase().contains("invalid username or password")) {
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("invalid login or password")) {
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("you must enter a valid login")) {
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("page has opened in a new window")) {
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("your account has not logged out")) {
						Time.sleep(10000, 30000);
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("login limit exceeded") || msg.toLowerCase().contains("too many connections")) {
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("error connecting to server")) {
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("our systems are currently unavailable")) {
						Time.sleep(30000, 60000);
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("connection timed out")) { // Connection timed out. Please try using a different world.
						LoginScreen.get.clickTryAgainButton(true);
						// return false;
					} else if (msg.toLowerCase().contains("your account has been involved in serious rule breaking")) {
						System.out.println("Account banned! Stopping script...");
						LoginScreen.get.clickTryAgainButton(true);
						ScriptHandler.currentScript.interruptStop();
						
						// clear database records
						if (AccountInfo.cur != null) {
							if (Database.isConnectionValid(5000)) {
								int accountId = AccountInfo.cur.getId();
								new StatsRow(accountId).executeDelete();
								new LevelsRow(accountId).executeDelete();
								
								AccountRow accountRow = AccountRow.select(accountId);
								if (accountRow != null) {
									accountRow.setScriptId(0);
									accountRow.setScriptParams("HideType=green;OreType=auto;MineLocation=auto;EnemyType=auto;CombatLocation=auto;AttackStyle=Melee;");
									accountRow.setLastScriptUpdate(null);
									accountRow.setDisabled(true);
									accountRow.executeUpdate();
								}
							}
						}
						
						return false;
					} else if (msg.toLowerCase().contains("runescape has been updated")) {
						System.out.println("RuneScape updated! Stopping script and restarting bot...");
						ScriptHandler.currentScript.interruptStop();
						
						if (ServerInfo.cur == null)
							System.out.println("ServerInfo is null!");
						else {
							Thread thread = new Thread() {
								public void run() {
									System.out.println("Waiting 5 minutes before restart...");
									Time.sleep(300000);
									ServerRow row = ServerInfo.cur.getRow();
									row.setKillBot(false);
									row.setRestartBot(true);
									row.executeUpdate();
								}
							};
							thread.start();
						}
						
						return false;
					} else if (msg.toLowerCase().contains("your game session has now ended")) {
						System.out.println("Game session ended! Stopping script and restarting bot...");
						ScriptHandler.currentScript.interruptStop();
						Time.sleepMedium();
						
						if (ServerInfo.cur == null)
							System.out.println("ServerInfo is null!");
						else {
							ServerRow row = ServerInfo.cur.getRow();
							row.setKillBot(false);
							row.setRestartBot(true);
							row.executeUpdate();
						}
						
						return false;
					} else {
						System.out.println("Unknown login message: " + msg);
						Time.sleep(5000);
					}
				}
				
				LoginScreen.get.enterUsername(AccountInfo.cur.getLogin());
				Time.sleepQuick();
				LoginScreen.get.enterPassword(AccountInfo.cur.getPassword());
				Time.sleepQuick();
				LoginScreen.get.clickLoginButton(true);
				
				Timer timer = new Timer(5000);
				while (timer.isRunning()) {
					if (LoginScreen.get.isLoginMessageVisible() && !LoginScreen.get.getLoginMessageText().toLowerCase().contains("logging in"))
						return false;
					
					if (isLoggedIn() || LobbyScreen.get.isOpen())
						return true;
					
					Time.sleep(50);
				}
				
				return false;
			}
		}

		return true;
	}
	
	public static boolean logout(boolean lobby) {
		if (lobby) {
			if (LobbyScreen.get.isOpen() || LoginScreen.get.isOpen())
				return true;
		} else if (LoginScreen.get.isOpen())
			return true;
		
		if (LobbyScreen.get.isOpen()) {
			return LobbyScreen.get.close(true);
		}
		
		if (LogoutWidget.get.isOpen() || MainWidget.get.clickMinimapLogoutButton(true)) {
			boolean success = false;
			Timer timer = new Timer(3000);
			while (timer.isRunning()) {
				if (LogoutWidget.get.isOpen()) {
					success = true;
					break;
				}
				
				Time.sleep(50);
			}
			
			if (success) {
				Time.sleepQuick();
				
				if (lobby) {
					if (LogoutWidget.get.clickExitToLobbyButton(true)) {
						timer = new Timer(10000);
						while (timer.isRunning()) {
							if (LobbyScreen.get.isOpen() || LoginScreen.get.isOpen())
								return true;
							
							Time.sleep(50);
						}
					}
				} else {
					if (LogoutWidget.get.clickLogoutButton(true)) {
						timer = new Timer(10000);
						while (timer.isRunning()) {
							if (LoginScreen.get.isOpen())
								return true;
							
							Time.sleep(50);
						}
					}
				}
			}
		}
		
		return false;
	}
}
