package org.rs3.api.wrappers;

import org.rs3.accessors.I_SDModel;
import org.rs3.api.Calculations;


public class SDModel extends Model {
	
	protected SDModel(Object obj) {
		super(obj);
		
		this.type = TYPE_SDMODEL;
	}
	
	public static SDModel create(Object obj, int orientationMod, int heightOff) {
		SDModel model = create(SDModel.class, obj);
		
		if (model == null || model.xPoints == null || model.yPoints == null || model.zPoints == null)
			return null;
		else {
			model.orientationMod = orientationMod;
			model.heightOff = heightOff;
			return model;
		}
	}
	
	@Override
	void update() {
		super.update();
	}
	
	
	@Override
	public short[] getIndices1() {
		return I_SDModel.getIndices1(obj);
	}
	
	@Override
	public short[] getIndices2() {
		return I_SDModel.getIndices2(obj);
	}
	
	@Override
	public short[] getIndices3() {
		return I_SDModel.getIndices3(obj);
	}
	
	@Override
	public int[] getXPoints() {
		return I_SDModel.getXPoints(obj);
	}
	
	@Override
	public int[] getYPoints() {
		return I_SDModel.getYPoints(obj);
	}
	
	@Override
	public int[] getZPoints() {
		return I_SDModel.getZPoints(obj);
	}
	
	@Override
	public int getNumFaces() {
		return I_SDModel.getNumFaces(obj);
	}
	
	@Override
	public int getNumVertices() {
		return I_SDModel.getNumVertices(obj);
	}
	
	
	@Override
	public int[][] projectVertices(double locX, double locY, int plane) {
		Render render = Client.getRender();
		
		//Viewport viewport = render.getClientViewport();
		
		float[] data = null;
		
		float[] callBackFloats = Viewport.getCallBackFloats();
		if (callBackFloats == null)
			data = render.getClientViewport().getFloats().clone();
		else
			data = callBackFloats.clone();
		
		if (orientationMod != 0) // just for efficiency
			orientationMod = (orientationMod + 360) % 360; // modified for SIN/COS table usage (+ 360 for the known case of negative values)
		
		int[][] screen = new int[this.numVertices][2];
	
		final float checkOff = data[14];
		final float xOff = data[12];
		final float yOff = data[13];
		final float zOff = data[15];
		final float checkX = data[2];
		final float checkY = data[6];
		final float checkZ = data[10];
		final float xX = data[0];
		final float xY = data[4];
		final float xZ = data[8];
		final float yX = data[1];
		final float yY = data[5];
		final float yZ = data[9];
		final float zX = data[3];
		final float zY = data[7];
		final float zZ = data[11];
		
		final float render_absoluteX = render.getAbsoluteX();
		final float render_absoluteY = render.getAbsoluteY();
		final float render_xMultiplier = render.getXMultiplier();
		final float render_yMultiplier = render.getYMultiplier();
		
		final int height = Calculations.tileHeight((int)locX, (int)locY, plane) + this.heightOff;
		
		for (int index = 0; index < this.numVertices; index++) {
			/*int vertexX = (int) (this.xPoints[index] + locX);
			int vertexY = this.yPoints[index] + height;
			int vertexZ = (int) (this.zPoints[index] + locY);*/
			
			final int x = this.xPoints[index];
			final int y = this.yPoints[index];
			final int z = this.zPoints[index];
			
			//double rads = (0.017453292519943295 * rotateDegs); // convert rotateDegs from degrees to radians
			
			// X-axis rotation
			/*int vertexX = x;
			int vertexY = (int) (y * Math.cos(rads) - z * Math.sin(rads));
			int vertexZ = (int) (y * Math.sin(rads) + z * Math.cos(rads));*/
			
			// Y-axis rotation
			/*int vertexX = (int) (z * Math.sin(rads) + x * Math.cos(rads));
			int vertexY = y;
			int vertexZ = (int) (z * Math.cos(rads) - x * Math.sin(rads));*/
			
			// Z-axis rotation
			/*int vertexX = (int) (x * Math.cos(rads) - y * Math.sin(rads));
			int vertexY = (int) (x * Math.sin(rads) + y * Math.cos(rads));
			int vertexZ = z;*/
			
			// Y-axis rotation
			int vertexX = (int) (x * D2R_COS_TABLE[orientationMod] + z * D2R_SIN_TABLE[orientationMod]);
			int vertexY = y;
			int vertexZ = (int) (-x * D2R_SIN_TABLE[orientationMod] + z * D2R_COS_TABLE[orientationMod]);
			
			vertexX += locX;
			vertexY += height;
			vertexZ += locY;
			
			float _check = (checkOff + (checkX * vertexX + checkY * vertexY + checkZ * vertexZ));
			float _z = (zOff + (zX * vertexX + zY * vertexY + zZ * vertexZ));
			
			if (_check >= -_z) {
				float _x = (xOff + (xX * vertexX + xY * vertexY + xZ * vertexZ));
				float _y = (yOff + (yX * vertexX + yY * vertexY + yZ * vertexZ));
				
				//float fx = Math.round(render_absoluteX + render_xMultiplier * _x / _z);
				//float fy = Math.round(render_absoluteY + render_yMultiplier * _y / _z);
				
				if (_x >= -_z && _x <= _z && _y >= -_z && _y <= _z) {
					//screen[index][0] = (int)(render_absoluteX + (render_xMultiplier - 35) * _x / _z); // TODO: the multiplier hack might be because of my dpi settings
					//screen[index][1] = (int)(render_absoluteY + (render_yMultiplier - 35) * _y / _z);
					screen[index][0] = (int)(render_absoluteX + render_xMultiplier * _x / _z);
					screen[index][1] = (int)(render_absoluteY + render_yMultiplier * _y / _z);
				} else {
					screen[index][0] = -999999; // actually, in the rs client it is -999999.0F;
					//screen[index][1] = -1; // this is not set in the rs client
				}
			} else {
				screen[index][0] = -999999; // actually, in the rs client it is -999999.0F;
				//screen[index][1] = -1; // this is not set in the rs client
			}
		}
		
		return screen;
	}
}
