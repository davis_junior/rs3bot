package org.rs3.api.wrappers;

public class Camera extends AbstractCamera {
	
	protected Camera(Object obj) {
		super(obj);
	}
	
	public static Camera create(Object obj) {
		return create(Camera.class, obj);
	}
}
