package org.rs3.api.wrappers;

import org.rs3.accessors.I_Settings;


public class Settings extends AccessorWrapper {
	
	public static final int BOOLEAN_RUN_ENABLED = 463;
	
	protected Settings(Object obj) {
		super(obj);
	}
	
	public static Settings create(Object obj) {
		return create(Settings.class, obj);
	}
	
	
	public int[] getData() {
		return I_Settings.getData(obj);
	}
	
	
	/**
	 * @param index The position of this setting in the game's database.
	 * @return The setting value of the desired index.
	 */
	public int get(final int index) {
		final int[] settings = getData();
		
		if (index < settings.length)
			return settings[index];
		
		return -1;
	}

	/**
	 * Gets the value at a given index and applies a given mask to the value
	 *
	 * @param index the index in the settings array
	 * @param mask  the bitmask
	 * @return the masked value
	 */
	public int get(final int index, final int mask) {
		return get(index) & mask;
	}

	/**
	 * Gets the value at a given index, bit shifts it right by a given number of bits and applies a mask
	 *
	 * @param index the index in the settings array
	 * @param shift the number of bits to right shift
	 * @param mask  the bitmask
	 * @return the masked value
	 */
	public int get(final int index, final int shift, final int mask) {
		return (get(index) >>> shift) & mask;
	}
}
