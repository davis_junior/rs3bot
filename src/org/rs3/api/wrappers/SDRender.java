package org.rs3.api.wrappers;

import org.rs3.accessors.I_SDRender;


public class SDRender extends Render {
	
	protected SDRender(Object obj) {
		super(obj);
	}
	
	public static SDRender create(Object obj) {
		return create(SDRender.class, obj);
	}
	
	
	@Override
	public Viewport getClientViewport() {
		return Viewport.create(I_SDRender.getClientViewport(obj));
	}
	
	@Override
	public float getXMultiplier() {
		return I_SDRender.getXMultiplier(obj);
	}
	
	@Override
	public float getYMultiplier() {
		return I_SDRender.getYMultiplier(obj);
	}
	
	@Override
	public float getAbsoluteX() {
		return I_SDRender.getAbsoluteX(obj);
	}
	
	@Override
	public float getAbsoluteY() {
		return I_SDRender.getAbsoluteY(obj);
	}
}
