package org.rs3.api.wrappers;

import java.awt.Graphics;

import javax.media.opengl.GL2;

import org.rs3.accessors.I_Player;
import org.rs3.api.Nodes;

public class Player extends Character {
	
	protected Player(Object obj) {
		super(obj);
	}
	
	public static Player create(Object obj) {
		return create(Player.class, obj);
	}
	
	
	public PlayerDef getDefinition() {
		return PlayerDef.create(I_Player.getDefinition(obj));
	}

	public String getName() {
		return I_Player.getName(obj);
	}
	
	public String getTitle() {
		return I_Player.getTitle(obj);
	}
	
	
	public Model getCachedModel() {
		try {
			PlayerDef definition = getDefinition();
			
			if (definition == null)
				return null;
			
			Node ref = Nodes.lookup(Client.getPlayerModelCache().getHashTable(), definition.getModelHash());
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, getOrientation(), 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, getOrientation(), 0);
					} else {
						return Model.create(def, getOrientation(), 0);
					}
				}
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, getOrientation(), 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, getOrientation(), 0);
					} else {
						return Model.create(def, getOrientation(), 0);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public Model getModel() {
		return getCachedModel();
	}
	
	
	@Override
	public void draw(final Graphics g) {
		if (curModel == null) {
			//updateModel();
		} else {
			curModel.draw(g, getData().getCenterLocation().getX(), getData().getCenterLocation().getY(), getPlane());
		}
		
		/*Model model = getCachedModel();
		
		if (model != null)
			model.draw(g, getData().getCenterLocation().getX(), getData().getCenterLocation().getY(), getPlane());
		else {
			System.out.println("player model from cache failed!");
			super.draw(g);
		}*/
	}
	
	@Override
	public void render(GL2 gl2) {
		if (curModel == null) {
			//updateModel();
		} else {
			curModel.render(gl2, getData().getCenterLocation().getX(), getData().getCenterLocation().getY(), getPlane());
		}
		
		/*Model model = getCachedModel();
		
		if (model != null)
			model.render(gl2, getData().getCenterLocation().getX(), getData().getCenterLocation().getY(), getPlane()));
		else {
			System.out.println("player model from cache failed!");
			//super.render(gl2);
		}*/
	}
}
