package org.rs3.api.wrappers;

import org.rs3.accessors.I_BaseInfo;


public class BaseInfo extends AccessorWrapper {
	
	protected BaseInfo(Object obj) {
		super(obj);
	}
	
	public static BaseInfo create(Object obj) {
		return create(BaseInfo.class, obj);
	}
	
	
	public int getX() {
		return I_BaseInfo.getX(obj);
	}
	
	public int getY() {
		return I_BaseInfo.getY(obj);
	}
}
