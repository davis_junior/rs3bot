package org.rs3.api.wrappers;

import org.rs3.accessors.I_GameInfo;


public class GameInfo extends AccessorWrapper {
	
	protected GameInfo(Object obj) {
		super(obj);
	}
	
	public static GameInfo create(Object obj) {
		return create(GameInfo.class, obj);
	}
	
	
	public BaseInfo getBaseInfo() {
		return BaseInfo.create(I_GameInfo.getBaseInfo(obj));
	}
	
	public GroundInfo getGroundInfo() {
		return GroundInfo.create(I_GameInfo.getGroundInfo(obj));
	}
	
	public GroundBytes getGroundBytes() {
		return GroundBytes.create(I_GameInfo.getGroundBytes(obj));
	}
	
	public ObjectDefLoader getObjectDefLoader() {
		return ObjectDefLoader.create(I_GameInfo.getObjectDefLoader(obj));
	}
}
