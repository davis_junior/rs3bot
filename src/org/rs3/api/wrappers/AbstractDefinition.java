package org.rs3.api.wrappers;

public abstract class AbstractDefinition extends AccessorWrapper {
	
	protected AbstractDefinition(Object obj) {
		super(obj);
	}
	
	public static AbstractDefinition create(Object obj) {
		return create(AbstractDefinition.class, obj);
	}
}
