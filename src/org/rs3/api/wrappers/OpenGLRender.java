package org.rs3.api.wrappers;

import org.rs3.accessors.I_OpenGLRender;


public class OpenGLRender extends Render {
	
	protected OpenGLRender(Object obj) {
		super(obj);
	}
	
	public static OpenGLRender create(Object obj) {
		return create(OpenGLRender.class, obj);
	}
	
	
	public Object getOpenGL() {
		return I_OpenGLRender.getOpenGL(obj);
	}
	
	@Override
	public Viewport getClientViewport() {
		return Viewport.create(I_OpenGLRender.getClientViewport(obj));
	}
	
	@Override
	public float getXMultiplier() {
		return I_OpenGLRender.getXMultiplier(obj);
	}
	
	@Override
	public float getYMultiplier() {
		return I_OpenGLRender.getYMultiplier(obj);
	}
	
	@Override
	public float getAbsoluteX() {
		return I_OpenGLRender.getAbsoluteX(obj);
	}
	
	@Override
	public float getAbsoluteY() {
		return I_OpenGLRender.getAbsoluteY(obj);
	}
}
