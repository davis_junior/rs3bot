package org.rs3.api.wrappers;

public class GraphicsCpuUsage extends AbstractGraphicsSetting {
	
	protected GraphicsCpuUsage(Object obj) {
		super(obj);
	}
	
	public static GraphicsCpuUsage create(Object obj) {
		return create(GraphicsCpuUsage.class, obj);
	}
}
