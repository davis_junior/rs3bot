package org.rs3.api.wrappers;

import org.rs3.accessors.I_IdComposite_AnimatedObject;


public class IdComposite_AnimatedObject extends AccessorWrapper {
	
	protected IdComposite_AnimatedObject(Object obj) {
		super(obj);
	}
	
	public static IdComposite_AnimatedObject create(Object obj) {
		return create(IdComposite_AnimatedObject.class, obj);
	}
	
	
	public long getId() {
		return I_IdComposite_AnimatedObject.getId(obj);
	}
}
