package org.rs3.api.wrappers;

import org.rs3.accessors.I_Interactable;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.BasicInteractable;
import org.rs3.api.objects.Tile;
import org.rs3.util.Condition;


public class Interactable extends EntityNode implements BasicInteractable {
	
	protected Interactable(Object obj) {
		super(obj);
	}
	
	public static Interactable create(Object obj) {
		return create(Interactable.class, obj);
	}
	
	
	public byte getPlane() {
		return I_Interactable.getPlane(obj);
	}

	@Override
	public Model getModel() {
		return null;
	}

	@Override
	public boolean loopInteract(Condition breakCondition, String action) {
		return BasicInteractable.Methods.loopInteract(this, breakCondition, action);
	}

	@Override
	public boolean loopInteract(Condition breakCondition, String action, String option,
			boolean breakOnWrongOption) {
		
		return BasicInteractable.Methods.loopInteract(this, breakCondition, action, option, breakOnWrongOption);
	}

	@Override
	public boolean loopInteract(boolean keepModelUpdated, Condition breakCondition, String action,
			String option, boolean breakOnWrongOption) {
		
		return BasicInteractable.Methods.loopInteract(this, keepModelUpdated, breakCondition, action, option, breakOnWrongOption);
	}

	@Override
	public boolean loopWalkTo(long maxPeriod) {
		return getTile().loopWalkTo(maxPeriod);
	}

	@Override
	public boolean loopWalkTo(long maxPeriod, Condition breakCondition) {
		return getTile().loopWalkTo(maxPeriod, breakCondition);
	}
	
	/**
	 * Walks to Interactable if within the constraining area.
	 * 
	 * @param constrainingArea
	 * @param maxPeriod
	 * @param breakCondition
	 * @return
	 */
	public boolean loopWalkTo(Area constrainingArea, long maxPeriod, Condition breakCondition) {
		Tile orig = getTile();
		Tile walkTile = orig.randomize(2, 2);
		if (!constrainingArea.contains(walkTile)) {
			// try to get a random tile within the constraining area
			boolean success = false;
			for (int i = 0; i < 5; i++) {
				walkTile = orig.randomize(2, 2);
				if (constrainingArea.contains(walkTile)) {
					success = true;
					break;
				}
			}
			
			if (!success)
				return false;
		}
		
		return walkTile.loopWalkTo(constrainingArea, maxPeriod, breakCondition);
	}
}
