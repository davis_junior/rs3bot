package org.rs3.api.wrappers;

import java.awt.Graphics;

import javax.media.opengl.GL2;

import org.rs3.accessors.I_AnimatedBoundaryObject;
import org.rs3.api.Nodes;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;

public class AnimatedBoundaryObject extends AbstractBoundary implements GroundObject {
	
	protected AnimatedBoundaryObject(Object obj) {
		super(obj);
	}
	
	public static AnimatedBoundaryObject create(Object obj) {
		return create(AnimatedBoundaryObject.class, obj);
	}
	
	
	public AnimatedObject getAnimatedObject() {
		return AnimatedObject.create(I_AnimatedBoundaryObject.getAnimatedObject(obj));
	}
	
	
	@Override
	public Tile getTile() {
		return getData().getCenterLocation().getTile(true);
	}
	
	@Override
	public int getId() {
		AnimatedObject animated = getAnimatedObject();
		
		if (animated != null)
			return animated.getId();
		
		return -1;
	}
	
	@Override
	public ObjectDefinition getDefinition() {
		try {
			Node ref = Nodes.lookup(getAnimatedObject().getDefinitionLoader().getDefinitionCache().getHashTable(), getAnimatedObject().getId());
			
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null)
					return ObjectDefinition.create(def);
			
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null)
					return ObjectDefinition.create(def);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public Model getModel() {
		AnimatedObject animated = getAnimatedObject();
		
		if (animated != null)
			return animated.getModel();
		
		return null;
	}
	
	
	@Override
	public void draw(Graphics g) {
		Model model = getModel();
		
		if (model != null)
			model.draw(g, getLocation(), getPlane());
	}
	
	@Override
	public void render(GL2 gl2) {
		Model model = getModel();
		
		if (model != null)
			model.render(gl2, getLocation(), getPlane());
	}
}
