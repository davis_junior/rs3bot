package org.rs3.api.wrappers;

import org.rs3.accessors.I_Animator;


public class Animator extends AccessorWrapper {
	
	protected Animator(Object obj) {
		super(obj);
	}
	
	public static Animator create(Object obj) {
		return create(Animator.class, obj);
	}
	
	
	public Animation getAnimation() {
		return Animation.create(I_Animator.getAnimation(obj));
	}
}
