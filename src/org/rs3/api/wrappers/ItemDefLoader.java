package org.rs3.api.wrappers;

public class ItemDefLoader extends DefLoader {
	
	protected ItemDefLoader(Object obj) {
		super(obj);
	}
	
	public static ItemDefLoader create(Object obj) {
		return create(ItemDefLoader.class, obj);
	}
}
