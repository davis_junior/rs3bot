package org.rs3.api.wrappers;

import org.rs3.accessors.I_Viewport;
import org.rs3.callbacks.ViewportCallback;

public class Viewport extends AccessorWrapper {
	
	public static float[] getCallBackFloats() {
		/*Render render = Client.getRender();
		if (render != null) {
			Viewport viewport = Viewport.create(Reflection.getDeclaredField("adv", render.getObject(), "bi"));
			if (viewport != null) {
				float[] floats = viewport.getFloats();
				if (floats != null)
					return floats.clone();
			}
		}
		
		
		return null;*/
		//return ViewportCapture.floats;
		return ViewportCallback.floats;
	}
	
	protected Viewport(Object obj) {
		super(obj);
	}
	
	public static Viewport create(Object obj) {
		return create(Viewport.class, obj);
	}
	
	
	public float[] getFloats() {
		return I_Viewport.getFloats(obj);
	}
}
