package org.rs3.api.wrappers;

public class GraphicsLevel extends AbstractGraphicsSetting {
	
	protected GraphicsLevel(Object obj) {
		super(obj);
	}
	
	public static GraphicsLevel create(Object obj) {
		return create(GraphicsLevel.class, obj);
	}
}
