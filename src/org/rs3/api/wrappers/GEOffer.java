package org.rs3.api.wrappers;

import org.rs3.accessors.I_GEOffer;


public class GEOffer extends AccessorWrapper {
	
	protected GEOffer(Object obj) {
		super(obj);
	}
	
	public static GEOffer create(Object obj) {
		return create(GEOffer.class, obj);
	}
	
	
	public byte getStatus() {
		return I_GEOffer.getStatus(obj);
	}
	
	public int getId() {
		return I_GEOffer.getId(obj);
	}
	
	public int getPrice() {
		return I_GEOffer.getPrice(obj);
	}
	
	public int getTotal() {
		return I_GEOffer.getTotal(obj);
	}
	
	public int getTransfered() {
		return I_GEOffer.getTransfered(obj);
	}
	
	public int getSpend() {
		return I_GEOffer.getSpend(obj);
	}
}
