package org.rs3.api.wrappers;

import org.rs3.accessors.I_NpcNode;

public class NpcNode extends Node {
	
	protected NpcNode(Object obj) {
		super(obj);
	}
	
	public static NpcNode create(Object obj) {
		return create(NpcNode.class, obj);
	}
	
	
	/*public Object getNpc() {
		return I_NpcNode.getNpc(obj);
	}*/
	
	public Npc getNpc() {
		return Npc.create(I_NpcNode.getNpc(obj));
	}
}
