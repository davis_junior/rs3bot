package org.rs3.api.wrappers;

import java.awt.Graphics;
import java.awt.Point;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.rs3.accessors.I_Location;
import org.rs3.api.Calculations;
import org.rs3.api.objects.PreciseTile;
import org.rs3.api.objects.Tile;

public class Location extends AccessorWrapper {
	
	Tile cachedLocalTile = null;
	PreciseTile cachedLocalPreciseTile = null;
	
	Tile cachedGlobalTile = null;
	PreciseTile cachedGlobalPreciseTile = null;
	
	protected Location(Object obj) {
		super(obj);
		
		float x = getX();
		float y = getY();
		float height = getHeight();
		int plane = Client.getPlane();
		
		cachedLocalTile = new Tile(getRelativeX(), getRelativeY(), plane);
		cachedLocalPreciseTile = new PreciseTile(x, y, height, plane);
		
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		int baseX = base.getX();
		int baseY = base.getY();
		
		cachedGlobalTile = new Tile(baseX + getRelativeX(), baseY + getRelativeY(), plane);
		cachedGlobalPreciseTile = new PreciseTile((baseX << 9) + x, (baseY << 9) + y, height, plane); // TODO: test << 9
	}
	
	public static Location create(Object obj) {
		return create(Location.class, obj);
	}
	

	public float getX() {
		return I_Location.getX(obj);
	}
	
	public float getY() {
		return I_Location.getY(obj);
	}
	
	public int getRelativeX() {
		return Math.round(getX()) >> 9;
	}
	
	public int getRelativeY() {
		return Math.round(getY()) >> 9;
	}
	
	public int getAbsoluteX() {
		return Client.getGameInfo().getBaseInfo().getX() + getRelativeX();
	}
	
	public int getAbsoluteY() {
		return Client.getGameInfo().getBaseInfo().getY() + getRelativeY();
	}
	
	public float getHeight() {
		return I_Location.getHeight(obj);
	}
	
	public Tile getLocalTile(boolean cached) {
		if (cached)
			return cachedLocalTile;
		else
			return new Tile(getRelativeX(), getRelativeY(), Client.getPlane());
	}
	
	public PreciseTile getLocalPreciseTile(boolean cached) {
		if (cached)
			return cachedLocalPreciseTile;
		else
			return new PreciseTile(getX(), getY(), getHeight(), Client.getPlane());
	}
	
	public Tile getGlobalTile(boolean cached) {
		if (cached)
			return cachedGlobalTile;
		else {
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			int baseX = base.getX();
			int baseY = base.getY();
			
			return new Tile(baseX + getRelativeX(), baseY + getRelativeY(), Client.getPlane());
		}
	}
	
	public PreciseTile getGlobalPreciseTile(boolean cached) {
		if (cached)
			return cachedGlobalPreciseTile;
		else {
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			int baseX = base.getX();
			int baseY = base.getY();
			
			return new PreciseTile((baseX << 9) + getX(), (baseY << 9) + getY(), getHeight(), Client.getPlane());
		}
	}
	
	// convenience method
	public Tile getTile(boolean cached) {
		return getGlobalTile(cached);
	}
	
	// convenience method
	public PreciseTile getPreciseTile(boolean cached) {
		return getGlobalPreciseTile(cached);
	}
	
	public Point getMapPoint() {
		return Calculations.worldToMap(getAbsoluteX(), getAbsoluteY());
	}
	
	public boolean isOnMap() {
		final Point p = getMapPoint();
		
		if (p == null)
			return false;
		
		return p.x != -1 && p.y != -1;
	}
	
	/*public Point getPoint(final double xOff, final double yOff, final int heightOff) {
		return Calculations.groundToScreen((int) ((getAbsoluteX() - Client.getGameInfo().getBaseInfo().getX() + xOff) * 0x200), (int) ((getAbsoluteY() - Client.getGameInfo().getBaseInfo().getY() + yOff) * 0x200), Client.getPlane(), -heightOff);
	}
	
	public Polygon getBounds() {
		Point localPoint1 = getPoint(0.0D, 0.0D, 0);
		Point localPoint2 = getPoint(1.0D, 0.0D, 0);
		Point localPoint3 = getPoint(0.0D, 1.0D, 0);
		Point localPoint4 = getPoint(1.0D, 1.0D, 0);
		
		//if (Calculations.isOnScreen(localPoint1) && Calculations.isOnScreen(localPoint2) 
		//		&& Calculations.isOnScreen(localPoint3) && Calculations.isOnScreen(localPoint4)) {
			
			final Polygon localPolygon = new Polygon();
			localPolygon.addPoint(localPoint1.x, localPoint1.y);
			localPolygon.addPoint(localPoint2.x, localPoint2.y);
			localPolygon.addPoint(localPoint4.x, localPoint4.y);
			localPolygon.addPoint(localPoint3.x, localPoint3.y);
			return localPolygon;
		//}
		
		//return null;
	}*/
	
	/*public void draw(java.awt.Graphics g) {
		Polygon bounds = getBounds();
		
		if (bounds != null) {
			/*for (int i = 0; i < bounds.npoints; i++) {
				if (bounds.xpoints[i] < 0)
					return;
				
				if (bounds.ypoints[i] < 0)
					return;
			}*//*
			
			g.drawPolygon(bounds);
		}
	}*/
	
	
	
	public int[][] projectVertices() {
		Render render = Client.getRender();
		
		//Viewport viewport = render.getClientViewport();
		
		float[] data = null;
		
		float[] callBackFloats = Viewport.getCallBackFloats();
		if (callBackFloats == null)
			data = render.getClientViewport().getFloats().clone();
		else
			data = callBackFloats.clone();
		
		
		int locX = (int) (getX() - (0.5 * 0x200));
		int locY = (int) (getY() - (0.5 * 0x200));
		int plane = Client.getPlane();
		
		int[] heights = new int[4];
		heights[0] = Calculations.tileHeight(locX, locY, plane);
		heights[1] = Calculations.tileHeight(locX + 0x200, locY , plane);
		heights[2] = Calculations.tileHeight(locX, locY + 0x200, plane);
		heights[3] = Calculations.tileHeight(locX + 0x200, locY + 0x200, plane);
		
		int[][] screen = new int[4][2];
	
		float checkOff = data[14];
		float xOff = data[12];
		float yOff = data[13];
		float zOff = data[15];
		float checkX = data[2];
		float checkY = data[6];
		float checkZ = data[10];
		float xX = data[0];
		float xY = data[4];
		float xZ = data[8];
		float yX = data[1];
		float yY = data[5];
		float yZ = data[9];
		float zX = data[3];
		float zY = data[7];
		float zZ = data[11];
		
		float render_absoluteX = render.getAbsoluteX();
		float render_absoluteY = render.getAbsoluteY();
		float render_xMultiplier = render.getXMultiplier();
		float render_yMultiplier = render.getYMultiplier();
		
		for (int index = 0; index < 4; index++) {
			int vertexX = locX;
			int vertexY = heights[index];
			int vertexZ = locY;
			
			switch (index) {
			case 0:
				break;
			case 1:
				vertexX += 0x200;
				break;
			case 2:
				vertexZ += 0x200;
				break;
			case 3:
				vertexX += 0x200;
				vertexZ += 0x200;
				break;
			}
			
			float _check = (checkOff + (checkX * vertexX + checkY * vertexY + checkZ * vertexZ));
			float _z = (zOff + (zX * vertexX + zY * vertexY + zZ * vertexZ));
			
			if (_check >= -_z) {
				float _x = (xOff + (xX * vertexX + xY * vertexY + xZ * vertexZ));
				float _y = (yOff + (yX * vertexX + yY * vertexY + yZ * vertexZ));
				
				//float fx = Math.round(render_absoluteX + render_xMultiplier * _x / _z);
				//float fy = Math.round(render_absoluteY + render_yMultiplier * _y / _z);
				
				if (_x >= -_z && _x <= _z && _y >= -_z && _y <= _z) {
					screen[index][0] = (int)(render_absoluteX + render_xMultiplier * _x / _z); // TODO: the multiplier hack might be because of my dpi settings
					screen[index][1] = (int)(render_absoluteY + render_yMultiplier * _y / _z);
				} else {
					screen[index][0] = -999999; // actually, in the rs client it is -999999.0F;
					//screen[index][1] = -1; // this is not set in the rs client
				}
			} else {
				screen[index][0] = -999999; // actually, in the rs client it is -999999.0F;
				//screen[index][1] = -1; // this is not set in the rs client
			}
		}
		
		return screen;
	}
	
	public static int[][][][] projectNeasrestTilesVertices() {
		Render render = Client.getRender();
		
		//Viewport viewport = render.getClientViewport();
		
		float[] data = null;
		
		float[] callBackFloats = Viewport.getCallBackFloats();
		if (callBackFloats == null)
			data = render.getClientViewport().getFloats().clone();
		else
			data = callBackFloats.clone();
		
		
		int locX = (int) (Client.getMyPlayer().getData().getCenterLocation().getX() - (0.5 * 0x200) - (20 * 0x200));
		int locY = (int) (Client.getMyPlayer().getData().getCenterLocation().getY() - (0.5 * 0x200) - (20 * 0x200));
		int plane = Client.getPlane();
		
		
		byte[][][] settings = Client.getGameInfo().getGroundBytes().getBytes();
		
		if (settings == null)
			return null;
		
		Array1D<TileData> planes = Client.getGameInfo().getGroundInfo().getTileData();
		
		if (planes == null)
			return null;
		
		int[][] originalHeights = null;
		int[][] heightsFixed = null;
		int planeFixed = plane;
		
		int relX = locX >> 9;
		int relY = locY >> 9;
		
		if ( relX >= 0 && relX < 104 && relY >= 0 && relY < 104) {
			if (planeFixed <= 3 && (settings[1][relX][relY] & 2) != 0)
				++planeFixed;
			
			int planesLength = planes.getLength();
			
			if (planeFixed < planesLength) {
				TileData planeData = planes.get(planeFixed);
				
				if (planeData != null)
					heightsFixed = planeData.getHeights();
			}
			
			if (plane < planesLength) {
				TileData planeData = planes.get(plane);
				
				if (planeData != null)
					originalHeights = planeData.getHeights();
			}
		} else
			return null;
		
		if (heightsFixed == null && originalHeights == null)
			return null;
		
		int[][][][] screen = new int[2][40][40][2];
		
		float checkOff = data[14];
		float xOff = data[12];
		float yOff = data[13];
		float zOff = data[15];
		float checkX = data[2];
		float checkY = data[6];
		float checkZ = data[10];
		float xX = data[0];
		float xY = data[4];
		float xZ = data[8];
		float yX = data[1];
		float yY = data[5];
		float yZ = data[9];
		float zX = data[3];
		float zY = data[7];
		float zZ = data[11];
		
		float render_absoluteX = render.getAbsoluteX();
		float render_absoluteY = render.getAbsoluteY();
		float render_xMultiplier = render.getXMultiplier();
		float render_yMultiplier = render.getYMultiplier();
		
		for (int axis = 0; axis < 2; axis++) {
			for (int index1 = 0; index1 < 40; index1++) {
				for (int index2 = 0; index2 < 40; index2++) {
					int curX;
					int curY;
					
					if (axis == 0) {
						curX = locX + index1*0x200;
						curY = locY + index2*0x200;
					} else {
						curX = locX + index2*0x200;
						curY = locY + index1*0x200;
					}
					
					//int height = Calculations.tileHeight(curX, curY, plane);
					int height = 0;
					
					// calc height
					try {
						int x1 = curX >> 9;
						int y1 = curY >> 9;
						
						if ( x1 >= 0 && x1 < 104 && y1 >= 0 && y1 < 104) {
							int[][] heights = null;
							if (plane <= 3 && (settings[1][x1][y1] & 2) != 0) {
								++plane;
								heights = heightsFixed;
							} else
								heights = originalHeights;

							int x2 = curX & 0x200 - 1;
							int y2 = curY & 0x200 - 1;
							int start_h = heights[x1][y1] * (0x200 - x2) + heights[x1 + 1][y1] * x2 >> 9;
							int end_h = heights[x1][1 + y1] * (0x200 - x2) + heights[x1 + 1][y1 + 1] * x2 >> 9;
				
							//return start_h * (512 - y2) + end_h * y2 >> 9;
							height = start_h * (512 - y2) + end_h * y2 >> 9;
						}
					} catch(Exception e){ 
						e.printStackTrace();
						
						screen[axis][index1][index2][0] = -999999;
						continue;
					}
					
					int vertexX = curX;
					int vertexY = height;
					int vertexZ = curY;
					
					float _check = (checkOff + (checkX * vertexX + checkY * vertexY + checkZ * vertexZ));
					float _z = (zOff + (zX * vertexX + zY * vertexY + zZ * vertexZ));
					
					if (_check >= -_z) {
						float _x = (xOff + (xX * vertexX + xY * vertexY + xZ * vertexZ));
						float _y = (yOff + (yX * vertexX + yY * vertexY + yZ * vertexZ));
						
						//float fx = Math.round(render_absoluteX + render_xMultiplier * _x / _z);
						//float fy = Math.round(render_absoluteY + render_yMultiplier * _y / _z);
						
						if (_x >= -_z && _x <= _z && _y >= -_z && _y <= _z) {
							screen[axis][index1][index2][0] = (int)(render_absoluteX + render_xMultiplier * _x / _z); // TODO: the multiplier hack might be because of my dpi settings
							screen[axis][index1][index2][1] = (int)(render_absoluteY + render_yMultiplier * _y / _z);
						} else {
							screen[axis][index1][index2][0] = -999999; // actually, in the rs client it is -999999.0F;
							//screen[axis][index1][index2][1] = -1; // this is not set in the rs client
						}
					} else {
						screen[axis][index1][index2][0] = -999999; // actually, in the rs client it is -999999.0F;
						//screen[axis][index1][index2][1] = -1; // this is not set in the rs client
					}
				}
			}
		}
		
		return screen;
	}
	
	public void draw(final Graphics g) {
		final int[][] screen = projectVertices();
		
		if (screen[0][0] != -999999
				&& screen[1][0] != -999999
				&& screen[2][0] != -999999
				&& screen[3][0] != -999999) {
			
			int point1X = screen[0][0];
			int point1Y = screen[0][1];
			int point2X = screen[1][0];
			int point2Y = screen[1][1];
			int point3X = screen[2][0];
			int point3Y = screen[2][1];
			int point4X = screen[3][0];
			int point4Y = screen[3][1];
			
			g.drawLine(point1X, point1Y, point2X, point2Y);
			g.drawLine(point2X, point2Y, point4X, point4Y);
			g.drawLine(point4X, point4Y, point3X, point3Y);
			g.drawLine(point3X, point3Y, point1X, point1Y);
		}
	}
	
	public void render(GL2 gl2) {
		final int[][] screen = projectVertices();
		
		if (screen[0][0] != -999999
				&& screen[1][0] != -999999
				&& screen[2][0] != -999999
				&& screen[3][0] != -999999) {
			
			int point1X = screen[0][0];
			int point1Y = screen[0][1];
			int point2X = screen[1][0];
			int point2Y = screen[1][1];
			int point3X = screen[2][0];
			int point3Y = screen[2][1];
			int point4X = screen[3][0];
			int point4Y = screen[3][1];
			
	        //gl2.glLoadIdentity();
	        gl2.glBegin( GL.GL_LINE_LOOP );
	        gl2.glColor3f( 0, 1, 0 );
	        gl2.glVertex2f( point1X, point1Y );
	        gl2.glVertex2f( point2X, point2Y );
	        gl2.glVertex2f( point4X, point4Y );
	        gl2.glVertex2f( point3X, point3Y );
	        
	        gl2.glFlush();
	        gl2.glEnd();
		}
	}
	
	public static void drawNearestTiles(final Graphics g) {
		final int[][][][] screen = projectNeasrestTilesVertices();
		
		if (screen == null)
			return;
		
		for (int axis = 0; axis < 2; axis++) {
			for (int index1 = 0; index1 < 40; index1++) {
				for (int index2 = 0; index2 < 40; index2++) {
					if (index2 == 40 - 1)
						break;
					
					if (screen[axis][index1][index2][0] != -999999
							&& screen[axis][index1][index2 + 1][0] != -999999) {
						
						int point1X = screen[axis][index1][index2][0];
						int point1Y = screen[axis][index1][index2][1];
						int point2X = screen[axis][index1][index2 + 1][0];
						int point2Y = screen[axis][index1][index2 + 1][1];
		
						g.drawLine(point1X, point1Y, point2X, point2Y);
					}
				}
			}
		}
	}
	
	public static void renderNearestTiles(GL2 gl2) {
		final int[][][][] screen = projectNeasrestTilesVertices();
		
		if (screen == null)
			return;
		
        //gl2.glLoadIdentity();
        gl2.glBegin( GL.GL_LINES );
        gl2.glColor3f( 0, 1, 0 );
		
		for (int axis = 0; axis < 2; axis++) {
			for (int index1 = 0; index1 < 40; index1++) {
				for (int index2 = 0; index2 < 40; index2++) {
					if (index2 == 40 - 1)
						break;
					
					if (screen[axis][index1][index2][0] != -999999
							&& screen[axis][index1][index2 + 1][0] != -999999) {
						
						int point1X = screen[axis][index1][index2][0];
						int point1Y = screen[axis][index1][index2][1];
						int point2X = screen[axis][index1][index2 + 1][0];
						int point2Y = screen[axis][index1][index2 + 1][1];
		

				        gl2.glVertex2f( point1X, point1Y );
				        gl2.glVertex2f( point2X, point2Y );
					}
				}
			}
		}
		
        gl2.glFlush();
        gl2.glEnd();
	}
}
