package org.rs3.api.wrappers;

import org.rs3.accessors.I_CameraLocationData;
import org.rs3.api.objects.PreciseTile;

public class CameraLocationData extends AbstractCameraLocationData {

	protected CameraLocationData(Object obj) {
		super(obj);
	}

	public static CameraLocationData create(Object obj) {
		return create(CameraLocationData.class, obj);
	}


	public Location getPoint1() {
		return Location.create(I_CameraLocationData.getPoint1(obj));
	}

	public Location getPoint2() {
		return Location.create(I_CameraLocationData.getPoint2(obj));
	}

	public Quaternion getQuaternion() {
		return Quaternion.create(I_CameraLocationData.getQuaternion(obj));
	}

	public PreciseTile getCameraLocation() {
		Location point1 = getPoint1();
		Location point2 = getPoint2();

		//System.out.println(point1.getX() + ", " + point1.getY() + ", " + point1.getHeight());
		//System.out.println(point2.getX() + ", " + point2.getY() + ", " + point2.getHeight());
		//System.out.println(getQuaternion().getX() + ", " + getQuaternion().getY() + ", " + getQuaternion().getZ() + ", " + getQuaternion().getA());

		Quaternion camLoc1 = new Quaternion(point1.getX(), point1.getHeight(), point1.getY(), 0.0F);
		Quaternion camLoc = getQuaternion();
		Quaternion camLoc2 = new Quaternion(-camLoc.x, -camLoc.y, -camLoc.z, camLoc.a);

		float k = (camLoc1.a * camLoc2.x + camLoc1.x * camLoc2.a + camLoc1.y * camLoc2.z) - camLoc1.z * camLoc2.y;
		float f = (camLoc1.a * camLoc2.y - camLoc1.x * camLoc2.z) + camLoc1.y * camLoc2.a + camLoc1.z * camLoc2.x;
		float n = ((camLoc1.a * camLoc2.z + camLoc1.x * camLoc2.y) - camLoc1.y * camLoc2.x) + camLoc1.z * camLoc2.a;
		float t = camLoc1.a * camLoc2.a - camLoc1.x * camLoc2.x - camLoc1.y * camLoc2.y - camLoc1.z * camLoc2.z;

		Quaternion camLoc3 = new Quaternion(k, f, n, t);

		float x = (camLoc.a * camLoc3.x + camLoc.x * camLoc3.a + camLoc.y * camLoc3.z) - camLoc.z * camLoc3.y;
		float y = (camLoc.a * camLoc3.y - camLoc.x * camLoc3.z) + camLoc.y * camLoc3.a + camLoc.z * camLoc3.x;
		float z = ((camLoc.a * camLoc3.z + camLoc.x * camLoc3.y) - camLoc.y * camLoc3.x) + camLoc.z * camLoc3.a;

		return new PreciseTile(x + point2.getX(), z + point2.getY(), y + point2.getHeight(), Client.getPlane());
	}
}
