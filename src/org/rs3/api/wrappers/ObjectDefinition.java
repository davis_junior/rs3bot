package org.rs3.api.wrappers;

import org.rs3.accessors.I_ObjectDefinition;


public class ObjectDefinition extends AccessorWrapper {
	
	protected ObjectDefinition(Object obj) {
		super(obj);
	}
	
	public static ObjectDefinition create(Object obj) {
		return create(ObjectDefinition.class, obj);
	}
	
	
	public String getName() {
		return I_ObjectDefinition.getName(obj);
	}
	
	public String[] getActions() {
		return I_ObjectDefinition.getActions(obj);
	}
	
	/*public String getName() {
		return I_ObjectDefinition.getName(obj);
	}*/
	
	public int getId() {
		return I_ObjectDefinition.getId(obj);
	}
	
	public AbstractDefLoader getDefinitionLoader() {
		return AbstractDefLoader.create(I_ObjectDefinition.getDefinitionLoader(obj));
	}
	
	public ObjectCacheLoader getCacheLoader() {
		return ObjectCacheLoader.create(I_ObjectDefinition.getCacheLoader(obj));
	}
	
	public byte[] getModelCache2byteArray() {
		return I_ObjectDefinition.getModelCache2byteArray(obj);
	}
	
	public int[][] getModelCache2intArray2D() {
		return I_ObjectDefinition.getModelCache2intArray2D(obj);
	}
}
