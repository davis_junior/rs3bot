package org.rs3.api.wrappers;

import org.rs3.accessors.I_WorldInfo;

public class WorldInfo extends AccessorWrapper {
	
	protected WorldInfo(Object obj) {
		super(obj);
	}
	
	public static WorldInfo create(Object obj) {
		return create(WorldInfo.class, obj);
	}
	
	
	public String getHost() {
		return I_WorldInfo.getHost(obj);
	}
	
	public int getWorld() {
		return I_WorldInfo.getWorld(obj);
	}
	
	public int getPort1() {
		return I_WorldInfo.getPort1(obj);
	}
	
	public int getPort2() {
		return I_WorldInfo.getPort2(obj);
	}
}
