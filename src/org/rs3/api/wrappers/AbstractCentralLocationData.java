package org.rs3.api.wrappers;

public abstract class AbstractCentralLocationData extends AccessorWrapper {
	
	protected AbstractCentralLocationData(Object obj) {
		super(obj);
	}
	
	public static AbstractCentralLocationData create(Object obj) {
		return create(AbstractCentralLocationData.class, obj);
	}
}
