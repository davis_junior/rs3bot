package org.rs3.api.wrappers;

import org.rs3.accessors.I_SkillData;


public class SkillData extends AccessorWrapper {
	
	protected SkillData(Object obj) {
		super(obj);
	}
	
	public static SkillData create(Object obj) {
		return create(SkillData.class, obj);
	}
	
	
	public int getExperience() {
		return I_SkillData.getExperience(obj);
	}
	
	public int getRealLevel() {
		return I_SkillData.getRealLevel(obj);
	}
	
	public int getCurrentLevel() {
		return I_SkillData.getCurrentLevel(obj);
	}
}
