package org.rs3.api.wrappers;

import org.rs3.accessors.I_Npc;
import org.rs3.callbacks.capture.ModelCapture;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Npc extends Character {
	
	protected Npc(Object obj) {
		super(obj);
	}
	
	public static Npc create(Object obj) {
		return create(Npc.class, obj);
	}
	
	
	public String getTitle() {
		return I_Npc.getTitle(obj);
	}
	
	public NpcDefinition getNpcDefinition() {
		return NpcDefinition.create(I_Npc.getNpcDefinition(obj));
	}
	
	
	public Model captureModel() {
		return captureModel(true);
	}
	
	/**
	 * Uses model callback to grab model. Adds itself to the npc model hashset and waits for a non-null model.
	 * 
	 * @param removeAfterCapture if true (default), will remove from model capture list. if false, will still capture the model but will stay in the capture list.
	 * @return
	 */
	public Model captureModel(boolean removeAfterCapture) {
		this.curModel = null;
		synchronized (ModelCapture.npcs) {
			ModelCapture.npcs.add(this);
		}
		
		Timer timer = new Timer(1000);
		while (timer.isRunning()) {
			if (this.curModel != null)
				break;
			
			Time.sleep(50);
		}
		
		if (removeAfterCapture) {
			synchronized (ModelCapture.npcs) {
				ModelCapture.npcs.remove(this);
			}
		}
		
		synchronized (this) {
			return this.curModel;
		}
	}
	
	
	/**
	 * Make sure to add npc to ModelCapture first, otherwise use captureModel() instead.
	 */
	@Override
	public Model getModel() {
		return super.getModel();
		
		//return curModel;
	}
	
	@Override
	public boolean loopInteract(Condition breakCondition, String action) {
		captureModel(); // make sure model is updated (since the super method uses getModel())
		boolean result = super.loopInteract(true, breakCondition, action, null, false); // makes sure to keep updating model also
		ModelCapture.npcs.remove(this);
		return result;
	}
	
	@Override
	public boolean loopInteract(Condition breakCondition, String action, String option, boolean breakOnWrongOption) {
		captureModel(); // make sure model is updated (since the super method uses getModel())
		boolean result = super.loopInteract(true, breakCondition, action, option, breakOnWrongOption); // makes sure to keep updating model also
		ModelCapture.npcs.remove(this);
		return result;
	}
	
	@Override
	public boolean loopInteract(boolean keepModelUpdated, Condition breakCondition, String action, String option, boolean breakOnWrongOption) {
		captureModel(); // make sure model is updated (since the super method uses getModel())
		boolean result = super.loopInteract(keepModelUpdated, breakCondition, action, option, breakOnWrongOption); // makes sure to keep updating model also
		ModelCapture.npcs.remove(this);
		return result;
	}
}
