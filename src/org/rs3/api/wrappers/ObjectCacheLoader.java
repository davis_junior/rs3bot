package org.rs3.api.wrappers;

import org.rs3.accessors.I_ObjectCacheLoader;


public class ObjectCacheLoader extends AccessorWrapper {
	
	protected ObjectCacheLoader(Object obj) {
		super(obj);
	}
	
	public static ObjectCacheLoader create(Object obj) {
		return create(ObjectCacheLoader.class, obj);
	}
	
	
	public Cache getAnimatedModelCache() {
		return Cache.create(I_ObjectCacheLoader.getAnimatedModelCache(obj));
	}
	
	public Cache getModelCache() {
		return Cache.create(I_ObjectCacheLoader.getModelCache(obj));
	}
	
	public Cache getCompositeCache() {
		return Cache.create(I_ObjectCacheLoader.getCompositeCache(obj));
	}
}
