package org.rs3.api.wrappers;

import org.rs3.accessors.I_Animation;


public class Animation extends AccessorWrapper {
	
	protected Animation(Object obj) {
		super(obj);
	}
	
	public static Animation create(Object obj) {
		return create(Animation.class, obj);
	}
	
	
	public int getId() {
		return I_Animation.getId(obj);
	}
}
