package org.rs3.api.wrappers;

import org.rs3.accessors.I_ItemCacheLoader;


public class ItemCacheLoader extends AccessorWrapper {
	
	protected ItemCacheLoader(Object obj) {
		super(obj);
	}
	
	public static ItemCacheLoader create(Object obj) {
		return create(ItemCacheLoader.class, obj);
	}
	
	
	public Cache getModelCache() {
		return Cache.create(I_ItemCacheLoader.getModelCache(obj));
	}
}
