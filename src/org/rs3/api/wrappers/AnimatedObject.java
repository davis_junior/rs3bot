package org.rs3.api.wrappers;

import java.awt.Graphics;

import javax.media.opengl.GL2;

import org.rs3.accessors.I_AnimatedObject;
import org.rs3.accessors.Reflection;
import org.rs3.api.Nodes;

public class AnimatedObject extends AccessorWrapper {
	
	protected AnimatedObject(Object obj) {
		super(obj);
	}
	
	public static AnimatedObject create(Object obj) {
		return create(AnimatedObject.class, obj);
	}
	
	
	public int getId() {
		return I_AnimatedObject.getId(obj);
	}
	
	public Model getModel_field() {
		Render render = Client.getRender();
		
		if (render instanceof SDRender)
			return SDModel.create(I_AnimatedObject.getModel(obj), 0, 0);
		else if (render instanceof OpenGLRender)
			return OpenGLModel.create(I_AnimatedObject.getModel(obj), 0, 0);
		else
			return Model.create(I_AnimatedObject.getModel(obj), 0, 0);
	}
	
	public ObjectDefLoader getDefinitionLoader() {
		return ObjectDefLoader.create(I_AnimatedObject.getDefinitionLoader(obj));
	}
	
	public Interactable getInteractable() {
		return Interactable.create(I_AnimatedObject.getInteractable(obj));
	}
	
	public Animator getAnimator1() {
		return Animator.create(I_AnimatedObject.getAnimator1(obj));
	}
	
	public Animator getAnimator2() {
		return Animator.create(I_AnimatedObject.getAnimator2(obj));
	}
	
	
	public ObjectDefinition getDefinition() {
		try {
			Node ref = Nodes.lookup(getDefinitionLoader().getDefinitionCache().getHashTable(), getId());
			
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null)
					return ObjectDefinition.create(def);
			
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null)
					return ObjectDefinition.create(def);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int getAnimationId() {
		return I_AnimatedObject.getAnimationId(obj);
	}
	
	public int getFace() {
		return I_AnimatedObject.getFace(obj);
	}
	
	public IdComposite_AnimatedObject getIdComposite() {
		return IdComposite_AnimatedObject.create(I_AnimatedObject.getIdComposite(obj));
	}
	
	public Model getCachedAnimatedModel() {
		try {
			ObjectDefinition definition = getDefinition();
			if (definition == null)
				return null;
			
			long face = getFace();
			long animationId = getAnimationId();
			
			if (animationId == 11L) {
				animationId = 10L;
				face += 4L;
			}
			
			//Object ul = Reflection.getStaticDeclaredField("ul", "e");
			//int h = (int) Reflection.getDeclaredField("ul", ul, "h", int.class);
			//System.out.println(h * 481840829);
			//animationId = 4L;
			
			long id = (definition.getId() << 10) + (animationId << 3) + face;
			id |= Client.getRender().getCacheIndex() << 29;
			
			if (getIdComposite() != null)
				id |= getIdComposite().getId() << 32;
			
			
			Node ref = Nodes.lookup(definition.getCacheLoader().getAnimatedModelCache().getHashTable(), id);
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, 0, 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, 0, 0);
					} else {
						return Model.create(def, 0, 0);
					}
				}
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, 0, 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, 0, 0);
					} else {
						return Model.create(def, 0, 0);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Model getCachedModel() {
		ObjectComposite comp = getComposite();
		
		if (comp == null)
			return null;
		
		return comp.getModel();
	}
	
	// TODO: haven't got a non-null return yet; needs verified maybe on other objects
	public ObjectComposite getComposite() {
		try {
			ObjectDefinition definition = getDefinition();
			
			if (definition == null)
				return null;
			
			long face = getFace();
			long animationId = getAnimationId();
			
			if (animationId == 11L) {
				animationId = 10L;
				face += 4L;
			}
			
			long id = (definition.getId() << 10) + (animationId << 3) + face;
			id |= Client.getRender().getCacheIndex() << 29;
			
			if (getIdComposite() != null)
				id |= getIdComposite().getId() << 32;
			
			Node ref = Nodes.lookup(definition.getCacheLoader().getCompositeCache().getHashTable(), id);
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null)
					return ObjectComposite.create(def);
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null)
					return ObjectComposite.create(def);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Model getModel() {
		return getCachedAnimatedModel();
		
		/*Render render = Client.getRender();
		
		if (render instanceof SDRender)
			return getModel_field();
		else if (render instanceof OpenGLRender)
			return getCachedAnimatedModel();
		else
			return getCachedAnimatedModel();*/
	}
	
	public void draw(Graphics g, Location location, int plane) {
		Model model = getModel();
		
		if (model != null)
			model.draw(g, location.getX(), location.getY(), plane);
	}
	
	public void render(GL2 gl2, Location location, int plane) {
		Model model = getModel();
		
		if (model != null)
			model.render(gl2, location.getX(), location.getY(), plane);
	}
}
