package org.rs3.api.wrappers;

public abstract class AbstractWallObject extends Interactable {
	
	protected AbstractWallObject(Object obj) {
		super(obj);
	}
	
	public static AbstractWallObject create(Object obj) {
		return create(AbstractWallObject.class, obj);
	}
}
