package org.rs3.api.wrappers;

import org.rs3.accessors.I_Quaternion;


public class Quaternion extends AccessorWrapper {
	
	private boolean hasIAccessor;
	protected float x, y, z, a; // cached when initialized, or used without iAccessor
	
	protected Quaternion(Object obj) {
		super(obj);
		hasIAccessor = true;
		this.x = getX();
		this.y = getY();
		this.z = getZ();
		this.a = getA();
	}
	
	public static Quaternion create(Object obj) {
		return create(Quaternion.class, obj);
	}
	
	
	public Quaternion(float x, float y, float z, float a) {
		super(null);
		hasIAccessor = false;
		this.x = x;
		this.y = y;
		this.z = z;
		this.a = a;
	}
	
	public float getX() {
		if (hasIAccessor)
			return I_Quaternion.getX(obj);
		else
			return x;
	}
	
	public float getY() {
		if (hasIAccessor)
			return I_Quaternion.getY(obj);
		else
			return y;
	}
	
	public float getZ() {
		if (hasIAccessor)
			return I_Quaternion.getZ(obj);
		else
			return z;
	}
	
	public float getA() {
		if (hasIAccessor)
			return I_Quaternion.getA(obj);
		else
			return a;
	}
}
