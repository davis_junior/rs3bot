package org.rs3.api.wrappers;

public class ObjectDefLoader extends DefLoader {
	
	protected ObjectDefLoader(Object obj) {
		super(obj);
	}
	
	public static ObjectDefLoader create(Object obj) {
		return create(ObjectDefLoader.class, obj);
	}
}
