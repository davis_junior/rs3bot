package org.rs3.api.wrappers;

import org.rs3.accessors.I_EntityNode;
import org.rs3.api.objects.BasicInteractable;
import org.rs3.api.objects.PreciseTile;
import org.rs3.api.objects.Tile;


public class EntityNode extends AccessorWrapper {
	
	protected EntityNode(Object obj) {
		super(obj);
	}
	
	public static EntityNode create(Object obj) {
		return create(EntityNode.class, obj);
	}
	
	
	public EntityNode getPrevious() {
		return create(I_EntityNode.getPrevious(obj));
	}
	
	public EntityNode getNext() {
		return create(I_EntityNode.getNext(obj));
	}
	
	public InteractableData getData() {
		return InteractableData.create(I_EntityNode.getData(obj));
	}
	
	public boolean isDisposed() {
		return I_EntityNode.isDisposed(obj);
	}
	
	
	/**
	 * <br><br>
	 * <i>convenience method</i>
	 * 
	 * @param cached
	 * @return
	 */
	public Location getLocation() {
		return getData().getCenterLocation();
	}
	
	/**
	 * Includes base.
	 * <br><br>
	 * <i>convenience method</i>
	 * 
	 * @param cached
	 * @return
	 */
	public Tile getTile() {
		return getLocation().getGlobalTile(false);
	}
	
	/**
	 * Includes base.
	 * <br><br>
	 * <i>convenience method</i>
	 * 
	 * @param cached
	 * @return
	 */
	public Tile getTile(boolean cached) {
		return getLocation().getGlobalTile(cached);
	}
	
	/**
	 * Does not include base.
	 * <br><br>
	 * <i>convenience method</i>
	 * 
	 * @param cached
	 * @return
	 */
	public PreciseTile getInternalTile() {
		return getLocation().getLocalPreciseTile(false);
	}
	
	/**
	 * Does not include base.
	 * <br><br>
	 * <i>convenience method</i>
	 * 
	 * @param cached
	 * @return
	 */
	public PreciseTile getInternalTile(boolean cached) {
		return getLocation().getLocalPreciseTile(cached);
	}
}
