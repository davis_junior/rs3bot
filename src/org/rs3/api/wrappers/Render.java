package org.rs3.api.wrappers;

import org.rs3.accessors.I_Render;


public abstract class Render extends AccessorWrapper {
	
	protected Render(Object obj) {
		super(obj);
	}
	
	public static Render create(Object obj) {
		return create(Render.class, obj);
	}
	
	
	public abstract float getXMultiplier();
	public abstract float getYMultiplier();
	
	public abstract float getAbsoluteX();
	public abstract float getAbsoluteY();
	
	public abstract Viewport getClientViewport();
	
	public int getIndex() {
		return I_Render.getIndex(obj);
	}
	
	public int getCacheIndex() {
		return I_Render.getCacheIndex(obj);
	}
}
