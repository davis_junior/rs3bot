package org.rs3.api.wrappers;

import org.rs3.accessors.I_InteractableData;


public class InteractableData extends AccessorWrapper {
	
	protected InteractableData(Object obj) {
		super(obj);
	}
	
	public static InteractableData create(Object obj) {
		return create(InteractableData.class, obj);
	}
	

	public Location getCenterLocation() {
		return Location.create(I_InteractableData.getCenterLocation(obj));
	}
}
