package org.rs3.api.wrappers;

import org.rs3.accessors.I_MessageData;


public class MessageData extends AccessorWrapper {
	
	protected MessageData(Object obj) {
		super(obj);
	}
	
	public static MessageData create(Object obj) {
		return create(MessageData.class, obj);
	}
	
	
	public String getMessage() {
		return I_MessageData.getMessage(obj);
	}
}
