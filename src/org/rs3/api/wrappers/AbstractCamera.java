package org.rs3.api.wrappers;

import org.rs3.accessors.I_AbstractCamera;
import org.rs3.api.objects.PreciseTile;

public abstract class AbstractCamera extends AccessorWrapper {
	
	protected AbstractCamera(Object obj) {
		super(obj);
	}
	
	public static AbstractCamera create(Object obj) {
		return create(AbstractCamera.class, obj);
	}
	
	
	public CameraLocationData getCameraLocationData() {
		return CameraLocationData.create(I_AbstractCamera.getCameraLocationData(obj));
	}
	
	public CentralLocationData getCentralLocationData() {
		return CentralLocationData.create(I_AbstractCamera.getCentralLocationData(obj));
	}
	
	/**
	 * Calculates minimap angle.
	 * @return float where 0.0 is 0 degrees and 6.0 is 360 degrees
	 */
	// referenced to own - 836 - ik.am(int)
	public float getMinimapAngle() {
		CameraLocationData cameraLocData = this.getCameraLocationData();
		CentralLocationData centralLocData = this.getCentralLocationData();
		
		if (cameraLocData != null && centralLocData != null) {
			/*Location point1 = cameraLocData.getPoint1();
			Location point2 = centralLocData.getPoint2();
			
			float result = 0f;
			if (point1 != null && point2 != null) {
				PreciseTile newLoc = new PreciseTile(point1.getX(), point1.getY(), point1.getHeight(), -1);
				newLoc.x -= point2.getX();
				newLoc.y -= point2.getY();
				//newLoc.height -= point2.getHeight(); // unnecessary
				
				newLoc.height = 0f;
				
				result = (float) Math.atan2(newLoc.getX(), newLoc.getY());
			}*/
			
			PreciseTile point1 = cameraLocData.getCameraLocation();
			PreciseTile point2 = centralLocData.getCentralLocation();
			
			float result = 0f;
			if (point1 != null && point2 != null) {
				//System.out.println(point1.getX() + ", " + point1.getY() + ",   " + point2.getX() + ", " + point2.getY());
				
				PreciseTile newLoc = new PreciseTile(point1.getX(), point1.getY(), point1.getHeight(), -1);
				newLoc.x -= point2.getX();
				newLoc.y -= point2.getY();
				//newLoc.height -= point2.getHeight(); // unnecessary
				
				newLoc.height = 0f;
				
				result = (float) Math.atan2(newLoc.getX(), newLoc.getY());
			}
			
			return (float) (3.141592653589793D - result);
		}
		
		return 0f;
	}
}
