package org.rs3.api.wrappers;

public class Array2D<T extends AccessorWrapper> {
	
	private Class<T> clazz;
	private Object[][] obj;
	
	private Array2D(Class<T> clazz, Object[][] obj) {
		this.clazz = clazz;
		this.obj = obj;
	}
	
	public static <T extends AccessorWrapper> Array2D<T> create(Class<T> clazz, Object[][] obj) {
		if (obj != null)
			return new Array2D<T>(clazz, obj);
		
		return null;
	}
	
	public Object[][] getObject() {
		return obj;
	}
	
	public int getLength() {
		return obj.length;
	}
	
	
	/**
	 * Creates an AccessorWrapper, wrapping the object at the specified indices.
	 * 
	 * @param d1index Dimension 1 index in internal array.
	 * @param d2index Dimension 2 index in internal array.
	 * @return new AccessorWrapper at indices.
	 */
	public T get(int d1index, int d2index) {
		return AccessorWrapper.create(clazz, obj[d1index][d2index]);
	}
	
	/**
	 * Creates an Array1D wrapper, wrapping the one-dimensional array object at the specified index.
	 * 
	 * @param d1index Dimension 1 index in internal array.
	 * @return new Array1D at index.
	 */
	public Array1D<T> get1D(int d1index) {
		return Array1D.create(clazz, obj[d1index]);
	}
	
	public T[][] getAll() {
		return WrapperCalculations.getArray2D(clazz, obj);
	}
}
