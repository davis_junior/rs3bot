package org.rs3.api.wrappers;

public class Array3D<T extends AccessorWrapper> {
	
	private Class<T> clazz;
	private Object[][][] obj;
	
	private Array3D(Class<T> clazz, Object[][][] obj) {
		this.clazz = clazz;
		this.obj = obj;
	}
	
	public static <T extends AccessorWrapper> Array3D<T> create(Class<T> clazz, Object[][][] obj) {
		if (obj != null)
			return new Array3D<T>(clazz, obj);
		
		return null;
	}
	
	public Object[][][] getObject() {
		return obj;
	}
	
	public int getLength() {
		return obj.length;
	}
	
	
	/**
	 * Creates an AccessorWrapper, wrapping the object at the specified indices.
	 * 
	 * @param d1index Dimension 1 index in internal array.
	 * @param d2index Dimension 2 index in internal array.
	 * @param d3index Dimension 3 index in internal array.
	 * @return new AccessorWrapper at indices.
	 */
	public T get(int d1index, int d2index, int d3index) {
		return AccessorWrapper.create(clazz, obj[d1index][d2index][d3index]);
	}
	
	/**
	 * Creates an Array2D wrapper, wrapping the two-dimensional array object at the specified index.
	 * 
	 * @param d1index Dimension 1 index in internal array.
	 * @return new Array2D at index.
	 */
	public Array2D<T> get2D(int d1index) {
		return Array2D.create(clazz, obj[d1index]);
	}
	
	/**
	 * Creates an Array1D wrapper, wrapping the one-dimensional array object at the specified indices.
	 * 
	 * @param d1index Dimension 1 index in internal array.
	 * @param d2index Dimension 2 index in internal array.
	 * @return new Array1D at indices.
	 */
	public Array1D<T> get1D(int d1index, int d2index) {
		return Array1D.create(clazz, obj[d1index][d2index]);
	}
	
	public T[][][] getAll() {
		return WrapperCalculations.getArray3D(clazz, obj);
	}
}
