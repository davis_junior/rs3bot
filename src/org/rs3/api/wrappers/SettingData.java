package org.rs3.api.wrappers;

import org.rs3.accessors.I_SettingData;


public class SettingData extends AccessorWrapper {
	
	protected SettingData(Object obj) {
		super(obj);
	}
	
	public static SettingData create(Object obj) {
		return create(SettingData.class, obj);
	}
	
	
	public Settings getSettings() {
		return Settings.create(I_SettingData.getSettings(obj));
	}
	
	public Array1D<SkillData> getSkillArray() {
		return Array1D.create(SkillData.class, I_SettingData.getSkillArray(obj));
	}
}
