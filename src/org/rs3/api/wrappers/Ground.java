package org.rs3.api.wrappers;

import org.rs3.accessors.I_Ground;


public class Ground extends AccessorWrapper {
	
	protected Ground(Object obj) {
		super(obj);
	}
	
	public static Ground create(Object obj) {
		return create(Ground.class, obj);
	}
	
	
	public byte getPlane() {
		return I_Ground.getPlane(obj);
	}
	
	public AnimableNode getAnimableList() {
		return AnimableNode.create(I_Ground.getAnimableList(obj));
	}
	
	public AbstractBoundary getBoundary1() {
		Object boundaryObj = I_Ground.getBoundary1(obj);
		
		if (boundaryObj != null) {
			if (boundaryObj.getClass().getName().equals(data.BoundaryObject.get.getInternalName()))
				return BoundaryObject.create(boundaryObj);
			else if (boundaryObj.getClass().getName().equals(data.AnimatedBoundaryObject.get.getInternalName()))
				return AnimatedBoundaryObject.create(boundaryObj);
			else
				return AbstractBoundary.create(boundaryObj);
		}
		
		return null;
	}
	
	public AbstractBoundary getBoundary2() {
		Object boundaryObj = I_Ground.getBoundary2(obj);
		
		if (boundaryObj != null) {
			if (boundaryObj.getClass().getName().equals(data.BoundaryObject.get.getInternalName()))
				return BoundaryObject.create(boundaryObj);
			else if (boundaryObj.getClass().getName().equals(data.AnimatedBoundaryObject.get.getInternalName()))
				return AnimatedBoundaryObject.create(boundaryObj);
			else
				return AbstractBoundary.create(boundaryObj);
		}
		
		return null;
	}
	
	public AbstractWallObject getWallDecoration1() {
		Object wallObj = I_Ground.getWallDecoration1(obj);
		
		if (wallObj != null) {
			if (wallObj.getClass().getName().equals(data.WallObject.get.getInternalName()))
				return WallObject.create(wallObj);
			else if (wallObj.getClass().getName().equals(data.AnimatedWallObject.get.getInternalName()))
				return AnimatedWallObject.create(wallObj);
			else
				return AbstractWallObject.create(wallObj);
		}
		
		return null;
	}
	
	public AbstractWallObject getWallDecoration2() {
		Object wallObj = I_Ground.getWallDecoration2(obj);
		
		if (wallObj != null) {
			if (wallObj.getClass().getName().equals(data.WallObject.get.getInternalName()))
				return WallObject.create(wallObj);
			else if (wallObj.getClass().getName().equals(data.AnimatedWallObject.get.getInternalName()))
				return AnimatedWallObject.create(wallObj);
			else
				return AbstractWallObject.create(wallObj);
		}
		
		return null;
	}
	
	public AbstractFloorObject getFloorDecoration() {
		Object floorObj = I_Ground.getFloorDecoration(obj);
		
		if (floorObj != null) {
			if (floorObj.getClass().getName().equals(data.FloorObject.get.getInternalName()))
				return FloorObject.create(floorObj);
			else if (floorObj.getClass().getName().equals(data.AnimatedFloorObject.get.getInternalName()))
				return AnimatedFloorObject.create(floorObj);
			else
				return AbstractFloorObject.create(floorObj);
		}
		
		return null;
	}
	
	
	/**
	 * convenience method
	 * 
	 * @return AbstractBoundary[] null if both boundaries are null, otherwise an array of length 2
	 */
	public AbstractBoundary[] getBoundaries() {
		AbstractBoundary[] boundaries = new AbstractBoundary[] { getBoundary1(), getBoundary2() };
		if (boundaries[0] == null && boundaries[1] == null)
			return null;
		
		return boundaries;
	}
	
	/**
	 * convenience method
	 * 
	 * @return AbstractWallObject[] null if both wall decorations are null, otherwise an array of length 2
	 */
	public AbstractWallObject[] getWallDecorations() {
		AbstractWallObject[] wallDecorations = new AbstractWallObject[] { getWallDecoration1(), getWallDecoration2() };
		if (wallDecorations[0] == null && wallDecorations[1] == null)
			return null;
		
		return wallDecorations;
	}
}
