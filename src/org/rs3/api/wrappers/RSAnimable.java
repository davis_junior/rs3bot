package org.rs3.api.wrappers;

import org.rs3.accessors.I_RSAnimable;


public class RSAnimable extends Interactable {
	
	protected RSAnimable(Object obj) {
		super(obj);
	}
	
	public static RSAnimable create(Object obj) {
		return create(RSAnimable.class, obj);
	}
	
	
	public short getMinX() {
		return I_RSAnimable.getMinX(obj);
	}
	
	public short getMaxX() {
		return I_RSAnimable.getMaxX(obj);
	}
	
	public short getMinY() {
		return I_RSAnimable.getMinY(obj);
	}
	
	public short getMaxY() {
		return I_RSAnimable.getMaxY(obj);
	}
	
	
	@Override
	public Model getModel() {
		return null;
	}
}
