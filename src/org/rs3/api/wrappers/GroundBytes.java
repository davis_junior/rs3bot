package org.rs3.api.wrappers;

import org.rs3.accessors.I_GroundBytes;


public class GroundBytes extends AccessorWrapper {
	
	protected GroundBytes(Object obj) {
		super(obj);
	}
	
	public static GroundBytes create(Object obj) {
		return create(GroundBytes.class, obj);
	}
	
	
	public byte[][][] getBytes() {
		return I_GroundBytes.getBytes(obj);
	}
}
