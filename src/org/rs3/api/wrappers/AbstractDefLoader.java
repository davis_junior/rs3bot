package org.rs3.api.wrappers;

public abstract class AbstractDefLoader extends AccessorWrapper {
	
	protected AbstractDefLoader(Object obj) {
		super(obj);
	}
	
	public static AbstractDefLoader create(Object obj) {
		return create(AbstractDefLoader.class, obj);
	}
}
