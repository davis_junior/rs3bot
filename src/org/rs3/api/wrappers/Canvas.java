package org.rs3.api.wrappers;

import java.awt.Component;

import org.rs3.accessors.I_Canvas;


public class Canvas extends AccessorWrapper {
	
	protected Canvas(Object obj) {
		super(obj);
	}
	
	public static Canvas create(Object obj) {
		return create(Canvas.class, obj);
	}
	
	
	public Component getComponent() {
		return I_Canvas.getComponent(obj);
	}
}
