package org.rs3.api.wrappers;

import org.rs3.accessors.I_GroundInfo;


public class GroundInfo extends AccessorWrapper {
	
	protected GroundInfo(Object obj) {
		super(obj);
	}
	
	public static GroundInfo create(Object obj) {
		return create(GroundInfo.class, obj);
	}
	
	
	public Array3D<Ground> getGroundArray() {
		return Array3D.create(Ground.class, I_GroundInfo.getGroundArray(obj));
	}
	
	public Array1D<TileData> getTileData() {
		return Array1D.create(TileData.class, I_GroundInfo.getTileData(obj));
	}
}
