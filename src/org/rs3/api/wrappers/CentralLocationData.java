package org.rs3.api.wrappers;

import org.rs3.accessors.I_CentralLocationData;
import org.rs3.api.objects.PreciseTile;

public class CentralLocationData extends AbstractCentralLocationData {

	protected CentralLocationData(Object obj) {
		super(obj);
	}

	public static CentralLocationData create(Object obj) {
		return create(CentralLocationData.class, obj);
	}


	public Location getPoint1() {
		return Location.create(I_CentralLocationData.getPoint1(obj));
	}

	public Location getPoint2() {
		return Location.create(I_CentralLocationData.getPoint2(obj));
	}

	public PreciseTile getCentralLocation() {
		Location point1 = getPoint1();
		Location point2 = getPoint2();

		return new PreciseTile(point1.getX() + point2.getX(), point1.getY() + point2.getY(), point1.getHeight() + point2.getHeight(), Client.getPlane());
	}
}
