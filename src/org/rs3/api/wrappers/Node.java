package org.rs3.api.wrappers;

import org.rs3.accessors.I_Node;

public class Node extends AccessorWrapper {
	
	protected Node(Object obj) {
		super(obj);
	}
	
	public static Node create(Object obj) {
		return create(Node.class, obj);
	}
	
	
	public Node getPrevious() {
		return create(I_Node.getPrevious(obj));
	}
	
	public Node getNext() {
		return create(I_Node.getNext(obj));
	}
	
	public long getId() {
		return I_Node.getId(obj);
	}
}
