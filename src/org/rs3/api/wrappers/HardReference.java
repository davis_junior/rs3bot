package org.rs3.api.wrappers;

import org.rs3.accessors.I_HardReference;


public class HardReference extends Reference {
	
	protected HardReference(Object obj) {
		super(obj);
	}
	
	public static HardReference create(Object obj) {
		return create(HardReference.class, obj);
	}
	

	public Object getHardReference() {
		return I_HardReference.getHardReference(obj);
	}
}
