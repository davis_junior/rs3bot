package org.rs3.api.wrappers;

import org.rs3.accessors.I_GraphicsSettingsContainer;


public class GraphicsSettingsContainer extends Node {
	
	protected GraphicsSettingsContainer(Object obj) {
		super(obj);
	}
	
	public static GraphicsSettingsContainer create(Object obj) {
		return create(GraphicsSettingsContainer.class, obj);
	}
	
	
	public GraphicsLevel getLevel() {
		return GraphicsLevel.create(I_GraphicsSettingsContainer.getLevel(obj));
	}
	
	public GraphicsCpuUsage getCpuUsage() {
		return GraphicsCpuUsage.create(I_GraphicsSettingsContainer.getCpuUsage(obj));
	}
}
