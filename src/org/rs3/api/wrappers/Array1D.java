package org.rs3.api.wrappers;

public class Array1D<T extends AccessorWrapper> {
	
	private Class<T> clazz;
	private Object[] obj;
	
	private Array1D(Class<T> clazz, Object[] obj) {
		this.clazz = clazz;
		this.obj = obj;
	}
	
	public static <T extends AccessorWrapper> Array1D<T> create(Class<T> clazz, Object[] obj) {
		if (obj != null)
			return new Array1D<T>(clazz, obj);
		
		return null;
	}
	
	public Object[] getObject() {
		return obj;
	}
	
	public int getLength() {
		return obj.length;
	}
	
	
	/**
	 * Creates an AccessorWrapper, wrapping the object at the specified index.
	 * 
	 * @param index Index in internal array.
	 * @return new AccessorWrapper at index.
	 */
	public T get(int index) {
		return AccessorWrapper.create(clazz, obj[index]);
	}
	
	public T[] getAll() {
		return getAll(true);
	}
	
	public T[] getAll(boolean includeNull) {
		return WrapperCalculations.getArray(clazz, obj, includeNull);
	}
}
