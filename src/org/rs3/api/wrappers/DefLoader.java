package org.rs3.api.wrappers;

import org.rs3.accessors.I_DefLoader;


public class DefLoader extends AccessorWrapper {
	
	protected DefLoader(Object obj) {
		super(obj);
	}
	
	public static DefLoader create(Object obj) {
		return create(DefLoader.class, obj);
	}
	
	
	/*public int getId() {
		return I_DefLoader.getId(obj);
	}*/
	
	public Cache getDefinitionCache() {
		return Cache.create(I_DefLoader.getDefinitionCache(obj));
	}
}