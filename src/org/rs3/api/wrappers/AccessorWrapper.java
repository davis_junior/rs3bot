package org.rs3.api.wrappers;

import java.lang.reflect.InvocationTargetException;

import org.rs3.accessors.Reflection;


public class AccessorWrapper {
	
	protected Object obj;
	
	protected AccessorWrapper(Object obj) {
		this.obj = obj;
	}
	
	public Object getObject() {
		return obj;
	}
	
	public static <T extends AccessorWrapper> T create(Class<T> clazz, Object obj) {
		if (obj != null) {
			try {
				return clazz.getDeclaredConstructor(Object.class).newInstance(obj);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	public static <T extends AccessorWrapper> T forceCast(Class<T> clazz, AccessorWrapper wrapper) {
		return create(clazz, wrapper.obj);
	}
	
	public <T extends AccessorWrapper> T forceCast(Class<T> clazz) {
		return forceCast(clazz, this);
	}
	
	
	public void printFieldDebugInfo() {
		Reflection.printFieldNameDebugInfo(obj.getClass());
	}
}