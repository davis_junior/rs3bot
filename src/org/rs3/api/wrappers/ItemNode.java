package org.rs3.api.wrappers;

import org.rs3.accessors.I_ItemNode;
import org.rs3.api.Nodes;


public class ItemNode extends Node {
	
	protected ItemNode(Object obj) {
		super(obj);
	}
	
	public static ItemNode create(Object obj) {
		return create(ItemNode.class, obj);
	}
	
	
	public int getItemId() {
		return I_ItemNode.getItemId(obj);
	}
	
	public int getStackSize() {
		return I_ItemNode.getStackSize(obj);
	}
	
	
	public ItemDefinition getDefinition() {
		HashTable defTable = Client.getItemDefLoader().getDefinitionCache().getHashTable();
		
		Node ref = Nodes.lookup(defTable, getItemId());
		if (ref == null)
			return null;
		
		if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
			HardReference hr = ref.forceCast(HardReference.class);
			Object def = hr.getHardReference();
			
			if (def != null) {
				return ItemDefinition.create(def);
			}
			
		} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
			SoftReference sr = ref.forceCast(SoftReference.class);
			Object def = sr.getSoftReference().get();
			
			if (def != null) {
				return ItemDefinition.create(def);
			}
		}
		
		return null;
	}
	
	public Model getCachedModel() {
		try {
			ItemDefinition definition = getDefinition();
			
			if (definition == null)
				return null;
			
			long id = definition.getId() | Client.getRender().getCacheIndex() << 29;
			
			Node ref = Nodes.lookup(definition.getCacheLoader().getModelCache().getHashTable(), id);
			if (ref == null)
				return null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, 0, 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, 0, 0);
					} else {
						return Model.create(def, 0, 0);
					}
				}
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null) {
					Render render = Client.getRender();
					
					if (render instanceof SDRender) {
						return SDModel.create(def, 0, 0);
					} else if (render instanceof OpenGLRender) {
						return OpenGLModel.create(def, 0, 0);
					} else {
						return Model.create(def, 0, 0);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Model getModel() {
		return getCachedModel();
	}
}
