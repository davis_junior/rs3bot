package org.rs3.api.wrappers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class WrapperCalculations {
	
	public static <T extends AccessorWrapper> T[] getArray(Class<T> returnClass, Object[] iArray, boolean includeNull) {
		if (iArray == null)
			return null;
		
		if (includeNull) {
			//T[] array = new T[iArray.length];
			@SuppressWarnings("unchecked")
			final T[] array = (T[]) Array.newInstance(returnClass, iArray.length);
			
			for (int i = 0; i < array.length; i++) {
				array[i] = AccessorWrapper.create(returnClass, iArray[i]);
			}
			
			return array;
		} else {
			List<T> list = new ArrayList<T>();
			
			for (int i = 0; i < iArray.length; i++) {
				if (iArray[i] != null)
					list.add(AccessorWrapper.create(returnClass, iArray[i]));
			}
			
			//return list.toArray(new T[0]);
			@SuppressWarnings("unchecked")
			final T[] arrayDef = (T[]) Array.newInstance(returnClass, 0);
			return list.toArray(arrayDef);
		}
	}
	
	// TODO: test
	public static <T extends AccessorWrapper> T[][] getArray2D(Class<T> returnClass, Object[][] iArray) {
		if (iArray == null)
			return null;
		
		//T[] array = new T[iArray.length];
		@SuppressWarnings("unchecked")
		final T[][] array = (T[][]) Array.newInstance(returnClass, iArray.length, iArray[0].length);
		
		for (int i = 0; i < array.length; i++) {
			for (int i2 = 0; i2 < array[0].length; i2++) {
				array[i][i2] = AccessorWrapper.create(returnClass, iArray[i][i2]);
			}
		}
		
		return array;		
	}
	
	public static <T extends AccessorWrapper> T[][][] getArray3D(Class<T> returnClass, Object[][][] iArray) {
		if (iArray == null)
			return null;
		
		//T[] array = new T[iArray.length];
		@SuppressWarnings("unchecked")
		final T[][][] array = (T[][][]) Array.newInstance(returnClass, iArray.length, iArray[0].length, iArray[0][0].length);
		
		for (int i = 0; i < array.length; i++) {
			for (int i2 = 0; i2 < array[0].length; i2++) {
				for (int i3 = 0; i3 < array[0][0].length; i3++) {
					array[i][i2][i3] = AccessorWrapper.create(returnClass, iArray[i][i2][i3]);
				}
			}
		}
		
		return array;		
	}
}
