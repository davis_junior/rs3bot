package org.rs3.api.wrappers;

import org.rs3.accessors.I_PlayerDef;


public class PlayerDef extends AccessorWrapper {
	
	protected PlayerDef(Object obj) {
		super(obj);
	}
	
	public static PlayerDef create(Object obj) {
		return create(PlayerDef.class, obj);
	}
	
	
	public long getModelHash() {
		return I_PlayerDef.getModelHash(obj);
	}
	
	public boolean isFemale() {
		return I_PlayerDef.isFemale(obj);
	}
	
	public int[] getEquipment() {
		return I_PlayerDef.getEquipment(obj);
	}
}
