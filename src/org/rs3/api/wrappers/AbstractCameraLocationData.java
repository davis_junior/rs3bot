package org.rs3.api.wrappers;

public abstract class AbstractCameraLocationData extends AccessorWrapper {
	
	protected AbstractCameraLocationData(Object obj) {
		super(obj);
	}
	
	public static AbstractCameraLocationData create(Object obj) {
		return create(AbstractCameraLocationData.class, obj);
	}
}
