package org.rs3.api.wrappers;

import org.rs3.accessors.I_NodeListCache;


public class NodeListCache extends Node {
	
	protected NodeListCache(Object obj) {
		super(obj);
	}
	
	public static NodeListCache create(Object obj) {
		return create(NodeListCache.class, obj);
	}
	
	
	public NodeDeque getNodeList() {
		return NodeDeque.create(I_NodeListCache.getNodeList(obj));
	}
}
