package org.rs3.api.wrappers;

import org.rs3.accessors.I_ComponentNode;


public class ComponentNode extends Node {
	
	protected ComponentNode(Object obj) {
		super(obj);
	}
	
	public static ComponentNode create(Object obj) {
		return create(ComponentNode.class, obj);
	}
	
	
	public int getMainId() {
		return I_ComponentNode.getMainId(obj);
	}
}
