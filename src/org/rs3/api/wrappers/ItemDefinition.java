package org.rs3.api.wrappers;

import org.rs3.accessors.I_ItemDefinition;


public class ItemDefinition extends AccessorWrapper {
	
	protected ItemDefinition(Object obj) {
		super(obj);
	}
	
	public static ItemDefinition create(Object obj) {
		return create(ItemDefinition.class, obj);
	}
	
	
	public String getName() {
		return I_ItemDefinition.getName(obj);
	}
	
	public String[] getActions() {
		return I_ItemDefinition.getActions(obj);
	}
	
	public int getId() {
		return I_ItemDefinition.getId(obj);
	}
	
	public AbstractDefLoader getDefinitionLoader() {
		return AbstractDefLoader.create(I_ItemDefinition.getDefinitionLoader(obj));
	}
	
	public ItemCacheLoader getCacheLoader() {
		return ItemCacheLoader.create(I_ItemDefinition.getCacheLoader(obj));
	}
	
	/*public byte[] getModelCache2byteArray() {
		return I_ItemDefinition.getModelCache2byteArray(obj);
	}
	
	public int[][] getModelCache2intArray2D() {
		return I_ItemDefinition.getModelCache2intArray2D(obj);
	}*/
}
