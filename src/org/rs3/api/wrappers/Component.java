package org.rs3.api.wrappers;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;

import javax.media.opengl.GL2;

import org.rs3.accessors.I_Component;
import org.rs3.api.Camera;
import org.rs3.api.Interfaces;
import org.rs3.api.Menu;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Component extends AccessorWrapper {
	
	private static boolean savedInteractionAntiban = true;
	public static boolean interactionAntiban = true;
	
	boolean isParentIdCached;
	int cachedParentId;
	
	boolean isParentCached;
	Component cachedParent;
	
	int overridenWidth = -1;
	int overridenHeight = -1;
	
	protected Component(Object obj) {
		super(obj);
		resetCaches();
	}
	
	public static Component create(Object obj) {
		return create(Component.class, obj);
	}
	
	
	public void resetCaches() {
		isParentIdCached = false;
		cachedParentId = -1;
		
		isParentCached = false;
		cachedParent = null;
	}
	
	public static void pauseAntiban() {
		savedInteractionAntiban = interactionAntiban;
		interactionAntiban = false;
	}
	
	public static void resumeAntiban() {
		interactionAntiban = savedInteractionAntiban;
	}
	
	
	public Array1D<Component> getComponents() {
		return Array1D.create(Component.class, I_Component.getComponents(obj));
	}
	
	public int getBoundsArrayIndex() {
		return I_Component.getBoundsArrayIndex(obj);
	}
	
	public int getParentId() {
		return getParentId(Interfaces.useCachedComponentNodes);
	}
	
	public int getParentId(boolean useCachedComponentNodes) {
		if (isParentIdCached)
			return cachedParentId;
		
		int parentId = I_Component.getParentId(obj);
		
		if (parentId != -1) {
			isParentIdCached = true;
			cachedParentId = parentId;
			return parentId;
		}
		
		int mainId = getId() >>> 16;
		
		if (!useCachedComponentNodes) {
			HashTable nc = Client.getComponentNodeCache();
			
			if (nc != null) {
				for (Node n = nc.getFirst(); n != null; n = nc.getNext()) {
					int id = (int) n.getId();
					
					ComponentNode cn = n.forceCast(ComponentNode.class);
					
					if (mainId == cn.getMainId())
						return id;
				}
			}
		} else {
			if (Interfaces.cache_componentNodes != null) {
				ComponentNode cn = Interfaces.cache_componentNodes.get(mainId);
				if (cn != null) {
					int id = (int) cn.getId();
					isParentIdCached = true;
					cachedParentId = id;
					return id;
				}
			}
		}
		
		return -1;
	}
	
	public Component getParent() {
		if (isParentCached)
			return cachedParent;
		
		int parentId = getParentId();
		
		if (parentId != -1) {
			isParentCached = true;
			cachedParent = Interfaces.getChild(parentId);
			return cachedParent;
		}
		
		return null;
	}
	
	public int getId() {
		return I_Component.getId(obj);
	}
	
	public int getX() {
		return I_Component.getX(obj);
	}
	
	public int getY() {
		return I_Component.getY(obj);
	}
	
	public int getWidth() {
		if (overridenWidth != -1)
			return overridenWidth;
		
		// TODO: this isn't accurate for setting gameplay subtab buttons
		if (!isInScrollableArea())
			return getHorizontalScrollbarThumbSize();
		
		//return I_Component.getWidth(obj) - 4;
		return I_Component.getWidth(obj);
	}
	
	public int getHeight() {
		if (overridenHeight != -1)
			return overridenHeight;
		
		// TODO: this isn't accurate for setting gameplay subtab buttons
		if (!isInScrollableArea())
			return getVerticalScrollbarThumbSize();
		
		//return iAccessor.getHeight() - 4;
		return I_Component.getHeight(obj);
	}
	
	public int getHorizontalScrollbarThumbSize() {
		return I_Component.getHorizontalScrollbarThumbSize(obj);
	}
	
	public int getVerticalScrollbarThumbSize() {
		return I_Component.getVerticalScrollbarThumbSize(obj);
	}
	
	public int getType() {
		return I_Component.getType(obj);
	}
	
	public int getComponentId() {
		return I_Component.getComponentId(obj);
	}
	
	public boolean _isHidden() {
		return I_Component.isHidden(obj);
	}
	
	public boolean _isVisible() {
		return I_Component.isVisible(obj);
	}
	
	public int getVerticalScrollbarSize() {
		return I_Component.getVerticalScrollbarSize(obj);
	}
	
	public int getHorizontalScrollbarSize() {
		return I_Component.getHorizontalScrollbarSize(obj);
	}
	
	public int getVerticalScrollbarPosition() {
		return I_Component.getVerticalScrollbarPosition(obj);
	}
	
	public int getHorizontalScrollbarPosition() {
		return I_Component.getHorizontalScrollbarPosition(obj);
	}
	
	public String[] getActions() {
		return I_Component.getActions(obj);
	}
	
	public String getTooltip() {
		return I_Component.getTooltip(obj);
	}
	
	public String getComponentName() {
		return I_Component.getComponentName(obj);
	}
	
	public String getText() {
		return I_Component.getText(obj);
	}
	
	public Point getAbsoluteLocation() {
		return __getAbsoluteLocation(false, Client.getComponentBoundsArray());
	}
	
	public Point __getAbsoluteLocation(boolean useCachedComponentNodes, Rectangle[] componentBoundsArray) {
		
		int parentId = getParentId();
		
		if (parentId == -1) {
			Rectangle[] bounds = componentBoundsArray; //Rectangle[] bounds = Client.getComponentBoundsArray();
			int index = getBoundsArrayIndex();
			
			if (bounds != null && index >= 0 && index < bounds.length && bounds[index] != null) {
				//x += bounds[index].x;
				//y += bounds[index].y;
				return new Point(bounds[index].x, bounds[index].y);
			}
		}
		
		
		Component parent = getParent();
		
		if (parent != null) {
			Point p = parent.getAbsoluteLocation();
			
			if (p != null) {
				int x = p.x;
				int y = p.y;
				
				int h = parent.getHorizontalScrollbarSize();
				int v = parent.getVerticalScrollbarSize();
				
				if (v > 0 || h > 0) {
					x -= parent.getHorizontalScrollbarPosition();
					y -= parent.getVerticalScrollbarPosition();
				}
				
				x += getX();
				y += getY();
				
				return new Point(x, y);
			}
		}
		
		return null;
	}
	
	/**
	 * Overrides width
	 * @param width
	 */
	public void setWidth(int width) {
		this.overridenWidth = width;
	}
	
	/**
	 * Overrides height
	 * @param height
	 */
	public void setHeight(int height) {
		this.overridenHeight = height;
	}
	
	// defaults to false on unknown result
	public boolean isVisible() {
		return isVisible(false, true);
	}
	
	// TODO: maybe hook displayTime for some interfaces like main menu
	// intersectsParent is used for scrolling components
	// defaultOnUnknown is returned on unknown result
	public boolean isVisible(boolean defaultOnUnknown, boolean intersectsParent) {
		if (_isHidden())
			return false;
		
		if (_isVisible())
			return true;
		
		if (!validate())
			return false;
		
		int parentId = getParentId();
		
		if (parentId != -1) {
			Component parent = getParent();
			
			if (parent == null)
				return false;
			
			//return parent.isVisible(defaultOnUnknown, false);
			return parent.isVisible(defaultOnUnknown, intersectsParent) && intersects(parent);
		}
		
		return defaultOnUnknown;
	}
	
	public boolean isInScrollableArea() {
		int parentId = getParentId();
		
		if (parentId == -1)
			return false;
		
		Component scrollableArea = getParent();
		if (scrollableArea == null)
			return false;
		
		while (scrollableArea.getVerticalScrollbarSize() == 0 && scrollableArea.getParentId() != -1) {
			scrollableArea = Interfaces.getChild(scrollableArea.getParentId());
			if (scrollableArea == null)
				return false;
		}
		
		return scrollableArea.getVerticalScrollbarSize() != 0;
	}
	
	public boolean validate() {
		//return parentWidget.validate() && getBoundsArrayIndex() != -1;
		return getBoundsArrayIndex() != -1; // TODO: maybe add parent widget validate
	}
	
	
	public boolean isOnScreen() {
		return validate() && isVisible();
	}
	
	public Polygon getBounds() {
		if (validate()) {
			final Point p = getAbsoluteLocation();
			
			if (p != null) {
				final int w = getWidth();
				final int h = getHeight();
				
				final Polygon poly = new Polygon();
				poly.addPoint(p.x, p.y);
				poly.addPoint(p.x + w, p.y);
				poly.addPoint(p.x + w, p.y + h);
				poly.addPoint(p.x, p.y + h);
				
				return poly;
			}
		}
		
		return null;
	}
	
	// percent: 1-100
	public Polygon getShrunkBounds(int percentage) {
		if (percentage <= 0 || percentage > 100)
			return null;
		
		if (validate()) {
			Point p = getAbsoluteLocation();
			
			if (p != null) {
				int w = getWidth();
				int h = getHeight();
				
				float percent =  percentage / 100.0F;
				//float factor =  1.0F + percent;
				float inverse = 1.0F - percent;
				
				p.setLocation(p.x + Math.round(w * inverse * 0.5F), p.y + Math.round(h * inverse * 0.5F));
				
				w = Math.round(w * percent);
				h = Math.round(h * percent);
				
				Polygon poly = new Polygon();
				poly.addPoint(p.x, p.y);
				poly.addPoint(p.x + w, p.y);
				poly.addPoint(p.x + w, p.y + h);
				poly.addPoint(p.x, p.y + h);
				return poly;
			}
		}
		
		return null;
	}
	
	public Rectangle getBoundingRectangle() {
		if (validate()) {
			final Point p = getAbsoluteLocation();
			
			if (p != null) {
				final int w = getWidth();
				final int h = getHeight();
				
				return new Rectangle(p.x, p.y, w, h);
			}
		}
		
		return null;
	}
	
	// percent: 1-100
	public Rectangle getShrunkBoundingRectangle(int percentage) {
		if (validate()) {
			Point p = getAbsoluteLocation();
			
			if (p != null) {
				int w = getWidth();
				int h = getHeight();
				
				float percent =  percentage / 100.0F;
				//float factor =  1.0F + percent;
				float inverse = 1.0F - percent;
				
				p.setLocation(p.x + Math.round(w * inverse * 0.5F), p.y + Math.round(h * inverse * 0.5F));
				
				w = Math.round(w * percent);
				h = Math.round(h * percent);
				
				return new Rectangle(p.x, p.y, w, h);
			}
		}
		
		return null;
	}
	
	public Point getCentralPoint() {
		final Point p = getAbsoluteLocation();
		
		if (p != null) {
			final int w = getWidth();
			final int h = getHeight();
			
			return validate() ? new Point((p.x * 2 + w) / 2, (p.y * 2 + h) / 2) : null;
		}
		
		return null;
	}
	
	// gets random point from getShrunkBoundingRectangle(...)
	// percent: 1-100
	public Point getRandomShrunkPoint(int percentage) {
		final Rectangle rect = getShrunkBoundingRectangle(percentage);
		
		if (rect == null)
			return null;
		
		if (rect.x == -1 || rect.y == -1 || rect.width == -1 || rect.height == -1)
			return null;
		
		final int min_x = rect.x + 1, min_y = rect.y + 1;
		final int max_x = min_x + rect.width - 2, max_y = min_y + rect.height - 2;
		
		return new Point(Random.nextGaussian(min_x, max_x, rect.width / 3), Random.nextGaussian(min_y, max_y, rect.height / 3));
	}
	
	public Point getNextViewportPoint() {
		final Rectangle rect = getBoundingRectangle();
		
		if (rect == null)
			return null;
		
		if (rect.x == -1 || rect.y == -1 || rect.width == -1 || rect.height == -1)
			return null;
		
		final int min_x = rect.x + 1, min_y = rect.y + 1;
		final int max_x = min_x + rect.width - 2, max_y = min_y + rect.height - 2;
		
		return new Point(Random.nextGaussian(min_x, max_x, rect.width / 3), Random.nextGaussian(min_y, max_y, rect.height / 3));
	}
	
	public boolean contains(final Point point) {
		final Rectangle rect = getBoundingRectangle();
		
		if (rect == null)
			return false;
		
		if (rect.x == -1 || rect.y == -1 || rect.width == -1 || rect.height == -1)
			return false;
		
		final int min_x = rect.x + 1, min_y = rect.y + 1;
		final int max_x = min_x + rect.width - 2, max_y = min_y + rect.height - 2;
		
		return (point.x >= min_x) && (point.x <= max_x) && (point.y >= min_y) && (point.y <= max_y);
	}
	
	public boolean contains(final Component comp) {
		final Rectangle rect = getBoundingRectangle();
		
		if (rect == null)
			return false;
		
		if (rect.x == -1 || rect.y == -1 || rect.width == -1 || rect.height == -1)
			return false;
		
		
		final Rectangle childRect = comp.getBoundingRectangle();
		
		if (childRect == null)
			return false;
		
		if (childRect.x == -1 || childRect.y == -1 || childRect.width == -1 || childRect.height == -1)
			return false;
		
		return rect.contains(childRect);
	}
	
	public boolean intersects(final Component comp) {
		final Rectangle rect = getBoundingRectangle();
		
		if (rect == null)
			return false;
		
		if (rect.x == -1 || rect.y == -1 || rect.width == -1 || rect.height == -1)
			return false;
		
		
		final Rectangle childRect = comp.getBoundingRectangle();
		
		if (childRect == null)
			return false;
		
		if (childRect.x == -1 || childRect.y == -1 || childRect.width == -1 || childRect.height == -1)
			return false;
		
		return rect.intersects(childRect);
	}
	
	// gets a random point often close to the central point but sometimes fully random
	public Point getRandomPoint() {
		int rand = Random.nextInt(0, 100);
		
		if (rand >= 0 && rand <= 50) {
			return getRandomShrunkPoint(50);
		} else if (rand > 50 && rand <= 75) {
			return getRandomShrunkPoint(75);
		} else if (rand > 75) {
			return getNextViewportPoint();
		}
		
		return null;
	}
	
	public boolean hover() {
		Point randPoint = getRandomPoint();
		
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2)) != null)
			return true;
		
		return false;
	}
	
	public boolean click(int button) {
		Point randPoint = getRandomPoint();
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2, button)) != null)
			return true;
		
		return false;
	}
	
	public boolean drag(int x, int y, int button) {
		Point randPoint = getRandomPoint();
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2)) != null) {
			Time.sleepQuickest();
			
			if ((randPoint = Mouse.drag(x, y, 2, 2, button)) != null)
				return true;
		}
		
		return false;
	}
	
	public boolean drag(Component otherComp, int button) {
		if (otherComp == null)
			return false;
		
		Point otherRandPoint = otherComp.getRandomPoint();
		if (otherRandPoint == null)
			return false;
		
		return drag(otherRandPoint.x, otherRandPoint.y, button);
	}
	
	public boolean interact(String action) {
		return interact(true, action);
	}
	
	public boolean interact(boolean hover, String action) {
		if (hover) {
			if (!hover())
				return false;
			
			Time.sleepVeryQuick();
		}

		if (Menu.select(action)) {
			if (interactionAntiban && Random.nextInt(0, 100) <= 75) {
				Time.sleepVeryQuick();
				Antiban.randomMoveMouse();
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean interact(String action, String option) {
		return interact(true, action, option);
	}
	
	public boolean interact(boolean hover, String action, String option) {
		if (hover) {
			if (!hover())
				return false;
			
			Time.sleepVeryQuick();
		}
		
		if (Menu.select(action, option)) {
			if (interactionAntiban && Random.nextInt(0, 100) <= 75) {
				Time.sleepVeryQuick();
				Antiban.randomMoveMouse();
			}
			
			return true;
		}
			
		return false;
	}
	
	public boolean loopInteract(Condition breakCondition, String action) {
		return loopInteract(breakCondition, action, null, false);
	}
	
	/**
	 * Same as BasicInteractable without model, tile, and camera usage
	 * 
	 * @param breakCondition
	 * @param action
	 * @param option
	 * @param breakOnWrongOption
	 * @return
	 */
	public boolean loopInteract(Condition breakCondition, String action, String option, boolean breakOnWrongOption) {
		Timer loopTimer = new Timer(5000);
		while (loopTimer.isRunning()) {
			if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
				return false;
			
			if (breakCondition != null && breakCondition.evaluate())
				return false;
			
			hover();
			Time.sleepQuickest();
			
			boolean hasAction = false;
			boolean firstAction = false;
			
			if (Menu.firstItemContains(action, option)) {
				hasAction = true;
				firstAction = true;
			} else {
				Menu.updateMenuItemsCaches();
				if (Menu.cachedItems == null || !Menu.contains(action, option)) {
					hover();
					Time.sleepQuickest();
					
					Timer menuTimer = new Timer(1000);
					while (menuTimer.isRunning()) {
						if (breakCondition != null && breakCondition.evaluate())
							return false;
						
						if (Menu.firstItemContains(action, option)) {
							hasAction = true;
							firstAction = true;
							break;
						}
						
						Menu.updateMenuItemsCaches();
						
						if (Menu.cachedItems != null) {
							if (Menu.contains(action, option)) {
								hasAction = true;
								break;
							} else if (breakOnWrongOption && Menu.contains(action)) { // wrong option "Iron rocks" rather than "Iron ore rocks" - a non-spawned ore
								return false;
							}
						}
						
						hover();
						Time.sleepVeryQuick();
					}
				} else
					hasAction = true;
			}
			
			if (hasAction) {
				if (breakCondition != null && breakCondition.evaluate())
					return false;
				
				if (firstAction) {
					if (Mouse.fastClick(Mouse.LEFT) != null && Menu.waitForSelection(action, option))
						return true;
				} else {
					if (interact(false, action, option))
						return true;
				}
				
				Time.sleepVeryQuick();
			}
		}
		
		return false;
	}
	
	public void draw(final Graphics g) {
		//TODO
	}
	
	public void render(GL2 gl2) {
		//TODO
	}
}
