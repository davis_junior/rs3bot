package org.rs3.api.wrappers;

import org.rs3.accessors.I_ItemTable;
import org.rs3.api.Interfaces;

public class ItemTable extends Node {
	
	protected ItemTable(Object obj) {
		super(obj);
	}
	
	public static ItemTable create(Object obj) {
		return create(ItemTable.class, obj);
	}
	
	
	public int[] getItemIds() {
		return I_ItemTable.getItemIds(obj);
	}
	
	public int[] getItemStackSizes() {
		return I_ItemTable.getItemStackSizes(obj);
	}
	
	
	public int countAllItems(boolean countStackSizes) {
		return countAllItems(countStackSizes, null);
	}
	
	public int countAllItems(boolean countStackSizes, int... exceptionIds) {
		ItemTable table = this;
			
		//if (table != null) {
			int count = 0;
			
			int[] itemIds = table.getItemIds();
			int[] stackSizes = table.getItemStackSizes();
			
			if (itemIds != null) {
				for (int i = 0; i < itemIds.length; i++) {
					if (itemIds[i] != -1) {
						boolean exception = false;
						
						if (exceptionIds != null) {
							for (int exceptionId : exceptionIds) {
								if (itemIds[i] == exceptionId) {
									exception = true;
									break;
								}
							}
						}
						
						if (!exception) {
							if (!countStackSizes)
								count++;
							else
								count += stackSizes[i];
						}
					}
				}
			}
			
			return count;
		//}
		
		//return -1;
	}
	
	public int countItems(boolean countStackSizes, int itemId) {
		ItemTable table = this;
			
		//if (table != null) {
			int count = 0;
			
			int[] itemIds = table.getItemIds();
			int[] stackSizes = table.getItemStackSizes();
			
			if (itemIds != null) {
				for (int i = 0; i < itemIds.length; i++) {
					if (itemIds[i] != -1) {
						if (itemIds[i] == itemId) {
							if (!countStackSizes)
								count++;
							else
								count += stackSizes[i];
						}
					}
				}
			}
			
			return count;
		//}
		
		//return -1;
	}
	
	public boolean containsAny(int... ids) {
		ItemTable table = this;
		
		//if (table == null)
		//	return false;
		
		int[] itemIds = table.getItemIds();
		if (itemIds == null)
			return false;
		
		for (int itemId : itemIds) {
			for (int checkId : ids) {
				if (itemId == checkId)
					return true;
			}
		}
		
		return false;
	}
	
	public boolean containsAll(int... ids) {
		ItemTable table = this;
		
		//if (table == null)
		//	return false;
		
		int[] itemIds = table.getItemIds();
		if (itemIds == null)
			return false;
		
		for (int id : ids) {
			boolean contains = false;
			for (int itemId : itemIds) {
				if (id == itemId) {
					contains = true;
					break;
				}
			}
			
			if (!contains)
				return false;
		}
		
		return true;
	}
	
	public boolean containsUselessItems() {
		return containsAny(Interfaces.internalItemExceptionIds);
	}
	
	public int getStackSize(int itemId) {
		ItemTable table = this;
		
		int[] itemIds = table.getItemIds();
		int[] itemStackSizes = table.getItemStackSizes();
		
		if (itemIds != null && itemStackSizes != null) {
			if (itemIds.length > 0 && itemStackSizes.length > 0) {
				for (int i = 0; i < itemIds.length; i++) {
					if (itemIds[i] == itemId)
						return itemStackSizes[i];
				}
				
				return 0;
			}
		}
		
		//return -1;
		return 0;
	}
	
	public int getItemIdAt(int index) {
		ItemTable table = this;
		
		int[] ids = table.getItemIds();
		if (ids != null && index < ids.length) {
			return ids[index];
		}
		
		return -1;
	}
	
	public void printItems() {
		long tableId = getId();
		int[] itemIds = getItemIds();
		int[] stackSizes = getItemStackSizes();
		if (itemIds != null && stackSizes != null) {
			for (int i = 0; i < Math.min(itemIds.length, stackSizes.length); i++) {
				System.out.println("[" + tableId + "] " + itemIds[i] + ", " + stackSizes[i]);
			}
		}
	}
}
