package org.rs3.api.wrappers;

public abstract class AbstractBoundary extends Interactable {
	
	protected AbstractBoundary(Object obj) {
		super(obj);
	}
	
	public static AbstractBoundary create(Object obj) {
		return create(AbstractBoundary.class, obj);
	}
}
