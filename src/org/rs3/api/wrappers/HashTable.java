package org.rs3.api.wrappers;

import java.util.ArrayList;
import java.util.List;

import org.rs3.accessors.I_HashTable;

public class HashTable extends AccessorWrapper {
	
	private Node current;
	private int c_index = 0;
	
	protected HashTable(Object obj) {
		super(obj);
	}
	
	public static HashTable create(Object obj) {
		return create(HashTable.class, obj);
	}
	
	
	public Array1D<Node> getBuckets() {
		return Array1D.create(Node.class, I_HashTable.getBuckets(obj));
	}
	
	
	public Node getFirst() {
		c_index = 0;
		return getNext();
	}

	public Node getNext() {
		try {
			final Array1D<Node> buckets = getBuckets();
			
			if (buckets == null)
				return null;
			
			if (c_index > 0 && buckets.get(c_index - 1).getObject() != current.getObject()) {
				final Node node = current;
				current = node.getNext();
				return node;
			}
			
			int length = buckets.getLength();
			while (c_index < length) {
				final Node node = buckets.get(c_index++).getNext();
				
				if (buckets.get(c_index - 1).getObject() != node.getObject()) {
					current = node.getNext();
					return node;
				}
			}
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
		
		return null;
	}
	
	/*public Node getNext() {
		final Node[] buckets = getBuckets();
		
		if (buckets == null)
			return null;
			
		//if (c_index > 0 && buckets[c_index - 1] != current) {
		if (c_index > 0 && buckets[c_index - 1].getObject() != current.getObject()) {
			final Node node = current;
			current = node.getPrevious();
			return node;
		}
		
		while (c_index < buckets.length) {
			final Node node = buckets[c_index++].getPrevious();
			
			//if (buckets[c_index - 1] != node) {
			if (buckets[c_index - 1].getObject() != node.getObject()) {
				current = node.getPrevious();
				return node;
			}
		}
		
		return null;
	}*/
	
	// old/inefficient, used before I fixed HashTable getNext()
	/*public Node[] getAll() {
    	List<Long> ids = new ArrayList<>();
    	
    	List<Node> nodes = new ArrayList<>();
    	
    	Node[] buckets = getBuckets();
    	
    	if (buckets == null)
    		return null;
    	
		for (Node node : buckets) {
			for (int i = 0; i < buckets.length; i++) {
				node = node.getNext();
				
				if (node == null)
					break;
				
				long id = node.getId();
				
				if (id != 0L) {
					if (ids.contains(id))
						continue;
					
					ids.add(id);
					
					nodes.add(node);
				} else
					break;
			}
		}
		
		if (nodes.size() != 0)
			return nodes.toArray(new Node<?>[0]);
		
		return null;
	}*/
	
	public Node[] getAll() {
		List<Node> nodes = new ArrayList<>();
		
		for (Node node = getFirst(); node != null; node = getNext()) {
			nodes.add(node);
		}
		
		if (nodes.size() != 0)
			return nodes.toArray(new Node[0]);
		
		return null;
	}
}
