package org.rs3.api.wrappers;

import org.rs3.accessors.I_NodeSub;


public class NodeSub extends Node {
	
	protected NodeSub(Object obj) {
		super(obj);
	}
	
	public static NodeSub create(Object obj) {
		return create(NodeSub.class, obj);
	}
	
	
	public NodeSub getPreviousSub() {
		return create(I_NodeSub.getPreviousSub(obj));
	}
	
	public NodeSub getNextSub() {
		return create(I_NodeSub.getNextSub(obj));
	}
	
	public long getSubId() {
		return I_NodeSub.getSubId(obj);
	}
}
