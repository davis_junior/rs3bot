package org.rs3.api.wrappers;

import org.rs3.accessors.I_TileData;


public class TileData extends AccessorWrapper {
	
	protected TileData(Object obj) {
		super(obj);
	}
	
	public static TileData create(Object obj) {
		return create(TileData.class, obj);
	}
	
	
	public int[][] getHeights() {
		return I_TileData.getHeights(obj);
	}
}
