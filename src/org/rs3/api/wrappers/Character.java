package org.rs3.api.wrappers;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.media.opengl.GL2;

import org.rs3.accessors.I_Character;
import org.rs3.api.Nodes;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.antiban.Antiban;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;



public class Character extends RSAnimable {
	
	protected Character(Object obj) {
		super(obj);
	}
	
	public static Character create(Object obj) {
		return create(Character.class, obj);
	}
	
	
	/*public ? getCombatStatusList() {
		return ?.create(I_Character.getCombatStatusList(obj));
	}*/
	
	public Animator getAnimator() {
		return Animator.create(I_Character.getAnimator(obj));
	}
	
	public MessageData getMessageData() {
		return MessageData.create(I_Character.getMessageData(obj));
	}
	
	/*public ? getPassiveAnimator() {
		return ?.create(I_Character.getPassiveAnimator(obj));
	}*/
	
	public int getOrientation() {
		//return (180 + I_Character.getOrientation(obj) * 45 / 2048) % 360;
		return (I_Character.getOrientation(obj) * 45 / 2048) % 360;
	}
	
	public int getInteractingIndex() {
		return I_Character.getInteractingIndex(obj);
	}
	
	public int getHeight() {
		return I_Character.getHeight(obj);
	}
	
	public int getMovementSpeed() {
		return I_Character.getMovementSpeed(obj);
	}
	
	public Array1D<? extends Model> getModels() {
		Render render = Client.getRender();
		
		if (render instanceof SDRender)
			return Array1D.create(SDModel.class, I_Character.getModels(obj));
		else if (render instanceof OpenGLRender)
			return Array1D.create(OpenGLModel.class, I_Character.getModels(obj));
		else
			return Array1D.create(Model.class, I_Character.getModels(obj));
	}
	
	
	// convenience method
	public int getAnimationId() {
		Animator animator = getAnimator();
		if (animator != null) {
			Animation animation = animator.getAnimation();
			if (animation != null)
				return animation.getId();
		}
		
		return -1;
	}
	
	public boolean isMoving() {
		return getMovementSpeed() != 0;
	}
	
	public Character getInteracting() {
		int index = getInteractingIndex();
		
		if (index != -1) {
			if (index < 0x8000) {
				Node node = Nodes.lookup(Client.getNpcNodeCache(), index);
				
				if (node != null) {
					NpcNode npcNode = node.forceCast(NpcNode.class);
					Npc npc = npcNode.getNpc();
					if (npc != null)
						return npc;
				}
			} else {
				return Client.getPlayers().get(index - 0x8000);
			}
		}
		
		return null;
	}
	
	public Character[] getOthersIneracting() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		List<Character> all = new ArrayList<>();
		
		Npc[] npcs = Npcs.getAll();
		if (npcs != null) {
			for (Npc npc : npcs) {
				if (npc != null) {
					Character interacting = npc.getInteracting();
					if (interacting != null && interacting.getObject() == player.getObject())
						all.add(npc);
				}
			}
		}
		
		Array1D<Player> players = Client.getPlayers();
		if (players != null) {
			for (Player p : players.getAll()) {
				if (p != null) {
					Character interacting = p.getInteracting();
					if (interacting != null && interacting.getObject() == player.getObject())
						all.add(p);
				}
			}
		}
		
		if (all.size() > 0)
			return all.toArray(new Character[0]);
		else
			return null;
	}
	
	public Model curModel = null;
	
	public void updateModel() {
		
		//long start = System.currentTimeMillis();
		
		//while (System.currentTimeMillis() - start <= 5000) {
			Model[] models = getModels().getAll(false);
			
			for(Model model : models)	{
				if(model==null)
					continue;
				else {
					curModel = model;
					curModel.orientationMod = getOrientation();
					break;
				}
			}
			
			/*if (curModel != null) {
				System.out.println(Debug.arrayToString(curModel.indices1, curModel.numFaces, "Indices1"));
				System.out.println(Debug.arrayToString(curModel.indices2, curModel.numFaces, "Indices2"));
				System.out.println(Debug.arrayToString(curModel.indices3, curModel.numFaces, "Indices3"));
	
				System.out.println(Debug.arrayToString(curModel.xPoints, curModel.numVerticies, "xPoints"));
				System.out.println(Debug.arrayToString(curModel.yPoints, curModel.numVerticies, "yPoints"));
				System.out.println(Debug.arrayToString(curModel.zPoints, curModel.numVerticies, "zPoints"));
				
				System.out.println("Indices comparison: " +  curModel.indices1.length + ", " + curModel.numFaces);
				System.out.println("Verticies comparison: " + curModel.xPoints.length + ", " + curModel.numVerticies);
			}*/
			
			/*if (curModel != null) {
				//System.out.println(Debug.arrayToString(curModel.xPoints, curModel.numVerticies, "xPoints"));
				//System.out.println(Debug.arrayToString(curModel.yPoints, curModel.numVerticies, "yPoints"));
				//System.out.println(Debug.arrayToString(curModel.zPoints, curModel.numVerticies, "zPoints"));
				
				// numfaces = 
				// numvert = 
				// as, aq
				int faces = (int) Reflection.getField(curModel.getIAccessor().getObject(), "aq");
				//short[] array = (short[]) Reflection.getField(curModel.getIAccessor().getObject(), "ae");
				short[] array = curModel.getIndices1();
				//System.out.println(Debug.arrayToString(array, "debug"));
				
				System.out.println(faces + ", " + array.length);
				//data.OpenGLModel.get.printReport();
			}*/
			
			//if (curModel != null)
			//	break;
			
			/*try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
		//}
	}
	
	@Override
	public Model getModel() {
		return curModel;
	}
	
	
	public boolean waitForMoving(long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		boolean antiban = false;
		Timer timer = new Timer(maxPeriod);
		while (timer.isRunning() || antiban) {
			antiban = false;
			
			if (isMoving())
				return true;
			
			if (breakConditions != null) {
				for (Condition breakCondition : breakConditions) {
					if (breakCondition != null && breakCondition.evaluate())
						return true;
				}
			}
			
			Time.sleep(50);
			
			if (doAntiban && Antiban.doAntiban()) {
				Time.sleepQuickest();
				antiban = true;
			}
		}
		
		return false;
	}
	
	public boolean waitForStopMoving(long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		boolean antiban = false;
		Timer timer = new Timer(maxPeriod);
		while (timer.isRunning() || antiban) {
			antiban = false;
			
			if (!isMoving())
				return true;
			
			if (breakConditions != null) {
				for (Condition breakCondition : breakConditions) {
					if (breakCondition != null && breakCondition.evaluate())
						return true;
				}
			}
			
			Time.sleep(50);
			
			if (doAntiban && Antiban.doAntiban()) {
				Time.sleepQuickest();
				antiban = true;
			}
		}
		
		return false;
	}
	
	public boolean waitForAnimation(Collection<Integer> animationIds, long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		
		boolean containsIdleAnimation = animationIds.contains(-1);
		
		boolean antiban = false;
		Timer timer = new Timer(maxPeriod);
		while (timer.isRunning() || antiban) {
			antiban = false;
			
			//System.out.println("Current Animation: " + getAnimationId());
			
			int animation = getAnimationId();
			
			if ((containsIdleAnimation || animation != -1) && animationIds.contains(animation))
				return true;
			
			if (breakConditions != null) {
				for (Condition breakCondition : breakConditions) {
					if (breakCondition != null && breakCondition.evaluate())
						return true;
				}
			}
			
			Time.sleep(50);
			
			if (doAntiban && Antiban.doAntiban()) {
				Time.sleepQuickest();
				antiban = true;
			}
		}
		
		return false;
	}
	
	public boolean waitForIdleAnimation(Collection<Integer> validAnimationIds, long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		
		Timer timer = new Timer(60000);
		Timer noAnimationTimer = null;
		
		while (timer.isRunning()) {
			if (breakConditions != null) {
				for (Condition breakCondition : breakConditions) {
					if (breakCondition.evaluate())
						return true;
				}
				
				//if (Calculations.distanceTo(curAaoAbsoluteTile) > 2)
				//	return true;
			}
			
			int animationId = getAnimationId();
			
			if (validAnimationIds != null) {
				if (animationId != -1 && validAnimationIds.contains(animationId)) {
					noAnimationTimer = null;
					Time.sleep(50);
					
					if (doAntiban && Antiban.doAntiban())
						Time.sleepQuickest();
					
					continue;
				}
			}
			
			// waits until animation id is -1 and waits to see if it is sustained for 4s, then breaks
			if (animationId == -1) {
				if (noAnimationTimer == null)
					noAnimationTimer = new Timer(4000);
				else if (!noAnimationTimer.isRunning())
					return true;
			}
			
			Time.sleep(50);
			
			if (doAntiban && Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		return false;
	}
	
	public boolean waitForInteracting(long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		return waitForInteracting(null, maxPeriod, doAntiban, breakConditions);
	}
	
	/**
	 * 
	 * @param validCharacter Specify null to wait for interaction with <i>any</i> Character.
	 * @param maxPeriod
	 * @param doAntiban
	 * @param breakConditions
	 * @return
	 */
	public boolean waitForInteracting(Character validCharacter, long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		
		boolean antiban = false;
		Timer timer = new Timer(maxPeriod);
		while (timer.isRunning() || antiban) {
			antiban = false;
			
			if (validCharacter == null) {
				int interactionIndex = getInteractingIndex();
				if (interactionIndex != -1)
					return true;
			} else {
				Character interacting = getInteracting();
				if (interacting != null && interacting.getObject() == validCharacter.getObject())
					return true;
			}
			
			if (breakConditions != null) {
				for (Condition breakCondition : breakConditions) {
					if (breakCondition.evaluate())
						return true;
				}
			}
			
			Time.sleep(50);
			
			if (doAntiban && Antiban.doAntiban()) {
				Time.sleepQuickest();
				antiban = true;
			}
		}
		
		return false;
	}
	
	public boolean waitForIdleInteracting(long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		return waitForStopInteracting(null, maxPeriod, doAntiban, breakConditions);
	}
	
	/**
	 * 
	 * @param stopChar Character to wait to stop interacting with. Specify null to wait for idle interaction.
	 * @param maxPeriod
	 * @param doAntiban
	 * @param breakConditions
	 * @return
	 */
	public boolean waitForStopInteracting(Character stopChar, long maxPeriod, boolean doAntiban, Condition... breakConditions) {
		
		Timer timer = new Timer(60000);
		
		while (timer.isRunning()) {
			if (breakConditions != null) {
				for (Condition breakCondition : breakConditions) {
					if (breakCondition.evaluate())
						return true;
				}
			}
			
			//System.out.println("Current Interacting Index: " + getInteractingIndex());
			
			int interactionIndex = getInteractingIndex();
			if (interactionIndex == -1)
				return true;
			
			if (stopChar != null) {
				Character interacting = getInteracting();
				if (interacting != null && interacting.getObject() != stopChar.getObject())
					return true;
			}
			
			Time.sleep(50);
			
			if (doAntiban && Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		return false;
	}
	
	
	public void draw(Graphics g) {
		if (curModel == null) {
			//updateModel();
		} else {
			curModel.draw(g, getData().getCenterLocation().getX(), getData().getCenterLocation().getY(), getPlane());
		}
	}
	
	public void render(GL2 gl2) {
		if (curModel == null) {
			//updateModel();
		} else {
			curModel.render(gl2, getData().getCenterLocation().getX(), getData().getCenterLocation().getY(), getPlane());
		}
	}
}
