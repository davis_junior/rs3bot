package org.rs3.api.wrappers;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;

import org.rs3.accessors.I_Graphics;


public class Graphics extends AccessorWrapper {
	
	protected Graphics(Object obj) {
		super(obj);
	}
	
	public static Graphics create(Object obj) {
		return create(Graphics.class, obj);
	}
	
	
	public DisplayMode getDisplayMode() {
		return I_Graphics.getDisplayMode(obj);
	}
	
	public GraphicsDevice getGraphicsDevice() {
		return I_Graphics.getGraphicsDevice(obj);
	}
}
