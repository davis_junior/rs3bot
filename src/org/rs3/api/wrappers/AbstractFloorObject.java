package org.rs3.api.wrappers;

public abstract class AbstractFloorObject extends Interactable {
	
	protected AbstractFloorObject(Object obj) {
		super(obj);
	}
	
	public static AbstractFloorObject create(Object obj) {
		return create(AbstractFloorObject.class, obj);
	}
}
