package org.rs3.api.wrappers;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.Menu;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Mouse;
import org.rs3.api.objects.PreciseTile;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;


public abstract class Model extends AccessorWrapper { // extends NodeSub {
	
	public static final int TYPE_DEFAULTMODEL = 0;
	public static final int TYPE_SDMODEL = 1;
	public static final int TYPE_OPENGLMODEL = 2;
	public static final int TYPE_DIRECTXMODEL = 3;
	
	public int type;
	
	// cached vars
	public short[] indices1, indices2, indices3;
	public int[] xPoints, yPoints, zPoints;
	public int numFaces, numVertices;
	
	public int orientationMod;
	public int heightOff;
	
	protected Model(Object obj) {
		super(obj);
		
		this.type = TYPE_DEFAULTMODEL;
		
		update();
	}
	
	/**
	 * Note: Should not normally be used, use create(boolean, Object, int) instead
	 * 
	 * @param obj
	 * @param orientationMod
	 * @param heightOff
	 * @return
	 */
	public static Model create(Object obj, int orientationMod, int hieghtOff) {
		Model model = create(Model.class, obj);
		
		if (model == null || model.xPoints == null || model.yPoints == null || model.zPoints == null)
			return null;
		else {
			model.orientationMod = orientationMod;
			model.heightOff = hieghtOff;
			return model;
		}
	}
	
	public static Model create(boolean subModel, Object obj, int orientationMod, int heightOff) {
		if (!subModel)
			return create(obj, orientationMod, heightOff);
		else {
			Render render = Client.getRender();
			
			if (render instanceof SDRender)
				return SDModel.create(obj, orientationMod, heightOff);
			else if (render instanceof OpenGLRender)
				return OpenGLModel.create(obj, orientationMod, heightOff);
			else
				return Model.create(obj, orientationMod, heightOff);
		}
	}
	
	void update() {
		numFaces = getNumFaces();
		numVertices = getNumVertices();
		
		short[] indices1 = getIndices1();
		int[] xPoints = getXPoints();
		int[] yPoints = getYPoints();
		int[] zPoints = getZPoints();
		
		if (indices1 == null || xPoints == null || yPoints == null || zPoints == null)
			return;
		
		this.indices1 = indices1.clone();
		this.indices2 = getIndices2().clone();
		this.indices3 = getIndices3().clone();
		this.xPoints = xPoints.clone();
		this.yPoints = yPoints.clone();
		this.zPoints = zPoints.clone();
		
		//numFaces = Math.min(indices1.length, Math.min(indices2.length, indices3.length));
		//numVertices = Math.min(xPoints.length, Math.min(yPoints.length, zPoints.length));
		
		//System.out.println("Indices: (" + numFaces + ") " + indices1.length + ", " + indices2.length + ", " + indices3.length);
		//System.out.println("Vertices: (" + numVerticies + ") " + xPoints.length + ", " + yPoints.length + ", " + zPoints.length);
	}
	
	
	public abstract short[] getIndices1();
	public abstract short[] getIndices2();
	public abstract short[] getIndices3();
	
	public abstract int[] getXPoints();
	public abstract int[] getYPoints();
	public abstract int[] getZPoints();
	
	public abstract int getNumFaces();
	public abstract int getNumVertices();
	
	public abstract int[][] projectVertices(double locX, double locY, int plane);
	
	
	// TODO: not working for opengl only?
	// gets central point based on internal vertices
	public Point getAbsoluteCentralPoint(double locX, double locY, int plane) {
		if (numFaces < 1)
			return null;
		
		int totalXAverage = 0;
		int totalYAverage = 0;
		int totalHeightAverage = 0;
		int index = 0;
		
		//final int x = getLocalX();
		//final int y = getLocalY();
		final int height = Calculations.tileHeight((int) locX, (int) locY, plane);
		
		final int[][] screen = projectVertices(locX, locY, plane);
		
		while (index < numFaces) {
			int index1;
			int index2;
			int index3;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				index1 = (indices1[index] & 0xFFFF);
				index2 = (indices2[index] & 0xFFFF);
				index3 = (indices3[index] & 0xFFFF);
				break;
			default:
				index1 = indices1[index];
				index2 = indices2[index];
				index3 = indices3[index];
			}
			
			if (screen[index1][0] != -999999
					&& screen[index2][0] != -999999
					&& screen[index3][0] != -999999) {
				
				totalXAverage += (xPoints[index1] + xPoints[index2] + xPoints[index3]) / 3;
				totalYAverage += (zPoints[index1] + zPoints[index2] + zPoints[index3]) / 3;
				totalHeightAverage += (yPoints[index1] + yPoints[index2] + yPoints[index3]) / 3;
			}
			
			index++;
		}
		
		final Point averagePoint = Calculations.worldToScreen(
				(int) locX + totalXAverage / numFaces,
				height + totalHeightAverage / numFaces,
				(int) locY + totalYAverage / numFaces
		);

		if (Calculations.isOnScreen(averagePoint))
			return averagePoint;
		
		return null;
	}
	
	// gets central point based on projected vertices
	public Point getCentralPoint(double locX, double locY, int plane) {
		if (numFaces < 1)
			return null;
		
		Polygon[] bounds = getBounds(locX, locY, plane);
		
		if (bounds == null)
			return null;
		
		int totalXAverage = 0;
		int totalYAverage = 0;
		
		for (Polygon poly : bounds) {
			totalXAverage += (poly.xpoints[0] + poly.xpoints[1] + poly.xpoints[2]) / 3;
			totalYAverage += (poly.ypoints[0] + poly.ypoints[1] + poly.ypoints[2]) / 3;
		}
		
		final Point averagePoint = new Point(totalXAverage / bounds.length, totalYAverage / bounds.length);
		
		if (Calculations.isOnScreen(averagePoint))
			return averagePoint;
		
		return null;
	}
	
	// gets random point based on getShrunkBounds(...)
	// percent: 1-100
	public Point getRandomShrunkPoint(int percentage, double locX, double locY, int plane) {
		if (numFaces < 1)
			return null;
		
		Polygon[] shrunkBounds = getShrunkBounds(percentage, locX, locY, plane);
		
		if (shrunkBounds == null)
			return null;
		
		Polygon randomTriangle = shrunkBounds[Random.nextInt(0, shrunkBounds.length)];
		
		if (randomTriangle != null) {
			int r1 = Random.nextInt(0, 2);
			int r2 = Random.nextInt(0, 2);
			
			// TODO: test formula, was only partially tested
			int x = (int) Math.round((1 - Math.sqrt(r1)) * randomTriangle.xpoints[0] + (Math.sqrt(r1) * (1 - r2)) * randomTriangle.xpoints[1] + (Math.sqrt(r1) * r2) * randomTriangle.xpoints[2]);
			int y = (int) Math.round((1 - Math.sqrt(r1)) * randomTriangle.ypoints[0] + (Math.sqrt(r1) * (1 - r2)) * randomTriangle.ypoints[1] + (Math.sqrt(r1) * r2) * randomTriangle.ypoints[2]);
			
			return new Point(x, y);
		}
		
		return null;
	}
	
	public Point getNextViewportPoint(double locX, double locY, int plane) {
		final List<Point> foundPoints = new ArrayList<>();
		final int[][] screen = projectVertices(locX, locY, plane);
		
		for (int index = 0; index < numVertices; index++) {
			boolean check;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				check = (screen[index & 0xFFFF][0] != -999999);
			default:
				check = (screen[index][0] != -999999);
			}
			
			if (check)
				foundPoints.add(new Point(screen[index][0], screen[index][1]));
		}
		
		if (foundPoints.size() > 0 && isOnScreen(locX, locY, plane))
			return foundPoints.get(Random.nextInt(0, foundPoints.size()));
		
		return null;
	}
	
	public boolean contains(Point point, double locX, double locY, int plane) {
		final Polygon[] polygons = getBounds(locX, locY, plane);
		
		for (final Polygon triangle : polygons) {
			if (triangle.contains(point.x, point.y))
				return true;
		}
		
		return false;
	}
	
	public boolean isOnScreen(double locX, double locY, int plane) {
		final int[][] screen = projectVertices(locX, locY, plane);
		
		for (int index = 0; index < numFaces; index++) {
			int index1;
			int index2;
			int index3;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				index1 = (indices1[index] & 0xFFFF);
				index2 = (indices2[index] & 0xFFFF);
				index3 = (indices3[index] & 0xFFFF);
				break;
			default:
				index1 = indices1[index];
				index2 = indices2[index];
				index3 = indices3[index];
			}
			
			if (screen[index1][0] != -999999
					&& screen[index2][0] != -999999
					&& screen[index3][0] != -999999) {
				
				return true;
				
			}
		}
		
		return false;
	}
	
	public Polygon[] getBounds(double locX, double locY, int plane) {
		final int[][] screen = projectVertices(locX, locY, plane);
		ArrayList<Polygon> polys = new ArrayList<>(numFaces);
		
		for (int index = 0; index < numFaces; index++) {
			int index1;
			int index2;
			int index3;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				index1 = (indices1[index] & 0xFFFF);
				index2 = (indices2[index] & 0xFFFF);
				index3 = (indices3[index] & 0xFFFF);
				break;
			default:
				index1 = indices1[index];
				index2 = indices2[index];
				index3 = indices3[index];
			}
			
			final int xPoints[] = new int[3];
			final int yPoints[] = new int[3];
			
			xPoints[0] = screen[index1][0];
			yPoints[0] = screen[index1][1];
			xPoints[1] = screen[index2][0];
			yPoints[1] = screen[index2][1];
			xPoints[2] = screen[index3][0];
			yPoints[2] = screen[index3][1];
			
			if (screen[index1][0] != -999999
					&& screen[index2][0] != -999999
					&& screen[index3][0] != -999999) {
				
				polys.add(new Polygon(xPoints, yPoints, 3));
			}
		}
		
		if (polys.size() == 0)
			return null;
		
		return polys.toArray(new Polygon[polys.size()]);
	}
	
	// percent: 1-100
	public Polygon[] getShrunkBounds(int percentage, double locX, double locY, int plane) {
		if (percentage <= 0 || percentage > 100)
			return null;
		
		final int[][] screen = projectVertices(locX, locY, plane);
		ArrayList<Polygon> polys = new ArrayList<>(numFaces);
		
		float percent =  percentage / 100.0F;
		//float factor =  1.0F + percent;
		float inverse = 1.0F - percent;
		
		Point centralPoint = getCentralPoint(locX, locY, plane);
		
		if (centralPoint == null)
			return null;
		
		for (int index = 0; index < numFaces; index++) {
			int index1;
			int index2;
			int index3;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				index1 = (indices1[index] & 0xFFFF);
				index2 = (indices2[index] & 0xFFFF);
				index3 = (indices3[index] & 0xFFFF);
				break;
			default:
				index1 = indices1[index];
				index2 = indices2[index];
				index3 = indices3[index];
			}
			
			final int xPoints[] = new int[3];
			final int yPoints[] = new int[3];
			
			xPoints[0] = screen[index1][0];
			yPoints[0] = screen[index1][1];
			xPoints[1] = screen[index2][0];
			yPoints[1] = screen[index2][1];
			xPoints[2] = screen[index3][0];
			yPoints[2] = screen[index3][1];
			
			for (int i = 0; i < xPoints.length; i++) {
				float distanceCenterX = xPoints[i] - centralPoint.x;
				xPoints[i] -= distanceCenterX * inverse; 
			}
			
			for (int i = 0; i < yPoints.length; i++) {
				float distanceCenterY = yPoints[i] - centralPoint.y;
				yPoints[i] -= distanceCenterY * inverse; 
			}
			
			if (screen[index1][0] != -999999
					&& screen[index2][0] != -999999
					&& screen[index3][0] != -999999) {
				
				polys.add(new Polygon(xPoints, yPoints, 3));
			}
		}
		
		if (polys.size() == 0)
			return null;
		
		return polys.toArray(new Polygon[polys.size()]);
	}
	
	
	// gets a random point often close to the central point but sometimes fully random
	public Point getRandomPoint(double locX, double locY, int plane) {
		int rand = Random.nextInt(0, 100);
		
		if (rand >= 0 && rand <= 50) {
			return getRandomShrunkPoint(50, locX, locY, plane);
		} else if (rand > 50 && rand <= 75) {
			return getRandomShrunkPoint(75, locX, locY, plane);
		} else if (rand > 75) {
			return getNextViewportPoint(locX, locY, plane);
		}
		
		return null;
	}
	
	public boolean hover(double locX, double locY, int plane) {
		Point randPoint = getRandomPoint(locX, locY, plane);
		
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2)) != null)
			return true;
		
		return false;
	}
	
	public boolean click(int button, double locX, double locY, int plane) {
		Point randPoint = getRandomPoint(locX, locY, plane);
		
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2, button)) != null)
			return true;
		
		return false;
	}
	
	public boolean interact(String action, double locX, double locY, int plane) {
		return interact(true, action, locX, locY, plane);
	}

	public boolean interact(boolean hover, String action, double locX, double locY, int plane) {
		if (hover) {
			if (!hover(locX, locY, plane))
				return false;
			
			Time.sleepVeryQuick();
		}
		
		if (Menu.select(action)) {
			if (Random.nextInt(0, 100) <= 75) {
				Time.sleepVeryQuick();
				Antiban.randomMoveMouse();
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean interact(String action, String option, double locX, double locY, int plane) {
		return interact(true, action, option, locX, locY, plane);
	}

	public boolean interact(boolean hover, String action, String option, double locX, double locY, int plane) {
		if (hover) {
			if (!hover(locX, locY, plane))
				return false;
			
			Time.sleepVeryQuick();
		}
		
		if (Menu.select(action, option)) {
			if (Random.nextInt(0, 100) <= 75) {
				Time.sleepVeryQuick();
				Antiban.randomMoveMouse();
			}
			
			return true;
		}
		
		return false;
	}
	
	
	public void draw(Graphics g, double locX, double locY, int plane) {
		final int[][] screen = projectVertices(locX, locY, plane);
		
		for (int index = 0; index < this.numFaces; index++) {
			int index1;
			int index2;
			int index3;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				index1 = (indices1[index] & 0xFFFF);
				index2 = (indices2[index] & 0xFFFF);
				index3 = (indices3[index] & 0xFFFF);
				break;
			default:
				index1 = indices1[index];
				index2 = indices2[index];
				index3 = indices3[index];
			}
			
			if (screen[index1][0] != -999999
					&& screen[index2][0] != -999999
					&& screen[index3][0] != -999999) {
				
				int point1X = screen[index1][0];
				int point1Y = screen[index1][1];
				int point2X = screen[index2][0];
				int point2Y = screen[index2][1];
				int point3X = screen[index3][0];
				int point3Y = screen[index3][1];
				
				//g.drawLine(point1X, point1Y, point2X, point2Y);
				//g.drawLine(point2X, point2Y, point3X, point3Y);
				//g.drawLine(point3X, point3Y, point1X, point1Y);
				
				Polygon polygon = new Polygon(new int[] {point1X, point2X, point3X},  new int[] {point1Y, point2Y, point3Y}, 3);
				
				/*Graphics2D g2 = (Graphics2D) g;
				Composite originalComp = g2.getComposite();
				
				AlphaComposite alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3F);
				g2.setComposite(alphaComp);
				g.fillPolygon(polygon);
				
				alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1F);
				g2.setComposite(alphaComp);
				g.drawPolygon(polygon);
				
				g2.setComposite(originalComp);*/
				
				g.drawPolygon(polygon);
			}
		}
	}
	
	public void render(GL2 gl2, double locX, double locY, int plane) {
		final int[][] screen = projectVertices(locX, locY, plane);
		
		for (int index = 0; index < this.numFaces; index++) {
			int index1;
			int index2;
			int index3;
			
			switch (type) {
			case TYPE_OPENGLMODEL:
				index1 = (indices1[index] & 0xFFFF);
				index2 = (indices2[index] & 0xFFFF);
				index3 = (indices3[index] & 0xFFFF);
				break;
			default:
				index1 = indices1[index];
				index2 = indices2[index];
				index3 = indices3[index];
			}
			
			if (screen[index1][0] != -999999
					&& screen[index2][0] != -999999
					&& screen[index3][0] != -999999) {
				
				int point1X = screen[index1][0];
				int point1Y = screen[index1][1];
				int point2X = screen[index2][0];
				int point2Y = screen[index2][1];
				int point3X = screen[index3][0];
				int point3Y = screen[index3][1];
				
				//g.drawLine(point1X, point1Y, point2X, point2Y);
				//g.drawLine(point2X, point2Y, point3X, point3Y);
				//g.drawLine(point3X, point3Y, point1X, point1Y);
				
		        gl2.glBegin( GL.GL_LINE_LOOP );
		        gl2.glColor3f( 0, 1, 0 );
		        gl2.glVertex2f( point1X, point1Y );
		        gl2.glVertex2f( point2X, point2Y );
		        gl2.glVertex2f( point3X, point3Y );
		        gl2.glFlush();
		        gl2.glEnd();
		        
		        // fill with alpha
		        gl2.glBegin( GL.GL_TRIANGLES );
		        gl2.glColor4f( 0, 1, 0, 0.3F );
		        gl2.glVertex2f( point1X, point1Y );
		        gl2.glVertex2f( point2X, point2Y );
		        gl2.glVertex2f( point3X, point3Y );
		        gl2.glFlush();
		        gl2.glEnd();
			}
		}
	}
	
	// convenience methods
	public Point getAbsoluteCentralPoint(PreciseTile internalTile) {
		return getAbsoluteCentralPoint(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Point getAbsoluteCentralPoint(Location loc, int plane) {
		return getAbsoluteCentralPoint(loc.getX(), loc.getY(), plane);
	}
	
	public Point getCentralPoint(PreciseTile internalTile) {
		return getCentralPoint(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Point getCentralPoint(Location loc, int plane) {
		return getCentralPoint(loc.getX(), loc.getY(), plane);
	}
	
	public Point getRandomShrunkPoint(int percentage, PreciseTile internalTile) {
		return getRandomShrunkPoint(percentage, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Point getRandomShrunkPoint(int percentage, Location loc, int plane) {
		return getRandomShrunkPoint(percentage, loc.getX(), loc.getY(), plane);
	}
	
	public Point getNextViewportPoint(PreciseTile internalTile) {
		return getNextViewportPoint(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Point getNextViewportPoint(Location loc, int plane) {
		return getNextViewportPoint(loc.getX(), loc.getY(), plane);
	}
	
	public boolean contains(Point point, PreciseTile internalTile) {
		return contains(point, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean contains(Point point, Location loc, int plane) {
		return contains(point, loc.getX(), loc.getY(), plane);
	}
	
	public boolean isOnScreen(PreciseTile internalTile) {
		return isOnScreen(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean isOnScreen(Location loc, int plane) {
		return isOnScreen(loc.getX(), loc.getY(), plane);
	}
	
	
	public Polygon[] getBounds(PreciseTile internalTile) {
		return getBounds(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Polygon[] getBounds(Location loc, int plane) {
		return getBounds(loc.getX(), loc.getY(), plane);
	}
	
	public Polygon[] getShrunkBounds(int percentage, PreciseTile internalTile) {
		return getShrunkBounds(percentage, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Polygon[] getShrunkBounds(int percentage, Location loc, int plane) {
		return getShrunkBounds(percentage, loc.getX(), loc.getY(), plane);
	}
	
	
	public int[][] projectVertices(PreciseTile internalTile) {
		return projectVertices(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public int[][] projectVertices(Location loc, int plane) {
		return projectVertices(loc.getX(), loc.getY(), plane);
	}
	
	
	public Point getRandomPoint(PreciseTile internalTile) {
		return getRandomPoint(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public Point getRandomPoint(Location loc, int plane) {
		return getRandomPoint(loc.getX(), loc.getY(), plane);
	}
	
	public boolean hover(PreciseTile internalTile) {
		return hover(internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean hover(Location loc, int plane) {
		return hover(loc.getX(), loc.getY(), plane);
	}
	
	public boolean click(int button, PreciseTile internalTile) {
		return click(button, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean click(int button, Location loc, int plane) {
		return click(button, loc.getX(), loc.getY(), plane);
	}
	
	public boolean interact(boolean hover, String action, PreciseTile internalTile) {
		return interact(hover, action, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean interact(boolean hover, String action, Location loc, int plane) {
		return interact(hover, action, loc.getX(), loc.getY(), plane);
	}
	
	public boolean interact(boolean hover, String action, String option, PreciseTile internalTile) {
		return interact(hover, action, option, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean interact(boolean hover, String action, String option, Location loc, int plane) {
		return interact(hover, action, option, loc.getX(), loc.getY(), plane);
	}
	
	public boolean interact(String action, PreciseTile internalTile) {
		return interact(action, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean interact(String action, Location loc, int plane) {
		return interact(action, loc.getX(), loc.getY(), plane);
	}
	
	public boolean interact(String action, String option, PreciseTile internalTile) {
		return interact(action, option, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public boolean interact(String action, String option, Location loc, int plane) {
		return interact(action, option, loc.getX(), loc.getY(), plane);
	}
	
	
	public void draw(Graphics g, PreciseTile internalTile) {
		draw(g, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public void draw(Graphics g, Location loc, int plane) {
		draw(g, loc.getX(), loc.getY(), plane);
	}
	
	public void render(GL2 gl2, PreciseTile internalTile) {
		render(gl2, internalTile.getX(), internalTile.getY(), internalTile.getPlane());
	}
	
	public void render(GL2 gl2, Location loc, int plane) {
		render(gl2, loc.getX(), loc.getY(), plane);
	}
	
	
	public static final double[] D2R_SIN_TABLE = new double[360];
	public static final double[] D2R_COS_TABLE = new double[360];
	
	static {
		final double d = 0.017453292519943295; // ~1 degree in radians
		for (int i = 0; i < 360; i++) {
			D2R_SIN_TABLE[i] = Math.sin(i * d);
			D2R_COS_TABLE[i] = Math.cos(i * d);
		}	
	}
}
