package org.rs3.api;

import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.wrappers.Client;

public enum CombatMode {

	FULL_MANUAL("Full Manual", 0),
	REVOLUTION("Revolution", 1),
	MOMENTUM("Momentum", 2),
	LEGACY("Legacy", -1),
	;
	
	String name;
	int settingValue;
	
	private CombatMode(String name, int settingValue) {
		this.name = name;
		this.settingValue = settingValue;
	}
	
	/**
	 * isActive() is preferred because it checks if Legacy mode is active.
	 * @return
	 */
	public boolean isSelected() {
		if (this.equals(LEGACY))
			return isLegacyModeActive();
		
		int combatModeSetting = Client.getSettingData().getSettings().get(627, 10, 0x3);
		return combatModeSetting == this.settingValue;
	}
	
	public boolean isActive() {
		if (this.equals(LEGACY))
			return isLegacyModeActive();
		else if (isLegacyModeActive())
			return false;
		
		return isSelected();
	}
	
	public boolean activate() {
		return activate(false);
	}
	
	public boolean activate(boolean waitAfter) {
		if (isActive())
			return true;
		
		// TODO: if already in Legacy mode, you must disable it from the powers subinterface, also must handle switch to EOC widget
		
		if (MainWidget.get.maximiseActionBar(true) && ActionBarWidget.get.isOpen()) {
			// TODO: if switching to Legacy mode, also must handle switch to Legacy widget
			return ActionBarWidget.get.interactCombatSettingsButton("Combat Mode: " + this.name, waitAfter);
		}
		
		return false;
	}
	
	public static boolean isLegacyModeActive() {
		int legacySetting = Client.getSettingData().getSettings().get(4332, 31, 0x1);
		return legacySetting == 1;
	}
	
	/**
	 * Excludes Legacy mode, use getActive() for Legacy mode
	 * @return
	 */
	public static CombatMode getSelected() {
		for (CombatMode mode : values()) {
			if (mode.equals(LEGACY))
				continue;
			if (mode.isSelected())
				return mode;
		}
		
		return null;
	}
	
	public static CombatMode getActive() {
		for (CombatMode mode : values()) {
			if (mode.isActive())
				return mode;
		}
		
		return null;
	}
}
