package org.rs3.api;

import java.util.ArrayList;

import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.HashTable;
import org.rs3.api.wrappers.ItemNode;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.NodeDeque;
import org.rs3.api.wrappers.NodeListCache;

public class GroundItems {
	
	public static GroundItem[] getAll() {
		return getAll(false);
	}
	
	public static GroundItem[] getAll(boolean onScreenOnly) {
		ArrayList<GroundItem> items = new ArrayList<GroundItem>();
		
		Node[] nodes = Client.getItemNodeCache().getAll();
		
		if (nodes != null) {
			for (Node n : nodes) {
				NodeListCache cache = n.forceCast(NodeListCache.class);
				NodeDeque nodeList = cache.getNodeList();
				
				for (Node curr = nodeList.getHead(); curr != null; curr = nodeList.getNext()) {
					if (curr.getObject().getClass().getName().equals(data.ItemNode.get.getInternalName())) {
						long id = n.getId();
						
						// refactored from client
			            int x = (int)(id & 0x3FFF); // includes base
			            int y = (int)(id >> 14 & 0x3FFF); // includes base
			            int plane = (int)(id >> 28 & 0x3);
			            
						Tile tile = new Tile(x, y, plane);
						
						if (onScreenOnly && !tile.isOnScreen())
							continue;
						
						items.add(new GroundItem(tile, curr.forceCast(ItemNode.class)));
					}
				}
			}
		}
		
		if (items.size() != 0)
			return items.toArray(new GroundItem[0]);
		else
			return null;
	}
	
	public static GroundItem[] getAllOnScreen() {
		return getAll(true);
	}
	
	public static GroundItem[] getItemsAt(int x, int y) {
		try {
			ArrayList<GroundItem> items = new ArrayList<GroundItem>();
			HashTable itemTable = Client.getItemNodeCache();
			
			int index = x | y << 14 | Client.getPlane() << 28;
			Node tileNode = Nodes.lookup(itemTable, index);
			
			if (tileNode != null) {
				NodeListCache cache = tileNode.forceCast(NodeListCache.class);
				NodeDeque nodeList = cache.getNodeList();
				
				for (Node curr = nodeList.getHead(); curr != null; curr = nodeList.getNext()) {
					if (curr.getObject().getClass().getName().equals(data.ItemNode.get.getInternalName())) {
						Tile tile = new Tile(x, y, Client.getPlane());
						items.add(new GroundItem(tile, curr.forceCast(ItemNode.class)));
					}
				}
			}
			
			if (items.size() != 0)
				return items.toArray(new GroundItem[0]);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
