package org.rs3.api.exchange;

import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rs3.api.GESlot;
import org.rs3.api.ItemTable;
import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.database.GrandExchangeRow;
import org.rs3.scripts.Miner;
import org.rs3.scripts.WineCollector;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.tanning.HideType;
import org.rs3.scripts.woodcutting.LogType;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class ExchangeLoop {
	
	private static final Logger log = Logger.getLogger("ExchangeLoop-debug");
	
	/**
	 * for use in scripts, how often to go to the GE to make exchanges, not used in loop implementation, for script use
	 */
	private Timer mainExchangeTimer;
	private ExchangeItem[] exchangeItems;
	/**
	 * for use in the exchange loop, how often to check prices, change offer prices, etc.
	 */
	private Timer exchangeTimer;
	private boolean retrievedItemsBank = false;
	private boolean putInSellOffers = false;
	private boolean putInBuyOffers = false;
	
	private static final int updatePeriod = 90000; // 90s
	
	ExchangeItem[] defaultSellItems = new ExchangeItem[] {
			new ExchangeItem(OreType.CLAY.getItemId(), OreType.CLAY.getItemName(), true),
			new ExchangeItem(Miner.softClayId, Miner.softClayName, true),
			new ExchangeItem(OreType.COPPER.getItemId(), OreType.COPPER.getItemName(), true),
			new ExchangeItem(OreType.IRON.getItemId(), OreType.IRON.getItemName(), true),
			new ExchangeItem(LogType.NORMAL.getItemId(), LogType.NORMAL.getItemName(), true),
			new ExchangeItem(LogType.OAK.getItemId(), LogType.OAK.getItemName(), true),
			new ExchangeItem(HideType.GREEN_DRAGONHIDE.getLeatherId(), HideType.GREEN_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(HideType.BLUE_DRAGONHIDE.getLeatherId(), HideType.BLUE_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(HideType.RED_DRAGONHIDE.getLeatherId(), HideType.RED_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(HideType.BLACK_DRAGONHIDE.getLeatherId(), HideType.BLACK_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(WineCollector.wineId, WineCollector.wineName, true),
	};
	
	public ExchangeLoop(Timer mainExchangeTimer, ExchangeItem[] exchangeItems) {
		this.mainExchangeTimer = mainExchangeTimer;
		this.exchangeItems = exchangeItems;
		
		for (ExchangeItem item : defaultSellItems) {
			addExchangeItem(item);
		}
	}
	
	public ExchangeLoop(long mainExchangePeriod, ExchangeItem[] exchangeItems) {
		this.mainExchangeTimer = new Timer(mainExchangePeriod);
		this.exchangeItems = exchangeItems;
		
		for (ExchangeItem item : defaultSellItems) {
			addExchangeItem(item);
		}
	}
	
	public Timer getMainExchangeTimer() {
		return mainExchangeTimer;
	}
	
	public void setMainExchangeTimer(Timer mainExchangeTimer) {
		this.mainExchangeTimer = mainExchangeTimer;
	}
	
	public void setMainExchangePeriod(long period) {
		this.mainExchangeTimer = new Timer(period);
	}
	
	public boolean isExchangeItem(int itemId) {
		for (ExchangeItem item : exchangeItems) {
			if (item.getItemId() == itemId)
				return true;
		}
		
		return false;
	}
	
	public void addExchangeItem(ExchangeItem item) {
		if (isExchangeItem(item.getItemId()))
			return;
		
		ExchangeItem[] newItems = new ExchangeItem[exchangeItems.length + 1];
		for (int i = 0; i < exchangeItems.length; i++) {
			newItems[i] = exchangeItems[i];
		}
		newItems[newItems.length - 1] = item;
		
		exchangeItems = newItems;
	}
	
	public void resetExchangeData() {
		if (mainExchangeTimer != null)
			mainExchangeTimer.reset();
		
		retrievedItemsBank = false;
		
		exchangeTimer = null;
		
		for (ExchangeItem item : exchangeItems) {
			item.setPrice(-1);
			item.setLastUpdate(null);
			item.disableExchange();
		}
		
		putInSellOffers = false;
		putInBuyOffers = false;
	}
	
	/**
	 * For selling
	 * @return
	 */
	public boolean retrieveItemsBank() {
		if (retrievedItemsBank)
			return true;
		
		log.fine("Retrieving items from bank...");
		
		if (!GrandExchangeWidget.get.close(true))
			return false;
		
		if (Bank.get.open(true)) {
			Time.sleepQuick();
			
			if (Bank.get.depositAllInvItems()) {
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					if (!Bank.get.withdrawAll(995, false)) // coins
						return false;
					Time.sleepQuick();
				}
				
				//
				// withdraw all items
				//
				for (ExchangeItem item : exchangeItems) {
					if (item.shouldSell()) {
						if (ItemTable.BANK.getStackSize(item.getItemId()) > 1) {
							int count = ItemTable.BANK.getStackSize(item.getItemId()) + ItemTable.INVENTORY.getStackSize(item.getItemId()) + ItemTable.EQUIPMENT.getStackSize(item.getItemId());
							int priceAll = -1;
							GrandExchangeRow row = GrandExchangeRow.select(item.getItemId());
							if (row != null) {
								if (row.getSellPrice() > 0) {
									priceAll = count*row.getSellPrice();
								}
							}
							
							// only sell if total value is at least 100k or price isn't in database
							if (priceAll == -1 || priceAll >= 100000) {
								item.enableExchange();
								
								if (!Bank.get.withdraw(item.getItemId(), -1, true))
									return false;
								
								Time.sleepQuick();
							} else
								item.disableExchange();
						}
					}
				}
				
				Time.sleepMedium();
				
				// if there are still items in bank, exit
				for (ExchangeItem item : exchangeItems) {
					int stackSize = ItemTable.BANK.getStackSize(item.getItemId());
					item.setBankCount(stackSize);
					
					if (item.shouldSell() && item.shouldExchange()) {
						if (stackSize > 1)
							return false;
					}
				}
				// else
				retrievedItemsBank = true;
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @return true if all exchanges are made and script is ready to continue, false otherwise
	 */
	public boolean exchange() {
		if (retrievedItemsBank && putInSellOffers && putInBuyOffers) {
			System.out.println("Exchange completed, script will continue momentarily...");
			
			if (Bank.get.close(true)) {
				Time.sleepQuick();
				
				GrandExchangeWidget.get.open(true);
				if (!GrandExchangeWidget.get.isOpen())
					Time.sleepMedium();
				Time.sleepQuick();
				
				if (GrandExchangeWidget.get.isOpen()) {
					if (!GrandExchangeWidget.get.collectAllToInventory())
						return false;
					
					resetExchangeData();
					
					return true;
				}
			}
		}
		
		log.setLevel(Level.ALL);
		if (exchangeTimer == null || !exchangeTimer.isRunning()) {
			if (!retrieveItemsBank())
				return false;
			
			if (!Bank.get.close(true))
				return false;
			
			log.finest("Opening Grand Exchange...");
			if (GrandExchangeWidget.get.open(true)) {
				Time.sleepQuick();
				
				if (!GrandExchangeWidget.get.isOpen())
					Time.sleepMedium();
				
				if (GrandExchangeWidget.get.isOpen()) {
					// collect to inv anything that needs to be collected
					for (GESlot slot : GESlot.values()) {
						if (slot.isValid()) {
							GEOfferStatus status = slot.getStatus();
							if (status.isFinalized()) {
								slot.collect(true);
								Time.sleepMedium();
								Time.sleepQuick();
							}
						}
					}
				} else
					return false;
				
				// double check, return if a finalized slot is not collected
				for (GESlot slot : GESlot.values()) {
					if (slot.isValid()) {
						GEOfferStatus status = slot.getStatus();
						if (status.isFinalized())
							return false;
					}
				}
				
				// abort offers with same item ids
				for (GESlot slot : GESlot.values()) {
					if (slot.isValid()) {
						for (ExchangeItem item : exchangeItems) {
							if (!putInSellOffers && item.shouldSell()) {
								if (slot.getItemId() == item.getItemId()) {
									slot.abort();
									Time.sleepMedium();
									Time.sleepQuick();
								}
							} else if (!putInBuyOffers && item.shouldBuy()) {
								if (slot.getItemId() == item.getItemId()) {
									slot.abort();
									Time.sleepMedium();
									Time.sleepQuick();
								}
							}
						}
					}
				}
				
				// double check, return if offers not aborted
				for (GESlot slot : GESlot.values()) {
					if (slot.isValid()) {
						for (ExchangeItem item : exchangeItems) {
							if (!putInSellOffers && item.shouldSell()) {
								if (slot.getItemId() == item.getItemId())
									return false;
							} else if (!putInBuyOffers && item.shouldBuy()) {
								if (slot.getItemId() == item.getItemId())
									return false;
							}
						}
					}
				}
				
				if (GESlot.getNextEmptySlot() == null) {
					log.severe("No empty slot!");
					Time.sleepMedium();
					return false;
				}
				
				
				if (!putInSellOffers())
					return false;
				
				if (!putInBuyOffers())
					return false;
				
				exchangeTimer = new Timer(Random.nextInt(60000, 120000)); // 1 to 2 mins.
			}
		} else
			Time.sleep(5000);
		
		return false;
	}
	
	private boolean putInSellOffers() {
		for (ExchangeItem item : exchangeItems) {
			if (item.shouldSell()) {
				//
				// check item prices
				//
				if (ItemTable.INVENTORY.containsAny(item.getItemId(), item.getItemId() + 1)
						|| GESlot.anyContainItem(item.getItemId())) {
					item.enableExchange();
				} else
					item.disableExchange();
				
				final int updatePeriod = 90000; // 90s
				
				if (item.shouldExchange()) {
					Timestamp curTimestamp = new Timestamp(System.currentTimeMillis());
					
					GrandExchangeRow row = GrandExchangeRow.select(item.getItemId());
					if (row == null) {
						row = new GrandExchangeRow();
						row.setItemId(item.getItemId());
						row.setBuyPrice(-1);
						row.setSellPrice(-1);
						if (row.executeInsert() == -1)
							return false;
					}
					
					item.setPrice(row.getSellPrice());
					item.setLastUpdate(row.getLastUpdate());
					
					if (item.getLastUpdate() == null || curTimestamp.getTime() - item.getLastUpdate().getTime() > updatePeriod) {
						// if we have enough money to check prices
						if (ItemTable.MONEYPOUCH.getCoinCount() >= 500) { // TODO: use current market price +25%
							GESlot slot = GESlot.getRandomEmptySlot();
							if (slot == null) {
								System.out.println("No empty slots for price check!");
								return false;
							}
							
							row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
							if (row.executeUpdate() == -1)
								return false;
							
							log.fine("Checking item price...");
							int[] prices = slot.checkPrices(item.getItemId(), item.getItemName(), 30000);
							if (prices == null) {
								row.setLastUpdate(null);
								row.executeUpdate();
								return false;
							}
							
							if (prices[1] - prices[0] <= 30) {
								if (item.getPrice() != prices[0])
									item.setPrice(prices[0] - 1);
							} else if (item.getPrice() != prices[1])
								item.setPrice(prices[1] - 1);
							
							row.setSellPrice(item.getPrice());
							row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
							if (row.executeUpdate() == -1)
								return false;
						} else { // use default or last database values
							if (item.getPrice() == -1)
								System.out.println("Not enough coins for price check! Using market price -15%");
						}
					}
				}
				
				//
				// sell ores
				//
				if (item.shouldExchange()) {
					GESlot curSlot = GESlot.findByItemId(item.getItemId());
					if (curSlot == null)
						curSlot = GESlot.getNextEmptySlot();
					if (curSlot == null)
						return false;
					
					if ((!curSlot.getStatus().isEmpty() 
							&& (curSlot.getItemId() != item.getItemId()
							|| curSlot.getAskingPriceEach() != item.getPrice())
							|| ItemTable.INVENTORY.containsAny(item.getItemId(), item.getItemId() + 1))) {
						
						log.fine("Selling " + item.getItemName() + "...");
						if (item.getPrice() != -1 && !curSlot.sell(item.getItemId(), -1, item.getPrice()))
							return false;
						else if (item.getPrice() == -1 && !curSlot.sellByMarketPrice(item.getItemId(), -1, -3))
							return false;
						
						Time.sleepMedium();
						Time.sleepQuick();
					}
				}
			}
		}
		
		putInSellOffers = true;
		return true;
	}
	
	private boolean putInBuyOffers() {
		for (ExchangeItem item : exchangeItems) {
			if (item.shouldBuy()) {
				//
				// check if there are already enough items
				//
				int itemId = item.getItemId();
				int itemCount = item.getBankCount() 
						+ ItemTable.INVENTORY.getStackSize(itemId) 
						+ ItemTable.INVENTORY.getStackSize(itemId + 1) 
						+ ItemTable.EQUIPMENT.getStackSize(itemId) // arrows, etc
						+ ItemTable.GESLOT1.getStackSize(itemId) 
						+ ItemTable.GESLOT2.getStackSize(itemId) 
						+ ItemTable.GESLOT3.getStackSize(itemId) 
						+ ItemTable.GESLOT4.getStackSize(itemId) 
						+ ItemTable.GESLOT5.getStackSize(itemId) 
						+ ItemTable.GESLOT6.getStackSize(itemId) 
						+ ItemTable.GESLOT7.getStackSize(itemId) 
						+ ItemTable.GESLOT8.getStackSize(itemId);
				if (itemCount < 0)
					itemCount = 0;
				
				if (itemCount >= item.getMaxToBuy()) {
					item.disableExchange();
					//putInBuyOffers = true;
					//return true;
					continue;
				} else
					item.enableExchange();
				
				//
				// check item prices
				//
				if (item.shouldExchange()) {
					Timestamp curTimestamp = new Timestamp(System.currentTimeMillis());
					
					GrandExchangeRow row = GrandExchangeRow.select(item.getItemId());
					if (row == null) {
						row = new GrandExchangeRow();
						row.setItemId(item.getItemId());
						row.setBuyPrice(-1);
						row.setSellPrice(-1);
						if (row.executeInsert() == -1)
							return false;
					}
					
					item.setPrice(row.getBuyPrice());
					item.setLastUpdate(row.getLastUpdate());
					
					if (item.getLastUpdate() == null || curTimestamp.getTime() - item.getLastUpdate().getTime() > updatePeriod) {
						GESlot slot = GESlot.getRandomEmptySlot();
						if (slot == null) {
							System.out.println("No empty slots for price check!");
							return false;
						}
						
						row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
						if (row.executeUpdate() == -1)
							return false;
						
						log.fine("Checking item price...");
						int[] prices = slot.checkPrices(item.getItemId(), item.getItemName(), 30000);
						if (prices == null) {
							row.setLastUpdate(null);
							row.executeUpdate();
							return false;
						}
						
						if (prices[1] - prices[0] <= 30) {
							if (item.getPrice() != prices[1])
								item.setPrice(prices[1] + 1);
						} else if (item.getPrice() != prices[0])
							item.setPrice(prices[0] + 1);
						
						row.setBuyPrice(item.getPrice());
						row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
						if (row.executeUpdate() == -1)
							return false;
					}
				}
				
				if (item.getPrice() == -1)
					return false;
				
				if (item.shouldExchange()) {
					//
					// buy items
					//
					int itemsToBuy = item.getMaxToBuy() - itemCount;
					if (itemsToBuy > 0) { // now check if we have enough money
						int coins = ItemTable.MONEYPOUCH.getCoinCount();
						int maxToBuy = (coins - item.getExtraCostPerItem()*item.getMaxToBuy() - item.getMaxToBuy()) / item.getPrice(); // subtract 10000 just to keep 10k in money pouch
						if (maxToBuy < itemsToBuy)
							itemsToBuy = maxToBuy;
					}
					
					if (itemsToBuy > 0) {
						GESlot slot = GESlot.findByItemId(item.getItemId());
						if (slot == null || slot.getAskingPriceEach() != item.getPrice()) {
							if (slot == null)
								slot = GESlot.getNextEmptySlot();
							
							log.fine("Buying items...");
							if (!slot.buy(item.getItemId(), item.getItemName(), itemsToBuy, item.getPrice()))
								return false;
							
							Time.sleepMedium();
							Time.sleepQuick();
						}
					}
				}
			}
		}
		
		putInBuyOffers = true;
		return true;
	}
	
	public static boolean buyEquipment(int minMoneyPouch, ExchangeItem... items) {
		int[] buyEquipmentIds = new int[items.length];
		for (int i = 0; i < buyEquipmentIds.length; i++) {
			buyEquipmentIds[i] = items[i].getItemId();
		}
		
		// equip staff if bought
		if (!ItemTable.EQUIPMENT.containsAll(buyEquipmentIds) && GrandExchangeWidget.get.open(true)) {
			Time.sleepQuick();
			
			boolean boughtEquipment = true;
			for (ExchangeItem item : items) {
				boolean boughtItem = ItemTable.BANK.getSnapshot().containsAny(item.getItemId()) || ItemTable.EQUIPMENT.containsAny(item.getItemId()) || ItemTable.INVENTORY.containsAny(item.getItemId()) || ItemTable.INVENTORY.containsAny(item.getItemId() + 1);
				if (!boughtItem)
					boughtEquipment = false;
				
			}
			
			// buy equipment
			if (!boughtEquipment /*&& !retrievedEquipment*/ && !ItemTable.EQUIPMENT.containsAll(buyEquipmentIds) && ItemTable.MONEYPOUCH.getCoinCount() >= minMoneyPouch) {
				for (ExchangeItem item : items) {
					boolean boughtItem = ItemTable.BANK.getSnapshot().containsAny(item.getItemId()) || ItemTable.EQUIPMENT.containsAny(item.getItemId()) || ItemTable.INVENTORY.containsAny(item.getItemId()) || ItemTable.INVENTORY.containsAny(item.getItemId() + 1);
					
					if (!boughtItem && !GESlot.anyContainItem(item.getItemId())) {
						System.out.println("Buying " + item.getItemName() + "...");
						GESlot slot = GESlot.getRandomEmptySlot();
						if (slot != null && slot.buy(item.getItemId(), item.getItemName(), 1, item.getPrice()))
							boughtItem = true;
						else
							return false;
						
						Time.sleepMedium();
					}
					
					if (!boughtItem)
						boughtEquipment = false;
				}
			}
			
			// Wait to see if equipment is bought, then collect
			if (/*boughtEquipment && !retrievedEquipment &&*/ !ItemTable.EQUIPMENT.containsAll(buyEquipmentIds)) {
				// make sure there is an offer
				boolean offerExists = false;
				Timer timer = new Timer(5000);
				while (timer.isRunning()) {
					for (int itemId : buyEquipmentIds) {
						if (GESlot.anyContainItem(itemId)) {
							offerExists = true;
							break;
						}
					}
					
					if (offerExists)
						break;
					
					Time.sleep(50);
				}
				
				if (offerExists) {
					timer = new Timer(300000); // 5 mins
					while (timer.isRunning()) {
						boolean finalised = false;
						for (int itemId : buyEquipmentIds) {
							GESlot equipmentSlot = GESlot.findByItemId(itemId);
							if (equipmentSlot != null && equipmentSlot.getStatus().isFinalized()) {
								finalised = true;
								Time.sleepQuick();
								break;
							}
						}
						
						if (finalised)
							break;
						
						Time.sleep(5000);
					}
				}
			}
			
			// Retrieve equipment if in GE (regardless if any has been bought during this script)
			for (int itemId : buyEquipmentIds) {
				GESlot equipmentSlot = GESlot.findByItemId(itemId);
				if (equipmentSlot != null && equipmentSlot.getStatus().isFinalized()) {
					//boughtEquipment = true;
					
					if (!equipmentSlot.collect(false))
						return false;
					
					Time.sleepMedium();
					Time.sleepQuick();
				}
			}
			
			return true;
		} else if (!ItemTable.EQUIPMENT.containsAll(buyEquipmentIds) && ItemTable.MONEYPOUCH.getCoinCount() >= minMoneyPouch)
			return false;
		else
			return true;
	}
}
