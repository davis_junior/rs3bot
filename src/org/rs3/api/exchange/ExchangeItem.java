package org.rs3.api.exchange;

import java.sql.Timestamp;

public class ExchangeItem {

	private int itemId;
	private String itemName;
	private boolean sell;
	private int maxToBuy; // for use in buying only; considers bank, inventory, and ge slot items
	private int extraCostPerItem; // for use in buying only. e.g., tanning hides require 20 coins. this will be used to figure out how many items should be bought depending on money pouch coins
	private int price = -1;
	private Timestamp lastUpdate = null;
	private boolean shouldExchange = false;
	private int bankCount = -1;
	
	public ExchangeItem(int itemId, String itemName, boolean sell) {
		this(itemId, itemName, sell, -1, 0);
	}
	
	public ExchangeItem(int itemId, String itemName, boolean sell, int maxToBuy, int extraCostPerItem) {
		this.itemId = itemId;
		this.itemName = itemName;
		this.sell = sell;
		this.maxToBuy = maxToBuy;
		this.extraCostPerItem = extraCostPerItem;
	}
	
	
	public int getItemId() {
		return itemId;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public boolean shouldSell() {
		return sell;
	}
	
	public boolean shouldBuy() {
		return !sell;
	}
	
	public int getMaxToBuy() {
		return maxToBuy;
	}
	
	public int getExtraCostPerItem() {
		return extraCostPerItem;
	}
	
	public int getPrice() {
		return price;
	}
	
	public Timestamp getLastUpdate() {
		return lastUpdate;
	}
	
	public boolean shouldExchange() {
		return shouldExchange;
	}
	
	public int getBankCount() {
		return bankCount;
	}
	
	
	public ExchangeItem setPrice(int price) {
		this.price = price;
		return this;
	}
	
	public ExchangeItem setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
		return this;
	}
	
	public ExchangeItem enableExchange() {
		this.shouldExchange = true;
		return this;
	}
	
	public ExchangeItem disableExchange() {
		this.shouldExchange = false;
		return this;
	}
	
	public ExchangeItem setBankCount(int bankCount) {
		this.bankCount = bankCount;
		return this;
	}
}
