package org.rs3.api;

public class Worlds {

	private static final int[] membersWorlds = new int[] { 1, 2, 4, 5, 6, 9, 10, 12, 14, 15, 16, 18, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 35, 36, 37, 39, 40, 42, 44, 45, 46, 48, 49, 50, 51, 52, 53, 54, 56, 58, 59, 60, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 76, 77, 78, 79, 82, 83, 84, 85, 86, 87, 88, 89, 91, 92, 96, 97, 98, 99, 100, 103, 104, 105, 106, 114, 115, 116, 117, 119, 123, 124, 134, 137, 138, 139, 140 };
	private static final int[] freeWorlds = new int[] { 3, 7, 8, 11, 17, 19, 20, 29, 33, 34, 38, 41, 43, 57, 61, 80, 81, 108, 120, 135, 136, 141 };
	
	public static boolean isMembers(int world) {
		for (int w : membersWorlds) {
			if (w == world)
				return true;
		}
		
		return false;
	}
	
	public static boolean isFree(int world) {
		for (int w : freeWorlds) {
			if (w == world)
				return true;
		}
		
		return false;
	}
	
	public static boolean isValid(int world) {
		return isMembers(world) || isFree(world);
	}
	
}
