package org.rs3.api;

import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.NpcDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.util.Filter;

public class Npcs {
	
	public static Npc[] getAll() {
		return Client.getAllNpcs_FromArray();
	}
	
	public static Npc getNearest(Filter<Npc> filter) {
		return getNearest(filter, null);
	}
	
	public static Npc getNearest(int... ids) {
		return getNearest(null, ids);
	}
	
	public static Npc getNearest(Filter<Npc> filter, int... ids) {
		if (filter == null && ids == null)
			return null;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		int playerRelX = player.getData().getCenterLocation().getRelativeX();
		int playerRelY = player.getData().getCenterLocation().getRelativeY();
		
		double distance = Double.MAX_VALUE;
		Npc closest = null;
		
		for (Npc npc : getAll()) {
			if (npc == null)
				continue;
			
			boolean match = false;
			if (ids != null) {
				NpcDefinition def = npc.getNpcDefinition();
				if (def != null) {
					int defId = def.getId();
					for (int id : ids) {
						if (defId == id) {
							match = true;
							break;
						}
					}
				}
			}
			
			if (match || ids == null) {
				if (filter != null && !filter.accept(npc))
					continue;
				
				int npcRelX = npc.getData().getCenterLocation().getRelativeX();
				int npcRelY = npc.getData().getCenterLocation().getRelativeY();
				
				double dist = Calculations.distance(playerRelX, playerRelY, npcRelX, npcRelY);
				if (dist < distance) {
					distance = dist;
					closest = npc;
				}
			}
		}
		
		return closest;
	}
}
