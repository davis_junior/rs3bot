package org.rs3.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import org.rs3.accessors.Reflection;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AbstractBoundary;
import org.rs3.api.wrappers.AbstractFloorObject;
import org.rs3.api.wrappers.AbstractWallObject;
import org.rs3.api.wrappers.AnimableNode;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.AnimatedBoundaryObject;
import org.rs3.api.wrappers.AnimatedFloorObject;
import org.rs3.api.wrappers.AnimatedWallObject;
import org.rs3.api.wrappers.Array2D;
import org.rs3.api.wrappers.Array3D;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.BoundaryObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.FloorObject;
import org.rs3.api.wrappers.GameInfo;
import org.rs3.api.wrappers.Ground;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.WallObject;
import org.rs3.util.Filter;

public class GroundObjects {
	
	public static GroundObject[] getAll(GroundObject.Type type) {
		return getAll(type, -1, false);
	}
	
	public static GroundObject[] getAllOnScreen(GroundObject.Type type) {
		return getAll(type, 15, true);
	}
	
	public static GroundObject[] getAllOnMap(GroundObject.Type type) {
		return getAll(type, 20, false);
	}
	
	public static GroundObject[] getAll(GroundObject.Type type, int maxDistance, boolean onScreenOnly) {
		try {
			ArrayList<GroundObject> allObjects = new ArrayList<>();
			
			GameInfo gameInfo = Client.getGameInfo();
			if (gameInfo == null)
				return null;
			
			Array3D<Ground> a3d = gameInfo.getGroundInfo().getGroundArray();
			Array2D<Ground> a2d = a3d.get2D(Client.getPlane());
			if (a2d == null)
				return null;
			
			for (Ground[] a1d : a2d.getAll()) {
				if (a1d == null)
					continue;
				
				for (Ground gd : a1d) {
					if (gd == null)
						continue;
					
					EnumSet<GroundObject.Type> types = (type != null) ? type.getPossibleTypes() : GroundObject.Type.All.getPossibleTypes();
					
					if (types.contains(GroundObject.Type.AnimableObject) || types.contains(GroundObject.Type.AnimatedAnimableObject)) {
						for (AnimableNode node = gd.getAnimableList(); node != null; node = node.getNext()) {
							RSAnimable animable = node.getAnimable();
							
							if (animable != null) {
								if ((animable instanceof AnimableObject && types.contains(GroundObject.Type.AnimableObject))
										|| (animable instanceof AnimatedAnimableObject && types.contains(GroundObject.Type.AnimatedAnimableObject))) {
									
									if (maxDistance == -1 || Calculations.distanceTo(animable) <= maxDistance) {
										if (onScreenOnly) {
											Tile tile = animable.getTile();
											if (tile != null && tile.isOnScreen())
												allObjects.add((GroundObject) animable);
											
											/*Model model = animable.getModel();
											if (model != null && model.isOnScreen(animable.getLocation(), animable.getPlane()))
												allObjects.add((GroundObject) animable);*/
										} else
											allObjects.add((GroundObject) animable);
									}
								}
							}
						}
					}
					
					if (types.contains(GroundObject.Type.BoundaryObject) || types.contains(GroundObject.Type.AnimatedBoundaryObject)) {
						AbstractBoundary[] boundaries = gd.getBoundaries();
						
						if (boundaries != null) {
							for (AbstractBoundary boundary : boundaries) {
								if (boundary != null) {
									if ((boundary instanceof BoundaryObject && types.contains(GroundObject.Type.BoundaryObject))
											|| (boundary instanceof AnimatedBoundaryObject && types.contains(GroundObject.Type.AnimatedBoundaryObject))) {
										
										if (maxDistance == -1 || Calculations.distanceTo(boundary) <= maxDistance) {
											if (onScreenOnly) {
												Tile tile = boundary.getTile();
												if (tile != null && tile.isOnScreen())
													allObjects.add((GroundObject) boundary);
												
												/*Model model = boundary.getModel();
												if (model != null && model.isOnScreen(boundary.getLocation(), boundary.getPlane()))
													allObjects.add((GroundObject) boundary);*/
											} else
												allObjects.add((GroundObject) boundary);
										}
									}
								}
							}
						}
					}
					
					if (types.contains(GroundObject.Type.WallObject) || types.contains(GroundObject.Type.AnimatedWallObject)) {
						AbstractWallObject[] wallObjects = gd.getWallDecorations();
						
						if (wallObjects != null) {
							for (AbstractWallObject wallObj : wallObjects) {
								if (wallObj != null) {
									if ((wallObj instanceof WallObject && types.contains(GroundObject.Type.WallObject))
											|| (wallObj instanceof AnimatedWallObject && types.contains(GroundObject.Type.AnimatedWallObject))) {
										
										if (maxDistance == -1 || Calculations.distanceTo(wallObj) <= maxDistance) {
											if (onScreenOnly) {
												Tile tile = wallObj.getTile();
												if (tile != null && tile.isOnScreen())
													allObjects.add((GroundObject) wallObj);
												
												/*Model model = wallObj.getModel();
												if (model != null && model.isOnScreen(wallObj.getLocation(), wallObj.getPlane()))
													allObjects.add((GroundObject) wallObj);*/
											} else
												allObjects.add((GroundObject) wallObj);
										}
									}
								}
							}
						}
					}
					
					if (types.contains(GroundObject.Type.FloorObject) || types.contains(GroundObject.Type.AnimatedFloorObject)) {
						AbstractFloorObject floorObject = gd.getFloorDecoration();
						
						if (floorObject != null) {
							if ((floorObject instanceof FloorObject && types.contains(GroundObject.Type.FloorObject))
									|| (floorObject instanceof AnimatedFloorObject && types.contains(GroundObject.Type.AnimatedFloorObject))) {
								
								if (maxDistance == -1 || Calculations.distanceTo(floorObject) <= maxDistance) {
									if (onScreenOnly) {
										Tile tile = floorObject.getTile();
										if (tile != null && tile.isOnScreen())
											allObjects.add((GroundObject) floorObject);
										
										/*Model model = floorObject.getModel();
										if (model != null && model.isOnScreen(floorObject.getLocation(), floorObject.getPlane()))
											allObjects.add((GroundObject) floorObject);*/
									} else
										allObjects.add((GroundObject) floorObject);
								}
							}
						}
					}
				}
			}
			
			if (allObjects.size() == 0)
				return null;
			
			return allObjects.toArray(new GroundObject[0]);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public static GroundObject getRandom(GroundObject.Type type, int maxDistance, boolean onScreenOnly) {
		try {
			GameInfo gameInfo = Client.getGameInfo();
			if (gameInfo == null)
				return null;
			
			Array3D<Ground> a3d = gameInfo.getGroundInfo().getGroundArray();
			Array2D<Ground> a2d = a3d.get2D(Client.getPlane());
			if (a2d == null)
				return null;
			
			List<int[]> groundIndices = GroundSearch.getGroundIndices(maxDistance);
			if (groundIndices == null)
				return null;
			
			Collections.shuffle(groundIndices);
			
			for (int[] indices : groundIndices) {
				if (indices == null)
					continue;
				
				Ground gd = a2d.get(indices[0], indices[1]);
				if (gd == null)
					continue;
				
				EnumSet<GroundObject.Type> types = (type != null) ? type.getPossibleTypes() : GroundObject.Type.All.getPossibleTypes();
				
				if (types.contains(GroundObject.Type.AnimableObject) || types.contains(GroundObject.Type.AnimatedAnimableObject)) {
					for (AnimableNode node = gd.getAnimableList(); node != null; node = node.getNext()) {
						RSAnimable animable = node.getAnimable();
						
						if (animable != null) {
							if ((animable instanceof AnimableObject && types.contains(GroundObject.Type.AnimableObject))
									|| (animable instanceof AnimatedAnimableObject && types.contains(GroundObject.Type.AnimatedAnimableObject))) {
								
								if (maxDistance == -1 || Calculations.distanceTo(animable) <= maxDistance) {
									if (onScreenOnly) {
										Tile tile = animable.getTile();
										if (tile != null && tile.isOnScreen())
											return (GroundObject) animable;
										
										/*Model model = animable.getModel();
										if (model != null && model.isOnScreen(animable.getLocation(), animable.getPlane()))
											return (GroundObject) animable;*/
									} else
										return (GroundObject) animable;
								}
							}
						}
					}
				}
				
				if (types.contains(GroundObject.Type.BoundaryObject) || types.contains(GroundObject.Type.AnimatedBoundaryObject)) {
					AbstractBoundary[] boundaries = gd.getBoundaries();
					
					if (boundaries != null) {
						for (AbstractBoundary boundary : boundaries) {
							if (boundary != null) {
								if ((boundary instanceof BoundaryObject && types.contains(GroundObject.Type.BoundaryObject))
										|| (boundary instanceof AnimatedBoundaryObject && types.contains(GroundObject.Type.AnimatedBoundaryObject))) {
									
									if (maxDistance == -1 || Calculations.distanceTo(boundary) <= maxDistance) {
										if (onScreenOnly) {
											Tile tile = boundary.getTile();
											if (tile != null && tile.isOnScreen())
												return (GroundObject) boundary;
											
											/*Model model = boundary.getModel();
											if (model != null && model.isOnScreen(boundary.getLocation(), boundary.getPlane()))
												return (GroundObject) boundary;*/
										} else
											return (GroundObject) boundary;
									}
								}
							}
						}
					}
				}
				
				if (types.contains(GroundObject.Type.WallObject) || types.contains(GroundObject.Type.AnimatedWallObject)) {
					AbstractWallObject[] wallObjs = gd.getWallDecorations();
					
					if (wallObjs != null) {
						for (AbstractWallObject wallObj : wallObjs) {
							if (wallObj != null) {
								if ((wallObj instanceof WallObject && types.contains(GroundObject.Type.WallObject))
										|| (wallObj instanceof AnimatedWallObject && types.contains(GroundObject.Type.AnimatedWallObject))) {
									
									if (maxDistance == -1 || Calculations.distanceTo(wallObj) <= maxDistance) {
										if (onScreenOnly) {
											Tile tile = wallObj.getTile();
											if (tile != null && tile.isOnScreen())
												return (GroundObject) wallObj;
											
											/*Model model = wallObj.getModel();
											if (model != null && model.isOnScreen(wallObj.getLocation(), wallObj.getPlane()))
												return (GroundObject) wallObj;*/
										} else
											return (GroundObject) wallObj;
									}
								}
							}
						}
					}
				}
				
				if (types.contains(GroundObject.Type.FloorObject) || types.contains(GroundObject.Type.AnimatedFloorObject)) {
					AbstractFloorObject floorObject = gd.getFloorDecoration();
					
					if (floorObject != null) {
						if ((floorObject instanceof FloorObject && types.contains(GroundObject.Type.FloorObject))
								|| (floorObject instanceof AnimatedFloorObject && types.contains(GroundObject.Type.AnimatedFloorObject))) {
							
							if (maxDistance == -1 || Calculations.distanceTo(floorObject) <= maxDistance) {
								if (onScreenOnly) {
									Tile tile = floorObject.getTile();
									if (tile != null && tile.isOnScreen())
										return (GroundObject) floorObject;
									
									/*Model model = floorObject.getModel();
									if (model != null && model.isOnScreen(floorObject.getLocation(), floorObject.getPlane()))
										return (GroundObject) floorObject;*/
								} else
									return (GroundObject) floorObject;
							}
						}
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static GroundObject getNearest(GroundObject.Type type, int... ids) {
		return getNearest(type, null, ids);
	}
	
	// checks filter first, then ids
	public static GroundObject getNearest(GroundObject.Type type, final Filter<GroundObject> filter, final int... ids) {
		return getNearest(type, new Filter<GroundObject>() {
			@Override
			public boolean accept(GroundObject animable) {
				if (filter != null && !filter.accept(animable))
					return false;
				
				int id = animable.getId();
				
				if (id != -1) {
					for (int i : ids) {
						if (id == i)
							return true;
					}
				}
				
				return false;
			}
		});
	}
	
	public static GroundObject getNearest(GroundObject.Type type, Filter<GroundObject> filter) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		return getNearestTo(type, player, filter);
	}
	
	public static GroundObject getNearestTo(GroundObject.Type type, Interactable interactable, Filter<GroundObject> filter) {
		if (interactable == null)
			return null;
		
		Tile tile = interactable.getData().getCenterLocation().getLocalTile(false);
		
		try {
			GameInfo gameInfo = Client.getGameInfo();
			if (gameInfo == null)
				return null;
			
			Array3D<Ground> a3d = gameInfo.getGroundInfo().getGroundArray();
			
			Array2D<Ground> a2d = a3d.get2D(Client.getPlane());
			if (a2d == null)
				return null;
			
			for (int i = 0; i < 104; i++) {
				List<int[]> groundIndices = GroundSearch.getSprialSquareIndicesAtLocal(tile, i);
				if (groundIndices == null)
					break;
				
				Collections.shuffle(groundIndices);
				
				for (int[] indices : groundIndices) {
					int x = indices[0];
					int y = indices[1];
					
					Ground gd = a2d.get(x, y);
					if (gd == null)
						continue;

					
					EnumSet<GroundObject.Type> types = (type != null) ? type.getPossibleTypes() : GroundObject.Type.All.getPossibleTypes();
					
					if (types.contains(GroundObject.Type.AnimableObject) || types.contains(GroundObject.Type.AnimatedAnimableObject)) {
						for (AnimableNode node = gd.getAnimableList(); node != null; node = node.getNext()) {
							RSAnimable animable = node.getAnimable();
							
							if (animable != null) {
								if ((animable instanceof AnimableObject && types.contains(GroundObject.Type.AnimableObject))
										|| (animable instanceof AnimatedAnimableObject && types.contains(GroundObject.Type.AnimatedAnimableObject))) {
									
									if (filter.accept((GroundObject) animable))
										return (GroundObject) animable;
								}
							}
						}
					}
					
					if (types.contains(GroundObject.Type.BoundaryObject) || types.contains(GroundObject.Type.AnimatedBoundaryObject)) {
						AbstractBoundary[] boundaries = gd.getBoundaries();
						
						if (boundaries != null) {
							for (AbstractBoundary boundary : boundaries) {
								if (boundary != null) {
									if ((boundary instanceof BoundaryObject && types.contains(GroundObject.Type.BoundaryObject))
											|| (boundary instanceof AnimatedBoundaryObject && types.contains(GroundObject.Type.AnimatedBoundaryObject))) {
										
										if (filter.accept((GroundObject) boundary))
											return (GroundObject) boundary;
									}
								}
							}
						}
					}
					
					if (types.contains(GroundObject.Type.WallObject) || types.contains(GroundObject.Type.AnimatedWallObject)) {
						AbstractWallObject[] wallObjs = gd.getWallDecorations();
						
						if (wallObjs != null) {
							for (AbstractWallObject wallObj : wallObjs) {
								if (wallObj != null) {
									if ((wallObj instanceof WallObject && types.contains(GroundObject.Type.WallObject))
											|| (wallObj instanceof AnimatedWallObject && types.contains(GroundObject.Type.AnimatedWallObject))) {
										
										if (filter.accept((GroundObject) wallObj))
											return (GroundObject) wallObj;
									}
								}
							}
						}
					}
					
					if (types.contains(GroundObject.Type.FloorObject) || types.contains(GroundObject.Type.AnimatedFloorObject)) {
						AbstractFloorObject floorObj = gd.getFloorDecoration();
						
						if (floorObj != null) {
							if ((floorObj instanceof FloorObject && types.contains(GroundObject.Type.FloorObject))
									|| (floorObj instanceof AnimatedFloorObject && types.contains(GroundObject.Type.AnimatedFloorObject))) {
								
								if (filter.accept((GroundObject) floorObj))
									return (GroundObject) floorObj;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static GroundObject getAt(GroundObject.Type type, int x, int y) {
		GroundObject[] objects = getAllAt(type, x, y);
		if (objects == null || objects.length == 0)
			return null;
		
		return objects[0];
	}
	
	public static GroundObject[] getAllAt(GroundObject.Type type, int x, int y) {
		if (x > 103 || y > 103) {
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			x -= base.getX();
			y -= base.getY();
		}
		
		if (x >= 0 && y >= 0 && x < 104 && y < 104) {
			return getAllAtLocal(type, x, y);
		}
		
		return null;
	}
	
	public static GroundObject getAtLocal(GroundObject.Type type, int x, int y) {
		GroundObject[] objects = getAllAtLocal(type, x, y);
		if (objects == null || objects.length == 0)
			return null;
		
		return objects[0];
	}
	
	public static GroundObject[] getAllAtLocal(GroundObject.Type type, int x, int y) {
		try {
			ArrayList<GroundObject> allObjects = new ArrayList<>();
			
			GameInfo gameInfo = Client.getGameInfo();
			if (gameInfo == null)
				return null;
			
			Array3D<Ground> a3d = gameInfo.getGroundInfo().getGroundArray();
			Ground gd = a3d.get(Client.getPlane(), x, y);
			if (gd == null)
				return null;
			
			EnumSet<GroundObject.Type> types = (type != null) ? type.getPossibleTypes() : GroundObject.Type.All.getPossibleTypes();
			
			if (types.contains(GroundObject.Type.AnimableObject) || types.contains(GroundObject.Type.AnimatedAnimableObject)) {
				for (AnimableNode node = gd.getAnimableList(); node != null; node = node.getNext()) {
					RSAnimable animable = node.getAnimable();
					
					if (animable != null) {
						if ((animable instanceof AnimableObject && types.contains(GroundObject.Type.AnimableObject))
								|| (animable instanceof AnimatedAnimableObject && types.contains(GroundObject.Type.AnimatedAnimableObject))) {
							
							allObjects.add((GroundObject) animable);
						}
					}
				}
			}
			
			if (types.contains(GroundObject.Type.BoundaryObject) || types.contains(GroundObject.Type.AnimatedBoundaryObject)) {
				AbstractBoundary[] boundaries = gd.getBoundaries();
				
				if (boundaries != null) {
					for (AbstractBoundary boundary : boundaries) {
						if (boundary != null) {
							if ((boundary instanceof BoundaryObject && types.contains(GroundObject.Type.BoundaryObject))
									|| (boundary instanceof AnimatedBoundaryObject && types.contains(GroundObject.Type.AnimatedBoundaryObject))) {
								
								allObjects.add((GroundObject) boundary);
							}
						}
					}
				}
			}
			
			if (types.contains(GroundObject.Type.WallObject) || types.contains(GroundObject.Type.AnimatedWallObject)) {
				AbstractWallObject[] wallObjs = gd.getWallDecorations();
				
				if (wallObjs != null) {
					for (AbstractWallObject wallObj : wallObjs) {
						if (wallObj != null) {
							if ((wallObj instanceof WallObject && types.contains(GroundObject.Type.WallObject))
									|| (wallObj instanceof AnimatedWallObject && types.contains(GroundObject.Type.AnimatedWallObject))) {
								
								allObjects.add((GroundObject) wallObj);
							}
						}
					}
				}
			}
			
			if (types.contains(GroundObject.Type.FloorObject) || types.contains(GroundObject.Type.AnimatedFloorObject)) {
				AbstractFloorObject floorObj = gd.getFloorDecoration();
				
				if (floorObj != null) {
					if ((floorObj instanceof FloorObject && types.contains(GroundObject.Type.FloorObject))
							|| (floorObj instanceof AnimatedFloorObject && types.contains(GroundObject.Type.AnimatedFloorObject))) {
						
						allObjects.add((GroundObject) floorObj);
					}
				}
			}
			
			if (allObjects.size() == 0)
				return null;
			
			return allObjects.toArray(new GroundObject[0]);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
