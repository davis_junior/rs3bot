package org.rs3.api;

import java.awt.Point;
import java.util.List;

import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.MiniMap;
import org.rs3.api.interfaces.widgets.useless.BeginnerHintMessages;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.AreaPath;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.Settings;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Walking {
	
	private static int minDistNextWalk = getRandMinDistNextWalk();
	
	public static int getMinDistNextWalk() {
		return minDistNextWalk;
	}
	
	private static int getRandMinDistNextWalk() {
		return Random.nextInt(0.75F, 9, 15, 5, 20);
	}
	
	/*public static void setMinDistNextWalk(int minDist) {
		minDistNextWalk = minDist;
	}*/
	
	public static void setRandMinDistNextWalk() {
		minDistNextWalk = getRandMinDistNextWalk();
	}
	
	/**
	 * 
	 * @return Global tile
	 */
	public static Tile getDestination() {
		//final int lx = Client.getDestX() / 4;
		//final int ly = Client.getDestY() / 4;
		//final int lx = (Client.getDestX() * 256) >> 9;
		//final int ly = (Client.getDestY() * 256) >> 9;
		
		final int lx = Client.getDestX();
		final int ly = Client.getDestY();
		
		if (lx == -1 || ly == -1)
		//if (lx == -1 || ly == -1 || lx == 0 || ly == 0)
			return new Tile(-1, -1, Client.getPlane());
		
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		
		if (base == null)
			return null;
		
		return new Tile(
				base.getX() + lx,
				base.getY() + ly,
				Client.getPlane()
		);
	}
	
	public static boolean hasDestination() {
		Tile dest = getDestination();
		
		if (dest != null && dest.x != -1 && dest.y != -1)
			return true;
		
		return false;
	}

	/**
	 * @param plane The plane of which to determine the collision offset for.
	 * @return The <code>Tile</code> of the offset location (different than map base!).
	 */
	/*public static Tile getCollisionOffset(final int plane) {
		final Multipliers multipliers = Context.multipliers();
		final RSInfo info = (RSInfo) Context.client().getRSGroundInfo();
		final RSGroundData data = ((RSGroundData[]) info.getGroundData())[plane];
		return new Tile(data.getX() * multipliers.GROUNDDATA_X, data.getY() * multipliers.GROUNDDATA_Y, plane);
	}*/

	/**
	 * @param plane The plane of which to provide the collision flags for.
	 * @return The collision flags of the current map block.
	 */
	/*public static int[][] getCollisionFlags(final int plane) {
		final RSInfo info = (RSInfo) Context.client().getRSGroundInfo();
		final RSGroundData data = ((RSGroundData[]) info.getGroundData())[plane];
		return (int[][]) data.getBlocks();
	}*/

	public static void setRun(final boolean enabled) {
		if (isRunEnabled() != enabled && MiniMap.runEnergyButton.getComponent().click(Mouse.LEFT)) {
			final Timer t = new Timer(1800);
			while (t.isRunning() && isRunEnabled() != enabled) {
				Time.sleep(50);
				//Time.sleep(5);
			}
		}
	}

	public static boolean isRunEnabled() {
		return Client.getSettingData().getSettings().get(Settings.BOOLEAN_RUN_ENABLED) == 1;
	}

	public static int getEnergy() {
		try {
			Component comp = MiniMap.runEnergyText.getComponent();
			if (comp != null)
				return Integer.parseInt(comp.getText().replace("%", ""));
		} catch (final NumberFormatException e) {
			e.printStackTrace();
		}
		
		return -1;
	}
	
	// TODO: verify rest (setting)
	public static boolean restUntil(int minEnergy) {
		Component mapRunButton = MiniMap.runEnergyButton.getComponent();
		if (mapRunButton != null && mapRunButton.interact("Rest")) {
			Time.sleepQuick();
			
			if (minEnergy == -1)
				return true;
			
			Timer timer = new Timer(30000);
			while (timer.isRunning()) {
				if (ScriptHandler.currentScript.shouldStop())
					return false;
				
				if (getEnergy() >= minEnergy)
					return true;
				
				Time.sleep(50);
			}
		}
		
		return false;
	}

	/*public static LocalPath findPath(final Locatable mobile) {
		return new LocalPath(mobile.getLocation());
	}*/
	
	public static boolean walk(final Interactable mobile) {
		Tile stepDirection = mobile.getData().getCenterLocation().getGlobalTile(true);
		return walk(stepDirection);
	}

	/**
	 * Clicks a tile on the minimap (or 25% chance on screen).
	 */
	public static boolean walk(Tile tile) {
		if (BeginnerHintMessages.get.isOpen()) // interferes with minimap
			BeginnerHintMessages.get.close(true);
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
			Time.sleepVeryQuick();
		}
		
		if (Random.nextInt(0, 100) > 75)
			return tile.walkOnScreen();
		
		return tile.walkOnMap();
	}
	
	public static Tile getClosestInArray(Tile[] tiles, int minDist) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		double distance = Double.MAX_VALUE;
		int index = -1;
		final Tile local = player.getData().getCenterLocation().getGlobalTile(true);
		
		for (int i = tiles.length - 1; i >= 0; --i) {
			double curDist = Calculations.distance(local, tiles[i]);
			
			if (minDist == -1 || curDist >= minDist) {
				if (curDist < distance) {
					distance = curDist;
					index = i;
				}
			}
		}
		
		if (index != -1)
			return tiles[index];
		
		return null;
	}
	
	public static Tile getClosestOnMap(Tile[] tiles, int minDist) {
		Tile closestInArray = getClosestInArray(tiles, minDist);
		if (closestInArray == null)
			return null;
		
		return Walking.getClosestOnMap(closestInArray);
	}
	
	// defaults to 75% 18-20 or 12-20 maxDist which is about the edge of the minimap
	public static Tile getClosestOnMap(Tile tile) {
		return getClosestOnMap(tile, Random.nextInt(0.75F, 18, 20, 12, 20));
	}
	
	/*public static Tile getClosestOnMap(Tile tile) {
		return getClosestOnMap(tile, 20);
	}*/
	
	public static Tile getClosestOnMap(Tile tile, int maxDist) {
		if (tile.isOnMap())
			return tile;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		final Tile location = player.getData().getCenterLocation().getGlobalTile(false);
		tile = tile.derive(-location.getX(), -location.getY());
		
		final double angle = Math.atan2(tile.getY(), tile.getX());
		
		return new Tile(
				//location.getX() + (int) (16d * Math.cos(angle)),
				//location.getY() + (int) (16d * Math.sin(angle)),
				//location.getX() + (int) (20d * Math.cos(angle)),
				//location.getY() + (int) (20d * Math.sin(angle)),
				location.getX() + (int) (maxDist * Math.cos(angle)),
				location.getY() + (int) (maxDist * Math.sin(angle)),
				tile.getPlane()
		);
	}
	
	public static Tile getClosestOnScreen(Tile[] tiles, int minDist) {
		Tile closestInArray = getClosestInArray(tiles, minDist);
		if (closestInArray == null)
			return null;
		
		return Walking.getClosestOnScreen(closestInArray);
	}
	
	public static Tile getClosestOnScreen(Tile tile) {
		if (tile.isOnScreen())
			return tile;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		List<Tile> path = AreaPath.getLine(tile, player.getTile());
		if (path != null) {
			for (Tile t : path) {
				if (t.distanceTo() < 15 && t.isOnScreen())
					return t;
			}
		}
		
		return null;
	}
	
	public static boolean loopWalkToArea(long maxPeriod, Area area) {
		return area.loopWalkTo(maxPeriod);
	}
}
