package org.rs3.api.objects;


public class PreciseTile {
	
	public float x;
	public float y;
	public float height;
	public int plane;
	
	public Tile cachedTile = null;
	
	public PreciseTile(float x, float y, float height, int plane) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.plane = plane;
		
		cachedTile = new Tile((int) x, (int) y, plane);
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float getHeight() {
		return height;
	}
	
	public int getPlane() {
		return plane;
	}
	
	public Tile getTile(boolean cached) {
		if (cached)
			return cachedTile;
		else
			return new Tile((int) x, (int) y, plane);
	}
}
