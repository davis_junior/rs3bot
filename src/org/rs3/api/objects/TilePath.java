package org.rs3.api.objects;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.rs3.api.Calculations;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.wrappers.Player;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class TilePath extends Path {
	protected Tile[] tiles;

	private boolean end;
	
	private boolean complete; // added
	private boolean onLastTile; // added
	
	private boolean walkOnScreenOnly; // added
	
	public TilePath(Tile... tiles) {
		this(false, tiles);
	}
	
	public TilePath(boolean walkOnScreenOnly, Tile... tiles) {
		// Connect all tiles TODO: remove duplicates
		if (tiles.length > 1) {
			List<Tile> line = new ArrayList<>();
			
			for (int i = 0; i < tiles.length; i++) {
				if (i != tiles.length - 1) {
					List<Tile> curLine = AreaPath.getLine(tiles[i], tiles[i + 1]);
					line.addAll(curLine);
				} else
					line.add(tiles[i]);
			}
			
			tiles = line.toArray(new Tile[0]);
		}
		
		this.walkOnScreenOnly = walkOnScreenOnly;
		this.tiles = tiles;
		
		complete = false;
		onLastTile = false;
	}
	
	public boolean isComplete() {
		return complete;
	}
	
	public boolean isOnLastTile() {
		return onLastTile;
	}
	
	public void reset() {
		end = false;
		
		complete = false;
		onLastTile = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean traverse(final EnumSet<TraversalOption> options) {
		final Tile next = getNext();
		if (next == null)
			return false;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		if (next.equals(getEnd())) {
			onLastTile = true;
			
			if (Calculations.distanceTo(next) <= 1 || end && (!player.isMoving() || Calculations.distanceTo(next) < 3)) {
				complete = true;
				return false;
			}
			
			end = true;
		} else {
			onLastTile = false;
			end = false;
		}
		
		if (options != null && options.contains(TraversalOption.HANDLE_RUN) && !Walking.isRunEnabled() && Walking.getEnergy() > 50) {
			Walking.setRun(true);
			Time.sleep(0.75F, 300, 600, 600, 1200);
			//Time.sleep(300);
		}
		
		if (options != null && options.contains(TraversalOption.SPACE_ACTIONS)) {
			final Tile dest = Walking.getDestination();
			if (dest.getX() != -1 && player.isMoving() && Calculations.distanceTo(dest) > 5 && Calculations.distance(next, dest) < 7)
				return true;
		}
		
		if (!walkOnScreenOnly)
			return Walking.walk(next);
		else
			return next.walkOnScreen();
	}
	
	public boolean loopWalk(long maxPeriod, Condition breakCondition, boolean doAntiban) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		Timer timer = new Timer(maxPeriod);
		//while (timer.isRunning() && !complete) {
		while (timer.isRunning() && !onLastTile) {
			if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
				return false;
			
			if (breakCondition != null && breakCondition.evaluate())
				return false;
			
			if (!player.isMoving() || Calculations.distanceTo(Walking.getDestination()) <= (walkOnScreenOnly ? 3 : Walking.getMinDistNextWalk())) {
				traverse();
				Time.sleepMedium();
				
				if (doAntiban && Antiban.doAntiban())
					Time.sleepVeryQuick();
				
				if (!walkOnScreenOnly)
					Walking.setRandMinDistNextWalk();
			} else {
				Time.sleep(50);
				
				if (doAntiban && Antiban.doAntiban())
					Time.sleepVeryQuick();
			}
		}
		
		if (onLastTile)
			return true;
		
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean validate() {
		return tiles.length > 0 && getNext() != null && Calculations.distanceTo(getEnd()) > Math.sqrt(2);
	}

	/**
	 * {@inheritDoc}
	 */
	/*@Override
	public Tile getNext() {
		final Tile dest = Walking.getDestination();
		
		for (int i = tiles.length - 1; i >= 0; --i) {
			if (tiles[i].isOnMap() && (tiles[i].validate() || (i != 0 && (dest.getX() != -1 ? Calculations.distance(dest, tiles[i - 1]) < 3 : Calculations.distanceTo(tiles[i - 1]) < 7))))
				return tiles[i];
			else if (Calculations.distanceTo(tiles[i]) < 25) // added
				return Walking.getClosestOnMap(tiles[i]);
		}
		
		// added: else find closest tile on map
		Tile closest = Walking.getClosestOnMap(tiles, 5);
		if (closest == null)
			closest = getEnd();
		
		return closest;
	}*/
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tile getNext() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		//final Tile dest = Walking.getDestination();
		Tile myTile = player.getTile();
		
		for (int i = tiles.length - 1; i >= 0; --i) {
			int dist = (int) Calculations.distance(tiles[i], myTile);
			
			if (!walkOnScreenOnly) {
				if (dist < 25) {
					if (dist >= 5 || i == tiles.length - 1)
						return Walking.getClosestOnMap(tiles[i]);
					else
						return Walking.getClosestOnMap(tiles[i + 1]);
				}
			} else {
				if (dist < 15) {
					if (dist >= 3 || i == tiles.length - 1)
						return Walking.getClosestOnScreen(tiles[i]);
					else
						return Walking.getClosestOnScreen(tiles[i + 1]);
				}
			}
		}
		
		// else find closest tile on map
		Tile closest = walkOnScreenOnly ? Walking.getClosestOnScreen(tiles, 3) : Walking.getClosestOnMap(tiles, 5);
		if (closest == null)
			closest = walkOnScreenOnly ? Walking.getClosestOnScreen(getEnd()) : Walking.getClosestOnMap(getEnd());
		
		return closest;
	}
	
	public double getDistanceToNearest() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return -1D;
		
		Tile myTile = player.getTile();
		
		double curDist = Double.MAX_VALUE;
		
		for (Tile tile : tiles) {
			double dist = tile.distance(myTile);
			if (dist < curDist)
				curDist = dist;
		}
		
		return curDist;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tile getStart() {
		return tiles[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tile getEnd() {
		return tiles[tiles.length - 1];
	}

	/**
	 * derives (does not affect current instance)
	 * 
	 * @param maxX
	 * @param maxY
	 * @return
	 */
	public TilePath randomize(final int maxX, final int maxY) {
		Tile[] newTiles = new Tile[tiles.length];
		
		for (int i = 0; i < tiles.length; ++i) {
			newTiles[i] = tiles[i].derive(Random.nextInt(-maxX, maxX), Random.nextInt(-maxY, maxY));
		}
		
		return new TilePath(walkOnScreenOnly, newTiles);
	}

	/**
	 * derives (does not affect current instance)
	 * 
	 * @return
	 */
	public TilePath reverse() {
		Tile[] reversed = new Tile[tiles.length];
		
		for (int i = 0; i < tiles.length; ++i) {
			reversed[i] = tiles[tiles.length - 1 - i];
		}
		
		return new TilePath(walkOnScreenOnly, reversed);
	}

	public Tile[] toArray() {
		final Tile[] a = new Tile[tiles.length];
		System.arraycopy(tiles, 0, a, 0, tiles.length);
		return a;
	}
	
	@Override
	public TilePath clone() {
		return new TilePath(walkOnScreenOnly, tiles);
	}
}
