package org.rs3.api.objects;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.rs3.api.Calculations;
import org.rs3.debug.Paint;
import org.rs3.util.Random;
import org.rs3.util.Timer;

public class AreaPath {
	private Tile[] bounds;
	private Area area;
	
	private Tile[] start;
	private Tile[] end;
	
	private Tile[] biggerWall;
	private Tile[] smallerWall;
	
	private boolean complete;
	
	public AreaPath(Area area, Tile[] start, Tile[] end) {
		this.area = area;
		this.start = start;
		this.end = end;
		bounds = area.getBoundingTiles();
		
		if (!findSideWalls()) {
			biggerWall = null;
			smallerWall = null;
		}
		
		complete = false;
	}
	
	public static Point derive(Point point, int dx, int dy) {
		Point newPoint = new Point(point);
		newPoint.translate(dx, dy);
		
		return newPoint;
	}
	
	public static boolean isAdjacent(Tile testTile, Tile... tiles) {
		return isAdjacent(tileToPoint(testTile), tilesToPoints(tiles));
	}
	
	// tests whether any point in points is adjacent to testPoint
	public static boolean isAdjacent(Point testPoint, Point... points) {
		return getAdjacentCount(true, testPoint, points) == 1;
	}
	
	// returns the number of points adjacent to testPoint
	public static int getAdjacentCount(Point testPoint, Point... points) {
		return getAdjacentCount(false, testPoint, points);
	}
	
	// returns the number of points adjacent to testPoint or 1 if breakOnNull and at least 1 point was adjacent
	public static int getAdjacentCount(boolean breakOnFirst, Point testPoint, Point... points) {
		if (testPoint == null || points == null)
			return 0;
		
		int count = 0;
		for (Point point : points) {
			if (isAdjacent(testPoint, point)) {
				if (breakOnFirst)
					return 1;
				
				count++;
			}
		}
		
		return count;
	}
	
	public static boolean isAdjacent(Tile tile1, Tile tile2) {
		return isAdjacent(tileToPoint(tile1), tileToPoint(tile2));
	}
	
	public static boolean isAdjacent(Point point1, Point point2) {
		if (point1 == null || point2 == null)
			return false;
		
		if (point1.equals(point2))
			return false;
		
		if (derive(point1, -1, -1).equals(point2))
			return true;
		if (derive(point1, 0, -1).equals(point2))
			return true;
		if (derive(point1, 1, -1).equals(point2))
			return true;
		if (derive(point1, 1, 0).equals(point2))
			return true;
		if (derive(point1, 1, 1).equals(point2))
			return true;
		if (derive(point1, 0, 1).equals(point2))
			return true;
		if (derive(point1, -1, 1).equals(point2))
			return true;
		if (derive(point1, -1, 0).equals(point2))
			return true;
		
		return false;
	}
	
	public static Tile[] getAdjacentTiles(Tile tile) {
		Tile[] tiles = new Tile[8];
		
		tiles[0] = tile.derive(-1, -1);
		tiles[1] = tile.derive(0, -1);
		tiles[2] = tile.derive(1, -1);
		tiles[3] = tile.derive(1, 0);
		tiles[4] = tile.derive(1, 1);
		tiles[5] = tile.derive(0, 1);
		tiles[6] = tile.derive(-1, 1);
		tiles[7] = tile.derive(-1, 0);
		
		return tiles;
	}
	
	// TODO: make tile equivalent
	// returns points of checkPoints that are adjacent to point
	public static Point[] getAdjacentPoints(Point point, Point... checkPoints) {
		Point[] adjacentPoints = getAdjacentPoints(point);
		
		List<Point> result = new ArrayList<>();
		for (Point check : checkPoints) {
			for (Point adjacent : adjacentPoints) {
				//if (isAdjacent(check, adjacent))
				if (check.equals(adjacent))
					result.add(check);
			}
		}
		
		if (result.size() > 0)
			return result.toArray(new Point[0]);
		
		return null;
	}
	
	public static Point[] getAdjacentPoints(Point point) {
		Point[] points = new Point[8];
		
		points[0] = derive(point, -1, -1);
		points[1] = derive(point, 0, -1);
		points[2] = derive(point, 1, -1);
		points[3] = derive(point, 1, 0);
		points[4] = derive(point, 1, 1);
		points[5] = derive(point, 0, 1);
		points[6] = derive(point, -1, 1);
		points[7] = derive(point, -1, 0);
		
		return points;
	}
	
	// Gives angle from A to B
	public static double angleFrom(int APositionX, int APositionY, int BPositionX, int BPositionY) {
	    int dx = BPositionX - APositionY;
	    int dy = BPositionY - APositionY;
	    
	    return Math.atan2(dy, dx);
	}
	
	/*public static double angleFrom(int APositionX, int APositionY, int BPositionX, int BPositionY) {
	    int dx = APositionX - BPositionX;
	    int dy = APositionY - BPositionY;
	    
	    return Math.atan2(dy, dx);
	}*/
	
	public static double angleFrom(Tile tileA, Tile tileB) {
		return angleFrom(tileA.x, tileA.y, tileB.x, tileB.y);
	}
	
	public static List<Tile> getLine(Tile tile1, Tile tile2) {
		List<Point> list = getLine(tile1.getX(), tile1.getY(), tile2.getX(), tile2.getY());
		if (list != null) {
			Point[] pointArray = list.toArray(new Point[0]);
			return Arrays.asList(pointsToTiles(pointArray, tile1.getPlane()));
		}
		
		return null;
	}
	
	public static List<Point> getLine(int x,int y,int x2, int y2) {
		List<Point> points = new ArrayList<>();
		
	    int w = x2 - x ;
	    int h = y2 - y ;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0 ;
	    if (w<0) dx1 = -1 ; else if (w>0) dx1 = 1 ;
	    if (h<0) dy1 = -1 ; else if (h>0) dy1 = 1 ;
	    if (w<0) dx2 = -1 ; else if (w>0) dx2 = 1 ;
	    int longest = Math.abs(w) ;
	    int shortest = Math.abs(h) ;
	    if (!(longest>shortest)) {
	        longest = Math.abs(h) ;
	        shortest = Math.abs(w) ;
	        if (h<0) dy2 = -1 ; else if (h>0) dy2 = 1 ;
	        dx2 = 0 ;            
	    }
	    int numerator = longest >> 1 ;
	    for (int i=0;i<=longest;i++) {
	        points.add(new Point(x,y)) ;
	        numerator += shortest ;
	        if (!(numerator<longest)) {
	            numerator -= longest ;
	            x += dx1 ;
	            y += dy1 ;
	        } else {
	            x += dx2 ;
	            y += dy2 ;
	        }
	    }
	    
	    if (points.size() > 0)
	    	return points;
	    	
	    return null;
	}
	
	/*public static List<Point> getPerpendicularLine(List<Point> origLine, Point point) {
		
	}*/
	
	public static Tile[][] points2dToTiles2d(Point[][] points2d, int plane) {
		if (points2d == null)
			return null;
		
		Tile[][] tiles2d = new Tile[points2d.length][];
		for (int i = 0; i < points2d.length; i++) {
			tiles2d[i] = pointsToTiles(points2d[i], plane);
		}
		
		return tiles2d;
	}
	
	public static Tile[] pointsToTiles(Point[] points, int plane) {
		if (points == null)
			return null;
		
		Tile[] tiles = new Tile[points.length];
		for (int i = 0; i < points.length; i++) {
			tiles[i] = pointToTile(points[i], plane);
		}
		
		return tiles;
	}
	
	public static Tile pointToTile(Point point, int plane) {
		return new Tile(point.x, point.y, plane);
	}
	
	public static Point[][] tiles2dToPoints2d(Tile[][] tiles2d) {
		if (tiles2d == null)
			return null;
		
		Point[][] points2d = new Point[tiles2d.length][];
		for (int i = 0; i < tiles2d.length; i++) {
			points2d[i] = tilesToPoints(tiles2d[i]);
		}
		
		return points2d;
	}
	
	public static Point[] tilesToPoints(Tile[] tiles) {
		if (tiles == null)
			return null;
		
		Point[] points = new Point[tiles.length];
		for (int i = 0; i < tiles.length; i++) {
			points[i] = tileToPoint(tiles[i]);
		}
		
		return points;
	}
	
	public static Point tileToPoint(Tile tile) {
		return new Point(tile.x, tile.y);
	}
	
	public static Point[][] getLines(Point[] points) {
		List<List<Point>> lines = getLines(Arrays.asList(points));
		
		if (lines.size() > 0) {
			Point[][] array2d = new Point[lines.size()][];
			for (int i = 0; i < lines.size(); i++) {
				array2d[i] = lines.get(i).toArray(new Point[0]);
			}
			
			return array2d;
		}
		
		return null;
	}
	
	
	
	public static List<List<Point>> getLines(List<Point> points) {
		if (points == null)
			return null;
		
		Point[] pointArray = points.toArray(new Point[0]);
		
		List<List<Point>> lines = new ArrayList<>();
		
		
		Point curPoint = points.get(0);
		
		List<Point> addedPoints = new ArrayList<>();
		addedPoints.add(curPoint);
		
		List<Point> line = new ArrayList<>();
		line.add(curPoint);
		
		Timer debugTimer = new Timer(10000);
		while (debugTimer.isRunning() && addedPoints.size() != points.size()) {
			boolean added = true;
			while (added) {
				added = false;
				
				for (Point point : line) {
					
					Point[] adjacentPoints = getAdjacentPoints(point, pointArray);
					if (adjacentPoints != null) {
						//System.out.println(adjacentPoints.length);
						
						for (Point adjacent : adjacentPoints) {
							
							/// test if already added
							boolean alreadyAdded_skip = false;
							for (Point alreadyAdded : addedPoints) {
								if (adjacent.equals(alreadyAdded)) {
									alreadyAdded_skip = true;
									break;
								}
							}
							
							if (alreadyAdded_skip)
								continue;
							/// end test
							
							line.add(adjacent);
							addedPoints.add(adjacent);
							added = true;
						}
					}
					
					if (added)
						break;
				}
				
			}
			
			lines.add(line);
			
			// find point not already added to start new line
			if (addedPoints.size() != points.size()) {
				
				boolean debug = false;
				
				for (Point point : points) {
					/// test if already added
					boolean alreadyAdded_skip = false;
					for (Point alreadyAdded : addedPoints) {
						if (point.equals(alreadyAdded)) {
							alreadyAdded_skip = true;
							break;
						}
					}
					
					if (alreadyAdded_skip)
						continue;
					/// end test
					
					line = new ArrayList<>();
					line.add(point);
					addedPoints.add(point);
					debug = true;
					break;
				}
				
				if (!debug) {
					System.out.println("ERROR: getLines(...): unable to find next point for new line!");
					
					if (lines.size() > 0)
						return lines;
					else
						return null;
				}
			}
		}
		
		if (lines.size() > 0)
			return lines;
		
		return null;
	}
	
	public boolean findSideWalls() {
		// find start walls
		Tile startLeftWall = null;
		Tile startRightWall = null;
		double distance = Double.MIN_VALUE;
		for (Tile tile1 : start) {
			for (Tile tile2 : start) {
				double curDist = Calculations.distance(tile1, tile2);
				
				if (curDist > distance) {
					startLeftWall = tile1;
					startRightWall = tile2;
					distance = curDist;
				}
			}
		}
		
		int startLeftWallIndex = -1;
		int startRightWallIndex = -1;
		
		for (int i = 0; i < bounds.length; i++) {
			if (startLeftWallIndex == -1 && bounds[i].equals(startLeftWall)) {
				startLeftWallIndex = i;
				continue;
			}
			
			if (startRightWallIndex == -1 && bounds[i].equals(startRightWall)) {
				startRightWallIndex = i;
				continue;
			}
			
			if (startLeftWallIndex != -1 && startRightWallIndex != -1)
				break;
		}
		
		//if (startLeftWall != null && startRightWall != null)
		//	Paint.updatePaintObject(new Tile[] {startLeftWall, startRightWall});
		
		List<Tile> sideWalls = new ArrayList<>();
		
		// gets side walls by removing start and end walls
		for (Tile tile : bounds) {
			boolean add = true;
			for (Tile startTile : start) {
				if (tile.equals(startTile))
					add = false;
			}
			
			if (!add)
				continue;
			
			for (Tile endTile : end) {
				if (tile.equals(endTile))
					add = false;
			}
			
			if (add)
				sideWalls.add(tile);
		}
		
		//if (sideWalls.size() > 0)
		//	Paint.updatePaintObject(sideWalls.toArray(new Tile[0]));
		
		List<Tile> sideWall1 = new ArrayList<>();
		List<Tile> sideWall2 = new ArrayList<>();
		
		/*boolean wall1Complete = false;
		// separate walls (there should be 2)
		for (int i = 0; i < sideWalls.size(); i++) {
			Tile tile1 = sideWalls.get(i);
			Tile tile2 = (i != sideWalls.size() - 1) ? sideWalls.get(i + 1) : sideWalls.get(0);
			
			if (isAdjacent(tile1, tile2)) {
				if (wall1Complete)
					sideWall1.add(tile1);
				else
					sideWall2.add(tile1);
			} else {
				if (!wall1Complete) {
					sideWall2.add(tile1);
					wall1Complete = true;
				} else {
					sideWall1.add(tile1);
				}
			}
		}*/
		

		
		// separate walls (there should be 2)
		Tile[][] lines = points2dToTiles2d(getLines(tilesToPoints(sideWalls.toArray(new Tile[0]))), startLeftWall.plane);
		
		if (lines != null) {
			//System.out.println("Found " + lines.length + " side walls.");
			
			if (lines.length >= 2) {
				sideWall1 = Arrays.asList(lines[0]);
				sideWall2 = Arrays.asList(lines[1]);
			} else {
				System.out.println("ERROR: findSideWalls(): there should be 2 side walls!");
				return false;
			}
		} else
			return false;
		
		
		//if (sideWall1.size() > 0)
		//	Paint.updatePaintObject(sideWall1.toArray(new Tile[0]));
		
		//if (sideWall2.size() > 0)
		//	Paint.updatePaintObject(sideWall2.toArray(new Tile[0]));
		
		List<Tile> leftWall = null;
		List<Tile> rightWall = null;
		
		// find left / right wall
		for (Tile tile : sideWall1) {
			if (isAdjacent(tile, startLeftWall)) {
				leftWall = sideWall1;
				break;
			}
			
			if (isAdjacent(tile, startRightWall)) {
				rightWall = sideWall1;
				break;
			}
		}
		
		// find left / right wall
		for (Tile tile : sideWall2) {
			if (isAdjacent(tile, startLeftWall)) {
				leftWall = sideWall2;
				break;
			}
			
			if (isAdjacent(tile, startRightWall)) {
				rightWall = sideWall2;
				break;
			}
		}
		
		if (leftWall != null && rightWall != null) {
			//System.out.println(leftWall.size() + ", " + rightWall.size());
			
			//leftWall.add(0, startLeftWall);
			//rightWall.add(0, startRightWall);
			
			//if (leftWall.size() > 0)
			//	Paint.updatePaintObject(leftWall.toArray(new Tile[0]));
			
			//if (rightWall.size() > 0)
			//	Paint.updatePaintObject(rightWall.toArray(new Tile[0]));
			
			List<Tile> biggerWall = null;
			List<Tile> smallerWall = null;
			
			if (leftWall.size() > rightWall.size()) {
				biggerWall = leftWall;
				smallerWall = rightWall;
			} else {
				biggerWall = rightWall;
				smallerWall = leftWall;
			}
			
			this.biggerWall = biggerWall.toArray(new Tile[0]);
			this.smallerWall = smallerWall.toArray(new Tile[0]);
			
			return true;
		}
		
		return false;
	}
	
	public Tile[] generateRandomPath() {
		return generateRandomPath1();
	}
	
	public Tile[] generateRandomPath1() {
		//if (area != null)
		//	Paint.updatePaintObject(area.getBoundingTiles());
		
		//if (start != null)
		//	Paint.updatePaintObject(start);
		
		//if (end != null)
		//	Paint.updatePaintObject(end);
		
		boolean reversed = Random.nextBoolean();
		
		Tile[] start;
		Tile[] end;
		
		if (!reversed) {
			start = this.start;
			end = this.end;
		} else {
			start = this.end;
			end = this.start;
		}
		
		Tile randStart = start[Random.nextInt(0, start.length)];
		Tile randEnd = end[Random.nextInt(0, end.length)];
		
		
		if (biggerWall != null && smallerWall != null) {
			List<Tile> path = new ArrayList<>();
			//Tile prevCurTile = randStart;
			Tile curTile = randStart;
			
			int i = -1;
			for (Tile leftTile : biggerWall) {
				i++;
				Tile closestRightTile = null;
				double distance = Double.MAX_VALUE;
				
				Tile[] adjacentTilesToCurTile = getAdjacentTiles(curTile);
				
				for (Tile rightTile : smallerWall) {
					/*List<Point> points = getLine(leftTile.x, leftTile.y, rightTile.x, rightTile.y);
					boolean adjacent = false;
					if (points != null) {
						for (Point point : points) {
							Tile tile = new Tile(point.x, point.y, leftTile.plane);
							
							if (isAdjacent(curTile, tile)) {
								adjacent = true;
								break;
							}
						}
					}
					
					if (!adjacent)
						continue;*/
					
					double curDist = Calculations.distance(leftTile, rightTile);
					
					if (curDist < distance) {
						closestRightTile = rightTile;
						distance = curDist;
					}
				}
				
				if (closestRightTile != null) {
					//if (i == 101)
					//	Paint.updatePaintObject(new Tile[] {leftTile, closestRightTile});
					
					Tile centerTile = new Tile(Math.round(((float)leftTile.x + (float)closestRightTile.x) / 2.0F), Math.round(((float)leftTile.y + (float)closestRightTile.y) / 2.0F), leftTile.plane);
					
					//curTile = centerTile;
					//path.add(centerTile);
					
					
					List<Tile> shuffledAdjacents = new ArrayList<>();
					for (Tile adjacent : adjacentTilesToCurTile) {
						shuffledAdjacents.add(adjacent);
					}
					Collections.shuffle(shuffledAdjacents);
					
					Area triArea = new Area(leftTile, curTile, closestRightTile);
					
					/*Tile[] areaArray = triArea.getTileArray();
					
					boolean isLine = true;
					boolean xDiff = false;
					boolean yDiff = false;
					for (Tile tile1 : areaArray) {
						for (Tile tile2 : areaArray) {
							if (tile1.x != tile2.x)
								xDiff = true;
							if (tile1.y != tile2.y)
								yDiff = true;
						}
					}
					
					if (xDiff && yDiff)
						isLine = false;*/
					
					for (Tile adjacentTile : shuffledAdjacents) {
						//if (adjacentTile.equals(curTile))
						//	continue;
						
						if (triArea.contains(adjacentTile)) {
							//prevCurTile = curTile;
							curTile = adjacentTile;
							path.add(adjacentTile);
							break;
						}
					}
					
					
					
					/*List<Point> points = getLine(leftTile.x, leftTile.y, closestRightTile.x, closestRightTile.y);
					if (points != null) {
						Collections.shuffle(points);
						
						for (Point point : points) {
							Tile tile = new Tile(point.x, point.y, leftTile.plane);
							
							if (isAdjacent(curTile, tile)) {
								curTile = tile;
								path.add(tile);
								break;
							}
						}
					}*/
					
				}
			}
			
			//System.out.println(path.size());
			
			if (path.size() > 0) {
				Tile[] pathArray = path.toArray(new Tile[0]);
				
				if (!reversed) {
					Paint.updatePaintObject(pathArray);
					return pathArray;
				} else {
					Tile[] reversedPath = reverse(pathArray);
					Paint.updatePaintObject(reversedPath);
					return reversedPath;
				}
			}
		}
		
		return null;
	}
	
	public Tile[] generateRandomPath2() {
		return null;
	}
	
	// add to api
	public static Tile[] reverse(Tile[] tiles) {
		if (tiles == null)
			return null;
		
		
		Tile[] reversed = new Tile[tiles.length];
		
		for (int i = 0; i < tiles.length; ++i) {
			reversed[i] = tiles[tiles.length - 1 - i];
		}
		
		return reversed;
	}
	
}
