package org.rs3.api.objects;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.Menu;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.MiniMap;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Player;
import org.rs3.debug.Paint;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Tile {
	
	public int x, y, plane;
	
	public Tile(int x, int y, int plane) {
		this.x = x;
		this.y = y;
		this.plane = plane;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getPlane() {
		return plane;
	}
	
	
	public Tile randomize(final int left, final int right, final int down, final int up) {
		return derive(Random.nextInt(left, right + 1), Random.nextInt(down, up + 1));
	}

	public Tile randomize(final int x, final int y) {
		return randomize(-x, x, -y, y);
	}

	public Tile derive(final int x, final int y) {
		return new Tile(this.x + x, this.y + y, this.plane);
	}

	public boolean validate() {
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		
		final int x = this.x - base.getX();
		final int y = this.y - base.getY();
		
		return x >= 0 && x < 104 && y >= 0 && y < 104;
	}
	

	public Point getMapPoint() {
		return Calculations.worldToMap(x, y);
	}

	public boolean isOnMap() {
		final Point p = getMapPoint();
		
		if (p == null)
			return false;
		
		return p.x != -1 && p.y != -1;
	}

	public Point getCentralPoint() {
		return getPoint(0.5d, 0.5d, 0);
	}
	
	public Point getCentralPoint(int height) {
		return getPoint(0.5d, 0.5d, height);
	}

	public Point getNextViewportPoint() {
		return getPoint(Random.nextDouble(), Random.nextDouble(), 0);
	}

	public boolean contains(final Point point) {
		final Polygon polygon = getBounds();
		return polygon.contains(point);
	}

	public boolean isOnScreen() {
		Polygon bounds = getBounds();
		
		if (bounds == null)
			return false;
		else
			return true;
	}

	public Polygon getBounds() {
		Point localPoint1 = getPoint(0.0D, 0.0D, 0);
		Point localPoint2 = getPoint(1.0D, 0.0D, 0);
		Point localPoint3 = getPoint(0.0D, 1.0D, 0);
		Point localPoint4 = getPoint(1.0D, 1.0D, 0);
		
		if (Calculations.isOnScreen(localPoint1) && Calculations.isOnScreen(localPoint2) &&
				Calculations.isOnScreen(localPoint3) && Calculations.isOnScreen(localPoint4)) {
			
			Polygon localPolygon = new Polygon();
			localPolygon.addPoint(localPoint1.x, localPoint1.y);
			localPolygon.addPoint(localPoint2.x, localPoint2.y);
			localPolygon.addPoint(localPoint4.x, localPoint4.y);
			localPolygon.addPoint(localPoint3.x, localPoint3.y);
			
			return localPolygon;
		}
		
		return null;
	}
	
	/**
	 * Forces each corner to have the same tile height, all height is the same as the center point height.
	 * 
	 * @return
	 */
	public Polygon getFlatBounds() {
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		int z = Calculations.tileHeight((int) ((x - base.getX() + 0.5f) * 0x200), (int) ((y - base.getY() + 0.5f) * 0x200), plane) - 0;
		
		Point localPoint1 = getPoint(0.0D, 0.0D, z, 0);
		Point localPoint2 = getPoint(1.0D, 0.0D, z, 0);
		Point localPoint3 = getPoint(0.0D, 1.0D, z, 0);
		Point localPoint4 = getPoint(1.0D, 1.0D, z, 0);
		
		if (Calculations.isOnScreen(localPoint1) && Calculations.isOnScreen(localPoint2) &&
				Calculations.isOnScreen(localPoint3) && Calculations.isOnScreen(localPoint4)) {
			
			Polygon localPolygon = new Polygon();
			localPolygon.addPoint(localPoint1.x, localPoint1.y);
			localPolygon.addPoint(localPoint2.x, localPoint2.y);
			localPolygon.addPoint(localPoint4.x, localPoint4.y);
			localPolygon.addPoint(localPoint3.x, localPoint3.y);
			
			return localPolygon;
		}
		
		return null;
	}


	public Point getRandomPoint() {
		Point p = getCentralPoint(-300);
		if (p != null) {
			p.x = Random.nextInt(p.x - 12, p.x + 12 + 1);
			p.y = Random.nextInt(p.y - 18, p.y + 18 + 1);
		}
		
		return p;
	}
	
	public boolean hover() {
		Point randPoint = getRandomPoint();
		
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2)) != null)
			return true;
		
		return false;
	}

	public boolean click(int button) {
		Point randPoint = getRandomPoint();
		
		if (randPoint == null)
			return false;
		
		if ((randPoint = Mouse.mouse(randPoint.x, randPoint.y, 2, 2, button)) != null)
			return true;
		
		return false;
	}

	/**
	 * Find closest tile on screen if not on screen
	 * 
	 * @return
	 */
	public boolean walkOnScreen() {
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
			Time.sleepVeryQuick();
		}
		
		if (isOnScreen())
			return loopInteract(null, "Walk here");
		else {
			Tile tile = Walking.getClosestOnScreen(this);
			if (tile != null)
				return tile.loopInteract(null, "Walk here");
		}
		
		return false;
	}
	
	/**
	 * Find closest tile on map if not on map
	 * 
	 * @return
	 */
	public boolean walkOnMap() {
		if (MiniMap.get.isOpen() && (MiniMap.mapBounds.getComponent() == null || !MiniMap.mapBounds.getComponent().isVisible()))
			return false;
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
			Time.sleepVeryQuick();
		}
		
		Tile walkTile = this;
		
		if (!isOnMap())
			walkTile = Walking.getClosestOnMap(this);
		
		if (walkTile.isOnMap()) {
			Point point = Calculations.worldToMap(walkTile.getX(), walkTile.getY());
			
			if (point != null) {
				if ((point = Mouse.mouse(point.x, point.y, 2, 2)) != null) {
					Time.sleepQuickest();
					return Menu.select("Walk here");
				}
			}
		}
		
		return false;
	}
	
	public boolean interact(String action) {
		return interact(true, action);
	}

	public boolean interact(boolean hover, String action) {
		if (hover) {
			if (!hover())
				return false;
			
			Time.sleepVeryQuick();
		}
		
		if (Menu.select(action)) {
			if (Random.nextInt(0, 100) <= 75) {
				Time.sleepVeryQuick();
				Antiban.randomMoveMouse();
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean interact(String action, String option) {
		return interact(true, action, option);
	}

	public boolean interact(boolean hover, String action, String option) {
		if (hover) {
			if (!hover())
				return false;
			
			Time.sleepVeryQuick();
		}
		
		if (Menu.select(action, option)) {
			if (Random.nextInt(0, 100) <= 75) {
				Time.sleepVeryQuick();
				Antiban.randomMoveMouse();
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean loopInteract(Condition breakCondition, String action) {
		return loopInteract(breakCondition, action, null, false);
	}
	
	/**
	 * Same as BasicInteractable without model usage
	 * 
	 * @param breakCondition
	 * @param action
	 * @param option
	 * @param breakOnWrongOption
	 * @return
	 */
	public boolean loopInteract(Condition breakCondition, String action, String option, boolean breakOnWrongOption) {
		if (!isOnScreen()) {
			Camera.turnTo(this);
			Time.sleepQuickest();
			
			if (!isOnScreen()) {
				Camera.setDefaultPitch();
				Time.sleepVeryQuick();
			}
		}
		
		Point centralPoint = getCentralPoint();
		if (centralPoint == null)
			return false;
		
		int absX = getX();
		int absY = getY();
		
		if (MainWidget.get.contains(true, centralPoint)) {
			Camera.turnTo(this);
			Time.sleepQuickest();
		}
		
		Timer cameraTimer = new Timer(5000);
		while (cameraTimer.isRunning()) {
			if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
				return false;
			
			if (breakCondition != null && breakCondition.evaluate())
				return false;
			
			hover();
			Time.sleepQuickest();
			
			boolean hasAction = false;
			boolean firstAction = false;
			
			if (Menu.firstItemContains(action, option)) {
				hasAction = true;
				firstAction = true;
			} else {
				Menu.updateMenuItemsCaches();
				if (Menu.cachedItems == null || !Menu.contains(action, option)) {
					//Camera.setPitch(true);
					//Time.sleepQuickest();
					
					hover();
					Time.sleepQuickest();
					
					Timer menuTimer = new Timer(1000);
					while (menuTimer.isRunning()) {
						if (breakCondition != null && breakCondition.evaluate())
							return false;
						
						if (Menu.firstItemContains(action, option)) {
							hasAction = true;
							firstAction = true;
							break;
						}
						
						Menu.updateMenuItemsCaches();
						
						if (Menu.cachedItems != null) {
							if (Menu.contains(action, option)) {
								hasAction = true;
								break;
							} else if (breakOnWrongOption && Menu.contains(action)) { // wrong option "Iron rocks" rather than "Iron ore rocks" - a non-spawned ore
								return false;
							}
						}
						
						if (MainWidget.get.contains(false, centralPoint)) {
							Camera.turnTo(this);
							Time.sleepQuickest();
						}
						
						hover();
						Time.sleepVeryQuick();
					}
				} else
					hasAction = true;
			}
			
			if (hasAction) {
				if (breakCondition != null && breakCondition.evaluate())
					return false;
				
				if (firstAction) {
					if (Mouse.fastClick(Mouse.LEFT) != null && Menu.waitForSelection(action, option))
						return true;
				} else {
					if (interact(false, action, option))
						return true;
				}
				
				Time.sleepVeryQuick();
			}
			
			Camera.turnTo(absX, absY);
			Time.sleepQuickest();
			Camera.setDefaultPitch();
			Time.sleepVeryQuick();
		}
		
		return false;
	}

	/*public boolean canReach() {
		return Walking.findPath(this).init();
	}*/

	public double distanceTo() {
		return Calculations.distanceTo(this);
	}

	public double distance(final Tile tile) {
		return Calculations.distance(this, tile);
	}

	public Point getPoint(double xOff, double yOff, int height) {
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		return Calculations.groundToScreen((int) ((x - base.getX() + xOff) * 0x200), (int) ((y - base.getY() + yOff) * 0x200), plane, -height);
	}
	
	public Point getPoint(double xOff, double yOff, int tileHeight, int heightOff) {
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		return Calculations.worldToScreen((int) ((x - base.getX() + xOff) * 0x200), tileHeight + heightOff, (int) ((y - base.getY() + yOff) * 0x200));
	}
	
	public boolean loopWalkTo(long maxPeriod) {
		return loopWalkTo(null, maxPeriod, null);
	}
	
	public boolean loopWalkTo(long maxPeriod, Condition breakCondition) {
		return loopWalkTo(null, maxPeriod, breakCondition);
	}
	
	/**
	 * Walks to the tile, making sure not to walk outside area when close enough.
	 * 
	 * @param constrainingArea
	 * @param maxPeriod
	 * @param breakCondition
	 * @return
	 */
	// about the same as Area loopWalkTo(...)
	public boolean loopWalkTo(Area constrainingArea, long maxPeriod, Condition breakCondition) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		Tile dest = Walking.getDestination();
		
		if (Calculations.distanceTo(this) > 4
				|| (player.isMoving() && dest.x != -1 && dest.y != -1 
				&& ((constrainingArea == null && Calculations.distance(this, dest) > 3) || (constrainingArea != null && !constrainingArea.contains(dest))))) {
			
			Tile walkTile = this.randomize(2, 2);
			Tile curTile = Walking.getClosestOnMap(walkTile);
			Walking.walk(curTile);
			Time.sleepMedium();
			
			Timer firstMovementTimer = new Timer(5000);
			while (firstMovementTimer.isRunning()) {
				if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
					return false;
				
				dest = Walking.getDestination();
				
				if (dest.getX() != -1 && dest.getY() != -1 && player.isMoving()) {
					Time.sleepQuick();
					break;
				}
				
				Time.sleep(50);
			}
			
			Timer walkTimer = new Timer(maxPeriod);
			while (walkTimer.isRunning()) {
				if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
					return false;
				
				if (breakCondition != null && breakCondition.evaluate())
					return false;
				
				if (Calculations.distanceTo(walkTile) <= 4) {
					System.out.println("exit tile loop");
					return true;
				}
				
				dest = Walking.getDestination();
				
				if (dest.x != -1 && dest.y != -1 
						&& ((constrainingArea == null && Calculations.distance(this, dest) > 3) || (constrainingArea != null && !constrainingArea.contains(dest)))) {
					
					walkTile = this.randomize(2, 2); // mm coords might be wrong, so choose a different tile
					curTile = Walking.getClosestOnMap(walkTile);
					Walking.walk(curTile);
					Time.sleepMedium();
				} else if (!player.isMoving() || (Calculations.distanceTo(curTile) <= 7 && Calculations.distanceTo(walkTile) >= 7)) {
					curTile = Walking.getClosestOnMap(walkTile);
					Walking.walk(curTile);
					Time.sleepMedium();
				}
				
				Time.sleep(50);
			}
		} else
			return true;
		
		return false;
	}
	
	
	public void drawOnScreen(Graphics g) {
		//Point p = getRandomPoint();
		//g.drawRect(p.x - 1, p.y - 1, 2, 2);
		
		Polygon polygon = getBounds();
		if (polygon != null) {
			Graphics2D g2 = (Graphics2D) g;
			Composite originalComp = g2.getComposite();
			
			AlphaComposite alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2F);
			g2.setComposite(alphaComp);
			g.fillPolygon(polygon);
			g2.setComposite(originalComp);
			
			g.drawPolygon(polygon);
		}
	}
	
	public void drawOnMap(Graphics g) {
		Point mapPoint = getMapPoint();
		if (mapPoint != null)
			g.drawRect(mapPoint.x - 2, mapPoint.y - 2, 4, 4);
	}

	public void draw(Graphics g) {
		drawOnScreen(g);
		drawOnMap(g);
	}
	
	public void renderOnScreen(GL2 gl2) {
		Polygon polygon = getBounds();
		if (polygon != null) {
	        gl2.glBegin( GL.GL_LINE_LOOP );
	        gl2.glColor3f( 0, 1, 0 );
	        for (int i = 0; i < polygon.npoints; i++) {
	        	gl2.glVertex2f( polygon.xpoints[i], polygon.ypoints[i] );
	        }
	        gl2.glFlush();
	        gl2.glEnd();
	        
	        // fill with alpha
	        gl2.glBegin( GL.GL_TRIANGLES );
	        gl2.glColor4f( 0, 1, 0, 0.2F );
	        for (int i = 0; i < polygon.npoints; i++) {
	        	gl2.glVertex2f( polygon.xpoints[i], polygon.ypoints[i] );
	        }
	        gl2.glFlush();
	        gl2.glEnd();
		}
	}
	
	public void renderOnMap(GL2 gl2) {
		Point mapPoint = getMapPoint();
		if (mapPoint != null)
			Paint.renderRectangles(gl2, Color.green, new Rectangle(mapPoint.x - 2, mapPoint.y - 2, 4, 4));
	}

	public void render(GL2 gl2) {
		renderOnScreen(gl2);
		renderOnMap(gl2);
	}
	
	/*public void render(GL2 gl2) {
		Polygon[] polygons = getBounds();
		if (polygons.length == 1) {
			gl2.drawPolygon(polygons[0]);
		}
	}*/

	/*public Location getLocation() {
		return new RegionOffset(x - Game.getBaseX(), y - Game.getBaseY(), plane);
	}*/
	
	public static Tile getNearestTile(Point point) {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		Tile playerTile = player.getData().getCenterLocation().getTile(true);
		if (playerTile == null)
			return null;
		
		Polygon playerTileBounds = playerTile.getBounds();
		
		if (playerTileBounds != null) {
			if (playerTileBounds.contains(point))
				return playerTile;
		} else
			 return null;
		
		int playerTileX = playerTile.getX();
		int playerTileY = playerTile.getY();
		int playerTilePlane = playerTile.getPlane();
		
		if (playerTile != null) {
			for (int x = 0; x < 40; x++) {
				for (int y = 0; y < 40; y++) {
					for (int i = 0; i < 4; i++) {
						int modX = 1;
						int modY = 1;
						
						switch (i) {
						case 0:
							modX = 1;
							modY = 1;
							break;
						case 1:
							modX = 1;
							modY = -1;
							break;
						case 2:
							modX = -1;
							modY = 1;
							break;
						case 3:
							modX = -1;
							modY = -1;
							break;
						}
						
						int modifiedX = x * modX;
						int modifiedY = y * modY;
						
						Tile tile1 = new Tile(playerTileX + modifiedX, playerTileY + modifiedY, playerTilePlane);
						Polygon tile1Bounds = tile1.getBounds();
						
						if (tile1Bounds != null) {
							if (tile1Bounds.contains(point))
								return tile1;
						}
					}
				}
			}
		}
		
		return null;
	}
	
	
	@Override
	public boolean equals(final Object o) {
		if (o != null && o instanceof Tile) {
			final Tile tile = (Tile) o;
			return x == tile.x && y == tile.y && plane == tile.plane;
		}
		
		return false;
	}
}
