package org.rs3.api.objects;

import java.awt.Graphics;

import javax.media.opengl.GL2;

import org.rs3.api.Calculations;
import org.rs3.api.Nodes;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.HardReference;
import org.rs3.api.wrappers.HashTable;
import org.rs3.api.wrappers.ItemDefinition;
import org.rs3.api.wrappers.ItemNode;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.SoftReference;
import org.rs3.util.Condition;

public class GroundItem implements BasicInteractable {
	
	ItemNode itemNode;
	Tile tile;
	
	public GroundItem(Tile tile, ItemNode itemNode) {
		this.itemNode = itemNode;
		this.tile = tile;
	}
	
	@Override
	public Tile getTile() {
		return tile;
	}
	
	public int getId() {
		return itemNode.getItemId();
	}
	
	public int getStackSize() {
		return itemNode.getStackSize();
	}
	
	// TODO: remove after debugging
	public ItemNode getItemNode() {
		return itemNode;
	}
	
	public void draw(Graphics g) {
		Model model = getModel();
		if (model != null) {
			//System.out.println(tile.getX());
			
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			model.draw(g, (tile.getX() - base.getX() + 0.5) * 512, (tile.getY() - base.getY() + 0.5) * 512, 0);
		}
	}
	
	public void render(GL2 gl2) {
		Model model = getModel();
		if (model != null) {
			//System.out.println(tile.getX());
			
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			model.render(gl2, (tile.getX() - base.getX() + 0.5) * 512, (tile.getY() - base.getY() + 0.5) * 512, 0);
		}
	}

	
	@Override
	public Model getModel() {
		return itemNode.getModel();
	}

	@Override
	public PreciseTile getInternalTile() {
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		PreciseTile internal = new PreciseTile((tile.getX() - base.getX() + 0.5f) * 512f, (tile.getY() - base.getY() + 0.5f) * 512f, 0, tile.plane);
		internal.height = Calculations.tileHeight((int)internal.x, (int)internal.y, internal.plane);
		return internal;
	}

	@Override
	public boolean loopInteract(Condition breakCondition, String action) {
		return BasicInteractable.Methods.loopInteract(this, breakCondition, action);
	}

	@Override
	public boolean loopInteract(Condition breakCondition, String action, String option,
			boolean breakOnWrongOption) {

		return BasicInteractable.Methods.loopInteract(this, breakCondition, action, option, breakOnWrongOption);
	}

	@Override
	public boolean loopInteract(boolean keepModelUpdated, Condition breakCondition, String action,
			String option, boolean breakOnWrongOption) {
		
		return BasicInteractable.Methods.loopInteract(this, keepModelUpdated, breakCondition, action, option, breakOnWrongOption);
	}

	@Override
	public boolean loopWalkTo(long maxPeriod) {
		return getTile().loopWalkTo(maxPeriod);
	}

	@Override
	public boolean loopWalkTo(long maxPeriod, Condition breakCondition) {
		return getTile().loopWalkTo(maxPeriod, breakCondition);
	}
}
