package org.rs3.api.objects;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.rs3.api.Calculations;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.wrappers.Player;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Condition;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import sun.security.action.GetLongAction;

/**
 * A polygonal area of tiles.
 *
 * @author Timer and Odell
 */
public class Area {
	protected final Polygon polygon;
	protected int plane = -1;
	private Tile[] tileArrayCache = null;
	private Point[] boundingPointsCache = null;

	/**
	 * Constructs a rectangular area.
	 */
	public Area(final Tile t1, final Tile t2) {
		/*this(
				new Tile(Math.min(t1.getX(), t2.getX()), Math.min(t1.getY(), t2.getY()), t1.getPlane()),
				new Tile(Math.max(t1.getX(), t2.getX()), Math.min(t1.getY(), t2.getY()), t1.getPlane()),
				new Tile(Math.max(t1.getX(), t2.getX()), Math.max(t1.getY(), t2.getY()), t2.getPlane()),
				new Tile(Math.min(t1.getX(), t2.getX()), Math.max(t1.getY(), t2.getY()), t2.getPlane())
		);*/
		
		this(createTileRectangle(t1, t2));
	}

	/**
	 * Constructs a polygonal area.
	 */
	public Area(Tile... bounds) {
		polygon = new Polygon();
		
		for (Tile tile : bounds) {
			
			if (plane != -1 && tile.getPlane() != plane)
				throw new RuntimeException("area does not support 3d");
			
			plane = tile.getPlane();
			addTile(tile);
		}
	}

	public static Tile[] createTileRectangle(final Tile t1, final Tile t2) {
		Tile tl = new Tile(Math.min(t1.getX(), t2.getX()), Math.min(t1.getY(), t2.getY()), t1.getPlane());
		Tile tr = new Tile(Math.max(t1.getX(), t2.getX()), Math.min(t1.getY(), t2.getY()), t1.getPlane());
		Tile br = new Tile(Math.max(t1.getX(), t2.getX()), Math.max(t1.getY(), t2.getY()), t2.getPlane());
		Tile bl = new Tile(Math.min(t1.getX(), t2.getX()), Math.max(t1.getY(), t2.getY()), t2.getPlane());
		
		List<Tile> tlToTr = AreaPath.getLine(tl, tr.derive(-1, 0));
		List<Tile> trToBr = AreaPath.getLine(tr, br.derive(0, -1));
		List<Tile> brToBl = AreaPath.getLine(br, bl.derive(1, 0));
		List<Tile> blToTl = AreaPath.getLine(bl, tl.derive(0, 1));
		
		List<Tile> all = new ArrayList<>();
		all.addAll(tlToTr);
		all.addAll(trToBr);
		all.addAll(brToBl);
		all.addAll(blToTl);
		
		return all.toArray(new Tile[0]);
	}
	
	public void translate(int x, int y) {
		polygon.translate(x, y);
		tileArrayCache = null;
		boundingPointsCache = null;
	}

	/**
	 * @return a bounding rectangle of this area. Includes bounding tiles by growing by 1 in both coordinates.
	 */
	public Rectangle getBounds() {
		Rectangle bounds = polygon.getBounds();
		bounds.grow(1, 1);
		return bounds;
	}

	/**
	 * @return the plane of this area.
	 */
	public int getPlane() {
		return plane;
	}

	/**
	 * Adds a Tile to this Area.
	 *
	 * @param t The Tile to add.
	 */
	public void addTile(Tile t) {
		addTile(t.getX(), t.getY());
	}

	/**
	 * Adds a Tile to this Area.
	 *
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 */
	public void addTile(int x, int y) {
		polygon.addPoint(x, y);
		tileArrayCache = null;
		boundingPointsCache = null;
	}

	/**
	 * Determines whether the given x,y pair is contained in the area. Bounding points are included. Plane is disregarded.
	 *
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return whether the area contains this pair.
	 */
	public boolean contains(int x, int y) {
		for (Point point : getBoundingPoints()) {
			if (point.x == x && point.y == y)
				return true;
		}
		
		return polygon.contains(x, y);
	}

	public boolean contains(Tile... locatables) {
		return contains(true, locatables);
	}
	
	/**
	 * Determines whether at least one of the given tiles is contained in this area.
	 *
	 * @param checkPlane whether to compare current plane.
	 * @param locatables The tiles to verify.
	 * @return <tt>true</tt> if at least one of the tiles is contained, otherwise <tt>false</tt>.
	 */
	public boolean contains(boolean checkPlane, Tile... locatables) {
		for (Tile loc : locatables) {
			if (loc == null)
				continue;
			
			Tile tile = loc;
			if (tile != null && (!checkPlane || plane == tile.getPlane()) && contains(tile.getX(), tile.getY()))
				return true;
		}
		
		return false;
	}

	/**
	 * Determines whether all the given Locatables are contained in this area.
	 *
	 * @param locatables The Locatables to verify.
	 * @return <tt>true</tt> if all Locatables are contained, otherwise <tt>false</tt>.
	 */
	public boolean containsAll(final Tile... locatables) {
		for (final Tile loc : locatables) {
			if (loc == null || !contains(loc)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return the averaged center tile of this area
	 */
	public Tile getCentralTile() {
		return polygon.npoints > 0 ? new Tile((int) Math.round(avg(polygon.xpoints)), (int) Math.round(avg(polygon.ypoints)), plane) : null;
	}
	
	/**
	 * Finds the nearest tile in this area to the local player.
	 *
	 * @return the nearest tile contained in this area closest to the local player.
	 */
	public Tile getNearest() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return null;
		
		return getNearest(player.getData().getCenterLocation().getTile(true));
	}

	public double getDistanceToNearest() {
		Tile nearest = getNearest();
		if (nearest != null)
			return Calculations.distanceTo(nearest);
		
		return -1;
	}
	
	
	/**
	 * Finds the nearest tile in this area to the base Locatable.
	 *
	 * @param base The Locatable to measure the closest tile to.
	 * @return the nearest tile contained in this area closest to the base tile.
	 */
	public Tile getNearest(final Tile base) {
		final Tile[] tiles = getTileArray();
		Tile tile = null;
		long dist = Long.MAX_VALUE, temp;
		for (final Tile t : tiles) {
			temp = (long) Calculations.distance(base, t);
			if (t == null || temp < dist) {
				dist = temp;
				tile = t;
			}
		}
		return tile;
	}

	/**
	 * @return the tiles backing this Area.
	 */
	public Tile[] getBoundingTiles() {
		final Tile[] bounding = new Tile[polygon.npoints];
		for (int i = 0; i < polygon.npoints; i++) {
			bounding[i] = new Tile(polygon.xpoints[i], polygon.ypoints[i], plane);
		}
		return bounding;
	}
	
	private Point[] getBoundingPoints() {
		if (boundingPointsCache == null) {
			Point[] points = new Point[polygon.npoints];
			int[] xpoints = polygon.xpoints;
			int[] ypoints = polygon.ypoints;
			
			for (int i = 0; i < polygon.npoints; i++) {
				points[i] = new Point(xpoints[i], ypoints[i]);
			}
			
			boundingPointsCache = points;
		}
		
		return boundingPointsCache;
	}

	/**
	 * @return an array of all the contained tiles in this area. Includes bounding tiles.
	 */
	public Tile[] getTileArray() {
		if (tileArrayCache == null) {
			Rectangle bounds = getBounds();
			
			ArrayList<Tile> tiles = new ArrayList<>(bounds.width * bounds.height);
			
			
			int xMax = bounds.x + bounds.width;
			int yMax = bounds.y + bounds.height;
			
			for (int x = bounds.x; x < xMax; x++) {
				for (int y = bounds.y; y < yMax; y++) {
					
					if (contains(x, y))
						tiles.add(new Tile(x, y, plane));
				}
			}
			
			tileArrayCache = tiles.toArray(new Tile[tiles.size()]);
		}
		
		return tileArrayCache;
	}
	
	/**
	 * @return a random tile from getTileArray(). Includes bounding tiles.
	 */
	public Tile getRandomTile() {
		Tile[] tileArray = getTileArray();
		return tileArray[Random.nextInt(0, tileArray.length)];
	}

	private double avg(final int... nums) {
		long total = 0;
		for (int i : nums) {
			total += i;
		}
		return (double) total / (double) nums.length;
	}
	
	public boolean loopWalkTo(long maxPeriod) {
		return loopWalkTo(null, maxPeriod, null);
	}
	
	public boolean loopWalkTo(Tile predefTile, long maxPeriod) {
		return loopWalkTo(predefTile, maxPeriod, null);
	}
	
	public boolean loopWalkTo(long maxPeriod, Condition breakCondition) {
		return loopWalkTo(null, maxPeriod, breakCondition);
	}
	
	/**
	 * 
	 * @param predefTile Attempts to walk to this tile, though the method will return when the area is first entered.
	 * @param maxPeriod
	 * @param breakCondition
	 * @return
	 */
	public boolean loopWalkTo(Tile predefTile, long maxPeriod, Condition breakCondition) {
		if (predefTile != null && !this.contains(predefTile))
			return false;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		Tile dest = Walking.getDestination();
		
		if (!this.contains(player.getTile()) 
				|| (player.isMoving() && dest.x != -1 && dest.y != -1 && !this.contains(dest))) {
			
			Tile walkTile = (predefTile == null) ? this.getRandomTile() : predefTile;
			Tile curTile = Walking.getClosestOnMap(walkTile);
			Walking.walk(curTile);
			Time.sleepMedium();
			
			Timer firstMovementTimer = new Timer(5000);
			while (firstMovementTimer.isRunning()) {
				if (ScriptHandler.currentScript.shouldStop())
					return false;
				
				dest = Walking.getDestination();
				
				if (dest.getX() != -1 && dest.getY() != -1 && player.isMoving()) {
					Time.sleepQuick();
					break;
				}
				
				Time.sleep(50);
			}
			
			Timer walkTimer = new Timer(maxPeriod);
			while (walkTimer.isRunning()) {
				if (ScriptHandler.currentScript.shouldStop())
					return false;
				
				if (breakCondition != null && breakCondition.evaluate())
					return false;
				
				if (Calculations.distanceTo(walkTile) <= 4 || this.contains(player.getTile())) {
					System.out.println("exit area loop");
					return true;
				}
				
				dest = Walking.getDestination();
				
				if (dest.x != -1 && dest.y != -1 && !this.contains(dest) /*&& !this.contains(my.getTile())*/) {
					if (predefTile == null)
						walkTile = this.getRandomTile(); // mm coords might be wrong, so choose a different tile
					else {
						Antiban.randomCameraAngle();
						Time.sleepVeryQuick();
					}
					curTile = Walking.getClosestOnMap(walkTile);
					Walking.walk(curTile);
					Time.sleepMedium();
				} else if (!player.isMoving() || (Calculations.distanceTo(curTile) <= 7 && Calculations.distanceTo(walkTile) >= 7)) {
					curTile = Walking.getClosestOnMap(walkTile);
					Walking.walk(curTile);
					Time.sleepMedium();
				}
				
				Time.sleep(50);
			}
		} else
			return true;
		
		return false;
	}
}
