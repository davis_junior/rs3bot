package org.rs3.api.objects;

import java.awt.Graphics;
import java.util.EnumSet;

import javax.media.opengl.GL2;

import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.ObjectDefinition;

public interface GroundObject extends BasicInteractable {
	public enum Type {
		AnimableObject,
		AnimatedAnimableObject,
		Animable, // combined
		
		BoundaryObject,
		AnimatedBoundaryObject,
		Boundary, // combined
		
		WallObject,
		AnimatedWallObject,
		Wall, // combined
		
		FloorObject,
		AnimatedFloorObject,
		Floor, // combined
		
		All; // combined
		
		public EnumSet<Type> getPossibleTypes() {
			switch (this) {
			case AnimableObject:
				return EnumSet.of(AnimableObject);
			case AnimatedAnimableObject:
				return EnumSet.of(AnimatedAnimableObject);
			case Animable:
				return EnumSet.of(AnimableObject, AnimatedAnimableObject);
				
			case BoundaryObject:
				return EnumSet.of(BoundaryObject);
			case AnimatedBoundaryObject:
				return EnumSet.of(AnimatedBoundaryObject);
			case Boundary:
				return EnumSet.of(BoundaryObject, AnimatedBoundaryObject);
				
			case WallObject:
				return EnumSet.of(WallObject);
			case AnimatedWallObject:
				return EnumSet.of(AnimatedWallObject);
			case Wall:
				return EnumSet.of(WallObject, AnimatedWallObject);
				
			case FloorObject:
				return EnumSet.of(FloorObject);
			case AnimatedFloorObject:
				return EnumSet.of(AnimatedFloorObject);
			case Floor:
				return EnumSet.of(FloorObject, AnimatedFloorObject);
				
			case All:
				return EnumSet.of(AnimableObject, AnimatedAnimableObject, BoundaryObject, AnimatedBoundaryObject, WallObject, AnimatedWallObject, FloorObject, AnimatedFloorObject);
			default:
				System.out.println("Error: GroundObjectType.getType(): Unimplemented type!");
				return null;
			}
		}
	}
	
	// TODO: getType() enum
	public Object getObject();
	public Tile getTile();
	public byte getPlane();
	public int getId();
	public ObjectDefinition getDefinition();
	public Model getModel();
	public void draw(Graphics g);
	public void render(GL2 gl2);
}
