package org.rs3.api.objects;

import java.awt.Point;

import org.rs3.api.Camera;
import org.rs3.api.Menu;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.wrappers.Model;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public interface BasicInteractable {
	public Model getModel();
	public Tile getTile();
	public PreciseTile getInternalTile();
	
	/**
	 * Attempts to interact for ~5s, moving the camera and hovering until success.
	 * 
	 * @return true if interaction succeeds.
	 */
	public boolean loopInteract(Condition breakCondition, String action);
	
	/**
	 * Attempts to interact for ~5s, moving the camera and hovering until success.
	 * 
	 * @return true if interaction succeeds.
	 */
	public boolean loopInteract(Condition breakCondition, String action, String option, boolean breakOnWrongOption);
	
	/**
	 * Attempts to interact for ~5s, moving the camera and hovering until success.
	 * 
	 * @return true if interaction succeeds.
	 */
	public boolean loopInteract(boolean keepModelUpdated, Condition breakCondition, String action, String option, boolean breakOnWrongOption);
	
	public boolean loopWalkTo(long maxPeriod);
	public boolean loopWalkTo(long maxPeriod, Condition breakCondition);

	public static class Methods {
		public static boolean loopInteract(BasicInteractable interactable, Condition breakCondition, String action) {
			return loopInteract(interactable, breakCondition, action, null, false);
		}
		
		public static boolean loopInteract(BasicInteractable interactable, Condition breakCondition, String action, String option, boolean breakOnWrongOption) {
			return loopInteract(interactable, false, breakCondition, action, option, breakOnWrongOption);
		}
		
		public static boolean loopInteract(BasicInteractable interactable, boolean keepModelUpdated, Condition breakCondition, String action, String option, boolean breakOnWrongOption) {
			if (!interactable.getTile().isOnScreen()) {
				Camera.turnTo(interactable);
				Time.sleepQuickest();
				
				if (!interactable.getTile().isOnScreen()) {
					Camera.setDefaultPitch();
					Time.sleepVeryQuick();
				}
			}
			
			Model model = interactable.getModel();
			if (model == null) {
				System.out.println("model is null; attempting to hover...");
				interactable.getTile().hover();
				Time.sleepQuick();
				
				if ((model = interactable.getModel()) == null)
					return false;
				else
					System.out.println("Successfully grabbed model");
			}
			
			Point centralPoint = model.getCentralPoint(interactable.getInternalTile());
			if (centralPoint == null)
				return false;
			
			Tile tile = interactable.getTile();
			int absX = tile.getX();
			int absY = tile.getY();
			
			if (MainWidget.get.contains(true, centralPoint)) {
				Camera.turnTo(interactable);
				Time.sleepQuickest();
			}
			
			Timer cameraTimer = new Timer(5000);
			while (cameraTimer.isRunning()) {
				if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.shouldStop())
					return false;
				
				if (breakCondition != null && breakCondition.evaluate())
					return false;
				
				//if (model.click(Mouse.LEFT, aao, 0)) {
				
				if (keepModelUpdated && (model = interactable.getModel()) == null) // update model
					return false;
				model.hover(interactable.getInternalTile());
				Time.sleepQuickest();
				
				boolean hasAction = false;
				boolean firstAction = false;
				
				if (Menu.firstItemContains(action, option)) {
					hasAction = true;
					firstAction = true;
				} else {
					Menu.updateMenuItemsCaches();
					if (Menu.cachedItems == null || !Menu.contains(action, option)) {
						//Camera.setPitch(true);
						//Time.sleepQuickest();
						
						if (keepModelUpdated && (model = interactable.getModel()) == null) // update model
							return false;
						model.hover(interactable.getInternalTile());
						Time.sleepQuickest();
						
						Timer menuTimer = new Timer(1000);
						while (menuTimer.isRunning()) {
							if (breakCondition != null && breakCondition.evaluate())
								return false;
							
							if (Menu.firstItemContains(action, option)) {
								hasAction = true;
								firstAction = true;
								break;
							}
							
							Menu.updateMenuItemsCaches();
							
							if (Menu.cachedItems != null) {
								if (Menu.contains(action, option)) {
									hasAction = true;
									break;
								} else if (breakOnWrongOption && Menu.contains(action)) { // wrong option "Iron rocks" rather than "Iron ore rocks" - a non-spawned ore
									return false;
								}
							}
							
							if (MainWidget.get.contains(false, centralPoint)) {
								Camera.turnTo(interactable);
								Time.sleepQuickest();
							}
							
							if (keepModelUpdated && (model = interactable.getModel()) == null) // update model
								return false;
							model.hover(interactable.getInternalTile());
							Time.sleepVeryQuick();
						}
					} else
						hasAction = true;
				}
				
				if (hasAction) {
					if (breakCondition != null && breakCondition.evaluate())
						return false;
					
					if (firstAction) {
						if (Mouse.fastClick(Mouse.LEFT) != null && Menu.waitForSelection(action, option))
							return true;
					} else {
						if (keepModelUpdated && (model = interactable.getModel()) == null) // update model
							return false;
						if (model.interact(false, action, option, interactable.getInternalTile()))
							return true;
					}
					
					Time.sleepVeryQuick();
				}
				
				Camera.turnTo(absX, absY);
				Time.sleepQuickest();
				Camera.setDefaultPitch();
				Time.sleepVeryQuick();
			}
			
			return false;
		}
	}
}
