package org.rs3.api;

import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Npc;


public enum Familiar {

	//
	// BoB
	//
	THORNY_SNAIL		(13, 16, 6806, 25479882, 12019), // 1785 ends on 314058
	SPIRIT_PACK_PIG_T1	(14, 17, -1, -1, -1),
	SPIRIT_KALPHITE		(25, 22, 6994, 50776778, 12063), // 1785 ends on 445130
	BULL_ANT			(40, 30, 6867, 101370570, 12087), // 1785 ends on 707274
	SPIRIT_PACK_PIG_T2	(44, 35, -1, -1, -1),
	SPIRIT_TERRORBIRD	(52, 36, 6794, 67947210, 12007), // 1785 ends on 838346
	WAR_TORTOISE		(67, 43, 6815, 168741578, 12031), // 1785 ends on 969418
	SPIRIT_PACK_PIG_T3	(84, 55, -1, -1, -1),
	PACK_YAK			(96, 58, -1, -1, -1),
	;
	
	int levelReq;
	int time;
	int npcId; // 1784
	int unknownId; // 1785
	int familiarId; // 1831
	
	private Familiar(int levelReq, int time, int npcId, int unknownId, int familiarId) {
		this.levelReq = levelReq;
		this.time = time;
		this.npcId = npcId;
		this.unknownId = unknownId;
		this.familiarId = familiarId;
	}
	
	public int getLevelReq() {
		return levelReq;
	}
	
	public int getTime() {
		return time;
	}
	
	public int getNpcId() {
		return npcId;
	}
	
	
	public boolean isSummoned() {
		int familiarId = Client.getSettingData().getSettings().get(1831);
		return this.familiarId == familiarId;
	}
	
	// TODO: this may return a different player's npc
	public Npc getNpc() {
		if (isSummoned())
			return Npcs.getNearest(null, npcId);
		
		return null;
	}
	
	public static boolean isFamiliarSummoned() {
		int familiarId = Client.getSettingData().getSettings().get(1831);
		return familiarId != -1;
	}
	
	public static Npc getFamiliarNpc() {
		int npcId = Client.getSettingData().getSettings().get(1784);
		return npcId != -1 ? Npcs.getNearest(null, npcId) : null;
	}
	
	/**
	 * Summoned familiar time left (128 = 1 min, 0 = no time left). Decreases by 64 every 30 seconds.
	 * 
	 * @return time left in ms, 0 if familiar not summoned
	 */
	public static long getFamiliarTimeLeft() {
		int timeLeft = Client.getSettingData().getSettings().get(1786);
		return Math.round(timeLeft / (128D / 60000D));
	}
}
