package org.rs3.api;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.LinkedList;

import javax.xml.crypto.Data;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.accessors.Reflection;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Mouse;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.MenuGroupNode;
import org.rs3.api.wrappers.MenuItemNode;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.NodeDeque;
import org.rs3.api.wrappers.NodeSubQueue;
import org.rs3.callbacks.MouseCallback;
import org.rs3.util.Random;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import data.methods.MethodData;

public class Menu {
	
	public static String[] cachedActions = null;
	public static String[] cachedOptions = null;
	public static String[] cachedItems = null;
	public static MenuItemNode[] cachedNodes = null;
	
	public static Point getLocation() {
		return new Point(Client.getMenuX(), Client.getMenuY());
	}
	
	public static Point getSubLocation() {
		return new Point(Client.getSubMenuX(), Client.getSubMenuY());
	}
	
	public static int getWidth() {
		return Client.getMenuWidth();
	}
	
	public static int getHeight() {
		return Client.getMenuHeight();
	}
	
	public static int getSubWidth() {
		return Client.getSubMenuWidth();
	}
	
	public static int getSubHeight() {
		return Client.getSubMenuHeight();
	}
	
	public static boolean isOpen() {
		return Client.isMenuOpen();
	}
	
	public static boolean isCollapsed() {
		return Client.isMenuCollapsed();
	}
	
	public static MenuItemNode getFirstItemNode() {
		return Client.getFirstMenuItem();
	}
	
	public static MenuItemNode getSecondItemNode() {
		return Client.getSecondMenuItem();
	}
	
	public static String[] getItems() {
		return getItems(getActions(), getOptions());
	}
	
	public static String[] getItems(String actions[], String[] options) {
		final LinkedList<String> output = new LinkedList<String>();

		final int len = Math.min(options.length, actions.length);
		for (int i = 0; i < len; i++) {
			final String option = options[i];
			final String action = actions[i];
			if (option != null && action != null) {
				final String text = action + " " + option;
				output.add(text.trim());
			}
		}

		return output.toArray(new String[0]);
	}
	
	/**
	 * Update menu item cache first!
	 * @param action
	 * @return
	 */
	public static boolean contains(final String action) {
		return getIndex(action) != -1;
	}
	
	/**
	 * Update menu item cache first!
	 * @param action
	 * @param option
	 * @return
	 */
	public static boolean contains(final String action, final String option) {
		return getIndex(action, option) != -1;
	}
	
	public static String[] getActions() {
		return getMenuItemPart(true);
	}

	public static String[] getOptions() {
		return getMenuItemPart(false);
	}
	
	public static String getFirstAction() {
		MenuItemNode firstMin = Menu.getFirstItemNode();
		if (firstMin != null) {
			String firstAction = firstMin.getAction();
			if (firstAction != null)
				return TextUtils.removeHtml(firstAction).trim();
		}
		
		return null;
	}
	
	public static String getSecondAction() {
		MenuItemNode secondMin = Menu.getSecondItemNode();
		if (secondMin != null) {
			String secondAction = secondMin.getAction();
			if (secondAction != null)
				return TextUtils.removeHtml(secondAction).trim();
		}
		
		return null;
	}
	
	public static String getFirstOption() {
		MenuItemNode firstMin = Menu.getFirstItemNode();
		if (firstMin != null) {
			String firstOption = firstMin.getOption();
			if (firstOption != null)
				return TextUtils.removeHtml(firstOption).trim();
		}
		
		return null;
	}
	
	public static String getSecondOption() {
		MenuItemNode secondMin = Menu.getSecondItemNode();
		if (secondMin != null) {
			String secondOption = secondMin.getOption();
			if (secondOption != null)
				return TextUtils.removeHtml(secondOption).trim();
		}
		
		return null;
	}
	
	public static String getFirstItem() {
		MenuItemNode firstMin = Menu.getFirstItemNode();
		if (firstMin != null) {
			String firstAction = firstMin.getAction();
			if (firstAction != null)
				firstAction = TextUtils.removeHtml(firstAction).trim();
			else
				firstAction = "";
				
			String firstOption = firstMin.getOption();
			if (firstOption != null)
				firstOption = TextUtils.removeHtml(firstOption).trim();
			else
				firstOption = "";
			
			String result = (firstAction + " " + firstOption).trim();
			if (!result.isEmpty())
				return result;
		}
		
		return null;
	}
	
	public static String getSecondItem() {
		MenuItemNode secondMin = Menu.getSecondItemNode();
		if (secondMin != null) {
			String secondAction = secondMin.getAction();
			if (secondAction != null)
				secondAction = TextUtils.removeHtml(secondAction).trim();
			else
				secondAction = "";
				
			String secondOption = secondMin.getOption();
			if (secondOption != null)
				secondOption = TextUtils.removeHtml(secondOption).trim();
			else
				secondOption = "";
			
			String result = (secondAction + " " + secondOption).trim();
			if (!result.isEmpty())
				return result;
		}
		
		return null;
	}
	
	public static boolean firstItemContains(String action, String option) {
		if (option == null) {
			String firstAction = getFirstAction();
			return firstAction != null && action.equals(firstAction);
		}
		
		String firstItem = getFirstItem();
		return firstItem != null && firstItem.equals(action + " " + option);
	}
	
	public static boolean secondItemContains(String action, String option) {
		if (option == null) {
			String secondAction = getSecondAction();
			return secondAction != null && action.equals(secondAction);
		}
		
		String secondItem = getSecondItem();
		return secondItem != null && secondItem.equals(action + " " + option);
	}
	
	private static int getIndex(String action) {
		action = action.toLowerCase();
		//final String[] items = getActions();
		final String[] items = cachedActions;
		for (int i = 0; i < items.length; i++) {
			if (items[i].toLowerCase().equals(action)) {
				return i;
			}
		}
		return -1;
	}

	private static int getIndex(String action, String option) {
		if (option == null) {
			return getIndex(action);
		}
		action = action.toLowerCase();
		option = option.toLowerCase();
		//final String[] actions = getActions();
		//final String[] options = getOptions();
		final String[] actions = cachedActions;
		final String[] options = cachedOptions;
		
		if (actions != null && options != null) {
			for (int i = 0; i < Math.min(actions.length, options.length); i++) {
				if (actions[i].toLowerCase().equals(action) && options[i].toLowerCase().equals(option)) {
					return i;
				}
			}
		}
		return -1;
	}

	private static String[] getMenuItemPart(final boolean firstPart) {
		final LinkedList<String> itemsList = new LinkedList<>();
		
		if (isCollapsed()) {
			NodeSubQueue menu = Client.getCollapsedMenuItems();
			
			if (menu == null)
				return null;
			
			try {
				for (Node groupNode = menu.getHead(); groupNode != null; groupNode = menu.getNext()) {
					if (!groupNode.getObject().getClass().getName().equals(data.MenuGroupNode.get.getInternalName()))
						continue;
					
					MenuGroupNode mgn = groupNode.forceCast(MenuGroupNode.class);
					
					final NodeSubQueue submenu = mgn.getItems();
					
					if (submenu == null)
						return null;
					
					for (Node node = submenu.getHead(); node != null; node = submenu.getNext()) {
						if (!node.getObject().getClass().getName().equals(data.MenuItemNode.get.getInternalName()))
							continue;
						
						MenuItemNode min = node.forceCast(MenuItemNode.class);
						
						itemsList.addLast(firstPart ?
								min.getAction() :
								min.getOption());
					}
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		} else {
			try {
				final NodeDeque menu = Client.getMenuItems();
				
				if (menu == null)
					return null;
				
				for (Node node = menu.getHead(); node != null; node = menu.getNext()) {
					if (!node.getObject().getClass().getName().equals(data.MenuItemNode.get.getInternalName()))
						continue;
					
					MenuItemNode min = node.forceCast(MenuItemNode.class);
					
					itemsList.addLast(firstPart ?
							min.getAction() :
							min.getOption());
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
		
		if (itemsList.size() == 0)
			return null;
		
		final String[] items = itemsList.toArray(new String[0]);
		final LinkedList<String> output = new LinkedList<>();
		
		for (int i = items.length - 1; i >= 0; i--) {
			final String item = items[i];
			output.add(item == null ? "" : TextUtils.removeHtml(item));
		}
		
		if (output.size() > 1 && (isCollapsed() ? output.getLast() : output.getFirst()).equals(firstPart ? "Cancel" : "")) {
			Collections.reverse(output);
		}
		
		return output.toArray(new String[0]);
	}
	
	public static void updateMenuItemsCaches() {
		final LinkedList<String> actionsList = new LinkedList<>();
		final LinkedList<String> optionsList = new LinkedList<>();
		final LinkedList<String> itemsList = new LinkedList<>();
		final LinkedList<MenuItemNode> nodeList = new LinkedList<>();
		
		if (isCollapsed()) {
			NodeSubQueue menu = Client.getCollapsedMenuItems();
			
			if (menu != null) {
				try {
					for (Node groupNode = menu.getHead(); groupNode != null; groupNode = menu.getNext()) {
						if (!groupNode.getObject().getClass().getName().equals(data.MenuGroupNode.get.getInternalName()))
							continue;
						
						MenuGroupNode mgn = groupNode.forceCast(MenuGroupNode.class);
						
						final NodeSubQueue submenu = mgn.getItems();
						
						if (submenu != null) {
							for (Node node = submenu.getHead(); node != null; node = submenu.getNext()) {
								if (!node.getObject().getClass().getName().equals(data.MenuItemNode.get.getInternalName()))
									continue;
								
								MenuItemNode min = node.forceCast(MenuItemNode.class);
								
								String action = min.getAction();
								String option = min.getOption();
								
								if (action != null)
									action = TextUtils.removeHtml(action).trim();
								else
									action = "";
								
								if (option != null)
									option = TextUtils.removeHtml(option).trim();
								else
									option = "";
								
								actionsList.addFirst(action);
								optionsList.addFirst(option);
								itemsList.addFirst((action + " " + option).trim());
								nodeList.addFirst(min);
								
								/*actionsList.addLast(action);
								optionsList.addLast(option);
								itemsList.addLast((action + " " + option).trim());*/
							}
						}
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}
		} else {
			try {
				final NodeDeque menu = Client.getMenuItems();
				
				if (menu != null) {
					for (Node node = menu.getHead(); node != null; node = menu.getNext()) {
						if (!node.getObject().getClass().getName().equals(data.MenuItemNode.get.getInternalName()))
							continue;
						
						MenuItemNode min = node.forceCast(MenuItemNode.class);
						
						String action = min.getAction();
						String option = min.getOption();
						
						if (action != null)
							action = TextUtils.removeHtml(action).trim();
						else
							action = "";
						
						if (option != null)
							option = TextUtils.removeHtml(option).trim();
						else
							option = "";
						
						actionsList.addFirst(action);
						optionsList.addFirst(option);
						itemsList.addFirst((action + " " + option).trim());
						nodeList.addFirst(min);
						
						/*actionsList.addLast(action);
						optionsList.addLast(option);
						itemsList.addLast((action + " " + option).trim());*/
					}
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
		
		if (actionsList.size() > 0) {
			/*if (actionsList.size() > 1) {
				//if ((isCollapsed() ? actionsList.getLast() : actionsList.getFirst()).equals("Cancel")) {
					Collections.reverse(actionsList);
					Collections.reverse(optionsList);
					Collections.reverse(itemsList);
				//}
			}*/
			
			cachedActions = actionsList.toArray(new String[0]);
			cachedOptions = optionsList.toArray(new String[0]);
			cachedItems = itemsList.toArray(new String[0]);
			cachedNodes = nodeList.toArray(new MenuItemNode[0]);
		} else {
			cachedActions = null;
			cachedOptions = null;
			cachedItems = null;
			cachedNodes = null;
		}
	}
	
	public static boolean waitForSelection(final String action, final String option) {
		//Timer timer = new Timer(200);
		Timer timer = new Timer(Random.nextInt(250, 300));
		while (timer.isRunning()) {
			if (MouseCallback.lastActionClickX != -1 && MouseCallback.lastActionClickY != -1) {
				MenuItemNode min = MouseCallback.lastActionClickMin;
				if (min != null) {
					if (TextUtils.removeHtml(min.getAction()).equals(action)) {
						if (option == null || TextUtils.removeHtml(min.getOption()).equals(option))
							return true;
					}
				}
			}
			
			Time.sleep(5);
		}
		
		return false;
	}
	
	public static boolean select(final String action) {
		return select(action, null);
	}

	public static boolean select(final String action, final String option) {
		MouseCallback.resetLastActionClickValues();
		
		if (firstItemContains(action, option)) {
			Mouse.fastClick(Mouse.LEFT);
			return waitForSelection(action, option);
		}
		
		// TODO: add second min?
		
		updateMenuItemsCaches();
		if (cachedItems == null) {
			//System.out.println("null");
			return false;
		}
		
		int idx = getIndex(action, option);
		if (!isOpen()) {
			if (idx == -1) {
				//System.out.println("-1");
				return false;
			}
			
			if (idx == 0) {
				Mouse.fastClick(Mouse.LEFT);
				return waitForSelection(action, option);
			}
			
			Mouse.fastClick(Mouse.RIGHT);
			final Timer t = new Timer(100);
			while (t.isRunning() && !isOpen()) {
				Time.sleep(50);
				//Time.sleep(5);
			}
			int sleepTime = Random.nextInt(125, 150) - (int) t.getElapsed();
			if (sleepTime > 0)
				Time.sleep(sleepTime);
			
			if (idx == -1) {
				updateMenuItemsCaches();
				if (cachedItems == null)
					return false;
				
				idx = getIndex(action, option);
			}
			
			return idx != -1 && clickIndex(idx) && waitForSelection(action, option);
		} else if (idx == -1) {
			Timer t = new Timer(5000);
			while (t.isRunning() && isOpen()) {
				Antiban.randomMoveMouse();
				//Mouse.move(0, 0);
				Time.sleepQuick();
				//Time.sleep(Random.nextInt(100, 500));
			}
			
			return false;
		}
		
		return clickIndex(idx) && waitForSelection(action, option);
	}
	
	/**
	 * possibly DETECTABLE!
	 * 
	 * @param action
	 * @return
	 */
	public static boolean invoke(String action) {
		return invoke(action, null);
	}

	/**
	 * possibly DETECTABLE!
	 */
	public static boolean invoke(String action, String option) {
		if (firstItemContains(action, option))
			return invoke(getFirstItemNode());
		else if (secondItemContains(action, option))
			return invoke(getSecondItemNode());
		
		updateMenuItemsCaches();
		if (cachedItems == null)
			return false;
		
		int idx = getIndex(action, option);
		if (idx != -1)
			return invoke(cachedNodes[idx]);
		else
			return false;
	}
	
	/**
	 * possibly DETECTABLE!
	 * 
	 * Invokes the action with Reflection with current mouse coords
	 * 
	 * @param item
	 * @return
	 */
	public static boolean invoke(MenuItemNode item) {
		Point mousePos = Mouse.getClientPos();
		if (mousePos != null)
			return invoke(item, mousePos.x, mousePos.y);
		else
			return false;
	}
	
	/**
	 * possibly DETECTABLE!
	 * 
	 * Invokes the action with Reflection
	 * 
	 * TODO: figure out what boolean value does, usually false but it used in the client code
	 * 
	 * @param item
	 * @param x
	 * @param y
	 * @return
	 */
	public static boolean invoke(MenuItemNode item, int x, int y) {
		if (item != null) {
			try {
				MethodData invokeMethod = data.methods.Callbacks.get.actionClicked.getMethods().get(0);
				if (invokeMethod != null) {
					Class<?>[] classArgs = new Class<?>[] { CustomClassLoader.get.loadClass(data.MenuItemNode.get.getInternalName()), int.class, int.class, boolean.class, int.class };
					Object[] args = new Object[] { item.getObject(), x, y, false, -1296778726 };
					Reflection.invokeMethod(invokeMethod.getInternalClassName(), invokeMethod.getInternalName(), null, classArgs, args);
					return true;
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	public static MenuItemNode getNode(String action) {
		return getNode(action, null);
	}
	
	public static MenuItemNode getNode(String action, String option) {
		updateMenuItemsCaches();
		int idx = getIndex(action, option);
		if (idx != -1)
			return cachedNodes[idx]; 
		
		return null;
	}

	private static boolean clickIndex(final int i) {
		if (!isOpen())
			return false;
		
		//final String[] items = getItems();
		final String[] items = cachedItems;
		
		if (items == null || items.length <= i)
			return false;
		
		if (isCollapsed()) {
			final NodeSubQueue groups = Client.getCollapsedMenuItems();
			if (groups == null)
				return false;
			
			int idx = 0;
			//int idx = 0, mainIdx = 0;
			
			for (int mainIdx = 0; mainIdx < groups.size(); mainIdx++) {
			//for (Node<?> groupNode = groups.getHead(); groupNode != null; groupNode = groups.getNext(), ++mainIdx) {
				//if (!groupNode.getObject().getClass().getName().equals(data.MenuGroupNode.get.getInternalName()))
				//	continue;
				
				//MenuGroupNode g = groupNode.forceCast(MenuGroupNode.class, I_MenuGroupNode.class);
				
				final NodeSubQueue subItems = Client.getCollapsedMenuItems();
				if (subItems == null)
					return false;
				
				//int subIdx = 0;
				for (int subIdx = 0; subIdx < subItems.size(); subIdx++) {
				//for (Node<?> node = subItems.getHead(); node != null; node = subItems.getNext(), ++subIdx) {
					//if (!node.getObject().getClass().getName().equals(data.MenuItemNode.get.getInternalName()))
					//	continue;
					
					//MenuItemNode item = node.forceCast(MenuItemNode.class, I_MenuItemNode.class);
					
					if (idx++ == i) {
						return subIdx == 0 ? clickMain(items, mainIdx) : clickSub(items, mainIdx, subIdx);
					}
				}
			}
			
			return false;
		} else {
			return clickMain(items, i);
		}
	}
	
	private static boolean clickMain(String[] items, int i) {
		//int xOff = Random.nextInt(4, items[i].length() * 4);
		//int yOff = 21 + 16 * i + Random.nextInt(3, 12);
		
		Point menuPoint = getLocation();
		Rectangle rect = new Rectangle(menuPoint.x, menuPoint.y + 21 + 16*i, Menu.getWidth(), 16);
		
		Mouse.mouse(Random.nextInt(rect.x, rect.x + rect.width), Random.nextInt(rect.y, rect.y + rect.height));
		
		// double check
		if (isOpen()) {
			Time.sleepVeryQuick();
			Mouse.fastClick(Mouse.LEFT);
			return true;
		}
		
		return false;
	}

	// TODO: get sub menu item rectangle like main menu item
	private static boolean clickSub(String[] items, int mIdx, int sIdx) {
		//int x = Random.nextInt(4, items[mIdx].length() * 4);
		//int y = 21 + 16 * mIdx + Random.nextInt(3, 12);
		
		Point menuPoint = getLocation();
		Rectangle rect = new Rectangle(menuPoint.x, menuPoint.y + 21 + 16*mIdx, Menu.getWidth(), 16);
		
		Mouse.mouse(Random.nextInt(rect.x, rect.x + rect.width), Random.nextInt(rect.y, rect.y + rect.height));
		Time.sleepVeryQuick();
		
		// double check
		if (isOpen()) {
			Point subLoc = getSubLocation();
			Point start = Mouse.getClientPos();
			int subOff = subLoc.x - start.x;
			
			int x = Random.nextInt(4, items[sIdx].length() * 4);
			
			if (subOff > 0) {
				Mouse.mouse(Random.nextInt(subOff + 4, subOff + Random.nextInt(4, items[sIdx].length() * 2)), start.y);
			} else {
				Mouse.mouse(subLoc.x + x, Mouse.getClientPos().y, 2, 0);
			}
			
			Time.sleepVeryQuick();
			//Time.sleep(Random.nextInt(125, 150));
			if (isOpen()) {
				int y = 16 * sIdx + Random.nextInt(3, 12) + 21;
				Mouse.mouse(subLoc.x + x, subLoc.y + y, 0, 2);
				Time.sleepVeryQuick();
				//Time.sleep(Random.nextInt(125, 150));
				
				if (isOpen()) {
					Mouse.fastClick(Mouse.LEFT);
					return true;
				}
			}
		}
		return false;
	}
}
