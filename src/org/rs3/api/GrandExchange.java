package org.rs3.api;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.Npc;
import org.rs3.database.GrandExchangeRow;
import org.rs3.scripts.mining.OreType;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class GrandExchange {
	
	public static TilePath varrockLStoGEEntrancePath = new TilePath( new Tile(3214, 3376, 0), new Tile(3213, 3376, 0), new Tile(3213, 3377, 0), new Tile(3212, 3377, 0), new Tile(3212, 3378, 0), new Tile(3211, 3378, 0), new Tile(3211, 3379, 0), new Tile(3211, 3380, 0), new Tile(3211, 3381, 0), new Tile(3211, 3382, 0), new Tile(3211, 3383, 0), new Tile(3211, 3384, 0), new Tile(3211, 3385, 0), new Tile(3211, 3386, 0), new Tile(3211, 3387, 0), new Tile(3211, 3388, 0), new Tile(3211, 3389, 0), new Tile(3211, 3390, 0), new Tile(3211, 3391, 0), new Tile(3211, 3392, 0), new Tile(3211, 3393, 0), new Tile(3211, 3394, 0), new Tile(3211, 3395, 0), new Tile(3211, 3396, 0), new Tile(3211, 3397, 0), new Tile(3211, 3398, 0), new Tile(3211, 3399, 0), new Tile(3211, 3400, 0), new Tile(3211, 3401, 0), new Tile(3211, 3402, 0), new Tile(3211, 3403, 0), new Tile(3211, 3404, 0), new Tile(3211, 3405, 0), new Tile(3211, 3406, 0), new Tile(3211, 3407, 0), new Tile(3211, 3408, 0), new Tile(3211, 3409, 0), new Tile(3211, 3410, 0), new Tile(3210, 3410, 0), new Tile(3210, 3411, 0), new Tile(3210, 3412, 0), new Tile(3210, 3413, 0), new Tile(3210, 3414, 0), new Tile(3210, 3415, 0), new Tile(3210, 3416, 0), new Tile(3211, 3416, 0), new Tile(3211, 3417, 0), new Tile(3211, 3418, 0), new Tile(3211, 3419, 0), new Tile(3211, 3420, 0), new Tile(3211, 3421, 0), new Tile(3210, 3421, 0), new Tile(3210, 3422, 0), new Tile(3210, 3423, 0), new Tile(3210, 3424, 0), new Tile(3209, 3424, 0), new Tile(3209, 3425, 0), new Tile(3208, 3425, 0), new Tile(3208, 3426, 0), new Tile(3208, 3427, 0), new Tile(3207, 3427, 0), new Tile(3206, 3427, 0), new Tile(3206, 3428, 0), new Tile(3205, 3428, 0), new Tile(3204, 3428, 0), new Tile(3203, 3428, 0), new Tile(3203, 3429, 0), new Tile(3202, 3429, 0), new Tile(3201, 3429, 0), new Tile(3200, 3429, 0), new Tile(3199, 3429, 0), new Tile(3198, 3429, 0), new Tile(3197, 3429, 0), new Tile(3197, 3430, 0), new Tile(3196, 3430, 0), new Tile(3195, 3430, 0), new Tile(3194, 3430, 0), new Tile(3193, 3430, 0), new Tile(3192, 3430, 0), new Tile(3191, 3430, 0), new Tile(3190, 3430, 0), new Tile(3189, 3430, 0), new Tile(3188, 3430, 0), new Tile(3187, 3430, 0), new Tile(3186, 3430, 0), new Tile(3186, 3431, 0), new Tile(3185, 3431, 0), new Tile(3185, 3432, 0), new Tile(3185, 3433, 0), new Tile(3185, 3434, 0), new Tile(3185, 3435, 0), new Tile(3185, 3436, 0), new Tile(3185, 3437, 0), new Tile(3185, 3438, 0), new Tile(3185, 3439, 0), new Tile(3185, 3440, 0), new Tile(3185, 3441, 0), new Tile(3186, 3441, 0), new Tile(3186, 3442, 0), new Tile(3186, 3443, 0), new Tile(3186, 3444, 0), new Tile(3186, 3445, 0), new Tile(3186, 3446, 0), new Tile(3186, 3447, 0), new Tile(3186, 3448, 0), new Tile(3186, 3449, 0), new Tile(3186, 3450, 0), new Tile(3185, 3450, 0), new Tile(3184, 3450, 0), new Tile(3183, 3450, 0), new Tile(3182, 3450, 0), new Tile(3181, 3450, 0), new Tile(3180, 3450, 0), new Tile(3179, 3450, 0), new Tile(3178, 3450, 0), new Tile(3177, 3450, 0), new Tile(3176, 3450, 0), new Tile(3175, 3450, 0), new Tile(3174, 3450, 0), new Tile(3174, 3451, 0), new Tile(3173, 3451, 0), new Tile(3172, 3451, 0), new Tile(3171, 3451, 0), new Tile(3171, 3452, 0), new Tile(3170, 3452, 0), new Tile(3170, 3453, 0), new Tile(3170, 3454, 0), new Tile(3170, 3455, 0), new Tile(3169, 3455, 0), new Tile(3169, 3456, 0), new Tile(3168, 3456, 0), new Tile(3167, 3456, 0), new Tile(3167, 3457, 0), new Tile(3167, 3458, 0), new Tile(3167, 3459, 0), new Tile(3167, 3460, 0), new Tile(3167, 3461, 0), new Tile(3167, 3462, 0), new Tile(3166, 3462, 0), new Tile(3166, 3463, 0), new Tile(3166, 3464, 0), new Tile(3166, 3465, 0), new Tile(3166, 3466, 0), new Tile(3166, 3467, 0), new Tile(3166, 3468, 0), new Tile(3165, 3468, 0), new Tile(3165, 3469, 0) );
	public static TilePath GEEntranceToSWBoothPath = new TilePath( new Tile(3165, 3469, 0), new Tile(3164, 3469, 0), new Tile(3163, 3469, 0), new Tile(3163, 3470, 0), new Tile(3162, 3470, 0), new Tile(3161, 3470, 0), new Tile(3160, 3470, 0), new Tile(3160, 3471, 0), new Tile(3159, 3471, 0), new Tile(3158, 3471, 0), new Tile(3157, 3471, 0), new Tile(3157, 3472, 0), new Tile(3156, 3472, 0), new Tile(3155, 3472, 0), new Tile(3154, 3472, 0), new Tile(3154, 3473, 0), new Tile(3153, 3473, 0), new Tile(3152, 3473, 0), new Tile(3152, 3474, 0), new Tile(3152, 3475, 0), new Tile(3151, 3475, 0), new Tile(3150, 3475, 0), new Tile(3149, 3475, 0) );
	
	public static boolean walkToEntrance() {
		if (varrockLStoGEEntrancePath.getEnd().distanceTo() < 5)
			return true;
		
		if (varrockLStoGEEntrancePath.getDistanceToNearest() <= 20 || GEEntranceToSWBoothPath.getDistanceToNearest() <= 20)
			return varrockLStoGEEntrancePath.randomize(4, 4).loopWalk(50000, null, true);
		else {
			if (!Lodestone.VARROCK.teleport(true))
				return false;
			
			Time.sleepQuick();
		}
		
		return false;
	}
	
	public static boolean walkToSWBooth() {
		if (GEEntranceToSWBoothPath.getEnd().distanceTo() < 5)
			return true;
		
		if (GEEntranceToSWBoothPath.getDistanceToNearest() <= 20 || walkToEntrance()) {
			Time.sleepVeryQuick();
			
			if (GEEntranceToSWBoothPath.getDistanceToNearest() <= 20) {
				return GEEntranceToSWBoothPath.randomize(4, 4).loopWalk(10000, null, true);
			}
		}
		
		return false;
	}
	
	public static boolean completeTutorial() {
		if (GrandExchangeWidget.get.isTutorialCompletedSetting())
			return true;
		
		if (walkToEntrance()) {
			Time.sleepQuick();
			
			Npc tutor = null;
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				tutor = Npcs.getNearest(null, 6521); // Grand Exchange Tutor
				if (tutor != null)
					break;
				
				Time.sleep(50);
			}
			
			if (tutor != null) {
				if (tutor.loopWalkTo(10000)) {
					Time.sleepQuick();
					
					if (tutor.loopInteract(null, "Talk-to")) {
						// wait for continue message
						boolean success = false;
						timer.reset();
						while (timer.isRunning()) {
							if (ContinueMessage.isOpen()) {
								success = true;
								break;
							}
							
							Time.sleep(50);
						}
						
						if (success) {
							Time.sleepQuick();
							
							success = false;
							timer = new Timer(20000); // manually takes about 10 seconds if fast
							while (timer.isRunning()) {
								if (GrandExchangeWidget.get.isTutorialCompletedSetting())
									return true;
								
								if (ContinueMessage.isOpen()) {
									ContinueMessage.close(true);
									Time.sleepVeryQuick();
								}
								
								if (ChatOptionPane.get.isOpen()) {
									ChatOptionPane.get.chooseOption("I want the basics from you", true);
									Time.sleepVeryQuick();
								}
								
								Time.sleep(50);
							}
						}
					}
				}
			}
		}
		
		return false;
	}
}
