package org.rs3.api;

import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.SettingData;
import org.rs3.api.wrappers.SkillData;

public enum Skill {

	ATTACK(false, 0),
	STRENGTH(false, 2),
	DEFENSE(false, 1),
	RANGED(false, 4),
	PRAYER(false, 5),
	MAGIC(false, 6),
	CONSTITUTION(false, 3),
	CRAFTING(false, 12),
	MINING(false, 14),
	SMITHING(false, 13),
	FISHING(false, 10),
	COOKING(false, 7),
	FIREMAKING(false, 11),
	WOODCUTTING(false, 8),
	RUNECRAFTING(false, 20),
	DUNGEONEERING(false, 24),
	
	AGILITY(true, 16),
	HERBLORE(true, 15),
	THIEVING(true, 17),
	FLETCHING(true, 9),
	SLAYER(true, 18),
	FARMING(true, 19),
	CONSTRUCTION(true, 22),
	HUNTER(true, 21),
	SUMMONING(true, 23),
	DIVINATION(true, 25)
	;
	
	boolean members;
	int skillDataId;
	
	private Skill(boolean members, int skillDataId) {
		this.members = members;
		this.skillDataId = skillDataId;
	}
	
	public SkillData getSkillData() {
		SettingData settingData = Client.getSettingData();
		if (settingData == null)
			return null;
		
		Array1D<SkillData> skillArray = settingData.getSkillArray();
		if (skillArray == null)
			return null;
		
		// TODO: use more efficient method to test if skill array is loaded, if not loaded, all levels are 1, this won't work for new accounts that haven't leveled at all
		boolean all1 = true;
		for (SkillData skillData : skillArray.getAll()) {
			if (skillData.getRealLevel() != 1) {
				all1 = false;
				break;
			}
		}
		if (all1)
			return null;
		
		return skillArray.get(skillDataId);
	}
	
	// wrapper
	public int getExperience() {
		SkillData skillData = getSkillData();
		if (skillData == null)
			return -1;
		
		return skillData.getExperience();
	}
	
	// wrapper
	public int getRealLevel() {
		SkillData skillData = getSkillData();
		if (skillData == null)
			return -1;
		
		return skillData.getRealLevel();
	}
	
	// wrapper
	public int getCurrentLevel() {
		SkillData skillData = getSkillData();
		if (skillData == null)
			return -1;
		
		return skillData.getCurrentLevel();
	}
}
