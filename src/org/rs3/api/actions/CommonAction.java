package org.rs3.api.actions;

public interface CommonAction {
	
	public boolean doAction();
	
}
