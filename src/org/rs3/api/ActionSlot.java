package org.rs3.api;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.rs3.api.ActionBar.AbilityActionType;
import org.rs3.api.ActionBar.AbilitySkillType;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.ComponentData;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.AbilitySubTab;
import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.CustomizableActionBarWidget;
import org.rs3.api.interfaces.widgets.DefenceAbilitiesSubInterfaceTabWidget;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MagicAbilitiesSubInterfaceTabWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.MeleeAbilitiesSubInterfaceTabWidget;
import org.rs3.api.interfaces.widgets.PrayersSubInterfaceTabWidget;
import org.rs3.api.interfaces.widgets.RangedAbilitiesSubInterfaceTabWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterface;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.interfaces.widgets.useless.BeginnerHintMessages;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.util.Random;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;

public enum ActionSlot {

	BAR1_SLOT1(1, 1, 739, 823, ActionBarWidget.slot1Data, CustomizableActionBarWidget.slot1Data, KeyEvent.VK_1),
	BAR1_SLOT2(1, 2, 740, 824, ActionBarWidget.slot2Data, CustomizableActionBarWidget.slot2Data, KeyEvent.VK_2),
	BAR1_SLOT3(1, 3, 741, 825, ActionBarWidget.slot3Data, CustomizableActionBarWidget.slot3Data, KeyEvent.VK_3),
	BAR1_SLOT4(1, 4, 742, 826, ActionBarWidget.slot4Data, CustomizableActionBarWidget.slot4Data, KeyEvent.VK_4),
	BAR1_SLOT5(1, 5, 743, 827, ActionBarWidget.slot5Data, CustomizableActionBarWidget.slot5Data, KeyEvent.VK_5),
	BAR1_SLOT6(1, 6, 744, 828, ActionBarWidget.slot6Data, CustomizableActionBarWidget.slot6Data, KeyEvent.VK_6),
	BAR1_SLOT7(1, 7, 745, 829, ActionBarWidget.slot7Data, CustomizableActionBarWidget.slot7Data, KeyEvent.VK_7),
	BAR1_SLOT8(1, 8, 746, 830, ActionBarWidget.slot8Data, CustomizableActionBarWidget.slot8Data, KeyEvent.VK_8),
	BAR1_SLOT9(1, 9, 747, 831, ActionBarWidget.slot9Data, CustomizableActionBarWidget.slot9Data, KeyEvent.VK_9),
	BAR1_SLOT10(1, 10, 748, 832, ActionBarWidget.slot10Data, CustomizableActionBarWidget.slot10Data, KeyEvent.VK_0),
	BAR1_SLOT11(1, 11, 749, 833, ActionBarWidget.slot11Data, CustomizableActionBarWidget.slot11Data, KeyEvent.VK_MINUS),
	BAR1_SLOT12(1, 12, 750, 834, ActionBarWidget.slot12Data, CustomizableActionBarWidget.slot12Data, KeyEvent.VK_EQUALS),
	BAR1_SLOT13(1, 13, 4415, 4429, ActionBarWidget.slot13Data, CustomizableActionBarWidget.slot13Data, -1),
	BAR1_SLOT14(1, 14, 4416, 4430, ActionBarWidget.slot14Data, CustomizableActionBarWidget.slot14Data, -1),
	
	BAR2_SLOT1(2, 1, 751, 835, ActionBarWidget.slot1Data, CustomizableActionBarWidget.slot1Data, KeyEvent.VK_1),
	BAR2_SLOT2(2, 2, 752, 836, ActionBarWidget.slot2Data, CustomizableActionBarWidget.slot2Data, KeyEvent.VK_2),
	BAR2_SLOT3(2, 3, 753, 837, ActionBarWidget.slot3Data, CustomizableActionBarWidget.slot3Data, KeyEvent.VK_3),
	BAR2_SLOT4(2, 4, 754, 838, ActionBarWidget.slot4Data, CustomizableActionBarWidget.slot4Data, KeyEvent.VK_4),
	BAR2_SLOT5(2, 5, 755, 839, ActionBarWidget.slot5Data, CustomizableActionBarWidget.slot5Data, KeyEvent.VK_5),
	BAR2_SLOT6(2, 6, 756, 840, ActionBarWidget.slot6Data, CustomizableActionBarWidget.slot6Data, KeyEvent.VK_6),
	BAR2_SLOT7(2, 7, 757, 841, ActionBarWidget.slot7Data, CustomizableActionBarWidget.slot7Data, KeyEvent.VK_7),
	BAR2_SLOT8(2, 8, 758, 842, ActionBarWidget.slot8Data, CustomizableActionBarWidget.slot8Data, KeyEvent.VK_8),
	BAR2_SLOT9(2, 9, 759, 843, ActionBarWidget.slot9Data, CustomizableActionBarWidget.slot9Data, KeyEvent.VK_9),
	BAR2_SLOT10(2, 10, 760, 844, ActionBarWidget.slot10Data, CustomizableActionBarWidget.slot10Data, KeyEvent.VK_0),
	BAR2_SLOT11(2, 11, 761, 845, ActionBarWidget.slot11Data, CustomizableActionBarWidget.slot11Data, KeyEvent.VK_MINUS),
	BAR2_SLOT12(2, 12, 762, 846, ActionBarWidget.slot12Data, CustomizableActionBarWidget.slot12Data, KeyEvent.VK_EQUALS),
	BAR2_SLOT13(2, 13, 4417, 4431, ActionBarWidget.slot13Data, CustomizableActionBarWidget.slot13Data, -1),
	BAR2_SLOT14(2, 14, 4418, 4432, ActionBarWidget.slot14Data, CustomizableActionBarWidget.slot14Data, -1),
	
	BAR3_SLOT1(3, 1, 763, 847, ActionBarWidget.slot1Data, CustomizableActionBarWidget.slot1Data, KeyEvent.VK_1),
	BAR3_SLOT2(3, 2, 764, 848, ActionBarWidget.slot2Data, CustomizableActionBarWidget.slot2Data, KeyEvent.VK_2),
	BAR3_SLOT3(3, 3, 765, 849, ActionBarWidget.slot3Data, CustomizableActionBarWidget.slot3Data, KeyEvent.VK_3),
	BAR3_SLOT4(3, 4, 766, 850, ActionBarWidget.slot4Data, CustomizableActionBarWidget.slot4Data, KeyEvent.VK_4),
	BAR3_SLOT5(3, 5, 767, 851, ActionBarWidget.slot5Data, CustomizableActionBarWidget.slot5Data, KeyEvent.VK_5),
	BAR3_SLOT6(3, 6, 768, 852, ActionBarWidget.slot6Data, CustomizableActionBarWidget.slot6Data, KeyEvent.VK_6),
	BAR3_SLOT7(3, 7, 769, 853, ActionBarWidget.slot7Data, CustomizableActionBarWidget.slot7Data, KeyEvent.VK_7),
	BAR3_SLOT8(3, 8, 770, 854, ActionBarWidget.slot8Data, CustomizableActionBarWidget.slot8Data, KeyEvent.VK_8),
	BAR3_SLOT9(3, 9, 771, 855, ActionBarWidget.slot9Data, CustomizableActionBarWidget.slot9Data, KeyEvent.VK_9),
	BAR3_SLOT10(3, 10, 772, 856, ActionBarWidget.slot10Data, CustomizableActionBarWidget.slot10Data, KeyEvent.VK_0),
	BAR3_SLOT11(3, 11, 773, 857, ActionBarWidget.slot11Data, CustomizableActionBarWidget.slot11Data, KeyEvent.VK_MINUS),
	BAR3_SLOT12(3, 12, 774, 858, ActionBarWidget.slot12Data, CustomizableActionBarWidget.slot12Data, KeyEvent.VK_EQUALS),
	BAR3_SLOT13(3, 13, 4419, 4433, ActionBarWidget.slot13Data, CustomizableActionBarWidget.slot13Data, -1),
	BAR3_SLOT14(3, 14, 4420, 4434, ActionBarWidget.slot14Data, CustomizableActionBarWidget.slot14Data, -1),
	;
	
	int barIndex;
	int index;
	int settingId;
	int itemIdSettingId;
	ComponentData mainSlotData;
	ComponentData customizableSlotData;
	int hotkeyCode;
	
	private ActionSlot(int barIndex, int index, int settingId, int itemIdSettingId, ComponentData mainSlotData, ComponentData customizableSlotData, int hotkeyCode) {
		this.barIndex = barIndex;
		this.index = index;
		this.settingId = settingId;
		this.itemIdSettingId = itemIdSettingId;
		this.mainSlotData = mainSlotData;
		this.customizableSlotData = customizableSlotData;
		this.hotkeyCode = hotkeyCode;
	}
	
	public int getBarIndex() {
		return barIndex;
	}
	
	public int getIndex() {
		return index;
	}
	
	public int getHotkeyCode() {
		return hotkeyCode;
	}
	
	public int getSettingValue() {
		int settingValue = Client.getSettingData().getSettings().get(this.settingId, 0xFFF);
		return settingValue;
	}
	
	public int getItemId() {
		int itemId = Client.getSettingData().getSettings().get(this.itemIdSettingId, 0xFFFF); // TODO: mask might need to be bigger
		return itemId;
	}
	
	public Component getMainSlot() {
		return mainSlotData.getComponent();
	}
	
	public Ability getAbility() {
		int settingValue = getSettingValue();
		for (Ability ability : Ability.values()) {
			if (ability.getSettingValue() == settingValue)
				return ability;
		}
		
		return null;
	}
	
	public boolean setItem(int itemId) {
		if (getItemId() == itemId)
			return true;
		
		if (ContinueMessage.isOpen())
			ContinueMessage.close(true); // first time using revolution
		if (BeginnerHintMessages.get.isOpen()) // interferes with setting abilities, etc.
			BeginnerHintMessages.get.close(true);
		if (ChatOptionPane.get.isOpen())
			ChatOptionPane.get.chooseYes(true); // overwrite action bar
		
		if (!ActionBar.setActionBar(this.barIndex))
			return false;
		
		// for an item, the customizable bar does not need to be present, you can drag directly from backpack without powers subinterface open
		
		// close subinterface (customizable ability bar) because it likely blocks inventory
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!MainWidget.get.closeSubInterface(true))
				return false;
			Time.sleepMedium();
		}
		
		if (ItemTable.INVENTORY.containsAny(itemId)) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				Time.sleepQuick();
				
				Component itemComp = InventoryWidget.get.getItem(itemId);
				if (itemComp != null) {
					if (CustomizableActionBarWidget.get.isOpen()) {
						Component slotComp = customizableSlotData.getComponent();
						if (slotComp != null && slotComp.isVisible())
							return itemComp.drag(slotComp, Mouse.LEFT);
					} else if (MainWidget.get.maximiseActionBar(true) && ActionBarWidget.get.isOpen()) {
						Component slotComp = mainSlotData.getComponent();
						if (slotComp != null && slotComp.isVisible())
							return itemComp.drag(slotComp, Mouse.LEFT);
					}
				}
			}
		}
		
		return false;
	}
	
	public boolean setAbility(Ability ability) {
		return setAbility(ability, false);
	}
	
	public boolean setAbility(Ability ability, boolean waitAfter) {
		if (ability.equals(getAbility()))
			return true;
		
		if (ContinueMessage.isOpen())
			ContinueMessage.close(true); // first time using revolution
		if (BeginnerHintMessages.get.isOpen()) // interferes with setting abilities, etc.
			BeginnerHintMessages.get.close(true);
		if (ChatOptionPane.get.isOpen())
			ChatOptionPane.get.chooseYes(true); // overwrite action bar
		
		if (ability.equals(Ability.EMPTY)) {
			if (CustomizableActionBarWidget.get.isOpen()) {
				if (!ActionBar.setActionBar(this.barIndex))
					return false;
				
				Component slotComp = customizableSlotData.getComponent();
				if (slotComp != null && slotComp.isVisible()) {
					Point onScreenPoint = MainWidget.get.getRandomOnScreenPoint();
					if (onScreenPoint != null) {
						boolean result = slotComp.drag(onScreenPoint.x, onScreenPoint.y, Mouse.LEFT);
						if (waitAfter)
							Time.sleepQuick();
						return result;
					}
				}
			} else {
				// close subinterface to make more room for removing abilities from main action bar
				if (MainWidget.get.isSubInterfaceOpen()) {
					if (!MainWidget.get.closeSubInterface(true))
						return false;
					Time.sleepQuick();
				}
				
				if (!ActionBar.setActionBar(this.barIndex))
					return false;
				
				if (ActionBarWidget.get.unlockActionBar(true)) {
					Component slotComp = mainSlotData.getComponent();
					if (slotComp != null && slotComp.isVisible()) {
						Point onScreenPoint = MainWidget.get.getRandomOnScreenPoint();
						if (onScreenPoint != null) {
							boolean result = slotComp.drag(onScreenPoint.x, onScreenPoint.y, Mouse.LEFT);
							if (waitAfter)
								Time.sleepQuick();
							return result;
						}
					}
				}
			}
			
			return false;
		} else if (ability.equals(Ability.ITEM)) {
			System.out.println("ERROR: ActionSlot.setAbility() - use setItem() instead!");
			return false;
		}
		
		boolean success = false;
		AbilitySubTab subTab = ability.getSubTab();
		if (subTab != null) {
			success = subTab.open(true);
			Time.sleepQuick();
		} else if (ability.getSkillType().getActionType().equals(AbilityActionType.PRAYERS)) {
			success = SubInterfaceTab.POWERS_PRAYERS.open(true);
			Time.sleepQuick();
		}
		
		if (success) {
			if (!ActionBar.setActionBar(this.barIndex))
				return false;
			
			Component[] abilities;
			switch (ability.getSkillType().getActionType()) {
			case MELEE:
				abilities = MeleeAbilitiesSubInterfaceTabWidget.get.getAbilities();
				break;
			case RANGED:
				abilities = RangedAbilitiesSubInterfaceTabWidget.get.getAbilities();
				break;
			case MAGIC:
				abilities = MagicAbilitiesSubInterfaceTabWidget.get.getAbilities();
				break;
			case DEFENSIVE:
				abilities = DefenceAbilitiesSubInterfaceTabWidget.get.getAbilities();
				break;
			case PRAYERS:
				abilities = PrayersSubInterfaceTabWidget.get.getPrayers();
				break;
			default:
				System.out.println("ERROR: ActionSlot.setAbility() - Unknown action type!");
				return false;
			}
			
			if (abilities != null) {
				Component abilityComp = null;
				for (Component a : abilities) {
					if (a != null) {
						String name = a.getComponentName();
						if (name != null && TextUtils.removeHtml(name).equals(ability.getName())) {
							abilityComp = a;
							break;
						}
					}
				}
				
				if (abilityComp != null) {
					Component slotComp = customizableSlotData.getComponent();
					if (slotComp != null && slotComp.isVisible()) {
						boolean result = abilityComp.drag(slotComp, Mouse.LEFT);
						if (waitAfter)
							Time.sleepQuick();
						return result;
					}
				}
			}
		}
		
		return false;
	}
	
	public boolean interact(String action) {
		return interact(false, action, null);
	}
	
	public boolean interact(String action, String option) {
		return interact(false, action, option);
	}
	
	public boolean interact(boolean waitAfter, String action) {
		return interact(waitAfter, action, null);
	}
	
	public boolean interact(boolean waitAfter, String action, String option) {
		if (MainWidget.get.maximiseActionBar(true) && ActionBarWidget.get.isOpen()) {
			Component mainSlot = this.getMainSlot();
			if (mainSlot != null && mainSlot.isVisible()) {
				String[] actions = mainSlot.getActions();
				if (getHotkeyCode() != -1 && Random.nextInt(0, 100) < 90 && ((getAbility() != null && getAbility().getSpell() != null) || (actions != null && actions[0] != null && actions[0].equals(action)))) {
					Keyboard.sendKey(getHotkeyCode());
					if (waitAfter)
						Time.sleepQuick();
					return true;
				} else {
					boolean result = mainSlot.interact(action, option);
					if (waitAfter)
						Time.sleepQuick();
					return result;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * If activated but waiting to be used in proper situation with proper requirements
	 * @return
	 */
	public boolean isQueued() {
		if (ActionBar.isAbilityQueued())
			return ActionBar.getCurrentQueuedActionSlot() == getIndex();
		
		return false;
	}
	
	/**
	 * Current bar slot
	 * 
	 * @param index 1 to 14 inclusive
	 * @return
	 */
	public static ActionSlot getSlot(int index) {
		if (index > 0 && index <= 14) {
			int currentActionBar = ActionBar.getCurrentActionBar();
			switch (currentActionBar) {
			case 1:
				switch (index) {
					case 1:
						return BAR1_SLOT1;
					case 2:
						return BAR1_SLOT2;
					case 3:
						return BAR1_SLOT3;
					case 4:
						return BAR1_SLOT4;
					case 5:
						return BAR1_SLOT5;
					case 6:
						return BAR1_SLOT6;
					case 7:
						return BAR1_SLOT7;
					case 8:
						return BAR1_SLOT8;
					case 9:
						return BAR1_SLOT9;
					case 10:
						return BAR1_SLOT10;
					case 11:
						return BAR1_SLOT11;
					case 12:
						return BAR1_SLOT12;
					case 13:
						return BAR1_SLOT13;
					case 14:
						return BAR1_SLOT14;
				}
				break;
			case 2:
				switch (index) {
				case 1:
					return BAR2_SLOT1;
				case 2:
					return BAR2_SLOT2;
				case 3:
					return BAR2_SLOT3;
				case 4:
					return BAR2_SLOT4;
				case 5:
					return BAR2_SLOT5;
				case 6:
					return BAR2_SLOT6;
				case 7:
					return BAR2_SLOT7;
				case 8:
					return BAR2_SLOT8;
				case 9:
					return BAR2_SLOT9;
				case 10:
					return BAR2_SLOT10;
				case 11:
					return BAR2_SLOT11;
				case 12:
					return BAR2_SLOT12;
				case 13:
					return BAR2_SLOT13;
				case 14:
					return BAR2_SLOT14;
			}
			break;
			case 3:
				switch (index) {
				case 1:
					return BAR3_SLOT1;
				case 2:
					return BAR3_SLOT2;
				case 3:
					return BAR3_SLOT3;
				case 4:
					return BAR3_SLOT4;
				case 5:
					return BAR3_SLOT5;
				case 6:
					return BAR3_SLOT6;
				case 7:
					return BAR3_SLOT7;
				case 8:
					return BAR3_SLOT8;
				case 9:
					return BAR3_SLOT9;
				case 10:
					return BAR3_SLOT10;
				case 11:
					return BAR3_SLOT11;
				case 12:
					return BAR3_SLOT12;
				case 13:
					return BAR3_SLOT13;
				case 14:
					return BAR3_SLOT14;
			}
			break;
			default:
				System.out.println("ActionSlot.getSlot() current action bar unknown!");
			}
		}
		
		return null;
	}
	
	/**
	 * Searches all action bars.
	 * Searches for the first slot that is set to the passed ability.
	 * 
	 * @param ability
	 * @return
	 */
	public static ActionSlot getSlot(Ability ability) {
		for (ActionSlot slot : values()) {
			if (slot.getAbility().equals(ability))
				return slot;
		}
		
		return null;
	}
	
	/**
	 * Current bar slot
	 * 
	 * @return
	 */
	public static ActionSlot getRandomSlot() {
		ActionSlot[] slots = ActionBar.getCurrentSlots();
		return slots[Random.nextInt(0, slots.length)];
	}
	
	/**
	 * Current bar slot
	 * 
	 * @return
	 */
	public static ActionSlot getRandomEmptySlot() {
		List<ActionSlot> list = Arrays.asList(ActionBar.getCurrentSlots());
		Collections.shuffle(list);
		
		for (ActionSlot slot : list) {
			if (slot.getAbility().equals(Ability.EMPTY))
				return slot;
		}
		
		return null;
	}
	
	/**
	 * Current bar slot
	 * 
	 * @return
	 */
	public static ActionSlot getNextEmptySlot() {
		ActionSlot[] slots = ActionBar.getCurrentSlots();
		
		for (ActionSlot slot : slots) {
			if (slot.getAbility().equals(Ability.EMPTY))
				return slot;
		}
		
		return null;
	}
}
