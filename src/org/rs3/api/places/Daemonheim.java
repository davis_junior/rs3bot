package org.rs3.api.places;

import org.rs3.api.Constants;
import org.rs3.api.EquipmentSlot;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.Constants.Animation;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.DungeoneeringFloorSettingsWidget;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Daemonheim {

	private static final TilePath tavlerleyLStoDaemonheimFerryPath = new TilePath( new Tile(2879, 3443, 0), new Tile(2880, 3443, 0), new Tile(2880, 3442, 0), new Tile(2880, 3441, 0), new Tile(2881, 3441, 0), new Tile(2881, 3440, 0), new Tile(2881, 3439, 0), new Tile(2881, 3438, 0), new Tile(2882, 3438, 0), new Tile(2882, 3437, 0), new Tile(2882, 3436, 0), new Tile(2882, 3435, 0), new Tile(2882, 3434, 0), new Tile(2883, 3434, 0), new Tile(2883, 3433, 0), new Tile(2883, 3432, 0), new Tile(2883, 3431, 0), new Tile(2883, 3430, 0), new Tile(2883, 3429, 0), new Tile(2883, 3428, 0), new Tile(2883, 3427, 0), new Tile(2883, 3426, 0), new Tile(2883, 3425, 0), new Tile(2883, 3424, 0), new Tile(2883, 3423, 0), new Tile(2883, 3422, 0), new Tile(2883, 3421, 0), new Tile(2882, 3421, 0), new Tile(2882, 3420, 0), new Tile(2882, 3419, 0), new Tile(2882, 3418, 0), new Tile(2882, 3417, 0), new Tile(2882, 3416, 0), new Tile(2882, 3415, 0), new Tile(2882, 3414, 0), new Tile(2881, 3414, 0), new Tile(2881, 3413, 0), new Tile(2881, 3412, 0), new Tile(2880, 3412, 0), new Tile(2879, 3412, 0), new Tile(2879, 3411, 0), new Tile(2878, 3411, 0), new Tile(2877, 3411, 0), new Tile(2877, 3410, 0), new Tile(2876, 3410, 0), new Tile(2875, 3410, 0), new Tile(2874, 3410, 0), new Tile(2874, 3409, 0), new Tile(2873, 3409, 0), new Tile(2872, 3409, 0), new Tile(2872, 3408, 0), new Tile(2872, 3407, 0), new Tile(2872, 3406, 0), new Tile(2872, 3405, 0), new Tile(2872, 3404, 0), new Tile(2872, 3403, 0), new Tile(2872, 3402, 0), new Tile(2871, 3402, 0) );
	private static final int bryllThoksdottirId = 14889; // Npc next to Daemonheim ferry below Taverley
	private static final Tile sailTaverleyDaemonheimFerryLandingTile = new Tile(3511, 3693, 0);
	
	private static final TilePath daemonheimFerryToDaemonheimEntrancePath = new TilePath( new Tile(3511, 3693, 0), new Tile(3510, 3693, 0), new Tile(3509, 3693, 0), new Tile(3508, 3693, 0), new Tile(3507, 3693, 0), new Tile(3506, 3693, 0), new Tile(3505, 3693, 0), new Tile(3504, 3693, 0), new Tile(3503, 3693, 0), new Tile(3502, 3693, 0), new Tile(3501, 3693, 0), new Tile(3501, 3694, 0), new Tile(3500, 3694, 0), new Tile(3499, 3694, 0), new Tile(3498, 3694, 0), new Tile(3497, 3694, 0), new Tile(3496, 3694, 0), new Tile(3495, 3694, 0), new Tile(3494, 3694, 0), new Tile(3493, 3694, 0), new Tile(3493, 3693, 0), new Tile(3492, 3693, 0), new Tile(3492, 3692, 0), new Tile(3492, 3691, 0), new Tile(3491, 3691, 0), new Tile(3491, 3690, 0), new Tile(3491, 3689, 0), new Tile(3490, 3689, 0), new Tile(3490, 3688, 0), new Tile(3490, 3687, 0), new Tile(3489, 3687, 0), new Tile(3488, 3687, 0), new Tile(3487, 3687, 0), new Tile(3486, 3687, 0), new Tile(3485, 3687, 0), new Tile(3484, 3687, 0), new Tile(3483, 3687, 0), new Tile(3482, 3687, 0), new Tile(3481, 3687, 0), new Tile(3480, 3687, 0), new Tile(3479, 3687, 0), new Tile(3478, 3687, 0), new Tile(3477, 3687, 0), new Tile(3476, 3687, 0), new Tile(3475, 3687, 0), new Tile(3474, 3687, 0), new Tile(3474, 3688, 0), new Tile(3473, 3688, 0), new Tile(3472, 3688, 0), new Tile(3471, 3688, 0), new Tile(3471, 3689, 0), new Tile(3471, 3690, 0), new Tile(3471, 3691, 0), new Tile(3471, 3692, 0), new Tile(3471, 3693, 0), new Tile(3471, 3694, 0), new Tile(3471, 3695, 0), new Tile(3471, 3696, 0), new Tile(3471, 3697, 0), new Tile(3470, 3697, 0), new Tile(3469, 3697, 0), new Tile(3469, 3698, 0), new Tile(3468, 3698, 0), new Tile(3467, 3698, 0), new Tile(3467, 3699, 0), new Tile(3466, 3699, 0), new Tile(3465, 3699, 0), new Tile(3464, 3699, 0), new Tile(3463, 3699, 0), new Tile(3462, 3699, 0), new Tile(3461, 3699, 0), new Tile(3460, 3699, 0), new Tile(3459, 3699, 0), new Tile(3458, 3699, 0), new Tile(3457, 3699, 0), new Tile(3456, 3699, 0), new Tile(3455, 3699, 0), new Tile(3454, 3699, 0), new Tile(3453, 3699, 0), new Tile(3452, 3699, 0), new Tile(3451, 3699, 0), new Tile(3451, 3700, 0), new Tile(3450, 3700, 0), new Tile(3450, 3701, 0), new Tile(3450, 3702, 0), new Tile(3450, 3703, 0), new Tile(3450, 3704, 0), new Tile(3450, 3705, 0), new Tile(3450, 3706, 0), new Tile(3450, 3707, 0), new Tile(3450, 3708, 0), new Tile(3450, 3709, 0), new Tile(3449, 3709, 0), new Tile(3449, 3710, 0), new Tile(3449, 3711, 0), new Tile(3449, 3712, 0), new Tile(3449, 3713, 0), new Tile(3449, 3714, 0), new Tile(3449, 3715, 0), new Tile(3449, 3716, 0), new Tile(3449, 3717, 0), new Tile(3449, 3718, 0), new Tile(3449, 3719, 0), new Tile(3449, 3720, 0) );
	
	private static final int dungeoneeringTutorId = 9712;
	private static final int dungeonEntranceId = 48496;
	public static final int ringOfKinshipId = 15707;
	private static final int dungeonExitId = 51156;
	private static final int brokenStairsId = 50552;
	
	private static final String dungeoneeringTutorName = "Dungeoneering tutor";
	
	private static boolean sailTaverleyDaemonheimFerry() {
		if (sailTaverleyDaemonheimFerryLandingTile.distanceTo() < 10)
			return true;
		
		if (tavlerleyLStoDaemonheimFerryPath.getDistanceToNearest() < 25 || Lodestone.TAVERLEY.teleport(true)) {
			// actual walk time is about 15 seconds
			if (tavlerleyLStoDaemonheimFerryPath.randomize(4, 4).loopWalk(30000, null, true)) {
				Time.sleepQuick();
				
				Npc bryllThoksdottir = Npcs.getNearest(null, bryllThoksdottirId);
				if (bryllThoksdottir != null) {
					if (bryllThoksdottir.loopWalkTo(10000)) {
						Time.sleepQuick();
						
						if (bryllThoksdottir.loopInteract(null, "Sail")) {
							Time.sleep(5000); // takes ~7 seconds
							Timer timer = new Timer(5000);
							while (timer.isRunning()) {
								if (sailTaverleyDaemonheimFerryLandingTile.distanceTo() < 10) {
									Time.sleep(2000, 3000);
									return true;
								}
								
								Time.sleep(50);
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	public static boolean isNearDaemonheimEntrance() {
		return daemonheimFerryToDaemonheimEntrancePath.getDistanceToNearest() <= 25;
	}
	
	public static boolean walkToDaemonheimEntrance() {
		if (daemonheimFerryToDaemonheimEntrancePath.getEnd().distanceTo() <= 15)
			return true;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		boolean teleporting = false;
		
		if (daemonheimFerryToDaemonheimEntrancePath.getDistanceToNearest() > 25 && (ItemTable.INVENTORY.containsAny(ringOfKinshipId) || EquipmentSlot.RING.getItemId() == ringOfKinshipId)) {
			if (ItemTable.INVENTORY.containsAny(ringOfKinshipId)) {
				if (Ribbon.Tab.BACKPACK.open(true)) {
					Time.sleepQuick();
					
					Component ring = InventoryWidget.get.getItem(ringOfKinshipId);
					if (ring != null) {
						boolean result = ring.interact("Teleport to Daemonheim");
						Time.sleepQuick();
						
						if (result)
							teleporting = true;
					}
				}
			} else if (EquipmentSlot.RING.getItemId() == ringOfKinshipId) {
				if (Ribbon.Tab.WORN_EQUIPMENT.open(true)) {
					Time.sleepQuick();
					
					if (EquipmentSlot.RING.interact("Teleport to Daemonheim")) {
						Time.sleepQuick();
						teleporting = true;
					}
				}
			}
		} else if (daemonheimFerryToDaemonheimEntrancePath.getDistanceToNearest() <= 25 || sailTaverleyDaemonheimFerry()) {
			// takes about 35 seconds to walk manually
			if (daemonheimFerryToDaemonheimEntrancePath.randomize(4, 4).loopWalk(70000, null, true)) {
				return true;
			}
		}
		
		if (teleporting) {
			if (player.waitForAnimation(Constants.Animation.RING_OF_KINSHIP_TELEPORT_START.getIds(), 5000, true, (Condition[]) null)) {
				Time.sleep(4000); // takes about 7 seconds to teleport
				
				if (player.waitForAnimation(Constants.Animation.RING_OF_KINSHIP_TELEPORT_END.getIds(), 10000, true, (Condition[]) null)) {
					if (player.waitForIdleAnimation(Constants.Animation.RING_OF_KINSHIP_TELEPORT_END.getIds(), 7000, true, (Condition[]) null)) {
						// takes about 10 seconds to walk manually from ring of kinship teleport location
						if (daemonheimFerryToDaemonheimEntrancePath.randomize(4, 4).loopWalk(20000, null, true)) {
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
	
	private static boolean depositAllAndWithdrawRing() {
		if (ItemTable.INVENTORY.countAllItems(false, ringOfKinshipId) == 0
				&& ItemTable.EQUIPMENT.countAllItems(false, ringOfKinshipId) == 0) {
			
			if (ItemTable.INVENTORY.containsAny(ringOfKinshipId) || ItemTable.EQUIPMENT.containsAny(ringOfKinshipId))
				return true;
		}
		
		if (Bank.get.open(true)) {
			Time.sleepQuick();
			
			if (Bank.get.depositAllInvItems(ringOfKinshipId)) {
				Time.sleepQuick();
				
				if (Bank.get.clickDepositWornItemsButton(true)) {
					Time.sleepQuick();
					
					boolean allEmpty = false;
					Timer timer = new Timer(3000);
					while (timer.isRunning()) {
						// TODO: item table count might be a bug
						System.out.println("inv count: " + ItemTable.INVENTORY.countAllItems(false, ringOfKinshipId));
						System.out.println("equip count: " + ItemTable.EQUIPMENT.countAllItems(false));
						if (ItemTable.INVENTORY.countAllItems(false, ringOfKinshipId) == 0
								&& ItemTable.EQUIPMENT.countAllItems(false) == 0) {
							allEmpty = true;
							break;
						}
						
						Time.sleep(50);
					}
					
					if (allEmpty) {
						if (!ItemTable.INVENTORY.containsAny(ringOfKinshipId)) {
							if (ItemTable.BANK.containsAny(ringOfKinshipId)) {
								if (!Bank.get.withdraw(ringOfKinshipId, 1, false))
									return false;
							}
						}
					}
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static boolean equipRing() {
		if (EquipmentSlot.RING.getItemId() == ringOfKinshipId)
			return true;
		
		if (ItemTable.INVENTORY.containsAny(ringOfKinshipId)) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				Component ring = InventoryWidget.get.getItem(ringOfKinshipId);
				if (ring != null)
					return ring.interact("Wear");
			}
		}
		
		return false;
	}
	
	public static boolean enterDungeon() {
		if (isInDungeon())
			return true;
		
		if (!Bank.get.close(true))
			return false;
		
		if (!DungeoneeringFloorSettingsWidget.get.close(true))
			return false;
		
		if (walkToDaemonheimEntrance()) {
			Time.sleepQuick();
			
			if (depositAllAndWithdrawRing()) {
				Time.sleepQuick();
				
				if (Bank.get.close(true)) {
					Time.sleepQuick();
					
					if (equipRing()) {
						Time.sleepQuick();
						
						GroundObject dungeonEntrance = GroundObjects.getNearest(Type.All, dungeonEntranceId);
						if (dungeonEntrance != null) {
							if (dungeonEntrance.loopInteract(null, "Climb-down")) {
								Time.sleepQuick();
								
								// this also accounts for a party that is already created (will skip continue message / chat options to create party)
								boolean interfaceOpened = false;
								Timer timer = new Timer(15000);
								while (timer.isRunning()) {
									if (ContinueMessage.isOpen())
										ContinueMessage.close(true);
									
									if (ChatOptionPane.get.isOpen()) {
										ChatOptionPane.get.chooseYes(true);
										Time.sleepMedium(); // TODO: wait for minigame tab opened
										dungeonEntrance.loopInteract(null, "Climb-down");
										Time.sleepQuick();
									}
									
									if (DungeoneeringFloorSettingsWidget.get.isOpen()) {
										interfaceOpened = true;
										break;
									}
									
									Time.sleep(50);
								}
								
								if (interfaceOpened) {
									Time.sleepQuick();
									
									timer = new Timer(10000);
									while (timer.isRunning()) {
										if (DungeoneeringFloorSettingsWidget.get.isOpen()) {
											DungeoneeringFloorSettingsWidget.get.clickEnterDungeonButton(true);
											Time.sleepQuick();
										}
										
										if (isInDungeon())
											return true;
										
										Time.sleep(50);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return false;
	}
	
	public static boolean isInDungeon() {
		int setting = Client.getSettingData().getSettings().get(2391, 30, 0x1); // or setting 82
		return setting == 1;
	}
	
	public static boolean exitDungeon() {
		if (isInDungeon()) {
			GroundObject dungeonExit = GroundObjects.getNearest(Type.All, dungeonExitId);
			if (dungeonExit != null) {
				if (dungeonExit.loopWalkTo(5000)) {
					Time.sleepQuick();
					
					if (dungeonExit.loopInteract(null, "Climb-up")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(10000);
						while (timer.isRunning()) {
							if (ContinueMessage.isOpen())
								ContinueMessage.close(true);
							
							if (ChatOptionPane.get.isOpen()) {
								ChatOptionPane.get.chooseYes(true);
								Time.sleepQuick();
							}
							
							if (!isInDungeon()) {
								Time.sleepMedium();
								break;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
		}
		
		if (!jumpDownBrokenStairs())
			return false;
		
		return !isInDungeon();
	}
	
	public static boolean jumpDownBrokenStairs() {
		if (!isInDungeon() && Client.getPlane() == 1) {
			GroundObject brokenStairs = GroundObjects.getNearest(Type.All, brokenStairsId);
			if (brokenStairs != null) {
				Condition breakCondition = new Condition() {
					@Override
					public boolean evaluate() {
						if (Client.getPlane() != 1)
							return true;
						
						return false;
					}
				};
				if (brokenStairs.loopWalkTo(5000, breakCondition)) {
					Time.sleepQuick();
					return brokenStairs.loopInteract(null, "Jump-down");
				} else
					return false;
			}
		}
		
		return true;
	}
	
	public static boolean retrieveRingOfKinship() {
		if (ItemTable.INVENTORY.containsAny(ringOfKinshipId) || ItemTable.EQUIPMENT.containsAny(ringOfKinshipId))
			return true;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		if (walkToDaemonheimEntrance()) {
			Time.sleepQuick();
			
			Npc tutor = Npcs.getNearest(null, dungeoneeringTutorId);
			if (tutor != null) {
				if (tutor.loopInteract(null, "Talk-to", dungeoneeringTutorName, false)) {
					Timer timer = new Timer(10000);
					while (timer.isRunning()) {
						if (ItemTable.INVENTORY.containsAny(ringOfKinshipId) || ItemTable.EQUIPMENT.containsAny(ringOfKinshipId))
							break;
						
						if (ChatOptionPane.get.isOpen())
							break;
						
						if (ContinueMessage.isOpen()) {
							ContinueMessage.close(true);
							Time.sleepQuick();
						}
						
						Time.sleepQuick();
					}
					
					if (ChatOptionPane.get.isOpen() || ContinueMessage.isOpen()) {
						// Quick cancel out of chat option pane or continue message
						if (Walking.walk(player.getTile().randomize(3, 3))) {
							
						}
						Time.sleepQuick();
					}
					
					if (ItemTable.INVENTORY.containsAny(ringOfKinshipId) || ItemTable.EQUIPMENT.containsAny(ringOfKinshipId))
						return true;
				}
			}
		}
		
		return false;
	}
}
