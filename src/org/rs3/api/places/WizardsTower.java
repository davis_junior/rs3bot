package org.rs3.api.places;

import org.rs3.api.Camera;
import org.rs3.api.GroundObjects;
import org.rs3.api.Lodestone;
import org.rs3.api.Players;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Player;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class WizardsTower {

	private static final TilePath pathDraynorLStoWizardsTowerBeam = new TilePath( new Tile(3105, 3298, 0), new Tile(3105, 3297, 0), new Tile(3105, 3296, 0), new Tile(3105, 3295, 0), new Tile(3105, 3294, 0), new Tile(3106, 3294, 0), new Tile(3107, 3294, 0), new Tile(3107, 3293, 0), new Tile(3107, 3292, 0), new Tile(3108, 3292, 0), new Tile(3109, 3292, 0), new Tile(3109, 3291, 0), new Tile(3109, 3290, 0), new Tile(3109, 3289, 0), new Tile(3109, 3288, 0), new Tile(3108, 3288, 0), new Tile(3108, 3287, 0), new Tile(3108, 3286, 0), new Tile(3108, 3285, 0), new Tile(3108, 3284, 0), new Tile(3107, 3284, 0), new Tile(3107, 3283, 0), new Tile(3106, 3283, 0), new Tile(3106, 3282, 0), new Tile(3106, 3281, 0), new Tile(3106, 3280, 0), new Tile(3105, 3280, 0), new Tile(3105, 3279, 0), new Tile(3105, 3278, 0), new Tile(3105, 3277, 0), new Tile(3105, 3276, 0), new Tile(3105, 3275, 0), new Tile(3104, 3275, 0), new Tile(3104, 3274, 0), new Tile(3104, 3273, 0), new Tile(3104, 3272, 0), new Tile(3104, 3271, 0), new Tile(3104, 3270, 0), new Tile(3104, 3269, 0), new Tile(3104, 3268, 0), new Tile(3104, 3267, 0), new Tile(3104, 3266, 0), new Tile(3104, 3265, 0), new Tile(3104, 3264, 0), new Tile(3104, 3263, 0), new Tile(3104, 3262, 0), new Tile(3104, 3261, 0), new Tile(3104, 3260, 0), new Tile(3104, 3259, 0), new Tile(3104, 3258, 0), new Tile(3104, 3257, 0), new Tile(3105, 3257, 0), new Tile(3105, 3256, 0), new Tile(3105, 3255, 0), new Tile(3105, 3254, 0), new Tile(3105, 3253, 0), new Tile(3105, 3252, 0), new Tile(3105, 3251, 0), new Tile(3105, 3250, 0), new Tile(3105, 3249, 0), new Tile(3105, 3248, 0), new Tile(3105, 3247, 0), new Tile(3105, 3246, 0), new Tile(3105, 3245, 0), new Tile(3105, 3244, 0), new Tile(3105, 3243, 0), new Tile(3105, 3242, 0), new Tile(3105, 3241, 0), new Tile(3105, 3240, 0), new Tile(3106, 3240, 0), new Tile(3106, 3239, 0), new Tile(3106, 3238, 0), new Tile(3106, 3237, 0), new Tile(3106, 3236, 0), new Tile(3106, 3235, 0), new Tile(3106, 3234, 0), new Tile(3106, 3233, 0), new Tile(3106, 3232, 0), new Tile(3106, 3231, 0), new Tile(3107, 3231, 0), new Tile(3107, 3230, 0), new Tile(3107, 3229, 0), new Tile(3107, 3228, 0), new Tile(3107, 3227, 0), new Tile(3107, 3226, 0), new Tile(3107, 3225, 0), new Tile(3107, 3224, 0), new Tile(3107, 3223, 0), new Tile(3106, 3223, 0), new Tile(3106, 3222, 0), new Tile(3106, 3221, 0), new Tile(3105, 3221, 0), new Tile(3105, 3220, 0), new Tile(3105, 3219, 0), new Tile(3105, 3218, 0), new Tile(3104, 3218, 0), new Tile(3103, 3218, 0), new Tile(3103, 3217, 0), new Tile(3103, 3216, 0), new Tile(3103, 3215, 0), new Tile(3103, 3214, 0), new Tile(3103, 3213, 0), new Tile(3103, 3212, 0), new Tile(3102, 3212, 0), new Tile(3102, 3211, 0), new Tile(3102, 3210, 0), new Tile(3102, 3209, 0), new Tile(3102, 3208, 0), new Tile(3102, 3207, 0), new Tile(3102, 3206, 0), new Tile(3102, 3205, 0), new Tile(3102, 3204, 0), new Tile(3102, 3203, 0), new Tile(3102, 3202, 0), new Tile(3102, 3201, 0), new Tile(3102, 3200, 0), new Tile(3102, 3199, 0), new Tile(3102, 3198, 0), new Tile(3102, 3197, 0), new Tile(3102, 3196, 0), new Tile(3102, 3195, 0), new Tile(3102, 3194, 0), new Tile(3102, 3193, 0), new Tile(3103, 3193, 0), new Tile(3103, 3192, 0), new Tile(3103, 3191, 0), new Tile(3103, 3190, 0), new Tile(3103, 3189, 0), new Tile(3103, 3188, 0), new Tile(3103, 3187, 0), new Tile(3103, 3186, 0), new Tile(3103, 3185, 0), new Tile(3103, 3184, 0), new Tile(3103, 3183, 0), new Tile(3103, 3182, 0), new Tile(3103, 3181, 0), new Tile(3103, 3180, 0), new Tile(3103, 3179, 0), new Tile(3103, 3178, 0), new Tile(3103, 3177, 0), new Tile(3103, 3176, 0), new Tile(3103, 3175, 0), new Tile(3103, 3174, 0), new Tile(3103, 3173, 0), new Tile(3103, 3172, 0), new Tile(3103, 3171, 0), new Tile(3103, 3170, 0), new Tile(3103, 3169, 0), new Tile(3103, 3168, 0), new Tile(3103, 3167, 0), new Tile(3103, 3166, 0), new Tile(3103, 3165, 0), new Tile(3103, 3164, 0), new Tile(3103, 3163, 0), new Tile(3102, 3163, 0), new Tile(3102, 3162, 0), new Tile(3102, 3161, 0), new Tile(3102, 3160, 0), new Tile(3102, 3159, 0) );
	private static final Area wizardsTowerArea = new Area( new Tile(3091, 3160, 0), new Tile(3091, 3161, 0), new Tile(3092, 3161, 0), new Tile(3092, 3162, 0), new Tile(3093, 3162, 0), new Tile(3093, 3163, 0), new Tile(3094, 3163, 0), new Tile(3094, 3164, 0), new Tile(3095, 3164, 0), new Tile(3095, 3165, 0), new Tile(3096, 3165, 0), new Tile(3096, 3166, 0), new Tile(3097, 3166, 0), new Tile(3097, 3167, 0), new Tile(3098, 3167, 0), new Tile(3099, 3167, 0), new Tile(3100, 3167, 0), new Tile(3100, 3168, 0), new Tile(3100, 3169, 0), new Tile(3100, 3170, 0), new Tile(3100, 3171, 0), new Tile(3100, 3172, 0), new Tile(3100, 3173, 0), new Tile(3101, 3173, 0), new Tile(3102, 3173, 0), new Tile(3103, 3173, 0), new Tile(3104, 3173, 0), new Tile(3105, 3173, 0), new Tile(3105, 3172, 0), new Tile(3105, 3171, 0), new Tile(3105, 3170, 0), new Tile(3105, 3169, 0), new Tile(3105, 3168, 0), new Tile(3105, 3167, 0), new Tile(3106, 3167, 0), new Tile(3107, 3167, 0), new Tile(3108, 3167, 0), new Tile(3108, 3166, 0), new Tile(3109, 3166, 0), new Tile(3109, 3165, 0), new Tile(3110, 3165, 0), new Tile(3110, 3164, 0), new Tile(3111, 3164, 0), new Tile(3111, 3163, 0), new Tile(3112, 3163, 0), new Tile(3112, 3162, 0), new Tile(3113, 3162, 0), new Tile(3113, 3161, 0), new Tile(3114, 3161, 0), new Tile(3114, 3160, 0), new Tile(3114, 3159, 0), new Tile(3114, 3158, 0), new Tile(3114, 3157, 0), new Tile(3114, 3156, 0), new Tile(3114, 3155, 0), new Tile(3114, 3154, 0), new Tile(3114, 3153, 0), new Tile(3114, 3152, 0), new Tile(3114, 3151, 0), new Tile(3114, 3150, 0), new Tile(3113, 3150, 0), new Tile(3113, 3149, 0), new Tile(3112, 3149, 0), new Tile(3112, 3148, 0), new Tile(3111, 3148, 0), new Tile(3111, 3147, 0), new Tile(3110, 3147, 0), new Tile(3110, 3146, 0), new Tile(3109, 3146, 0), new Tile(3109, 3145, 0), new Tile(3108, 3145, 0), new Tile(3108, 3144, 0), new Tile(3107, 3144, 0), new Tile(3106, 3144, 0), new Tile(3105, 3144, 0), new Tile(3105, 3143, 0), new Tile(3105, 3142, 0), new Tile(3105, 3141, 0), new Tile(3105, 3140, 0), new Tile(3105, 3139, 0), new Tile(3105, 3138, 0), new Tile(3104, 3138, 0), new Tile(3103, 3138, 0), new Tile(3102, 3138, 0), new Tile(3101, 3138, 0), new Tile(3100, 3138, 0), new Tile(3100, 3139, 0), new Tile(3100, 3140, 0), new Tile(3100, 3141, 0), new Tile(3100, 3142, 0), new Tile(3100, 3143, 0), new Tile(3100, 3144, 0), new Tile(3099, 3144, 0), new Tile(3098, 3144, 0), new Tile(3097, 3144, 0), new Tile(3097, 3145, 0), new Tile(3096, 3145, 0), new Tile(3096, 3146, 0), new Tile(3095, 3146, 0), new Tile(3095, 3147, 0), new Tile(3094, 3147, 0), new Tile(3094, 3148, 0), new Tile(3093, 3148, 0), new Tile(3093, 3149, 0), new Tile(3092, 3149, 0), new Tile(3092, 3150, 0), new Tile(3091, 3150, 0), new Tile(3091, 3151, 0), new Tile(3091, 3152, 0), new Tile(3091, 3153, 0), new Tile(3091, 3154, 0), new Tile(3091, 3155, 0), new Tile(3091, 3156, 0), new Tile(3091, 3157, 0), new Tile(3091, 3158, 0), new Tile(3091, 3159, 0) );
	
	private static final int[] beamIds = new int[] { 80528, 80551 }; // use tile, name is null, model is null TODO: add other 2 ids that are on same tile
	private static final int runecraftingGuildPortalId = 79518; // Runecrafting Guild portal
	private static final int lowLevelRunespanPortalId = 79519; // Low-level Runespan portal
	private static final int midLevelRunespanPortalId = 79520; // Mid-level Runespan portal
	
	public static enum WizardsTowerFloorLevel {
		FIRST(0),
		SECOND(1),
		THIRD(2),
		FOURTH(3),
		;
		
		int plane;
		
		private WizardsTowerFloorLevel(int plane) {
			this.plane = plane;
		}
		
		public boolean isCurrent() {
			Player player = Players.getMyPlayer();
			if (player == null)
				return false;
			
			if (wizardsTowerArea.contains(false, player.getTile())) {
				return Client.getPlane() == this.plane;
			}
			
			return false;
		}
		
		public boolean walkTo() {
			if (isCurrent())
				return true;
			
			if (walkToWizardsTower()) {
				Time.sleepQuick();
				
				if (isCurrent())
					return true;
				
				Timer timer = new Timer(30000);
				while (timer.isRunning()) {
					if (isCurrent())
						return true;
					
					GroundObject beam = GroundObjects.getNearest(Type.All, beamIds);
					if (beam != null) {
						Tile tile = beam.getTile();
						if (!tile.isOnScreen()) {
							Camera.turnTo(tile);
							Time.sleepQuick();
						}
						
						String action;
						if (Client.getPlane() < this.plane)
							action = "Ascend";
						else
							action = "Descend";
						
						if (tile.loopInteract(null, action))
							Time.sleep(3000, 4000);
					}
					
					Time.sleep(50);
				}
			}
			
			return false;
		}
		
		
		public static WizardsTowerFloorLevel getCurrent() {
			for (WizardsTowerFloorLevel floor : values()) {
				if (floor.isCurrent())
					return floor;
			}
			
			return null;
		}
	}
	
	public static boolean walkToWizardsTower() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		if (wizardsTowerArea.contains(false, player.getTile()))
			return true;
		
		if (pathDraynorLStoWizardsTowerBeam.getDistanceToNearest() < 25 || Lodestone.DRAYNOR_VILLAGE.teleport(true)) {
			// actual walk time is about 45 seconds
			if (pathDraynorLStoWizardsTowerBeam.randomize(4, 4).loopWalk(90000, null, true))
				return true;
		}
		
		return false;
	}
	
	public static boolean enterRunecraftingGuildPortal() {
		if (WizardsTowerFloorLevel.FOURTH.walkTo()) {
			Time.sleepQuick();
			
			GroundObject portal = GroundObjects.getNearest(Type.All, runecraftingGuildPortalId);
			if (portal != null) {
				Tile tile = portal.getTile();
				if (!tile.isOnScreen()) {
					Camera.turnTo(tile);
					Time.sleepQuick();
				}
				
				return portal.loopInteract(null, "Enter", "Runecrafting Guild portal", false);
			}
		}
		
		return false;
	}
	
	public static boolean enterLowLevelRunespanPortal() {
		if (WizardsTowerFloorLevel.FOURTH.walkTo()) {
			Time.sleepQuick();
			
			GroundObject portal = GroundObjects.getNearest(Type.All, lowLevelRunespanPortalId);
			if (portal != null) {
				Tile tile = portal.getTile();
				if (!tile.isOnScreen()) {
					Camera.turnTo(tile);
					Time.sleepQuick();
				}
				
				return portal.loopInteract(null, "Enter", "Low-level Runespan portal", false);
			}
		}
		
		return false;
	}
	
	public static boolean enterMidLevelRunespanPortal() {
		if (WizardsTowerFloorLevel.FOURTH.walkTo()) {
			Time.sleepQuick();
			
			GroundObject portal = GroundObjects.getNearest(Type.All, midLevelRunespanPortalId);
			if (portal != null) {
				Tile tile = portal.getTile();
				if (!tile.isOnScreen()) {
					Camera.turnTo(tile);
					Time.sleepQuick();
				}
				
				return portal.loopInteract(null, "Enter", "Mid-level Runespan portal", false);
			}
		}
		
		return false;
	}
}
