package org.rs3.api;

import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.CustomizableActionBarWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.wrappers.Client;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class ActionBar {
	
	private static final ActionSlot[] actionBar1slots = new ActionSlot[] { ActionSlot.BAR1_SLOT1, ActionSlot.BAR1_SLOT2, ActionSlot.BAR1_SLOT3, ActionSlot.BAR1_SLOT4, ActionSlot.BAR1_SLOT5, ActionSlot.BAR1_SLOT6, ActionSlot.BAR1_SLOT7, ActionSlot.BAR1_SLOT8, ActionSlot.BAR1_SLOT9, ActionSlot.BAR1_SLOT10, ActionSlot.BAR1_SLOT11, ActionSlot.BAR1_SLOT12, ActionSlot.BAR1_SLOT13, ActionSlot.BAR1_SLOT14 };
	private static final ActionSlot[] actionBar2slots = new ActionSlot[] { ActionSlot.BAR2_SLOT1, ActionSlot.BAR2_SLOT2, ActionSlot.BAR2_SLOT3, ActionSlot.BAR2_SLOT4, ActionSlot.BAR2_SLOT5, ActionSlot.BAR2_SLOT6, ActionSlot.BAR2_SLOT7, ActionSlot.BAR2_SLOT8, ActionSlot.BAR2_SLOT9, ActionSlot.BAR2_SLOT10, ActionSlot.BAR2_SLOT11, ActionSlot.BAR2_SLOT12, ActionSlot.BAR2_SLOT13, ActionSlot.BAR2_SLOT14 };
	private static final ActionSlot[] actionBar3slots = new ActionSlot[] { ActionSlot.BAR3_SLOT1, ActionSlot.BAR3_SLOT2, ActionSlot.BAR3_SLOT3, ActionSlot.BAR3_SLOT4, ActionSlot.BAR3_SLOT5, ActionSlot.BAR3_SLOT6, ActionSlot.BAR3_SLOT7, ActionSlot.BAR3_SLOT8, ActionSlot.BAR3_SLOT9, ActionSlot.BAR3_SLOT10, ActionSlot.BAR3_SLOT11, ActionSlot.BAR3_SLOT12, ActionSlot.BAR3_SLOT13, ActionSlot.BAR3_SLOT14 };
	
	/**
	 * aka main tab
	 */
	public static enum AbilityActionType {
		MELEE, RANGED, MAGIC, DEFENSIVE, PRAYERS
	}
	
	/**
	 * aka sub tab of main tab (note magic has a few more sub tabs)
	 */
	public static enum AbilitySkillType {
		
		ATTACK(AbilityActionType.MELEE, Skill.ATTACK),
		STRENGTH(AbilityActionType.MELEE, Skill.STRENGTH),
		RANGED(AbilityActionType.RANGED, Skill.RANGED),
		MAGIC(AbilityActionType.MAGIC, Skill.MAGIC),
		DEFENCE(AbilityActionType.DEFENSIVE, Skill.DEFENSE),
		CONSTITUTION(AbilityActionType.DEFENSIVE, Skill.CONSTITUTION),
		PRAYER(AbilityActionType.PRAYERS, Skill.PRAYER),
		;
		
		AbilityActionType actionType;
		Skill skill;
		
		private AbilitySkillType(AbilityActionType actionType, Skill skill) {
			this.actionType = actionType;
			this.skill = skill;
		}
		
		public AbilityActionType getActionType() {
			return actionType;
		}
		
		public Skill getSkill() {
			return skill;
		}
	}
	
	public static enum AbilityType {
		BASIC, THRESHOLD, ULTIMATE, SPELL, PRAYER, ITEM
	}
	
	public static enum AbilityRequirement {
		NONE, TWO_HAND, DUAL_WIELD, SHIELD, THE_WORLD_WAKES
	}
	
	// should return 1, 2, or 3 (up to 10 for members)
	public static int getCurrentActionBar() {
		int currentActionBar = Client.getSettingData().getSettings().get(682, 5, 0x3); // TODO: mask for members might be 0xF, there are 10 action bars
		return currentActionBar;
	}
	
	/**
	 * Waits after
	 * 
	 * @param barIndex
	 * @return
	 */
	public static boolean setActionBar(int barIndex) {
		if (getCurrentActionBar() == barIndex)
			return true;
		
		Timer timer = new Timer(20000);
		Timer barChangeTimer = new Timer(3000);
		while (timer.isRunning()) {
			boolean waitForBarChange = false;
			
			int curBarIndex = getCurrentActionBar();
			if (curBarIndex == barIndex)
				return true;
			else if (curBarIndex - barIndex < 0) {
				if (CustomizableActionBarWidget.get.isOpen())
					CustomizableActionBarWidget.get.clickNext(true);
				else if (ActionBarWidget.get.isOpen())
					ActionBarWidget.get.clickNext(true);
				
				Time.sleepMedium();
				waitForBarChange = true;
			} else {
				if (CustomizableActionBarWidget.get.isOpen())
					CustomizableActionBarWidget.get.clickPrevious(true);
				else if (ActionBarWidget.get.isOpen())
					ActionBarWidget.get.clickPrevious(true);
				
				Time.sleepMedium();
				waitForBarChange = true;
			}
			
			if (waitForBarChange) {
				barChangeTimer.reset();
				while (barChangeTimer.isRunning()) {
					if (getCurrentActionBar() != curBarIndex) {
						Time.sleepQuickest();
						break;
					}
					
					Time.sleep(50);
				}
			}
			
			Time.sleep(50);
		}
		
		return false;
	}
	
	public static ActionSlot[] getSlots(int barIndex) {
		switch (barIndex) {
		case 1:
			return actionBar1slots;
		case 2:
			return actionBar2slots;
		case 3:
			return actionBar3slots;
		default:
			System.out.println("ActionBar.getSlots(...) - action bar unknown!");
		}
		
		return null;
	}
	
	/**
	 * Be careful with indices on this array, 0 refers to slot 1, 13 refers to 14, etc.
	 * 
	 * @return Current action bar slots
	 */
	public static ActionSlot[] getCurrentSlots() {
		return getSlots(getCurrentActionBar());
	}
	
	/**
	 * Waits after
	 * 
	 * @param abilities
	 * @return
	 */
	public static boolean setup(Ability[] abilities) {
		return setup(getCurrentActionBar(), abilities);
	}
	
	/**
	 * Waits after
	 * 
	 * @param barIndex
	 * @param abilities
	 * @return
	 */
	public static boolean setup(int barIndex, Ability[] abilities) {
		ActionSlot[] slots = getSlots(barIndex);
		
		// check if all abilities are already set
		boolean abilitiesAlreadySet = true;
		for (int i = 0; i < abilities.length; i++) {
			if (!abilities[i].equals(slots[i].getAbility())) {
				abilitiesAlreadySet = false;
				break;
			}
		}
		if (abilitiesAlreadySet)
			return true;
		
		// check if we should clear all
		int slotsToClear = 0;
		for (int i = 0; i < abilities.length; i++) {
			if (abilities[i].equals(Ability.EMPTY) && !slots[i].getAbility().equals(Ability.EMPTY))
				slotsToClear++;
		}
		
		if (slotsToClear > 3) {
			if (!ActionBar.setActionBar(barIndex))
				return false;
			
			if (!clearAll(true))
				return false;
		} else if (slotsToClear > 0) { // clear slots than need cleared
			// close subinterface to make more room for removing abilities from main action bar
			if (MainWidget.get.isSubInterfaceOpen()) {
				if (!MainWidget.get.closeSubInterface(true))
					return false;
				Time.sleepQuick();
			}
			
			if (!ActionBar.setActionBar(barIndex))
				return false;
			
			if (ActionBarWidget.get.unlockActionBar(true)) {
				for (int i = 0; i < abilities.length; i++) {
					if (abilities[i].equals(Ability.EMPTY) && !slots[i].getAbility().equals(Ability.EMPTY)) {
						Timer timer = new Timer(7000);
						while (timer.isRunning()) {
							if (slots[i].setAbility(Ability.EMPTY, true)) {
								Time.sleepQuick();
								break;
							}
							
							Time.sleepQuick();
							Time.sleep(50);
						}
					}
				}
			}
		}
		
		// ActionSlot.setAbility() takes care of empty slots
		for (int i = 0; i < abilities.length; i++) {
			Timer timer = new Timer(15000);
			while (timer.isRunning()) {
				if (slots[i].setAbility(abilities[i], true)) {
					Time.sleepQuick();
					break;
				}
				
				Time.sleepQuick();
				Time.sleep(50);
			}
		}
		
		Time.sleepQuick();
		
		// verify
		Timer timer = new Timer(5000);
		while (timer.isRunning()) {
			boolean verified = true;
			for (int i = 0; i < abilities.length; i++) {
				if (!abilities[i].equals(slots[i].getAbility()))
					verified = false;
			}
			if (verified)
				return true;
			
			Time.sleep(500);
		}
		
		return false;
	}
	
	public static boolean clearAll() {
		return clearAll(false);
	}
	
	public static boolean clearAll(boolean waitAfter) {
		if (CustomizableActionBarWidget.get.isOpen())
			return CustomizableActionBarWidget.get.clearAll(waitAfter);
		else if (ActionBarWidget.get.isOpen())
			return ActionBarWidget.get.interactCombatSettingsButton("Clear Action bar", waitAfter);
		
		return false;
	}
	
	public static boolean isAbilityQueued() {
		int abilityQueued = Client.getSettingData().getSettings().get(5861, 0x3FF);
		return abilityQueued == 1003;
	}
	
	public static int getCurrentQueuedActionSlot() {
		int currentQueuedActionSlot = Client.getSettingData().getSettings().get(4164, 0xF);
		return currentQueuedActionSlot;
	}
}
