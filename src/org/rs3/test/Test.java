package org.rs3.test;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.media.opengl.GL2;

import org.apache.commons.io.FileUtils;
import org.rs3.api.input.realmouse.Block;
import org.rs3.api.input.realmouse.MouseData;
import org.rs3.database.AccountInfo;
import org.rs3.ftp.Ftp;
import org.rs3.scripts.parameters.RunTimeParam;


public class Test {

	public Random chicken = null;
	
	protected static int i = 0;
	
	public int abc() {
		return i;
	}
	
	public static void test(Object[][] o, Object o2, int i, int i2, Object o3, byte b, double d, double d2) {
		//new Integer(i);
		//new Byte(b);
		//o.getClass();
		//double d = 3252359925252353252.234242424242343;
		//Object o5 = o2;
	}
	
	public static void accessibleTest() {
		String string = "test";
		
		try {
			Field field = string.getClass().getDeclaredField("value");
			System.out.println("-Field Accessible Test-");
			System.out.println("Original value: " + field.isAccessible());
			
			System.out.println("Setting accessible to 'true'...");
			field.setAccessible(true);
			System.out.println("Value: " + field.isAccessible());
			
			System.out.println("Acquiring new Field object...");
			field = string.getClass().getDeclaredField("value");
			System.out.println("Value: " + field.isAccessible());
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public static void funny() throws InterruptedException {	
		char[] seed = new char[] { 'K', ' ', 'C', 'O', 'U', 'F' };
		System.out.println("Generating password...");
		
		for (int i = 0; i < 1000*1000; i++) {
			String key1 = new String(new char[] { seed[5], seed[4], seed[2], seed[0], seed[1], seed[3], seed[5], seed[5] });
			String key2 = new String(new char[] { seed[5], seed[4], seed[2], seed[0], seed[1], seed[3], seed[5], seed[5], seed[1], seed[4], seed[1], seed[2], seed[3], seed[2], seed[0] });
			
			Random rand = new Random();
			int num = rand.nextInt(3);
			switch (num) {
			case 0:
				System.out.println(key1);
				break;
			case 1:
				System.out.println(key2);
				break;
			case 2:
				char[] key = rand.nextBoolean() ? key1.toCharArray() : key2.toCharArray();
				System.out.println(seed[1]);
				for (char c : key) {
					System.out.print(c);
					Thread.sleep(50);
					System.out.print("...");
					Thread.sleep(50);
					System.out.print("...");
					Thread.sleep(50);
					System.out.println("...");
					Thread.sleep(100);
				}
				System.out.println(seed[1]);
				break;
			}
		}
	}
	
	private static int adjustPriceFivePercent(int price, boolean increase) {
		if (increase)
			return (int) Math.floor(price*(1 + 0.05));
		else
			return (int) Math.floor(price*(1 - 0.05));
	}
	
	private static int adjustPrice(int price, int priceFivePercentIncrements) {
		int newPrice = price;
		boolean increase = priceFivePercentIncrements > 0;
		for (int i = 0; i < Math.abs(priceFivePercentIncrements); i++) {
			newPrice = adjustPriceFivePercent(newPrice, increase);
		}
		
		return newPrice;
	}
	
	public static void main(String[] args) throws InterruptedException, IOException {
		
		
		
		Ftp client = new Ftp();
		client.login();
		
		//File destFile = new File(System.getProperty("user.home") + "\\Desktop\\FileUpdaterUpdater.jar");
		//client.downloadSingleFile("/updates/FileUpdaterUpdater.jar", destFile.getAbsolutePath());
		
		//File destFile = new File(System.getProperty("user.home") + "\\Desktop\\");
		//client.downloadDirectory("/updates", "", destFile.getAbsolutePath());
		
		//File runescapeCacheDir = new File(System.getProperty("user.home") + "\\jagexcache\\runescape");
		//client.downloadDirectory("/updates/runescape/", runescapeCacheDir.getAbsolutePath());
		
	    //client.downloadDirectory("/updates/workspace/RS3Bot/bin", System.getProperty("user.home") + "\\workspace\\RS3Bot123\\bin\\");
		//client.downloadDirectory("/updates/workspace/RS3Bot/extracted/RS3Bot_lib", System.getProperty("user.home") + "\\workspace\\RS3Bot123\\extracted\\RS3Bot_lib\\");
		
		client.logout();
		
		/*File runescapeCacheDir = new File(System.getProperty("user.home") + "\\jagexcache\\runescape");
		File networkShare = new File("\\\\PC-DAVE\\updates\\runescape");
		try {
			FileUtils.copyDirectory(networkShare, runescapeCacheDir);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		// test mouse records for excessive wait time
		/*Map<String, List<MouseData>> list = MouseData.getMouseData();
		for (String key : list.keySet()) {
			for (MouseData md : list.get(key)) {
				if (md.getTime() > 500) { 
					System.out.println(key + " - " + md.getTime());
				}
			}
		}*/
		
		/*for (int x = 0; x < 1100; x++) {
			for (int y = 0; y < 800; y++) {
				boolean success = false;
				for (Block block : Block.values()) {
					if (block.getBounds().contains(x, y)) {
						success = true;
						break;
					}
				}
				
				if (!success) {
					System.out.println("Error: " + x + ", " + y);
				}
			}
		}*/
		
		//Rectangle rect = new Rectangle(0, 0, 10, 10);
		//System.out.println(rect.contains(new Point(10, 10)));
		
		//System.out.println(Integer.MAX_VALUE + 3);
		
		//System.out.println(adjustPrice(315, -10));
		
		//System.out.println(Math.round(315*1.1));
		//System.out.println(Math.floor(315 - 315*0.1));
		//System.out.println(Math.round(315 + 315*0.05));
		
		/*Runtime rt = Runtime.getRuntime();
		try {
			Process p = rt.exec("taskkill /F /IM winamp.exe");
			System.out.println(p.exitValue());
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		/*String dateStr = "Jul 27, 2011 12:00:00 AM";
		DateFormat readFormat = new SimpleDateFormat( "MMM dd, yyyy hh:mm:ss aa");
		DateFormat writeFormat = new SimpleDateFormat("HH:mm");
		Date date = null;
		try
		{
		    date = readFormat.parse( dateStr );
		}
		catch ( ParseException e )
		{
		        e.printStackTrace();
		}
		if( date != null )
		{
		    String formattedDate = writeFormat.format(date);
		    System.out.println(formattedDate);
		}*/
		
		//Date date = new Date();
		//System.out.println(writeFormat.format(date));
		
		/*try {
			Class.forName("org.rs3.test.AbstractTest").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int priceEach = 2000000000;
		String input;
		if (priceEach >= 1000000 && priceEach % 1000000 == 0)
			input = Integer.toString(priceEach / 1000000) + "m";
		else if (priceEach % 1000 == 0)
			input = Integer.toString(priceEach / 1000) + "k";
		else
			input = Integer.toString(priceEach);
		
		System.out.println(input);*/
		//funny();
		
		//accessibleTest();
		
		//39059527; // Index: 596, 71
		
		//final int x = id >> 16;
		//final int y = id & 0xffff;
		
		//int result = 596 << 16 | 71;
		
		//System.out.println(result);
		
		
		/*List<String> aList = new ArrayList<String>();
		
		aList.add("abc");
		aList.add("xyz");
		aList.add("123");
		
		ListIterator<String> it = aList.listIterator();
		
		while (it.hasNext()) {
			String cur = it.next();
			it.set("qqq");
		}
		
		for (String str : aList.toArray(new String[0])) {
			System.out.println(str);
		}*/
	}
}
