package org.rs3.test;

import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.rs3.database.AccountRow;
import org.rs3.database.Database;
import org.rs3.database.EmailValidationRow;
import org.rs3.database.ServerRow;

import proxy.ProxyAuthenticator;

public class AccountValidator {

	public static void main(String[] args) throws IOException {
		System.setProperty("https.protocols", "TLSv1.1,TLSv1.2");
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession arg1) {
				return true;
			}
		});
		
		List<EmailValidationRow> emailList = Database.select(EmailValidationRow.class, null);
		List<ServerRow> serverList = Database.select(ServerRow.class, null);
		
		if (emailList == null) {
			System.out.println("ERROR: unable to get email validation rows!");
			System.exit(1);
		}
		
		if (serverList == null) {
			System.out.println("ERROR: unable to get server rows!");
			System.exit(1);
		}
		
		if (emailList != null && serverList != null) {
			for (EmailValidationRow emailRow : emailList) {
				String link = emailRow.getLink();
				if (link != null && !link.trim().isEmpty()) {
					// find server row
					ServerRow serverRow = null;
					for (ServerRow sr : serverList) {
						String accounts = sr.getAccounts();
						if (accounts != null && !accounts.trim().isEmpty()) {
							String[] accountsSplit = accounts.split(",");
							if (accountsSplit != null) {
								for (String account : accountsSplit) {
									if (account.equals(Integer.toString(emailRow.getAccountId()))) {
										serverRow = sr;
										break;
									}
								}
								
								if (serverRow != null)
									break;
							}
						}
					}
					
					if (serverRow != null) {
						Proxy proxy = new Proxy(java.net.Proxy.Type.SOCKS, new InetSocketAddress(serverRow.getProxyHost(), serverRow.getProxyPort()));
						Authenticator.setDefault(new ProxyAuthenticator(serverRow.getProxyUsername(), serverRow.getProxyPassword()));
						
						URL url = new URL(emailRow.getLink());
						URLConnection con = url.openConnection(proxy);
						con.setConnectTimeout(10000);
						con.setReadTimeout(10000);
						con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0");
						con.getInputStream().close();
						
						emailRow.setLink(null);
						emailRow.executeUpdate();
						
						System.out.println("Validated account: " + emailRow.getAccountId() + " [" + serverRow.getProxyHost() + "]");
					} else
						System.out.println("ERROR: Unable to identify server row: account id: " + emailRow.getAccountId());
				}
			}
		}
	}
	
	/*public static void manualValidate() throws IOException {
		System.setProperty("https.protocols", "TLSv1.1,TLSv1.2");
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession arg1) {
				return true;
			}
		});
		
		Proxy proxy = new Proxy(java.net.Proxy.Type.SOCKS, new InetSocketAddress("104.148.11.203", 61336));
		Authenticator.setDefault(new ProxyAuthenticator("davisjunior11", "E25lEePY"));
		
		URL url = new URL("https://secure.runescape.com/m=email-register/submit_code.ws?address=corazoninoueuvhn%40yahoo.com&code=LX1PtJJOmwTbO1G3H6K4hzJqSRHu7hY*p8ePlw");
		URLConnection con = url.openConnection(proxy);
		con.setConnectTimeout(5000);
		con.setReadTimeout(5000);
		con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0");
		con.getInputStream().close();
		System.out.println("Ok");
	}*/
}
