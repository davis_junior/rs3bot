package org.rs3.debug;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.image.VolatileImage;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.glu.GLU;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.GroundItems;
import org.rs3.api.Menu;
import org.rs3.api.Npcs;
import org.rs3.api.GroundObjects;
import org.rs3.api.Players;
import org.rs3.api.input.Mouse;
import org.rs3.api.input.realmouse.MouseRecorder;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.LobbyScreen;
import org.rs3.api.interfaces.widgets.WornEquipmentWidget;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.AnimatedObject;
import org.rs3.api.wrappers.CameraLocationData;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.ItemDefinition;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.NpcDefinition;
import org.rs3.api.wrappers.ObjectDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.PlayerDef;
import org.rs3.api.wrappers.Quaternion;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.boot.BootType;
import org.rs3.boot.MultiClassLoader;
import org.rs3.boot.OfficialClientInject;
import org.rs3.callbacks.ViewportCallback;
import org.rs3.callbacks.capture.ModelCapture;
import org.rs3.database.ServerInfo;
import org.rs3.eventqueue.EventNazi;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import sun.java2d.SurfaceData;
import sun.java2d.loops.Blit;
import sun.java2d.loops.CompositeType;
import sun.java2d.loops.SurfaceType;


public class Paint {
	public static Graphics rsg = null;

	public static Graphics getGraphicsObject() {
		return rsg;
	}

	public static void setGraphicsObject(Graphics g) {
		Paint.rsg = g;
	}


	public enum DebugObject {
		NPCS(true),
		GROUND_OBJECTS(false),
		GROUND_ITEMS(true),
		INVENTORY(true),
		BANK(true),
		WORN_EQUIPMENT(true),
		;

		private boolean paint;
		private static boolean skipNullNames = true;

		private DebugObject(boolean defualtPaint) {
			this.paint = defualtPaint;
		}

		public boolean shouldPaint() {
			return paint;
		}

		public void setPaint(boolean paint) {
			this.paint = paint;
		}
		
		public static boolean skipNullNames() {
			return skipNullNames;
		}
		
		public static void setSkipNullNames(boolean skipNullNames) {
			DebugObject.skipNullNames = skipNullNames;
		}
	}

	public enum DebugString {
		CurrentTime(true, "Current Time"),
		AccountInfo(true, "Account Info"),
		ScriptElapsed(false, "Script Elapsed"),
		ScriptInfo(false, "Script Info"),
		PlayerInfo(true, "Player Info"),
		Camera(false, "Camera"),
		CameraLocationData(false, "Camera Location Data"),
		Viewport(false, "Viewport"),
		DebugViewports(false, "Debug Viewports"),
		Menu(false, "Menu"),
		;
		
		private String name;
		private boolean paint = false;
		
		private DebugString(boolean defaultPaint, String name) {
			this.paint = defaultPaint;
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
		
		public String getString() {
			switch(this) {
			case CurrentTime:
				StringBuilder timeSb = new StringBuilder(getName() + ": ");
				timeSb.append(Time.format(System.currentTimeMillis()));
				return timeSb.toString();
			case AccountInfo:
				StringBuilder accountInfoSb = new StringBuilder("-" + getName() + "-");
				org.rs3.database.AccountInfo cur = org.rs3.database.AccountInfo.cur;
				if (cur != null) {
					accountInfoSb.append(System.lineSeparator());
					accountInfoSb.append("Account id: " + cur.getId());
				}
				if (ServerInfo.cur != null) {
					accountInfoSb.append(System.lineSeparator());
					accountInfoSb.append("Server id: " + ServerInfo.cur.getServerId());
				}
				return accountInfoSb.toString();
			case PlayerInfo:
				StringBuilder infoSb = new StringBuilder("-" + getName() + "-");
				
				Player player = Players.getMyPlayer();
				if (player != null) {
					Tile tile = player.getTile();
					if (tile != null) {
						infoSb.append(System.lineSeparator());
						infoSb.append("Tile: " + tile.getX() + ", " + tile.getY() + ", " + tile.getPlane());
					}
					infoSb.append(System.lineSeparator());
					infoSb.append("Animation Id: " + player.getAnimationId());
					
					org.rs3.api.wrappers.Character interacting = player.getInteracting();
					if (interacting != null) {
						if (interacting instanceof Npc) {
							NpcDefinition def = ((Npc) interacting).getNpcDefinition();
							if (def != null) {
								infoSb.append(System.lineSeparator());
								infoSb.append("Interacting: (Npc) " + def.getName() + ", " + def.getId());
							}
						} else if (interacting instanceof Player) {
							infoSb.append(System.lineSeparator());
							infoSb.append("Interacting: (Player) " + ((Player)interacting).getName());
						}
					}
					
					infoSb.append(System.lineSeparator());
				}
				return infoSb.toString();
			case Camera:
				StringBuilder cameraSb = new StringBuilder("-" + getName() + "-");
				cameraSb.append(System.lineSeparator());
				cameraSb.append("X: " + org.rs3.api.Camera.getX());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Y: " + org.rs3.api.Camera.getY());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Z: " + org.rs3.api.Camera.getZ());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Pitch: " + org.rs3.api.Camera.getPitch());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Yaw: " + org.rs3.api.Camera.getYaw());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Zoom: " + org.rs3.api.Camera.getZoomPercent() + "%");
				cameraSb.append(System.lineSeparator());
				return cameraSb.toString();
			case CameraLocationData:
				CameraLocationData cameraLocData = Client.getCamera().getCameraLocationData();
				Location point1 = cameraLocData.getPoint1();
				Location point2 = cameraLocData.getPoint2();
				Quaternion quaternion = cameraLocData.getQuaternion();

				cameraSb = new StringBuilder("-" + getName() + "-");
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Point 1 (x, z, y): " + point1.getX() + ", " + point1.getHeight() + ", " + point1.getY());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Point 2 (x, y, z): " + point2.getX() + ", " + point2.getY() + ", " + point2.getHeight());
				cameraSb.append(System.lineSeparator());
				cameraSb.append("Quaternion (x, y, z, a): " + quaternion.getX() + ", " + quaternion.getY() + ", " + quaternion.getZ() + ", " + quaternion.getA());
				cameraSb.append(System.lineSeparator());
				return cameraSb.toString();
			case Viewport:
				StringBuilder viewportSb = new StringBuilder("-" + getName() + "-");
				viewportSb.append(System.lineSeparator());
				viewportSb.append("Viewport: " + ArrayUtils.arrayToString(ViewportCallback.floats));
				viewportSb.append(System.lineSeparator());
				return viewportSb.toString();
			case DebugViewports:
				viewportSb = new StringBuilder("-" + getName() + "-");
				viewportSb.append(System.lineSeparator());
				viewportSb.append("Viewport 1: " + ArrayUtils.arrayToString(ViewportCallback.debugFloats1));
				viewportSb.append(System.lineSeparator());
				viewportSb.append("Viewport 2: " + ArrayUtils.arrayToString(ViewportCallback.debugFloats2));
				viewportSb.append(System.lineSeparator());
				return viewportSb.toString();
			case Menu:
				StringBuilder menuSb = new StringBuilder("-" + getName() + "-");
				if (org.rs3.api.Menu.isOpen()) {
					String[] items = org.rs3.api.Menu.getItems();
					if (items != null) {
						for (String item : items) {
							menuSb.append(System.lineSeparator());
							menuSb.append("- " + item);
						}
					}
				}
				return menuSb.toString();
			case ScriptElapsed:
				StringBuilder elapsedSb = new StringBuilder(getName() + ": ");
				elapsedSb.append(Time.format(System.currentTimeMillis() - ScriptHandler.scriptStartTime));
				return elapsedSb.toString();
			case ScriptInfo:
				if (ScriptHandler.currentScript != null)
					return ScriptHandler.currentScript.getInfo();
				break;
			}
			
			return null;
		}
		
		public boolean shouldPaint() {
			return paint;
		}

		public void setPaint(boolean paint) {
			this.paint = paint;
		}
	}

	//public static BufferedImage image = null;
	public static VolatileImage image = null;

	public static void blit(SurfaceData srcData, SurfaceData dstData) {
		try {
			int w = srcData.getBounds().width;
			int h = srcData.getBounds().height;

			SurfaceType srcType = srcData.getSurfaceType();
			SurfaceType dstType = dstData.getSurfaceType();

			Blit blit = Blit.getFromCache(srcType, CompositeType.SrcNoEa, dstType);
			blit.Blit(srcData, dstData,
					AlphaComposite.Src, null,
					0, 0, 0, 0, w, h);
			dstData.markDirty();
		} catch (SecurityException | IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	public static void blit(Graphics srcGraphics, Graphics dstGraphics) {
		if (srcGraphics == null)
			return;
		
		try {
			Class<?> classGraphics = Class.forName("sun.java2d.SunGraphics2D");
			Field fieldSurfaceData = classGraphics.getField("surfaceData");

			Object objSrcSurfaceData = fieldSurfaceData.get(srcGraphics);
			Object objDstSurfaceData = fieldSurfaceData.get(dstGraphics);

			SurfaceData srcData = (SurfaceData) objSrcSurfaceData;
			SurfaceData dstData = (SurfaceData) objDstSurfaceData;
			dstData.makeProxyFor(srcData).updateSurfaceData(srcData, dstData, srcData.getBounds().width, srcData.getBounds().height);
		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static Map<Long, Point[]> mousePoints = new HashMap<>();

	public static Player my = null;
	public static Npc npc = null;
	public static AnimableObject animableObj = null;
	public static AnimatedAnimableObject animatedAnimableObj = null;
	public static GroundObject groundObject = null;
	public static GroundItem groundItem = null;
	public static Rectangle[] components = null;

	public static Point point = null;

	public static Tile tile = null;
	public static Tile[] tiles = null;
	public static Tile[][] tiles2D = null;

	public static String paintString = null;

	public static Model model = null; // rendered at player location

	private static final Stroke singleStroke = new BasicStroke(1);
	private static final Stroke doubleStroke = new BasicStroke(2);
	private static final Stroke tripleStroke = new BasicStroke(3);
	
	private static GroundObject[] groundObjectCache = null;
	private static final Timer groundObjectCacheUpdateTimer = new Timer(5000);

	public static void updatePaintObject(Object paintObj) {
		updatePaintObject(false, paintObj, null);
	}

	public static void updatePaintObject(Object paintObj, Set<Npc> modelCaptureSet) {
		updatePaintObject(false, paintObj, modelCaptureSet);
	}

	public static void clearPaintObject(Object paintObj) {
		updatePaintObject(true, paintObj, null);
	}

	public static void clearPaintObject(Object paintObj, Set<Npc> modelCaptureSet) {
		updatePaintObject(true, paintObj, modelCaptureSet);
	}

	public static void updatePaintObject(boolean clearOnly, Object paintObj, Set<Npc> modelCaptureSet) {
		if (paintObj == null)
			return;

		boolean nullModelCapture = modelCaptureSet == null ? true : false;

		if (paintObj instanceof Player) {
			if (!clearOnly && Paint.my == null)
				Paint.my = (Player) paintObj;

			if (Paint.my != null) {
				synchronized (Paint.my) {
					if (!nullModelCapture)
						modelCaptureSet.remove(Paint.my);

					if (clearOnly) {
						Paint.my = null;
						return;
					}

					Paint.my = (Player) paintObj;

					// TODO: add when player model callback is done
					//if (!nullModelCapture)
					//	modelCaptureSet.add(Paint.my);
				}
			}
		} else if (paintObj instanceof Npc) {
			if (!clearOnly && Paint.npc == null)
				Paint.npc = (Npc) paintObj;

			if (Paint.npc != null) {
				synchronized (Paint.npc) {
					if (!nullModelCapture)
						modelCaptureSet.remove(Paint.npc);

					if (clearOnly) {
						Paint.npc = null;
						return;
					}

					Paint.npc = (Npc) paintObj;

					if (!nullModelCapture)
						modelCaptureSet.add(Paint.npc);
				}
			}
		} else if (paintObj instanceof AnimableObject) {
			if (!clearOnly && Paint.animableObj == null)
				Paint.animableObj = (AnimableObject) paintObj;

			if (Paint.animableObj != null) {
				synchronized (Paint.animableObj) {
					if (clearOnly) {
						Paint.animableObj = null;
						return;
					}

					Paint.animableObj = (AnimableObject) paintObj;
				}
			}
		} else if (paintObj instanceof AnimatedAnimableObject) {
			if (!clearOnly && Paint.animatedAnimableObj == null)
				Paint.animatedAnimableObj = (AnimatedAnimableObject) paintObj;

			if (Paint.animatedAnimableObj != null) {
				synchronized (Paint.animatedAnimableObj) {
					if (clearOnly) {
						Paint.animatedAnimableObj = null;
						return;
					}

					Paint.animatedAnimableObj = (AnimatedAnimableObject) paintObj;
				}
			}
		} else if (paintObj instanceof GroundObject) {
			if (!clearOnly && Paint.groundObject == null)
				Paint.groundObject = (GroundObject) paintObj;

			if (Paint.groundObject != null) {
				synchronized (Paint.groundObject) {
					if (clearOnly) {
						Paint.groundObject = null;
						return;
					}

					Paint.groundObject = (GroundObject) paintObj;
				}
			}
		} else if (paintObj instanceof GroundItem) {
			if (!clearOnly && Paint.groundItem == null)
				Paint.groundItem = (GroundItem) paintObj;

			if (Paint.groundItem != null) {
				synchronized (Paint.groundItem) {
					if (clearOnly) {
						Paint.groundItem = null;
						return;
					}

					Paint.groundItem = (GroundItem) paintObj;
				}
			}
		} else if (paintObj instanceof Rectangle[]) {
			if (!clearOnly && Paint.components == null)
				Paint.components = (Rectangle[]) paintObj;

			if (Paint.components != null) {
				synchronized (Paint.components) {
					if (clearOnly) {
						Paint.components = null;
						return;
					}

					Paint.components = (Rectangle[]) paintObj;
				}
			}
		} else if (paintObj instanceof Point) {
			if (!clearOnly && Paint.point == null)
				Paint.point = (Point) paintObj;

			if (Paint.point != null) {
				synchronized (Paint.point) {
					if (clearOnly) {
						Paint.point = null;
						return;
					}

					Paint.point = (Point) paintObj;
				}
			}
		} else if (paintObj instanceof Tile) {
			if (!clearOnly && Paint.tile == null)
				Paint.tile = (Tile) paintObj;

			if (Paint.tile != null) {
				synchronized (Paint.tile) {
					if (clearOnly) {
						Paint.tile = null;
						return;
					}

					Paint.tile = (Tile) paintObj;
				}
			}
		} else if (paintObj instanceof Tile[]) {
			if (!clearOnly && Paint.tiles == null)
				Paint.tiles = (Tile[]) paintObj;

			if (Paint.tiles != null) {
				synchronized (Paint.tiles) {
					if (clearOnly) {
						Paint.tiles = null;
						return;
					}

					Paint.tiles = (Tile[]) paintObj;
				}
			}
		} else if (paintObj instanceof Tile[][]) {
			if (!clearOnly && Paint.tiles2D == null)
				Paint.tiles2D = (Tile[][]) paintObj;

			if (Paint.tiles2D != null) {
				synchronized (Paint.tiles2D) {
					if (clearOnly) {
						Paint.tiles2D = null;
						return;
					}

					Paint.tiles2D = (Tile[][]) paintObj;
				}
			}
		} else if (paintObj instanceof String) {
			if (!clearOnly && Paint.paintString == null)
				Paint.paintString = (String) paintObj;

			if (Paint.paintString != null) {
				synchronized (Paint.paintString) {
					if (clearOnly) {
						Paint.paintString = null;
						return;
					}

					Paint.paintString = (String) paintObj;
				}
			}
		} else if (paintObj instanceof Model) {
			if (!clearOnly && Paint.model == null)
				Paint.model = (Model) paintObj;

			if (Paint.model != null) {
				synchronized (Paint.model) {
					if (clearOnly) {
						Paint.model = null;
						return;
					}

					Paint.model = (Model) paintObj;
				}
			}
		} else
			System.out.println("Unsupported paint object to update! (" + paintObj.getClass().getName() + ")");
	}
	
	public static void draw(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		Composite originalComp = g2.getComposite();
		AlphaComposite alphaComp;
		
		if (BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85F);
			int y = 20;
			
			for (DebugString debugString : DebugString.values()) {
				if (debugString.equals(DebugString.ScriptElapsed)
						|| debugString.equals(DebugString.AccountInfo)
						|| debugString.equals(DebugString.ScriptInfo)) {
					if (debugString.shouldPaint()) {
						String string = debugString.getString();
						
						if (string != null) {
							g2.setComposite(alphaComp);
							g2.setColor(Color.black);
							Point stringBox = getStringBox(g, string);
							g2.fillRect(20, y, stringBox.x + 6, stringBox.y + 6);
							g2.setComposite(originalComp);
		
							g2.setColor(Color.red);
							drawString(g2, string, 24, y);
							
							y += stringBox.y + 10;
						}
					}
				}
			}
			
			return;
		}
		
		if (ScriptHandler.currentScript != null && ScriptHandler.currentScript instanceof PaintListenter)
			((PaintListenter) ScriptHandler.currentScript).draw(g);
		
		boolean loggedIn = Client.isLoggedIn();

		if (components != null) {
			synchronized (components) {
				g.setColor(Color.white);

				for (Rectangle rect : components) {
					g2.draw(rect);
				}
			}
		}

		if (point != null) {
			synchronized (point) {
				drawCross(g2, singleStroke, 4, Color.cyan, point);
			}
		}

		if (paintString != null) {
			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85F);

			g2.setComposite(alphaComp);
			g2.setColor(Color.black);

			synchronized (paintString) {
				Point stringBox = getStringBox(g, paintString);
				g2.fillRect(10, 10, stringBox.x + 4, stringBox.y + 4);
				g2.setComposite(originalComp);
				g2.setColor(Color.green);
				drawString(g2, paintString, 12, 12);
			}
		}

		if (loggedIn || LobbyScreen.get.isOpen()) {
			if (Menu.isOpen()) {
				Point menu = Menu.getLocation();

				g.setColor(Color.red);
				g.drawRect(menu.x, menu.y, Menu.getWidth(), Menu.getHeight());
				
				String[] items = Menu.getItems();
				for (int i = 0; i < items.length; i++) {
					g.drawRect(menu.x, menu.y + 21 + 16*i, Menu.getWidth(), 16);
				}
			}
		}


		if (loggedIn) {
			g.setColor(Color.green);

			//Location.drawNearestTiles(g);

			if (tiles2D != null) {
				for (int i = 0; i < tiles2D.length; i++) {
					for (int i2 = 0; i2 < tiles2D[0].length; i2++) {
						Tile tile = tiles2D[i][i2];
						if (tile != null)
							tile.draw(g);
					}
				}
			}

			if (tiles != null) {
				for (Tile tile : tiles) {
					if (tile != null) {
						tile.draw(g);
					}
				}
			}

			g.setColor(Color.red);
			if (tile != null) {
				tile.draw(g);
			}

			g.setColor(Color.green);
			if (my != null) {
				synchronized (my) {
					my.getData().getCenterLocation().draw(g);
					my.draw(g);

					Point mapPoint = my.getData().getCenterLocation().getMapPoint();
					if (mapPoint != null)
						g.drawRect(mapPoint.x - 2, mapPoint.y - 2, 4, 4);
				}
			}

			if (npc != null) {
				synchronized (npc) {
					npc.draw(g);
				}
			}

			Set<Npc> npcsCopy = Collections.newSetFromMap(new IdentityHashMap<Npc, Boolean>());
			synchronized (ModelCapture.npcs) {
				npcsCopy.addAll(ModelCapture.npcs);
			}

			for (Npc npc : npcsCopy) {
				synchronized (npc) {
					npc.draw(g);
				}
			}

			if (animableObj != null) {
				synchronized (animableObj) {
					//animableObj.draw(g);
					animableObj.getTile().drawOnScreen(g);
				}
			}

			if (animatedAnimableObj != null) {
				synchronized (animatedAnimableObj) {
					animatedAnimableObj.draw(g);
				}
			}

			if (groundObject != null) {
				synchronized (groundObject) {
					groundObject.draw(g);
				}
			}

			if (groundItem != null) {
				synchronized (groundItem) {
					groundItem.draw(g);
				}
			}

			if (model != null) {
				Player player = Players.getMyPlayer();
				if (player != null) {
					synchronized (model) {
						model.draw(g, player.getInternalTile());
					}
				}
			}	

			g.setColor(Color.blue);
			g2.setStroke(doubleStroke);
			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7F);
			g2.setComposite(alphaComp);

			List<Long> mousePointsToRemove = new ArrayList<>();

			synchronized (mousePoints) {
				for (long time : mousePoints.keySet()) {
					if (System.currentTimeMillis() - time > 2000)
						mousePointsToRemove.add(time);
					else {
						Point[] points = mousePoints.get(time);

						g.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
					}
				}

				for (long time : mousePointsToRemove) {
					mousePoints.remove(time);
				}
			}

			g2.setComposite(originalComp);

			Point mousePos = Mouse.getClientPos();
			if (mousePos != null) {
				Color color = null;

				if (!Mouse.isMouseButtonHeld(Mouse.LEFT)) {
					if (Mouse.lastClickMs == 0L) {
						color = Color.yellow;
					} else {
						if (System.currentTimeMillis() - Mouse.lastClickMs < 500)
							color = Color.red;
						else {
							color = Color.yellow;
							Mouse.lastClickMs = 0L;
						}
					}
				} else
					color = Color.red;

				drawCross(g2, doubleStroke, 6, color, mousePos);
			}

			g2.setStroke(singleStroke);


			// Debug Objects
			if (true) {
				if (DebugObject.NPCS.shouldPaint()) {
					boolean paintModel = false;

					g.setColor(Color.lightGray);
					g.setFont(g.getFont().deriveFont(Font.BOLD));
					Npc[] npcs = Npcs.getAll();
					if (npcs != null) {
						List<Npc> okNpcs = new ArrayList<Npc>();

						for (Npc npc : npcs) {
							if (npc == null)
								continue;

							NpcDefinition def = npc.getNpcDefinition();

							if (def != null) {
								Tile tile = npc.getData().getCenterLocation().getTile(true);
								Point center = tile.getPoint(0.5D, 0.5D, -800);

								if (Calculations.isOnScreen(center)) {
									g.setColor(Color.lightGray);
									drawString(g2, def.getName() + "\nID: " + def.getId() + "\nAnimation: " + npc.getAnimationId(), center.x, center.y, true);

									if (paintModel) {
										Npc foundNpc = ModelCapture.findNpc(npc.getObject());
										if (foundNpc == null) {
											okNpcs.add(npc);

											synchronized (ModelCapture.npcs) {
												ModelCapture.npcs.add(npc);
											}
										} else
											okNpcs.add(foundNpc);

										// draw is not necessary here, captured models are drawn by default
										/*synchronized (npc) {
											npc.draw(g2);
										}*/
									}
								}
							}

							Point mapPoint = npc.getData().getCenterLocation().getTile(true).getMapPoint();
							if (mapPoint != null) {
								g.setColor(Color.red);
								g.drawRoundRect(mapPoint.x - 2, mapPoint.y - 2, 4, 4, 2, 2);
							}
						}

						if (paintModel) {
							// clear npcs from model capture that should no longer be in there
							List<Npc> toRemove = new ArrayList<Npc>();
							for (Npc npc : ModelCapture.npcs) {
								boolean ok = false;
								for (Npc okNpc : okNpcs) {
									if (okNpc == npc) {
										ok = true;
										break;
									}
								}

								if (!ok) {
									toRemove.add(npc);
								}
							}


							synchronized (ModelCapture.npcs) {
								for (Npc npc : toRemove) {
									ModelCapture.npcs.remove(npc);
								}
							}
						} else {
							synchronized (ModelCapture.npcs) {
								ModelCapture.npcs.clear();
							}
						}
					}
				}

				if (DebugObject.GROUND_OBJECTS.shouldPaint()) {
					g.setColor(Color.lightGray);
					g.setFont(g.getFont().deriveFont(Font.BOLD));
					
					if (groundObjectCache == null || !groundObjectCacheUpdateTimer.isRunning()) {
						groundObjectCache = GroundObjects.getAllOnScreen(GroundObject.Type.All);
						groundObjectCacheUpdateTimer.reset();
					}
					
					GroundObject[] gos = groundObjectCache;
					if (gos != null) {
						for (GroundObject go : gos) {
							if (go == null)
								continue;
							
							ObjectDefinition def = go.getDefinition();

							if (def != null) {
								if (DebugObject.skipNullNames()) {
									if (def.getName() == null || def.getName().equals("null"))
										continue;
								}
								
								Tile tile = go.getTile();
								Point center = tile.getPoint(0.5D, 0.5D, -800);

								if (Calculations.isOnScreen(center)) {
									g.setColor(Color.lightGray);
									//drawString(g2, def.getName() + "\nID: " + def.getId() + "\nAnimation: " + go.getAnimationId(), center.x, center.y, true);
									drawString(g2, def.getName() + "\nID: " + def.getId(), center.x, center.y, true);

									//g.setColor(Color.green);
									//go.draw(g);
								}
							}
						}
					}
				}

				if (DebugObject.GROUND_ITEMS.shouldPaint()) {
					g.setColor(Color.red);
					g.setFont(g.getFont().deriveFont(Font.BOLD));
					GroundItem[] gis = GroundItems.getAll();
					if (gis != null) {
						for (GroundItem gi : gis) {
							if (gi == null)
								continue;

							ItemDefinition def = gi.getItemNode().getDefinition();

							if (def != null) {
								Tile tile = gi.getTile();
								Point center = tile.getPoint(0.5D, 0.5D, -800);

								if (Calculations.isOnScreen(center)) {
									if (gi.getStackSize() > 1)
										drawString(g2, def.getName() + "\nID: " + def.getId() + "\nStack: " + gi.getStackSize(), center.x, center.y, true);
									else
										drawString(g2, def.getName() + "\nID: " + def.getId(), center.x, center.y, true);
								}
							}
						}
					}
				}

				if (DebugObject.INVENTORY.shouldPaint()) {
					g.setColor(Color.yellow);
					g.setFont(g.getFont().deriveFont(Font.BOLD));
					if (InventoryWidget.get.isOpen()) {
						Component[] items = InventoryWidget.get.getItems();

						if (items != null) {
							for (Component item : items) {
								if (item == null)
									continue;

								Point point = item.getAbsoluteLocation();
								if (point != null)
									drawString(g2, "" + item.getComponentId(), point.x, point.y);
							}
						}
					}
				}

				if (DebugObject.BANK.shouldPaint()) {
					g.setColor(Color.yellow);
					g.setFont(g.getFont().deriveFont(Font.BOLD));
					if (Bank.get.isOpen()) {
						Component[] bankItems = Bank.get.getBankItems();

						if (bankItems != null) {
							for (Component item : bankItems) {
								if (item == null /*|| !Bank.get.isBankItemDisplayed(item)*/)
									continue;

								Point point = item.getAbsoluteLocation();
								if (point != null)
									drawString(g2, "" + item.getComponentId(), point.x, point.y);
							}
						}


						Component[] invItems = Bank.get.getInvItems();

						if (invItems != null) {
							for (Component item : invItems) {
								if (item == null)
									continue;

								Point point = item.getAbsoluteLocation();
								if (point != null)
									drawString(g2, "" + item.getComponentId(), point.x, point.y);
							}
						}
					}
				}
				
				if (DebugObject.WORN_EQUIPMENT.shouldPaint()) {
					g.setColor(Color.yellow);
					g.setFont(g.getFont().deriveFont(Font.BOLD));
					if (WornEquipmentWidget.get.isOpen()) {
						Component[] equipment = WornEquipmentWidget.get.getEquipment();

						if (equipment != null) {
							for (Component item : equipment) {
								if (item == null)
									continue;
								
								if (item.isVisible()) {
									Point point = item.getAbsoluteLocation();
									if (point != null)
										drawString(g2, "" + item.getComponentId(), point.x, point.y);
								}
							}
						}
					}
				}
			}

			g2.setStroke(singleStroke);
			g.setFont(g.getFont().deriveFont(Font.PLAIN));

			// Debug strings
			
			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85F);
			int y = 20;
			
			for (DebugString debugString : DebugString.values()) {
				if (debugString.shouldPaint()) {
					String string = debugString.getString();
					
					if (string != null) {
						g2.setComposite(alphaComp);
						g2.setColor(Color.black);
						Point stringBox = getStringBox(g, string);
						g2.fillRect(20, y, stringBox.x + 6, stringBox.y + 6);
						g2.setComposite(originalComp);
	
						g2.setColor(Color.red);
						drawString(g2, string, 24, y);
						
						y += stringBox.y + 10;
					}
				}
			}

			
			/*String debugString1 = getDebugString(DebugString.Camera)

			String debugString2 = getDebugString(DebugString.CameraLocationData);

			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85F);
			g2.setComposite(alphaComp);
			g2.setColor(Color.black);
			Point stringBox1 = getStringBox(g, debugString1);
			g2.fillRect(20, 20, stringBox1.x + 4, stringBox1.y + 4);
			Point stringBox2 = getStringBox(g, debugString2);
			g2.fillRect(20, 20 + stringBox1.y + 8, stringBox2.x + 4, stringBox2.y + 4);
			g2.setComposite(originalComp);

			g2.setColor(Color.red);
			drawString(g2, debugString1, 20, 20);
			drawString(g2, debugString2, 20, 20 + stringBox1.y + 8);*/
		}
	}

	public static Point getStringBox(Graphics g, String text) {
		FontMetrics metrics = g.getFontMetrics();
		//int metricHeight = metrics.getHeight();

		int metricHeightSum = 0;
		int largestTextWidth = 0;
		
		for (String line : text.split("\n")) {
			//g.drawString(line, x, y += metricHeight);

			Rectangle2D rect = metrics.getStringBounds(line, g);

			metricHeightSum += rect.getHeight();

			int adv = (int) rect.getWidth();
			if (adv > largestTextWidth)
				largestTextWidth = adv;

			/*metricHeightSum += metricHeight;

	        int adv = metrics.stringWidth(line);
	        if (adv > largestTextWidth)
	        	largestTextWidth = adv;*/
		}

		return new Point(largestTextWidth, metricHeightSum);
	}

	public static void drawString(Graphics g, String text, int x, int y) {
		drawString(g, text, x, y, false);
	}

	public static void drawString(Graphics g, String text, int x, int y, boolean center) {
		FontMetrics metrics = g.getFontMetrics();

		String[] lines = text.split("\n");

		if (center)
			y -= Math.round((metrics.getHeight() * lines.length) / 2D);

		for (String line : lines) {
			if (!center)
				g.drawString(line, x, y += metrics.getStringBounds(line, g).getHeight());
			else {
				int xoff = (int) Math.round(metrics.getStringBounds(line, g).getWidth() / 2D);
				g.drawString(line, x - xoff, y += metrics.getStringBounds(line, g).getHeight());
			}
		}
	}

	public static void drawCross(Graphics2D g2, Stroke stroke, int size, Color color, Point point) {
		g2.setColor(color);
		g2.setStroke(stroke);

		g2.drawLine(point.x - size, point.y - size, point.x + size, point.y + size);
		g2.drawLine(point.x - size, point.y + size, point.x + size, point.y - size);
	}

	static float[] rsOrigProjectionMatrix = null;
	static float[] rsOrigModelViewMatrix = null;
	static int rsOrigMatrixMode = -1;
	static int[] rsOrigViewport = null;
	static boolean rsOrigTexture2d = false;

	public static void setup(GL2 gl2, int width, int height) {
		//
		// get rs current values
		//
		float[] projectionMatrix = new float[16];
		gl2.glGetFloatv(GL2.GL_PROJECTION_MATRIX, projectionMatrix, 0);
		rsOrigProjectionMatrix = projectionMatrix;
		//System.out.println("Orig projection matrix: " + Arrays.toString(rsOrigProjectionMatrix));

		float[] modelViewMatrix = new float[16];
		gl2.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, modelViewMatrix, 0);
		rsOrigModelViewMatrix = modelViewMatrix;
		//System.out.println("Orig model view matrix: " + Arrays.toString(rsOrigModelViewMatrix));

		int[] matrixMode = new int[1];
		gl2.glGetIntegerv(GL2.GL_MATRIX_MODE, matrixMode, 0);
		rsOrigMatrixMode = matrixMode[0];
		//System.out.println("Orig matrix mode: " + rsOrigMatrixMode);

		int[] viewport = new int[4];
		gl2.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
		rsOrigViewport = viewport;
		//System.out.println("Orig viewport: " + Arrays.toString(rsOrigViewport));

		int[] texture2d = new int[1];
		gl2.glGetIntegerv(GL2.GL_TEXTURE_2D, texture2d, 0);
		if (texture2d[0] == GL2.GL_TRUE)
			rsOrigTexture2d = true;
		else
			rsOrigTexture2d = false;
		//System.out.println("Orig texture 2d: " + rsOrigTexture2d);


		gl2.glMatrixMode( GLMatrixFunc.GL_PROJECTION );
		gl2.glLoadIdentity();
		gl2.glOrtho(0, width, height, 0, -1d, 1d);

		// coordinate system origin at top left with width and height same as the window
		//GLU glu = new GLU();
		//glu.gluOrtho2D( 0f, width, 0f, height );
		//glu.gluOrtho2D( 0f, width, height, 0f );

		gl2.glMatrixMode( GLMatrixFunc.GL_MODELVIEW );
		gl2.glLoadIdentity();

		gl2.glViewport( 0, 0, width, height );

		gl2.glDisable(GL2.GL_TEXTURE_2D);
	}

	public static void reset(GL2 gl2) {
		gl2.glMatrixMode( GLMatrixFunc.GL_PROJECTION );
		gl2.glLoadMatrixf(rsOrigProjectionMatrix, 0);

		gl2.glMatrixMode( GLMatrixFunc.GL_MODELVIEW );
		gl2.glLoadMatrixf(rsOrigModelViewMatrix, 0);

		gl2.glMatrixMode(rsOrigMatrixMode);

		gl2.glViewport(rsOrigViewport[0], rsOrigViewport[1], rsOrigViewport[2], rsOrigViewport[3]);

		if (rsOrigTexture2d)
			gl2.glEnable(GL2.GL_TEXTURE_2D);
		else
			gl2.glDisable(GL2.GL_TEXTURE_2D);
	}

	public static void render(GL2 gl2) {
		boolean loggedIn = Client.isLoggedIn();

		if (components != null) {
			synchronized (components) {
				renderRectangles(gl2, Color.white, components);
			}
		}

		if (point != null) {
			synchronized (point) {
				renderCross(gl2, singleStroke, 4, Color.cyan, point);
			}
		}

		// TODO
		/*if (paintString != null) {
			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85F);

			g2.setComposite(alphaComp);
			g2.setColor(Color.black);

			synchronized (paintString) {
				Point stringBox = getStringBox(g, paintString);
				g2.fillRect(10, 10, stringBox.x + 4, stringBox.y + 4);
				g2.setComposite(originalComp);
				g2.setColor(Color.green);
				drawString(g2, paintString, 12, 12);
			}
		}*/

		if (loggedIn || LobbyScreen.get.isOpen()) {
			if (Menu.isOpen()) {
				Point menu = Menu.getLocation();

				renderRectangles(gl2, Color.red, new Rectangle(menu.x, menu.y, Menu.getWidth(), Menu.getHeight()));
			}
		}


		if (loggedIn) {
			//g.setColor(Color.green);

			//Location.drawNearestTiles(g);

			if (tiles2D != null) {
				for (int i = 0; i < tiles2D.length; i++) {
					for (int i2 = 0; i2 < tiles2D[0].length; i2++) {
						Tile tile = tiles2D[i][i2];
						if (tile != null)
							tile.render(gl2);
					}
				}
			}

			if (tiles != null) {
				for (Tile tile : tiles) {
					if (tile != null) {
						tile.render(gl2);
					}
				}
			}

			//g.setColor(Color.red); TODO
			if (tile != null) {
				tile.render(gl2);
			}

			if (my != null) {
				synchronized (my) {
					my.getData().getCenterLocation().render(gl2);
					my.render(gl2);

					Point mapPoint = my.getData().getCenterLocation().getMapPoint();
					if (mapPoint != null)
						renderRectangles(gl2, Color.green, new Rectangle(mapPoint.x - 2, mapPoint.y - 2, 4, 4));
				}
			}

			if (npc != null) {
				synchronized (npc) {
					npc.render(gl2);
				}
			}

			Set<Npc> npcsCopy = Collections.newSetFromMap(new IdentityHashMap<Npc, Boolean>());
			synchronized (ModelCapture.npcs) {
				npcsCopy.addAll(ModelCapture.npcs);
			}

			for (Npc npc : npcsCopy) {
				synchronized (npc) {
					npc.render(gl2);
				}
			}

			if (animableObj != null) {
				synchronized (animableObj) {
					animableObj.render(gl2);
				}
			}

			if (animatedAnimableObj != null) {
				synchronized (animatedAnimableObj) {
					animatedAnimableObj.render(gl2);
				}
			}

			if (groundObject != null) {
				synchronized (groundObject) {
					groundObject.render(gl2);
				}
			}

			if (groundItem != null) {
				synchronized (groundItem) {
					groundItem.render(gl2);
				}
			}

			if (model != null) {
				Player player = Players.getMyPlayer();
				if (player != null) {
					synchronized (model) {
						model.render(gl2, player.getInternalTile());
					}
				}
			}

			float[] blueRgb = getGlRGBColor(Color.blue);

			List<Long> mousePointsToRemove = new ArrayList<>();

			synchronized (mousePoints) {
				for (long time : mousePoints.keySet()) {
					if (System.currentTimeMillis() - time > 2000)
						mousePointsToRemove.add(time);
					else {
						Point[] points = mousePoints.get(time);

						gl2.glLineWidth(getGlLineWidth(doubleStroke));
						gl2.glBegin( GL.GL_LINES );
						gl2.glColor4f( blueRgb[0], blueRgb[1], blueRgb[2], 0.7f );
						gl2.glVertex2f( points[0].x, points[0].y );
						gl2.glVertex2f( points[1].x, points[1].y );
						gl2.glFlush();
						gl2.glEnd();
					}
				}

				for (long time : mousePointsToRemove) {
					mousePoints.remove(time);
				}
			}

			Point mousePos = Mouse.getClientPos();
			if (mousePos != null) {
				Color color = null;

				if (!Mouse.isMouseButtonHeld(Mouse.LEFT)) {
					if (Mouse.lastClickMs == 0L) {
						color = Color.yellow;
					} else {
						if (System.currentTimeMillis() - Mouse.lastClickMs < 500)
							color = Color.red;
						else {
							color = Color.yellow;
							Mouse.lastClickMs = 0L;
						}
					}
				} else
					color = Color.red;

				renderCross(gl2, doubleStroke, 6, color, mousePos);
			}

			gl2.glLineWidth(getGlLineWidth(singleStroke));

			// Debug Objects
			if (true) {
				if (DebugObject.NPCS.shouldPaint()) {
					boolean paintModel = false;

					//g.setColor(Color.lightGray);
					//g.setFont(g.getFont().deriveFont(Font.BOLD));
					Npc[] npcs = Npcs.getAll();
					if (npcs != null) {
						List<Npc> okNpcs = new ArrayList<Npc>();

						for (Npc npc : npcs) {
							if (npc == null)
								continue;

							NpcDefinition def = npc.getNpcDefinition();

							if (def != null) {
								Tile tile = npc.getData().getCenterLocation().getTile(true);
								Point center = tile.getPoint(0.5D, 0.5D, -800);

								if (Calculations.isOnScreen(center)) {
									//g.setColor(Color.lightGray);
									//drawString(g2, def.getName() + "\nID: " + def.getId() + "\nAnimation: " + npc.getAnimationId(), center.x, center.y, true);

									if (paintModel) {
										Npc foundNpc = ModelCapture.findNpc(npc.getObject());
										if (foundNpc == null) {
											okNpcs.add(npc);

											synchronized (ModelCapture.npcs) {
												ModelCapture.npcs.add(npc);
											}
										} else
											okNpcs.add(foundNpc);

										// draw is not necessary here, captured models are drawn by default
										/*synchronized (npc) {
											npc.render(gl2);
										}*/
									}
								}
							}

							Point mapPoint = npc.getData().getCenterLocation().getTile(true).getMapPoint();
							if (mapPoint != null) {
								//g.setColor(Color.red);
								//g.drawRoundRect(mapPoint.x - 2, mapPoint.y - 2, 4, 4, 2, 2);
							}
						}

						if (paintModel) {
							// clear npcs from model capture that should no longer be in there
							List<Npc> toRemove = new ArrayList<Npc>();
							for (Npc npc : ModelCapture.npcs) {
								boolean ok = false;
								for (Npc okNpc : okNpcs) {
									if (okNpc == npc) {
										ok = true;
										break;
									}
								}

								if (!ok) {
									toRemove.add(npc);
								}
							}


							synchronized (ModelCapture.npcs) {
								for (Npc npc : toRemove) {
									ModelCapture.npcs.remove(npc);
								}
							}
						} else {
							synchronized (ModelCapture.npcs) {
								ModelCapture.npcs.clear();
							}
						}
					}
				}

				if (DebugObject.GROUND_OBJECTS.shouldPaint()) {
					//g.setColor(Color.lightGray);
					//g.setFont(g.getFont().deriveFont(Font.BOLD));
					GroundObject[] gos = GroundObjects.getAllOnScreen(GroundObject.Type.All);
					if (gos != null) {
						for (GroundObject go : gos) {
							if (go == null)
								continue;

							ObjectDefinition def = go.getDefinition();

							if (def != null) {
								Tile tile = go.getTile();
								Point center = tile.getPoint(0.5D, 0.5D, -800);

								if (Calculations.isOnScreen(center)) {
									//g.setColor(Color.lightGray);
									//drawString(g2, def.getName() + "\nID: " + def.getId() + "\nAnimation: " + go.getAnimationId(), center.x, center.y, true);
									//drawString(g2, def.getName() + "\nID: " + def.getId(), center.x, center.y, true);

									//g.setColor(Color.green);
									go.render(gl2);
								}
							}
						}
					}
				}

				if (DebugObject.GROUND_ITEMS.shouldPaint()) {
					//g.setColor(Color.red);
					//g.setFont(g.getFont().deriveFont(Font.BOLD));
					GroundItem[] gis = GroundItems.getAll();
					if (gis != null) {
						for (GroundItem gi : gis) {
							if (gi == null)
								continue;

							ItemDefinition def = gi.getItemNode().getDefinition();

							if (def != null) {
								Tile tile = gi.getTile();
								Point center = tile.getPoint(0.5D, 0.5D, -800);

								if (Calculations.isOnScreen(center)) {
									//if (gi.getStackSize() > 1)
									//	drawString(g2, def.getName() + "\nID: " + def.getId() + "\nStack: " + gi.getStackSize(), center.x, center.y, true);
									//else
									//	drawString(g2, def.getName() + "\nID: " + def.getId(), center.x, center.y, true);
								}
							}
						}
					}
				}

				if (DebugObject.INVENTORY.shouldPaint()) {
					//g.setColor(Color.yellow);
					//g.setFont(g.getFont().deriveFont(Font.BOLD));
					if (InventoryWidget.get.isOpen()) {
						Component[] items = InventoryWidget.get.getItems();

						if (items != null) {
							for (Component item : items) {
								if (item == null)
									continue;

								Point point = item.getAbsoluteLocation();
								//if (point != null)
								//	drawString(g2, "" + item.getComponentId(), point.x, point.y);
							}
						}
					}
				}

				if (DebugObject.BANK.shouldPaint()) {
					//g.setColor(Color.yellow);
					//g.setFont(g.getFont().deriveFont(Font.BOLD));
					if (Bank.get.isOpen()) {
						Component[] bankItems = Bank.get.getBankItems();

						if (bankItems != null) {
							for (Component item : bankItems) {
								if (item == null /*|| !Bank.get.isBankItemDisplayed(item)*/)
									continue;

								Point point = item.getAbsoluteLocation();
								//if (point != null)
								//	drawString(g2, "" + item.getComponentId(), point.x, point.y);
							}
						}


						Component[] invItems = Bank.get.getInvItems();

						if (invItems != null) {
							for (Component item : invItems) {
								if (item == null)
									continue;

								Point point = item.getAbsoluteLocation();
								//if (point != null)
								//	drawString(g2, "" + item.getComponentId(), point.x, point.y);
							}
						}
					}
				}
			}

			// TODO
			gl2.glLineWidth(getGlLineWidth(singleStroke));
			/*g2.setStroke(singleStroke);
			g.setFont(g.getFont().deriveFont(Font.PLAIN));

			// Debug strings
			String debugString = getDebugString("Camera")
					+ System.lineSeparator() + getDebugString("CameraLocationData")
					/*+ System.lineSeparator() + getDebugString("Viewport")*//*;

			alphaComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85F);
			g2.setComposite(alphaComp);
			g2.setColor(Color.black);
			Point stringBox = getStringBox(g, debugString);
			g2.fillRect(20, 20, stringBox.x + 4, stringBox.y + 4);
			g2.setComposite(originalComp);

			g2.setColor(Color.red);
			drawString(g2, debugString, 20, 20);*/
		}
	}

	public static float[] getGlRGBColor(Color color) {
		if (color == null)
			return new float[] { 1, 1, 1 };

		return new float[] { color.getRed(), color.getGreen(), color.getBlue() };
	}

	public static float getGlLineWidth(Stroke stroke) {
		if (stroke.equals(singleStroke))
			return 1.0f;
		if (stroke.equals(doubleStroke))
			return 2.0f;
		if (stroke.equals(tripleStroke))
			return 3.0f;

		return 1.0f;
	}

	public static void renderCross(GL2 gl2, Stroke stroke, int size, Color color, Point point) {
		//System.out.println("dfgdddd");

		//gl2.glLoadIdentity();

		gl2.glLineWidth(getGlLineWidth(stroke));
		gl2.glBegin( GL.GL_LINES );
		//gl2.glColor3f( 1, 1, 1 );
		float[] rgb = getGlRGBColor(color);
		gl2.glColor3f(rgb[0], rgb[1], rgb[2]);

		gl2.glVertex2f( point.x - size, point.y - size );
		gl2.glVertex2f( point.x + size, point.y + size );

		gl2.glVertex2f( point.x - size, point.y + size );
		gl2.glVertex2f( point.x + size, point.y - size );

		gl2.glFlush();

		gl2.glEnd();
	}

	public static void renderRectangles(GL2 gl2, Color color, Rectangle... rectangles) {
		//System.out.println("lol");

		//gl2.glLoadIdentity();

		gl2.glBegin( GL.GL_LINES );
		for (Rectangle rect : rectangles) {
			//gl2.glColor3f( 1, 1, 1 );
			float[] rgb = getGlRGBColor(color);
			gl2.glColor3f(rgb[0], rgb[1], rgb[2]);

			/*gl2.glVertex2f( rect.x, rect.y );
	        gl2.glVertex2f( rect.x + rect.width, rect.y );
	        gl2.glVertex2f( rect.x + rect.width, rect.y + rect.height );
	        gl2.glVertex2f( rect.x, rect.y + rect.height );*/

			gl2.glVertex2f( rect.x, rect.y );
			gl2.glVertex2f( rect.x + rect.width, rect.y );

			gl2.glVertex2f( rect.x + rect.width, rect.y );
			gl2.glVertex2f( rect.x + rect.width, rect.y + rect.height );

			gl2.glVertex2f( rect.x + rect.width, rect.y + rect.height );
			gl2.glVertex2f( rect.x, rect.y + rect.height );

			gl2.glVertex2f( rect.x, rect.y + rect.height );
			gl2.glVertex2f( rect.x, rect.y );

			gl2.glFlush();
		}

		gl2.glEnd();
	}
}