package org.rs3.debug;

import java.awt.Graphics;

public interface PaintListenter {

	public void draw(Graphics g);
	
}
