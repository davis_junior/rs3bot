package org.rs3.debug;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.OpenGLRender;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.SDRender;
import org.rs3.boot.Frame;
import org.rs3.callbacks.capture.CanvasCapture;
import org.rs3.scripts.ScriptHandler;
import org.rs3.tools.ModelHashTableExplorer;
import org.rs3.tools.PathGenerator;
import org.rs3.tools.SettingsExplorer;
import org.rs3.tools.WidgetExplorer;



public class DebugFrame {

	public static DebugFrame get = null;
	
	java.awt.Frame frame;
	Panel panel;
	Composite comp = null;
	Canvas ccanvas = null;
	Thread animationThread;
	
	protected Shell shell;
	
	protected ToolBar toolBar;
	protected Menu dropMenu;
	
	public static volatile boolean paint = true;
	public static volatile boolean bitBlt = true;
	
	private volatile boolean close = false;
	private volatile boolean terminate = false;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DebugGL window = new DebugGL();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		createContents();
		shell.open();
		shell.layout();
		
		CanvasCapture.readPixels = true;
	}
	
	public synchronized void sendCloseSignal() {
		this.close = true;
		
		if (shell.isDisposed())
			return;
		
		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}
	
	private void close() {
		terminate = true;
		
		if  (animationThread != null) {
			try {
				animationThread.join();
				animationThread = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});
		
		dropMenu.dispose();
		shell.dispose();
		
		CanvasCapture.readPixels = false;
		
		System.out.println("DebugFrame closed and disposed.");
	}

	static Object memobj = null;
	
    public void resize(int width, int height) {
    	if (shell.isDisposed())
    		return;
    	
    	shell.setSize(width, height);
    	
		if (panel != null && comp != null) {
			toolBar.setSize(shell.getClientArea().width, toolBar.getBounds().height);
			ccanvas.setPreferredSize(new Dimension(shell.getClientArea().width, shell.getClientArea().height - toolBar.getBounds().height + 5));
			panel.setPreferredSize(ccanvas.getPreferredSize());
			comp.setBounds(0, toolBar.getBounds().height - 5, ccanvas.getPreferredSize().width, ccanvas.getPreferredSize().height);
			shell.pack();
		}
    }
    
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(Frame.getDisplay());
		
	    shell.addListener(SWT.Close, new Listener() {
	        @Override
			public void handleEvent(Event event) {
	        	//event.doit = false;
	        	close = true;
	        	close();
	        }
	      });
	    
		/*shell.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				resize();
			}
		});*/
	    
		//shell.setSize(1024, 768);
		shell.setText("DebugSD");
		
		toolBar = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);
		toolBar.setSize(1024, 36);
		
		final ToolItem tltmPaint = new ToolItem(toolBar, SWT.CHECK);
		tltmPaint.setWidth(240);
		tltmPaint.setSelection(true);
		paint = tltmPaint.getSelection();
		tltmPaint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				paint = tltmPaint.getSelection();
			}
		});
		tltmPaint.setText("Paint");
		
		final ToolItem tltmBitBlt = new ToolItem(toolBar, SWT.CHECK);
		tltmBitBlt.setWidth(240);
		tltmBitBlt.setSelection(true);
		bitBlt = tltmBitBlt.getSelection();
		tltmBitBlt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				bitBlt = tltmBitBlt.getSelection();
				CanvasCapture.readPixels = bitBlt;
			}
		});
		tltmBitBlt.setText("BitBlt");
		
		final ToolItem tltmWidgetExplorer = new ToolItem(toolBar, SWT.NONE);
		tltmWidgetExplorer.setWidth(240);
		tltmWidgetExplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					WidgetExplorer window = new WidgetExplorer();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmWidgetExplorer.setText("WidgetExplorer");
		
		ToolItem tltmPathgenerator = new ToolItem(toolBar, SWT.NONE);
		tltmPathgenerator.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					PathGenerator window = new PathGenerator();
					window.open(ccanvas);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmPathgenerator.setText("PathGenerator");
		
		ToolItem tltmSettingsExplorer = new ToolItem(toolBar, SWT.NONE);
		tltmSettingsExplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					SettingsExplorer window = new SettingsExplorer();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmSettingsExplorer.setText("SettingsExplorer");
		
		ToolItem tltmModelhashtabeexplorer = new ToolItem(toolBar, SWT.NONE);
		tltmModelhashtabeexplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					ModelHashTableExplorer window = new ModelHashTableExplorer();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmModelhashtabeexplorer.setText("ModelHashTabeExplorer");
		
		dropMenu = new Menu(shell, SWT.POP_UP);
		MenuItem item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Npcs");
		item.setSelection(Paint.DebugObject.NPCS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.NPCS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Ground Objects");
		item.setSelection(Paint.DebugObject.GROUND_OBJECTS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.GROUND_OBJECTS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Ground Items");
		item.setSelection(Paint.DebugObject.GROUND_ITEMS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.GROUND_ITEMS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Inventory");
		item.setSelection(Paint.DebugObject.INVENTORY.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.INVENTORY.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Bank");
		item.setSelection(Paint.DebugObject.BANK.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.BANK.setPaint(menuItem.getSelection());
			}
		});
		
		final ToolItem tltmObjects = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmObjects.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.ARROW) {
	                // Position the menu below and vertically aligned with the the drop down tool button.
	                final ToolItem toolItem = (ToolItem) e.widget;
	                final ToolBar  toolBar = toolItem.getParent();

	                Point point = toolBar.toDisplay(new Point(e.x, e.y));
	                dropMenu.setLocation(point.x, point.y);
	                dropMenu.setVisible(true);
				}
			}
		});
		tltmObjects.setText("Objects");

		class CustomCanvas extends Canvas {
			public BufferStrategy strategy = null;
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 3144891486031908540L;
			
			public void startAnimationThread() {
		      animationThread = new Thread() {
		    	  @Override
				public void run() {
		    		  
		    		  while (!terminate) {
			    		  Graphics bb = strategy.getDrawGraphics();
			    		  
			    		  java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();
	    				  int width = Client.getCanvasWidth();
	    				  int height = Client.getCanvasHeight();
	    				  
	    				  ccanvas.setLocation(canvas.getLocation());
	    				  
			    		  if (bitBlt) {
				    		  Render render = Client.getRender(); // TODO: calling render can be cpu intensive every frame
				    		  
				    		  if (render != null) {
					    		  if (render instanceof SDRender)
					    			  Paint.blit(canvas.getGraphics(), bb);
					    		  else if (render instanceof OpenGLRender) {
					    			  BufferedImage bufferedImage = CanvasCapture.getGlImage(false);
					    			  
					    			  if (bufferedImage != null)
					    				  Paint.blit(bufferedImage.getGraphics(), bb);
					    			  else {
						    			  bb.setColor(Color.black);
						    			  bb.fillRect(0, 0, width, height);
					    			  }
					    		  } else
					    			  Paint.blit(canvas.getGraphics(), bb);
				    		  } else {
				    			  bb.setColor(Color.black);
				    			  bb.fillRect(0, 0, width, height);
				    		  }
			    		  } else {
			    			  bb.setColor(Color.black);
			    			  bb.fillRect(0, 0, width, height);
			    		  }
			    		  
			    		  if (memobj != null) {
			    			  //Paint.blit(((Canvas) memobj).getGraphics(), bb);
			    		  }
			    		  

			    		  if (paint) {
			    			  Paint.draw(bb);
			    		  }
			    		 
			    		  bb.dispose();
			    		  
			    		  if (!terminate && !shell.isDisposed()) {
				    		  strategy.show();
				    		  
				    		  Toolkit.getDefaultToolkit().sync();
			    		  }
		    		  }
		    	  };
		      };
		      animationThread.start();
			}
			
			/*@Override
			public void paint(Graphics g) {
				super.paint(g);
				
				
				
				//g.fillRect(0, 0, 400, 400);
				Point playerp = new Point(Calculations.locationToScreen((int)Client.getMyPlayer().getData().getCenterLocation().getX(), (int)Client.getMyPlayer().getData().getCenterLocation().getY()));
				g.drawRect(playerp.x, playerp.y, 2, 2);
			}*/
		};
		
		//java.awt.Canvas icanvas = (java.awt.Canvas) Client.getCanvas().getIAccessor().getObject();
		//int width = Client.getCanvasWidth();
		//int height = Client.getCanvasHeight();
		int width = Frame.window.rs2app.getPreferredSize().width;
		int height = Frame.window.rs2app.getPreferredSize().height;
		
		final CustomCanvas canvas = new CustomCanvas();
		
		ccanvas = canvas;
		
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setVisible(true);
		
		if (ScriptHandler.redirect != null) {
			canvas.addMouseListener(ScriptHandler.redirect.getMouseListener());
			canvas.addMouseMotionListener(ScriptHandler.redirect.getMouseMotionListener());
			canvas.addKeyListener(ScriptHandler.redirect.getKeyListener());
		}
		
		// Create a panel which the applet will be inserted into; this is not affected by offsets which would be a problem in adding to a JFrame
		comp = new Composite(shell, SWT.EMBEDDED);
		comp.setBounds(0, toolBar.getBounds().height - 5, canvas.getPreferredSize().width, canvas.getPreferredSize().height);
		
		final java.awt.Frame frame = SWT_AWT.new_Frame(comp);
		
		Panel panel = new Panel();
		
		panel.setPreferredSize(canvas.getPreferredSize());
		panel.add(canvas);
		
		frame.add(panel);
		
		shell.pack();
		//shell.layout();
		
		toolBar.setSize(shell.getClientArea().width, 36);
		
		this.frame = frame;
		this.panel = panel;
		
		canvas.setBackground(Color.black);
		panel.setBackground(Color.black);
		canvas.setIgnoreRepaint(true);
		canvas.createBufferStrategy(2);
		canvas.strategy = canvas.getBufferStrategy();
		canvas.startAnimationThread();
	}

}