package org.rs3.debug;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Hashtable;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES1;
import javax.media.opengl.GL2GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLLightingFunc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.OpenGLRender;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.SDRender;
import org.rs3.boot.Frame;
import org.rs3.callbacks.capture.CanvasCapture;
import org.rs3.scripts.ScriptHandler;
import org.rs3.tools.ModelHashTableExplorer;
import org.rs3.tools.PathGenerator;
import org.rs3.tools.SettingsExplorer;
import org.rs3.tools.WidgetExplorer;

import com.jogamp.opengl.util.FPSAnimator;

public class DebugGL {

	public static DebugGL get = null;

	java.awt.Frame frame;
	Panel panel;
	Composite comp = null;
	FPSAnimator animator = null;
	GLCanvas glcanvas = null;
	public static GL2 gl2 = null;

	protected Shell shell;

	protected ToolBar toolBar;
	protected Menu dropMenu;

	public static volatile boolean paint = true;
	public static volatile boolean bitBlt = true;

	private volatile boolean close = false;
	private volatile boolean terminate = false;


	private static volatile int width = 0;
	private static volatile int height = 0;

	private static int texture;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DebugGL window = new DebugGL();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		createContents();
		shell.open();
		shell.layout();

		CanvasCapture.readPixels = true;
	}

	public synchronized void sendCloseSignal() {
		this.close = true;

		if (shell.isDisposed())
			return;

		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}

	private void close() {
		terminate = true;

		if (animator != null) {
			if (animator.stop()) // supposed to block until animation thread is finished
				animator = null;
		}

		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});

		dropMenu.dispose();
		shell.dispose(); // disposes GLCanvas

		CanvasCapture.readPixels = false;
		CanvasCapture.capturedPixels = null;

		System.out.println("DebugGL closed and disposed.");
	}

	static Object memobj = null;

	public void resize(int width, int height) {
		if (terminate)
			return;

		if (shell.isDisposed())
			return;

		shell.setSize(width, height);

		if (panel != null && comp != null) {
			toolBar.setSize(shell.getClientArea().width, toolBar.getBounds().height);
			glcanvas.setPreferredSize(new Dimension(shell.getClientArea().width, shell.getClientArea().height - toolBar.getBounds().height + 5));
			panel.setPreferredSize(glcanvas.getPreferredSize());
			comp.setBounds(0, toolBar.getBounds().height - 5, glcanvas.getPreferredSize().width, glcanvas.getPreferredSize().height);
			shell.pack();

			/*java.awt.Canvas icanvas = (java.awt.Canvas) Client.getCanvas().getIAccessor().getObject();
			width = icanvas.getWidth();
			height = icanvas.getHeight();*/

			width = glcanvas.getPreferredSize().width;
			height = glcanvas.getPreferredSize().height;
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(Frame.getDisplay());

		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				//event.doit = false;
				close = true;
				close();
			}
		});

		/*shell.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				resize();
			}
		});*/

		//shell.setSize(1024, 768);
		shell.setText("DebugGL");

		toolBar = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);
		toolBar.setSize(1024, 36);

		final ToolItem tltmPaint = new ToolItem(toolBar, SWT.CHECK);
		tltmPaint.setWidth(240);
		tltmPaint.setSelection(true);
		paint = tltmPaint.getSelection();
		tltmPaint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				paint = tltmPaint.getSelection();
			}
		});
		tltmPaint.setText("Paint");

		final ToolItem tltmBitBlt = new ToolItem(toolBar, SWT.CHECK);
		tltmBitBlt.setWidth(240);
		tltmBitBlt.setSelection(true);
		bitBlt = tltmBitBlt.getSelection();
		tltmBitBlt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				bitBlt = tltmBitBlt.getSelection();
				CanvasCapture.readPixels = bitBlt;
			}
		});
		tltmBitBlt.setText("BitBlt");

		final ToolItem tltmWidgetExplorer = new ToolItem(toolBar, SWT.NONE);
		tltmWidgetExplorer.setWidth(240);
		tltmWidgetExplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				try {
					WidgetExplorer window = new WidgetExplorer();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmWidgetExplorer.setText("WidgetExplorer");

		ToolItem tltmPathgenerator = new ToolItem(toolBar, SWT.NONE);
		tltmPathgenerator.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					PathGenerator window = new PathGenerator();
					window.open(glcanvas);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmPathgenerator.setText("PathGenerator");
		
		ToolItem tltmSettingsExplorer = new ToolItem(toolBar, SWT.NONE);
		tltmSettingsExplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					SettingsExplorer window = new SettingsExplorer();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmSettingsExplorer.setText("SettingsExplorer");
		
		ToolItem tltmModelhashtableexplorer = new ToolItem(toolBar, SWT.NONE);
		tltmModelhashtableexplorer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				try {
					ModelHashTableExplorer window = new ModelHashTableExplorer();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		tltmModelhashtableexplorer.setText("ModelHashTableExplorer");

		dropMenu = new Menu(shell, SWT.POP_UP);
		MenuItem item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Npcs");
		item.setSelection(Paint.DebugObject.NPCS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.NPCS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Ground Objects");
		item.setSelection(Paint.DebugObject.GROUND_OBJECTS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.GROUND_OBJECTS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Ground Items");
		item.setSelection(Paint.DebugObject.GROUND_ITEMS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.GROUND_ITEMS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Inventory");
		item.setSelection(Paint.DebugObject.INVENTORY.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.INVENTORY.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(dropMenu, SWT.CHECK);
		item.setText("Bank");
		item.setSelection(Paint.DebugObject.BANK.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.BANK.setPaint(menuItem.getSelection());
			}
		});
		
		final ToolItem tltmObjects = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmObjects.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.ARROW) {
	                // Position the menu below and vertically aligned with the the drop down tool button.
	                final ToolItem toolItem = (ToolItem) e.widget;
	                final ToolBar  toolBar = toolItem.getParent();

	                Point point = toolBar.toDisplay(new Point(e.x, e.y));
	                dropMenu.setLocation(point.x, point.y);
	                dropMenu.setVisible(true);
				}
			}
		});
		tltmObjects.setText("Objects");


		//java.awt.Canvas icanvas = (java.awt.Canvas) Client.getCanvas().getIAccessor().getObject();
		width = Client.getCanvasWidth();
		height = Client.getCanvasHeight();

		GLProfile glprofile = GLProfile.getDefault();
		GLCapabilities glcapabilities = new GLCapabilities( glprofile );
		final GLCanvas glcanvas = new GLCanvas( glcapabilities );
		this.glcanvas = glcanvas;

		if (ScriptHandler.redirect != null) {
			glcanvas.addMouseListener(ScriptHandler.redirect.getMouseListener());
			glcanvas.addMouseMotionListener(ScriptHandler.redirect.getMouseMotionListener());
			glcanvas.addKeyListener(ScriptHandler.redirect.getKeyListener());
		}

		final FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		this.animator = animator;
		//animator.add(glcanvas);
		animator.start();

		glcanvas.addGLEventListener( new GLEventListener() {

			@Override
			public void reshape( GLAutoDrawable glautodrawable, int x, int y, int width, int height ) {
				if (terminate)
					return;
				
				java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();
				glcanvas.setLocation(canvas.getLocation());
				
				Render render = Client.getRender(); // TODO: calling render can be cpu intensive every frame

				BufferedImage sdScreen = null; // screenshot below if in safe mode
				
				boolean dimensionsMatch = false;
				if (render instanceof OpenGLRender) {
					synchronized (CanvasCapture.capturedLock) {
						if (CanvasCapture.capturedPixels != null) {
							if (CanvasCapture.capturedWidth == DebugGL.width && CanvasCapture.capturedHeight == DebugGL.height)
								dimensionsMatch = true;
							else {
								width = CanvasCapture.capturedWidth;
								height = CanvasCapture.capturedHeight;
							}
						}
					}
				} else if (render instanceof SDRender) {
					sdScreen = getSafeModeScreen();
					
					if (sdScreen != null) {
						if (sdScreen.getWidth() == DebugGL.width && sdScreen.getHeight() == DebugGL.height)
							dimensionsMatch = true;
						else {
							width = sdScreen.getWidth();
							height = sdScreen.getHeight();
						}
					}
				}

				/*if (!dimensionsMatch) {
            		return;
            	}*/

            	GL2 gl2 = glautodrawable.getGL().getGL2();
            	DebugGL.gl2 = gl2;

            	Paint.setup( gl2, width, height );

            	final int[] tmp = new int[1];

            	gl2.glEnable(GL.GL_TEXTURE_2D);
            	gl2.glGenTextures(1, tmp, 0);
            	texture = tmp[0];

            	gl2.glBindTexture(GL.GL_TEXTURE_2D, texture);
            	
            	if (render instanceof OpenGLRender) {
	            	synchronized (CanvasCapture.capturedLock) {
	            		if (CanvasCapture.capturedPixels != null) {
	            			gl2.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, CanvasCapture.capturedWidth, CanvasCapture.capturedHeight, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, IntBuffer.wrap(CanvasCapture.capturedPixels));
	
	            			DebugGL.width = CanvasCapture.capturedWidth;
	            			DebugGL.height = CanvasCapture.capturedHeight;
	            		}
	            	}
            	} else if (render instanceof SDRender) {
            		if (sdScreen != null) {
	        			gl2.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, sdScreen.getWidth(), sdScreen.getHeight(), 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, convertImageData(sdScreen));
	        			
	        			DebugGL.width = sdScreen.getWidth();
	        			DebugGL.height = sdScreen.getHeight();
            		}
            	}

            	gl2.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
            	gl2.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);

            	gl2.glDisable(GL.GL_TEXTURE_2D);
			}

			@Override
			public void init( GLAutoDrawable glautodrawable ) {
				GL2 gl2 = glautodrawable.getGL().getGL2();
				DebugGL.gl2 = gl2;

				gl2.glDisable(GL.GL_CULL_FACE); // Enable Culling
				gl2.glDisable(GL.GL_DEPTH_TEST); // Enable Culling
				gl2.glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
				gl2.glDisable(GLLightingFunc.GL_LIGHTING);
		        gl2.glEnable(GL.GL_BLEND);
		        gl2.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
				gl2.glColor4f(1, 1, 1, 1);

				Paint.setup(gl2, width, height);
			}

			@Override
			public void dispose( GLAutoDrawable glautodrawable ) {
				GL2 gl2 = glautodrawable.getGL().getGL2();
				DebugGL.gl2 = gl2;

				int[] tmp = new int[1];
				tmp[0] = texture;
				gl2.glDeleteTextures(1, tmp, 0);

				System.out.println("GLCanvas disposed.");
			}

			@Override
			public void display( GLAutoDrawable glautodrawable ) {
				if (terminate)
					return;
				
				java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();
				glcanvas.setLocation(canvas.getLocation());

				Render render = Client.getRender(); // TODO: calling render can be cpu intensive every frame

				BufferedImage sdScreen = null; // screenshot below if in safe mode
				
				boolean dimensionsMatch = false;
				if (render instanceof OpenGLRender) {
					synchronized (CanvasCapture.capturedLock) {
						if (CanvasCapture.capturedPixels != null) {
							if (CanvasCapture.capturedWidth == DebugGL.width && CanvasCapture.capturedHeight == DebugGL.height)
								dimensionsMatch = true;
							else
								reshape(glautodrawable, 0, 0, CanvasCapture.capturedWidth, CanvasCapture.capturedHeight);
						}
					}
				} else if (render instanceof SDRender) {
					sdScreen = getSafeModeScreen();
					
					if (sdScreen != null) {
						if (sdScreen.getWidth() == DebugGL.width && sdScreen.getHeight() == DebugGL.height)
							dimensionsMatch = true;
						else
							reshape(glautodrawable, 0, 0, sdScreen.getWidth(), sdScreen.getHeight());
					}
				}

				GL2 gl2 = glautodrawable.getGL().getGL2();
				DebugGL.gl2 = gl2;

				if (dimensionsMatch && bitBlt) {
					gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
					gl2.glEnable(GL.GL_TEXTURE_2D);
					gl2.glBindTexture(GL.GL_TEXTURE_2D, texture);

					if (render instanceof OpenGLRender) {
						synchronized (CanvasCapture.capturedLock) {
							if (CanvasCapture.capturedPixels != null) {
								//gl2.glDrawPixels(width, height, GL2.GL_BGRA, GL2.GL_UNSIGNED_BYTE, IntBuffer.wrap(CanvasCapture.capturedPixels));
								//gl2.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, width, height, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, IntBuffer.wrap(CanvasCapture.capturedPixels));
								gl2.glTexSubImage2D(GL.GL_TEXTURE_2D, 0, 0, 0, CanvasCapture.capturedWidth, CanvasCapture.capturedHeight, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, IntBuffer.wrap(CanvasCapture.capturedPixels));
							}
						}
					} else if (render instanceof SDRender) {
	            		if (sdScreen != null) {
		        			//gl2.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, sdScreen.getWidth(), sdScreen.getHeight(), 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, convertImageData(sdScreen));
	            			gl2.glTexSubImage2D(GL.GL_TEXTURE_2D, 0, 0, 0, sdScreen.getWidth(), sdScreen.getHeight(), GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, convertImageData(sdScreen));
	            		}
					}

					//gl2.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
					//gl2.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);

					//gl2.glBindTexture(GL2.GL_TEXTURE_2D, texture);

					gl2.glColor4f(1, 1, 1, 1);

					gl2.glBegin(GL2GL3.GL_QUADS);
					gl2.glTexCoord2f(0.0f, 1.0f);
					gl2.glVertex2f(0.0f, 0.0f);
					gl2.glTexCoord2f(0.0f, 0.0f);
					gl2.glVertex2f(0, height);
					gl2.glTexCoord2f(1.0f, 0.0f);
					gl2.glVertex2f(width, height);
					gl2.glTexCoord2f(1.0f, 1.0f);
					gl2.glVertex2f(width, 0.0f);
					gl2.glFlush();
					gl2.glEnd();

					gl2.glDisable(GL.GL_TEXTURE_2D);
				} else
					gl2.glClear(GL.GL_COLOR_BUFFER_BIT);

				if (paint)
					Paint.render(gl2);

				//Toolkit.getDefaultToolkit().sync();
			}
		});

		//glcanvas.setPreferredSize(new Dimension(width, height));
		glcanvas.setPreferredSize(Frame.window.rs2app.getPreferredSize());
		glcanvas.setVisible(true);

		// Create a panel which the applet will be inserted into; this is not affected by offsets which would be a problem in adding to a JFrame
		comp = new Composite(shell, SWT.EMBEDDED);
		comp.setBounds(0, toolBar.getBounds().height - 5, glcanvas.getPreferredSize().width, glcanvas.getPreferredSize().height);

		final java.awt.Frame frame = SWT_AWT.new_Frame(comp);

		/*frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we){
            	animator.stop();
            	frame.remove(glcanvas);
                frame.dispose();
                //System.exit( 0 );
                CanvasCapture.readPixels = false;

                System.out.println("DebugGL GL frame disposed.");
			}
		});*/

		Panel panel = new Panel();

		panel.setPreferredSize(glcanvas.getPreferredSize());
		panel.add(glcanvas);
		panel.setBackground(Color.black);

		frame.add(panel);

		shell.pack();
		//shell.layout();
		
		toolBar.setSize(shell.getClientArea().width, 36);

		this.frame = frame;
		this.panel = panel;
	}
	
	// TODO: flip (actually, a different technique should be used since this is so slow)
	private BufferedImage getSafeModeScreen() {
    	java.awt.Canvas canvas = (java.awt.Canvas) Client.getCanvas().getObject();
    	
    	int width = Client.getCanvasWidth();
    	int height = Client.getCanvasHeight();
    	
    	BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // sun's jpg encoder/decoder doesn't support alpha, although using png will work
    	Graphics g = bi.getGraphics();
    	
    	Paint.blit(canvas.getGraphics(), g);
    	
    	return bi;
	}
	
	/**
	 * Convert the buffered image to a texture
	 * 
	 * Credits to Space Invaders example (https://stackoverflow.com/questions/5194325/how-do-i-load-an-image-for-use-as-an-opengl-texture-with-lwjgl -- Ron Romero)
	 */
	private ByteBuffer convertImageData(BufferedImage bufferedImage) {
	    ByteBuffer imageBuffer;
	    WritableRaster raster;
	    BufferedImage texImage;

	    ColorModel glAlphaColorModel = new ComponentColorModel(ColorSpace
	            .getInstance(ColorSpace.CS_sRGB), new int[] { 8, 8, 8, 8 },
	            true, false, Transparency.TRANSLUCENT, DataBuffer.TYPE_BYTE);

	    raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE,
	            bufferedImage.getWidth(), bufferedImage.getHeight(), 4, null);
	    texImage = new BufferedImage(glAlphaColorModel, raster, true,
	            new Hashtable());

	    // copy the source image into the produced image
	    Graphics g = texImage.getGraphics();
	    g.setColor(new Color(0f, 0f, 0f, 0f));
	    g.fillRect(0, 0, 256, 256);
	    g.drawImage(bufferedImage, 0, 0, null);

	    // build a byte buffer from the temporary image
	    // that be used by OpenGL to produce a texture.
	    byte[] data = ((DataBufferByte) texImage.getRaster().getDataBuffer())
	            .getData();

	    imageBuffer = ByteBuffer.allocateDirect(data.length);
	    imageBuffer.order(ByteOrder.nativeOrder());
	    imageBuffer.put(data, 0, data.length);
	    imageBuffer.flip();

	    return imageBuffer;
	}
}