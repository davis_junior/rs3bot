package org.rs3.remote;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

import org.rs3.boot.BootType;
import org.rs3.boot.Frame;
import org.rs3.boot.OfficialClientInject;
import org.rs3.database.AccountInfo;
import org.rs3.database.ServerInfo;
import org.rs3.database.ServerRow;
import org.rs3.scripts.ScriptHandler;

public class Server {
	
	public static boolean test = false;
	
	public static volatile boolean running = false;
	
	public static volatile boolean terminate = false;
	
	public static void main(String argv[]) throws InterruptedException {
		test = true;
		
		Thread serverThread = getNewThread();
		serverThread.start();
		serverThread.join();
      }
	
	public static Thread getNewThread() {
		Thread serverThread = new Thread() {
			@Override
			public void run() {
				if (running) {
					System.out.println("Server already running!");
					return;
				}
				
				running = true;
				terminate = false;
				
				ServerSocket servSock = null;
				int tryCount = 0;
				while (servSock == null && tryCount < 20) {
					try {
						servSock = new ServerSocket(6791 + tryCount);
					} catch (IOException e) {
						if (e instanceof BindException) {
							System.out.println("Server: port " + (6791 + tryCount) + " already in use.");
						}
					}
					
					tryCount++;
				}
				
				if (servSock == null) {
					System.out.println("ERROR: Server thread terminated! Unable to bind to port!");
					return;
				}
				
				System.out.println("Server: listening on port: " + servSock.getLocalPort());
				if (ServerInfo.cur != null) {
					ServerRow serverRow = ServerRow.select(ServerInfo.cur.getServerId());
					if (serverRow != null) {
						serverRow.setPort(servSock.getLocalPort());
						serverRow.executeUpdate();
					}
				}
				
				try {
			         while(!terminate) {
			            Socket conSock = servSock.accept();
			            
			            System.out.println("Server: Connected to " + conSock.getRemoteSocketAddress());
			            
			            BufferedReader input = new BufferedReader(new InputStreamReader(conSock.getInputStream()));
			            DataOutputStream output = new DataOutputStream(conSock.getOutputStream());
			            
			            String inputMsg = input.readLine();
			            System.out.println("Received: " + inputMsg);
			            
			            String response = "OK";
			            if (inputMsg.equals("::BLOCK::")) {
			            	System.out.println("Blocking input...");
			            	
			            	if (!test) {
			            		OfficialClientInject.blockingInput = true;
			            		ScriptHandler.setupInput(OfficialClientInject.rs2app, "");
			            		if (!OfficialClientInject.startBlocking())
			            			response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::UNBLOCK::")) {
			            	System.out.println("Unblocking input...");
			            	
			            	if (!test) {
			            		OfficialClientInject.blockingInput = false;
			            		ScriptHandler.setupInput(OfficialClientInject.rs2app, "");
				            	if (!OfficialClientInject.stopBlocking())
				            		response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::STOP::")) {
			            	System.out.println("Stopping script...");
			            	
			            	if (!test) {
			            		if (!OfficialClientInject.stopScript())
			            			response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::START::")) {
			            	System.out.println("Starting script...");
			            	
			            	if (!test) {
			            		OfficialClientInject.loadAccountInfo();
			            		if (!OfficialClientInject.loadScriptInfo() || !OfficialClientInject.startScript())
			            			response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::RELOADPARAMS::")) {
			            	System.out.println("Reloading script data params...");
			            	
			            	if (!test) {
			            		if (ScriptHandler.currentScript != null)
			            			ScriptHandler.currentScript.loadDataParams();
			            		else
			            			response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::RELOADACCOUNTINFO::")) {
			            	System.out.println("Reloading account info...");
			            	
			            	if (!test) {
			            		if (BootType.get().equals(BootType.NORMAL))
			            			Frame.window.loadAccountInfo();
			            		else if (BootType.get().equals(BootType.OFFICIAL_CLIENT))
			            			OfficialClientInject.loadAccountInfo();
			            		else
			            			response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::SCRIPTINFO::")) {
			            	if (!test) {
			            		if (ScriptHandler.currentScript != null)
			            			response = "INFO\n" + ScriptHandler.currentScript.getInfo();
			            		else
			            			response = "UNSUCCESSFUL";
			            	}
			            } else if (inputMsg.equals("::TERMINATE::")) {
			            	System.out.println("Terminating...");
			            	terminate = true;
			            } else
			            	response = "UNKNOWN";
			            
			            output.writeBytes(response + "\n");
			            output.flush();
			            conSock.close();
			         }
			         servSock.close();
			         
			         System.out.println("Successfully terminated server.");
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("ERROR: Server thread terminated!");
				}
				
				running = false;
				terminate = false;
			}
		};
		
		return serverThread;
	}
	

}