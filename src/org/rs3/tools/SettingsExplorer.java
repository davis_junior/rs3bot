package org.rs3.tools;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.SettingData;
import org.rs3.api.wrappers.Settings;
import org.rs3.boot.Frame;
import org.rs3.callbacks.SettingsCallback;
import org.rs3.util.Time;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class SettingsExplorer {

	protected Display display;
	protected Clipboard cb;
	protected Shell shell;
	
	private Tree tree;
	private org.eclipse.swt.widgets.List list;
	
	private volatile boolean close = false;
	private volatile boolean terminate = false;
	private Text text;
	private Text listenerText;
	private List settingsList;
	private List listenerList;
	
	public static Object lock = new Object();
	
	private boolean exclude = true;
	private Set<Integer> listenerFilters = new HashSet<>();
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			SettingsExplorer window = new SettingsExplorer();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		cb = new Clipboard(display);
		createContents();
		
		SettingsCallback.listener = this;
		synchronized (lock) {
			populateList();
		}
		
		shell.open();
		shell.layout();
	}
	
	public synchronized void sendCloseSignal() {
		this.close = true;
		
		if (shell.isDisposed())
			return;
		
		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}
	
	private void close() {
		synchronized (lock) {
			SettingsCallback.listener = null;
		}
		
		terminate = true;
		
		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		/*EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});*/
		
		shell.dispose();
		
		cb.dispose();
		
		System.out.println("SettingExplorer closed and disposed.");
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(Frame.getDisplay());
		
	    shell.addListener(SWT.Close, new Listener() {
	        @Override
			public void handleEvent(Event event) {
	        	//event.doit = false;
	        	close = true;
	        	close();
	        }
	      });
	    
		shell.setSize(706, 634);
		shell.setText("Settings Explorer");
		
		Group grpSettings = new Group(shell, SWT.NONE);
		grpSettings.setText("Settings");
		grpSettings.setBounds(10, 10, 192, 569);
		
		settingsList = new List(grpSettings, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		settingsList.setBounds(10, 23, 172, 504);
		settingsList.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// Enable copy to clipboard
				
				if (e.keyCode == 'c' && (e.stateMask & SWT.CTRL) != 0) {
					String[] selection = settingsList.getSelection();
					
					StringBuffer buffer = new StringBuffer();
					for (int i = 0; i < selection.length; i++) {
						buffer.append(selection[i]);
						
						if (i != selection.length - 1)
							buffer.append(System.getProperty("line.separator"));
					}
					
			        TextTransfer textTransfer = TextTransfer.getInstance();
			        cb.setContents(new Object[] { buffer.toString() },
			            new Transfer[] { textTransfer });
				}
			}
		});
		
		Label lblGoto = new Label(grpSettings, SWT.NONE);
		lblGoto.setBounds(10, 536, 39, 20);
		lblGoto.setText("Goto:");
		
		text = new Text(grpSettings, SWT.BORDER);
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				try {
					int index = Integer.parseInt(text.getText());
					
					if (index >= 0 && index < settingsList.getItemCount()) {
						settingsList.deselectAll();
						settingsList.select(index);
						settingsList.showSelection();
					}
				} catch (NumberFormatException nfe) {
					//nfe.printStackTrace();
				}
			}
		});
		text.setBounds(55, 533, 127, 26);
		
		Group grpListener = new Group(shell, SWT.NONE);
		grpListener.setText("Listener");
		grpListener.setBounds(208, 10, 470, 569);
		
		listenerList = new List(grpListener, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		listenerList.setBounds(10, 23, 450, 471);
		listenerList.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// Enable copy to clipboard
				
				if (e.keyCode == 'c' && (e.stateMask & SWT.CTRL) != 0) {
					String[] selection = listenerList.getSelection();
					
					StringBuffer buffer = new StringBuffer();
					for (int i = 0; i < selection.length; i++) {
						buffer.append(selection[i]);
						
						if (i != selection.length - 1)
							buffer.append(System.getProperty("line.separator"));
					}
					
			        TextTransfer textTransfer = TextTransfer.getInstance();
			        cb.setContents(new Object[] { buffer.toString() },
			            new Transfer[] { textTransfer });
				}
			}
		});
		
		Button btnClear = new Button(grpListener, SWT.NONE);
		btnClear.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				listenerList.removeAll();
			}
		});
		btnClear.setBounds(10, 532, 450, 30);
		btnClear.setText("Clear");
		
		Button btnInclude = new Button(grpListener, SWT.RADIO);
		btnInclude.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				exclude = false;
			}
		});
		btnInclude.setBounds(10, 506, 72, 20);
		btnInclude.setText("Include");
		
		Button btnExclude = new Button(grpListener, SWT.RADIO);
		btnExclude.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				exclude = true;
			}
		});
		btnExclude.setSelection(true);
		btnExclude.setBounds(89, 506, 75, 20);
		btnExclude.setText("Exclude");
		
		if (btnExclude.getSelection())
			exclude = true;
		else if (btnInclude.getSelection())
			exclude = false;
		else
			exclude = true;
		
		listenerText = new Text(grpListener, SWT.BORDER);
		listenerText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				String text = listenerText.getText();
				
				if (text == null || text.trim().isEmpty()) {
					listenerFilters.clear();
				} else {
					listenerFilters.clear();
					
					for (String part : text.split(",")) {
						part = part.trim();
						
						try {
							int index = Integer.parseInt(part);
							listenerFilters.add(index);
						} catch (NumberFormatException nfe) {
							//nfe.printStackTrace();
						}
					}
				}
			}
		});
		listenerText.setBounds(170, 500, 290, 26);
	}
	
	private void populateList() {
		SettingData settingData = Client.getSettingData();
		
		if (settingData != null) {
			Settings settings = settingData.getSettings();
			
			if (settings != null) {
				int[] data = settings.getData();
				
				if (data != null) {
					data = data.clone();
					
					settingsList.removeAll();
					
					for (int i = 0; i < data.length; i++) {
						settingsList.setData(Integer.toString(i), data[i]);
						settingsList.add(i + ": " + data[i]);
					}
				}
			}
		}
	}
	
	// used as a listener to SettingsCallback
	public synchronized void changeSetting(int index, int value) {
		if (index >= 0 && index < settingsList.getItemCount()) {
			//String oldItemText = settingsList.getItem(index);
			
			//int oldValue = Integer.parseInt(oldItemText.substring(oldItemText.indexOf(":") + 1).trim());
			int oldValue = (int) settingsList.getData(Integer.toString(index));
			settingsList.setData(Integer.toString(index), value);
			settingsList.setItem(index, index + ": " + value);
			
			
			boolean append;
			
			if (exclude) {
				append = true;
				
				for (int i : listenerFilters) {
					if (index == i) {
						append = false;
						break;
					}
				}
			} else {
				append = false;
				
				for (int i : listenerFilters) {
					if (index == i) {
						append = true;
						break;
					}
				}
			}
			
			if (append) {
				listenerList.add("(" + Time.format(System.currentTimeMillis()) + ") [" + index + "]: " + oldValue + " -> " + value);
				listenerList.setTopIndex(listenerList.getItemCount() - 1);
			}
		}
	}
}
