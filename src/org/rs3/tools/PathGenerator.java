package org.rs3.tools;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.Tile;
import org.rs3.boot.Frame;
import org.rs3.debug.Paint;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class PathGenerator implements MouseListener, MouseMotionListener {
	
	Component debugFrameCanvas;
	
	protected Clipboard cb;
	protected Shell shell;
	
	Text txtArea;
	Button radioArea;
	Button radioTilepath;
	Button radioTileArray;
	Button radioTile;
	
	private volatile boolean close = false;
	private volatile boolean terminate = false;
	
	private String tileString = null;
	private String selectedTileString = null;
	public boolean allowDuplicates = false;
	public boolean includeDeclaration = false;
	
	
	List<Tile> tiles = new ArrayList<>();
	private Table table;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			PathGenerator window = new PathGenerator();
			window.open(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open(Component debugFrameCanvas) {
		this.debugFrameCanvas = debugFrameCanvas;
		
		cb = new Clipboard(Frame.getDisplay());
		createContents();
		
		shell.open();
		shell.layout();
		
		
		debugFrameCanvas.addMouseListener(this);
		debugFrameCanvas.addMouseMotionListener(this);
		//debugFrameCanvas.addKeyListener(this);
		
		tiles.clear();
		Paint.tiles = null;
		
		// reload tiles
		/*Tile[] tiles = new Tile[] { new Tile(3043, 3258, 0), new Tile(3043, 3257, 0), new Tile(3043, 3256, 0), new Tile(3043, 3255, 0), new Tile(3043, 3254, 0), new Tile(3044, 3254, 0), new Tile(3044, 3253, 0), new Tile(3044, 3252, 0), new Tile(3044, 3251, 0), new Tile(3043, 3251, 0), new Tile(3043, 3250, 0), new Tile(3043, 3249, 0), new Tile(3044, 3249, 0), new Tile(3044, 3248, 0), new Tile(3044, 3247, 0), new Tile(3044, 3246, 0), new Tile(3044, 3245, 0), new Tile(3043, 3245, 0), new Tile(3043, 3244, 0), new Tile(3043, 3243, 0), new Tile(3043, 3242, 0), new Tile(3043, 3241, 0), new Tile(3043, 3240, 0), new Tile(3043, 3239, 0), new Tile(3043, 3238, 0), new Tile(3043, 3237, 0), new Tile(3044, 3237, 0), new Tile(3045, 3237, 0), new Tile(3046, 3237, 0), new Tile(3047, 3237, 0), new Tile(3048, 3237, 0), new Tile(3049, 3237, 0), new Tile(3049, 3236, 0), new Tile(3049, 3235, 0), new Tile(3049, 3234, 0) };
		for (Tile tile : tiles)
			this.tiles.add(tile);
		updateAll();*/
	}
	
	public synchronized boolean shouldClose() {
		return close;
	}
	
	public synchronized void sendCloseSignal() {
		this.close = true;
		
		if (shell.isDisposed())
			return;
		
		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}
	
	private void close() {
		terminate = true;
		
		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		/*EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});*/
		
		shell.dispose();
		
		cb.dispose();
		
		System.out.println("PathGeneratorExplorer closed and disposed.");
		
		
		debugFrameCanvas.removeMouseListener(this);
		debugFrameCanvas.removeMouseMotionListener(this);
		//debugFrameCanvas.removeKeyListener(this);
		
		tiles.clear();
		Paint.tiles = null;
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(Frame.getDisplay());
		
	    shell.addListener(SWT.Close, new Listener() {
	        @Override
			public void handleEvent(Event event) {
	        	//event.doit = false;
	        	close = true;
	        	close();
	        }
	      });
	    
		shell.setSize(548, 575);
		shell.setText("Path Generator");
		
		Group grpType = new Group(shell, SWT.NONE);
		grpType.setText("Type");
		grpType.setBounds(0, 350, 103, 126);
		
		radioArea = new Button(grpType, SWT.RADIO);
		radioArea.setSelection(true);
		radioArea.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateTextBox();
			}
		});
		radioArea.setBounds(10, 23, 75, 20);
		radioArea.setText("Area");
		
		radioTilepath = new Button(grpType, SWT.RADIO);
		radioTilepath.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateTextBox();
			}
		});
		radioTilepath.setBounds(10, 49, 83, 20);
		radioTilepath.setText("TilePath");
		
		radioTileArray = new Button(grpType, SWT.RADIO);
		radioTileArray.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateTextBox();
			}
		});
		radioTileArray.setBounds(10, 75, 75, 20);
		radioTileArray.setText("Tile[]");
		
		radioTile = new Button(grpType, SWT.RADIO);
		radioTile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateTextBox();
			}
		});
		radioTile.setBounds(10, 101, 83, 20);
		radioTile.setText("Tile");
		
		txtArea = new Text(shell, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP | SWT.BORDER);
		txtArea.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		txtArea.setText("new Area();");
		txtArea.setBounds(109, 350, 401, 126);
		
		Button btnClear = new Button(shell, SWT.NONE);
		btnClear.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearTiles();
			}
		});
		btnClear.setBounds(157, 482, 90, 46);
		btnClear.setText("Clear");
		
		Button btnUndo = new Button(shell, SWT.NONE);
		btnUndo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				undoLastTile();
			}
		});
		btnUndo.setBounds(253, 482, 257, 46);
		btnUndo.setText("Undo");
		
		final Button btnAllowDuplicates = new Button(shell, SWT.CHECK);
		btnAllowDuplicates.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				allowDuplicates = btnAllowDuplicates.getSelection();
			}
		});
		btnAllowDuplicates.setBounds(0, 482, 134, 20);
		btnAllowDuplicates.setText("Allow duplicates");
		
		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem[] items = table.getSelection();
				if (items != null && items.length > 0) {
					Paint.updatePaintObject((Tile)items[0].getData());
				} else
					Paint.tile = null;
			}
		});
		table.setBounds(10, 10, 356, 334);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnIndex = new TableColumn(table, SWT.NONE);
		tblclmnIndex.setWidth(55);
		tblclmnIndex.setText("Index");
		
		TableColumn tblclmnX = new TableColumn(table, SWT.NONE);
		tblclmnX.setWidth(120);
		tblclmnX.setText("X");
		
		TableColumn tblclmnY = new TableColumn(table, SWT.NONE);
		tblclmnY.setWidth(120);
		tblclmnY.setText("Y");
		
		TableColumn tblclmnPlane = new TableColumn(table, SWT.NONE);
		tblclmnPlane.setWidth(57);
		tblclmnPlane.setText("Plane");
		
		// credits to http://bingjava.appspot.com/snippet.jsp?id=1340
		final TableEditor editor = new TableEditor (table);
		
		final Button btnIncludeDeclaration = new Button(shell, SWT.CHECK);
		btnIncludeDeclaration.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				includeDeclaration = btnIncludeDeclaration.getSelection();
				updateAllText();
			}
		});
		btnIncludeDeclaration.setBounds(0, 508, 151, 20);
		btnIncludeDeclaration.setText("Include declaration");
		
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;
		table.addListener (SWT.Selection, new Listener () {
			@Override
			public void handleEvent (Event event) {
				if (radioTile.getSelection())
					updateAllText();
			}
		});
		table.addListener (SWT.MouseDown, new Listener () {
			public void handleEvent (Event event) {
				Rectangle clientArea = table.getClientArea ();
				org.eclipse.swt.graphics.Point pt = new org.eclipse.swt.graphics.Point(event.x, event.y);
				int index = table.getTopIndex();
				
				while (index < table.getItemCount()) {
					boolean visible = false;
					final TableItem item = table.getItem(index);
					
					for (int i = 1; i < table.getColumnCount(); i++) { // skip first column
						Rectangle rect = item.getBounds(i);
						if (rect.contains(pt)) {
							final int column = i;
							final Text text = new Text(table, SWT.NONE);
							
							Listener textListener = new Listener() {
								public void handleEvent(final Event e) {
									Tile tile;
									int num = -1;
									
									try {
										num = Integer.parseInt(text.getText());
									} catch (NumberFormatException nfe) { /*ignored*/ }
									
									switch (e.type) {
										case SWT.FocusOut:
											if (num != -1) {
												tile = (Tile) item.getData();
												
												item.setText(column, text.getText());
												switch (column) {
												case 1:
													tile.x = num;
													break;
												case 2:
													tile.y = num;
													break;
												case 3:
													tile.plane = num;
													break;
												}
												
												updateAllText();
												updatePaint();
											}
											
											text.dispose();
											break;
										case SWT.Traverse:
											switch (e.detail) {
												case SWT.TRAVERSE_RETURN:
													if (num != -1) {
														tile = (Tile) item.getData();
														
														item.setText(column, text.getText());
														switch (column) {
														case 1:
															tile.x = num;
															break;
														case 2:
															tile.y = num;
															break;
														case 3:
															tile.plane = num;
															break;
														}
														
														updateAllText();
														updatePaint();
													}
													//FALL THROUGH
												case SWT.TRAVERSE_ESCAPE:
													text.dispose();
													e.doit = false;
											}
											break;
									}
								}
							};
							
							text.addListener(SWT.FocusOut, textListener);
							text.addListener(SWT.Traverse, textListener);
							editor.setEditor(text, item, i);
							text.setText (item.getText(i));
							text.selectAll();
							text.setFocus();
							return;
						}
						if (!visible && rect.intersects(clientArea))
							visible = true;
					}
					if (!visible)
						return;
					index++;
				}
			}
		});
	}
	
	
	private void updateTextBox() {
		if (tileString == null)
			tileString = "";
		
		if (selectedTileString == null)
			selectedTileString = "";
		
		String prefix = "";
		String suffix = "";
		
		if (radioArea.getSelection()) {
			if (includeDeclaration)
				prefix = "Area area = ";
			
			prefix += "new Area( ";
			suffix = " );";
		} else if (radioTilepath.getSelection()) {
			if (includeDeclaration)
				prefix = "TilePath tilePath = ";
			
			prefix += "new TilePath( ";
			suffix = " );";
		} else if (radioTileArray.getSelection()) {
			if (includeDeclaration)
				prefix = "Tile[] tiles = ";
			
			prefix += "new Tile[] { ";
			suffix = " };";
		} else if (radioTile.getSelection()) {
			if (includeDeclaration)
				prefix = "Tile tile = ";
		}
		
		if (!radioTile.getSelection())
			txtArea.setText(prefix + tileString + suffix);
		else
			txtArea.setText(prefix + selectedTileString + suffix);
	}

	
	private boolean containsTile(Tile check) {
		if (check != null && tiles.size() > 0) {
			for (Tile tile : tiles) {
				if (check.equals(tile))
					return true;
			}
		}
		
		return false;
	}
	
	private void addTile(Point point) {
		final Tile nearest = Tile.getNearestTile(point);
		
		if (nearest != null) {
			//System.out.println(nearest.getX() + ", " + nearest.getY());
			
			if (allowDuplicates || !containsTile(nearest)) {
				tiles.add(nearest);
				
				Frame.getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
						TableItem item = new TableItem(table, SWT.NONE);
						item.setData(nearest);
						item.setText(new String[] {
								"" + table.getItemCount(),
								"" + nearest.getX(),
								"" + nearest.getY(),
								"" + nearest.getPlane()
						});
						table.setSelection(item);
						updateAllText();
						updatePaint();
				    }
				});
			}
		}
	}
	
	public void undoLastTile() {
		if (tiles.size() > 0) {
			tiles.remove(tiles.size() - 1);
			
			Frame.getDisplay().asyncExec(new Runnable() {
			    @Override
				public void run() {
			    	table.remove(table.getItemCount() - 1);
			    	if (table.getSelectionCount() == 0 && table.getItemCount() > 0)
			    		table.setSelection(table.getItemCount() - 1);
			    	updateAllText();
			    	updatePaint();
			    }
			});
		}
	}
	
	public void clearTiles() {
		if (tiles.size() > 0) {
			tiles.clear();
			
			Frame.getDisplay().asyncExec(new Runnable() {
			    @Override
				public void run() {
			    	table.removeAll();
			    	updateAllText();
			    	updatePaint();
			    }
			});
		}
	}
	
	// does not include type (Area, TilePath, etc.)
	private String getTilesString() {
		if (tiles.size() > 0) {
			Tile[] array = tiles.toArray(new Tile[0]);
			
			StringBuilder sb = new StringBuilder();
			//sb.append("Area area = new Area(");
			
			for (int i = 0; i < array.length; i++) {
				sb.append("new Tile(");
				sb.append(array[i].getX() + ", ");
				sb.append(array[i].getY() + ", ");
				sb.append(array[i].getPlane() + ")");
				
				if (i != array.length - 1)
					sb.append(", ");
			}
			
			//sb.append(");");
			
			return sb.toString();
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return last selected tile or last tile if there is no selection
	 */
	private String getSelectedTilesString() {
		if (tiles.size() > 0) {
			Tile tile = null;
			TableItem[] selections = table.getSelection();
			if (selections != null && selections.length > 0) {
				TableItem item = selections[selections.length - 1];
				if (item != null) {
					tile = (Tile) item.getData();
				}
			}
			
			if (tile == null)
				tile = tiles.get(tiles.size() - 1);
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("new Tile(");
			sb.append(tile.getX() + ", ");
			sb.append(tile.getY() + ", ");
			sb.append(tile.getPlane() + ")");
			
			return sb.toString();
		}
		
		return null;
	}
	
	private void rebuildTable() {
		table.removeAll();
		
		if (tiles.size() > 0) {
			Tile[] array = tiles.toArray(new Tile[0]);
			
			for (int i = 0; i < array.length; i++) {
				TableItem item = new TableItem(table, SWT.NONE);
				item.setData(array[i]);
				item.setText(new String[] {
						"" + i,
						"" + array[i].getX(),
						"" + array[i].getY(),
						"" + array[i].getPlane()
				});
			}
		}
	}
	
	private void updateAllText() {
		Frame.getDisplay().asyncExec(new Runnable() {
		    @Override
			public void run() {
				tileString = getTilesString();
				selectedTileString = getSelectedTilesString();
				updateTextBox();
		    }
		});
	}
	
	private void updatePaint() {
		if (tiles.size() > 0) {
			Frame.getDisplay().asyncExec(new Runnable() {
			    @Override
				public void run() {
			    	if (tiles.size() == 2 && radioArea.getSelection())
			    		Paint.tiles = new Area(tiles.get(0), tiles.get(1)).getBoundingTiles();
			    	else
			    		Paint.tiles = tiles.toArray(new Tile[0]);
			    	
					TableItem[] items = table.getSelection();
					if (items != null && items.length > 0) {
						Paint.updatePaintObject((Tile)items[0].getData());
					} else
						Paint.tile = null;
			    }
			});
		} else {
			Paint.tiles = null;
			Paint.tile = null;
		}
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//System.out.println(e.getX());
		
		int x = e.getX();
		int y = e.getY();
		
		Point point = new Point(x, y);
		addTile(point);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		//System.out.println(e.getX());
		
		int x = e.getX();
		int y = e.getY();
		
		Point point = new Point(x, y);
		addTile(point);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
