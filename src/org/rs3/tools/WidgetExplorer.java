package org.rs3.tools;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.rs3.api.Interfaces;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Widget;
import org.rs3.boot.Frame;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;

public class WidgetExplorer {

	protected Display display;
	protected Clipboard cb;
	protected Shell shell;
	
	private Tree tree;
	private org.eclipse.swt.widgets.List list;
	
	private boolean draw1stLevelChildren = false;
	private boolean draw2ndLevelChildren = false;
	private boolean drawInvisible = false;
	private boolean useCaches = true;
	
	private volatile boolean close = false;
	private volatile boolean terminate = false;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			WidgetExplorer window = new WidgetExplorer();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		cb = new Clipboard(display);
		createContents();
		
		refreshWidgets();
		
		shell.open();
		shell.layout();
	}
	
	public synchronized void sendCloseSignal() {
		this.close = true;
		
		if (shell.isDisposed())
			return;
		
		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}
	
	private void close() {
		terminate = true;
		
		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		/*EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});*/
		
		shell.dispose();
		
		cb.dispose();
		
		System.out.println("WidgetExplorer closed and disposed.");
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(Frame.getDisplay());
		
	    shell.addListener(SWT.Close, new Listener() {
	        @Override
			public void handleEvent(Event event) {
	        	//event.doit = false;
	        	close = true;
	        	close();
	        }
	      });
	    
		shell.setSize(547, 539);
		shell.setText("Widget Explorer");
		
		tree = new Tree(shell, SWT.MULTI | SWT.BORDER);
		tree.setBounds(0, 62, 195, 422);
		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Tree tree = (Tree) e.widget;
				
				TreeItem[] items = tree.getSelection();
				
				if (items == null)
					return;
				
				for (TreeItem item : items) {
					Widget widget = (Widget) item.getData("widget");
					if (widget != null) {
						list.removeAll();
					}
					
					Component parentChild = (Component) item.getData("child");
					if (parentChild != null) {
						list.removeAll();
						
						int id = parentChild.getId();
						final int x = id >> 16;
						final int y = id & 0xffff;
						
						if (item.getData("isChild2") == null)
							list.add("Index: " + x + ", " + y);
						else
							list.add("Index: " + x + ", " + y + ", " + item.getText());
						
						list.add("Id: " + id);
						
						list.add("Bounds array index: " + parentChild.getBoundsArrayIndex());
						
						Array1D<Component> children = parentChild.getComponents();
						if (children != null)
							list.add("Children count: " + children.getLength());
						else
							list.add("Children count: " + 0);
						
						Point absoluteLocation = parentChild.getAbsoluteLocation();
						if (absoluteLocation != null)
							list.add("Absolute location: (" + absoluteLocation.x + ", " + absoluteLocation.y + ")");
						else
							list.add("Absolute location: null");
						list.add("Relative location: (" + parentChild.getX() + ", " + parentChild.getY() + ")");
						list.add("Width: " + parentChild.getWidth());
						list.add("Height: " + parentChild.getHeight());
						list.add("API Visible: " + parentChild.isVisible());
						list.add("_Visible: " + parentChild._isVisible());
						list.add("_Hidden: " + parentChild._isHidden());
						list.add("Type: " + parentChild.getType());
						list.add("Component id: " + parentChild.getComponentId());
						list.add("Actions: " + ArrayUtils.arrayToString(parentChild.getActions()));
						list.add("Tooltip: " + parentChild.getTooltip());
						list.add("Component name: " + parentChild.getComponentName());
						list.add("Text: " + parentChild.getText());
					}
				}
				
				updateRectangles(items);
			}
		});
		
		list = new org.eclipse.swt.widgets.List(shell, SWT.BORDER);
		list.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// Enable copy to clipboard
				
				if (e.keyCode == 'c' && (e.stateMask & SWT.CTRL) != 0) {
					String[] selection = list.getSelection();
					
					StringBuffer buffer = new StringBuffer();
					for (int i = 0; i < selection.length; i++) {
						buffer.append(selection[i]);
						
						if (i != selection.length - 1)
							buffer.append(System.getProperty("line.separator"));
					}
					
			        TextTransfer textTransfer = TextTransfer.getInstance();
			        cb.setContents(new Object[] { buffer.toString() },
			            new Transfer[] { textTransfer });
				}
			}
		});
		list.setBounds(201, 62, 318, 422);
		
		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				refreshWidgets();
			}
		});
		btnNewButton.setBounds(0, 26, 195, 30);
		btnNewButton.setText("Refresh");
		
		Button btnValidatedOnly = new Button(shell, SWT.CHECK);
		btnValidatedOnly.setSelection(true);
		btnValidatedOnly.setBounds(10, 0, 118, 20);
		btnValidatedOnly.setText("Validated only");
		
		Group grpDrawChildren = new Group(shell, SWT.NONE);
		grpDrawChildren.setText("Draw children");
		grpDrawChildren.setBounds(201, 0, 188, 56);
		
		final Button btnDraw2ndChildren = new Button(grpDrawChildren, SWT.CHECK);
		btnDraw2ndChildren.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button button = (Button) e.widget;
				draw2ndLevelChildren = button.getSelection();
				updateRectangles(tree.getSelection());
			}
		});
		btnDraw2ndChildren.setSelection(true);
		draw2ndLevelChildren = btnDraw2ndChildren.getSelection();
		btnDraw2ndChildren.setBounds(98, 26, 82, 20);
		btnDraw2ndChildren.setText("2nd level");
		
		Button btnDraw1stChildren = new Button(grpDrawChildren, SWT.CHECK);
		btnDraw1stChildren.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button button = (Button) e.widget;
				draw1stLevelChildren = button.getSelection();
				
				if (!draw1stLevelChildren)
					btnDraw2ndChildren.setEnabled(false);
				else
					btnDraw2ndChildren.setEnabled(true);
				
				updateRectangles(tree.getSelection());
			}
		});
		btnDraw1stChildren.setSelection(true);
		draw1stLevelChildren = btnDraw1stChildren.getSelection();
		btnDraw1stChildren.setBounds(10, 26, 82, 20);
		btnDraw1stChildren.setText("1st level");
		
		Button btnDrawInvisible = new Button(shell, SWT.CHECK);
		btnDrawInvisible.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button button = (Button) e.widget;
				drawInvisible = button.getSelection();
				updateRectangles(tree.getSelection());
			}
		});
		btnDrawInvisible.setSelection(false);
		drawInvisible = btnDrawInvisible.getSelection();
		btnDrawInvisible.setBounds(395, 36, 111, 20);
		btnDrawInvisible.setText("Draw invisible");
		
		Button btnUseCaches = new Button(shell, SWT.CHECK);
		btnUseCaches.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button button = (Button) e.widget;
				useCaches = button.getSelection();
				updateRectangles(tree.getSelection());
			}
		});
		btnUseCaches.setSelection(true);
		useCaches = btnUseCaches.getSelection();
		btnUseCaches.setBounds(395, 10, 111, 20);
		btnUseCaches.setText("Use Caches");
	}
	
	public void updateRectangles(TreeItem[] items) {
		if (items == null)
			return;
		
		boolean orig_useCachedComponentNodes = Interfaces.useCachedComponentNodes;
		boolean orig_useWidgetComponentCache = Interfaces.useWidgetComponentCache;
		
		Interfaces.useCachedComponentNodes = useCaches;
		Interfaces.useWidgetComponentCache = useCaches;
		if (useCaches) {
			Interfaces.updateCache_componentNodes();
			Interfaces.updateCache_widgetCache();
		}
		
		List<Rectangle> rects = new ArrayList<>();
		
		for (TreeItem item : items) {
			Widget widget = (Widget) item.getData("widget");
			if (widget != null) {
				// draw 1st level children
				if (draw1stLevelChildren) {
					for (TreeItem childItem : item.getItems()) {
						Component child = (Component) childItem.getData("child");
						
						if (child != null && (drawInvisible || child.isVisible())) {
							Point loc = child.getAbsoluteLocation();
							if (loc != null)
								rects.add(new Rectangle(loc.x, loc.y, child.getWidth(), child.getHeight()));
						}
						
						if (child != null) {
							// draw 2nd level children
							if (draw2ndLevelChildren) {
								for (TreeItem childItem2 : childItem.getItems()) {
									Component child2 = (Component) childItem2.getData("child");
									
									if (child2 != null && (drawInvisible || child2.isVisible())) {
										Point loc = child2.getAbsoluteLocation();
										if (loc != null)
											rects.add(new Rectangle(loc.x, loc.y, child2.getWidth(), child2.getHeight()));
									}
								}
							}
						}
					}
				}
			}
			
			Component parentChild = (Component) item.getData("child");
			if (parentChild != null) {
				// draw selected component
				if ((drawInvisible || parentChild.isVisible())) {
					Point loc = parentChild.getAbsoluteLocation();
					rects.add(new Rectangle(loc.x, loc.y, parentChild.getWidth(), parentChild.getHeight()));
				}
				
				// draw 1st level children
				if (draw1stLevelChildren) {
					TreeItem[] childItems = item.getItems();
					
					if (childItems != null) {
						for (TreeItem childItem : childItems) {
							Component child = (Component) childItem.getData("child");
							
							if (child != null && (drawInvisible || child.isVisible())) {
								Point loc = child.getAbsoluteLocation();
								rects.add(new Rectangle(loc.x, loc.y, child.getWidth(), child.getHeight()));
							}
							
							if (child != null) {
								// draw 2nd level children
								if (draw2ndLevelChildren) {
									TreeItem[] childItems2 = childItem.getItems();
									
									if (childItems2 != null) {
										for (TreeItem childItem2 : childItems2) {
											Component child2 = (Component) childItem2.getData("child");
											
											if (child2 != null && (drawInvisible || child2.isVisible())) {
												Point loc = child2.getAbsoluteLocation();
												rects.add(new Rectangle(loc.x, loc.y, child2.getWidth(), child2.getHeight()));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		Rectangle[] array = rects.toArray(new Rectangle[0]);
		
		if (Paint.components == null)
			Paint.components = array;
		
		if (Paint.components != null) {
			synchronized (Paint.components) {
				Paint.components = array;
			}
		}
		
		Interfaces.useCachedComponentNodes = orig_useCachedComponentNodes;
		Interfaces.useWidgetComponentCache = orig_useWidgetComponentCache;
	}
	
	public void refreshWidgets() {
		tree.removeAll();
		list.removeAll();
		
		boolean orig_useCachedComponentNodes = Interfaces.useCachedComponentNodes;
		boolean orig_useWidgetComponentCache = Interfaces.useWidgetComponentCache;
		
		Interfaces.useCachedComponentNodes = true;
		Interfaces.useWidgetComponentCache = true;
		Interfaces.updateCache_componentNodes();
		Interfaces.updateCache_widgetCache();
		
		Widget[] widgets = Interfaces.cache_widgetCache;
		
		for (int widgeti = 0; widgeti < widgets.length; widgeti++) {
			if (widgets[widgeti] != null && widgets[widgeti].validate(widgeti)) {
				
				Array1D<Component> children = widgets[widgeti].getComponents();
				if (children != null) {
					for (Component child : children.getAll()) {
						if (child != null && child.validate()) {
							
							int id = child.getId();
							final int x = id >> 16;
							final int y = id & 0xffff;
							
							TreeItem xItem = null;
							for (TreeItem item : tree.getItems()) {
								if (item.getText().equals(Integer.toString(x))) {
									xItem = item;
									break;
								}
							}
							
							if (xItem == null)
								xItem = new TreeItem(tree, SWT.NONE);
							
							TreeItem yItem = new TreeItem(xItem, SWT.NONE);
							
							xItem.setText(Integer.toString(x));
							yItem.setText(Integer.toString(y));
							
							xItem.setData("widget", widgets[widgeti]);
							yItem.setData("child", child);
							
							
							// 2nd level children
							Array1D<Component> children2 = child.getComponents();
							if (children2 != null) {
								for (int i = 0; i < children2.getLength(); i++) {
									Component child2 = children2.get(i);
									
									if (child2 != null && child2.validate()) {
										//int id2 = child2.getId();
										//final int x2 = id2 >> 16;
										//final int y2 = id2 & 0xffff;
										
										TreeItem iItem = new TreeItem(yItem, SWT.NONE);
										iItem.setText(Integer.toString(i));
										iItem.setData("child", child2);
										iItem.setData("isChild2", true);
									}
								}
							}
						}
					}
				}
			}
		}
		
		Interfaces.useCachedComponentNodes = orig_useCachedComponentNodes;
		Interfaces.useWidgetComponentCache = orig_useWidgetComponentCache;
	}
}
