package org.rs3.tools;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.rs3.accessors.CustomClassLoader;
import org.rs3.accessors.Reflection;
import org.rs3.boot.Frame;

public class ReflectionExplorer {

	protected Display display;
	protected Clipboard cb;
	protected Shell shlReflectionExplorer;
	
	private Tree tree;
	private org.eclipse.swt.widgets.List list;
	
	private volatile boolean close = false;
	private volatile boolean terminate = false;
	private Text text;
	
	private Object curStaticObject = null;
	private String curStaticObjectClassName = null;
	private String curStaticObjectFieldName = null;
	private Table table;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ReflectionExplorer window = new ReflectionExplorer();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		cb = new Clipboard(display);
		createContents();
		
		shlReflectionExplorer.open();
		shlReflectionExplorer.layout();
	}
	
	public synchronized void sendCloseSignal() {
		this.close = true;
		
		if (shlReflectionExplorer.isDisposed())
			return;
		
		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}
	
	private void close() {	
		terminate = true;
		
		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		/*EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});*/
		
		shlReflectionExplorer.dispose();
		
		cb.dispose();
		
		System.out.println("ReflectionExplorer closed and disposed.");
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlReflectionExplorer = new Shell(Frame.getDisplay());
		
	    shlReflectionExplorer.addListener(SWT.Close, new Listener() {
	        @Override
			public void handleEvent(Event event) {
	        	//event.doit = false;
	        	close = true;
	        	close();
	        }
	      });
	    
		shlReflectionExplorer.setSize(706, 634);
		shlReflectionExplorer.setText("Reflection Explorer");
		
		Group grpClassFields = new Group(shlReflectionExplorer, SWT.NONE);
		grpClassFields.setText("Class Fields");
		grpClassFields.setBounds(10, 10, 222, 569);
		
		Label lblStaticField = new Label(grpClassFields, SWT.NONE);
		lblStaticField.setBounds(10, 536, 83, 20);
		lblStaticField.setText("Static Field:");
		
		text = new Text(grpClassFields, SWT.BORDER);
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				tree.removeAll();
				table.removeAll();
				String str = text.getText();
				if (str != null && !str.trim().isEmpty()) {
					String[] split = str.split("\\.");
					if (split != null && split.length == 2) {
						curStaticObjectClassName = split[0];
						curStaticObjectFieldName = split[1];
						curStaticObject = Reflection.getStaticDeclaredField(split[0], split[1]);
						populateTree();
						refreshTable();
					}
				}
			}
		});
		text.setBounds(99, 533, 113, 26);
		
		tree = new Tree(grpClassFields, SWT.BORDER);
		tree.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				refreshTable();
			}
		});
		tree.setBounds(10, 22, 202, 505);
		
		Group grpInfo = new Group(shlReflectionExplorer, SWT.NONE);
		grpInfo.setText("Info");
		grpInfo.setBounds(238, 10, 440, 569);
		
		Button btnRefresh = new Button(grpInfo, SWT.NONE);
		btnRefresh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				tree.removeAll();
				table.removeAll();
				populateTree();
				refreshTable();
			}
		});
		btnRefresh.setBounds(10, 532, 420, 30);
		btnRefresh.setText("Refresh");
		
		table = new Table(grpInfo, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		table.setLinesVisible(true);
		table.setBounds(10, 24, 420, 502);
		table.setHeaderVisible(true);
		table.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// Enable copy to clipboard
				
				if (e.keyCode == 'c' && (e.stateMask & SWT.CTRL) != 0) {
					TableItem[] selection = table.getSelection();
					
					StringBuffer buffer = new StringBuffer();
					for (int i = 0; i < selection.length; i++) {
						buffer.append(String.format("%-15s %-15s %-25s", selection[i].getText(0), selection[i].getText(1), selection[i].getText(2)));
						
						if (i != selection.length - 1)
							buffer.append(System.getProperty("line.separator"));
					}
					
			        TextTransfer textTransfer = TextTransfer.getInstance();
			        cb.setContents(new Object[] { buffer.toString() },
			            new Transfer[] { textTransfer });
				}
			}
		});
		
		TableColumn tblclmnType = new TableColumn(table, SWT.NONE);
		tblclmnType.setWidth(87);
		tblclmnType.setText("Type");
		
		TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
		tblclmnName.setWidth(73);
		tblclmnName.setText("Name");
		
		TableColumn tblclmnValue = new TableColumn(table, SWT.NONE);
		tblclmnValue.setWidth(255);
		tblclmnValue.setText("Value");
	}
	
	private void populateTree() {
		if (curStaticObject == null) {
			try {
				Class<?> clazz = CustomClassLoader.get.loadClass(curStaticObjectClassName);
				
				if (clazz.getDeclaredField(curStaticObjectFieldName) != null) {
					TreeItem topItem = new TreeItem(tree, SWT.NONE);
					topItem.setText(text.getText() + " = " + curStaticObject);
					topItem.setData("obj", curStaticObject);
					topItem.setData("fieldName", curStaticObjectFieldName);
					topItem.setData("type", clazz);
					
					tree.setSelection(topItem);
				}
			} catch (ClassNotFoundException | NoSuchFieldException | SecurityException e) {
				//e.printStackTrace();
			}
		} else {
			Class<?> clazz = curStaticObject.getClass();
			
			TreeItem topItem = new TreeItem(tree, SWT.NONE);
			topItem.setText(text.getText() + " = " + curStaticObject);
			topItem.setData("obj", curStaticObject);
			topItem.setData("fieldName", curStaticObjectFieldName);
			topItem.setData("type", clazz);
			
			for (Field field : clazz.getDeclaredFields()) {
				field.setAccessible(true);
				
				if (Modifier.isStatic(field.getModifiers()))
					continue;
				
				try {
					Object obj = field.get(curStaticObject);
					TreeItem item = new TreeItem(topItem, SWT.NONE);
					item.setText(field.getName() + " = " + obj);
					item.setData("obj", obj);
					item.setData("fieldName", field.getName());
					item.setData("type", field.getType());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			
			topItem.setExpanded(true);
			tree.setSelection(topItem);
		}
	}
	
	private void refreshTable() {
		table.removeAll();
		
		if (tree.getSelection() != null && tree.getSelection().length != 0) {
			TreeItem selected = tree.getSelection()[0];
			if (selected != null) {
				Object selectedObj = selected.getData("obj");
				if (selectedObj != null) {
					Class<?> clazz = selectedObj.getClass();
					
					for (Field field : clazz.getDeclaredFields()) {
						field.setAccessible(true);
						
						if (Modifier.isStatic(field.getModifiers()))
							continue;
						
						try {
							Object obj = field.get(selectedObj);
							TableItem item = new TableItem(table, SWT.NONE);
							item.setText(new String[]{ field.getType().getSimpleName(), 
									field.getName(), 
									obj != null ? obj.toString() : "null" });
						} catch (IllegalArgumentException | IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
