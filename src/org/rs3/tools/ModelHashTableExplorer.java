package org.rs3.tools;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.rs3.api.Interfaces;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.HardReference;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.ObjectComposite;
import org.rs3.api.wrappers.ObjectDefinition;
import org.rs3.api.wrappers.OpenGLModel;
import org.rs3.api.wrappers.OpenGLRender;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.SDModel;
import org.rs3.api.wrappers.SDRender;
import org.rs3.api.wrappers.SoftReference;
import org.rs3.api.wrappers.Widget;
import org.rs3.boot.Frame;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;

public class ModelHashTableExplorer {

	protected Display display;
	protected Clipboard cb;
	protected Shell shell;
	
	private org.eclipse.swt.widgets.List defList;
	private org.eclipse.swt.widgets.List modelCacheList;
	
	private volatile boolean close = false;
	private volatile boolean terminate = false;
	
	int modelCacheIndex = 0;
	ObjectDefinition[] objectDefs = null;
	Model[] models = null;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ModelHashTableExplorer window = new ModelHashTableExplorer();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		cb = new Clipboard(display);
		createContents();
		
		refreshDefs();
		
		shell.open();
		shell.layout();
	}
	
	public synchronized void sendCloseSignal() {
		this.close = true;
		
		if (shell.isDisposed())
			return;
		
		Frame.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				close();
			}
		});
	}
	
	private void close() {
		terminate = true;
		
		// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
		/*EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
			@Override
			public void run() {
				frame.dispose();
			}
		});*/
		
		shell.dispose();
		
		cb.dispose();
		
		System.out.println("ModelHashTableExplorer closed and disposed.");
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(Frame.getDisplay());
		
	    shell.addListener(SWT.Close, new Listener() {
	        @Override
			public void handleEvent(Event event) {
	        	//event.doit = false;
	        	close = true;
	        	close();
	        }
	      });
	    
		shell.setSize(376, 558);
		shell.setText("Model Hash Table Explorer");
		
		defList = new org.eclipse.swt.widgets.List(shell, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		defList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				refreshModelCache();
			}
		});
		defList.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// Enable copy to clipboard
				
				if (e.keyCode == 'c' && (e.stateMask & SWT.CTRL) != 0) {
					String[] selection = defList.getSelection();
					
					StringBuffer buffer = new StringBuffer();
					for (int i = 0; i < selection.length; i++) {
						buffer.append(selection[i]);
						
						if (i != selection.length - 1)
							buffer.append(System.getProperty("line.separator"));
					}
					
			        TextTransfer textTransfer = TextTransfer.getInstance();
			        cb.setContents(new Object[] { buffer.toString() },
			            new Transfer[] { textTransfer });
				}
			}
		});
		defList.setBounds(10, 46, 120, 457);
		
		Button btnRefreshDefs = new Button(shell, SWT.NONE);
		btnRefreshDefs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				refreshDefs();
			}
		});
		btnRefreshDefs.setBounds(10, 10, 120, 30);
		btnRefreshDefs.setText("Refresh Defs");
		
		modelCacheList = new org.eclipse.swt.widgets.List(shell, SWT.BORDER | SWT.V_SCROLL);
		modelCacheList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//long id = Long.valueOf(modelCacheList.getSelection()[0]);
				
				Model model = models[modelCacheList.getSelectionIndex()];
				if (model != null) {
					Paint.updatePaintObject(model);
				}
			}
		});
		modelCacheList.setBounds(136, 108, 212, 395);
		
		Button btnRefreshModelCache = new Button(shell, SWT.NONE);
		btnRefreshModelCache.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				refreshModelCache();
			}
		});
		btnRefreshModelCache.setBounds(136, 72, 212, 30);
		btnRefreshModelCache.setText("Refresh Model Cache");
		
		Button btnAnimated = new Button(shell, SWT.RADIO);
		btnAnimated.setSelection(true);
		btnAnimated.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				modelCacheIndex = 0;
				refreshModelCache();
			}
		});
		btnAnimated.setBounds(136, 10, 89, 20);
		btnAnimated.setText("Animated");
		
		Button btnRadioButton = new Button(shell, SWT.RADIO);
		btnRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				modelCacheIndex = 1;
				refreshModelCache();
			}
		});
		btnRadioButton.setBounds(226, 10, 122, 20);
		btnRadioButton.setText("Non-animated");
		
		Button btnComposite = new Button(shell, SWT.RADIO);
		btnComposite.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				modelCacheIndex = 2;
				refreshModelCache();
			}
		});
		btnComposite.setBounds(136, 46, 111, 20);
		btnComposite.setText("Composite");
	}
	
	public void refreshDefs() {
		defList.removeAll();
		objectDefs = null;
		
		List<ObjectDefinition> objectDefsList = new ArrayList<>();
		
		Node[] refs = Client.getGameInfo().getObjectDefLoader().getDefinitionCache().getHashTable().getAll();
		if (refs == null)
			return;
		
		for (Node ref : refs) {
			if (ref != null) {
				ObjectDefinition od = null;
				
				if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
					HardReference hr = ref.forceCast(HardReference.class);
					Object def = hr.getHardReference();
					
					if (def != null)
						od = ObjectDefinition.create(def);
				
				} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
					SoftReference sr = ref.forceCast(SoftReference.class);
					Object def = sr.getSoftReference().get();
					
					if (def != null)
						od = ObjectDefinition.create(def);
				}
				
				if (od != null) {
					defList.add(Integer.toString(od.getId()));
					objectDefsList.add(od);
				}
			}
		}
		
		if (objectDefsList.size() > 0)
			objectDefs = objectDefsList.toArray(new ObjectDefinition[0]);
	}
	
	public void refreshModelCache() {
		modelCacheList.removeAll();
		models = null;
		
		if (objectDefs == null)
			return;
		
		List<Model> modelList = new ArrayList<>();
		
		final int selectionIndex = defList.getSelectionIndex();
		if (selectionIndex == -1)
			return;
		
		ObjectDefinition od = objectDefs[selectionIndex];
		
		Node[] refs;
		if (modelCacheIndex == 0)
			refs = od.getCacheLoader().getAnimatedModelCache().getHashTable().getAll();
		else if (modelCacheIndex == 1)
			refs = od.getCacheLoader().getModelCache().getHashTable().getAll();
		else if (modelCacheIndex == 2)
			refs = od.getCacheLoader().getCompositeCache().getHashTable().getAll();
		else
			return;
		
		if (refs == null)
			return;
		
		for (Node ref : refs) {
			if (ref == null)
				continue;
			
			Model model = null;
			
			if (ref.getObject().getClass().getName().equals(data.HardReference.get.getInternalName())) {
				HardReference hr = ref.forceCast(HardReference.class);
				Object def = hr.getHardReference();
				
				if (def != null) {
					if (modelCacheIndex != 2) {
						Render render = Client.getRender();
						
						if (render instanceof SDRender) {
							model = SDModel.create(def, 0, 0);
						} else if (render instanceof OpenGLRender) {
							model = OpenGLModel.create(def, 0, 0);
						} else {
							model = Model.create(def, 0, 0);
						}
					} else {
						model = ObjectComposite.create(def).getModel();
					}
				}
				
			} else if (ref.getObject().getClass().getName().equals(data.SoftReference.get.getInternalName())) {
				SoftReference sr = ref.forceCast(SoftReference.class);
				Object def = sr.getSoftReference().get();
				
				if (def != null) {
					if (modelCacheIndex != 2) {
						Render render = Client.getRender();
						
						if (render instanceof SDRender) {
							model = SDModel.create(def, 0, 0);
						} else if (render instanceof OpenGLRender) {
							model = OpenGLModel.create(def, 0, 0);
						} else {
							model = Model.create(def, 0, 0);
						}
					} else {
						model = ObjectComposite.create(def).getModel();
					}
				}
			}
			
			modelCacheList.add(Long.toString(ref.getId()));
			modelList.add(model);
		}
		
		if (modelList.size() > 0)
			models = modelList.toArray(new Model[0]);
	}
}
