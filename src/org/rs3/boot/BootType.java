package org.rs3.boot;

public enum BootType {

	NORMAL, OFFICIAL_CLIENT;
	
	private static BootType get;
	
	public static void set(BootType bootType) {
		get = bootType;
	}
	
	public static BootType get() {
		return get;
	}
}
