package org.rs3.boot;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.accessors.Reflection;
import org.rs3.api.wrappers.Character;
import org.rs3.debug.Paint;
import org.zeroturnaround.javarebel.ClassResourceSource;
import org.zeroturnaround.javarebel.IntegrationFactory;
import org.zeroturnaround.javarebel.Resource;
import org.zeroturnaround.javarebel.support.URLResource;

import sun.net.www.ParseUtil;

public class MultiClassLoader extends URLClassLoader {
	
	//public static final MultiClassLoader get = new MultiClassLoader();
	
    private final Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
    
	public MultiClassLoader() {
		super(new URL[0]);
		
		//setupJrebel();
		
		URLClassLoader urlParent = (URLClassLoader) getParent();
		for (URL url : urlParent.getURLs()) {
			this.addURL(url);
		}
	}
	
	public MultiClassLoader(ClassLoader parent) {
		super(new URL[0], parent);
		
		//setupJrebel();
		
		URLClassLoader urlParent = (URLClassLoader) getParent();
		for (URL url : urlParent.getURLs()) {
			this.addURL(url);
		}
	}
	
    /**
     * This class loader supports dynamic additions to the class path
     * at runtime.
     *
     * @see java.lang.instrument.Instrumentation#appendToSystemClassPathSearch
     */
    private void appendToClassPathForInstrumentation(String path) {
        assert(Thread.holdsLock(this));

        // addURL is a no-op if path already contains the URL
        super.addURL( getFileURL(new File(path)) );
    }
    
    static URL getFileURL(File file) {
        try {
            file = file.getCanonicalFile();
        } catch (IOException e) {}

        try {
            return ParseUtil.fileToEncodedURL(file);
        } catch (MalformedURLException e) {
            // Should never happen since we specify the protocol...
            throw new InternalError();
        }
    }
	
	private void setupJrebel() {
		//IntegrationFactory.getInstance().unregisterClassLoader(this);
		
		/*IntegrationFactory.getInstance().registerClassLoader(this, new ClassResourceSource() {
			
			@Override
			public Resource[] getLocalResources(String name) {
				try {
					List<Resource> result = new ArrayList<>();
					Enumeration<URL> urls = MultiClassLoader.this.findResources(name);
					while (urls.hasMoreElements()) {
						URL url = urls.nextElement();
						result.add(new URLResource(url));
					}
					
					if (result.size() > 0) {
						System.out.println("ok: " + name);
						return result.toArray(new Resource[0]);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				System.out.println("failed: " + name);
				
				return null;
			}
			
			@Override
			public Resource getLocalResource(String name) {
				URL url = MultiClassLoader.this.findResource(name);
				if (url != null) {
					System.out.println("ok: " + name);
					return new URLResource(url);
				}
				
				System.out.println("failed: " + name);
				
				return null;
			}
			
			@Override
			public Resource getClassResource(String className) {
				return getLocalResource(className.replace(".", "/") + ".class");
			}
		});*/
		
		//IntegrationFactory.getInstance().reinitializeClassLoader(this);
	}
	
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
    	name = name.replace("/", ".");
    	
		if (name.contains("$$M$") || name.contains("$$A$")) {
			try {
				System.out.println("rebel " + name);
				//if (Class.forName("org.zeroturnaround.javarebel.IntegrationFactory") != null)
					return IntegrationFactory.getInstance().findReloadableClass(this, name);
			} catch (ClassNotFoundException e) {e.printStackTrace();}
		}
		
		if (name.startsWith("java.") || name.startsWith("javax.") || name.startsWith("sun.") || name.startsWith("com.sun.") || name.startsWith("com.mysql.") || name.startsWith("org.xml.") || name.startsWith("org.w3c.") || name.startsWith("com.zeroturnaround.") || name.startsWith("org.zeroturnaround."))
        	return super.findClass(name);
        
        if (name.equals("org.rs3.boot.MultiClassLoader")) // bootstrap class loader loaded
        	return super.findClass(name);
        
        if (name.equals("org.rs3.accessors.CustomClassLoader"))
        	return super.findClass(name);
        
		if (name.contains("jaggl"))
			return CustomClassLoader.get.loadClass(name);
        
        if (classes.containsKey(name)) {
            return classes.get(name);
        }

        byte[] classData;

        try {
            classData = loadClassData(name);
        } catch (IOException e) {
            throw new ClassNotFoundException("Class [" + name
                    + "] could not be found", e);
        }
        
        try {
        Class<?> c = defineClass(name, classData, 0, classData.length);
        resolveClass(c);
        classes.put(name, c);

        return c;
        } catch (Error e) {
        	System.out.println(Paint.class.getClassLoader());
        	System.out.println(Character.class.getClassLoader());
        	e.printStackTrace();
        }

        throw new ClassNotFoundException();
    }

    /**
     * Load the class file into byte array
     * 
     * @param name
     *            The name of the class e.g. com.codeslices.test.TestClass}
     * @return The class file as byte array
     * @throws IOException
     */
    private byte[] loadClassData(String name) throws IOException {
        BufferedInputStream in = new BufferedInputStream(
                ClassLoader.getSystemResourceAsStream(name.replace(".", "/")
                        + ".class"));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int i;

        while ((i = in.read()) != -1) {
            out.write(i);
        }

        in.close();
        byte[] classData = out.toByteArray();
        out.close();

        return classData;
    }
	
	@Override
	protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		name = name.replace("/", ".");
		
        if (name.startsWith("java.") || name.startsWith("javax.") || name.startsWith("sun.") || name.startsWith("com.sun.") || name.startsWith("com.mysql.") || name.startsWith("org.xml.") || name.startsWith("org.w3c.") || name.startsWith("com.zeroturnaround.") || name.startsWith("org.zeroturnaround."))
        	return super.loadClass(name, resolve);
        
        if (name.startsWith("org.rs3.instrument.")) {
    		/*Class<?> clazz = this.findLoadedClass(name);

    		if (clazz == null) {
    			// first find the class from the topology's dependencies
    			try {
    				clazz = this.findClass(name);
    			} catch (ClassNotFoundException e) {
    				// ignore it.
    			}
    			
    			if (clazz == null) {
    				// then delegate to parent loader to load
    				clazz = this.getParent().loadClass(name);
    			}
    		}

    		return clazz;*/
        }
        
        if (name.equals("org.rs3.boot.MultiClassLoader")) // bootstrap class loader loaded
        	return super.loadClass(name, resolve);
        
        if (name.equals("org.rs3.accessors.CustomClassLoader"))
        	return super.loadClass(name, resolve);
        
		if (name.contains("jaggl"))
			return CustomClassLoader.get.loadClass(name);
        
		Class<?> clazz = findClass(name);
		if (clazz != null)
			return clazz;
		
		throw new ClassNotFoundException();
	}
	
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		name = name.replace("/", ".");
		
        if (name.startsWith("java.") || name.startsWith("javax.") || name.startsWith("sun.") || name.startsWith("com.sun.") || name.startsWith("com.mysql.") || name.startsWith("org.xml.") || name.startsWith("org.w3c.") || name.startsWith("com.zeroturnaround.") || name.startsWith("org.zeroturnaround."))
        	return super.loadClass(name);
        
        if (name.startsWith("org.rs3.instrument.")) {
    		/*Class<?> clazz = this.findLoadedClass(name);

    		if (clazz == null) {
    			// first find the class from the topology's dependencies
    			try {
    				clazz = this.findClass(name);
    			} catch (ClassNotFoundException e) {
    				// ignore it.
    			}
    			
    			if (clazz == null) {
    				// then delegate to parent loader to load
    				clazz = this.getParent().loadClass(name);
    			}
    		}

    		return clazz;*/
        }
        
        if (name.equals("org.rs3.boot.MultiClassLoader")) // bootstrap class loader loaded
        	return super.loadClass(name);
        
        if (name.equals("org.rs3.accessors.CustomClassLoader"))
        	return super.loadClass(name);
        
		if (name.contains("jaggl"))
			return CustomClassLoader.get.loadClass(name);
        
		Class<?> clazz = findClass(name);
		if (clazz != null)
			return clazz;
		
		throw new ClassNotFoundException();
	}
}
