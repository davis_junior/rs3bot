package org.rs3.boot;

import gamepack.GamePackFetcher;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.instrument.UnmodifiableClassException;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Authenticator;
import java.net.ProxySelector;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.DropMode;

import org.apache.commons.io.FileUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.rs3.accessors.CustomClassLoader;
import org.rs3.accessors.I_AnimableObject;
import org.rs3.accessors.I_Client;
import org.rs3.accessors.I_OpenGLModel;
import org.rs3.accessors.Reflection;
import org.rs3.api.Ability;
import org.rs3.api.ActionBar;
import org.rs3.api.ActionSlot;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.CombatMode;
import org.rs3.api.Familiar;
import org.rs3.api.GESlot;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundItems;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Nodes;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Skill;
import org.rs3.api.Spell;
import org.rs3.api.Toolbelt;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.input.HardwareInput;
import org.rs3.api.input.Input;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.input.realmouse.MouseData;
import org.rs3.api.input.realmouse.MouseRecorder;
import org.rs3.api.interfaces.DropMenu;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.ContinueMessageGame;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.interfaces.widgets.GraphicsSettingsWidget;
import org.rs3.api.interfaces.widgets.InputPane;
import org.rs3.api.interfaces.widgets.LoginScreen;
import org.rs3.api.interfaces.widgets.MagicAbilitiesMainTabWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Settings_Gameplay_LootSettingsSubTabWidget;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterface;
import org.rs3.api.interfaces.widgets.Ribbon.SubInterfaceTab;
import org.rs3.api.interfaces.widgets.WorldSelect;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.objects.Tile;
import org.rs3.api.places.Daemonheim;
import org.rs3.api.places.WizardsTower;
import org.rs3.api.places.WizardsTower.WizardsTowerFloorLevel;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.GEOffer;
import org.rs3.api.wrappers.HashTable;
import org.rs3.api.wrappers.ItemDefinition;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Node;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.NpcDefinition;
import org.rs3.api.wrappers.ObjectDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.SkillData;
import org.rs3.callbacks.capture.ModelCapture;
import org.rs3.callbacks.proxy.ProxySetup;
import org.rs3.database.AccountInfo;
import org.rs3.database.Database;
import org.rs3.database.GrandExchangeRow;
import org.rs3.database.LevelsRow;
import org.rs3.database.ServerInfo;
import org.rs3.database.StatsRow;
import org.rs3.debug.DebugFrame;
import org.rs3.debug.DebugGL;
import org.rs3.debug.Paint;
import org.rs3.debug.Paint.DebugObject;
import org.rs3.debug.Paint.DebugString;
import org.rs3.eventqueue.BlockingEventQueue;
import org.rs3.eventqueue.EventNazi;
import org.rs3.instrument.ClassWriterFixed;
import org.rs3.instrument.SimpleMain;
import org.rs3.instrument.transformers.callbacks.CallbackMethodsTransformer;
import org.rs3.instrument.transformers.callbacks.CallbackMultipleMethodsTransformer;
import org.rs3.instrument.transformers.debug.DebugTransformer;
import org.rs3.instrument.transformers.officialclient.DisableBrowserTransformer;
import org.rs3.instrument.transformers.officialclient.Rs2AppletTransformer;
import org.rs3.instrument.transformers.opengl.OwnJOGLTransformer;
import org.rs3.instrument.transformers.reverse.GetfieldCallTransformer;
import org.rs3.instrument.transformers.reverse.GetstaticCallTransformer;
import org.rs3.instrument.transformers.reverse.MethodCallTransformer;
import org.rs3.instrument.transformers.reverse.MethodExitTransformer;
import org.rs3.instrument.transformers.reverse.OwnMethodCallTransformer;
import org.rs3.instrument.transformers.reverse.PutfieldCallTransformer;
import org.rs3.instrument.transformers.reverse.PutstaticCallTransformer;
import org.rs3.remote.Server;
import org.rs3.reverse.GetfieldFieldRecorder;
import org.rs3.reverse.GetstaticFieldRecorder;
import org.rs3.reverse.MethodRecorder;
import org.rs3.reverse.PutfieldFieldRecorder;
import org.rs3.reverse.PutstaticFieldRecorder;
import org.rs3.scripts.AccountCreator;
import org.rs3.scripts.Combat;
import org.rs3.scripts.Miner;
import org.rs3.scripts.Runespan;
import org.rs3.scripts.Script;
import org.rs3.scripts.ScriptHandler;
import org.rs3.scripts.Tanner;
import org.rs3.scripts.WildernessBoneCollector;
import org.rs3.scripts.WineCollector;
import org.rs3.scripts.Woodcutter;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.runespan.Creature;
import org.rs3.scripts.woodcutting.LogType;
import org.rs3.tools.ModelHashTableExplorer;
import org.rs3.tools.PathGenerator;
import org.rs3.tools.ReflectionExplorer;
import org.rs3.tools.SettingsExplorer;
import org.rs3.tools.WidgetExplorer;
import org.rs3.util.ArrayUtils;
import org.rs3.util.FileCleaner;
import org.rs3.util.Filter;
import org.rs3.util.NetworkDrives;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import proxy.ProxyAuthenticator;
import analyzers.MultiplierFinder;

import com.jogamp.opengl.util.gl2.GLUT;
import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;

import sun.awt.resources.awt;
import sun.misc.Version;
import tk.ivybits.agent.AgentLoader;
import tk.ivybits.agent.Tools;
import data.ClassData;
import data.MenuItemNode;
import data.XMLLoader;

public class Frame implements AppletStub {
	
	public static Frame window;
	
	private static Display display = null;
	
	public java.awt.Frame awtFrame;
	public Panel panel;
	public Composite comp = null;
	private Shell shell;
	private ToolBar toolBar;
	protected Menu debugDropMenu;
	protected Menu showDropMenu;
	protected Menu debugStringsDropMenu;
	protected Menu debugObjectsDropMenu;
	protected Menu toolsDropMenu;
	private ToolItem tltmPlay;
	private ToolItem tltmFilterAll;
	private ToolItem tltmClearFilters;
	
	public volatile boolean minimized = false;
	public volatile boolean activated = true;
	
	private GamePackFetcher gamepackFetcher = null;
	
	public volatile boolean debugGL = false;
	public volatile boolean paint = true;
	private boolean scriptPlayButton = true;
	private boolean blockingInput = true;
	
	public Applet rs2app = null;
	public boolean runescapeLoaded = false;
	
	public static MouseRecorder recorder = new MouseRecorder();
	public static boolean recordMouseMode = false;
	
	public static Display getDisplay() {
		return display;
	}
	
	public static void attachAgent() throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException {
		System.out.println("Attaching agent...");
		Tools.loadAgentLibrary();
		AgentLoader.attachAgentToJVM(Tools.getCurrentPID(), SimpleMain.class, ClassWriterFixed.class, CallbackMethodsTransformer.class, CallbackMultipleMethodsTransformer.class, DebugTransformer.class, DisableBrowserTransformer.class, Rs2AppletTransformer.class, OwnJOGLTransformer.class, GetfieldCallTransformer.class, GetstaticCallTransformer.class, MethodCallTransformer.class, MethodExitTransformer.class, OwnMethodCallTransformer.class, PutfieldCallTransformer.class, PutstaticCallTransformer.class);
	}
	
	/**
	 * Launch the application.
	 * @param args
	 * @throws AgentInitializationException 
	 * @throws AgentLoadException 
	 * @throws AttachNotSupportedException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException {
		/*System.setProperty("socksProxyHost", "104.148.11.196");
		System.setProperty("socksProxyPort", "61336");
		System.setProperty("java.net.socks.username", "davisjunior11");
		System.setProperty("java.net.socks.password", "E25lEePY");
		Authenticator.setDefault(new ProxyAuthenticator("davisjunior11", "E25lEePY"));*/
		
		//NetworkDrives.map();
		
		ServerInfo.loadServerInfo();
		//ServerInfo.cur = new ServerInfo(8);
		
		ProxySelector.setDefault(ProxySetup.getProxySelector());
		
		FileCleaner.loadNetworkRunescapeCache();
		FileCleaner.deleteRunescapeFiles();
		
		attachAgent();

		Thread loadMouseDataThread = new Thread() {
			public void run() {
				MouseData.getMouseData();
			}
		};
		loadMouseDataThread.start();
		
		
		//
		// Set console handler to finest so loggers can output everything
		//
	    //get the top Logger:
	    Logger topLogger = java.util.logging.Logger.getLogger("");

	    // Handler for console (reuse it if it already exists)
	    Handler consoleHandler = null;
	    //see if there is already a console handler
	    for (Handler handler : topLogger.getHandlers()) {
	        if (handler instanceof ConsoleHandler) {
	            //found the console handler
	            consoleHandler = handler;
	            break;
	        }
	    }

	    if (consoleHandler == null) {
	        //there was no console handler found, create a new one
	        consoleHandler = new ConsoleHandler();
	        topLogger.addHandler(consoleHandler);
	    }
	    //set the console handler to fine:
	    consoleHandler.setLevel(java.util.logging.Level.FINEST);
	    
	    
	    //List<String> inputArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
	    //System.out.println("input arguments = " + inputArguments);
	    
		BootType.set(BootType.NORMAL);
		
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread arg0, Throwable arg1) {
				System.out.println("Uncaught exception handler: ");
				arg1.printStackTrace();
			}
		});
		
		System.out.println("Attempting database connection...");
		System.out.println("Database connection valid:" + Database.setupSql(2));
		
		try {
			Frame window = new Frame();
			Frame.window = window;
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TODO: remove
	public static void test() {
		
	}
	
	/**
	 * Open the window.
	 */
	public void open() {
		//Input.setUseHardWareInput(false);
		Input.setUseHardWareInput(true);
		
		final String initializeKey = "";
		
		display = Display.getDefault();
		createContents();
		
		initialize();
		
		Thread initialEventQueueThread = new Thread() {
			@Override
			public void run() {
				ScriptHandler.setupInput(awtFrame, initializeKey);
			}
		};
		initialEventQueueThread.start();
		
		Thread eventQueueThread2 = new Thread() {
			@Override
			public void run() {
				//Time.sleep(10000); // TODO: get client state hook? check when rs is loaded
				
				Time.sleep(4000);
				ScriptHandler.setupInput(awtFrame, initializeKey);
				
				boolean loaded = false;
				
				Timer timer = new Timer(120000); // 2 min.
				while (timer.isRunning()) {
					if (Client.isRSDoneLoading()) {
						loaded = true;
						break;
					}
					
					Time.sleep(50);
				}
				
				if (loaded) {
					runescapeLoaded = true;
					
					System.out.println("Runescape is done loading.");
					
					ScriptHandler.setupInput(awtFrame, null);
				} else {
					System.out.println("ERROR: Runescape didn't load!");
				}
			}
		};
		
		if (!recordMouseMode)
			eventQueueThread2.start();
		
		shell.open();
		shell.layout();
		while (!display.isDisposed()) {
			try {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//display.dispose(); // handled in overridden close event (listener)
		
		System.exit(0);
	}
	
	/*public boolean isCompositeFocused() {
	}*/
	
	public boolean isRsCanvasFocused() {
		if (awtFrame == null)
			return false;
		
		Component focusOwner = awtFrame.getFocusOwner();
		if (focusOwner != null && focusOwner.getClass().getName().equals(data.Canvas.get.getInternalName())) {
			//System.out.println("focused to: " + focusOwner);
			return true;
		}
		
		return false;
	}
	
	private void onPlayButtonClick() {
		if (scriptPlayButton) {
			runScript();
			/*if (ScriptHandler.currentScript == null || ScriptHandler.currentScript.isStopped())
				runScript();
			else
				return;*/
		} else
			ScriptHandler.currentScript.stopInterrupt = true;
		
		setScriptPlayButton(scriptPlayButton);
	}

	private void setScriptPlayButton(boolean running) {
		try {
			scriptPlayButton = !running;
			
			if (scriptPlayButton) {
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	tltmPlay.setText("Play");
				    }
				});
			} else {
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	tltmPlay.setText("Stop");
				    }
				});
			}
		} catch (Throwable e) {
			System.out.println("Script exception: ");
			e.printStackTrace();
		}
	}
	
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		//shell = new Shell(SWT.TITLE | SWT.CLOSE | SWT.BORDER);
		shell = new Shell(getDisplay());
		shell.setSize(1161, 81);
		
		shell.addShellListener(new ShellAdapter() {
			@Override
	        public void shellActivated(ShellEvent e) {
				//System.out.println("activated");
				activated = true;
			}
			
			@Override
	        public void shellDeactivated(ShellEvent e) {
				//System.out.println("deactivated");
				activated = false;
			}
			
			@Override
	        public void shellIconified(ShellEvent e) {
				//System.out.println("minimized");
				minimized = true;
			}
			
			@Override
	        public void shellDeiconified(ShellEvent e) {
				//System.out.println("maximized");
				minimized = false;
			}
			
			@Override
	        public void shellClosed(ShellEvent e) {
				//System.out.println("shell closed");
				// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
				EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
					@Override
					public void run() {
						if (rs2app != null)
							rs2app.destroy();
						
						if (awtFrame != null) {
							//System.out.println("disposing");
							awtFrame.dispose();
						}
					}
				});
				
				debugDropMenu.dispose();
				showDropMenu.dispose();
				debugStringsDropMenu.dispose();
				debugObjectsDropMenu.dispose();
				toolsDropMenu.dispose();
				shell.dispose();
			}
		});
		
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (recordMouseMode) {
					if (!recorder.recording) {
						System.out.println("Flushing mouse data to file...");
						recorder.rebaseMouseDataTime();
						recorder.flushToFile();
					}
					
					try {
						GlobalScreen.unregisterNativeHook();
					} catch (NativeHookException e) {
						e.printStackTrace();
					}
				}
				
				if (gamepackFetcher != null)
					gamepackFetcher.getGamepackJar().delete();
				
				// runescape applet won't dispose correctly, so System.exit is required
				//rs2app.stop();
				//rs2app.destroy();
				
				// DEADLOCK WORKAROUND: java.awt.Window (frame) must be disposed on the EDT
				EventQueue.invokeLater(new Runnable() { // invokeAndWait() will cause deadlock
					@Override
					public void run() {
						if (awtFrame != null)
							awtFrame.dispose();
					}
				});

				display.dispose(); // supposed to dispose all child shells also
			}
		});
		
		shell.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (!recordMouseMode) {
					if (panel != null && comp != null) {
						toolBar.setSize(shell.getClientArea().width, toolBar.getBounds().height);
						if (rs2app != null) {
							rs2app.setPreferredSize(new Dimension(shell.getClientArea().width, shell.getClientArea().height - toolBar.getBounds().height + 5));
							panel.setPreferredSize(rs2app.getPreferredSize());
						}
						comp.setBounds(0, toolBar.getBounds().height - 5, rs2app.getPreferredSize().width, rs2app.getPreferredSize().height);
						//shell.pack();
						
						if (!debugGL) {
							if (DebugFrame.get != null)
								DebugFrame.get.resize(shell.getSize().x, shell.getSize().y);
						} else {
							if (DebugGL.get != null)
								DebugGL.get.resize(shell.getSize().x, shell.getSize().y);
						}
					}
				} else if (recordMouseMode) {
					Rectangle clientArea = shell.getClientArea();
					comp.setSize(clientArea.width, clientArea.height);
				}
			}
		});
		//shell.setSize(1024, 768);
		shell.setText("RS3");
		
		toolBar = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);
		toolBar.setSize(1024, 36);
		
		if (recordMouseMode)
			toolBar.setVisible(false);
		
		ToolItem comboSep = new ToolItem(toolBar, SWT.SEPARATOR);
		
		final Map<String, Script> scripts = new HashMap<>();
		scripts.put("Multi", null);
		scripts.put("Mine", new Miner());
		scripts.put("Woodcutter", new Woodcutter());
		scripts.put("Combat", new Combat());
		scripts.put("WildernessBoneCollector", new WildernessBoneCollector());
		scripts.put("Tanner", new Tanner());
		scripts.put("WineCollector", new WineCollector());
		scripts.put("Runespan", new Runespan());
		scripts.put("AccountCreator", new AccountCreator());
		
		final Combo combo = new Combo(toolBar, SWT.READ_ONLY);
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String comboText = combo.getText();
				
				if (!comboText.equals("Multi"))
					ScriptHandler.setupSingleScriptMode(scripts.get(comboText));
				else {
					List<Script> multiScripts = new ArrayList<>();
					multiScripts.add(new Miner());
					multiScripts.add(new Woodcutter());
					
					ScriptHandler.setupMultiScriptMode(multiScripts);
				}
			}
		});
		combo.setBounds(0, 0, 120, 36);
		String[] keys = scripts.keySet().toArray(new String[0]);
		combo.setItems(keys);
		combo.setText(keys[0]);
		//ScriptHandler.currentScript = scripts.get(keys[0]);
		
		if (!combo.getText().equals("Multi"))
			ScriptHandler.setupSingleScriptMode(scripts.get(combo.getText()));
		else {
			List<Script> multiScripts = new ArrayList<>();
			multiScripts.add(new Miner());
			multiScripts.add(new Woodcutter());
			
			ScriptHandler.setupMultiScriptMode(multiScripts);
		}
		
		comboSep.setWidth(combo.getSize().x);
		comboSep.setControl(combo);
		
		final ToolItem tltmPlay = new ToolItem(toolBar, SWT.NONE);
		this.tltmPlay = tltmPlay;
		tltmPlay.setWidth(240);
		tltmPlay.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				onPlayButtonClick();
			}
		});
		tltmPlay.setText("Play");
		
		final ToolItem tltmBlockInput = new ToolItem(toolBar, SWT.NONE);
		tltmBlockInput.setWidth(240);
		tltmBlockInput.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				blockingInput = !blockingInput;
				
				if (!blockingInput) {
					ScriptHandler.setupInput(awtFrame, "");
					stopBlocking();
					tltmBlockInput.setText("Block");
				} else {
					ScriptHandler.setupInput(awtFrame, "");
					startBlocking();
					tltmBlockInput.setText("Unblock");
				}
			}
		});
		tltmBlockInput.setText("Unblock");
		
		ToolItem tltmCustom = new ToolItem(toolBar, SWT.NONE);
		tltmCustom.setWidth(240);
		tltmCustom.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				try {
					//System.out.println("Camera: " + Client.getCameraX() + ", " + Client.getCameraY() + ", " + Client.getCameraZ() + ", "
					//		+ Client.getCameraPitch() + ", " + Client.getCameraYaw());
					
					/*System.out.println("Plane: " + Client.getPlane());
					
					System.out.println("My player name: " + Client.getMyPlayer().getName());
					System.out.println("Location: " + Client.getMyPlayer().getData().getCenterLocation().getAbsoluteX() + ", " + Client.getMyPlayer().getData().getCenterLocation().getAbsoluteY() + ", " + Client.getMyPlayer().getPlane());
					System.out.println("Destination: " + Walking.getDestination().x + ", " + Walking.getDestination().y);*/
					
					custom();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		});
		tltmCustom.setText("Custom");
		
		final ToolItem tltmDebug = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmDebug.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.ARROW) {
	                // Position the menu below and vertically aligned with the the drop down tool button.
	                final ToolItem toolItem = (ToolItem) e.widget;
	                final ToolBar  toolBar = toolItem.getParent();

	                Point point = toolBar.toDisplay(new Point(e.x, e.y));
	                debugDropMenu.setLocation(point.x, point.y);
	                debugDropMenu.setVisible(true);
				}
			}
		});
		tltmDebug.setText("Debug");
		
		debugDropMenu = new Menu(shell, SWT.POP_UP);
		MenuItem item = new MenuItem(debugDropMenu, SWT.CHECK);
		item.setText("DebugGL");
		item.setSelection(debugGL);
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				debugGL = menuItem.getSelection();
				
				if (debugGL && DebugFrame.get != null)
					DebugFrame.get.sendCloseSignal();
				else if (!debugGL && DebugGL.get != null)
					DebugGL.get.sendCloseSignal();
			}
		});
		
		item = new MenuItem(debugDropMenu, SWT.PUSH);
		item.setText("Debug");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!debugGL) {
					if (DebugFrame.get != null)
						DebugFrame.get.sendCloseSignal();
					
					final DebugFrame debug = new DebugFrame();
					DebugFrame.get = debug;
					
					getDisplay().asyncExec(new Runnable() {
					    @Override
						public void run() {
					    	debug.open();
					    }
					});
				} else {
					if (DebugGL.get != null)
						DebugGL.get.sendCloseSignal();
					
					final DebugGL debug = new DebugGL();
					DebugGL.get = debug;
					
					getDisplay().asyncExec(new Runnable() {
					    @Override
						public void run() {
					    	debug.open();
					    }
					});
				}
			}
		});
		
		item = new MenuItem(debugDropMenu, SWT.PUSH);
		item.setText("New Model");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				newModel();
			}
		});
		
		item = new MenuItem(debugDropMenu, SWT.PUSH);
		item.setText("Clear All");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					//Paint.clearPaintObject(Paint.my, ModelCapture.charactersModelCapture);
					Paint.clearPaintObject(Paint.npc, ModelCapture.npcs);
					ModelCapture.npcs.clear();
					Paint.clearPaintObject(Paint.my);
					//Paint.clearPaintObject(Paint.my, ModelCapture.players);
					//ModelCapture.players.clear();
					Paint.clearPaintObject(Paint.animableObj);
					Paint.clearPaintObject(Paint.animatedAnimableObj);
					
					ModelCapture.animableObjectModels.clear();
					ModelCapture.boundaryObjectModels.clear();
					ModelCapture.floorObjectModels.clear();
					ModelCapture.wallObjectModels.clear();
					
					Paint.clearPaintObject(Paint.components);
					Paint.clearPaintObject(Paint.groundItem);
					Paint.clearPaintObject(Paint.groundObject);
					Paint.clearPaintObject(Paint.model);
					Paint.clearPaintObject(Paint.paintString);
					Paint.clearPaintObject(Paint.point);
					Paint.clearPaintObject(Paint.tile);
					Paint.clearPaintObject(Paint.tiles);
					Paint.clearPaintObject(Paint.tiles2D);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});
		
		
		final ToolItem tltmShow = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmShow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.ARROW) {
	                // Position the menu below and vertically aligned with the the drop down tool button.
	                final ToolItem toolItem = (ToolItem) e.widget;
	                final ToolBar  toolBar = toolItem.getParent();

	                Point point = toolBar.toDisplay(new Point(e.x, e.y));
	                showDropMenu.setLocation(point.x, point.y);
	                showDropMenu.setVisible(true);
				}
			}
		});
		tltmShow.setText("Show");
		
		showDropMenu = new Menu(shell, SWT.POP_UP);
		item = new MenuItem(showDropMenu, SWT.CHECK);
		item.setText("Paint");
		item.setSelection(paint);
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				paint = menuItem.getSelection();
			}
		});
		
		debugStringsDropMenu = new Menu(shell, SWT.DROP_DOWN);
		item = new MenuItem(showDropMenu, SWT.CASCADE);
		item.setText("Debug Strings");
		item.setMenu(debugStringsDropMenu);
		
		debugObjectsDropMenu = new Menu(shell, SWT.DROP_DOWN);
		item = new MenuItem(showDropMenu, SWT.CASCADE);
		item.setText("Objects");
		item.setMenu(debugObjectsDropMenu);
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("Current Time");
		item.setSelection(Paint.DebugString.CurrentTime.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.CurrentTime.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("Account Info");
		item.setSelection(Paint.DebugString.AccountInfo.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.AccountInfo.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("Player Info");
		item.setSelection(Paint.DebugString.PlayerInfo.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.PlayerInfo.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("Camera");
		item.setSelection(Paint.DebugString.Camera.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.Camera.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("CameraLocationData");
		item.setSelection(Paint.DebugString.CameraLocationData.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.CameraLocationData.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("Viewport");
		item.setSelection(Paint.DebugString.Viewport.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.Viewport.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugStringsDropMenu, SWT.CHECK);
		item.setText("Menu");
		item.setSelection(Paint.DebugString.Menu.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugString.Menu.setPaint(menuItem.getSelection());
			}
		});
		
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Npcs");
		item.setSelection(Paint.DebugObject.NPCS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.NPCS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Ground Objects");
		item.setSelection(Paint.DebugObject.GROUND_OBJECTS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.GROUND_OBJECTS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Ground Items");
		item.setSelection(Paint.DebugObject.GROUND_ITEMS.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.GROUND_ITEMS.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Inventory");
		item.setSelection(Paint.DebugObject.INVENTORY.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.INVENTORY.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Bank");
		item.setSelection(Paint.DebugObject.BANK.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.BANK.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Worn Equipment");
		item.setSelection(Paint.DebugObject.WORN_EQUIPMENT.shouldPaint());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.WORN_EQUIPMENT.setPaint(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.CHECK);
		item.setText("Skip Null Names");
		item.setSelection(Paint.DebugObject.skipNullNames());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Paint.DebugObject.setSkipNullNames(menuItem.getSelection());
			}
		});
		
		item = new MenuItem(debugObjectsDropMenu, SWT.PUSH);
		item.setText("Disable All");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final MenuItem menuItem = (MenuItem) e.widget;
				Menu menu = menuItem.getParent();
				for (MenuItem mi : menu.getItems()) {
					if (!mi.getText().equals("Skip Null Names"))
						mi.setSelection(false);
					else
						mi.setSelection(true);
				}
				
				for (DebugObject debugObject : Paint.DebugObject.values()) {
					debugObject.setPaint(false);
				}
				
				Paint.DebugObject.setSkipNullNames(true);
			}
		});
		
		
		item = new MenuItem(showDropMenu, SWT.PUSH);
		item.setText("Disable All");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				//final MenuItem menuItem = (MenuItem) e.widget;
				
				//
				// Debug Strings
				//
				for (MenuItem mi : debugStringsDropMenu.getItems()) {
					mi.setSelection(false);
				}
				
				for (DebugString debugString : Paint.DebugString.values()) {
					debugString.setPaint(false);
				}
				
				//
				// Debug Objects
				//
				for (MenuItem mi : debugObjectsDropMenu.getItems()) {
					if (!mi.getText().equals("Skip Null Names"))
						mi.setSelection(false);
					else
						mi.setSelection(true);
				}
				
				for (DebugObject debugObject : Paint.DebugObject.values()) {
					debugObject.setPaint(false);
				}
				
				Paint.DebugObject.setSkipNullNames(true);
			}
		});
		
		
		final ToolItem tltmTools = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmTools.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.detail == SWT.ARROW) {
	                // Position the menu below and vertically aligned with the the drop down tool button.
	                final ToolItem toolItem = (ToolItem) e.widget;
	                final ToolBar  toolBar = toolItem.getParent();

	                Point point = toolBar.toDisplay(new Point(e.x, e.y));
	                toolsDropMenu.setLocation(point.x, point.y);
	                toolsDropMenu.setVisible(true);
				}
			}
		});
		tltmTools.setText("Tools");
		
		toolsDropMenu = new Menu(shell, SWT.POP_UP);
		item = new MenuItem(toolsDropMenu, SWT.PUSH);
		item.setText("Reflection Explorer");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final ReflectionExplorer window = new ReflectionExplorer();
				
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	window.open();
				    }
				});
			}
		});
		
		item = new MenuItem(toolsDropMenu, SWT.PUSH);
		item.setText("Widget Explorer");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final WidgetExplorer window = new WidgetExplorer();
				
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	window.open();
				    }
				});
			}
		});
		
		item = new MenuItem(toolsDropMenu, SWT.PUSH);
		item.setText("Settings Explorer");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final SettingsExplorer window = new SettingsExplorer();
				
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	window.open();
				    }
				});
			}
		});
		
		item = new MenuItem(toolsDropMenu, SWT.PUSH);
		item.setText("Path Generator");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final PathGenerator window = new PathGenerator();
				
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	java.awt.Component c = (java.awt.Component) Client.getCanvas().getObject();
				    	window.open(c);
				    }
				});
			}
		});
		
		item = new MenuItem(toolsDropMenu, SWT.PUSH);
		item.setText("Model Hash Table Explorer");
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final ModelHashTableExplorer window = new ModelHashTableExplorer();
				
				getDisplay().asyncExec(new Runnable() {
				    @Override
					public void run() {
				    	window.open();
				    }
				});
			}
		});
		
		
		this.tltmFilterAll = new ToolItem(toolBar, SWT.CHECK);
		final ToolItem tltmFilterAll = this.tltmFilterAll;
		tltmFilterAll.setSelection(true);
		tltmFilterAll.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setFilterStatus(tltmFilterAll.getSelection());
			}
		});
		tltmFilterAll.setText("Filter All");
		
		this.tltmClearFilters = new ToolItem(toolBar, SWT.NONE);
		ToolItem tltmClearFilters = this.tltmClearFilters;
		tltmClearFilters.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clearFilters();
			}
		});
		tltmClearFilters.setText("Clear Filters");
	}
	
	public void setFilterStatus(boolean filterAll) {
		MethodRecorder.setFilterStatus(filterAll);
		PutstaticFieldRecorder.setFilterStatus(filterAll);
		GetstaticFieldRecorder.setFilterStatus(filterAll);
		PutfieldFieldRecorder.setFilterStatus(filterAll);
		GetfieldFieldRecorder.setFilterStatus(filterAll);
	}
	
	public void clearFilters() {
		MethodRecorder.clearFilters();
		PutstaticFieldRecorder.clearFilters();
		GetstaticFieldRecorder.clearFilters();
		PutfieldFieldRecorder.clearFilters();
		GetfieldFieldRecorder.clearFilters();
	}
	

	
	/**
	* Starts sending the image and accepting commands from Delphi.
	*
	* @result True if success
	*/
	public boolean startBlocking() {
		if (blockingInput) {
			if (!Input.useHardWareInput()) {
				try {
					java.awt.Component c = (java.awt.Component) Client.getCanvas().getObject();
					
					if (c != null) {
						BlockingEventQueue.setBlocking(c, true);
						BlockingEventQueue.ensureBlocking();
						ScriptHandler.nazi = EventNazi.getNazi(c);
					}
					
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	/**
	* Stops sending the image and accepting commands from Delphi.
	* @result True if success
	*/
	public boolean stopBlocking() {
		if (!blockingInput) {
			if (!Input.useHardWareInput()) {
				try {
					//blocking = false;
					
					java.awt.Component c = (java.awt.Component) Client.getCanvas().getObject();
					
					if (c != null) {
						BlockingEventQueue.setBlocking(c, false);
					}
					
					ScriptHandler.nazi = null;
					
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	//static TilePath miningToBank = new TilePath(new Tile(2970, 3237, 0), new Tile(2968, 3226, 0), new Tile(2981, 3223, 0), new Tile(3000, 3226, 0), new Tile(3012, 3233, 0), new Tile(3028, 3240, 0), new Tile(3047, 3236, 0));
	
	public void newModel() {
		//Paint.updatePaintObject(Client.getMyPlayer(), ModelCapture.charactersModelCapture);
		
		// AnimableObject
		/*RSAnimable animable = GroundObjects.getNearest(LogType.Normal.getObjectIds());
		if (animable != null) {
			AnimableObject ao = (AnimableObject) animable;
			Paint.updatePaintObject(ao);
			
			System.out.println("ADDED: " + ao.getDebugInfo());
		}*/
		
		// AnimatedAnimableObject
		/*animable = GroundObjects.getNearest(OreType.IRON.getObjectIds());
		if (animable != null) {
			AnimatedAnimableObject aao = (AnimatedAnimableObject) animable;
			Paint.updatePaintObject(aao);
			
			System.out.println("ADDED: " + aao);
		}*/
		
		// Npc
		for (Npc npc : Npcs.getAll()) {
			if (npc == null)
				continue;
			
			NpcDefinition def = npc.getNpcDefinition();
			
			if (def != null) {
				if (def.getName().toLowerCase().contains("troll")) {
				//if (def.getName().contains("irate")) {
					System.out.println("Added: " + def.getName() 
							+ "(" + def.getId() + ")"
							+ ": " + npc.getData().getCenterLocation().getAbsoluteX() + ", " + npc.getData().getCenterLocation().getAbsoluteY() + ", " + npc.getPlane());
					
					//Paint.updatePaintObject(npc, ModelCapture.charactersModelCapture);
					//Paint.updatePaintObject(npc, ModelCapture.npcs);
					ModelCapture.npcs.add(npc);
					ModelCapture.rotateTimer = new Timer(10000);
				}
			}
		}
	}
	
	public void custom() {
		Antiban.randomMouseSpeed();
		
		//System.out.println(Client.getSettingData().getSettings());
		
		//ItemTable.EQUIPMENT.printItems();
		
		//ItemTable.BANK.getSnapshot().printItems();
		
		//org.rs3.api.ItemTable.INVENTORY.printItems();
		//org.rs3.api.ItemTable.SHOP_BUY_FREE_ITEMS.printItems();
		
		//System.out.println(ProxySetup.getProxy());
		
		/*int[] cpuInfo = (int[]) Reflection.invokeMethod("jaclib.hardware_info.HardwareInfo", "getRawCPUInfo", null, null, null);
		if (cpuInfo != null) {
			for (int i = 0; i < cpuInfo.length; i++) {
				System.out.println("[" + i + "] = " + cpuInfo[i]);
			}
		}*/
		
		//Paint.tile = Players.getMyPlayer().getTile();
		
		/*System.out.println("Elpased: " + (System.currentTimeMillis() - startTime));
		Iterator it = ManagementFactory.getGarbageCollectorMXBeans().iterator();
		while (it.hasNext()) {
			GarbageCollectorMXBean bean = (GarbageCollectorMXBean) it.next();
			if (bean.isValid()) {
				long time = bean.getCollectionTime();
				System.out.println("GC time: " + time);
			}
		}*/
		
		//loadModscript();
		//System.out.println("Database connection valid: " + Database.isConnectionValid(2));
		
		//System.out.println(Client.isLoggedIn());
		//System.out.println((int)Reflection.getStaticDeclaredField("client", "pr", int.class) * -1776637517);
		
		/*GroundObjects.getNearest(Type.All, new Filter<GroundObject>() {
			@Override
			public boolean accept(GroundObject go) {
				ObjectDefinition def = go.getDefinition();
				
				if (def != null) {
					if (def.getName().contains("Death")) {
						System.out.println(def.getName() + " " + go.getId());
						return true;
					}
				}
				
				if (go.getId() == 92233) {
					System.out.println(go.getTile().distanceTo());
				}
				
				return false;
			}
		});*/
		
		/*for (Npc npc : Npcs.getAll()) {
			if (npc != null) {
				NpcDefinition def = npc.getNpcDefinition();
				if (def != null) {
					if (def.getName().contains("Death"))
						System.out.println(def.getId() + " " + def.getName());
				}
			}
		}*/
		
		/*HashTable tables = Client.getItemTables();
		if (tables != null) {
			for (Node tableNode : tables.getAll()) {
				if (tableNode != null) {
					ItemTable table = tableNode.forceCast(ItemTable.class);
					System.out.println(table.getId());
				}
			}
		}*/
		
		Thread thread = new Thread() {
			public void run() {
				System.out.println("[begin thread]");
				Time.sleepMedium();
				
				//Client.logout(false);
				
				//WizardsTower.enterLowLevelRunespanPortal();
				
				//Time.sleep(1000);
				
				//Camera.setZoomPercent(20);

				//System.out.println(Ribbon.SubInterfaceTab.SETTINGS_GAMEPLAY.open(true));
				//System.out.println(Daemonheim.retrieveRingOfKinship());
				
				//System.out.println(ActionSlot.BAR1_SLOT8.setAbility(Ability.AIR_WAVE));
				//System.out.println(ActionSlot.BAR1_SLOT4.setItem(12158));
				//System.out.println(ActionBar.setActionBar(3));
				
				//System.out.println(org.rs3.api.Menu.isCollapsed());
				//org.rs3.api.Menu.updateMenuItemsCaches();
				//System.out.println(GraphicsOptionsWidget.GraphicsLevel.LOW.select(true));
				//System.out.println(GraphicsOptionsWidget.GraphicsCpuUsage.VERY_LOW.select(true));
				
				//System.out.println(DeathHandlerParam.exitDeathsOffice());
				
				//Object obj = Reflection.getStaticDeclaredField("m", "w");
				//if (obj != null)
				//	System.out.println(org.rs3.api.wrappers.MenuItemNode.create(obj).getAction() + " " + org.rs3.api.wrappers.MenuItemNode.create(obj).getOption());
				
				System.out.println("[end thread]");
			}
		};
		//thread.start();
		
		//System.out.println(GrandExchangeWidget.get.isTutorialCompletedSetting());
		
		/*try {
			CustomClassLoader.instrument.retransformClasses(CustomClassLoader.get.loadClass("aex"));
		} catch (UnmodifiableClassException | ClassNotFoundException e) {
			e.printStackTrace();
		}*/
		
		//RunTimeParam runTimeParam = new RunTimeParam();
		//System.out.println("RunTime: " + runTimeParam.onStart());
		
		//System.out.println(Client.getRender());
		
		// aja is a node
		// aja = GraphicsSettingsContainer
		// zn = GraphicsSetting
		// alk = GraphicsLevel (0 = custom, 1 = min, 2 = low, 3 = mid, 4 = high)
		
		/*Object aja = Reflection.getStaticDeclaredField("add", "dc");
		if (aja != null) {
			Object zn = Reflection.getDeclaredField("aja", aja, "av");
			if (zn != null) {
				int l = (int) Reflection.getDeclaredField("zn", zn, "l");
				
				System.out.println(l * MultiplierFinder.findInverse(258274691));
			}
		}*/
		
		//int[] array = (int[]) Reflection.getStaticDeclaredField("client", "mv");
		//System.out.println(Arrays.toString(array));
		
		//System.out.println(Client.getWorldInfo().getWorld());
		//System.out.println(WorldSelect.get.getScrollBarTopGapHeight());
		
		
		/*for (Object key : ModelCapture.animableObjectModels.keySet()) {
			AnimableObject ao = AnimableObject.create(key);
			if (ao != null) {
				System.out.println(key);
				Reflection.printDeclaredFields(data.EntityNode.get.getInternalName(), key);
				//System.out.println(ao.isDisposed());
			}
		}*/
		
		/*GroundObject[] gos = GroundObjects.getAll(Type.All, -1, false);
		if (gos != null) {
			System.out.println("-START-");
			for (GroundObject go : gos) {
				if (go.getId() == 38760) {
					System.out.println(go.getObject());
				}
			}
			System.out.println("-END-");
		}*/
		
		//System.out.println(Client.getRender());
		//System.out.println(Client.getWorldInfo().getWorld());
		
		//System.out.println(ModelCapture.animableObjectModels.size());
		
		/*AnimableObject ao = (AnimableObject) GroundObjects.getNearest(Type.AnimableObject, 38785);
		if (ao != null) {
			System.out.println("cur ref = " + ao.getObject());
			System.out.println("cur model ref = " + I_AnimableObject.getModel(ao.getObject()));
			System.out.println("cur model: " + ao.getModel_field());
			
			System.out.println("xpoints: " + I_OpenGLModel.getXPoints(I_AnimableObject.getModel(ao.getObject())));
		}*/
		
		/*GrandExchansgeRow row = new GrandExchangeRow();
		row.setItemId(1);
		row.setBuyPrice(99);
		row.setSellPrice(800);
		row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
		
		row = GrandExchangeRow.select(1);
		System.out.println(row.getLastUpdate());*/
		
		//System.out.println(org.rs3.api.GEOffer.FIRST.getSpent());
		
		//System.out.println(ContinueMessage.get.isOpen());
		//System.out.println(ContinueMessage.get.getText(true));
		
		//System.out.println(Lodestone.WILDERNESS_VOLCANO.isActivated());
		
		//System.out.println(Players.getMyPlayer().getAnimationId());
		
		//System.out.println(Bank.get.isOpen());
		
		//Bank.get.getScrollBarBottomGapHeight();
		
		//System.out.println(Toolbelt.Hatchet.getEquipped());
		
		//System.out.println(GESlot.THIRD.getGEOffer().getId());
		
		/*for (int i : org.rs3.api.ItemTable.BANK.getSnapshot().getItemIds()) {
			System.out.println(i);
		}*/
		//System.out.println(org.rs3.api.ItemTable.BANK.getItemIdAt(4));

		
		/*for (GEOffer offer : Client.getGEOffers().getAll()) {
			System.out.println(offer.getStatus() + "  " + offer.getId() + ", " + offer.getPrice() + ", " + offer.getSpend() + ", " + offer.getTotal() + ", " + offer.getTransfered());
		}*/
		
		/*for (Node n : Client.getItemTables().getAll()) {
			System.out.println(n.getId());
		}*/
		
		/*Node n = Nodes.lookup(Client.getItemTables(), 530);
		ItemTable table = n.forceCast(ItemTable.class);
		if (table != null) {
			for (int i = 0; i < table.getItemIds().length; i++) {
				System.out.println(table.getItemIds()[i] + ", " + table.getItemStackSizes()[i]);
			}
		}*/
		
		/*GroundObject[] objects = GroundObjects.getAll(Type.Floor);
		//GroundObject[] objects = GroundObjects.getAllOnScreen(Type.Boundary);
		if (objects != null) {
			for (GroundObject go : objects) {
				//System.out.println(go.getId());
				
				ObjectDefinition def = go.getDefinition();
				if (def != null) {
					//if (def.getName().contains("rid"))
						System.out.println(def.getName() + " " + go.getId());
				}
			}
		}*/
		
		/*GroundObject go = GroundObjects.getNearest(GroundObject.Type.Boundary, 24379, 24381);
		if (go != null) {
			Paint.groundObject = go;
		}*/
		
		/*GroundItem[] items = GroundItems.getAll();
		
		if (items != null) {
			Tile[] tiles = new Tile[items.length];
			
			int i = 0;
			for (GroundItem item : items) {
				tiles[i] = item.getTile();
				
				ItemDefinition def = item.getItemNode().getDefinition();
				
				if (def != null) {
					System.out.println(def.getName() + " " + item.getId());
					
					if (def.getName().contains("ones")) {
						Paint.updatePaintObject(item);
					}
				} else
					System.out.println(item.getId());
				
				System.out.println(item.getTile().getPlane());
				
				i++;
			}
			
			Paint.updatePaintObject(tiles);
		}*/
		
		//System.out.println(Skill.STRENGTH.getCurrentLevel());
		
		//System.out.println(AbilityBar.get.setAutoRetaliate(true));
		//System.out.println(Stats.PRAYER.getPercentage());
		
		//Ribbon.Tab.SKILLS.open();
		
		/*Render render = Client.getRender();
		System.out.println("Absolute X: " + render.getAbsoluteX());
		System.out.println("Absolute Y: " + render.getAbsoluteY());
		System.out.println("X Multiplier: " + render.getXMultiplier());
		System.out.println("Y Multiplier: " + render.getYMultiplier());*/
		
		//Random.randomOrgReseed();
		
		//System.out.println(CombatLocation.BURTHOPE_TROLLS.getArea().contains(Players.getMyPlayer().getData().getCenterLocation().getTile(true)));
		
		/*org.rs3.api.wrappers.Character interacting = Players.getMyPlayer().getInteracting();
		
		if (interacting != null) {
			if (interacting instanceof Npc) {
				Npc npc = (Npc) interacting;
				System.out.println(npc.getNpcDefinition().getName());
			}
		}*/
		
		/*for (Npc npc : Client.getAllNpcs()) {
			if (npc == null)
				continue;
			
			NpcDefinition def = npc.getNpcDefinition();
			
			if (def != null) {
				//System.out.println(npc.getNpcDefinition().getName() 
				//		+ ": " + npc.getData().getCenterLocation().getAbsoluteX() + ", " + npc.getData().getCenterLocation().getAbsoluteY() + ", " + npc.getPlane());
				
				if (def.getName().contains("warrior")) {
				//if (def.getName().contains("Death")) {
					System.out.println(def.getId());
				}
			}
		}*/
		
		
		//BreakHandler.takeBreak(false, 60000);
		
		/*List<int[]> list = Objects.getSprialSquareIndicesAt(Players.getMyPlayer().getData().getCenterLocation().getLocalTile(true), 5);
		List<Tile> tiles = new ArrayList<>();
		
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		int baseX = base.getX();
		int baseY = base.getY();
		
		for (int[] array : list) {
			tiles.add(new Tile(baseX + array[0], baseY + array[1], 0));
		}
		
		Paint.updatePaintObject(tiles.toArray(new Tile[0]));*/
		
		//System.out.println(Players.getMyPlayer().getAnimationId());
		// get all ids
		/*RSAnimable[] objects = Objects.getAllAnimableObjects();
		
		Set<Integer> set = new HashSet<>();
		
		for (RSAnimable animable : objects) {
			if (animable instanceof AnimableObject) {
				AnimableObject ao = (AnimableObject) animable;
				
				ObjectDefinition def = ao.getObjectDef();
				if (def != null && def.getName().equals("Tree")) {
					set.add(ao.getId());
				}
			}
		}
		
		for (Integer i : set) {
			System.out.println(i);
		}*/
		
		
		//if (!Client.isRSDoneLoading())
		//	return;
		
		//if (!Client.isLoggedIn())
		//	return;
		
		//Callbacks.get.printReport();
		
		//System.out.println(Camera.getX() + ", " + Camera.getY() + ", " + Camera.getZ() + ", " + Camera.getPitch() + ", " + Camera.getYaw());
		
		//Paint.updatePaintObject(DepositBox.get.getNearest());
		
		/*for (MethodData md : Callbacks.get.methods) {
			System.out.println(md.getInternalClassName());
		}*/
		
		/*Object qp = Client.getGameInfo().getObject();
		Object agp = Reflection.getDeclaredField("qp", qp, "u");
		
		byte[][][] array = (byte[][][]) Reflection.getDeclaredField("agp", agp, "w");
		if (array != null) {
			byte[][] colFlags = array[0];
			if (colFlags != null) {
				
				List<Tile> list = new ArrayList<>();
				BaseInfo base = Client.getGameInfo().getBaseInfo();
				int baseX = base.getX();
				int baseY = base.getY();
				
				for (int x = 0; x < 104; x++) {
					for (int y = 0; y < 104; y++) {
						byte val = colFlags[x][y];
						
						if (val != 0) {
							list.add(new Tile(baseX + x, baseY + y, 0));
							//System.out.println(b + ": " + x + ", " + y);
						} //else
						//	System.out.println(val);
					}
				}
				
				Paint.updatePaintObject(list.toArray(new Tile[0]));
				
				//System.out.println(colFlags[50][50]);
				//System.out.println(colFlags.length);
				//System.out.println(ArrayUtils.arrayToString(colFlags));
			} else
				System.out.println("null");
		} else
			System.out.println("null");*/
		
		
		
		/*boolean[] array = (boolean[]) Reflection.getStaticDeclaredField("client", "sl");
		
		if (array != null) {
			System.out.println(ArrayUtils.arrayToString(array));
		} else
			System.out.println("null");*/
		
		
		/*int[] array = (int[]) Reflection.getDeclaredField("aeb", Players.getMyPlayer().getObject(), "at");
		
		System.out.println(Players.getMyPlayer().getAnimationId());
		
		if (array != null) {
			System.out.println(ArrayUtils.arrayToString(array));
		} else
			System.out.println("null");*/
		
		//System.out.println(MoneyPouch.getCoinCount());
		
		
		
		
		/*RSAnimable animable = Objects.getNearest(72098);
		if (animable != null) {
			System.out.println(animable.getData().getCenterLocation().getAbsoluteX() + ", " + animable.getData().getCenterLocation().getAbsoluteY());
			AnimatedAnimableObject aao = (AnimatedAnimableObject) animable;
			aao.getAnimatedObject().getCachedAnimatedModel().click(Mouse.LEFT, aao, 0);
			
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				int test = (int)Reflection.getStaticDeclaredField("client", "mv") * 1660507583;
				
				if (test != 0)
					System.out.println(test);
				
				Time.sleep(50);
			}
		}*/
		
		/*Ground[][][] groundArray = Client.getGameInfo().getGroundInfo().getGroundArray();
		
		int absX = 2966;
		int absY = 3234;
		
		BaseInfo base = Client.getGameInfo().getBaseInfo();
		
		int relX = absX - base.getX();
		int relY = absY - base.getY();
		
		if (relX >= 0 && relX < 104 && relY >= 0 && relY < 104) {
			
			Ground ground = groundArray[0][relX][relY];
			if (ground != null) {
				Object ard = Reflection.getDeclaredField("tw", ground.getObject(), "t");
				System.out.println(ard);
			}
		}*/
		
		//System.out.println(Reflection.getStaticDeclaredField("hh", "aw"));

		//System.out.println((int)Reflection.getStaticDeclaredField("client", "mv") * 1660507583);
		
		/*Player my = Players.getMyPlayer();
		Animator animator = my.getAnimator();
		if (animator != null) {
			Object yb = Reflection.getDeclaredField("yv", animator.getObject(), "l");
			Object obj = Reflection.getDeclaredField("yb", yb, "y");
			System.out.println(obj);
		}*/
		/*Object[] aqiArray = (Object[]) Reflection.getDeclaredField("aen", my.getObject(), "by");
		
		if (aqiArray != null) {
			for (Object aqi : aqiArray) {
				if (aqi != null) {
					Animation anim = Animation.create(Reflection.getDeclaredField("yv", aqi, "c"));
					System.out.println(anim.getId());
				}
			}
		}*/
		
		

		
		//System.out.println("client.ro: " + (int)Reflection.getStaticDeclaredField("client", "ro") * 39150423);
		//System.out.println("client.qb: " + (int)Reflection.getStaticDeclaredField("client", "qb") * -1203897903 * 39150423);
		
		//System.out.println("client.mb: " + (int)Reflection.getStaticDeclaredField("client", "mb") * 1884185817);
		//System.out.println("client.mc: " + (int)Reflection.getStaticDeclaredField("client", "mc") * -1648706485);
		
		//System.out.println(Reflection.getStaticDeclaredField("client", "ot"));
		
		
		/*List list = (List) Reflection.getStaticDeclaredField("xj", "w");
		Object xo = list.get(0);
		
		Object xo_u = Reflection.getDeclaredField("xo", xo, "u");
		System.out.println(xo_u);*/
		
		/*Object aa = Reflection.getStaticDeclaredField("ma", "dn");
		
		Object[] vhArray = (Object[]) Reflection.getDeclaredField("aa", aa, "c");
		
		int i = 0;
		for (Object vh : vhArray) {
			Object vh_c = Reflection.getDeclaredField("vh", vh, "c");
			Object vh_y = Reflection.getDeclaredField("vh", vh, "y");
			Object vh_m = Reflection.getDeclaredField("vh", vh, "m");
			
			System.out.println("vh " + i + ": " + vh_c + ", " + vh_y + ", " + vh_m);
			
			i++;
		}*/
		
		// List of all on screen object
		
		/*Object qd = Reflection.getStaticDeclaredField("client", "iw");
		Object agr = Reflection.getDeclaredField("qd", qd, "m");
		Object tn = Reflection.getDeclaredField("agr", agr, "c");
		Object sx = Reflection.getDeclaredField("tn", tn, "bj");
		LinkedList list = (LinkedList) Reflection.getDeclaredField("sx", sx, "g");
		list = (LinkedList) list.clone();
		
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object td = it.next();
			
			if (td != null) {
				Object entity = Reflection.getDeclaredField("td", td, "p");
				if (entity != null) {
					//System.out.println(entity.getClass().getName());
					
					if (entity.getClass().getName().equals(data.AnimableObject.get.getInternalName())) {
						AnimableObject ao = AnimableObject.create(entity);
						//System.out.println(ao.getId());
						ObjectDefinition def = ao.getObjectDef();
						if (def != null) {
							System.out.println(def.getName());
						}
					}
				}
			}
			
		}*/
		
		
		
		
		
		/*Timer timer = new Timer(5000);
		while (timer.isRunning()) {
			int obj = (int) Reflection.getStaticDeclaredField("xy", "p");
			
			if (obj != 433098285)
				System.out.println(obj);
			Time.sleep(5);
		}*/
		
		
		
		/*Object fc = Reflection.getStaticDeclaredField("vn", "ve");
		
		List quList = (List) Reflection.getDeclaredField("fc", fc, "e");
		System.out.println(quList.size());*/
		
		
		/*List quList = (List) Reflection.getDeclaredField("fc", fc, "e");
		
		//System.out.println(quList);
		
		Object resultQu = null;
		Iterator it = quList.iterator();
		while (it.hasNext()) {
			Object qu = it.next();
			
			// et?
			Object qu_f = Reflection.getDeclaredField("qu", qu, "f");
			
			// qp
			Object qu_o = Reflection.getDeclaredField("qu", qu, "o");
			
			
			// et
			Object et_n = Reflection.getStaticDeclaredField("et", "n");
			
			// qp
			Object qp_y = Reflection.getStaticDeclaredField("qp", "y");
			
			if ((qu_f == et_n) && (qu_o == qp_y)) {
				resultQu = qu;
				break;
			}
		}
		
		if (resultQu != null) {
			System.out.println(resultQu);
		}*/
		
		
		/*List quList = (List) Reflection.getDeclaredField("fc", fc, "w");
		
		Object resultQu = null;
		Iterator it = quList.iterator();
		int i1 = 0;
		while (it.hasNext()) {
			Object qu = it.next();
			
			// qp
			Object qu_o = Reflection.getDeclaredField("qu", qu, "o");
			
			// qp
			Object qp_e = Reflection.getStaticDeclaredField("qp", "e");
			
			if ((qu_o == qp_e)) {
				resultQu = qu;
				break;
			}
		}
		
		if (resultQu != null) {
			System.out.println(resultQu);
			
			//Object obj = Reflection.getDeclaredField("qu", resultQu, "j");
			//System.out.println(obj);
		}*/
		
		
		
		//Object qu = Reflection.getDeclaredField("fc", fc, "k");
		
		//Object obj = Reflection.getDeclaredField("fc", fc, "r");
		//System.out.println(obj);
		
		
		//Object test = Reflection.getStaticDeclaredField("qp", "y");
		//System.out.println(test);
		
		/*Object air = Reflection.getStaticDeclaredField("sl", "by");
		if (air != null) {
			System.out.println(Reflection.getDeclaredField("air", air, "i"));
			//byte[] m = (byte[]) Reflection.getDeclaredField("air", air, "m");
			//System.out.println(m.length);
		} else
			System.out.println("null");*/
		
		
		//HashTable nz = HashTable.create(Reflection.getStaticDeclaredField("hv", "p"));
		//System.out.println(nz.getFirst().getNext().getNext().getObject().getClass().getName());
		
		//NodeSubQueue queue = NodeSubQueue.create(Reflection.getStaticDeclaredField("hv", "o"));
		//System.out.println(queue.getHead().getNextSub().getObject().getClass().getName());
		
		
		/*Map map = (Map) Reflection.getStaticDeclaredField("hv", "g");
		
		for (Object hz : map.values()) {
			//System.out.println(Reflection.getDeclaredField("hz", hz, "p"));
			System.out.println("BEGIN");
			Object[] apgArray = (Object[]) Reflection.getDeclaredField("hz", hz, "p");
			for (Object apg : apgArray) {
				if (apg != null)
					System.out.println(Reflection.getDeclaredField("apg", apg, "i"));
			}
			System.out.println("END");
		}*/
		
		
		//System.out.println(map.size());
		
		//System.out.println("e1: " + ((e1 != null) ? e1.getClass().getName() : "null"));
		
		// set coins in money pouch, inventory, and bank
		/*try {
			Class<?> ev = CustomClassLoader.get.loadClass("ev");
			if (ev != null) {
				Method n = ev.getDeclaredMethod("n", new Class<?>[] {int.class, int.class, int.class, int.class, boolean.class, int.class});
				if (n != null) {
					n.setAccessible(true);
					n.invoke(null, new Object[] {623, 0, 995, 10000, false, -1907775368});
					n.invoke(null, new Object[] {93, 0, 995, 10000, false, -1907775368});
					n.invoke(null, new Object[] {95, 4, 995, 10000, false, -1907775368});
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		
		//loadModscript();
		
		//reflectionDebug();
		
		/*Npc nearest = null;
		double dist = Double.MAX_VALUE;
		
		for (Npc npc : Client.getNpcs()) {
			if (npc == null)
				continue;
			
			NpcDefinition def = npc.getNpcDefinition();
			
			if (def != null) {
				//System.out.println(npc.getNpcDefinition().getName() 
				//		+ ": " + npc.getData().getCenterLocation().getAbsoluteX() + ", " + npc.getData().getCenterLocation().getAbsoluteY() + ", " + npc.getPlane());
				
				if (def.getName().contains("calf")) {
					//Paint.updatePaintObject(npc, ModelCapture.charactersModelCapture);
					
					double curDist = Calculations.distanceTo(npc);
					if (curDist < dist) {
						nearest = npc;
						dist = curDist;
					}

					
				}
			}
		}
		
		if (nearest != null) {
			System.out.println("found npc");
			
			Paint.updatePaintObject(new Tile[] {nearest.getData().getCenterLocation().getTile(true)});
			
			Timer timer = new Timer(15000);
			
			while (timer.isRunning()) {
				Object test = Reflection.getDeclaredField("wt", nearest.getObject(), "f");
				System.out.println(test);
				//System.out.println(nearest.isDisposed());
				
				Time.sleep(50);
			}
		}*/
		
		
		
		//CanvasCapture.saveScreenshot(false, new File("C:\\screenshots\\"), "screen.jpg");
		
		//System.out.println(Menu.isOpen());
		
		/*Client.updateCache_componentNodes();
		Client.updateCache_widgetCache();
		System.out.println("Component nodes: " + Client.cache_componentNodes.size());*/
		
		//System.out.println("Canvas size: " + Client.getCanvasWidth() + ", " + Client.getCanvasHeight());
		
		

		
		//Antiban.randomRightClickRSAnimable();
		//Antiban.randomRightClickItem();
		
		//if (DepositBox.isOpen())
		//	DepositBox.close();
		
		//Interfaces.closeUselessInterfaces(Interfaces.TABS_CLOSE_ID);
		
		//int cameraAngle = Camera.getAngleTo(0);
		//if (cameraAngle < 0)
		//	cameraAngle = Math.abs(cameraAngle) + 180;
		
		//System.out.println(Camera.getYaw());
		//System.out.println(Camera.getPitch());
		
		/*TilePath path = miningToBank.clone();
		path.randomize(2, 2);
		
		Timer timer = new Timer(5000);
		while (timer.isRunning() && !path.complete()) {
			path.traverse();
			Time.sleep(500, 600);
		}*/
		
		/*RSAnimable<?> deposit = Objects.getNearest(36788);
		if (deposit != null) {
			if (deposit instanceof AnimableObject) {
				AnimableObject ao = (AnimableObject) deposit;
				//Paint.updatePaintObject(ao);
				Paint.updatePaintObject(ao.getData().getCenterLocation().getMapPoint());
			}
		}*/
		
		/*Area area = new Area(new Tile(2964, 3250, 0), new Tile(2984, 3225, 0));
		
		if (area.contains(Players.getMyPlayer().getData().getCenterLocation().getTile(false)))
			System.out.println("yes");
		else
			System.out.println("no");*/
		
		//System.out.println(Players.getMyPlayer().getAnimationId());
		
		//System.out.println(Client.getSettingData().getSettings().get(Settings.BOOLEAN_RUN_ENABLED));
		//Walking.walk(new Tile(2967, 3233, 0));
		
		// Get all animable objects
		/*RSAnimable<?>[] animables = Objects.getAllAnimableObjects();
		for (RSAnimable<?> animable : animables) {
			if (animable != null) {
				if (animable instanceof AnimableObject) {
					AnimableObject ao = (AnimableObject) animable;
					
					ObjectDefinition def = ao.getObjectDef();
					if (def != null && def.getName().contains("Bank")) {
						System.out.println("Non-Animated: " + def.getName() + ": " + ao.getId());
						Paint.updatePaintObject(ao);
						
						Model<?> model = ao.getCachedAnimatedModel();
						
						if (model != null) {
							//model.interact("Mine", aao, 0);
							Paint.updatePaintObject(model.getRandomPoint(ao, 0));
						}
					}
				}
				
				if (animable instanceof AnimatedAnimableObject) {
					AnimatedAnimableObject aao = (AnimatedAnimableObject) animable;
					
					ObjectDefinition def = aao.getObjectDef();
					if (def != null && def.getName().contains("Bank")) {
						System.out.println("Animated: " + def.getName() + ": " + aao.getAnimatedObject().getId());
						Paint.updatePaintObject(aao);
						
						AnimatedObject animated = aao.getAnimatedObject();
						if (animated != null) {
							Model<?> model = animated.getCachedAnimatedModel();
							
							if (model != null) {
								//model.interact("Mine", aao, 0);
								Paint.updatePaintObject(model.getRandomPoint(aao, 0));
							}
						}
					}
				}
			}
		}*/
		
		//path.traverse();
		
		// Drop inventory
		//Inventory.dropAllItems();
		
		//if (true) return;
		
		// Print actions
		/*for (Component[] cs : Client.cache_widgetComponentCache) {
			if (cs != null) {
				for (Component c : cs) {
					if (c != null) {
						String[] actions = c.getActions();
						if (actions != null) {
							for (String action : actions) {
								System.out.println(action);
							}
						}
					}
				}
			}
		}*/
		
		//System.out.println(Client.getMinimapAngle() + ", " + Client.getMinimapScale() + ", " + Client.getMinimapOffset() + ", " + Client.getMinimapSetting());
        
		
		/*for (Rectangle r : Client.componentBounds) {
			System.out.println("rect " + r.x + ", " + r.y);
		}*/
		
		/*Player my = Client.getMyPlayer();
		if (my != null) {
			System.out.println(my.getAnimationId());
		}*/
		
		
		//System.out.println("Inventory count: " + Inventory.count());
        
		
        // Setup event queue
		/*setupInput(null);
		
        System.out.println(Toolkit.getDefaultToolkit().getSystemEventQueue().getClass());
        
        final Thread thread = new Thread() {
        	@Override
        	public void run() {
        		try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
        		System.out.println("hhhhhhhh");
        		
        		if (nazi != null) {
        			//nazi.moveMouse(100, 100);
        			//nazi.clickMouse(100, 100, 1);
        			nazi.sendKeys("abc", 90, 60);
        		} else
        			System.out.println("nazi null!");
        	}
        };
		thread.start();*/

		
        // Add components to paint
        /*final Thread thread = new Thread() {
        	@Override
        	public void run() {
		List<Rectangle> rects = new ArrayList<>();
        
		Widget[] widgets = Client.getValidatedWidgetCache(false);
		System.out.println("Widgets: " + widgets.length);
		Rectangle[] componentBoundsArray = Client.getComponentBoundsArray();
		
		// 2nd level children
		for (int widgeti = 0; widgeti < widgets.length; widgeti++) {
			//if (widgets[widgeti] != null && widgets[widgeti].validate(widgeti)) {
			if (widgets[widgeti] != null) {
				Component[] childComponents = widgets[widgeti].getComponents();
				
				for (Component child123 : childComponents) {
					Component[] child123components;
					if (child123 != null && child123.isVisible() && (child123components = child123.getComponents()) != null) {
						
						for (Component child : child123components) {
							if (child != null && child.isVisible()) {
								
								//int id = child.getId();
								//final int x = id >> 16;
								//final int y = id & 0xffff;
								
								Point loc = child.__getAbsoluteLocation(true, componentBoundsArray);
								rects.add(new Rectangle(loc.x, loc.y, child.getWidth(), child.getHeight()));
							}
						}
					}
				}
			}
		}
		
		
		
		// 1st level children
		for (int widgeti = 0; widgeti < widgets.length; widgeti++) {
			//if (widgets[widgeti] != null && widgets[widgeti].validate(widgeti)) {
			if (widgets[widgeti] != null) {
				
				for (Component child : widgets[widgeti].getComponents()) {
					if (child != null && child.isVisible()) {
						
						//int id = child.getId();
						//final int x = id >> 16;
						//final int y = id & 0xffff;
						
						//Component[] children = child.getComponents();
						//if (children != null) {
						//	if (children.length > 27) {
						//		System.out.println(id + " " + children.length);
						//	}
						//}
						
						Point loc = child.getAbsoluteLocation();
						rects.add(new Rectangle(loc.x, loc.y, child.getWidth(), child.getHeight()));
					}
				}
			}
		}
		
		
		System.out.println("done!");
		
		Rectangle[] array = rects.toArray(new Rectangle[0]);
		
		if (Paint.components == null)
			Paint.components = array;
		
		if (Paint.components != null) {
			synchronized (Paint.components) {
				Paint.components = array;
			}
		}
        	}
        };
        thread.start();*/
		
		
		/*Component[] children = Client.getCustom();
		
		for (Component child : children) {
			System.out.println(child.getId());
			
			Point loc = child.getAbsoluteLocation();
			
			//if (loc.x >= 0 && loc.y >= 0 && child.getWidth() > 0 && child.getHeight() > 0)
				rects.add(new Rectangle(loc.x, loc.y, child.getWidth(), child.getHeight()));
		}*/
	}
	
	public void reflectionDebug() {
		//Tile copperTile = new Tile(2966, 3234, 0);
		//RSAnimable<?>[] animables = Objects.getAnimableObjectsAt(copperTile.x, copperTile.y);
		
		//if (animables != null) {
		//	if (animables[0] instanceof AnimatedAnimableObject) {
				
				//AnimatedAnimableObject aao = (AnimatedAnimableObject) animables[0];
				//AnimatedObject ao = aao.getAnimatedObject();
				Player player = Players.getMyPlayer();
				if (player == null)
					return;
		
				// own - 820 - c4bd551c53aa850483864c7e24557a02e62f1083
				
				
				
				// AnimatedObject - int k field : -634760677 to 1587764713 randomly, not when rock is mined
				// AnimatedObject - int l changes randomly until rock is mined; might be an animation index; this could also be used to test if rock is disposed (291000513 is the result when disposed which could be 0 or -1 when multiplier is used)
				
				// EntityNode - boolean wt.b and wt.c and wt.f false when rock spawned, true when not (name could be EntityNode.disposed)
				
				
				// RSAnimable - sole byte field might be plane
				// AnimatedObject - sole byte field might be plane
				
				
				Timer timer = new Timer(4000);
				
				while (timer.isRunning()) {
					//System.out.println("Testing...");
					
					
					/*Interactable<?> inter = aao.getAnimatedObject().getInteractable();
					
					if (true) {
						if (inter != null) {
							System.out.println(inter.getData().getCenterLocation().getAbsoluteX());
						} else
							System.out.println("null");
						
						Time.sleep(50);
						continue;
					}*/
					
					
					
					Object test = Reflection.getDeclaredField("aeo", player.getObject(), "x");
					System.out.println(test);
					
					// array
					/*if (test != null) {
						Object[] array = (Object[]) test;
						
						System.out.println(array.length);
						
						/*if (array.length > 0) {
							for (int i = 0; i < array.length; i++) {
								if (array[i] != null) {
									Object test2 = Reflection.getDeclaredField("ahd", array[i], "m");
									System.out.println(test2);
								}
							}
						}*/
					/*} else
						System.out.println("null");*/
					
					
					// field of field
					/*if (test != null) {
						Object test2 = Reflection.getDeclaredField("vd", test, "s");
						System.out.println(test2);
					} else
						System.out.println("null");*/
						
					
					
					// known wrapper
					/*Node<?> wrapper = Node.create(I_Node.create(test));
					
					if (wrapper != null) {
						System.out.println(wrapper.getId());
					} else
						System.out.println("null");*/
					
					
					Time.sleep(50);
				}
			//}
		//}
	}
	
	private void loadModscript() {
		File modScriptDirectory = new File("C:\\modscript\\" + data.Client.get.getBuildNumberString() + "\\");
		File xmlFile = new File(modScriptDirectory, gamepackFetcher.getSha1Hex() + ".xml");
		
		if (!xmlFile.exists()) {
			modScriptDirectory = new File("C:\\modscript\\");
			
			for (File file : modScriptDirectory.listFiles()) {
				if (file.isDirectory()) {
					for (File subFile : file.listFiles()) {
						if (subFile.getName().contains(gamepackFetcher.getSha1Hex())) {
							xmlFile = subFile;
							break;
						}
					}
				} else {
					if (file.getName().contains(gamepackFetcher.getSha1Hex())) {
						xmlFile = file;
						break;
					}
				}
			}
		}
		
		if (xmlFile.exists())
			XMLLoader.loadHookDataFromXML(xmlFile);
		else
			System.out.println("Error: unable to find modscript xml file!");
	}
	
	public void loadAccountInfo() {
		File accountInfoDirectory = new File("C:\\accounts\\");
		//File iniFile = new File(accountInfoDirectory, "current.ini");
		File xmlFile = new File(accountInfoDirectory, "info.xml");
		
		if (/*iniFile.exists() &&*/ xmlFile.exists())
			AccountInfo.loadCurrentAccountDataFromXML(xmlFile);
		else
			System.out.println("Error: unable to find account info xml file!");
	}
	
	
	// bot TODO - !!! if object is outside the rs display distance, clicking it is not possible; ensure the model is within view distance (7-15 tiles or something)
	
	//
	// OFFICIAL CLIENT CREATED: - do not use on own client, only official client
	//
	// darklance247@outlook.com
	
	
	//
	// OWN CLIENT CREATED:
	//

	
	
	//
	// BANNED
	//
	// wisevoidone@outlook.com
	// moatsnowelf@outlook.com
	// verac2872@outlook.com
	// iiritsail@outlook.com (login) / iritsail@outlook.com (email)
	// fightsmith2@outlook.com
	// harpersave@outlook.com
	// astralodin@outlook.com
	// archonrune@outlook.com
	// frozensand@outlook.com
	// mageotto@outlook.com
	// cairnpriest@outlook.com
	// devoutloki@outlook.com
	// fastevoke@outlook.com
	// heatedfury@outlook.com
	// battlesand@outlook.com
	// clericrain23@outlook.com
	// crowstrike@outlook.com
	// gladylygood2@outlook.com
	// golemfire@outlook.com
	// corruptsea@outlook.com
	// deviant3000@hotmail.com
	// hero_of_war4@hotmail.com
	// darkwarrior576@hotmail.com
	
	// Icecreamtruck2
	@SuppressWarnings("deprecation")
	protected void initialize() {
		try {
			if (recordMouseMode) {
				//
				// Mouse recorder
				//
				// Notes
				
				//recorder.loadMouseData();
				//recorder.rebaseMouseDataTime();
				//recorder.printMouseData();
				//recorder.flushToFile();
				
				comp = new Composite(shell, SWT.EMBEDDED);
				comp.setBounds(0, 0, 1100, 800);
				
				final java.awt.Frame frame1 = SWT_AWT.new_Frame(comp);
				
				final BufferedImage buffer = new BufferedImage(1920, 1080, BufferedImage.TYPE_INT_ARGB);
				
				final Panel panel1 = new Panel();
				panel1.addMouseListener(recorder);
				//panel1.addMouseMotionListener(recorder);
				panel1.setIgnoreRepaint(true);
				
				Thread paintThread = new Thread() {
					public void run() {
						while (!shell.isDisposed()) {
							recorder.draw(buffer.getGraphics());
							if (!shell.isDisposed() && panel1.getGraphics() != null)
								panel1.getGraphics().drawImage(buffer, 0, 0, null);
							Time.sleep(16);
						}
					}
				};
				paintThread.start();
				
				frame1.add(panel1);
				
				shell.pack();
				//shell.layout();
				
				this.awtFrame = frame1;
				this.panel = panel1;
				
				LogManager.getLogManager().reset();
				Logger logger = Logger.getLogger(GlobalScreen.class.getName());
				logger.setLevel(Level.OFF);
				
				try {
					if (GlobalScreen.isNativeHookRegistered())
						GlobalScreen.unregisterNativeHook();
					
					GlobalScreen.registerNativeHook();
				} catch (NativeHookException e) {
					e.printStackTrace();
				}
				
				//GlobalScreen.addNativeMouseListener(recorder);
				GlobalScreen.addNativeMouseMotionListener(recorder);
				
				return;
			}
			
			gamepackFetcher = new GamePackFetcher(ProxySetup.getProxy(), false, false, false);
			
			ClassData.setClasses();
			ClassData.setAllClassNames();
			ClassData.setAllRequiredClassNames();
			
			int[] buildNumber = gamepackFetcher.getBuildNumber();
			if (buildNumber != null)
				data.Client.get.setVersionInfo(buildNumber[0], buildNumber[1]);
			
			loadModscript();
			//ServerInfo.loadServerInfo();
			//ServerInfo.cur = new ServerInfo(7);
			loadAccountInfo();
			
			System.out.println(gamepackFetcher.getGamepackJar().getAbsolutePath());
			
			// Find gamepack classloader
			
			/*FieldData classLoaderHolder = null;
			FieldData classLoader = null;
			
			GamePackUpdater updater = new GamePackUpdater(gamepackFetcher.getGamepackJar().getAbsolutePath());
			updater.run();
			
			if (updater.isSuccessful()) {
				classLoaderHolder = updater.classLoaderHolder;
				classLoader = updater.classLoader;
			} else {
				OldGamePackUpdater oldupdater =  new OldGamePackUpdater(gamepackFetcher.getGamepackJar().getAbsolutePath());
				oldupdater.run();
				
				if (oldupdater.isSuccessful()) {
					classLoaderHolder = oldupdater.classLoaderHolder;
					classLoader = oldupdater.classLoader;
				}
			}
			
			
			System.out.println(classLoaderHolder.getInternalClassName() + "." + classLoaderHolder.getInternalName());
			System.out.println(classLoader.getInternalClassName() + "." + classLoader.getInternalName());*/
			// IMPORTANT! Comment below "CustomClassLoader.get.setFields(...)" to fully disable
			
			
			//CustomClassLoader.get = new CustomClassLoader(new URLClassLoader(new URL[] {new URL("file:" + gamepackFetcher.getGamepackJar().getAbsolutePath())}));
			CustomClassLoader.get.setUrlLoader(new URLClassLoader(new URL[] {new URL("file:" + gamepackFetcher.getGamepackJar().getAbsolutePath())}));
			
			final Applet rs2app = (Applet) CustomClassLoader.get.urlLoader.loadClass("Rs2Applet").newInstance();
			this.rs2app = rs2app;
			CustomClassLoader.get.setApplet(rs2app);
			//CustomClassLoader.get.setFields(classLoaderHolder, classLoader);
			
			rs2app.setPreferredSize(new Dimension(1024, 768));
			rs2app.setStub(this);
			rs2app.setVisible(true);
			
			// Create a panel which the applet will be inserted into; this is not affected by offsets which would be a problem in adding to a JFrame
			comp = new Composite(shell, SWT.EMBEDDED);
			comp.setBounds(0, toolBar.getBounds().height - 5, rs2app.getPreferredSize().width, rs2app.getPreferredSize().height);
			
			final java.awt.Frame frame = SWT_AWT.new_Frame(comp);
			// these window events are specific to the inner frame that contains the applet (rs); use shell for checking actual window events
			frame.addWindowListener(new WindowAdapter() {
	            @Override
	            public void windowActivated(WindowEvent e) {
	            	//System.out.println("act");
	                frame.requestFocusInWindow();
	                //activated = true;
	            }
	            
	            @Override
	            public void windowDeactivated(WindowEvent e) {
	            	//System.out.println("de");
	                //activated = false;
	            }
	            
	            @Override
	            public void windowDeiconified(WindowEvent e) {
	                frame.requestFocusInWindow();
	                minimized = false;
	            }
	            
	            @Override
	            public void windowIconified(WindowEvent e) {
	                minimized = true;
	            }
	            
	            @Override
				public void windowClosing(WindowEvent we){
	            	System.out.println("closing!");
					frame.dispose();
				}
			});
			frame.setFocusTraversalKeysEnabled(false);
			frame.addKeyListener(new KeyAdapter() {
	            @Override
	            public void keyTyped(KeyEvent e) {
	                if (ScriptHandler.nazi != null) {
	                	ScriptHandler.nazi.passKeyEvent(e);
	                }
	            }
	            @Override
	            public void keyPressed(KeyEvent e) {
	            	if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_S) {
	    				getDisplay().asyncExec(new Runnable() {
	    				    @Override
	    					public void run() {
	    				    	System.out.println("Starting/stopping script...");
	    				    	onPlayButtonClick();
	    				    }
	    				});
	            	}
	            	
	            	if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_A) {
	    				getDisplay().asyncExec(new Runnable() {
	    				    @Override
	    					public void run() {
	    				    	System.out.println("Setting filter status...");
	    				    	tltmFilterAll.setSelection(!tltmFilterAll.getSelection());
	    				    	setFilterStatus(tltmFilterAll.getSelection());
	    				    }
	    				});
	            	}
	            	
	            	if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_C) {
	    				getDisplay().asyncExec(new Runnable() {
	    				    @Override
	    					public void run() {
	    				    	System.out.println("Clearing filters...");
	    				    	clearFilters();
	    				    }
	    				});
	            	}
	            	
	                if (ScriptHandler.nazi != null) {
	                	ScriptHandler.nazi.passKeyEvent(e);
	                }
	            }
	            @Override
	            public void keyReleased(KeyEvent e) {
	                if (ScriptHandler.nazi != null) {
	                	ScriptHandler.nazi.passKeyEvent(e);
	                }
	            }
	        });
			
			Panel panel = new Panel();
			panel.setPreferredSize(rs2app.getPreferredSize());
			panel.add(rs2app);
			
			frame.add(panel);
			
			shell.pack();
			//shell.layout();
			
			toolBar.setSize(shell.getClientArea().width, 36);

			rs2app.init();
			rs2app.start();
			
			this.awtFrame = frame;
			this.panel = panel;
			
			org.rs3.api.wrappers.Client.applet = rs2app;
			org.rs3.api.wrappers.Client.g = rs2app.getGraphics();
			org.rs3.api.wrappers.Client.peer = rs2app.getPeer();
			
			//Paint.startDrawThread();
			
			// Set client static accessor / appletloader if necessary
			for (Field field : rs2app.getClass().getDeclaredFields()) {
				//System.out.println(field.getType().getName());
				field.setAccessible(true);
				
				Object val = field.get(rs2app);
				
				if (val == null) continue;
				
				if (val.getClass().getName().equals("client")) {
					System.out.println("Rs2Applet ClassLoader classname: " + val.getClass().getClassLoader().getClass().getName());
					System.out.println("Rs2Applet." + field.getName());
					CustomClassLoader.get.setAppletLoader(val.getClass().getClassLoader());
					org.rs3.api.wrappers.Client.setObject(val);
				}
				System.out.println(field.getName() + ": " + val.getClass().getName() + "(" + field.getType().getName() + ")");
			}
			
			Thread serverThread = Server.getNewThread();
			serverThread.start();
			
			//data.Client.get.printReport();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void runScript() {
		Thread thread = new Thread() {
			
			@Override
			public void run() {
				Thread scriptThread = ScriptHandler.getNewScriptThread(false, false);
				scriptThread.start();
				
				try {
					scriptThread.join();
				} catch (InterruptedException e) {
					ScriptHandler.currentScript.interruptStop();
					e.printStackTrace();
				} finally {
					setScriptPlayButton(false);
				}
			}
		};
		thread.start();
	}
	
	@Override
	public void appletResize(int width, int height) {
		System.out.println("appletResize(...); TODO: possibly detectable");
	}
	
	@Override
	public AppletContext getAppletContext() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public URL getCodeBase() {
		return gamepackFetcher.getCodeBase();
	}
	
	@Override
	public URL getDocumentBase() {
		return gamepackFetcher.getDocumentBase();
	}
	
	@Override
	public String getParameter(String name) {
		String param = gamepackFetcher.getClientParams().get(name);
		return (param != null) ? param : "";
	}
	
	@Override
	public boolean isActive() {
		return true;
	}
}