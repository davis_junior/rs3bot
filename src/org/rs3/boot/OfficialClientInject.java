package org.rs3.boot;

import gamepack.GamePackFetcher;
import gamepack.InnerPackDecrypter;

import java.applet.Applet;
import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.Authenticator;
import java.net.ProxySelector;
import java.net.Socket;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.api.input.Input;
import org.rs3.api.input.realmouse.MouseData;
import org.rs3.api.wrappers.Client;
import org.rs3.callbacks.proxy.ProxySetup;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.Database;
import org.rs3.database.ScriptRow;
import org.rs3.database.ServerInfo;
import org.rs3.eventqueue.BlockingEventQueue;
import org.rs3.eventqueue.EventNazi;
import org.rs3.ftp.Ftp;
import org.rs3.instrument.ClassWriterFixed;
import org.rs3.instrument.SimpleMain;
import org.rs3.instrument.transformers.callbacks.CallbackMethodsTransformer;
import org.rs3.instrument.transformers.callbacks.CallbackMultipleMethodsTransformer;
import org.rs3.instrument.transformers.debug.DebugTransformer;
import org.rs3.instrument.transformers.officialclient.DisableBrowserTransformer;
import org.rs3.instrument.transformers.officialclient.Rs2AppletTransformer;
import org.rs3.instrument.transformers.opengl.OwnJOGLTransformer;
import org.rs3.instrument.transformers.reverse.GetfieldCallTransformer;
import org.rs3.instrument.transformers.reverse.GetstaticCallTransformer;
import org.rs3.instrument.transformers.reverse.MethodCallTransformer;
import org.rs3.instrument.transformers.reverse.MethodExitTransformer;
import org.rs3.instrument.transformers.reverse.OwnMethodCallTransformer;
import org.rs3.instrument.transformers.reverse.PutfieldCallTransformer;
import org.rs3.instrument.transformers.reverse.PutstaticCallTransformer;
import org.rs3.remote.Server;
import org.rs3.scripts.AccountCreator;
import org.rs3.scripts.Combat;
import org.rs3.scripts.Miner;
import org.rs3.scripts.Runespan;
import org.rs3.scripts.Script;
import org.rs3.scripts.ScriptHandler;
import org.rs3.scripts.Tanner;
import org.rs3.scripts.WildernessBoneCollector;
import org.rs3.scripts.WineCollector;
import org.rs3.scripts.Woodcutter;
import org.rs3.util.FileCleaner;
import org.rs3.util.NetworkDrives;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import analyzers.BuildNumberAnalyzer_ASM;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;

import tk.ivybits.agent.AgentLoader;
import tk.ivybits.agent.Tools;
import data.ClassData;
import data.XMLLoader;

public class OfficialClientInject {
	
	public static java.awt.Frame frame = null;
	public static Applet rs2app = null;
	
	public static boolean blockingInput = true;
	
	public static boolean runescapeLoaded = false;
	
	public static volatile boolean paint = false;
	
	public static String sha1 = null;
	
	public static void injectMain() throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException, ClassNotFoundException {
		//NetworkDrives.map();
		ProxySelector.setDefault(ProxySetup.getProxySelector());
		FileCleaner.loadNetworkRunescapeCache();
		FileCleaner.deleteRunescapeFiles();
		BootType.set(BootType.OFFICIAL_CLIENT);
		Frame.attachAgent();
		//attachJRebelAgent();
		
		Thread loadMouseDataThread = new Thread() {
			public void run() {
				MouseData.getMouseData();
			}
		};
		loadMouseDataThread.start();
	}
	
	// TODO: attaching does not work even after renaming premain() to agentmain() in AgentInstall of jrebel
	// for remoting, the -javaagent vm option is required
	// remoting/jetty will not start without -javaagent
	// i speculate that the premain method contains code that must be run before the application starts
	// possible workaround is to use jvmti agent
	public static void attachJRebelAgent() throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException, ClassNotFoundException {
		System.out.println("Attaching JRebel agent...");
		System.setProperty("rebel.remoting_plugin", "true");
		System.setProperty("rebel.remoting_port", "1234");
		System.setProperty("rebel.log", "true");
		System.setProperty("rebel.log.file", System.getProperty("user.home") + "\\.jrebel\\jrebel.log");
		System.setProperty("rebel.metadata.path", System.getProperty("user.home") + "\\.jrebel\\");
		Tools.loadAgentLibrary();
		AgentLoader.attachAgentToJVM(Tools.getCurrentPID(), Class.forName("com.zeroturnaround.javarebel.java5.AgentInstall"));
	}
	
	public static void injectBeforeStart(Object objApplet) {
		System.setProperty("https.protocols", "TLSv1.1,TLSv1.2"); // the offical client sets this to SSLv3, we want this instead
		
		if (objApplet == null) {
			System.out.println("Injection obj is null!");
			return;
		}
		
		System.out.println("Injection grabbed applet " + objApplet);
		
		Applet applet = (Applet) objApplet;
		
		System.out.println("Attempting database connection...");
		System.out.println("Database connection valid: " + Database.setupSql(2));
		
		//System.out.println(gamepackFetcher.getGamepackJar().getAbsolutePath());
		
		// Find gamepack classloader
		
		/*FieldData classLoaderHolder = null;
		FieldData classLoader = null;
		
		GamePackUpdater updater = new GamePackUpdater(gamepackFetcher.getGamepackJar().getAbsolutePath());
		updater.run();
		
		if (updater.isSuccessful()) {
			classLoaderHolder = updater.classLoaderHolder;
			classLoader = updater.classLoader;
		} else {
			OldGamePackUpdater oldupdater =  new OldGamePackUpdater(gamepackFetcher.getGamepackJar().getAbsolutePath());
			oldupdater.run();
			
			if (oldupdater.isSuccessful()) {
				classLoaderHolder = oldupdater.classLoaderHolder;
				classLoader = oldupdater.classLoader;
			}
		}
		
		
		System.out.println(classLoaderHolder.getInternalClassName() + "." + classLoaderHolder.getInternalName());
		System.out.println(classLoader.getInternalClassName() + "." + classLoader.getInternalName());*/
		// IMPORTANT! Comment below "CustomClassLoader.get.setFields(...)" to fully disable
		
		
		//CustomClassLoader.get = new CustomClassLoader(null);
		//CustomClassLoader.get = new CustomClassLoader(new URLClassLoader(new URL[] {new URL("file:" + gamepackFetcher.getGamepackJar().getAbsolutePath())}));
		
		OfficialClientInject.rs2app = applet;
		OfficialClientInject.frame = (java.awt.Frame) applet.getParent().getParent();
		
		CustomClassLoader.get.setApplet(rs2app);
		//CustomClassLoader.get.setFields(classLoaderHolder, classLoader);
		
		/*rs2app.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (nazi != null) {
                    nazi.passKeyEvent(e);
                }
            }
            @Override
            public void keyPressed(KeyEvent e) {
                if (nazi != null) {
                    nazi.passKeyEvent(e);
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (nazi != null) {
                    nazi.passKeyEvent(e);
                }
            }
        });*/
		
		start();
	}
	
	public static String javConfigReadLine(String line) {
		System.out.println(line);
		
		if (line != null) {
			if (line.contains("advert_height"))
				return "advert_height=0";
			else if (line.contains("window_preferredwidth"))
				return "window_preferredwidth=1100";
			else if (line.contains("window_preferredheight"))
				return "window_preferredheight=768";
			/*else if (line.contains("applet_minwidth"))
				return "applet_minwidth=1024";
			else if (line.contains("applet_minheight"))
				return "applet_minheight=768";*/
		}
		
		return line;
	}
	
	public static void analyzeGamepack(byte[] unpacked, byte[] packed) {
		ClassData.setClasses();
		ClassData.setAllClassNames();
		ClassData.setAllRequiredClassNames();
		
		HashMap<String, byte[]> classes = InnerPackDecrypter.decryptPack(unpacked);
		if (classes != null) {
			int[] buildNumber = GamePackFetcher.getBuildNumber(classes.get("client"));
			if (buildNumber != null)
				data.Client.get.setVersionInfo(buildNumber[0], buildNumber[1]);
		}
		
		System.out.println("Build number: " + data.Client.get.getBuildNumberString());
		sha1 = GamePackFetcher.getSHA1(packed);
		System.out.println("SHA1: " + sha1);
		
		loadModscript();
	}
	
	public static Script loadCurrentScript() {
		if (AccountInfo.cur == null)
			return null;
		
		AccountRow accountRow = AccountRow.select(AccountInfo.cur.getId());
		if (accountRow == null) {
			System.out.println("Unable to load account row! Stopping bot...");
			return null;
		}
		
		ScriptRow scriptRow = ScriptRow.select(accountRow.getScriptId());
		if (scriptRow == null) {
			System.out.println("Unable to load script row! Stopping bot...");
			return null;
		}
		
		String scriptName = scriptRow.getScriptName();
		if (scriptName.equals("Combat"))
			return new Combat();
		else if (scriptName.equals("Miner"))
			return new Miner();
		else if (scriptName.equals("Tanner"))
			return new Tanner();
		else if (scriptName.equals("WildernessBoneCollector"))
			return new WildernessBoneCollector();
		else if (scriptName.equals("Woodcutter"))
			return new Woodcutter();
		else if (scriptName.equals("WineCollector"))
			return new WineCollector();
		else if (scriptName.equals("Runespan"))
			return new Runespan();
		else if (scriptName.equals("AccountCreator"))
			return new AccountCreator();
		
		return null;
	}
	
	public static boolean loadScriptInfo() {
		Script curScript = loadCurrentScript();
		if (curScript != null) {
			ScriptHandler.setupSingleScriptMode(curScript);
			return true;
		}
		
		System.out.println("Unable to determine script! Stopping bot...");
		return false;
	}
	
	public static void start() {
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread arg0, Throwable arg1) {
				System.out.println("Uncaught exception handler: ");
				arg1.printStackTrace();
			}
		});
		
		//rs2app.setPreferredSize(new Dimension(1024, 768));
		
		Input.setUseHardWareInput(true);
		
		//ScriptHandler.setupSingleScriptMode(new Tanner());
		
		
		/*ClassData.setClasses();
		
		//int[] buildNumber = gamepackFetcher.getBuildNumber();
		int[] buildNumber = new int[] { 862, 1 };
		if (buildNumber != null)
			data.Client.get.setVersionInfo(buildNumber[0], buildNumber[1]);
		
		loadModscript();*/
		ServerInfo.loadServerInfo();
		loadAccountInfo();
		
		if (AccountInfo.cur == null) {
			System.out.println("Unable to load account info! Stopping bot...");
			return;
		}
		
		int accountId = AccountInfo.cur.getId();
		System.out.println("Account id: " + accountId);
		
		if (!loadScriptInfo())
			return;
		
		
		org.rs3.api.wrappers.Client.applet = rs2app;
		org.rs3.api.wrappers.Client.g = rs2app.getGraphics();
		org.rs3.api.wrappers.Client.peer = rs2app.getPeer();
		
		//Paint.startDrawThread();
		
		// Set client static accessor / appletloader if necessary
		for (Field field : rs2app.getClass().getDeclaredFields()) {
			try {
				//System.out.println(field.getType().getName());
				field.setAccessible(true);
				
				Object val = field.get(rs2app);
					
				if (val == null) continue;
				
				if (val.getClass().getName().equals("client")) {
					System.out.println("Rs2Applet ClassLoader classname: " + val.getClass().getClassLoader().getClass().getName());
					System.out.println("Rs2Applet." + field.getName());
					CustomClassLoader.get.setAppletLoader(val.getClass().getClassLoader());
					org.rs3.api.wrappers.Client.setObject(val);
				}
				System.out.println(field.getName() + ": " + val.getClass().getName() + "(" + field.getType().getName() + ")");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		//data.Client.get.printReport();
		
		Thread serverThread = Server.getNewThread();
		serverThread.start();
		
		Thread titleUpdaterThread = new Thread() {
			public void run() {
				while (true) {
					String title = "RuneScape";
					
					if (ServerInfo.cur != null) {
						title = "RuneScape - Server " + ServerInfo.cur.getServerId();
						
						if (AccountInfo.cur != null) {
							title = "RuneScape - Server " + ServerInfo.cur.getServerId() + " - Account " + AccountInfo.cur.getId();
							
							if (ScriptHandler.currentScript != null && !ScriptHandler.currentScript.isStopped()) {
								title = "RuneScape - Server " + ServerInfo.cur.getServerId() + " - Account " + AccountInfo.cur.getId() + " - Script Elapsed: " + Time.format(System.currentTimeMillis() - ScriptHandler.scriptStartTime);
							}
						}
					}
					
					frame.setTitle(title);
					
					Time.sleep(1000);
				}
			}
		};
		titleUpdaterThread.start();
		
		Thread waitForRSToLoad = new Thread() {
			@Override
			public void run() {
				//Time.sleep(10000); // TODO: get client state hook? check when rs is loaded
				
				Time.sleep(4000);
				//ScriptHandler.setupInput(rs2app, "s");
				
				boolean loaded = false;
				
				//Timer timer = new Timer(120000); // 2 min.
				Timer timer = new Timer(300000); // 5 min.
				while (timer.isRunning()) {
					if (Client.isRSDoneLoading()) {
						//NetworkDrives.map();
						loaded = true;
						break;
					}
					
					Time.sleep(50);
				}
				
				if (loaded) {
					runescapeLoaded = true;
					
					System.out.println("Runescape is done loading.");
					
					//setupInput("s");
					ScriptHandler.setupInput(rs2app, "");
					
					runScript();
				} else {
					System.out.println("ERROR: Runescape didn't load!");
				}
			}
		};
		waitForRSToLoad.start();
		
		
		/*Thread initialEventQueueThread = new Thread() {
			@Override
			public void run() {
				//setupInput("s");
				setupInput("");
			}
		};
		initialEventQueueThread.start();
		
		Thread eventQueueThread2 = new Thread() {
			@Override
			public void run() {
				//Time.sleep(10000); // TODO: get client state hook? check when rs is loaded
				
				Time.sleep(4000);
				
				boolean loaded = false;
				
				Timer timer = new Timer(120000); // 2 min.
				while (timer.isRunning()) {
					if (Client.isRSDoneLoading()) {
						loaded = true;
						break;
					}
					
					Time.sleep(50);
				}
				
				if (loaded) {
					runescapeLoaded = true;
					
					System.out.println("Runescape is done loading.");
					
					//setupInput("s");
					setupInput("");
				} else {
					System.out.println("ERROR: Runescape didn't load!");
				}
			}
		};
		eventQueueThread2.start();*/
		
		
		//runScript();
	}
	
	 /**
	* Starts sending the image and accepting commands from Delphi.
	*
	* @result True if success
	*/
	public static boolean startBlocking() {
		if (blockingInput) {
			if (!Input.useHardWareInput()) {
				try {
					java.awt.Component c = (java.awt.Component) Client.getCanvas().getObject();
					
					if (c != null) {
						BlockingEventQueue.setBlocking(c, true);
						BlockingEventQueue.ensureBlocking();
						ScriptHandler.nazi = EventNazi.getNazi(c);
					}
					
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	 /**
	* Stops sending the image and accepting commands from Delphi.
	* @result True if success
	*/
	public static boolean stopBlocking() {
		if (!blockingInput) {
			if (!Input.useHardWareInput()) {
				try {
					//blocking = false;
					
					java.awt.Component c = (java.awt.Component) Client.getCanvas().getObject();
					
					if (c != null) {
						BlockingEventQueue.setBlocking(c, false);
					}
					
					ScriptHandler.nazi = null;
					
					return true;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	private static void loadModscript() {
		/*File modScriptDirectory = new File("Z:\\modscript\\" + data.Client.get.getBuildNumberString() + "\\");
		if (!modScriptDirectory.exists())
			modScriptDirectory = new File("Y:\\" + data.Client.get.getBuildNumberString() + "\\");
		if (!modScriptDirectory.exists())
			modScriptDirectory = new File("C:\\modscript\\" + data.Client.get.getBuildNumberString() + "\\");*/
		
		File modScriptDirectory = new File("C:\\modscript\\" + data.Client.get.getBuildNumberString() + "\\");
		File xmlFile = new File(modScriptDirectory, sha1 + ".xml");
		
		if (ServerInfo.cur == null) {
			System.out.println("ERROR: load modscript: ServerInfo is null, unable to check if server is -1");
			return;
		} else if (ServerInfo.cur.getServerId() == -1) {
			System.out.println("load modscript: server is -1, skipping ftp download...");
		} else {
			Ftp client = new Ftp();
			client.login();
			
			try {
				client.downloadSingleFile("/modscript/" + data.Client.get.getBuildNumberString() + "/" + sha1 + ".xml", xmlFile.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			client.logout();
		}
		
		if (xmlFile.exists())
			XMLLoader.loadHookDataFromXML(xmlFile);
		else
			System.out.println("Error: unable to find modscript xml file!");
	}
	
	public static void loadAccountInfo() {
		//File currentAccountDirectory = new File("C:\\accounts\\");
		//File iniFile = new File(currentAccountDirectory, "current.ini");
		
		/*File accountInfoDirectory = new File("Z:\\accounts\\");
		File xmlFile = new File(accountInfoDirectory, "info.xml");
		if (!xmlFile.exists()) {
			accountInfoDirectory = new File("X:\\");
			xmlFile = new File(accountInfoDirectory, "info.xml");
			if (!xmlFile.exists()) {
				accountInfoDirectory = new File("C:\\accounts");
				xmlFile = new File(accountInfoDirectory, "info.xml");
			}
		}*/
		
		File accountInfoDirectory = new File("C:\\accounts");
		File xmlFile = new File(accountInfoDirectory, "info.xml");
		
		if (ServerInfo.cur == null) {
			System.out.println("ERROR: load account info: ServerInfo is null, unable to check if server is -1");
			return;
		} else if (ServerInfo.cur.getServerId() == -1) {
			System.out.println("load account info: server is -1, skipping ftp download...");
		} else {
			Ftp client = new Ftp();
			client.login();
			
			try {
				client.downloadSingleFile("/accounts/info.xml", xmlFile.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			client.logout();
		}
		
		if (/*iniFile.exists() &&*/ xmlFile.exists())
			AccountInfo.loadCurrentAccountDataFromXML(xmlFile);
		else
			System.out.println("Error: unable to find account info xml file!");
	}
	
	public static boolean stopScript() {
		if (ScriptHandler.currentScript != null) {
			ScriptHandler.currentScript.interruptStop();
			return true;
		}
		
		return false;
	}
	
	public static boolean startScript() {
		if (ScriptHandler.currentScript != null) {
			if (ScriptHandler.currentScript.isStopped()) {
				ScriptHandler.currentScript.stopInterrupt = false;
				return runScript();
			}
		}
		
		return false;
	}
	
	// digifrit@outlook.com
	
	// BANNED
	// goldalrik@outlook.com
	// drakanslay@outlook.com
	
	// Icecreamtruck2
	
	
	// ! safe mode until viewport is fixed for opengl (on level up) - though, it might also be wrong for safe mode
	
	public static boolean runScript() {
		if (!runescapeLoaded) {
			if (Client.isRSDoneLoading()) {
				runescapeLoaded = true;
				ScriptHandler.setupInput(rs2app, "");
			} else {
				System.out.println("ERROR: Runescape didn't load!");
				return false;
			}
		}
		
		ScriptHandler.runScriptThreaded(false, true);
		return true;
	}
}