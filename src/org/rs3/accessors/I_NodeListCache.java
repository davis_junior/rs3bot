package org.rs3.accessors;

public class I_NodeListCache extends I_Node {
	
	/**
	 * 
	 * @return Type: NodeDeque
	 */
	public static Object getNodeList(Object obj) {
		return Reflection.getDeclaredField(data.NodeListCache.get.getInternalName(), obj, data.NodeListCache.get.nodeList.getInternalName());
	}
}