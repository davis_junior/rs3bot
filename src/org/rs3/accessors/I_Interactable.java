package org.rs3.accessors;

public class I_Interactable extends I_EntityNode {
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getPlane(Object obj) {
		return (byte) Reflection.getDeclaredField(data.Interactable.get.getInternalName(), obj, data.Interactable.get.plane.getInternalName(), byte.class);
	}
}
