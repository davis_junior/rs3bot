package org.rs3.accessors;

import java.lang.reflect.Field;

public class Unsafe {
	
	public static sun.misc.Unsafe getUnsafe() {
		try {
			Field f = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
			f.setAccessible(true);
			sun.misc.Unsafe unsafe = (sun.misc.Unsafe) f.get(null);
			return unsafe;
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/*public static long sizeOf(Object o) {
		sun.misc.Unsafe u = getUnsafe();
	    HashSet<Field> fields = new HashSet<Field>();
	    Class<?> c = o.getClass();
	    while (c != Object.class) {
	        for (Field f : c.getDeclaredFields()) {
	            if ((f.getModifiers() & Modifier.STATIC) == 0) {
	                fields.add(f);
	            }
	        }
	        c = c.getSuperclass();
	    }

	    // get offset
	    long maxSize = 0;
	    for (Field f : fields) {
	        long offset = u.objectFieldOffset(f);
	        if (offset > maxSize) {
	            maxSize = offset;
	        }
	    }

	    return ((maxSize/8) + 1) * 8;   // padding
	}*/
	
	public static long normalize(int value) {
	    if(value >= 0) return value;
	    return (~0L >>> 32) & value;
	}
	
	public static long sizeOf(Object object){
	    return getUnsafe().getAddress(
	        normalize(getUnsafe().getInt(object, 4L)) + 12L);
	}
	
	public static long toAddress(Object obj) {
	    Object[] array = new Object[] {obj};
	    long baseOffset = getUnsafe().arrayBaseOffset(Object[].class);
	    return normalize(getUnsafe().getInt(array, baseOffset));
	}
	
	public static Object fromAddress(long address) {
	    Object[] array = new Object[] {null};
	    long baseOffset = getUnsafe().arrayBaseOffset(Object[].class);
	    getUnsafe().putLong(array, baseOffset, address);
	    return array[0];
	}
	
	public static Object shallowCopy(Object obj) {
	    long size = sizeOf(obj);
	    long start = toAddress(obj);
	    long address = getUnsafe().allocateMemory(size);
	    getUnsafe().copyMemory(start, address, size);
	    return fromAddress(address);
	}
}
