package org.rs3.accessors;

public class I_CentralLocationData extends I_AbstractCentralLocationData {
	
	/**
	 * 
	 * @return Type: Location
	 */
	public static Object getPoint1(Object obj) {
		return Reflection.getDeclaredField(data.CentralLocationData.get.getInternalName(), obj, data.CentralLocationData.get.point1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Location
	 */
	public static Object getPoint2(Object obj) {
		return Reflection.getDeclaredField(data.CentralLocationData.get.getInternalName(), obj, data.CentralLocationData.get.point2.getInternalName());
	}
}
