package org.rs3.accessors;

public class I_Component {
	
	/**
	 * 
	 * @return Type: Component
	 */
	public static Object[] getComponents(Object obj) {
		return (Object[]) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.components.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getBoundsArrayIndex(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.boundsArrayIndex.getInternalName(), int.class) * data.Component.get.boundsArrayIndex.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getParentId(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.parentId.getInternalName(), int.class) * data.Component.get.parentId.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.id.getInternalName(), int.class) * data.Component.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getX(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.x.getInternalName(), int.class) * data.Component.get.x.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getY(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.y.getInternalName(), int.class) * data.Component.get.y.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getWidth(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.width.getInternalName(), int.class) * data.Component.get.width.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getHeight(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.height.getInternalName(), int.class) * data.Component.get.height.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getHorizontalScrollbarThumbSize(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.horizontalScrollbarThumbSize.getInternalName(), int.class) * data.Component.get.horizontalScrollbarThumbSize.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getVerticalScrollbarThumbSize(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.verticalScrollbarThumbSize.getInternalName(), int.class) * data.Component.get.verticalScrollbarThumbSize.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getType(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.type.getInternalName(), int.class) * data.Component.get.type.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getComponentId(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.componentId.getInternalName(), int.class) * data.Component.get.componentId.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isHidden(Object obj) {
		return (boolean) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.hidden.getInternalName(), boolean.class);
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isVisible(Object obj) {
		return (boolean) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.visible.getInternalName(), boolean.class);
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getVerticalScrollbarSize(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.verticalScrollbarSize.getInternalName(), int.class) * data.Component.get.verticalScrollbarSize.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getHorizontalScrollbarSize(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.horizontalScrollbarSize.getInternalName(), int.class) * data.Component.get.horizontalScrollbarSize.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getVerticalScrollbarPosition(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.verticalScrollbarPosition.getInternalName(), int.class) * data.Component.get.verticalScrollbarPosition.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getHorizontalScrollbarPosition(Object obj) {
		return (int) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.horizontalScrollbarPosition.getInternalName(), int.class) * data.Component.get.horizontalScrollbarPosition.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: java.lang.String[]
	 */
	public static String[] getActions(Object obj) {
		return (String[]) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.actions.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getTooltip(Object obj) {
		return (String) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.tooltip.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getComponentName(Object obj) {
		return (String) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.componentName.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getText(Object obj) {
		return (String) Reflection.getDeclaredField(data.Component.get.getInternalName(), obj, data.Component.get.text.getInternalName());
	}
}
