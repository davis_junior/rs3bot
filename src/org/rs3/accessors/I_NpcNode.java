package org.rs3.accessors;

public class I_NpcNode extends I_Node {
	
	/*/*
	 * 
	 * Note: See the note below: the other method will be more efficient although less like the RuneScape client retrieval system.
	 * @return Type: java.lang.Object
	 */
	/*public static Object getNpc(Object obj) {
		return Reflection.getField(data.NpcNode.get.getInternalName(), obj, data.NpcNode.get.npc.getInternalName());
	}*/
	
	
	/**
	 * 
	 * Note: In the RuneScape client, an Object is returned (java.lang.Object); this is more efficient.
	 * @return Type: Npc
	 */
	public static Object getNpc(Object obj) {
		return Reflection.getDeclaredField(data.NpcNode.get.getInternalName(), obj, data.NpcNode.get.npc.getInternalName());
	}
}