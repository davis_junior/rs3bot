package org.rs3.accessors;

public class I_SoftReference extends I_Reference {
	
	/**
	 * 
	 * @return Type: java.lang.ref.SoftReference<?>
	 */
	public static java.lang.ref.SoftReference<?> getSoftReference(Object obj) {
		return (java.lang.ref.SoftReference<?>) Reflection.getDeclaredField(data.SoftReference.get.getInternalName(), obj, data.SoftReference.get.softReference.getInternalName());
	}
}