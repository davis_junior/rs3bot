package org.rs3.accessors;

public class I_GEOffer {
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getStatus(Object obj) {
		return (byte) Reflection.getDeclaredField(data.GEOffer.get.getInternalName(), obj, data.GEOffer.get.status.getInternalName(), byte.class);
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.GEOffer.get.getInternalName(), obj, data.GEOffer.get.id.getInternalName(), int.class) * data.GEOffer.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getPrice(Object obj) {
		return (int) Reflection.getDeclaredField(data.GEOffer.get.getInternalName(), obj, data.GEOffer.get.price.getInternalName(), int.class) * data.GEOffer.get.price.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getTotal(Object obj) {
		return (int) Reflection.getDeclaredField(data.GEOffer.get.getInternalName(), obj, data.GEOffer.get.total.getInternalName(), int.class) * data.GEOffer.get.total.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getTransfered(Object obj) {
		return (int) Reflection.getDeclaredField(data.GEOffer.get.getInternalName(), obj, data.GEOffer.get.transfered.getInternalName(), int.class) * data.GEOffer.get.transfered.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getSpend(Object obj) {
		return (int) Reflection.getDeclaredField(data.GEOffer.get.getInternalName(), obj, data.GEOffer.get.spend.getInternalName(), int.class) * data.GEOffer.get.spend.getMultiplier().intValue();
	}
}
