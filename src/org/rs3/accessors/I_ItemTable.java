package org.rs3.accessors;

public class I_ItemTable extends I_Node {
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getItemIds(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.ItemTable.get.getInternalName(), obj, data.ItemTable.get.itemIds.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getItemStackSizes(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.ItemTable.get.getInternalName(), obj, data.ItemTable.get.itemStackSizes.getInternalName());
	}
}