package org.rs3.accessors;

public class I_ObjectDefinition { // actually implements I_AbstractDefinition
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getName(Object obj) {
		return (String) Reflection.getDeclaredField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.name.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String[]
	 */
	public static String[] getActions(Object obj) {
		return (String[]) Reflection.getDeclaredField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.actions.getInternalName());
	}
	
	/*/*
	 * 
	 * @return Type: java.lang.String[]
	 */
	/*public static String[] getActions(Object obj) {
		return (String[]) Reflection.getDeclaredField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.actions.getInternalName());
	}*/
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.id.getInternalName(), int.class) * data.ObjectDefinition.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: AbstractDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectCacheLoader
	 */
	public static Object getCacheLoader(Object obj) {
		return Reflection.getField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.cacheLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: byte[]
	 */
	public static byte[] getModelCache2byteArray(Object obj) {
		return (byte[]) Reflection.getDeclaredField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.modelCache2byteArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[][]
	 */
	public static int[][] getModelCache2intArray2D(Object obj) {
		return (int[][]) Reflection.getDeclaredField(data.ObjectDefinition.get.getInternalName(), obj, data.ObjectDefinition.get.modelCache2intArray2D.getInternalName());
	}
}
