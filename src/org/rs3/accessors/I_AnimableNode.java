package org.rs3.accessors;

public class I_AnimableNode {
	
	/**
	 * 
	 * @return Type: AnimableNode
	 */
	public static Object getNext(Object obj) {
		return Reflection.getDeclaredField(data.AnimableNode.get.getInternalName(), obj, data.AnimableNode.get.next.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: RSAnimable
	 */
	public static Object getAnimable(Object obj) {
		return Reflection.getDeclaredField(data.AnimableNode.get.getInternalName(), obj, data.AnimableNode.get.animable.getInternalName());
	}
}
