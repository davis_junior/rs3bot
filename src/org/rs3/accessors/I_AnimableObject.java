package org.rs3.accessors;

public class I_AnimableObject extends I_RSAnimable {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.AnimableObject.get.getInternalName(), obj, data.AnimableObject.get.id.getInternalName(), int.class) * data.AnimableObject.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: Model
	 */
	public static Object getModel(Object obj) {
		return Reflection.getDeclaredField(data.AnimableObject.get.getInternalName(), obj, data.AnimableObject.get.model.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getDeclaredField(data.AnimableObject.get.getInternalName(), obj, data.AnimableObject.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getAnimationId(Object obj) {
		return (byte) Reflection.getDeclaredField(data.AnimableObject.get.getInternalName(), obj, data.AnimableObject.get.animationId.getInternalName(), byte.class);
	}

	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getFace(Object obj) {
		return (byte) Reflection.getDeclaredField(data.AnimableObject.get.getInternalName(), obj, data.AnimableObject.get.face.getInternalName(), byte.class);
	}
}