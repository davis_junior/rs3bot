package org.rs3.accessors;

public class I_WallObject extends I_AbstractWallObject {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.WallObject.get.getInternalName(), obj, data.WallObject.get.id.getInternalName(), int.class) * data.WallObject.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: Model
	 */
	public static Object getModel(Object obj) {
		return Reflection.getDeclaredField(data.WallObject.get.getInternalName(), obj, data.WallObject.get.model.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getDeclaredField(data.WallObject.get.getInternalName(), obj, data.WallObject.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getAnimationId(Object obj) {
		return (byte) Reflection.getDeclaredField(data.WallObject.get.getInternalName(), obj, data.WallObject.get.animationId.getInternalName(), byte.class);
	}

	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getFace(Object obj) {
		return (byte) Reflection.getDeclaredField(data.WallObject.get.getInternalName(), obj, data.WallObject.get.face.getInternalName(), byte.class);
	}
}