package org.rs3.accessors;

public class I_Ground {
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getPlane(Object obj) {
		return (byte) Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.plane.getInternalName(), byte.class);
	}
	
	/**
	 * 
	 * @return Type: AnimableNode
	 */
	public static Object getAnimableList(Object obj) {
		return Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.animableList.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: AbstractBoundary
	 */
	public static Object getBoundary1(Object obj) {
		return Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.boundary1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: AbstractBoundary
	 */
	public static Object getBoundary2(Object obj) {
		return Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.boundary2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: AbstractWallObject
	 */
	public static Object getWallDecoration1(Object obj) {
		return Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.wallDecoration1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: AbstractWallObject
	 */
	public static Object getWallDecoration2(Object obj) {
		return Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.wallDecoration2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: AbstractFloorObject
	 */
	public static Object getFloorDecoration(Object obj) {
		return Reflection.getDeclaredField(data.Ground.get.getInternalName(), obj, data.Ground.get.floorDecoration.getInternalName());
	}
}
