package org.rs3.accessors;

public class I_GraphicsSettingsContainer extends I_Node {
	
	/**
	 * 
	 * @return Type: GraphicsLevel
	 */
	public static Object getLevel(Object obj) {
		return Reflection.getDeclaredField(data.GraphicsSettingsContainer.get.getInternalName(), obj, data.GraphicsSettingsContainer.get.level.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: GraphicsCpuUsage
	 */
	public static Object getCpuUsage(Object obj) {
		return Reflection.getDeclaredField(data.GraphicsSettingsContainer.get.getInternalName(), obj, data.GraphicsSettingsContainer.get.cpuUsage.getInternalName());
	}
}