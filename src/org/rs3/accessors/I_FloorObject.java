package org.rs3.accessors;

public class I_FloorObject extends I_AbstractFloorObject {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.FloorObject.get.getInternalName(), obj, data.FloorObject.get.id.getInternalName(), int.class) * data.FloorObject.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: Model
	 */
	public static Object getModel(Object obj) {
		return Reflection.getDeclaredField(data.FloorObject.get.getInternalName(), obj, data.FloorObject.get.model.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getDeclaredField(data.FloorObject.get.getInternalName(), obj, data.FloorObject.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getAnimationId(Object obj) {
		return (byte) Reflection.getDeclaredField(data.FloorObject.get.getInternalName(), obj, data.FloorObject.get.animationId.getInternalName(), byte.class);
	}

	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getFace(Object obj) {
		return (byte) Reflection.getDeclaredField(data.FloorObject.get.getInternalName(), obj, data.FloorObject.get.face.getInternalName(), byte.class);
	}
}