package org.rs3.accessors;

public class I_AnimatedAnimableObject extends I_RSAnimable {
	
	/**
	 * 
	 * @return Type: AnimatedObject
	 */
	public static Object getAnimatedObject(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedAnimableObject.get.getInternalName(), obj, data.AnimatedAnimableObject.get.animatedObject.getInternalName());
	}
}