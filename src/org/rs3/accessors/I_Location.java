package org.rs3.accessors;

public class I_Location {
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getX(Object obj) {
		return (float) Reflection.getDeclaredField(data.Location.get.getInternalName(), obj, data.Location.get.x.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getY(Object obj) {
		return (float) Reflection.getDeclaredField(data.Location.get.getInternalName(), obj, data.Location.get.y.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getHeight(Object obj) {
		return (float) Reflection.getDeclaredField(data.Location.get.getInternalName(), obj, data.Location.get.height.getInternalName());
	}
}
