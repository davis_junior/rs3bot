package org.rs3.accessors;

public class I_IdComposite_AnimatedObject {
	
	/**
	 * 
	 * @return Type: long
	 */
	public static long getId(Object obj) {
		return (long) Reflection.getDeclaredField(data.IdComposite_AnimatedObject.get.getInternalName(), obj, data.IdComposite_AnimatedObject.get.id.getInternalName(), long.class) * data.IdComposite_AnimatedObject.get.id.getMultiplier().longValue();
	}
}
