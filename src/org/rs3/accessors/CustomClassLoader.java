package org.rs3.accessors;
import java.applet.Applet;
import java.lang.instrument.Instrumentation;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

import org.rs3.api.wrappers.Client;

import data.FieldData;

public class CustomClassLoader extends ClassLoader {
	
	public static final CustomClassLoader get = new CustomClassLoader();
	
	public static Instrumentation instrument = null;
	
	//public static Object classMapLock = new Object();
	private static Map<String, Class<?>> classes = new HashMap<>();
	//public static Set<String> classesToAdd = new HashSet<>();
	
	public URLClassLoader urlLoader = null;
	public ClassLoader appletLoader = null;
	
	public Applet rs2app = null;
	public FieldData field_classLoaderHolder = null;
	public FieldData field_classLoader = null;
	
	private CustomClassLoader() {
		//super(); // sets parent to system loader
		//get = this;
	}
	
	/*private CustomClassLoader(URLClassLoader urlLoader) {
		//super(); // sets parent to system loader
		this.urlLoader = urlLoader;
	}*/
	

	
	public void setUrlLoader(URLClassLoader urlLoader) {
		this.urlLoader = urlLoader;
	}
	
	public void setAppletLoader(ClassLoader appletLoader) {
		this.appletLoader = appletLoader;
	}
	
	public void setApplet(Applet rs2app) {
		this.rs2app = rs2app;
	}
	
	public void setFields(FieldData classLoaderHolder, FieldData classLoader) {
		this.field_classLoaderHolder = classLoaderHolder;
		this.field_classLoader = classLoader;
	}
	
	/*public static void cacheClassesToAdd() {
		if (instrument == null)
			return;
		
		if (classesToAdd.size() > 0) {
			Class<?>[] loaded = instrument.getAllLoadedClasses();
			
			Set<String> classesToRemove = new HashSet<>();
			
			for (String className : classesToAdd) {
				for (Class<?> clazz : loaded) {
					if (className.equals(clazz.getName())) {
						classes.put(className, clazz);
						classesToRemove.add(className);
						break;
					}
				}
			}
			
			if (classesToRemove.size() > 0) {
				for (String className : classesToRemove) {
					classesToAdd.remove(className);
				}
			}
		}
	}*/
	
	public static Class<?> getInstrumentClass(String className) {
		if (instrument == null)
			return null;
		
		Class<?>[] loaded;
		
		Object clientObj = Client.getObject();
		boolean allLoadedClasses;
		
		if (clientObj != null) {
			loaded = instrument.getInitiatedClasses(Client.getObject().getClass().getClassLoader());
			allLoadedClasses = false;
		} else {
			loaded = instrument.getAllLoadedClasses();
			allLoadedClasses = true;
		}
		
		if (loaded != null) {
			for (Class<?> clazz : loaded) {
				if (clazz == null)
					continue;
				
				if (className.equals(clazz.getName())) {
					// wrong class has classloader (from Frame): java.net.URLClassLoader
					// wrong class has classloader like (from JagexAppletViewer): app.a
					// correct class has classloader like "g"
					
					if (allLoadedClasses) {
						if (clazz.getClassLoader() == null)
							continue;
						
						if (clazz.getClassLoader().getClass().getName().startsWith("java."))
							continue;
						
						if (clazz.getClassLoader().getClass().getName().startsWith("app."))
							continue;
					}
					
					return clazz;
				}
			}
		}
		
		// class not found, try all loaded classes instead if we haven't yet (mainly for loading our own classes -- e.g. org.rs3.debug.Paint)
		if (!allLoadedClasses) {
			loaded = instrument.getAllLoadedClasses();
			allLoadedClasses = true;
			
			if (loaded != null) {
				for (Class<?> clazz : loaded) {
					if (className.equals(clazz.getName())) {
						return clazz;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		return loadClass(name);
	}
	
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		//cacheClassesToAdd();
		
		Class<?> alreadyLoaded = classes.get(name);
		if (alreadyLoaded != null) 
			return alreadyLoaded;
		
		alreadyLoaded = getInstrumentClass(name);
		if (alreadyLoaded != null)  {
			classes.put(name, alreadyLoaded);
			return alreadyLoaded;
		}
		
		return null;
		//return ClassLoader.getSystemClassLoader().loadClass(name);
		//return super.loadClass(name, false);
	}
	
	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		//cacheClassesToAdd();
		
		Class<?> alreadyLoaded = classes.get(name);
		if (alreadyLoaded != null)  {
			return alreadyLoaded;
		}
		
		alreadyLoaded = getInstrumentClass(name);
		if (alreadyLoaded != null)  {
			classes.put(name, alreadyLoaded);
			return alreadyLoaded;
		}
		
		if (true)
			return null;
			//return ClassLoader.getSystemClassLoader().loadClass(name);
		
		if (appletLoader != null) {
			try {
				Class<?> clazz = appletLoader.loadClass(name);
				classes.put(name, clazz);
				return clazz;
			} catch (ClassNotFoundException e) {
				// ignored 
			}
		}
		//else
		//	System.out.println("CustomClassLoader: Applet ClassLoader is null!");
		
		if (urlLoader != null) {
			try {
				Class<?> clazz = urlLoader.loadClass(name);
				return clazz;
			} catch (ClassNotFoundException e) {
				// ignored 
			}
		}
		//else
		//	System.out.println("CustomClassLoader: URLClassLoader is null!");
		
		if (rs2app != null 
				&& field_classLoaderHolder != null && field_classLoaderHolder.getInternalName() != null
				&& field_classLoader != null && field_classLoader.getInternalName() != null) {
			
			Object holder = Reflection.getField(field_classLoaderHolder.getInternalClassName(), rs2app, field_classLoaderHolder.getInternalName());
			
			ClassLoader rs2Loader = (ClassLoader) Reflection.getField(field_classLoader.getInternalClassName(), holder, field_classLoader.getInternalName());
			
			if (rs2Loader != null) {
				System.out.println("Updating rs2applet loader: " + rs2Loader.getClass().getName());
				
				try {
					appletLoader = rs2Loader;
					Class<?> clazz = appletLoader.loadClass(name);
					return clazz;
				} catch (ClassNotFoundException e) {
					// ignored 
				}
			} else {
				System.out.println("null!!!!");
			}
		}
		
		return super.loadClass(name);
	}
}
