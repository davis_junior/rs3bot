package org.rs3.accessors;

public class I_ItemDefinition { // actually implements I_AbstractDefinition
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getName(Object obj) {
		return (String) Reflection.getDeclaredField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.name.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String[]
	 */
	public static String[] getActions(Object obj) {
		return (String[]) Reflection.getDeclaredField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.actions.getInternalName());
	}
	
	/*/*
	 * 
	 * @return Type: java.lang.String[]
	 */
	/*public static String[] getActions(Object obj) {
		return (String[]) Reflection.getDeclaredField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.actions.getInternalName());
	}*/
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.id.getInternalName(), int.class) * data.ItemDefinition.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: AbstractDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ItemCacheLoader
	 */
	public static Object getCacheLoader(Object obj) {
		return Reflection.getField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.cacheLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: byte[]
	 */
	/*public static byte[] getModelCache2byteArray(Object obj) {
		return (byte[]) Reflection.getDeclaredField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.modelCache2byteArray.getInternalName());
	}*/
	
	/**
	 * 
	 * @return Type: int[][]
	 */
	/*public static int[][] getModelCache2intArray2D(Object obj) {
		return (int[][]) Reflection.getDeclaredField(data.ItemDefinition.get.getInternalName(), obj, data.ItemDefinition.get.modelCache2intArray2D.getInternalName());
	}*/
}
