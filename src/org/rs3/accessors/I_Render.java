package org.rs3.accessors;

public class I_Render {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getIndex(Object obj) {
		return (int) Reflection.getDeclaredField(data.Render.get.getInternalName(), obj, data.Render.get.index.getInternalName(), int.class) * data.Render.get.index.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getCacheIndex(Object obj) {
		return (int) Reflection.getDeclaredField(data.Render.get.getInternalName(), obj, data.Render.get.cacheIndex.getInternalName(), int.class) * data.Render.get.cacheIndex.getMultiplier().intValue();
	}
}
