package org.rs3.accessors;

public class I_Player extends I_Character {
	
	/**
	 * 
	 * @return Type: PlayerDef
	 */
	public static Object getDefinition(Object obj) {
		return Reflection.getDeclaredField(data.Player.get.getInternalName(), obj, data.Player.get.definition.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getName(Object obj) {
		return (String) Reflection.getDeclaredField(data.Player.get.getInternalName(), obj, data.Player.get.name.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getTitle(Object obj) {
		return (String) Reflection.getDeclaredField(data.Player.get.getInternalName(), obj, data.Player.get.title.getInternalName());
	}
}