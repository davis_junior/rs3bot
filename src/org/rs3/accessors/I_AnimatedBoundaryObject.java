package org.rs3.accessors;

public class I_AnimatedBoundaryObject extends I_AbstractBoundary {
	
	/**
	 * 
	 * @return Type: AnimatedObject
	 */
	public static Object getAnimatedObject(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedBoundaryObject.get.getInternalName(), obj, data.AnimatedBoundaryObject.get.animatedObject.getInternalName());
	}
}