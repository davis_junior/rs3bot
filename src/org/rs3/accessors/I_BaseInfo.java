package org.rs3.accessors;

public class I_BaseInfo {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getX(Object obj) {
		return (int) Reflection.getDeclaredField(data.BaseInfo.get.getInternalName(), obj, data.BaseInfo.get.x.getInternalName(), int.class) * data.BaseInfo.get.x.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getY(Object obj) {
		return (int) Reflection.getDeclaredField(data.BaseInfo.get.getInternalName(), obj, data.BaseInfo.get.y.getInternalName(), int.class) * data.BaseInfo.get.y.getMultiplier().intValue();
	}
}
