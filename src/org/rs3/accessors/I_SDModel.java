package org.rs3.accessors;

import data.SDModel;

public class I_SDModel extends I_Model {
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getIndices1(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.indices1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getIndices2(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.indices2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getIndices3(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.indices3.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getXPoints(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.xPoints.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getYPoints(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.yPoints.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getZPoints(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.zPoints.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getNumFaces(Object obj) {
		return (int) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.numFaces.getInternalName(), int.class);
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getNumVertices(Object obj) {
		return (int) Reflection.getDeclaredField(data.SDModel.get.getInternalName(), obj, SDModel.get.numVertices.getInternalName(), int.class);
	}
}
