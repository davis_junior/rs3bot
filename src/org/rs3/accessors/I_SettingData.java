package org.rs3.accessors;

public class I_SettingData {
	
	/**
	 * 
	 * @return Type: Settings
	 */
	public static Object getSettings(Object obj) {
		return Reflection.getDeclaredField(data.SettingData.get.getInternalName(), obj, data.SettingData.get.settings.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: SkillData[]
	 */
	public static Object[] getSkillArray(Object obj) {
		return (Object[]) Reflection.getDeclaredField(data.SettingData.get.getInternalName(), obj, data.SettingData.get.skillArray.getInternalName());
	}
}
