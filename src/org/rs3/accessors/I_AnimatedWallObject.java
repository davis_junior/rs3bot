package org.rs3.accessors;

public class I_AnimatedWallObject extends I_AbstractWallObject {
	
	/**
	 * 
	 * @return Type: AnimatedObject
	 */
	public static Object getAnimatedObject(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedWallObject.get.getInternalName(), obj, data.AnimatedWallObject.get.animatedObject.getInternalName());
	}
}