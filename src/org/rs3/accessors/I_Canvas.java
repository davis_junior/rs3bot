package org.rs3.accessors;

import java.awt.Component;

import data.Canvas;

public class I_Canvas {
	
	/**
	 * 
	 * @return Type: java.awt.Component
	 */
	public static Component getComponent(Object obj) {
		return (Component) Reflection.getDeclaredField(Canvas.get.getInternalName(), obj, Canvas.get.component.getInternalName());
	}
}
