package org.rs3.accessors;

public class I_NodeSub extends I_Node {
	
	/**
	 * 
	 * @return Type: NodeSub
	 */
	public static Object getPreviousSub(Object obj) {
		return Reflection.getDeclaredField(data.NodeSub.get.getInternalName(), obj, data.NodeSub.get.prevSub.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: NodeSub
	 */
	public static Object getNextSub(Object obj) {
		return Reflection.getDeclaredField(data.NodeSub.get.getInternalName(), obj, data.NodeSub.get.nextSub.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: long
	 */
	public static long getSubId(Object obj) {
		return (long) Reflection.getDeclaredField(data.NodeSub.get.getInternalName(), obj, data.NodeSub.get.subId.getInternalName(), long.class) * data.NodeSub.get.subId.getMultiplier().longValue();
	}
}