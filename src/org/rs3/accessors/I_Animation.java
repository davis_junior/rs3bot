package org.rs3.accessors;

public class I_Animation {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.Animation.get.getInternalName(), obj, data.Animation.get.id.getInternalName(), int.class) * data.Animation.get.id.getMultiplier().intValue();
	}
}
