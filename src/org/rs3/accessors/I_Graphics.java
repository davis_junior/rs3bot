package org.rs3.accessors;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;

public class I_Graphics {
	
	/**
	 * 
	 * @return Type: java.awt.DisplayMode
	 */
	public static DisplayMode getDisplayMode(Object obj) {
		return (DisplayMode) Reflection.getDeclaredField(data.Graphics.get.getInternalName(), obj, data.Graphics.get.displayMode.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.awt.GraphicsDevice
	 */
	public static GraphicsDevice getGraphicsDevice(Object obj) {
		return (GraphicsDevice) Reflection.getDeclaredField(data.Graphics.get.getInternalName(), obj, data.Graphics.get.graphicsDevice.getInternalName());
	}
}
