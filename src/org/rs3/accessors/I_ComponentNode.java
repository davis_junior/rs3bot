package org.rs3.accessors;

public class I_ComponentNode extends I_Node {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMainId(Object obj) {
		return (int) Reflection.getDeclaredField(data.ComponentNode.get.getInternalName(), obj, data.ComponentNode.get.mainId.getInternalName(), int.class) * data.ComponentNode.get.mainId.getMultiplier().intValue();
	}
}