package org.rs3.accessors;

import data.OpenGLModel;

public class I_OpenGLModel extends I_Model {
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getIndices1(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.indices1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getIndices2(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.indices2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getIndices3(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.indices3.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getXPoints(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.xPoints.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getYPoints(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.yPoints.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getZPoints(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.zPoints.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getNumFaces(Object obj) {
		return (int) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.numFaces.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getNumVertices(Object obj) {
		return (int) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.numVertices.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getProjectionLength(Object obj) {
		return (int) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.projectionLength.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getProjectionChecks(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.projectionChecks.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: short[]
	 */
	public static short[] getProjectionIndices(Object obj) {
		return (short[]) Reflection.getDeclaredField(data.OpenGLModel.get.getInternalName(), obj, OpenGLModel.get.projectionIndices.getInternalName());
	}
}
