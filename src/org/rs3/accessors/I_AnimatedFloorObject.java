package org.rs3.accessors;

public class I_AnimatedFloorObject extends I_AbstractFloorObject {
	
	/**
	 * 
	 * @return Type: AnimatedObject
	 */
	public static Object getAnimatedObject(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedFloorObject.get.getInternalName(), obj, data.AnimatedFloorObject.get.animatedObject.getInternalName());
	}
}