package org.rs3.accessors;

public class I_ObjectCacheLoader {
	
	/**
	 * 
	 * @return Type: Cache
	 */
	public static Object getAnimatedModelCache(Object obj) {
		return Reflection.getDeclaredField(data.ObjectCacheLoader.get.getInternalName(), obj, data.ObjectCacheLoader.get.animatedModelCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Cache
	 */
	public static Object getModelCache(Object obj) {
		return Reflection.getDeclaredField(data.ObjectCacheLoader.get.getInternalName(), obj, data.ObjectCacheLoader.get.modelCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Cache
	 */
	public static Object getCompositeCache(Object obj) {
		return Reflection.getDeclaredField(data.ObjectCacheLoader.get.getInternalName(), obj, data.ObjectCacheLoader.get.compositeCache.getInternalName());
	}
}
