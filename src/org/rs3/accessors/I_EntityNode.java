package org.rs3.accessors;

public class I_EntityNode {
	
	/**
	 * 
	 * @return Type: EntityNode
	 */
	public static Object getPrevious(Object obj) {
		return Reflection.getDeclaredField(data.EntityNode.get.getInternalName(), obj, data.EntityNode.get.previous.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: EntityNode
	 */
	public static Object getNext(Object obj) {
		return Reflection.getDeclaredField(data.EntityNode.get.getInternalName(), obj, data.EntityNode.get.next.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: InteractableData
	 */
	public static Object getData(Object obj) {
		return Reflection.getDeclaredField(data.EntityNode.get.getInternalName(), obj, data.EntityNode.get.data.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isDisposed(Object obj) {
		return (boolean) Reflection.getDeclaredField(data.EntityNode.get.getInternalName(), obj, data.EntityNode.get.disposed.getInternalName(), boolean.class);
	}
}
