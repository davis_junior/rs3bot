package org.rs3.accessors;

public class I_WorldInfo {
	
	/**
	 * 
	 * @return Type: String
	 */
	public static String getHost(Object obj) {
		return (String) Reflection.getDeclaredField(data.WorldInfo.get.getInternalName(), obj, data.WorldInfo.get.host.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getWorld(Object obj) {
		return (int) Reflection.getDeclaredField(data.WorldInfo.get.getInternalName(), obj, data.WorldInfo.get.world.getInternalName(), int.class) * data.WorldInfo.get.world.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getPort1(Object obj) {
		return (int) Reflection.getDeclaredField(data.WorldInfo.get.getInternalName(), obj, data.WorldInfo.get.port1.getInternalName(), int.class) * data.WorldInfo.get.port1.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getPort2(Object obj) {
		return (int) Reflection.getDeclaredField(data.WorldInfo.get.getInternalName(), obj, data.WorldInfo.get.port2.getInternalName(), int.class) * data.WorldInfo.get.port2.getMultiplier().intValue();
	}
}
