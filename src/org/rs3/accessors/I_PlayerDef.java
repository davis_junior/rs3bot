package org.rs3.accessors;

public class I_PlayerDef {
	
	/**
	 * 
	 * @return Type: long
	 */
	public static long getModelHash(Object obj) {
		return (long) Reflection.getDeclaredField(data.PlayerDef.get.getInternalName(), obj, data.PlayerDef.get.modelHash.getInternalName(), long.class) * data.PlayerDef.get.modelHash.getMultiplier().longValue();
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isFemale(Object obj) {
		return (boolean) Reflection.getDeclaredField(data.PlayerDef.get.getInternalName(), obj, data.PlayerDef.get.female.getInternalName(), boolean.class);
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getEquipment(Object obj) {
		return (int[]) Reflection.getDeclaredField(data.PlayerDef.get.getInternalName(), obj, data.PlayerDef.get.equipment.getInternalName());
	}
}