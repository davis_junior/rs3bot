package org.rs3.accessors;

public class I_GroundInfo {
	
	/**
	 * 
	 * @return Type: Ground[][][]
	 */
	public static Object[][][] getGroundArray(Object obj) {
		return (Object[][][]) Reflection.getDeclaredField(data.GroundInfo.get.getInternalName(), obj, data.GroundInfo.get.groundArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: TileData[]
	 */
	public static Object[] getTileData(Object obj) {
		return (Object[]) Reflection.getDeclaredField(data.GroundInfo.get.getInternalName(), obj, data.GroundInfo.get.tileData.getInternalName());
	}
}
