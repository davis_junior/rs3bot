package org.rs3.accessors;

import java.applet.Applet;
import java.awt.EventQueue;
import java.awt.Rectangle;

public class I_Client {
	
	/**
	 * 
	 * @return Type: Canvas
	 */
	public static Object getCanvas(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.canvas.getInternalClassName(), data.Client.get.canvas.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getCanvasWidth(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.canvasWidth.getInternalClassName(), data.Client.get.canvasWidth.getInternalName(), int.class) * data.Client.get.canvasWidth.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getCanvasHeight(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.canvasHeight.getInternalClassName(), data.Client.get.canvasHeight.getInternalName(), int.class) * data.Client.get.canvasHeight.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: java.applet.Applet
	 */
	public static Applet getApplet1(Object obj) {
		return (Applet) Reflection.getStaticDeclaredField(data.Client.get.applet1.getInternalClassName(), data.Client.get.applet1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.applet.Applet
	 */
	public static Applet getApplet2(Object obj) {
		return (Applet) Reflection.getStaticDeclaredField(data.Client.get.applet2.getInternalClassName(), data.Client.get.applet2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.applet.Applet
	 */
	public static Applet getApplet3(Object obj) {
		return (Applet) Reflection.getStaticDeclaredField(data.Client.get.applet3.getInternalClassName(), data.Client.get.applet3.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Graphics
	 */
	public static Object getRSGraphics(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.graphics.getInternalClassName(), data.Client.get.graphics.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getPlane(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.plane.getInternalClassName(), data.Client.get.plane.getInternalName(), int.class) * data.Client.get.plane.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getCameraPitch(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.cameraPitch.getInternalClassName(), data.Client.get.cameraPitch.getInternalName(), int.class) * data.Client.get.cameraPitch.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getCameraYaw(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.cameraYaw.getInternalClassName(), data.Client.get.cameraYaw.getInternalName(), int.class) * data.Client.get.cameraPitch.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: Player[]
	 */
	public static Object[] getPlayers(Object obj) {
		return (Object[]) Reflection.getStaticDeclaredField(data.Client.get.players.getInternalClassName(), data.Client.get.players.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Player
	 */
	public static Object getMyPlayer(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.myPlayer.getInternalClassName(), data.Client.get.myPlayer.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Cache
	 */
	public static Object getPlayerModelCache(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.playerModelCache.getInternalClassName(), data.Client.get.playerModelCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: NpcNode[]
	 */
	public static Object[] getNpcNodeArray(Object obj) {
		return (Object[]) Reflection.getStaticDeclaredField(data.Client.get.npcNodeArray.getInternalClassName(), data.Client.get.npcNodeArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: HashTable
	 */
	public static Object getNpcNodeCache(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.npcNodeCache.getInternalClassName(), data.Client.get.npcNodeCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int[]
	 */
	public static int[] getNpcNodeIndexArray(Object obj) {
		return (int[]) Reflection.getStaticDeclaredField(data.Client.get.npcNodeIndexArray.getInternalClassName(), data.Client.get.npcNodeIndexArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Location[]
	 */
	public static Object[] getLocationArray(Object obj) {
		return (Object[]) Reflection.getStaticDeclaredField(data.Client.get.locationArray.getInternalClassName(), data.Client.get.locationArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Camera
	 */
	public static Object getCamera(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.camera.getInternalClassName(), data.Client.get.camera.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: GameInfo
	 */
	public static Object getGameInfo(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.gameInfo.getInternalClassName(), data.Client.get.gameInfo.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Render
	 */
	public static Object getRender(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.render.getInternalClassName(), data.Client.get.render.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.awt.Rectangle[]
	 */
	public static Rectangle[] getComponentBoundsArray(Object obj) {
		return (Rectangle[]) Reflection.getStaticDeclaredField(data.Client.get.componentBoundsArray.getInternalClassName(), data.Client.get.componentBoundsArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: HashTable
	 */
	public static Object getComponentNodeCache(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.componentNodeCache.getInternalClassName(), data.Client.get.componentNodeCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Widget[]
	 */
	public static Object[] getWidgetCache(Object obj) {
		return (Object[]) Reflection.getStaticDeclaredField(data.Client.get.widgetCache.getInternalClassName(), data.Client.get.widgetCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: boolean[]
	 */
	public static boolean[] getValidWidgetArray(Object obj) {
		return (boolean[]) Reflection.getStaticDeclaredField(data.Client.get.validWidgetArray.getInternalClassName(), data.Client.get.validWidgetArray.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isMenuOpen(Object obj) {
		return (boolean) Reflection.getStaticDeclaredField(data.Client.get.isMenuOpen.getInternalClassName(), data.Client.get.isMenuOpen.getInternalName(), boolean.class);
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isMenuCollapsed(Object obj) {
		return (boolean) Reflection.getStaticDeclaredField(data.Client.get.isMenuCollapsed.getInternalClassName(), data.Client.get.isMenuCollapsed.getInternalName(), boolean.class);
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMenuX(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.menuX.getInternalClassName(), data.Client.get.menuX.getInternalName(), int.class) * data.Client.get.menuX.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMenuY(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.menuY.getInternalClassName(), data.Client.get.menuY.getInternalName(), int.class) * data.Client.get.menuY.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMenuWidth(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.menuWidth.getInternalClassName(), data.Client.get.menuWidth.getInternalName(), int.class) * data.Client.get.menuWidth.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMenuHeight(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.menuHeight.getInternalClassName(), data.Client.get.menuHeight.getInternalName(), int.class) * data.Client.get.menuHeight.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getSubMenuX(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.subMenuX.getInternalClassName(), data.Client.get.subMenuX.getInternalName(), int.class) * data.Client.get.subMenuX.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getSubMenuY(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.subMenuY.getInternalClassName(), data.Client.get.subMenuY.getInternalName(), int.class) * data.Client.get.subMenuY.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getSubMenuWidth(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.subMenuWidth.getInternalClassName(), data.Client.get.subMenuWidth.getInternalName(), int.class) * data.Client.get.subMenuWidth.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getSubMenuHeight(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.subMenuHeight.getInternalClassName(), data.Client.get.subMenuHeight.getInternalName(), int.class) * data.Client.get.subMenuHeight.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: MenuItemNode
	 */
	public static Object getFirstMenuItem(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.firstMenuItem.getInternalClassName(), data.Client.get.firstMenuItem.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: MenuItemNode
	 */
	public static Object getSecondMenuItem(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.secondMenuItem.getInternalClassName(), data.Client.get.secondMenuItem.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: NodeDeque
	 */
	public static Object getMenuItems(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.menuItems.getInternalClassName(), data.Client.get.menuItems.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: NodeSubQueue
	 */
	public static Object getCollapsedMenuItems(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.collapsedMenuItems.getInternalClassName(), data.Client.get.collapsedMenuItems.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: MenuGroupNode
	 */
	public static Object getCurrentMenuGroupNode(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.currentMenuGroupNode.getInternalClassName(), data.Client.get.currentMenuGroupNode.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: boolean
	 */
	public static boolean isUsableComponentSelected(Object obj) {
		return (boolean) Reflection.getStaticDeclaredField(data.Client.get.isUsableComponentSelected.getInternalClassName(), data.Client.get.isUsableComponentSelected.getInternalName(), boolean.class);
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getLastSelectedUsableComponentId(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.lastSelectedUsableComponentId.getInternalClassName(), data.Client.get.lastSelectedUsableComponentId.getInternalName(), int.class) * data.Client.get.lastSelectedUsableComponentId.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getLastSelectedUsableComponentAction(Object obj) {
		return (String) Reflection.getStaticDeclaredField(data.Client.get.lastSelectedUsableComponentAction.getInternalClassName(), data.Client.get.lastSelectedUsableComponentAction.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getLastSelectedUsableComponentOption(Object obj) {
		return (String) Reflection.getStaticDeclaredField(data.Client.get.lastSelectedUsableComponentOption.getInternalClassName(), data.Client.get.lastSelectedUsableComponentOption.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getMinimapAngle(Object obj) {
		return (float) Reflection.getStaticDeclaredField(data.Client.get.minimapAngle.getInternalClassName(), data.Client.get.minimapAngle.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMinimapScale(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.minimapScale.getInternalClassName(), data.Client.get.minimapScale.getInternalName(), int.class) * data.Client.get.minimapScale.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMinimapOffset(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.minimapOffset.getInternalClassName(), data.Client.get.minimapOffset.getInternalName(), int.class) * data.Client.get.minimapOffset.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getMinimapSetting(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.minimapSetting.getInternalClassName(), data.Client.get.minimapSetting.getInternalName(), int.class) * data.Client.get.minimapSetting.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getDestX(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.destX.getInternalClassName(), data.Client.get.destX.getInternalName(), int.class) * data.Client.get.destX.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getDestY(Object obj) {
		return (int) Reflection.getStaticDeclaredField(data.Client.get.destY.getInternalClassName(), data.Client.get.destY.getInternalName(), int.class) * data.Client.get.destY.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: SettingData
	 */
	public static Object getSettingData(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.settingData.getInternalClassName(), data.Client.get.settingData.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: HashTable
	 */
	public static Object getItemTables(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.itemTables.getInternalClassName(), data.Client.get.itemTables.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.awt.EventQueue
	 */
	public static EventQueue getEventQueue(Object obj) {
		return (EventQueue) Reflection.getStaticDeclaredField(data.Client.get.eventQueue.getInternalClassName(), data.Client.get.eventQueue.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: HashTable
	 */
	public static Object getItemNodeCache(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.itemNodeCache.getInternalClassName(), data.Client.get.itemNodeCache.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ItemDefLoader
	 */
	public static Object getItemDefLoader(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.itemDefLoader.getInternalClassName(), data.Client.get.itemDefLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: GEOffer[][]
	 */
	public static Object[][] getGEOffers(Object obj) {
		return (Object[][]) Reflection.getStaticDeclaredField(data.Client.get.geOffers.getInternalClassName(), data.Client.get.geOffers.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: WorldInfo
	 */
	public static Object getWorldInfo(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.worldInfo.getInternalClassName(), data.Client.get.worldInfo.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: GraphicsSettingsContainer
	 */
	public static Object getGraphicsSettingsContainer(Object obj) {
		return Reflection.getStaticDeclaredField(data.Client.get.graphicsSettingsContainer.getInternalClassName(), data.Client.get.graphicsSettingsContainer.getInternalName());
	}
}