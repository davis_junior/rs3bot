package org.rs3.accessors;

public class I_SDRender extends I_Render {
	
	/**
	 * 
	 * @return Type: Viewport
	 */
	public static Object getClientViewport(Object obj) {
		return Reflection.getDeclaredField(data.SDRender.get.getInternalName(), obj, data.SDRender.get.clientViewport.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getXMultiplier(Object obj) {
		return (float) Reflection.getDeclaredField(data.SDRender.get.getInternalName(), obj, data.SDRender.get.xMultiplier.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getYMultiplier(Object obj) {
		return (float) Reflection.getDeclaredField(data.SDRender.get.getInternalName(), obj, data.SDRender.get.yMultiplier.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getAbsoluteX(Object obj) {
		return (float) Reflection.getDeclaredField(data.SDRender.get.getInternalName(), obj, data.SDRender.get.absoluteX.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getAbsoluteY(Object obj) {
		return (float) Reflection.getDeclaredField(data.SDRender.get.getInternalName(), obj, data.SDRender.get.absoluteY.getInternalName(), float.class);
	}
}
