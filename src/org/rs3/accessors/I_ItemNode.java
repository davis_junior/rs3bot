package org.rs3.accessors;

public class I_ItemNode extends I_Node {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getItemId(Object obj) {
		return (int) Reflection.getDeclaredField(data.ItemNode.get.getInternalName(), obj, data.ItemNode.get.id.getInternalName(), int.class) * data.ItemNode.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getStackSize(Object obj) {
		return (int) Reflection.getDeclaredField(data.ItemNode.get.getInternalName(), obj, data.ItemNode.get.stackSize.getInternalName(), int.class) * data.ItemNode.get.stackSize.getMultiplier().intValue();
	}
}