package org.rs3.accessors;

public class I_Node {
	
	/**
	 * 
	 * @return Type: Node
	 */
	public static Object getPrevious(Object obj) {
		return Reflection.getDeclaredField(data.Node.get.getInternalName(), obj, data.Node.get.prev.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Node
	 */
	public static Object getNext(Object obj) {
		return Reflection.getDeclaredField(data.Node.get.getInternalName(), obj, data.Node.get.next.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: long
	 */
	public static long getId(Object obj) {
		return (long) Reflection.getDeclaredField(data.Node.get.getInternalName(), obj, data.Node.get.id.getInternalName(), long.class) * data.Node.get.id.getMultiplier().longValue();
	}
}
