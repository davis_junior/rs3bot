package org.rs3.accessors;

public abstract class I_AbstractGraphicsSetting {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getValue(Object obj) {
		return (int) Reflection.getDeclaredField(data.AbstractGraphicsSetting.get.getInternalName(), obj, data.AbstractGraphicsSetting.get.value.getInternalName(), int.class) * data.AbstractGraphicsSetting.get.value.getMultiplier().intValue();
	}
	
}
