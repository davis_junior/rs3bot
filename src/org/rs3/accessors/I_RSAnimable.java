package org.rs3.accessors;

public class I_RSAnimable extends I_Interactable {
	
	/**
	 * 
	 * @return Type: short
	 */
	public static short getMinX(Object obj) {
		return (short) Reflection.getDeclaredField(data.RSAnimable.get.getInternalName(), obj, data.RSAnimable.get.minX.getInternalName(), short.class);
	}
	
	/**
	 * 
	 * @return Type: short
	 */
	public static short getMaxX(Object obj) {
		return (short) Reflection.getDeclaredField(data.RSAnimable.get.getInternalName(), obj, data.RSAnimable.get.maxX.getInternalName(), short.class);
	}
	
	/**
	 * 
	 * @return Type: short
	 */
	public static short getMinY(Object obj) {
		return (short) Reflection.getDeclaredField(data.RSAnimable.get.getInternalName(), obj, data.RSAnimable.get.minY.getInternalName(), short.class);
	}
	
	/**
	 * 
	 * @return Type: short
	 */
	public static short getMaxY(Object obj) {
		return (short) Reflection.getDeclaredField(data.RSAnimable.get.getInternalName(), obj, data.RSAnimable.get.maxY.getInternalName(), short.class);
	}
}
