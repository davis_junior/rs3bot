package org.rs3.accessors;

public class I_Quaternion {
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getX(Object obj) {
		return (float) Reflection.getDeclaredField(data.Quaternion.get.getInternalName(), obj, data.Quaternion.get.x.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getY(Object obj) {
		return (float) Reflection.getDeclaredField(data.Quaternion.get.getInternalName(), obj, data.Quaternion.get.y.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getZ(Object obj) {
		return (float) Reflection.getDeclaredField(data.Quaternion.get.getInternalName(), obj, data.Quaternion.get.z.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getA(Object obj) {
		return (float) Reflection.getDeclaredField(data.Quaternion.get.getInternalName(), obj, data.Quaternion.get.a.getInternalName(), float.class);
	}
}
