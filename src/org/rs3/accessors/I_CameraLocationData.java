package org.rs3.accessors;

public class I_CameraLocationData extends I_AbstractCameraLocationData {
	
	/**
	 * 
	 * @return Type: Location
	 */
	public static Object getPoint1(Object obj) {
		return Reflection.getDeclaredField(data.CameraLocationData.get.getInternalName(), obj, data.CameraLocationData.get.point1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Location
	 */
	public static Object getPoint2(Object obj) {
		return Reflection.getDeclaredField(data.CameraLocationData.get.getInternalName(), obj, data.CameraLocationData.get.point2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Quaternion
	 */
	public static Object getQuaternion(Object obj) {
		return Reflection.getDeclaredField(data.CameraLocationData.get.getInternalName(), obj, data.CameraLocationData.get.quaternion.getInternalName());
	}
}
