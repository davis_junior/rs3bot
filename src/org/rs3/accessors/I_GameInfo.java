package org.rs3.accessors;

public class I_GameInfo {
	
	/**
	 * 
	 * @return Type: BaseInfo
	 */
	public static Object getBaseInfo(Object obj) {
		return Reflection.getDeclaredField(data.GameInfo.get.getInternalName(), obj, data.GameInfo.get.baseInfo.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: GroundInfo
	 */
	public static Object getGroundInfo(Object obj) {
		return Reflection.getDeclaredField(data.GameInfo.get.getInternalName(), obj, data.GameInfo.get.groundInfo.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: GroundBytes
	 */
	public static Object getGroundBytes(Object obj) {
		return Reflection.getDeclaredField(data.GameInfo.get.getInternalName(), obj, data.GameInfo.get.groundBytes.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectDefLoader
	 */
	public static Object getObjectDefLoader(Object obj) {
		return Reflection.getDeclaredField(data.GameInfo.get.getInternalName(), obj, data.GameInfo.get.objectDefLoader.getInternalName());
	}
}
