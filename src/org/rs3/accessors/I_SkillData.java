package org.rs3.accessors;

public class I_SkillData {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getExperience(Object obj) {
		return (int) Reflection.getDeclaredField(data.SkillData.get.getInternalName(), obj, data.SkillData.get.experience.getInternalName(), int.class) * data.SkillData.get.experience.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getRealLevel(Object obj) {
		return (int) Reflection.getDeclaredField(data.SkillData.get.getInternalName(), obj, data.SkillData.get.realLevel.getInternalName(), int.class) * data.SkillData.get.realLevel.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getCurrentLevel(Object obj) {
		return (int) Reflection.getDeclaredField(data.SkillData.get.getInternalName(), obj, data.SkillData.get.currentLevel.getInternalName(), int.class) * data.SkillData.get.currentLevel.getMultiplier().intValue();
	}
}
