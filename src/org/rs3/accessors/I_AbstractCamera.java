package org.rs3.accessors;

public class I_AbstractCamera {
	
	/**
	 * 
	 * @return Type: CameraLocationData
	 */
	public static Object getCameraLocationData(Object obj) {
		return Reflection.getDeclaredField(data.AbstractCamera.get.getInternalName(), obj, data.AbstractCamera.get.cameraLocationData.getInternalName());
	}
	
	/**
	 * @return Type: CentralLocationData
	 */
	public static Object getCentralLocationData(Object obj) {
		return Reflection.getDeclaredField(data.AbstractCamera.get.getInternalName(), obj, data.AbstractCamera.get.centralLocationData.getInternalName());
	}
}
