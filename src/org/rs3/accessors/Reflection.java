package org.rs3.accessors;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class Reflection {
	
	private static Map<String, Field> cachedFields = new HashMap<>();
	
	public static byte getByte(Object obj, Field field) {
		try {
			return field.getByte(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static short getShort(Object obj, Field field) {
		try {
			return field.getShort(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static int getInt(Object obj, Field field) {
		try {
			return field.getInt(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static long getLong(Object obj, Field field) {
		try {
			return field.getLong(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static float getFloat(Object obj, Field field) {
		try {
			return field.getFloat(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static double getDouble(Object obj, Field field) {
		try {
			return field.getDouble(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static boolean getBoolean(Object obj, Field field) {
		try {
			return field.getBoolean(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static char getChar(Object obj, Field field) {
		try {
			return field.getChar(obj);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static Object get(Object obj, Field field, Class<?> type) {
		try {
			/*if (type == byte.class || type == Byte.class)
				return field.getByte(obj);
			if (type == short.class || type == Short.class)
				return field.getShort(obj);
			if (type == int.class || type == Integer.class)
				return field.getInt(obj);
			if (type == long.class || type == Long.class)
				return field.getLong(obj);
			if (type == float.class || type == Float.class)
				return field.getFloat(obj);
			if (type == double.class || type == Double.class)
				return field.getDouble(obj);
			if (type == boolean.class || type == Boolean.class)
				return field.getBoolean(obj);
			if (type == char.class || type == Character.class)
				return field.getChar(obj);*/
			
			if (type == null || Object.class.equals(type))
				return field.get(obj);
			if (Byte.class.equals(type))
				return field.getByte(obj);
			if (Short.class.equals(type))
				return field.getShort(obj);
			if (Integer.class.equals(type))
				return field.getInt(obj);
			if (Long.class.equals(type))
				return field.getLong(obj);
			if (Float.class.equals(type))
				return field.getFloat(obj);
			if (Double.class.equals(type))
				return field.getDouble(obj);
			if (Boolean.class.equals(type))
				return field.getBoolean(obj);
			if (Character.class.equals(type))
				return field.getChar(obj);
			
			return field.get(obj);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static Object getStaticDeclaredField(String className, String fieldName, Class<?> type) {
		return getDeclaredField(className, null, fieldName, type);
	}
	
	public static Object getStaticDeclaredField(String className, String fieldName) {
		return getDeclaredField(className, null, fieldName, null);
	}
	
	public static Object getDeclaredField(Object obj, String fieldName, Class<?> type) {
		return getDeclaredField(null, obj, fieldName, type);
	}
	
	public static Object getDeclaredField(Object obj, String fieldName) {
		return getDeclaredField(null, obj, fieldName, null);
	}
	
	public static Object getDeclaredField(String className, Object obj, String fieldName) {
		return getDeclaredField(className, obj, fieldName, null);
	}
	
	// TODO: rework inherited classes private field search; find class first inside catch block
	public static Object getDeclaredField(String className, Object obj, String fieldName, Class<?> type) {
		Field cachedField = cachedFields.get(className + "." + fieldName);
		if (cachedField != null)
			return get(obj, cachedField, type);
			
		Class<?> clazz = null;
		
		try {
			//if (className != null && !className.isEmpty() && obj == null)
			if (className != null && !className.isEmpty()) {
				clazz = CustomClassLoader.get.loadClass(className);
				//clazz = Class.forName(className);
				
				if (clazz == null) {
					if (obj != null)
						clazz = obj.getClass();
					else
						return null;
				}
			} else if (obj != null)
				clazz = obj.getClass();
			else
				return null;
			
			cachedField = cachedFields.get(clazz.getName() + "." + fieldName);
			if (cachedField != null)
				return get(obj, cachedField, type);
			
			Field field = clazz.getDeclaredField(fieldName);
			field.setAccessible(true);
			cachedFields.put(clazz.getName() + "." + fieldName, field);
			return get(obj, field, type);
		} catch (NoSuchFieldException e) {
			try {
				//System.out.println("ERROR " + clazz.getClassLoader());
				
				//System.out.println(obj + ", " + clazz.getName() + ", " + fieldName);
				//printFieldDebugInfo(clazz);
				
				Field field = clazz.getField(fieldName);
				field.setAccessible(true);
				cachedFields.put(clazz.getName() + "." + fieldName, field);
				return get(obj, field, type);
			} catch (NoSuchFieldException e1) {
				while (clazz != Object.class) {
					clazz = clazz.getSuperclass();
					
					try {
						Field field = clazz.getDeclaredField(fieldName);
						field.setAccessible(true);
						cachedFields.put(clazz.getName() + "." + fieldName, field);
						return get(obj, field, type);
					} catch (NoSuchFieldException e2) {
						e2.printStackTrace();
					} catch (SecurityException | IllegalArgumentException e2) {
						e2.printStackTrace();
					}
				}
				
				//e1.printStackTrace();
			} catch (SecurityException | IllegalArgumentException e1) {
				e1.printStackTrace();
			}
			
			e.printStackTrace();
		} catch (SecurityException | IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Object getStaticField(String className, String fieldName, Class<?> type) {
		return getField(className, null, fieldName, type);
	}
	
	public static Object getStaticField(String className, String fieldName) {
		return getField(className, null, fieldName, null);
	}
	
	public static Object getField(Object obj, String fieldName, Class<?> type) {
		return getField(null, obj, fieldName, type);
	}
	
	public static Object getField(Object obj, String fieldName) {
		return getField(null, obj, fieldName, null);
	}
	
	public static Object getField(String className, Object obj, String fieldName) {
		return getField(className, obj, fieldName, null);
	}
	
	public static Object getField(String className, Object obj, String fieldName, Class<?> type) {
		Field cachedField = cachedFields.get(className + "." + fieldName);
		if (cachedField != null)
			return get(obj, cachedField, type);
		
		try {
			Class<?> clazz;
			
			//if (className != null && !className.isEmpty() && obj == null)
			if (className != null && !className.isEmpty()) {
				clazz = CustomClassLoader.get.loadClass(className);
				//clazz = Class.forName(className);
				
				if (clazz == null) {
					if (obj != null)
						clazz = obj.getClass();
					else
						return null;
				}
			} else if (obj != null)
				clazz = obj.getClass();
			else
				return null;
			
			cachedField = cachedFields.get(clazz.getName() + "." + fieldName);
			if (cachedField != null)
				return get(obj, cachedField, type);
			
			Field field = clazz.getField(fieldName);
			field.setAccessible(true);
			cachedFields.put(clazz.getName() + "." + fieldName, field);
			return get(obj, field, type);
		} catch (NoSuchFieldException e) {
			return getDeclaredField(className, obj, fieldName, type);
		} catch (SecurityException | IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/*public static <T extends InternalAccessor> T[] getStaticArray(Class<T> returnClass, String className, String fieldName, Class<?> type) {
		return getArray(returnClass, className, null, fieldName, type);
	}
	
	public static <T extends InternalAccessor> T[] getStaticArray(Class<T> returnClass, String className, String fieldName) {
		return getArray(returnClass, className, null, fieldName, null);
	}
	
	public static <T extends InternalAccessor> T[] getArray(Class<T> returnClass, Object obj, String fieldName, Class<?> type) {
		return getArray(returnClass, null, obj, fieldName, type);
	}
	
	public static <T extends InternalAccessor> T[] getArray(Class<T> returnClass, Object obj, String fieldName) {
		return getArray(returnClass, null, obj, fieldName, null);
	}
	
	public static <T extends InternalAccessor> T[] getArray(Class<T> returnClass, String className, Object obj, String fieldName) {
		return getArray(returnClass, className, obj, fieldName, null);
	}
	
	public static <T extends InternalAccessor> T[] getArray(Class<T> returnClass, String className, Object obj, String fieldName, Class<?> type) {
		Object arrayObject = Reflection.getField(className, obj, fieldName, type);
		
		if (arrayObject == null)
			return null;
		
		int length = Array.getLength(arrayObject);
		//T[] iArray = new T[length]; // unable to create generic array
		@SuppressWarnings("unchecked")
		final T[] iArray = (T[]) Array.newInstance(returnClass, length);
		
		for (int i = 0; i < length; i++) {
			Object elem = Array.get(arrayObject, i);
			
			if (elem == null) {
				iArray[i] = null;
				continue;
			}
				
			iArray[i] = InternalAccessor.create(returnClass, elem);
		}
		
		return iArray;
	}
	
	// TODO: not tested fully
	public static <T extends InternalAccessor> T[][][] getArray3D(Class<T> returnClass, String className, Object obj, String fieldName, Class<?> type) {
		Object arrayObject = Reflection.getField(className, obj, fieldName, type);
		
		if (arrayObject == null)
			return null;
		
		Object array2Dtemp = Array.get(arrayObject, 0);
		Object array3Dtemp = Array.get(array2Dtemp, 0);
		
		int length1D = Array.getLength(arrayObject);
		int length2D = Array.getLength(array2Dtemp);
		int length3D = Array.getLength(array3Dtemp);
		//T[] iArray = new T[length]; // unable to create generic array
		@SuppressWarnings("unchecked")
		final T[][][] iArray = (T[][][]) Array.newInstance(returnClass, length1D, length2D, length3D);
		
		//System.out.println(length1D + " " + length2D + " " + length3D);
		
		for (int i = 0; i < length1D; i++) {
			Object array1D = Array.get(arrayObject, i);
			
			for (int i2 = 0; i2 < length2D; i2++) {
				Object array2D = Array.get(array1D, i2);
				
				for (int i3 = 0; i3 < length3D; i3++) {
					Object elem = Array.get(array2D, i3);
					
					if (elem == null) {
						iArray[i][i2][i3] = null;
						continue;
					}
						
					iArray[i][i2][i3] = InternalAccessor.create(returnClass, elem);
				}
			}
		}
		
		return iArray;
	}*/
	
	public static Object invokeMethod(String className, String methodName, Object obj, Class<?>[] classArgs, Object... args) {
		try {
			Class<?> clazz = CustomClassLoader.get.loadClass(className);
			if (clazz != null) {
				Method method = clazz.getDeclaredMethod(methodName, classArgs);
				if (method != null) {
					method.setAccessible(true);
					return method.invoke(obj, args);
				}
			}
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void printFieldNameDebugInfo(Class<?> clazz) {
		System.out.println(clazz.getName() + " extends " + clazz.getSuperclass().getName());
		System.out.println("Declared fields: ");
		for (Field f : clazz.getDeclaredFields()) {
			System.out.println("  --" + f.getName());
		}
		
		System.out.println("Fields: ");
		for (Field f : clazz.getFields()) {
			System.out.println("  --" + f.getName());
		}
	}
	
	public static void printDeclaredFields(Object obj) {
		printDeclaredFields((Class<?>)null, obj);
	}
	
	public static void printDeclaredFields(String className, Object obj) {
		try {
			printDeclaredFields(CustomClassLoader.get.loadClass(className), obj);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void printDeclaredFields(Class<?> clazz, Object obj) {
		if (obj == null)
			return;
		
		if (clazz == null)
			clazz = obj.getClass();
		
		System.out.println(clazz.getName() + ": " + obj + " extends " + clazz.getSuperclass().getName());
		System.out.println("Declared fields: ");
		for (Field f : clazz.getDeclaredFields()) {
			f.setAccessible(true);
			try {
				System.out.println("  --" + f.getName() + " = " + f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void printFields(Object obj) {
		if (obj == null)
			return;
		
		Class<?> clazz = obj.getClass();
		if (clazz == null)
			return;
		
		System.out.println(clazz.getName() + ": " + obj + " extends " + clazz.getSuperclass().getName());
		System.out.println("Fields: ");
		for (Field f : clazz.getFields()) {
			f.setAccessible(true);
			try {
				System.out.println("  --" + f.getName() + " = " + f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
