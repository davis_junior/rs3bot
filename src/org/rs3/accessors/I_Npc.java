package org.rs3.accessors;

public class I_Npc extends I_Character {
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getTitle(Object obj) {
		return (String) Reflection.getDeclaredField(data.Npc.get.getInternalName(), obj, data.Npc.get.title.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: NpcDefinition
	 */
	public static Object getNpcDefinition(Object obj) {
		return Reflection.getDeclaredField(data.Npc.get.getInternalName(), obj, data.Npc.get.npcDefinition.getInternalName());
	}
}