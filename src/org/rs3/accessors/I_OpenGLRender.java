package org.rs3.accessors;

public class I_OpenGLRender extends I_Render {
	
	/**
	 * 
	 * @return Type: java.lang.Object?
	 */
	public static Object getOpenGL(Object obj) {
		return Reflection.getDeclaredField(data.OpenGLRender.get.getInternalName(), obj, data.OpenGLRender.get.openGL.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Viewport
	 */
	public static Object getClientViewport(Object obj) {
		return Reflection.getDeclaredField(data.OpenGLRender.get.getInternalName(), obj, data.OpenGLRender.get.clientViewport.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getXMultiplier(Object obj) {
		return (float) Reflection.getDeclaredField(data.OpenGLRender.get.getInternalName(), obj, data.OpenGLRender.get.xMultiplier.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getYMultiplier(Object obj) {
		return (float) Reflection.getDeclaredField(data.OpenGLRender.get.getInternalName(), obj, data.OpenGLRender.get.yMultiplier.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getAbsoluteX(Object obj) {
		return (float) Reflection.getDeclaredField(data.OpenGLRender.get.getInternalName(), obj, data.OpenGLRender.get.absoluteX.getInternalName(), float.class);
	}
	
	/**
	 * 
	 * @return Type: float
	 */
	public static float getAbsoluteY(Object obj) {
		return (float) Reflection.getDeclaredField(data.OpenGLRender.get.getInternalName(), obj, data.OpenGLRender.get.absoluteY.getInternalName(), float.class);
	}
}
