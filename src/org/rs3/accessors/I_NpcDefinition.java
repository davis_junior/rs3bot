package org.rs3.accessors;

public class I_NpcDefinition {
	
	/**
	 * 
	 * @return Type: java.lang.String[]
	 */
	public static String[] getActions(Object obj) {
		return (String[]) Reflection.getDeclaredField(data.NpcDefinition.get.getInternalName(), obj, data.NpcDefinition.get.actions.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.NpcDefinition.get.getInternalName(), obj, data.NpcDefinition.get.id.getInternalName(), int.class) * data.NpcDefinition.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getName(Object obj) {
		return (String) Reflection.getDeclaredField(data.NpcDefinition.get.getInternalName(), obj, data.NpcDefinition.get.name.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: HashTable
	 */
	/*public static Object getNodeTable() {
		return Reflection.getDeclaredField(data.NpcDefinition.get.getInternalName(), obj, data.NpcDefinition.get.nodeTable.getInternalName());
	}*/
}