package org.rs3.accessors;

public class I_MenuItemNode extends I_NodeSub {
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getOption(Object obj) {
		return (String) Reflection.getDeclaredField(data.MenuItemNode.get.getInternalName(), obj, data.MenuItemNode.get.option.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getAction(Object obj) {
		return (String) Reflection.getDeclaredField(data.MenuItemNode.get.getInternalName(), obj, data.MenuItemNode.get.action.getInternalName());
	}
}
