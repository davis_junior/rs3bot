package org.rs3.accessors;

public class I_Reference extends I_NodeSub {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getIndex(Object obj) {
		return (int) Reflection.getDeclaredField(data.Reference.get.getInternalName(), obj, data.Reference.get.index.getInternalName(), int.class) * data.Reference.get.index.getMultiplier().intValue();
	}
}