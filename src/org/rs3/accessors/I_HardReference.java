package org.rs3.accessors;

public class I_HardReference extends I_Reference {
	
	/**
	 * 
	 * @return Type: java.lang.Object
	 */
	public static Object getHardReference(Object obj) {
		return Reflection.getDeclaredField(data.HardReference.get.getInternalName(), obj, data.HardReference.get.hardReference.getInternalName());
	}
}