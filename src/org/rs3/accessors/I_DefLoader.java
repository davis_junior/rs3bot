package org.rs3.accessors;

public class I_DefLoader { // actually implements I_AbstractDefLoader
	
	/**
	 * 
	 * @return Type: int
	 */
	/*public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.DefLoader.get.getInternalName(), obj, data.DefLoader.get.id.getInternalName(), int.class) * data.DefLoader.get.id.getMultiplier().intValue();
	}*/
	
	/**
	 * 
	 * @return Type: Cache
	 */
	public static Object getDefinitionCache(Object obj) {
		return Reflection.getDeclaredField(data.DefLoader.get.getInternalName(), obj, data.DefLoader.get.definitionCache.getInternalName());
	}
}
