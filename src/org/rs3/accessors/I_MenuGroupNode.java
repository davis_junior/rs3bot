package org.rs3.accessors;

public class I_MenuGroupNode extends I_NodeSub {
	
	/**
	 * 
	 * @return Type: NodeSubQueue
	 */
	public static Object getItems(Object obj) {
		return Reflection.getDeclaredField(data.MenuGroupNode.get.getInternalName(), obj, data.MenuGroupNode.get.items.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: java.lang.String
	 */
	public static String getOptions(Object obj) {
		return (String) Reflection.getDeclaredField(data.MenuGroupNode.get.getInternalName(), obj, data.MenuGroupNode.get.options.getInternalName());
	}
}
