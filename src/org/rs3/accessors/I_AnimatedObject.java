package org.rs3.accessors;

public class I_AnimatedObject {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.id.getInternalName(), int.class) * data.AnimatedObject.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: Model
	 */
	public static Object getModel(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.model.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Interactable
	 */
	public static Object getInteractable(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.interactable.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Animator
	 */
	public static Object getAnimator1(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.animator1.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: Animator
	 */
	public static Object getAnimator2(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.animator2.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getAnimationId(Object obj) {
		return (int) Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.animationId.getInternalName(), int.class) * data.AnimatedObject.get.animationId.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getFace(Object obj) {
		return (int) Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.face.getInternalName(), int.class) * data.AnimatedObject.get.face.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: IdComposite_AnimatedObject
	 */
	public static Object getIdComposite(Object obj) {
		return Reflection.getDeclaredField(data.AnimatedObject.get.getInternalName(), obj, data.AnimatedObject.get.idComposite.getInternalName());
	}
}
