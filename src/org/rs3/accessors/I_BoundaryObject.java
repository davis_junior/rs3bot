package org.rs3.accessors;

public class I_BoundaryObject extends I_AbstractBoundary {
	
	/**
	 * 
	 * @return Type: int
	 */
	public static int getId(Object obj) {
		return (int) Reflection.getDeclaredField(data.BoundaryObject.get.getInternalName(), obj, data.BoundaryObject.get.id.getInternalName(), int.class) * data.BoundaryObject.get.id.getMultiplier().intValue();
	}
	
	/**
	 * 
	 * @return Type: Model
	 */
	public static Object getModel(Object obj) {
		return Reflection.getDeclaredField(data.BoundaryObject.get.getInternalName(), obj, data.BoundaryObject.get.model.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: ObjectDefLoader
	 */
	public static Object getDefinitionLoader(Object obj) {
		return Reflection.getDeclaredField(data.BoundaryObject.get.getInternalName(), obj, data.BoundaryObject.get.definitionLoader.getInternalName());
	}
	
	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getAnimationId(Object obj) {
		return (byte) Reflection.getDeclaredField(data.BoundaryObject.get.getInternalName(), obj, data.BoundaryObject.get.animationId.getInternalName(), byte.class);
	}

	/**
	 * 
	 * @return Type: byte
	 */
	public static byte getFace(Object obj) {
		return (byte) Reflection.getDeclaredField(data.BoundaryObject.get.getInternalName(), obj, data.BoundaryObject.get.face.getInternalName(), byte.class);
	}
}