package org.rs3.scripts;

import java.util.EventListener;
import java.util.List;

import org.rs3.scripts.parameters.ScriptParameter;

public abstract class Script implements EventListener {
	
	public long startTime = -1;
	
	public volatile boolean stopInterrupt;
	public volatile boolean stopped;
	
	public List<ScriptParameter> params;
	public boolean useConstructorParams;
	
	public Script() {
		stopInterrupt = false;
		stopped = true;
		
		params = null;
		useConstructorParams = false;
	}
	
	/**
	 * Skips script setupParams() call and uses the supplied parameters instead.
	 * 
	 * @param params
	 */
	public Script(List<ScriptParameter> params) {
		this();
		
		this.params = params;
		useConstructorParams = true;
	}
	
	public abstract List<ScriptParameter> setupParams();
	public abstract void loadDataParams();
	public abstract String getInfo();
	public abstract boolean onStart();
	public abstract void run();
	public abstract boolean onStop();
	
	public void interruptStop() {
		stopInterrupt = true;
	}
	
	public boolean shouldStop() {
		return stopInterrupt || stopped;
	}
	
	public boolean isStopped() {
		return stopped;
	}
	
	public long getStartTime() {
		return startTime;
	}
}
