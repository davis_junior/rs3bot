package org.rs3.scripts.mining;

import org.rs3.api.objects.Area;
import org.rs3.api.objects.Tile;

public enum OreType {
	CLAY(
			"Clay", // item name
			434, // item id
			// area - all 3 rocks
			new Area( new Tile(2979, 3241, 0), new Tile(2980, 3236, 0) ), // area
			"Clay rocks", // menu option
			new int[] { 72077, 72075 } // object ids
	),
	
	COPPER(
			"Copper ore", // item name
			436, // item id
			// area - all rocks?
			new Area( new Tile(2969, 3236, 0), new Tile(2969, 3235, 0), new Tile(2969, 3234, 0), new Tile(2969, 3233, 0), new Tile(2968, 3233, 0), new Tile(2968, 3232, 0), new Tile(2967, 3232, 0), new Tile(2966, 3232, 0), new Tile(2966, 3233, 0), new Tile(2966, 3234, 0), new Tile(2965, 3234, 0), new Tile(2965, 3235, 0), new Tile(2965, 3236, 0), new Tile(2966, 3236, 0), new Tile(2967, 3236, 0), new Tile(2968, 3236, 0) ), // area
			"Copper ore rocks", // menu option
			new int[] { 72098, 72099, 72100 } // object ids
	),
	
	IRON(
			"Iron ore", // item name
			440, // item id
			// area - 2 corner rocks
			//new Area( new Tile(2967, 3239, 0), new Tile(2967, 3238, 0), new Tile(2966, 3238, 0), new Tile(2966, 3239, 0) ),
			// area - all 4 rocks
			new Area( new Tile(2971, 3240, 0), new Tile(2971, 3239, 0), new Tile(2971, 3238, 0), new Tile(2971, 3237, 0), new Tile(2970, 3237, 0), new Tile(2969, 3237, 0), new Tile(2968, 3237, 0), new Tile(2967, 3237, 0), new Tile(2966, 3237, 0), new Tile(2966, 3238, 0), new Tile(2966, 3239, 0), new Tile(2967, 3239, 0), new Tile(2967, 3240, 0), new Tile(2968, 3240, 0), new Tile(2969, 3240, 0), new Tile(2970, 3240, 0) ), // area
			"Iron ore rocks", // menu option
			new int[] { 72081, 72082, 72083 } // object ids
	),
	;
	
	private String itemName;
	private int itemId;
	private Area area;
	private String menuOption;
	private int[] objectIds;
	
	private static OreType cur;
	
	public static OreType getCurrent() {
		return cur;
	}
	
	public static void setCurrent(OreType oreType) {
		cur = oreType;
	}
	
	OreType(String itemName, int itemId, Area area, String menuOption, int[] objectIds) {
		this.itemName = itemName;
		this.itemId = itemId;
		this.area = area;
		this.menuOption = menuOption;
		this.objectIds = objectIds;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public int getItemId() {
		return itemId;
	}
	
	public Area getArea() {
		return area;
	}
	
	public String getMenuOption() {
		return menuOption;
	}
	
	public int[] getObjectIds() {
		return objectIds;
	}
}