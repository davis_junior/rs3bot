package org.rs3.scripts.mining;

import org.rs3.api.objects.Area;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;

public enum MineLocation {
	RIMMINGTON(
			new Area(new Tile(2964, 3250, 0), new Tile(2984, 3225, 0)), // mine area
			new Tile(2955, 3297, 0), // bank tile (clan camp)
			//new Tile(3047, 3236, 0), // deposit box tile (port sarim)
			// mineToBankTilePath
			new TilePath( new Tile(2969, 3237, 0), new Tile(2970, 3237, 0), new Tile(2970, 3238, 0), new Tile(2970, 3239, 0), new Tile(2971, 3239, 0), new Tile(2971, 3240, 0), new Tile(2971, 3241, 0), new Tile(2971, 3242, 0), new Tile(2971, 3243, 0), new Tile(2971, 3244, 0), new Tile(2971, 3245, 0), new Tile(2971, 3246, 0), new Tile(2971, 3247, 0), new Tile(2971, 3248, 0), new Tile(2971, 3249, 0), new Tile(2971, 3250, 0), new Tile(2971, 3251, 0), new Tile(2971, 3252, 0), new Tile(2971, 3253, 0), new Tile(2971, 3254, 0), new Tile(2970, 3254, 0), new Tile(2970, 3255, 0), new Tile(2970, 3256, 0), new Tile(2970, 3257, 0), new Tile(2970, 3258, 0), new Tile(2970, 3259, 0), new Tile(2970, 3260, 0), new Tile(2969, 3260, 0), new Tile(2969, 3261, 0), new Tile(2969, 3262, 0), new Tile(2969, 3263, 0), new Tile(2969, 3264, 0), new Tile(2969, 3265, 0), new Tile(2969, 3266, 0), new Tile(2968, 3266, 0), new Tile(2968, 3267, 0), new Tile(2968, 3268, 0), new Tile(2968, 3269, 0), new Tile(2968, 3270, 0), new Tile(2968, 3271, 0), new Tile(2968, 3272, 0), new Tile(2968, 3273, 0), new Tile(2968, 3274, 0), new Tile(2967, 3274, 0), new Tile(2967, 3275, 0), new Tile(2967, 3276, 0), new Tile(2967, 3277, 0), new Tile(2966, 3277, 0), new Tile(2966, 3278, 0), new Tile(2965, 3278, 0), new Tile(2965, 3279, 0), new Tile(2965, 3280, 0), new Tile(2965, 3281, 0), new Tile(2965, 3282, 0), new Tile(2964, 3282, 0), new Tile(2963, 3282, 0), new Tile(2963, 3283, 0), new Tile(2963, 3284, 0), new Tile(2963, 3285, 0), new Tile(2963, 3286, 0), new Tile(2962, 3286, 0), new Tile(2962, 3287, 0), new Tile(2962, 3288, 0), new Tile(2961, 3288, 0), new Tile(2960, 3288, 0), new Tile(2960, 3289, 0), new Tile(2960, 3290, 0), new Tile(2960, 3291, 0), new Tile(2960, 3292, 0), new Tile(2959, 3292, 0), new Tile(2958, 3292, 0), new Tile(2958, 3293, 0), new Tile(2958, 3294, 0), new Tile(2958, 3295, 0), new Tile(2957, 3295, 0), new Tile(2957, 3296, 0), new Tile(2957, 3297, 0), new Tile(2956, 3297, 0) ),
			new Area[] { // mineToBankAreaPath (port sarim deposit box)
				new Area( new Tile(2965, 3236, 0), new Tile(2965, 3237, 0), new Tile(2966, 3237, 0), new Tile(2966, 3238, 0), new Tile(2966, 3239, 0), new Tile(2967, 3239, 0), new Tile(2967, 3240, 0), new Tile(2968, 3240, 0), new Tile(2969, 3240, 0), new Tile(2969, 3241, 0), new Tile(2970, 3241, 0), new Tile(2970, 3242, 0), new Tile(2971, 3242, 0), new Tile(2972, 3242, 0), new Tile(2972, 3241, 0), new Tile(2972, 3240, 0), new Tile(2972, 3239, 0), new Tile(2972, 3238, 0), new Tile(2971, 3238, 0), new Tile(2971, 3237, 0), new Tile(2971, 3236, 0), new Tile(2970, 3236, 0), new Tile(2969, 3236, 0), new Tile(2968, 3236, 0), new Tile(2967, 3236, 0), new Tile(2966, 3236, 0) ),
				new Area( new Tile(2971, 3251, 0), new Tile(2971, 3252, 0), new Tile(2971, 3253, 0), new Tile(2972, 3253, 0), new Tile(2972, 3254, 0), new Tile(2973, 3254, 0), new Tile(2974, 3254, 0), new Tile(2975, 3254, 0), new Tile(2976, 3254, 0), new Tile(2977, 3254, 0), new Tile(2978, 3254, 0), new Tile(2979, 3254, 0), new Tile(2980, 3254, 0), new Tile(2981, 3254, 0), new Tile(2982, 3254, 0), new Tile(2982, 3253, 0), new Tile(2982, 3252, 0), new Tile(2982, 3251, 0), new Tile(2982, 3250, 0), new Tile(2982, 3249, 0), new Tile(2982, 3248, 0), new Tile(2982, 3247, 0), new Tile(2982, 3246, 0), new Tile(2982, 3245, 0), new Tile(2981, 3245, 0), new Tile(2980, 3245, 0), new Tile(2979, 3245, 0), new Tile(2978, 3245, 0), new Tile(2977, 3245, 0), new Tile(2976, 3245, 0), new Tile(2976, 3246, 0), new Tile(2975, 3246, 0), new Tile(2975, 3247, 0), new Tile(2975, 3248, 0), new Tile(2975, 3249, 0), new Tile(2975, 3250, 0), new Tile(2974, 3250, 0), new Tile(2973, 3250, 0), new Tile(2972, 3250, 0), new Tile(2971, 3250, 0) ),
				new Area( new Tile(2991, 3254, 0), new Tile(2992, 3254, 0), new Tile(2993, 3254, 0), new Tile(2994, 3254, 0), new Tile(2995, 3254, 0), new Tile(2996, 3254, 0), new Tile(2997, 3254, 0), new Tile(2998, 3254, 0), new Tile(2999, 3254, 0), new Tile(3000, 3254, 0), new Tile(3001, 3254, 0), new Tile(3002, 3254, 0), new Tile(3002, 3253, 0), new Tile(3002, 3252, 0), new Tile(3002, 3251, 0), new Tile(3002, 3250, 0), new Tile(3002, 3249, 0), new Tile(3002, 3248, 0), new Tile(3002, 3247, 0), new Tile(3002, 3246, 0), new Tile(3002, 3245, 0), new Tile(3001, 3245, 0), new Tile(3000, 3245, 0), new Tile(2999, 3245, 0), new Tile(2998, 3245, 0), new Tile(2997, 3245, 0), new Tile(2996, 3245, 0), new Tile(2995, 3245, 0), new Tile(2994, 3245, 0), new Tile(2993, 3245, 0), new Tile(2992, 3245, 0), new Tile(2991, 3245, 0), new Tile(2991, 3246, 0), new Tile(2991, 3247, 0), new Tile(2991, 3248, 0), new Tile(2991, 3249, 0), new Tile(2991, 3250, 0), new Tile(2991, 3251, 0), new Tile(2991, 3252, 0), new Tile(2991, 3253, 0) ),
				new Area( new Tile(3007, 3244, 0), new Tile(3008, 3244, 0), new Tile(3009, 3244, 0), new Tile(3010, 3244, 0), new Tile(3010, 3243, 0), new Tile(3011, 3243, 0), new Tile(3012, 3243, 0), new Tile(3013, 3243, 0), new Tile(3014, 3243, 0), new Tile(3015, 3243, 0), new Tile(3016, 3243, 0), new Tile(3016, 3244, 0), new Tile(3016, 3245, 0), new Tile(3017, 3245, 0), new Tile(3018, 3245, 0), new Tile(3019, 3245, 0), new Tile(3019, 3244, 0), new Tile(3020, 3244, 0), new Tile(3020, 3243, 0), new Tile(3020, 3242, 0), new Tile(3020, 3241, 0), new Tile(3019, 3241, 0), new Tile(3018, 3241, 0), new Tile(3017, 3241, 0), new Tile(3016, 3241, 0), new Tile(3015, 3241, 0), new Tile(3014, 3241, 0), new Tile(3014, 3240, 0), new Tile(3014, 3239, 0), new Tile(3013, 3239, 0), new Tile(3012, 3239, 0), new Tile(3011, 3239, 0), new Tile(3010, 3239, 0), new Tile(3009, 3239, 0), new Tile(3009, 3238, 0), new Tile(3008, 3238, 0), new Tile(3007, 3238, 0), new Tile(3007, 3239, 0), new Tile(3007, 3240, 0), new Tile(3007, 3241, 0), new Tile(3007, 3242, 0), new Tile(3007, 3243, 0) ),
				new Area( new Tile(3025, 3243, 0), new Tile(3026, 3243, 0), new Tile(3027, 3243, 0), new Tile(3028, 3243, 0), new Tile(3029, 3243, 0), new Tile(3029, 3242, 0), new Tile(3029, 3241, 0), new Tile(3029, 3240, 0), new Tile(3029, 3239, 0), new Tile(3029, 3238, 0), new Tile(3029, 3237, 0), new Tile(3030, 3237, 0), new Tile(3031, 3237, 0), new Tile(3032, 3237, 0), new Tile(3033, 3237, 0), new Tile(3034, 3237, 0), new Tile(3035, 3237, 0), new Tile(3036, 3237, 0), new Tile(3036, 3236, 0), new Tile(3036, 3235, 0), new Tile(3036, 3234, 0), new Tile(3035, 3234, 0), new Tile(3034, 3234, 0), new Tile(3033, 3234, 0), new Tile(3032, 3234, 0), new Tile(3031, 3234, 0), new Tile(3030, 3234, 0), new Tile(3029, 3234, 0), new Tile(3029, 3233, 0), new Tile(3028, 3233, 0), new Tile(3027, 3233, 0), new Tile(3026, 3233, 0), new Tile(3026, 3234, 0), new Tile(3026, 3235, 0), new Tile(3026, 3236, 0), new Tile(3026, 3237, 0), new Tile(3026, 3238, 0), new Tile(3026, 3239, 0), new Tile(3026, 3240, 0), new Tile(3025, 3240, 0), new Tile(3025, 3241, 0), new Tile(3025, 3242, 0) ),
				new Area( new Tile(3042, 3237, 0), new Tile(3043, 3237, 0), new Tile(3044, 3237, 0), new Tile(3045, 3237, 0), new Tile(3046, 3237, 0), new Tile(3047, 3237, 0), new Tile(3048, 3237, 0), new Tile(3049, 3237, 0), new Tile(3050, 3237, 0), new Tile(3051, 3237, 0), new Tile(3052, 3237, 0), new Tile(3052, 3236, 0), new Tile(3052, 3235, 0), new Tile(3052, 3234, 0), new Tile(3051, 3234, 0), new Tile(3050, 3234, 0), new Tile(3049, 3234, 0), new Tile(3048, 3234, 0), new Tile(3047, 3234, 0), new Tile(3046, 3234, 0), new Tile(3045, 3234, 0), new Tile(3044, 3234, 0), new Tile(3043, 3234, 0), new Tile(3043, 3235, 0), new Tile(3042, 3235, 0), new Tile(3042, 3236, 0) )
			}
	);
	
	private Area mainMineArea;
	private Tile bankTile;
	private TilePath mineToBankTilePath;
	private Area[] mineToLodestoneAreaPath;
	
	private static MineLocation cur;
	
	public static MineLocation getCurrent() {
		return cur;
	}
	
	public static void setCurrent(MineLocation mineLocation) {
		cur = mineLocation;
	}
	
	MineLocation(Area mainMineArea, Tile depositBoxTile, TilePath mineToBankTilePath, Area[] mineToLodestoneAreaPath) {
		this.mainMineArea = mainMineArea;
		this.bankTile = depositBoxTile;
		this.mineToBankTilePath = mineToBankTilePath;
		this.mineToLodestoneAreaPath = mineToLodestoneAreaPath;
	}
	
	public Area getMainMineArea() {
		return mainMineArea;
	}
	
	public Tile getBankTile() {
		return bankTile;
	}
	
	public TilePath getRandomPathToBank() {
		return mineToBankTilePath.randomize(4, 4);
	}
	
	public Area[] getMineToLodestoneAreaPath() {
		return mineToLodestoneAreaPath;
	}
	
	public TilePath getRandomPathToMine_fromLodestone() {
		Tile[] tiles = new Tile[mineToLodestoneAreaPath.length];
		
		for (int i = 0; i < mineToLodestoneAreaPath.length; i++) {
			tiles[i] = mineToLodestoneAreaPath[i].getRandomTile();
		}
		
		return new TilePath(tiles).reverse();
	}
}
