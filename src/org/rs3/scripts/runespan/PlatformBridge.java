package org.rs3.scripts.runespan;

import java.util.ArrayList;
import java.util.List;

import org.rs3.api.objects.Tile;

public class PlatformBridge {
	
	private static final PlatformBridge[] lowerBridges = new PlatformBridge[] {
		new PlatformBridge(Island.LOWER_1, Island.LOWER_2, Platform.ICE, new Tile(3996, 6118, 1), false),
		new PlatformBridge(Island.LOWER_1, Island.LOWER_4, Platform.EARTH, new Tile(4002, 6118, 1), false),
		new PlatformBridge(Island.LOWER_1, Island.LOWER_8, Platform.FLOAT, new Tile(4007, 6104, 1), false),
		new PlatformBridge(Island.LOWER_1, Island.LOWER_10, Platform.FLOAT, new Tile(3997, 6094, 1), false),
		new PlatformBridge(Island.LOWER_1, Island.LOWER_7, Platform.ICE, new Tile(3991, 6097, 1), false),
		new PlatformBridge(Island.LOWER_1, Island.LOWER_51, Platform.FLOAT, new Tile(3988, 6103, 1), false),
		new PlatformBridge(Island.LOWER_1, Island.LOWER_50, Platform.EARTH, new Tile(3983, 6112, 1), false),
		
		new PlatformBridge(Island.LOWER_2, Island.LOWER_1, Platform.ICE, new Tile(3991, 6123, 1), true),
		
		new PlatformBridge(Island.LOWER_3, Island.LOWER_4, Platform.EARTH, new Tile(3998, 6134, 1), true),
		
		new PlatformBridge(Island.LOWER_4, Island.LOWER_3, Platform.EARTH, new Tile(4004, 6134, 1), false),
		new PlatformBridge(Island.LOWER_4, Island.LOWER_5, Platform.ICE, new Tile(4017, 6130, 1), false),
		new PlatformBridge(Island.LOWER_4, Island.LOWER_6, Platform.SMALL_MISSILE, new Tile(4012, 6122, 1), false),
		new PlatformBridge(Island.LOWER_4, Island.LOWER_1, Platform.EARTH, new Tile(4007, 6123, 1), true),
		
		new PlatformBridge(Island.LOWER_5, Island.LOWER_4, Platform.ICE, new Tile(4023, 6130, 1), true),
		
		new PlatformBridge(Island.LOWER_6, Island.LOWER_4, Platform.SMALL_MISSILE, new Tile(4012, 6116, 1), true),
		
		new PlatformBridge(Island.LOWER_7, Island.LOWER_1, Platform.ICE, new Tile(3986, 6092, 1), true),
		
		new PlatformBridge(Island.LOWER_8, Island.LOWER_9, Platform.ICE, new Tile(4023, 6110, 1), false),
		new PlatformBridge(Island.LOWER_8, Island.LOWER_13, Platform.EARTH, new Tile(4019, 6099, 1), false),
		new PlatformBridge(Island.LOWER_8, Island.LOWER_1, Platform.FLOAT, new Tile(4013, 6104, 1), true),
		
		new PlatformBridge(Island.LOWER_9, Island.LOWER_8, Platform.ICE, new Tile(4023, 6116, 1), true),
		
		new PlatformBridge(Island.LOWER_10, Island.LOWER_1, Platform.FLOAT, new Tile(3997, 6088, 1), false),
		new PlatformBridge(Island.LOWER_10, Island.LOWER_13, Platform.ICE, new Tile(4008, 6087, 1), false),
		new PlatformBridge(Island.LOWER_10, Island.LOWER_16, Platform.FLOAT, new Tile(3999, 6068, 1), false),
		new PlatformBridge(Island.LOWER_10, Island.LOWER_11, Platform.EARTH, new Tile(3997, 6073, 1), false),
		
		new PlatformBridge(Island.LOWER_11, Island.LOWER_10, Platform.EARTH, new Tile(3991, 6073, 1), true),
		new PlatformBridge(Island.LOWER_11, Island.LOWER_12, Platform.ICE, new Tile(3986, 6068, 1), false),
		
		new PlatformBridge(Island.LOWER_12, Island.LOWER_11, Platform.ICE, new Tile(3986, 6062, 1), true),
		
		new PlatformBridge(Island.LOWER_13, Island.LOWER_8, Platform.EARTH, new Tile(4019, 6093, 1), false),
		new PlatformBridge(Island.LOWER_13, Island.LOWER_14, Platform.SMALL_MISSILE, new Tile(4022, 6079, 1), false),
		new PlatformBridge(Island.LOWER_13, Island.LOWER_10, Platform.ICE, new Tile(4014, 6087, 1), true),
		
		new PlatformBridge(Island.LOWER_14, Island.LOWER_13, Platform.SMALL_MISSILE, new Tile(4022, 6073, 1), false),
		new PlatformBridge(Island.LOWER_14, Island.LOWER_15, Platform.EARTH, new Tile(4021, 6066, 1), true),
		
		new PlatformBridge(Island.LOWER_15, Island.LOWER_14, Platform.EARTH, new Tile(4021, 6060, 1), false),
		new PlatformBridge(Island.LOWER_15, Island.LOWER_19, Platform.EARTH, new Tile(4021, 6053, 1), false),
		new PlatformBridge(Island.LOWER_15, Island.LOWER_16, Platform.FLOAT, new Tile(4016, 6058, 1), true),
		
		new PlatformBridge(Island.LOWER_16, Island.LOWER_10, Platform.FLOAT, new Tile(3999, 6062, 1), false),
		new PlatformBridge(Island.LOWER_16, Island.LOWER_15, Platform.FLOAT, new Tile(4010, 6058, 1), false),
		new PlatformBridge(Island.LOWER_16, Island.LOWER_17, Platform.FLOAT, new Tile(4004, 6043, 1), false),
		new PlatformBridge(Island.LOWER_16, Island.LOWER_26, Platform.EARTH, new Tile(3990, 6053, 1), false),
		
		new PlatformBridge(Island.LOWER_17, Island.LOWER_22, Platform.EARTH, new Tile(3993, 6036, 1), false),
		new PlatformBridge(Island.LOWER_17, Island.LOWER_16, Platform.FLOAT, new Tile(3999, 6038, 1), true),
		new PlatformBridge(Island.LOWER_17, Island.LOWER_18, Platform.SMALL_MISSILE, new Tile(4003, 6029, 1), false),
		new PlatformBridge(Island.LOWER_17, Island.LOWER_21, Platform.ICE, new Tile(4002, 6025, 1), false),
		new PlatformBridge(Island.LOWER_17, Island.LOWER_23, Platform.ICE, new Tile(3994, 6029, 1), false),
		
		new PlatformBridge(Island.LOWER_18, Island.LOWER_19, Platform.SMALL_MISSILE, new Tile(4014, 6031, 1), true),
		new PlatformBridge(Island.LOWER_18, Island.LOWER_17, Platform.SMALL_MISSILE, new Tile(4009, 6029, 1), false),
		
		new PlatformBridge(Island.LOWER_19, Island.LOWER_15, Platform.EARTH, new Tile(4021, 6047, 1), true),
		new PlatformBridge(Island.LOWER_19, Island.LOWER_20, Platform.ICE, new Tile(4024, 6033, 1), false),
		new PlatformBridge(Island.LOWER_19, Island.LOWER_18, Platform.SMALL_MISSILE, new Tile(4019, 6036, 1), false),
		
		new PlatformBridge(Island.LOWER_20, Island.LOWER_19, Platform.ICE, new Tile(4024, 6027, 1), true),
		
		new PlatformBridge(Island.LOWER_21, Island.LOWER_17, Platform.ICE, new Tile(4007, 6020, 1), true),
		
		new PlatformBridge(Island.LOWER_22, Island.LOWER_17, Platform.EARTH, new Tile(3988, 6041, 1), true),
		
		new PlatformBridge(Island.LOWER_23, Island.LOWER_24, Platform.SMALL_MISSILE, new Tile(3980, 6026, 1), false),
		new PlatformBridge(Island.LOWER_23, Island.LOWER_17, Platform.ICE, new Tile(3988, 6029, 1), true),
		
		new PlatformBridge(Island.LOWER_24, Island.LOWER_25, Platform.ICE, new Tile(3966, 6026, 1), true),
		new PlatformBridge(Island.LOWER_24, Island.LOWER_23, Platform.SMALL_MISSILE, new Tile(3974, 6026, 1), false),
		
		new PlatformBridge(Island.LOWER_25, Island.LOWER_27, Platform.EARTH, new Tile(3958, 6039, 1), true),
		new PlatformBridge(Island.LOWER_25, Island.LOWER_26, Platform.EARTH, new Tile(3968, 6042, 1), true),
		new PlatformBridge(Island.LOWER_25, Island.LOWER_24, Platform.ICE, new Tile(3966, 6032, 1), false),
		
		new PlatformBridge(Island.LOWER_26, Island.LOWER_35, Platform.FLOAT, new Tile(3959, 6059, 1), true),
		new PlatformBridge(Island.LOWER_26, Island.LOWER_25, Platform.EARTH, new Tile(3973, 6047, 1), false),
		
		new PlatformBridge(Island.LOWER_27, Island.LOWER_30, Platform.SMALL_MISSILE, new Tile(3937, 6039, 1), false),
		new PlatformBridge(Island.LOWER_27, Island.LOWER_35, Platform.EARTH, new Tile(3943, 6047, 1), true),
		new PlatformBridge(Island.LOWER_27, Island.LOWER_25, Platform.EARTH, new Tile(3952, 6039, 1), false),
		new PlatformBridge(Island.LOWER_27, Island.LOWER_28, Platform.ICE, new Tile(3947, 6032, 1), false),
		new PlatformBridge(Island.LOWER_27, Island.LOWER_29, Platform.EARTH, new Tile(3940, 6033, 1), false),
		
		new PlatformBridge(Island.LOWER_28, Island.LOWER_27, Platform.ICE, new Tile(3947, 6026, 1), true),
		
		new PlatformBridge(Island.LOWER_29, Island.LOWER_27, Platform.EARTH, new Tile(3935, 6028, 1), true),
		
		new PlatformBridge(Island.LOWER_30, Island.LOWER_32, Platform.SMALL_MISSILE, new Tile(3918, 6035, 1), false),
		new PlatformBridge(Island.LOWER_30, Island.LOWER_33, Platform.EARTH, new Tile(3923, 6047, 1), false),
		new PlatformBridge(Island.LOWER_30, Island.LOWER_27, Platform.SMALL_MISSILE, new Tile(3931, 6039, 1), false),
		new PlatformBridge(Island.LOWER_30, Island.LOWER_31, Platform.ICE, new Tile(3922, 6032, 1), false),
		
		new PlatformBridge(Island.LOWER_31, Island.LOWER_30, Platform.ICE, new Tile(3922, 6026, 1), true),
		
		new PlatformBridge(Island.LOWER_32, Island.LOWER_30, Platform.SMALL_MISSILE, new Tile(3912, 6035, 1), true),
		
		new PlatformBridge(Island.LOWER_33, Island.LOWER_34, Platform.ICE, new Tile(3921, 6057, 1), false),
		new PlatformBridge(Island.LOWER_33, Island.LOWER_36, Platform.EARTH, new Tile(3924, 6063, 1), false),
		new PlatformBridge(Island.LOWER_33, Island.LOWER_35, Platform.FLOAT, new Tile(3929, 6061, 1), true),
		new PlatformBridge(Island.LOWER_33, Island.LOWER_30, Platform.EARTH, new Tile(3928, 6052, 1), true),
		
		new PlatformBridge(Island.LOWER_34, Island.LOWER_33, Platform.ICE, new Tile(3915, 6057, 1), true),
		
		new PlatformBridge(Island.LOWER_35, Island.LOWER_46, Platform.FLOAT, new Tile(3937, 6086, 1), false),
		new PlatformBridge(Island.LOWER_35, Island.LOWER_52, Platform.ICE, new Tile(3948, 6087, 1), false),
		new PlatformBridge(Island.LOWER_35, Island.LOWER_26, Platform.FLOAT, new Tile(3954, 6064, 1), false),
		new PlatformBridge(Island.LOWER_35, Island.LOWER_27, Platform.EARTH, new Tile(3943, 6053, 1), false),
		new PlatformBridge(Island.LOWER_35, Island.LOWER_33, Platform.FLOAT, new Tile(3935, 6061, 1), false),
		new PlatformBridge(Island.LOWER_35, Island.LOWER_36, Platform.FLOAT, new Tile(3933, 6075, 1), false),
		
		new PlatformBridge(Island.LOWER_36, Island.LOWER_37, Platform.SMALL_MISSILE, new Tile(3915, 6072, 1), false),
		new PlatformBridge(Island.LOWER_36, Island.LOWER_35, Platform.FLOAT, new Tile(3927, 6075, 1), true),
		new PlatformBridge(Island.LOWER_36, Island.LOWER_33, Platform.EARTH, new Tile(3919, 6068, 1), true),
		
		new PlatformBridge(Island.LOWER_37, Island.LOWER_38, Platform.ICE, new Tile(3916, 6085, 1), true),
		new PlatformBridge(Island.LOWER_37, Island.LOWER_36, Platform.SMALL_MISSILE, new Tile(3915, 6078, 1), false),
		
		new PlatformBridge(Island.LOWER_38, Island.LOWER_39, Platform.ICE, new Tile(3916, 6100, 1), false),
		new PlatformBridge(Island.LOWER_38, Island.LOWER_42, Platform.EARTH, new Tile(3921, 6101, 1), false),
		new PlatformBridge(Island.LOWER_38, Island.LOWER_46, Platform.EARTH, new Tile(3925, 6098, 1), true),
		new PlatformBridge(Island.LOWER_38, Island.LOWER_37, Platform.ICE, new Tile(3921, 6090, 1), false),
		
		new PlatformBridge(Island.LOWER_39, Island.LOWER_40, Platform.EARTH, new Tile(3909, 6110, 1), false),
		new PlatformBridge(Island.LOWER_39, Island.LOWER_38, Platform.ICE, new Tile(3911, 6105, 1), true),
		
		new PlatformBridge(Island.LOWER_40, Island.LOWER_41, Platform.SMALL_MISSILE, new Tile(3911, 6126, 1), false),
		new PlatformBridge(Island.LOWER_40, Island.LOWER_42, Platform.ICE, new Tile(3914, 6119, 1), true),
		new PlatformBridge(Island.LOWER_40, Island.LOWER_39, Platform.EARTH, new Tile(3909, 6116, 1), true),
		
		new PlatformBridge(Island.LOWER_41, Island.LOWER_40, Platform.SMALL_MISSILE, new Tile(3911, 6132, 1), true),
		
		new PlatformBridge(Island.LOWER_42, Island.LOWER_40, Platform.ICE, new Tile(3919, 6114, 1), false),
		new PlatformBridge(Island.LOWER_42, Island.LOWER_43, Platform.FLOAT, new Tile(3925, 6114, 1), true),
		new PlatformBridge(Island.LOWER_42, Island.LOWER_38, Platform.EARTH, new Tile(3921, 6107, 1), true),
		
		new PlatformBridge(Island.LOWER_43, Island.LOWER_44, Platform.ICE, new Tile(3927, 6127, 1), false),
		new PlatformBridge(Island.LOWER_43, Island.LOWER_45, Platform.SMALL_MISSILE, new Tile(3932, 6130, 1), false),
		new PlatformBridge(Island.LOWER_43, Island.LOWER_47, Platform.EARTH, new Tile(3940, 6122, 1), false),
		new PlatformBridge(Island.LOWER_43, Island.LOWER_46, Platform.SMALL_MISSILE, new Tile(3937, 6112, 1), false),
		new PlatformBridge(Island.LOWER_43, Island.LOWER_42, Platform.FLOAT, new Tile(3930, 6119, 1), false),
		
		new PlatformBridge(Island.LOWER_44, Island.LOWER_43, Platform.ICE, new Tile(3922, 6132, 1), true),
		
		new PlatformBridge(Island.LOWER_45, Island.LOWER_43, Platform.SMALL_MISSILE, new Tile(3932, 6136, 1), true),
		
		new PlatformBridge(Island.LOWER_46, Island.LOWER_38, Platform.EARTH, new Tile(3931, 6098, 1), false),
		new PlatformBridge(Island.LOWER_46, Island.LOWER_43, Platform.SMALL_MISSILE, new Tile(3942, 6107, 1), false),
		new PlatformBridge(Island.LOWER_46, Island.LOWER_51, Platform.FLOAT, new Tile(3948, 6104, 1), true),
		new PlatformBridge(Island.LOWER_46, Island.LOWER_35, Platform.FLOAT, new Tile(3937, 6092, 1), true),
		
		new PlatformBridge(Island.LOWER_47, Island.LOWER_43, Platform.EARTH, new Tile(3946, 6122, 1), true),
		new PlatformBridge(Island.LOWER_47, Island.LOWER_48, Platform.ICE, new Tile(3950, 6133, 1), false),
		new PlatformBridge(Island.LOWER_47, Island.LOWER_50, Platform.SMALL_MISSILE, new Tile(3957, 6126, 1), true),
		
		new PlatformBridge(Island.LOWER_48, Island.LOWER_47, Platform.ICE, new Tile(3955, 6138, 1), true),
		
		new PlatformBridge(Island.LOWER_49, Island.LOWER_50, Platform.SMALL_MISSILE, new Tile(3978, 6137, 1), true),
		
		new PlatformBridge(Island.LOWER_50, Island.LOWER_47, Platform.SMALL_MISSILE, new Tile(3963, 6126, 1), false),
		new PlatformBridge(Island.LOWER_50, Island.LOWER_49, Platform.SMALL_MISSILE, new Tile(3973, 6132, 1), false),
		new PlatformBridge(Island.LOWER_50, Island.LOWER_1, Platform.EARTH, new Tile(3978, 6117, 1), true),
		new PlatformBridge(Island.LOWER_50, Island.LOWER_51, Platform.FLOAT, new Tile(3971, 6118, 1), true),
		
		new PlatformBridge(Island.LOWER_51, Island.LOWER_46, Platform.FLOAT, new Tile(3954, 6104, 1), false),
		new PlatformBridge(Island.LOWER_51, Island.LOWER_50, Platform.FLOAT, new Tile(3971, 6112, 1), false),
		new PlatformBridge(Island.LOWER_51, Island.LOWER_1, Platform.FLOAT, new Tile(3982, 6103, 1), false),
		new PlatformBridge(Island.LOWER_51, Island.LOWER_52, Platform.EARTH, new Tile(3957, 6097, 1), false),
		
		new PlatformBridge(Island.LOWER_52, Island.LOWER_35, Platform.ICE, new Tile(3954, 6087, 1), true),
		new PlatformBridge(Island.LOWER_52, Island.LOWER_51, Platform.EARTH, new Tile(3957, 6091, 1), true),
	};
	
	private static final PlatformBridge[] middleF2pBridges = new PlatformBridge[] {
		new PlatformBridge(Island.MIDDLE_1, Island.MIDDLE_15, Platform.FLOAT, new Tile(4167, 6099, 1), false),
		new PlatformBridge(Island.MIDDLE_1, Island.MIDDLE_2, Platform.SMALL_MISSILE, new Tile(4167, 6092, 1), false),
		new PlatformBridge(Island.MIDDLE_1, Island.MIDDLE_5, Platform.FLOAT, new Tile(4146, 6097, 1), false),
		
		new PlatformBridge(Island.MIDDLE_2, Island.MIDDLE_1, Platform.SMALL_MISSILE, new Tile(4173, 6092, 1), true),
		new PlatformBridge(Island.MIDDLE_2, Island.MIDDLE_3, Platform.EARTH, new Tile(4180, 6086, 1), true),
		
		new PlatformBridge(Island.MIDDLE_3, Island.MIDDLE_2, Platform.EARTH, new Tile(4180, 6080, 1), false),
		new PlatformBridge(Island.MIDDLE_3, Island.MIDDLE_29, Platform.SMALL_MISSILE, new Tile(4184, 6058, 1), false),
		new PlatformBridge(Island.MIDDLE_3, Island.MIDDLE_33, Platform.ICE, new Tile(4174, 6053, 1), false),
		new PlatformBridge(Island.MIDDLE_3, Island.MIDDLE_4, Platform.SMALL_MISSILE, new Tile(4159, 6064, 1), false),
		
		new PlatformBridge(Island.MIDDLE_4, Island.MIDDLE_5, Platform.FLOAT, new Tile(4136, 6062, 1), false),
		new PlatformBridge(Island.MIDDLE_4, Island.MIDDLE_3, Platform.SMALL_MISSILE, new Tile(4153, 6064, 1), true),
		
		new PlatformBridge(Island.MIDDLE_5, Island.MIDDLE_6, Platform.SMALL_MISSILE, new Tile(4130, 6089, 1), false),
		new PlatformBridge(Island.MIDDLE_5, Island.MIDDLE_1, Platform.FLOAT, new Tile(4141, 6092, 1), false),
		new PlatformBridge(Island.MIDDLE_5, Island.MIDDLE_4, Platform.FLOAT, new Tile(4136, 6068, 1), false),
		new PlatformBridge(Island.MIDDLE_5, Island.MIDDLE_44, Platform.SMALL_MISSILE, new Tile(4130, 6071, 1), false),
		
		new PlatformBridge(Island.MIDDLE_6, Island.MIDDLE_5, Platform.SMALL_MISSILE, new Tile(4130, 6095, 1), true),
		
		new PlatformBridge(Island.MIDDLE_15, Island.MIDDLE_1, Platform.FLOAT, new Tile(4172, 6104, 1), true),
		
		new PlatformBridge(Island.MIDDLE_29, Island.MIDDLE_3, Platform.SMALL_MISSILE, new Tile(4189, 6053, 1), true),
		
		new PlatformBridge(Island.MIDDLE_33, Island.MIDDLE_3, Platform.ICE, new Tile(4174, 6047, 1), true),
		
		new PlatformBridge(Island.MIDDLE_44, Island.MIDDLE_5, Platform.SMALL_MISSILE, new Tile(4125, 6066, 1), true),
	};
	
	Island mainIsland;
	Island branchIsland;
	Platform platform;
	Tile platformTile;
	boolean free;
	
	public PlatformBridge(Island mainIsland, Island branchIsland, Platform platform, Tile platformTile, boolean free) {
		this.mainIsland = mainIsland;
		this.branchIsland = branchIsland;
		this.platform = platform;
		this.platformTile = platformTile;
		this.free = free;
	}
	
	public Island getMainIsland() {
		return mainIsland;
	}
	
	public Island getBranchIsland() {
		return branchIsland;
	}
	
	public Platform getPlatform() {
		return platform;
	}
	
	public Tile getPlatformTile() {
		return platformTile;
	}
	
	public static Island[] getShortestRoute(Island sourceIsland, Island destIsland) {
		FindAllPaths algo = new FindAllPaths(sourceIsland, destIsland);
		return algo.getShortestPath().toArray(new Island[0]);
	}
	
	public static PlatformBridge[] toBridges(Island[] islands) {
		List<PlatformBridge> bridges = new ArrayList<>();
		for (int i = 0; i < islands.length; i++) {
			if (i < islands.length - 1) {
				// find the PlatformBridge defining current islands
				for (PlatformBridge b : getBridges(islands[i].floorLevel)) {
					if (b.mainIsland.equals(islands[i]) && b.branchIsland.equals(islands[i + 1])) {
						bridges.add(b);
						break;
					}
				}
			}
		}
		
		return bridges.toArray(new PlatformBridge[0]);
	}
	
	public static PlatformBridge[] getBridges(FloorLevel floorLevel) {
		switch (floorLevel) {
		case HIGHER:
			break;
		case LOWER:
			return lowerBridges;
		case MIDDLE_F2P:
			return middleF2pBridges;
		case MIDDLE:
			break;
		default:
			break;
		}
		
		return null;
	}
}
