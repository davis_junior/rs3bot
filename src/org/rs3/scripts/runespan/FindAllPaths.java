package org.rs3.scripts.runespan;

import java.util.ArrayList;
import java.util.List;

public class FindAllPaths {

	Island sourceIsland;
	Island destIsland;
	Creature searchCreature;
	
	private List<List<Island>> workPaths = new ArrayList<List<Island>>();
	private List<List<Island>> resultPaths = new ArrayList<List<Island>>();
	
	public static void main(String[] args) {
		//FindAllPaths instance = new FindAllPaths(Island.LOWER_1, Island.LOWER_52);
		//FindAllPaths instance = new FindAllPaths(Island.LOWER_5, Island.LOWER_6);
		//System.out.println(instance.findAllPaths().size());
	}
	
	public FindAllPaths(Island sourceIsland, Island destIsland) {
		this.sourceIsland = sourceIsland;
		this.destIsland = destIsland;
		this.searchCreature = null;
	}
	
	public FindAllPaths(Island sourceIsland, Creature searchCreature) {
		this.sourceIsland = sourceIsland;
		this.destIsland = null;
		this.searchCreature = searchCreature;
	}
	
	public List<List<Island>> findAllPaths() {
		workPaths.clear();
		resultPaths.clear();
		
		// create initial path
		List<Island> path = new ArrayList<>();
		path.add(sourceIsland);
		workPaths.add(path);
		
		addNextBranchPaths();
		
		return resultPaths;
	}
	
	public List<Island> getShortestPath() {
		List<List<Island>> paths = findAllPaths();
		List<Island> shortest = paths.get(0);
		for (List<Island> path : paths) {
			if (path.size() < shortest.size())
				shortest = path;
		}
		
		return shortest;
	}
	
	// TODO: breaks on first path found, which is usually shortest; method is too slow to find all paths
	// recursive -- makes new paths for every new branch
	private void addNextBranchPaths() {
		boolean addedNode = false;
		List<List<Island>> toAdd = new ArrayList<List<Island>>();
		List<List<Island>> toRemove = new ArrayList<List<Island>>();
		
		for (List<Island> path : workPaths) {
			// continue if path length (distance) is over 10 nodes - this should be the longest distance necessary to travel
			if (path.size() > 10) {
				toRemove.add(path);
				continue;
			}
			
			// get last node
			Island last = path.get(path.size() - 1);
			
			// continue if last is destination node
			if (destIsland != null) {
				if (last.equals(destIsland)) {
					resultPaths.add(path);
					toRemove.add(path);
					//continue; // TODO
					return;
				}
			} else if (searchCreature != null) {
				for (Creature creature : last.getCreatures()) {
					if (creature.equals(searchCreature)) {
						resultPaths.add(path);
						toRemove.add(path);
						//continue; // TODO
						return;
					}
				}
			}
			
			Island[] branches = last.getBranchIslands();
			
			for (int i = 0; i < branches.length; i++) {
				Island island = branches[i];
				
				// skip previously traveled nodes
				if (!path.contains(island)) {
					// add current node to current path if on last iteration, otherwise create a new path
					if (i == branches.length - 1) {
						path.add(island);
						addedNode = true;
					} else {
						List<Island> newPath = new ArrayList<>(path);
						newPath.add(island);
						toAdd.add(newPath);
						addedNode = true;
					}
				} else {
					// if this is an edge (or node containing only one node) and is not the dest node, skip and remove
					if ((destIsland == null || !island.equals(destIsland)) && branches.length == 1) {
						toRemove.add(path);
						continue;
					}
				}
			}
		}
		
		for (List<Island> path : toRemove) {
			workPaths.remove(path);
		}
		
		if (addedNode) {
			for (List<Island> newPath : toAdd) {
				workPaths.add(newPath);
			}
			
			//System.out.println(workPaths.size());
			addNextBranchPaths();
		}
	}
}
