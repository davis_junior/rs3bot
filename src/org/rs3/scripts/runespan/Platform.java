package org.rs3.scripts.runespan;

import org.rs3.api.Skill;
import org.rs3.api.Worlds;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.WorldInfo;

/**
 * Platforms are ground objects
 * 
 * @author User
 *
 */
public enum Platform {

	FLOAT("Float platform", false, 1, new int[] { 70476, 70477, 70483 }, new Rune[] {Rune.AIR}),
	EARTH("Earth platform", false, 9, new int[] { 70478, 70479, 70485 }, new Rune[] {Rune.EARTH}),
	ICE("Ice platform", false, 15, new int[] { 70480, 70486 }, new Rune[] {Rune.AIR, Rune.WATER}),
	SMALL_MISSILE("Small missile platform", false, 20, new int[] { 70481, 70482, 70487 }, new Rune[] {Rune.MIND, Rune.ELEMENTAL}),
	CONJURATION(null, true, 25, null, new Rune[] {Rune.RUNE_ESSENCE, Rune.MIND, Rune.BODY}),
	MISSILE(null, true, 35, null, new Rune[] {Rune.CHAOS, Rune.ELEMENTAL}),
	VINE(null, true, 44, null, new Rune[] {Rune.WATER, Rune.EARTH, Rune.NATURE}),
	MIST(null, true, 50, null, new Rune[] {Rune.WATER, Rune.BODY, Rune.NATURE}),
	COMET(null, true, 55, null, new Rune[] {Rune.COSMIC, Rune.ASTRAL, Rune.LAW}),
	SKELETAL(null, true, 66, null, new Rune[] {Rune.DEATH}),
	GREATER_MISSILE(null, true, 77, null, new Rune[] {Rune.DEATH, Rune.BLOOD, Rune.ELEMENTAL}),
	FLESH(null, true, 85, null, new Rune[] {Rune.BODY, Rune.DEATH, Rune.BLOOD}),
	GREATER_CONJURATION(null, true, 95, null, new Rune[] {Rune.RUNE_ESSENCE, Rune.MIND, Rune.BODY, Rune.DEATH, Rune.BLOOD, Rune.SOUL}),
	;
	
	String name;
	boolean members;
	int levelReq;
	int[] ids;
	Rune[] runesReq;
	
	private Platform(String name, boolean members, int levelReq, int[] ids, Rune[] runesReq) {
		this.name = name;
		this.members = members;
		this.levelReq = levelReq;
		this.ids = ids;
		this.runesReq = runesReq;
	}
	
	public String getName() {
		return name;
	}
	
	public int[] getIds() {
		return ids;
	}
	
	public Rune[] getRunesReq() {
		return runesReq;
	}
	
	public boolean isInteractable() {
		if (members) {
			WorldInfo worldInfo = Client.getWorldInfo();
			if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
				return false;
		}
		
		if (Skill.RUNECRAFTING.getCurrentLevel() < levelReq)
			return false;
		
		return true;
	}
}
