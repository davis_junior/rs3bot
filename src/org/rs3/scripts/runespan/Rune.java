package org.rs3.scripts.runespan;

import org.rs3.api.ItemTable;
import org.rs3.api.Skill;
import org.rs3.api.Worlds;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.WorldInfo;

public enum Rune {

	RUNE_ESSENCE("Rune essence", false, 24227),
	AIR("Air rune", false, 24215),
	MIND("Mind rune", false, 24217),
	WATER("Water rune", false, 24214),
	EARTH("Earth rune", false, 24216),
	FIRE("Fire rune", false, 24213),
	BODY("Body rune", false, 24218),
	COSMIC(null, true, -1),
	CHAOS(null, true, -1),
	ASTRAL(null, true, -1),
	NATURE(null, true, -1),
	LAW(null, true, -1),
	DEATH(null, true, -1),
	BLOOD(null, true, -1),
	SOUL(null, true, -1),
	
	ELEMENTAL(null, false, -1),
	;
	
	String name;
	boolean members;
	int id;
	
	private Rune(String name, boolean members, int id) {
		this.name = name;
		this.members = members;
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public int count() {
		if (this.equals(ELEMENTAL)) {
			return ItemTable.INVENTORY.countItems(true, AIR.id)
					+ ItemTable.INVENTORY.countItems(true, WATER.id)
					+ ItemTable.INVENTORY.countItems(true, EARTH.id)
					+ ItemTable.INVENTORY.countItems(true, FIRE.id);
		}
		
		return ItemTable.INVENTORY.countItems(true, id);
	}
	
	public boolean isObtainable() {
		if (members) {
			WorldInfo worldInfo = Client.getWorldInfo();
			if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
				return false;
		}
		
		return true;
	}
}
