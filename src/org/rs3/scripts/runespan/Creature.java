package org.rs3.scripts.runespan;

import java.util.ArrayList;
import java.util.List;

import org.rs3.api.Skill;
import org.rs3.api.Worlds;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.WorldInfo;
import org.rs3.util.ArrayUtils;

/**
 * Creatures are Npcs
 * 
 * @author User
 *
 */
public enum Creature {

	AIR_ESSLING("Air essling", false, 1, 9.5, 15403, 16571, Rune.AIR, Rune.AIR),
	MIND_ESSLING("Mind essling", false, 1, 10.0, 15404, 16571, Rune.MIND, Rune.AIR),
	WATER_ESSLING("Water essling", false, 5, 12.6, 15405, 16571, Rune.WATER, Rune.AIR),
	EARTH_ESSLING("Earth essling", false, 9, 14.3, 15406, 16571, Rune.EARTH, Rune.WATER),
	FIRE_ESSLING("Fire essling", false, 14, 17.4, 15407, 16571, Rune.FIRE, Rune.WATER),
	BODY_ESSHOUND("Body esshound", false, 20, 23.1, 15408, 16661, Rune.BODY, Rune.MIND),
	COSMIC_ESSHOUND(null, true, 27, 26.6, -1, -1, Rune.COSMIC, Rune.EARTH),
	CHAOS_ESSHOUND(null, true, 35, 30.8, -1, -1, Rune.CHAOS, Rune.FIRE),
	ASTRAL_ESSHOUND(null, true, 40, 35.7, -1, -1, Rune.ASTRAL, Rune.COSMIC),
	NATURE_ESSHOUND(null, true, 44, 43.4, -1, -1, Rune.NATURE, Rune.CHAOS),
	LAW_ESSHOUND(null, true, 54, 53.9, -1, -1, Rune.LAW, Rune.ASTRAL),
	DEATH_ESSWRAITH(null, true, 65, 60.0, -1, -1, Rune.DEATH, Rune.NATURE),
	BLOOD_ESSWRAITH(null, true, 77, 73.1, -1, -1, Rune.BLOOD, Rune.LAW),
	SOUL_ESSWRAITH(null, true, 90, 106.5, -1, -1, Rune.ASTRAL, Rune.BLOOD),
	;
	
	String name;
	boolean members;
	int levelReq;
	double xp;
	int id;
	int deathAnimationId;
	Rune rune;
	Rune chippingRune;
	
	private Creature(String name, boolean members, int levelReq, double xp, int id, int deathAnimationId, Rune rune, Rune chippingRune) {
		this.name = name;
		this.members = members;
		this.levelReq = levelReq;
		this.xp = xp;
		this.id = id;
		this.deathAnimationId = deathAnimationId;
		this.rune = rune;
		this.chippingRune = chippingRune;
	}
	
	public int getLevelReq() {
		return levelReq;
	}
	
	public double getXp() {
		return xp;
	}
	
	public int getId() {
		return id;
	}
	
	public int getDeathAnimationId() {
		return deathAnimationId;
	}
	
	public Rune getRune() {
		return rune;
	}
	
	public Rune getChippingRune() {
		return chippingRune;
	}
	
	public boolean isInteractable() {
		if (members) {
			WorldInfo worldInfo = Client.getWorldInfo();
			if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
				return false;
		}
		
		if (Skill.RUNECRAFTING.getCurrentLevel() < levelReq)
			return false;
		
		return true;
	}
	
	
	public static int[] getAllInteractableIds() {
		List<Integer> ids = new ArrayList<>();
		for (Creature creature : values()) {
			if (creature.isInteractable())
				ids.add(creature.id);
		}
		
		if (ids.size() == 0)
			return null;
		
		return ArrayUtils.toPrimitiveIntArray(ids.toArray(new Integer[0]));
	}
}
