package org.rs3.scripts.runespan;

import org.rs3.api.Skill;
import org.rs3.api.Worlds;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.WorldInfo;

/**
 * Nodes are ground objects
 * 
 * @author User
 *
 */
public enum Node {

	CYCLONE("Cyclone", false, 1, 19.0, 70455, new Rune[] {Rune.AIR}),
	MIND_STORM("Mind storm", false, 1, 20.0, 70456, new Rune[] {Rune.MIND}),
	WATER_POOL("Water pool", false, 5, 25.3, 70457, new Rune[] {Rune.WATER}),
	ROCK_FRAGMENT("Rock fragment", false, 9, 28.6, 70458, new Rune[] {Rune.EARTH}),
	FIREBALL("Fireball", false, 14, 34.8, 70459, new Rune[] {Rune.FIRE}),
	VINE("Vine", false, 17, 32.3, 70460, new Rune[] {Rune.WATER, Rune.EARTH}),
	FLESHY_GROWTH("Fleshy growth", false, 20, 46.2, 70461, new Rune[] {Rune.BODY}),
	FIRE_STORM("Fire storm", false, 27, 32.25, 70462, new Rune[] {Rune.AIR, Rune.FIRE}),
	CHAOTIC_CLOUD(null, true, 35, 61.6, -1, new Rune[] {Rune.CHAOS}),
	NEBULA(null, true, 40, 74.7, -1, new Rune[] {Rune.COSMIC, Rune.ASTRAL}),
	SHIFTER(null, true, 44, 86.8, -1, new Rune[] {Rune.NATURE}),
	JUMPER(null, true, 54, 107.8, -1, new Rune[] {Rune.LAW}),
	SKULLS(null, true, 65, 120.0, -1, new Rune[] {Rune.DEATH}),
	BLOOD_POOL(null, true, 77, 146.3, -1, new Rune[] {Rune.BLOOD}),
	BLOODY_SKULLS(null, true, 83, 159.75, -1, new Rune[] {Rune.DEATH, Rune.BLOOD}),
	LIVING_SOUL(null, true, 90, 213.0, -1, new Rune[] {Rune.SOUL}),
	UNDEAD_SOUL(null, true, 95, 199.75, -1, new Rune[] {Rune.DEATH, Rune.SOUL}),
	;
	
	String name;
	boolean members;
	int levelReq;
	double xp;
	int id;
	Rune[] runes;
	
	private Node(String name, boolean members, int levelReq, double xp, int id, Rune[] runes) {
		this.name = name;
		this.members = members;
		this.levelReq = levelReq;
		this.xp = xp;
		this.id = id;
		this.runes = runes;
	}
	
	public int getLevelReq() {
		return levelReq;
	}
	
	public double getXp() {
		return xp;
	}
	
	public int getId() {
		return id;
	}
	
	public Rune[] getRunes() {
		return runes;
	}
	
	public boolean isInteractable() {
		if (members) {
			WorldInfo worldInfo = Client.getWorldInfo();
			if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
				return false;
		}
		
		if (Skill.RUNECRAFTING.getCurrentLevel() < levelReq)
			return false;
		
		return true;
	}
}
