package org.rs3.scripts.runespan;

import org.rs3.api.Players;
import org.rs3.api.Worlds;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.Tile;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.WorldInfo;

public enum FloorLevel {

	LOWER(false, new Area( new Tile(3900, 6145, 1), new Tile(4033, 6012, 1) ), new Creature[] { Creature.AIR_ESSLING, Creature.MIND_ESSLING, Creature.WATER_ESSLING, Creature.EARTH_ESSLING, Creature.FIRE_ESSLING }),
	MIDDLE_F2P(false, new Area( new Tile(4110, 6121, 1), new Tile(4206, 6030, 1) ), new Creature[] { Creature.AIR_ESSLING, Creature.MIND_ESSLING, Creature.WATER_ESSLING, Creature.EARTH_ESSLING, Creature.BODY_ESSHOUND }),
	MIDDLE(true, null, null),
	HIGHER(true, null, null),
	;
	
	boolean members;
	Area area;
	Creature[] creatures;
	
	private FloorLevel(boolean members, Area area, Creature[] creatures) {
		this.members = members;
		this.area = area;
		this.creatures = creatures;
	}
	
	public Area getArea() {
		return area;
	}
	
	public Creature[] getCreatures() {
		return creatures;
	}
	
	public boolean isCurrent() {
		boolean onMembersWorld = true;
		
		WorldInfo worldInfo = Client.getWorldInfo();
		if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
			onMembersWorld = false;
		
		if (members && !onMembersWorld)
			return false;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		boolean contains = area.contains(player.getTile());
		if (!contains)
			return false;
		
		if (this.equals(MIDDLE_F2P))
			return !onMembersWorld;
		else if (this.equals(MIDDLE))
			return onMembersWorld;
		else
			return true;
	}
	
	public static FloorLevel getCurrent() {
		for (FloorLevel level : values()) {
			if (level.isCurrent())
				return level;
		}
		
		return null;
	}
}
