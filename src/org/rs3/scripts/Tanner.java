package org.rs3.scripts;

import java.awt.Checkbox;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.Constants;
import org.rs3.api.GESlot;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Npcs;
import org.rs3.api.Paths;
import org.rs3.api.Players;
import org.rs3.api.Skill;
import org.rs3.api.Walking;
import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.Worlds;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.exchange.ExchangeItem;
import org.rs3.api.exchange.ExchangeLoop;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.CollectionBox;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.ItemSelectInfoWidget;
import org.rs3.api.interfaces.widgets.ItemSelectWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.api.wrappers.WorldInfo;
import org.rs3.callbacks.ItemTableCallback;
import org.rs3.callbacks.ItemTableListener;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.Database;
import org.rs3.database.GrandExchangeRow;
import org.rs3.debug.Paint;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.FailsafesParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.tanning.HideType;
import org.rs3.scripts.woodcutting.LogType;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Tanner extends Script {
	
	private static boolean exchange = false;
	private static Timer scriptStartExchangeTimer = null; // only used at the start of script for initial exchange if near GE when it starts
	private static ExchangeLoop exchangeLoop = new ExchangeLoop(null, new ExchangeItem[] {
			new ExchangeItem(HideType.GREEN_DRAGONHIDE.getLeatherId(), HideType.GREEN_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(HideType.BLUE_DRAGONHIDE.getLeatherId(), HideType.BLUE_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(HideType.RED_DRAGONHIDE.getLeatherId(), HideType.RED_DRAGONHIDE.getLeatherName(), true),
			new ExchangeItem(HideType.BLACK_DRAGONHIDE.getLeatherId(), HideType.BLACK_DRAGONHIDE.getLeatherName(), true),
	});
	
	private int greenLeatherAccumulated;
	private int blueLeatherAccumulated;
	private int redLeatherAccumulated;
	private int blackLeatherAccumulated;
	
	private ItemTableListener itemTableListener = new ItemTableListener() {
		
		@Override
		public void replace(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			//System.out.println("replace[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize + " with old = " + originalItemId + ", " + originalItemStackSize);
			
			if (itemTableId == ItemTable.INVENTORY.getId()) {
				if (itemId == HideType.GREEN_DRAGONHIDE.getLeatherId())
					greenLeatherAccumulated++;
				else if (itemId == HideType.BLUE_DRAGONHIDE.getLeatherId())
					blueLeatherAccumulated++;
				else if (itemId == HideType.RED_DRAGONHIDE.getLeatherId())
					redLeatherAccumulated++;
				else if (itemId == HideType.BLACK_DRAGONHIDE.getLeatherId())
					blackLeatherAccumulated++;
			}
		}
		
		@Override
		public void remove(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("remove[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			
		}
		
		@Override
		public void increase(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("increase[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void decrease(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("decrease[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void add(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("add[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
	};
	
	private enum Action {
		
		BANK, 
		TAN, 
		WALK_TO_AL_KHARID, 
		WALK_TO_GE,
		EXCHANGE,
		;
		
		public static Action getNextAction() {
			if (!exchange) {
				if (tanningBuilding.getDistanceToNearest() <= 25
						|| bankArea.getDistanceToNearest() <= 25) {
					
					if (ItemTable.INVENTORY.containsAny(HideType.getCurrent().getHideId())) // hide
						return TAN;
					else if (ItemTable.INVENTORY.containsAny(HideType.getCurrent().getLeatherId())) // leather
						return BANK;
					else
						return BANK;
				} else if (scriptStartExchangeTimer != null && scriptStartExchangeTimer.isRunning()) { // TODO: cancel timer after bank opened or exchanged
					Npc npc = GrandExchangeWidget.get.getNearest();
					if (npc != null) {
						exchange = true;
						if (Calculations.distanceTo(npc) <= 10)
							return EXCHANGE;
						else
							return WALK_TO_GE;
					} else
						return WALK_TO_AL_KHARID;
				} else
					return WALK_TO_AL_KHARID;
			} else { // exchange
				Npc npc = GrandExchangeWidget.get.getNearest();
				if (npc != null && Calculations.distanceTo(npc) <= 10)
					return EXCHANGE;
				else
					return WALK_TO_GE;
			}
		}
	}
	
	static final Area tanningBuilding = new Area( new Tile(3270, 3198, 0), new Tile(3276, 3193, 0) );
	static final Area bankArea = new Area( new Tile(3268, 3173, 0), new Tile(3272, 3161, 0) );
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public boolean walkToTile(Tile tile, boolean randomize) {
		if (Calculations.distanceTo(tile) < 4)
			return true;
		
		if (randomize)
			tile = tile.randomize(2, 2);
		
		Timer timer = new Timer(20000);
		while (timer.isRunning()) {
			if (Calculations.distanceTo(tile) < 4)
				return true;
			
			Tile curTile = Walking.getClosestOnMap(tile);
			Walking.walk(curTile);
			Time.sleepMedium();
			Timer curTileTimer = new Timer(10000);
			while (curTileTimer.isRunning()) {
				if (Calculations.distanceTo(curTile) < 7)
					break;
				
				Time.sleep(50);
				
				if (Antiban.doAntiban())
					Time.sleepQuickest();
			}
			
			Time.sleep(50);
			
			if (Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		return false;
	}
	
	public boolean openTanningScreen() {
		
		if (ItemSelectWidget.get.isOpen())
			return true;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		if (!tanningBuilding.contains(player.getTile()))
			if (!tanningBuilding.loopWalkTo(20000))
				return false;
		
		Npc ellis = Npcs.getNearest(null, 2824);
		if (ellis != null && ellis.loopInteract(null, "Tan hides")) {
			Time.sleepQuick();
			
			Timer timer = new Timer(5000);
			while (timer.isRunning()) {
				if (ItemSelectWidget.get.isOpen())
					return true;
				
				Time.sleep(50);
			}
		}
		
		return false;
	}
	
	public boolean tan() {
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		Tab.closeAll();
		
		if (ItemSelectWidget.get.isOpen() && ItemSelectInfoWidget.get.isOpen()) {
			if (ItemSelectInfoWidget.get.getSelectedItemId() != HideType.getCurrent().getLeatherId()) { // leather
				if (!ItemSelectWidget.get.selectItem(HideType.getCurrent().getLeatherId(), true)) // leather
					return false;
			}
			
			if (ItemSelectInfoWidget.get.getSelectedItemAmount() != ItemTable.INVENTORY.countItems(false, HideType.getCurrent().getHideId())) { // hide
				Timer timer = new Timer(10000);
				while (timer.isRunning()) {
					if (ItemSelectInfoWidget.get.getSelectedItemAmount() == ItemTable.INVENTORY.countItems(false, HideType.getCurrent().getHideId()))
						break;
					
					ItemSelectWidget.get.increaseAmount(true);
					
					Time.sleep(50);
				}
			}
			
			if (ItemSelectInfoWidget.get.clickDoActionButton(true)) {
				Timer timer = new Timer(5000);
				while (timer.isRunning()) {
					if (ItemSelectInfoWidget.get.isClosed())
						return true;
					
					Time.sleep(50);
				}
			}
		}
		
		return false;
	}
	
	boolean checkedCollectionBox = false;
	
	public boolean bank() {
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		if (!bankArea.contains(player.getTile())) {
			if (!bankArea.loopWalkTo(20000))
				return false;
		}
		
		if (Bank.get.open(true)) {
			if (Bank.get.depositAllInvItems()) {
				Time.sleepQuick();
				
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					Bank.get.withdrawAll(995, false); // coins
					Time.sleepQuick();
				}
				
				final int hideStackSize = ItemTable.BANK.getStackSize(HideType.getCurrent().getHideId());
				if (hideStackSize <= 1) {
					/*System.out.println("Out of hides. Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Out of hides. Checking colleciton box and going to Grand Exchange...");
					
					if (Bank.get.close(true)) {
						Time.sleepMedium();
						
						if (!checkedCollectionBox && CollectionBox.get.open(true)) {
							Time.sleepMedium();
							
							int hideId = HideType.getCurrent().getHideId();
							if (ItemTable.GESLOT1.containsAny(hideId) 
									|| ItemTable.GESLOT2.containsAny(hideId)
									|| ItemTable.GESLOT3.containsAny(hideId)
									|| ItemTable.GESLOT4.containsAny(hideId)
									|| ItemTable.GESLOT5.containsAny(hideId)
									|| ItemTable.GESLOT6.containsAny(hideId)) {
								
								if (CollectionBox.get.collectAllToBank())
									checkedCollectionBox = true;
								Time.sleepQuick();
								CollectionBox.get.close();
								return false;
							} else {
								if (CollectionBox.get.collectAllToBank())
									checkedCollectionBox = true;
								Time.sleepQuick();
								CollectionBox.get.close();
							}
						}
					}
					
					
					if (checkedCollectionBox) {
						exchange = true;
						return true;
					}
				}
				
				if (ItemTable.MONEYPOUCH.getCoinCount() / 20 < Math.min(hideStackSize, 28)) {
					System.out.println("Out of coins. Stopping script...");
					interruptStop();
					return false;
				}
				
				if (Bank.get.withdraw(HideType.getCurrent().getHideId(), -1, false)) { // hide
					Time.sleepQuick();
					
					Bank.get.close();
					Time.sleepVeryQuick();
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		failsafeWalkedToMine = new Failsafe("Walked to Mine", 600000); // 10 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 600000); // 10 mins.
		failsafeMinedRock = new Failsafe("Mine", 600000); // 10 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 600000); // 10 mins.
		//failsafes.add(failsafeWalkedToMine);
		//failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		breakHandlerParam = new BreakHandlerParam(failsafes);
		params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		boolean success = false;
		if (AccountInfo.cur != null) {
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Map<String, String> params = row.getScriptParams_toMap();
				String hideType = params.get("HideType");
				if (hideType != null) {
					success = true;
					
					hideType = hideType.toLowerCase();
					if (hideType.contains("green"))
						HideType.setCurrent(HideType.GREEN_DRAGONHIDE);
					else if (hideType.contains("blue"))
						HideType.setCurrent(HideType.BLUE_DRAGONHIDE);
					else if (hideType.contains("red"))
						HideType.setCurrent(HideType.RED_DRAGONHIDE);
					else if (hideType.contains("black"))
						HideType.setCurrent(HideType.BLACK_DRAGONHIDE);
					else
						success = false;
				}
			}
		}
		
		if (!success) {
			System.out.println("Tanner: Error loading params! Loaded default data params.");
			HideType.setCurrent(HideType.GREEN_DRAGONHIDE);
		} else {
			System.out.println("Tanner: Successfully loaded data params: ");
		}
		
		System.out.println("\t-HideType: " + HideType.getCurrent().name());
		
		exchangeLoop.addExchangeItem(new ExchangeItem(HideType.getCurrent().getHideId(), HideType.getCurrent().getHideName(), false, 10000, 20));
	}
	
	public int getLeathersPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int totalLeathers = greenLeatherAccumulated + blueLeatherAccumulated + redLeatherAccumulated + blackLeatherAccumulated;
			int leathersPerHour = (int) (totalLeathers / timeDiffHours);
			return leathersPerHour;
		}
		
		return 0;
	}
	
	public int getGpPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int greenGp = greenLeatherAccumulated*167;
			int blueGp = blueLeatherAccumulated*101;
			int redGp = redLeatherAccumulated*105;
			int blackGp = blackLeatherAccumulated*172;
			int totalGp = greenGp + blueGp + redGp + blackGp;
			int gpPerHour = (int) (totalGp / timeDiffHours);
			return gpPerHour;
		}
		
		return 0;
	}
	
	@Override
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("-Tanner-");
		sb.append(System.lineSeparator());
		
		if (greenLeatherAccumulated > 0) {
			sb.append("Green hides tanned: " + greenLeatherAccumulated);
			sb.append(System.lineSeparator());
		}
		if (blueLeatherAccumulated > 0) {
			sb.append("Blue hides tanned: " + blueLeatherAccumulated);
			sb.append(System.lineSeparator());
		}
		if (redLeatherAccumulated > 0) {
			sb.append("Red hides tanned: " + redLeatherAccumulated);
			sb.append(System.lineSeparator());
		}
		if (blackLeatherAccumulated > 0) {
			sb.append("Black hides tanned: " + blackLeatherAccumulated);
			sb.append(System.lineSeparator());
		}
		
		sb.append("Leathers/hr: " + getLeathersPerHour());
		sb.append(System.lineSeparator());
		
		sb.append("GP/hr: " + getGpPerHour() / 1000 + "k");
		sb.append(System.lineSeparator());
		
		return sb.toString();
	}
	
	@Override
	public boolean onStart() {
		
		greenLeatherAccumulated = 0;
		blueLeatherAccumulated = 0;
		redLeatherAccumulated = 0;
		blackLeatherAccumulated = 0;
		
		ItemTableCallback.addListener(itemTableListener);
		
		WorldInfo worldInfo = Client.getWorldInfo();
		if (worldInfo == null) {
			System.out.println("Unable to get world info!");
			return false;
		}
		
		if (!Worlds.isMembers(worldInfo.getWorld())) { // TODO: check if doing green leather, etc
			System.out.println("World is not members! Stopping script...");
			interruptStop();
			return false;
		}
		
		if (!Database.isConnectionValid(10)) {
			System.out.println("Unable to connect to database!");
			return false;
		}
		
		loadDataParams();
		//HideType.setCurrent(HideType.BLUE_DRAGONHIDE);
		
		Antiban.resetTimer();
		scriptStartExchangeTimer = new Timer(120000);
		
		return true;
	}
	
	
	/*
	 * Script Notes
	 * 
	 */

	@Override
	public void run() {

		Antiban.randomMouseSpeed();
		
		if (!Client.loopLogin())
			return;
		
		if (Skill.CRAFTING.getCurrentLevel() < 2) { // TODO: check if doing green leather, etc (since cowhide works at level 1)
			// double check
			Time.sleep(10000);
			
			if (Skill.CRAFTING.getCurrentLevel() < 2) {
				System.out.println("Crafting level is not at least 2! Stopping script...");
				interruptStop();
				return;
			}
		}
		
		/*if (!exchange && !exchangeLoop.getMainExchangeTimer().isRunning()) {
			exchangeLoop.setMainExchangePeriod(Random.nextInt(10800000, 18000000)); // 3 to 5 hrs
		}*/
		
		if (CollectionBox.get.isOpen()) {
			CollectionBox.get.collectAllToBank();
			Time.sleepQuick();
			CollectionBox.get.close();
			return;
		}
		
		if (!exchange) {
			if (!GrandExchangeWidget.get.close(true))
				return;
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		if (!Walking.isRunEnabled()) {
			Walking.setRun(true);
			Time.sleepQuick();
		}
		
		if (Walking.getEnergy() < 30) {
			System.out.println("Resting...");
			if (!Walking.restUntil(Random.nextInt(70, 100)))
				return;
		}
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!Lodestone.AL_KHARID.isActivated()) {
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			Lodestone.AL_KHARID.activate();
			Time.sleepMedium();
			return;
		}
		
		Action action = Action.getNextAction();
		System.out.println(action);
		
		switch (action) {
		case TAN:
			if (Bank.get.isOpen())
				Bank.get.close(true);
			
			if (openTanningScreen()) {
				Time.sleepVeryQuick();
				
				if (tan()) {
					checkedCollectionBox = false;
					failsafeMinedRock.reset();
				}
			}
			
			Time.sleepVeryQuick();
			break;
			
		case BANK:
			if (bank()) {
				failsafeDropOrBank.reset();
				failsafeWalkedToBank.reset();
			}
			
			break;
		case EXCHANGE:
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			if (exchangeLoop.exchange()) {
				scriptStartExchangeTimer = null;
				
				exchange = false;
			}
			
			break;
		case WALK_TO_AL_KHARID:
			if (Bank.get.isOpen())
				Bank.get.close(true);
			
			if (GrandExchangeWidget.get.isOpen())
				GrandExchangeWidget.get.close(true);
				
			if (Lodestone.AL_KHARID.teleport(true)) {
				
			}
			
			break;
		case WALK_TO_GE:
			if (Bank.get.isOpen())
				Bank.get.close(true);
			
			if (GrandExchange.walkToSWBooth()) {
				
			}
			
			break;
		}
	}
	
	@Override
	public boolean onStop() {
		ItemTableCallback.removeListener(itemTableListener);
		
		System.out.println("Final Leathers/hr: " + getLeathersPerHour());
		System.out.println("Final GP/hr: " + getGpPerHour() + " (" + getGpPerHour() / 1000 + "k)");
		
		return true;
	}
}
