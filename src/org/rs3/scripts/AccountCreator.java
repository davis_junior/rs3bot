package org.rs3.scripts;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.GroundItems;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Npcs;
import org.rs3.api.Paths;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.AccountCreateWidget;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.LobbyScreen;
import org.rs3.api.interfaces.widgets.LoginGraphicsAutoSetupScreen;
import org.rs3.api.interfaces.widgets.LoginGraphicsSetupConfirmationScreen;
import org.rs3.api.interfaces.widgets.LoginScreen;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.OptionsMenuWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.useless.IntroCutsceneWidget;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.database.AccountInfo;
import org.rs3.scripts.combat.EnemyType;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.FailsafesParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.tanning.HideType;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class AccountCreator extends Script {
	
	private enum Action {
		
		CREATE, 
		;
		
		public static Action getNextAction() {
			return CREATE;
		}
	}
	
	public enum Screen {
		RS_LOADING,
		LOGIN_SCREEN,
		ACCOUNT_CREATE_DESIGN,
		ACCOUNT_CREATE_ENLIST,
		LOGGED_IN,
		LOBBY_SCREEN;
		
		private Screen() {
			
		}
		
		public boolean isCurrent() {
			Screen curScreen = getCurrent();
			if (curScreen != null) {
				if (curScreen.equals(this))
					return true;
			}
			
			return false;
		}
		
		
		public static Screen getCurrent() {
			if (!Client.isRSDoneLoading())
				return RS_LOADING;
			
			if (Client.isLoggedIn() || IntroCutsceneWidget.get.isOpen())
				return LOGGED_IN;
			
			if (AccountCreateWidget.get.isOpen()) {
				if (AccountCreateWidget.get.isDesignScreenVisible())
					return ACCOUNT_CREATE_DESIGN;
				else if (AccountCreateWidget.get.isEnlistScreenVisible())
					return ACCOUNT_CREATE_ENLIST;
				else
					return null;
			}
			
			if (LoginScreen.get.isOpen())
				return LOGIN_SCREEN;
			
			if (LobbyScreen.get.isOpen())
				return LOBBY_SCREEN;
			
			return null;
		}
	}
	
	public void createAccount() {
		Screen curScreen = Screen.getCurrent();
		if (curScreen == null)
			return;
		
		Timer timer = new Timer(5000);
		
		switch (curScreen) {
		case ACCOUNT_CREATE_DESIGN:
			boolean female = Random.nextBoolean();
			if (female) {
				if (!AccountCreateWidget.get.clickFemaleButton(true))
					return;
			} else {
				if (!AccountCreateWidget.get.clickMaleButton(true))
					return;
			}
			
			Time.sleepQuick();
			
			for (int i = 0; i < Random.nextInt(3, 6); i++) {
				if (!AccountCreateWidget.get.clickRandomiseButton(true))
					return;
				Time.sleepMedium();
				
				if (Random.nextBoolean())
					Time.sleepMedium();
			}
			
			if (!AccountCreateWidget.get.clickDoneButton(true))
				return;
			
			Time.sleep(2000, 4000);
			timer = new Timer(10000);
			while (timer.isRunning()) {
				if (Screen.getCurrent() != null && Screen.getCurrent().equals(Screen.ACCOUNT_CREATE_ENLIST))
					break;
				
				Time.sleep(50);
			}
			
			Time.sleepMedium();
			
			break;
		case ACCOUNT_CREATE_ENLIST:
			AccountInfo accountInfo = AccountInfo.cur;
			if (accountInfo == null) {
				System.out.println("Account info is null! Stopping script...");
				stopInterrupt = true;
				return;
			}
			
			if (!AccountCreateWidget.get.enterCharacterName(accountInfo.getName()))
				return;
			Time.sleepQuick();
			
			boolean enterAge;
			try {
				String age = AccountCreateWidget.get.getAgeText();
				if (age != null && Integer.parseInt(age) >= 18 && Integer.parseInt(age) < 30)
					enterAge = false;
				else
					enterAge = true;
			} catch (NumberFormatException e) {
				enterAge = true;
			}
			
			if (enterAge) {
				if (!AccountCreateWidget.get.enterAge(Integer.toString(Random.nextInt(18, 30))))
					return;
				Time.sleepQuick();
			}
			
			if (!AccountCreateWidget.get.enterEmailAddress(accountInfo.getLogin()))
				return;
			Time.sleepQuick();
			
			if (!AccountCreateWidget.get.setShowPasswordCheckbox(true, true))
				return;
			Time.sleepMedium();
			Time.sleepQuick();
			
			if (!AccountCreateWidget.get.isShowPasswordChecked())
				return;
			
			if (!AccountCreateWidget.get.enterPassword(accountInfo.getPassword()))
				return;
			Time.sleepQuick();
			
			if (!AccountCreateWidget.get.setSendMeNewsCheckbox(true, false))
				return;
			Time.sleepMedium();
			Time.sleepQuick();
			
			Time.sleep(3000, 5000);
			
			if (AccountCreateWidget.get.isSendMeNewsChecked())
				return;
			
			if (!AccountCreateWidget.get.getCharacterNameText().equals(accountInfo.getName()))
				return;
			
			try {
				String age = AccountCreateWidget.get.getAgeText();
				if (!(age != null && Integer.parseInt(age) >= 18 && Integer.parseInt(age) < 30))
					return;
			} catch (NumberFormatException e) {
				return;
			}
			
			if (!AccountCreateWidget.get.getEmailAddressText().equals(accountInfo.getLogin()))
				return;
			
			if (!AccountCreateWidget.get.getPasswordText().equals(accountInfo.getPassword()))
				return;
			
			if (!AccountCreateWidget.get.clickPlayNowButton(true))
				return;
			
			Time.sleep(5000, 8000);
			timer = new Timer(30000);
			while (timer.isRunning()) {
				if (IntroCutsceneWidget.get.isOpen())
					break;
				
				if (Client.isLoggedIn())
					break;
				
				Time.sleep(50);
			}
			
			Time.sleepMedium();
			
			break;
		case LOBBY_SCREEN:
			if (LobbyScreen.get.isValidateEmailScreenVisible()) {
				LobbyScreen.get.clickResendEmailButton(true);
				Time.sleepMedium();
			}
			
			if (LobbyScreen.get.isMessageVisible()) {
				if (LobbyScreen.get.getMessageText().toLowerCase().contains("an email has been sent")) {
					if (LobbyScreen.get.clickMessageBackButton(true)) {
						System.out.println("Successfully resent validation email. Stopping script...");
						stopInterrupt = true;
					}
				}
			}
			
			break;
		case LOGGED_IN:
			if (IntroCutsceneWidget.get.isOpen()) {
				if (!IntroCutsceneWidget.get.close(true))
					return;
				
				timer = new Timer(10000);
				while (timer.isRunning()) {
					if (IntroCutsceneWidget.get.isClosed())
						return;
					
					Time.sleep(50);
				}
				
				Time.sleepMedium();
			}
			
			if (MainWidget.isTutorialActive()) {
				if (ContinueMessage.isClosed() && OptionsMenuWidget.get.open(true)) {
					Time.sleepQuick();
					
					if (OptionsMenuWidget.get.clickSkipTutorialButton(true)) {
						Time.sleepQuick();
						
						timer = new Timer(5000);
						while (timer.isRunning()) {
							if (ContinueMessage.isOpen())
								break;
							
							Time.sleep(50);
						}
					} else
						return;
				}
				
				if (ContinueMessage.isOpen()) {
					String text = ContinueMessage.getText(true);
					if (text != null && text.contains("tutorial")) {
						if (!ContinueMessage.close(true))
							return;
						Time.sleepQuick();
						
						timer = new Timer(5000);
						while (timer.isRunning()) {
							if (ChatOptionPane.get.isOpen())
								break;
							
							Time.sleep(50);
						}
						
						Time.sleepQuick();
					} else
						return;
				}
				
				if (ChatOptionPane.get.isOpen()) {
					if (ChatOptionPane.get.chooseYes(true)) {
						Time.sleepMedium();
						
						timer = new Timer(20000);
						while (timer.isRunning()) {
							if (!MainWidget.isTutorialActive())
								break;
							
							Time.sleep(50);
						}
						
						Time.sleepMedium();
					} else
						return;
				}
			}
			
			if (!MainWidget.isTutorialActive()) {
				if (!Client.logout(true))
					return;
				
				Time.sleepMedium();
				
				timer = new Timer(10000);
				while (timer.isRunning()) {
					if (LobbyScreen.get.isOpen())
						break;
					
					Time.sleep(50);
				}
				
				Time.sleepMedium();
			}
			
			break;
		case LOGIN_SCREEN:
			if (!LoginScreen.get.clickCreateAccountButton(true))
				return;
			
			Time.sleep(2000, 4000);
			timer = new Timer(10000);
			while (timer.isRunning()) {
				if (AccountCreateWidget.get.isOpen())
					break;
				
				Time.sleep(50);
			}
			
			Time.sleepMedium();
			
			break;
		case RS_LOADING:
			timer = new Timer(60000);
			while (timer.isRunning()) {
				if (Client.isRSDoneLoading())
					break;
				
				Time.sleep(50);
			}
			
			break;
		default:
			break;
		}
	}
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		//List<Failsafe> failsafes = new ArrayList<>();
		//failsafeWalkedToMine = new Failsafe("Walked to Tree Area", 900000); // 15 mins.
		//failsafeWalkedToBank = new Failsafe("Walked to Bank", 900000); // 15 mins.
		//failsafeMinedRock = new Failsafe("Chop", 900000); // 15 mins.
		//failsafeDropOrBank = new Failsafe("Drop or Bank", 900000); // 15 mins.
		//failsafes.add(failsafeWalkedToMine);
		//failsafes.add(failsafeWalkedToBank);
		//failsafes.add(failsafeMinedRock);
		//failsafes.add(failsafeDropOrBank);
		//params.add(new FailsafesParam(failsafes));
		
		//params.add(new LoginParam());
		
		//breakHandlerParam = new BreakHandlerParam(failsafes);
		//params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		//params.add(new CloseUselessInterfacesParam(false));
		//params.add(new CloseUselessInterfacesParam(true));
		
		//params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		
	}
	
	@Override
	public String getInfo() {
		return "";
	}
	
	@Override
	public boolean onStart() {
		
		loadDataParams();
		
		Antiban.resetTimer();
		
		return true;
	}
	
	Action curAction = null;
	Action lastAction = null;
	
	
	/*
	 * Script Notes
	 * 
	 * - 
	 */
	@Override
	public void run() {
		Antiban.randomMouseSpeed();
		
		//if (!Client.loopLogin())
		//	return;
		
		if (LoginGraphicsAutoSetupScreen.get.isOpen()) {
			LoginGraphicsAutoSetupScreen.get.clickAutoSetupButton(true);
			return;
		} else if (LoginGraphicsSetupConfirmationScreen.get.isOpen()) {
			LoginGraphicsSetupConfirmationScreen.get.clickYesButton(true);
			return;
		}
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		Action action = Action.getNextAction();
		
		
		if (curAction != null)
			lastAction = curAction;
		else
			lastAction = action;
		
		curAction = action;
		
		System.out.println(action);
		
		switch (action) {
		case CREATE:
			createAccount();
			break;
		}
	}
	
	@Override
	public boolean onStop() {
		return true;
	}
}
