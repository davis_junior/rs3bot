package org.rs3.scripts.parameters;

import org.rs3.util.Random;
import org.rs3.util.Timer;

public class RandomReseedParam implements ScriptParameter {
	
	long period;
	Timer timer;
	
	public RandomReseedParam() {
		period = 300000; // 5 mins.
		timer = new Timer(period);
	}
	
	public RandomReseedParam(long period) {
		this.period = period;
		this.timer = new Timer(period);
	}
	
	@Override
	public boolean onStart() {
		return true;
	}

	@Override
	public RunResult run() {
		if (!timer.isRunning()) {
			System.out.println("Attempting to reseed random...");
			Random.randomOrgReseed();
			
			timer.reset();
		}
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}

}
