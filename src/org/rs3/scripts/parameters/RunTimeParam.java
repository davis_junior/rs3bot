package org.rs3.scripts.parameters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.util.Timer;

public class RunTimeParam implements ScriptParameter {
	
	Timer mainTimer = null;
	
	@Override
	public boolean onStart() {
		mainTimer = new Timer(-1);
		return run().equals(RunResult.OK);
	}

	@Override
	public RunResult run() {
		if (!mainTimer.isRunning() && AccountInfo.cur != null) {
			mainTimer = new Timer(60000);
			
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				if (!row.isWithinTimeConstraint()) {
					System.out.println("Stopping script...");
					return RunResult.STOPSCRIPT;
				}
			}
		}
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}
}
