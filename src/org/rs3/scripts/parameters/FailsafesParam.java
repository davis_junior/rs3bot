package org.rs3.scripts.parameters;

import java.util.List;

import org.rs3.scripts.Failsafe;

public class FailsafesParam implements ScriptParameter {
	
	List<Failsafe> failsafes;
	
	public FailsafesParam(List<Failsafe> failsafes) {
		this.failsafes = failsafes;
	}
	
	@Override
	public boolean onStart() {
		return true;
	}

	@Override
	public RunResult run() {
		if (failsafes != null) {
			for (Failsafe failsafe : failsafes) {
				if (!failsafe.isRunning()) {
					System.out.println("ERROR: Failsafe: '" + failsafe.getName() + "'!");
					//stopInterrupt = true;
					return RunResult.STOPSCRIPT;
				}
			}
		}
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}
}
