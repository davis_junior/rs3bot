package org.rs3.scripts.parameters;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.ContinueMessageGame;
import org.rs3.api.interfaces.widgets.LodestoneNetwork;
import org.rs3.api.interfaces.widgets.LootScreen;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.OptionsMenuWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.useless.BecomeAMember;
import org.rs3.api.interfaces.widgets.useless.BecomeAMember2;
import org.rs3.api.interfaces.widgets.useless.BeginnerHintMessages;
import org.rs3.api.interfaces.widgets.useless.BondsWidget;
import org.rs3.api.interfaces.widgets.useless.ClanCup;
import org.rs3.api.interfaces.widgets.useless.ExaminePlayer;
import org.rs3.api.interfaces.widgets.useless.IntroCutsceneWidget;
import org.rs3.api.interfaces.widgets.useless.MegaMay;
import org.rs3.api.interfaces.widgets.useless.MembersOnlyContent;
import org.rs3.api.interfaces.widgets.useless.TaskComplete;
import org.rs3.api.interfaces.widgets.useless.TreasureHunterChest;
import org.rs3.api.interfaces.widgets.useless.WellOfGoodwill;
import org.rs3.api.interfaces.widgets.useless.WorldMap;
import org.rs3.api.wrappers.Client;
import org.rs3.util.Timer;

public class CloseUselessInterfacesParam implements ScriptParameter {
	
	boolean closeUnknownInterfaces;
	long period;
	Timer timer;
	
	/**
	 * If 'closeUnknownInterfaces' is false, the timer is not used.
	 * 
	 * @param closeUnknownInterfaces
	 */
	public CloseUselessInterfacesParam(boolean closeUnknownInterfaces) {
		this.closeUnknownInterfaces = closeUnknownInterfaces;
		period = 300000; // 5 mins.
		timer = new Timer(period);
	}
	
	/**
	 * If 'closeUnknownInterfaces' is false, the timer is not used.
	 * 
	 * @param closeUnknownInterfaces
	 * @param period
	 */
	public CloseUselessInterfacesParam(boolean closeUnknownInterfaces, long period) {
		this.closeUnknownInterfaces = closeUnknownInterfaces;
		this.period = period;
		this.timer = new Timer(period);
	}
	
	@Override
	public boolean onStart() {
		return true;
	}

	@Override
	public RunResult run() {
		// Each of these known interfaces close methods internally wait after click
		
		if (!Client.isLoggedIn())
			return RunResult.OK;
		
		if (MainWidget.get.isSubInterfaceOpen()
				&& !Ribbon.SubInterface.GRAND_EXCHANGE.isOpen()
				&& !MainWidget.get.closeSubInterface(true))
			return RunResult.FAIL;
		
		if (!OptionsMenuWidget.get.close(true))
			return RunResult.FAIL;
		
		if (!BeginnerHintMessages.get.close(true))
			return RunResult.FAIL;
		
		if (!BondsWidget.get.close(true))
			return RunResult.FAIL;
		
		if (!TaskComplete.get.close(true))
			return RunResult.FAIL;
		
		if (!ContinueMessage.close(true))
			return RunResult.FAIL;
		
		if (!MembersOnlyContent.get.close(true))
			return RunResult.FAIL;
		
		if (!BecomeAMember.get.close(true))
			return RunResult.FAIL;
		
		if (!BecomeAMember2.get.close(true))
			return RunResult.FAIL;
		
		if (!WellOfGoodwill.get.close(true))
			return RunResult.FAIL;
		
		if (!WorldMap.get.close(true))
			return RunResult.FAIL;
		
		if (!LodestoneNetwork.get.close(true))
			return RunResult.FAIL;
		
		if (!ExaminePlayer.get.close(true))
			return RunResult.FAIL;
		
		if (!LootScreen.get.close(true))
			return RunResult.FAIL;
		
		if (!ClanCup.get.close(true))
			return RunResult.FAIL;
		
		if (!TreasureHunterChest.get.close(true))
			return RunResult.FAIL;
		
		if (!MegaMay.get.close(true))
			return RunResult.FAIL;
		
		if (!IntroCutsceneWidget.get.close(true))
			return RunResult.FAIL;
		
		if (closeUnknownInterfaces && !timer.isRunning()) {
			System.out.println("Attempting to close useless interfaces...");
			
			if (closeUnknownInterfaces)
				Interfaces.closeUselessInterfaces(Interfaces.closeUselessInterfacesExclusionIds); // TODO: kinda slow; add common close button ids to only be closed every run cycle, and a separate thread every 5 mins or so should run this method
			
			timer.reset();
		}
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}

}
