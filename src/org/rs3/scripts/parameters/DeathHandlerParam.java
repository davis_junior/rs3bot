package org.rs3.scripts.parameters;

import org.rs3.api.GroundObjects;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.DeathItemReclaimingWidget;
import org.rs3.api.interfaces.widgets.OptionPane;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.Client;
import org.rs3.scripts.ScriptHandler;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class DeathHandlerParam implements ScriptParameter {
	
	private static final int deathId = 92233; // Death (Npc but is actually a ground object) - note name and model is always null, so use tile with height offset instead
	private static final int deathsHourglassId = 96782; // "Exit Death's Hourglass"
	private static final int deathsPlatformId = 92016; // platform in front of Death's desk, just used to create path back (at all times either the platform or the hourglass in reach for tile access)
	
	// ends on the platform in front of Death's desk - unusable because area tiles are randomized every time the office is enterred
	//private static final TilePath deathsHourglassToDeathPath = new TilePath( true, new Tile(8222, 1803, 0), new Tile(8222, 1804, 0), new Tile(8222, 1805, 0), new Tile(8222, 1806, 0), new Tile(8222, 1807, 0), new Tile(8222, 1808, 0), new Tile(8222, 1809, 0), new Tile(8222, 1810, 0), new Tile(8222, 1811, 0), new Tile(8222, 1812, 0), new Tile(8222, 1813, 0), new Tile(8222, 1814, 0), new Tile(8222, 1815, 0), new Tile(8222, 1816, 0), new Tile(8222, 1817, 0), new Tile(8222, 1818, 0), new Tile(8222, 1819, 0), new Tile(8222, 1820, 0), new Tile(8222, 1821, 0), new Tile(8222, 1822, 0), new Tile(8222, 1823, 0), new Tile(8222, 1824, 0), new Tile(8222, 1825, 0), new Tile(8222, 1826, 0) );
	
	public static boolean isPlayerDead() {
		return GroundObjects.getNearest(Type.All, deathId, deathsHourglassId) != null;
	}
	
	public static TilePath getPathToDeathsPlatform() {
		GroundObject hourglass = GroundObjects.getNearest(Type.All, deathsHourglassId);
		if (hourglass != null) {
			Tile tile = hourglass.getTile();
			int tileX = tile.getX();
			int tileY = tile.getY();
			int plane = Client.getPlane();
			
			Tile[] tiles = new Tile[24];
			for (int i = 0; i < 24; i++) {
				tiles[i] = new Tile(tileX, tileY + i, plane);
			}
			
			return new TilePath(true, tiles);
		}
		
		GroundObject platform = GroundObjects.getNearest(Type.All, deathsPlatformId);
		if (platform != null) {
			Tile tile = platform.getTile();
			int tileX = tile.getX();
			int tileY = tile.getY();
			int plane = Client.getPlane();
			
			Tile[] tiles = new Tile[24];
			int tileIndex = 0;
			for (int i = 23; i >= 0; i--) {
				tiles[tileIndex] = new Tile(tileX, tileY - i, plane);
				tileIndex++;
			}
			
			return new TilePath(true, tiles);
		}
		
		return null;
	}
	
	public static boolean exitDeathsOffice() {
		if (!isPlayerDead())
			return true;
		
		if (isPlayerDead()) {
			// manually takes about 8 seconds to walk
			if (getPathToDeathsPlatform().loopWalk(20000, null, true)) {
				GroundObject death = GroundObjects.getNearest(Type.All, deathId);
				
				if ((ContinueMessage.isOpen() && ContinueMessage.getText(true).contains("Greetings."))
						|| (ContinueMessage.isOpen() && ContinueMessage.getText(true).contains("Welcome back"))
						|| (DeathItemReclaimingWidget.get.isOpen())
						|| (death != null && death.getTile().loopInteract(null, "Talk-to"))) {
					
					Time.sleepQuick();
					
					if (ContinueMessage.isClosed() && !DeathItemReclaimingWidget.get.isOpen()) {
						Timer timer = new Timer(10000);
						while (timer.isRunning()) {
							if (ContinueMessage.isOpen())
								break;
							
							Time.sleep(50);
						}
					}
					
					if (ContinueMessage.isOpen() || ChatOptionPane.get.isOpen() || DeathItemReclaimingWidget.get.isOpen()) {
						boolean talkedToDeath = false;
						
						// manually takes about 12 seconds to get through chat options/continue messages
						// manually takes about 40? seconds to get through chat options/continue messages with new item reclaiming widget
						//Timer timer = new Timer(24000);
						Timer timer = new Timer(80000);
						while (timer.isRunning()) {
							if (ScriptHandler.currentScript != null && ScriptHandler.currentScript.stopInterrupt)
								return false;
							
							if (ContinueMessage.isOpen()) {
								ContinueMessage.close(true);
								Time.sleepQuick();
							}
							
							if (ChatOptionPane.get.isOpen()) {
								ChatOptionPane.get.chooseOption("Pleased to meet you", true);
								ChatOptionPane.get.chooseOption("Great! How do I get back?", true);
								if (ChatOptionPane.get.chooseOption("Goodbye!", true)) {
									talkedToDeath = true;
									break;
								}
								
								ChatOptionPane.get.chooseOption("Hello again", true);
								
								Time.sleepQuick();
							}
							
							if (DeathItemReclaimingWidget.get.isOpen()) {
								if (!DeathItemReclaimingWidget.get.isConfirmationScreenOpen()) {
									DeathItemReclaimingWidget.get.clickConfirm(true);
									Time.sleepMedium();
								} else {
									DeathItemReclaimingWidget.get.clickConfirmationScreenYesButton(true);
									Time.sleepMedium();
								}
							}
							
							Time.sleep(50);
						}
						
						if (talkedToDeath) {
							Time.sleepMedium();
							
							if (getPathToDeathsPlatform().reverse().loopWalk(20000, null, true)) {
								GroundObject deathsHourglass = GroundObjects.getNearest(Type.All, deathsHourglassId);
								if (deathsHourglass != null) {
									if (deathsHourglass.loopInteract(null, "Exit")) {
										timer = new Timer(15000);
										while (timer.isRunning()) {
											if (OptionPane.get.isOpen()) {
												if (OptionPane.get.chooseOption("Exit via the portal", true)) {
													Time.sleep(5000, 7000);
													return true;
												}
											}
											
											Time.sleep(50);
										}
									}
								}
							}
						}
					}
				}
				
				
			}
		}
		
		return false;
	}
	
	@Override
	public boolean onStart() {
		return true;
	}

	@Override
	public RunResult run() {
		if (!Client.isLoggedIn())
			return RunResult.OK;
		
		if (!exitDeathsOffice())
			return RunResult.FAIL;
		else
			return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}

}
