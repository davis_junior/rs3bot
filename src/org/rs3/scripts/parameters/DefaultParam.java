package org.rs3.scripts.parameters;

import org.rs3.api.places.Daemonheim;
import org.rs3.api.wrappers.Client;
import org.rs3.util.Random;

public class DefaultParam implements ScriptParameter {
	
	@Override
	public boolean onStart() {
		Random.randomOrgReseed();
		return true;
	}

	@Override
	public RunResult run() {
		if (Client.isLoggedIn()) {
			if (!Daemonheim.jumpDownBrokenStairs())
				return RunResult.FAIL;
		}
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}

}
