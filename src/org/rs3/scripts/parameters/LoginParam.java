package org.rs3.scripts.parameters;

import org.rs3.api.Interfaces;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.wrappers.Client;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;

public class LoginParam implements ScriptParameter {
	
	@Override
	public boolean onStart() {
		if (Client.loopLogin()) {
			Interfaces.closeUselessInterfaces(Interfaces.closeUselessInterfacesExclusionIds); // TODO: kinda slow; add common close button ids to only be closed every run cycle, and a separate thread every 5 mins or so should run this method
			return true;
		} else
			return false;
	}

	@Override
	public RunResult run() {
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}

}
