package org.rs3.scripts.parameters;

import java.util.List;

import org.rs3.api.antiban.BreakSystem;
import org.rs3.scripts.Failsafe;
import org.rs3.util.Timer;

public class BreakHandlerParam implements ScriptParameter {
	
	BreakSystem breakSystem;
	List<Failsafe> failsafes;
	
	public BreakHandlerParam(List<Failsafe> failsafes) {
		this.failsafes = failsafes;
		
		this.breakSystem = new BreakSystem();
	}
	
	@Override
	public boolean onStart() {
		return true;
	}

	@Override
	public RunResult run() {
		if (handleBreaks())
			return RunResult.FAIL;
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		return true;
	}
	
	public boolean handleBreaks() {
		//if (true) return false; // TODO: remove
		
		long timeMs;
		if ((timeMs = breakSystem.breakCheck()) != 0) {
			if (failsafes != null) {
				for (Failsafe failsafe : failsafes) {
					if (timeMs <= failsafe.getPeriod()) {
						Timer timer = failsafe.getTimer();
						
						if (!failsafe.isRunning())
							timer.setEndIn(timeMs);
						else
							timer.setEndIn(timer.getRemaining() + timeMs);
					} else
						failsafe.reset();
				}
			}
			
			return true;
		}
		
		return false;
	}
}
