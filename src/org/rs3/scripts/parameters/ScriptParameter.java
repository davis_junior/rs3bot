package org.rs3.scripts.parameters;

public interface ScriptParameter {
	
	public enum RunResult {
		OK, FAIL, STOPSCRIPT
	}
	
	public abstract boolean onStart();
	public abstract RunResult run();
	public abstract boolean onStop();
	
}
