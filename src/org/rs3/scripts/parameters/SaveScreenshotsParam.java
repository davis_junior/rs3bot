package org.rs3.scripts.parameters;

import java.io.File;

import org.rs3.callbacks.capture.CanvasCapture;
import org.rs3.util.Timer;

public class SaveScreenshotsParam implements ScriptParameter {
	
	Timer timer;
	long period;
	int maxScreenshots;
	int curScreenshotIndex;
	
	File directory;
	
	public SaveScreenshotsParam() {
		this.period = 60000; // 1 min
		this.maxScreenshots = 10;
		
		this.curScreenshotIndex = 0;
	}
	
	public SaveScreenshotsParam(long period, int maxScreenhots) {
		this.period = period;
		this.maxScreenshots = maxScreenhots;
		
		this.curScreenshotIndex = 0;
	}
	
	@Override
	public boolean onStart() {
		int i = 0;
		do {
			directory = new File("C:\\screenshots\\" + i + "\\");
			i++;
		} while (directory.exists());
		
		System.out.println("Screenshot directory: " + directory.getAbsolutePath());
		System.out.println("Max screenshots: " + maxScreenshots);
		
		CanvasCapture.saveScreenshot(true, directory, "onStart.jpg");
		
		timer = new Timer(period);
		
		return true;
	}

	@Override
	public RunResult run() {
		if (!timer.isRunning()) {
			CanvasCapture.saveScreenshot(true, directory, Integer.toString(curScreenshotIndex) + ".jpg");
			
			if (curScreenshotIndex >= maxScreenshots)
				curScreenshotIndex = 0;
			else
				curScreenshotIndex++;
			
			timer.reset();
		}
		
		return RunResult.OK;
	}

	@Override
	public boolean onStop() {
		System.out.println("Screenshot directory: " + directory.getAbsolutePath());
		System.out.println("Max screenshots: " + maxScreenshots);
		
		CanvasCapture.saveScreenshot(true, directory, "onStop.jpg");
		
		return true;
	}
}
