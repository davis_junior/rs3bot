package org.rs3.scripts;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rs3.accessors.CustomClassLoader;
import org.rs3.api.Ability;
import org.rs3.api.ActionBar;
import org.rs3.api.ActionSlot;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.CombatMode;
import org.rs3.api.Constants;
import org.rs3.api.EquipmentSlot;
import org.rs3.api.GESlot;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundItems;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Menu;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Prayer;
import org.rs3.api.Skill;
import org.rs3.api.Spell;
import org.rs3.api.Stats;
import org.rs3.api.Walking;
import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.exchange.ExchangeItem;
import org.rs3.api.exchange.ExchangeLoop;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.CollectionBox;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.interfaces.widgets.HoveredRibbonTabs;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.interfaces.widgets.Settings_Gameplay_LootSettingsSubTabWidget;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.PreciseTile;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.places.Daemonheim;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.Array1D;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Character;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.MenuItemNode;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.NpcDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.callbacks.ItemTableCallback;
import org.rs3.callbacks.ItemTableListener;
import org.rs3.callbacks.MenuCallback;
import org.rs3.callbacks.MenuClickFilter;
import org.rs3.callbacks.MenuListener;
import org.rs3.callbacks.MouseCallback;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.GrandExchangeRow;
import org.rs3.debug.Paint;
import org.rs3.scripts.combat.AttackStyle;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.FailsafesParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.parameters.ScriptParameter.RunResult;
import org.rs3.scripts.tanning.HideType;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.TextUtils;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import sun.org.mozilla.javascript.internal.InterfaceAdapter;

public class WineCollector extends Script {
	
	private static final int lawRuneId = 563;
	public static final int wineId = 245;
	private static final int staffOfAirId = 1381;
	private static final int greenDhideCoifId = 12936;
	private static final int runeArrowId = 892;
	//private static final int magicShortbowId = 861;
	private static final int magicShieldbowId = 859;
	private static final int greenDhideBodyId = 1135;
	private static final int blueDhideChapsId = 2493;
	private static final int blueDhideVambracesId = 2487;
	private static final int amuletOfPowerId = 1731;
	private static final int airRuneId = 556;
	//private static final int lobsterId = 379;
	
	private static final String lawRuneName = "Law rune";
	public static final String wineName = "Wine of Zamorak";
	private static final String staffOfAirName = "Staff of air";
	private static final String greenDhideCoifName = "Green d'hide coif";
	private static final String runeArrowName = "Rune arrow";
	//private static final String magicShortbowName = "Magic shortbow";
	private static final String magicShieldbowName = "Magic shieldbow";
	private static final String greenDhideBodyName = "Green d'hide body";
	private static final String blueDhideChapsName = "Blue d'hide chaps";
	private static final String blueDhideVambracesName = "Blue d'hide vambraces";
	private static final String amuletOfPowerName = "Amulet of power";
	private static final String airRuneName = "Air rune";
	//private static final String lobsterName = "Lobster";
	
	private static final int[] buyEquipmentIds = new int[] { greenDhideCoifId, magicShieldbowId, greenDhideBodyId, blueDhideChapsId, blueDhideVambracesId, amuletOfPowerId };
	private static final int[] allEquipmentIds = new int[] { greenDhideCoifId, magicShieldbowId, greenDhideBodyId, blueDhideChapsId, blueDhideVambracesId, amuletOfPowerId, runeArrowId, Daemonheim.ringOfKinshipId };
	
	private static final int chaosAltarId = 78228; // Actions: Pray-at
	private static final String chaosAltarName = "Chaos altar";
	
	private static Tile wineTile = new Tile(2952, 3473, 0);
	private static Tile[] winePlayerStandingTiles = new Tile[] { new Tile(2951, 3473, 0), new Tile(2951, 3474, 0), new Tile(2952, 3474, 0) };
	//private static Tile burthopeBankTile = new Tile(2887, 3535, 0);
	
	private static Area capturedTempleArea = new Area( new Tile(2946, 3478, 0), new Tile(2947, 3478, 0), new Tile(2948, 3478, 0), new Tile(2949, 3478, 0), new Tile(2950, 3478, 0), new Tile(2951, 3478, 0), new Tile(2952, 3478, 0), new Tile(2952, 3477, 0), new Tile(2953, 3477, 0), new Tile(2954, 3477, 0), new Tile(2954, 3476, 0), new Tile(2954, 3475, 0), new Tile(2954, 3474, 0), new Tile(2953, 3474, 0), new Tile(2952, 3474, 0), new Tile(2952, 3473, 0), new Tile(2951, 3473, 0), new Tile(2950, 3473, 0), new Tile(2949, 3473, 0), new Tile(2948, 3473, 0), new Tile(2947, 3473, 0), new Tile(2946, 3473, 0), new Tile(2946, 3474, 0), new Tile(2946, 3475, 0), new Tile(2946, 3476, 0), new Tile(2946, 3477, 0) );
	private static TilePath faladorLStoCapturedTemplePath = new TilePath( new Tile(2967, 3403, 0), new Tile(2966, 3403, 0), new Tile(2966, 3404, 0), new Tile(2966, 3405, 0), new Tile(2966, 3406, 0), new Tile(2966, 3407, 0), new Tile(2966, 3408, 0), new Tile(2966, 3409, 0), new Tile(2966, 3410, 0), new Tile(2966, 3411, 0), new Tile(2966, 3412, 0), new Tile(2965, 3412, 0), new Tile(2965, 3413, 0), new Tile(2964, 3413, 0), new Tile(2963, 3413, 0), new Tile(2963, 3414, 0), new Tile(2963, 3415, 0), new Tile(2962, 3415, 0), new Tile(2962, 3416, 0), new Tile(2962, 3417, 0), new Tile(2961, 3417, 0), new Tile(2961, 3418, 0), new Tile(2961, 3419, 0), new Tile(2960, 3419, 0), new Tile(2959, 3419, 0), new Tile(2959, 3420, 0), new Tile(2959, 3421, 0), new Tile(2959, 3422, 0), new Tile(2958, 3422, 0), new Tile(2957, 3422, 0), new Tile(2957, 3423, 0), new Tile(2957, 3424, 0), new Tile(2957, 3425, 0), new Tile(2956, 3425, 0), new Tile(2955, 3425, 0), new Tile(2955, 3426, 0), new Tile(2955, 3427, 0), new Tile(2955, 3428, 0), new Tile(2954, 3428, 0), new Tile(2953, 3428, 0), new Tile(2953, 3429, 0), new Tile(2953, 3430, 0), new Tile(2953, 3431, 0), new Tile(2952, 3431, 0), new Tile(2951, 3431, 0), new Tile(2951, 3432, 0), new Tile(2951, 3433, 0), new Tile(2950, 3433, 0), new Tile(2950, 3434, 0), new Tile(2950, 3435, 0), new Tile(2949, 3435, 0), new Tile(2949, 3436, 0), new Tile(2949, 3437, 0), new Tile(2949, 3438, 0), new Tile(2949, 3439, 0), new Tile(2948, 3439, 0), new Tile(2948, 3440, 0), new Tile(2948, 3441, 0), new Tile(2948, 3442, 0), new Tile(2948, 3443, 0), new Tile(2948, 3444, 0), new Tile(2948, 3445, 0), new Tile(2948, 3446, 0), new Tile(2948, 3447, 0), new Tile(2948, 3448, 0), new Tile(2948, 3449, 0), new Tile(2948, 3450, 0), new Tile(2948, 3451, 0), new Tile(2948, 3452, 0), new Tile(2948, 3453, 0), new Tile(2948, 3454, 0), new Tile(2948, 3455, 0), new Tile(2948, 3456, 0), new Tile(2948, 3457, 0), new Tile(2949, 3457, 0), new Tile(2949, 3458, 0), new Tile(2949, 3459, 0), new Tile(2949, 3460, 0), new Tile(2949, 3461, 0), new Tile(2949, 3462, 0), new Tile(2950, 3462, 0), new Tile(2950, 3463, 0), new Tile(2950, 3464, 0), new Tile(2950, 3465, 0), new Tile(2951, 3465, 0), new Tile(2951, 3466, 0), new Tile(2951, 3467, 0), new Tile(2952, 3467, 0), new Tile(2953, 3467, 0), new Tile(2954, 3467, 0), new Tile(2954, 3468, 0), new Tile(2954, 3469, 0), new Tile(2954, 3470, 0), new Tile(2955, 3470, 0), new Tile(2956, 3470, 0), new Tile(2956, 3471, 0), new Tile(2956, 3472, 0), new Tile(2957, 3472, 0), new Tile(2958, 3472, 0), new Tile(2958, 3473, 0), new Tile(2958, 3474, 0), new Tile(2958, 3475, 0), new Tile(2957, 3475, 0), new Tile(2956, 3475, 0), new Tile(2955, 3475, 0), new Tile(2954, 3475, 0), new Tile(2953, 3475, 0), new Tile(2952, 3475, 0), new Tile(2952, 3474, 0) );
	
	// area next to wheat farm below captured temple, player will go here when monks are attacking
	private static Area monkSafeZone = new Area( new Tile(2945, 3455, 0), new Tile(2946, 3455, 0), new Tile(2947, 3455, 0), new Tile(2948, 3455, 0), new Tile(2949, 3455, 0), new Tile(2949, 3454, 0), new Tile(2949, 3453, 0), new Tile(2949, 3452, 0), new Tile(2949, 3451, 0), new Tile(2949, 3450, 0), new Tile(2949, 3449, 0), new Tile(2950, 3449, 0), new Tile(2950, 3448, 0), new Tile(2950, 3447, 0), new Tile(2949, 3447, 0), new Tile(2948, 3447, 0), new Tile(2947, 3447, 0), new Tile(2946, 3447, 0), new Tile(2945, 3447, 0), new Tile(2944, 3447, 0), new Tile(2944, 3448, 0), new Tile(2944, 3449, 0), new Tile(2944, 3450, 0), new Tile(2944, 3451, 0), new Tile(2944, 3452, 0), new Tile(2944, 3453, 0), new Tile(2944, 3454, 0), new Tile(2944, 3455, 0) );
	
	private static List<Integer> monkOfZamorakIds = Arrays.asList(188, 189);
	
	private static boolean exchange = false;
	private static ExchangeLoop exchangeLoop = new ExchangeLoop(null, new ExchangeItem[] {
			new ExchangeItem(wineId, wineName, true),
			new ExchangeItem(lawRuneId, lawRuneName, false, 2000, 30), // 25000 is ge limit
			new ExchangeItem(airRuneId, airRuneName, false, 5000, 10),
			new ExchangeItem(runeArrowId, runeArrowName, false, 2000, 20),
			//new ExchangeItem(lobsterId, lobsterName, false, 1000, 20),
	});
	
	private Timer breakTimer = null;
	
	private Model wineModel = null;
	private PreciseTile winePreciseTile = null;
	private int wineAccumulated;
	
	private static final int MIN_HEALTH = 1000;
	
	private ItemTableListener itemTableListener = new ItemTableListener() {
		
		@Override
		public void replace(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			//System.out.println("replace[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize + " with old = " + originalItemId + ", " + originalItemStackSize);
		}
		
		@Override
		public void remove(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("remove[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			
		}
		
		@Override
		public void increase(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("increase[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void decrease(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("decrease[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void add(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("add[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
			
			if (itemTableId == ItemTable.INVENTORY.getId()) {
				if (itemId == wineId)
					wineAccumulated++;
			}
		}
	};
	
	private volatile boolean waitForMenuOption = false;
	private volatile boolean takeWineInvoked = false;
	private volatile boolean takeWineThreadRunning = false;
	private MenuListener menuListener = new MenuListener() {
		@Override
		public void append(Object menuItemNodeObj) {
			if (takeWineThreadRunning)
				return;
			
			if (waitForMenuOption && !takeWineInvoked) {
				if (menuItemNodeObj != null) {
					MenuItemNode menuItemNode = MenuItemNode.create(menuItemNodeObj);
					
					String menuOption = TextUtils.removeHtml(menuItemNode.getOption());
					if (menuOption.contains("Wine of Zamorak")) {
						String menuAction = TextUtils.removeHtml(menuItemNode.getAction());
						
						boolean shouldCastSpell = shouldCastSpell();
						
						String preferredAction = shouldCastSpell ? "Cast" : "Take";
						String perferredOption = shouldCastSpell ? Spell.TELEKINETIC_GRAB.getName() + " -> Wine of Zamorak" : "Wine of Zamorak";
						
						boolean menuItemMatches = false;
						if (menuAction.equals(preferredAction) && menuOption.equals(perferredOption))
							menuItemMatches = true;
						else {
							menuItemNode = Menu.getFirstItemNode();
							if (menuItemNode != null) {
								menuAction = TextUtils.removeHtml(menuItemNode.getAction());
								menuOption = TextUtils.removeHtml(menuItemNode.getOption());
								if (menuAction.equals(preferredAction) && menuOption.equals(perferredOption))
									menuItemMatches = true;
							}
							
							if (!menuItemMatches) {
								menuItemNode = Menu.getSecondItemNode();
								if (menuItemNode != null) {
									menuAction = TextUtils.removeHtml(menuItemNode.getAction());
									menuOption = TextUtils.removeHtml(menuItemNode.getOption());
									if (menuAction.equals(preferredAction) && menuOption.equals(perferredOption))
										menuItemMatches = true;
								}
							}
						}
						
						if (menuItemMatches && !takeWineThreadRunning) {
							final String action = menuAction;
							final String option = menuOption;
							Thread thread = new Thread() {
								public void run() {
									try {
										// 10% chance to skip wine (1 in 10)
										if (Random.nextInt(0, 100) < 10) {
											System.out.println("Skipping this wine...");
											boolean doAntiban = Random.nextBoolean();
											Timer timer = new Timer(Random.nextInt(5000, 10000));
											while (timer.isRunning()) {
												if (doAntiban) {
													if (Antiban.doAntiban())
														Time.sleepVeryQuick();
												}
												
												Time.sleep(50);
											}
											return;
										}
										
										//Time.sleep(0.90F, 30, 60, 40, 90);
										//Time.sleep(0.90F, 60, 90, 90, 300);
										//Time.sleep(0.90F, 200, 300, 500, 1200); // 250ms is average human reaction to visual stimuli
										Time.sleep(0.90F, 30, 60, 500, 1200);
										if (shouldCastSpell() && !Client.isUsableComponentSelected())
											return;
										
										MouseCallback.menuClickFilter = new MenuClickFilter() {
											@Override
											public Object setMenuItem(Object origMenuItemNode) {
												MenuItemNode min = Menu.getNode(action, option);
												if (min != null) {
													try {
														Class<?> clazz = CustomClassLoader.get.loadClass(data.Client.get.firstMenuItem.getInternalClassName());
														Field field = clazz.getDeclaredField(data.Client.get.firstMenuItem.getInternalName());
														field.setAccessible(true);
														field.set(null, min.getObject());
													} catch (ClassNotFoundException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
														e.printStackTrace();
													}
													
													return min.getObject();
												}
												
												return origMenuItemNode;
											}
										};
										
										if (Menu.getNode(action, option) != null && Mouse.fastClick(Mouse.LEFT) != null) {
										//if (Menu.invoke(action, option)) {
										//if (Menu.select(action, option)) {
											Time.sleepQuick();
											waitForMenuOption = false;
											takeWineInvoked = true;
										} else
											Time.sleepQuick();
									} catch (Exception e) {
										//e.printStackTrace();
									} finally {
										MouseCallback.menuClickFilter = null;
										takeWineThreadRunning = false;
									}
								}
							};
							takeWineThreadRunning = true;
							thread.start();
						}
					}
				}
			}
		}
	};
	
	private enum Action {
		
		COLLECT(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Player player = Players.getMyPlayer();
				if (player == null)
					return null;
				
				if(capturedTempleArea.contains(player.getTile()))
					return SubAction.DEFAULT;
				else if (capturedTempleArea.getDistanceToNearest() < 10)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		BANK(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Interactable bank = Bank.get.getNearest();
				if (bank != null && Calculations.distanceTo(bank) < 25)
					return SubAction.DEFAULT;
				else if (Daemonheim.isNearDaemonheimEntrance())
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		EXCHANGE(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Npc npc = GrandExchangeWidget.get.getNearest();
				if (npc != null && Calculations.distanceTo(npc) <= 10)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		};
		
		EnumSet<SubAction> possibleSubs;
		
		Action(EnumSet<SubAction> possibleSubs) {
			this.possibleSubs = possibleSubs;
		}
		
		public static Action getNextAction() {
			if (!exchange) {
				if (exchangeLoop.getMainExchangeTimer() != null && !exchangeLoop.getMainExchangeTimer().isRunning()) {
					exchange = true;
					return EXCHANGE;
				}
				
				if (Stats.HEALTH.getCurrent() < MIN_HEALTH)
					return BANK;
				
				if (ItemTable.INVENTORY.countAllItems(false) >= 28)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(lawRuneId))
					return BANK;
				else if (ItemTable.INVENTORY.countItems(true, airRuneId) < 3)
					return BANK;
				//else if (!ItemTable.INVENTORY.containsAny(staffOfAirId) && EquipmentSlot.MAINHAND.getItemId() != staffOfAirId)
				//	return BANK;
				else if (!ItemTable.INVENTORY.containsAny(greenDhideCoifId) && EquipmentSlot.HEAD.getItemId() != greenDhideCoifId)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(magicShieldbowId) && EquipmentSlot.MAINHAND.getItemId() != magicShieldbowId)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(greenDhideBodyId) && EquipmentSlot.TORSO.getItemId() != greenDhideBodyId)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(blueDhideChapsId) && EquipmentSlot.LEGS.getItemId() != blueDhideChapsId)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(blueDhideVambracesId) && EquipmentSlot.HANDS.getItemId() != blueDhideVambracesId)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(amuletOfPowerId) && EquipmentSlot.NECK.getItemId() != amuletOfPowerId)
					return BANK;
				else if (!ItemTable.INVENTORY.containsAny(runeArrowId) && EquipmentSlot.AMMUNITION.getItemId() != runeArrowId)
					return BANK;
				
				else if (!ItemTable.INVENTORY.containsAny(Daemonheim.ringOfKinshipId) && EquipmentSlot.RING.getItemId() != Daemonheim.ringOfKinshipId)
					return BANK;
				else
					return COLLECT;
			} else
				return EXCHANGE;
		}
		
		public abstract SubAction getSubAction();
	}
	
	public enum SubAction {
		DEFAULT, WALK
	};
	
	public static GroundItem getWine() {
		GroundItem[] gis = GroundItems.getItemsAt(wineTile.getX(), wineTile.getY());
		if (gis != null) {
			for (GroundItem gi : gis) {
				if (gi != null && gi.getId() == wineId)
					return gi;
			}
		}
		
		return null;
	}
	
	Filter<GroundObject> oresAreaFilter = new Filter<GroundObject>() {
		@Override
		public boolean accept(GroundObject animable) {
			if (OreType.getCurrent().getArea().contains(animable.getTile()))
				return true;
			
			return false;
		}
	};
	
	Condition wineIsOnTable = new Condition() {
		@Override
		public boolean evaluate() {
			return getWine() != null;
		}
	};
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public boolean standingOnGoodTile() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		for (Tile tile : winePlayerStandingTiles) {
			if (player.getTile().equals(tile)) {
				return true;
			}
		}
		
		return false;
	}
	
	long lastWineGrabTime;
	long durationLastGrab;
	
	public boolean areMonksDead() {
		Npc[] npcs = Npcs.getAll();
		if (npcs != null) {
			for (Npc npc : npcs) {
				if (npc != null) {
					NpcDefinition def = npc.getNpcDefinition();
					if (def != null && monkOfZamorakIds.contains(def.getId()))
						return false;
				}
			}
		}
		
		return true;
	}
	
	public boolean isMonkAttackingMyPlayer() {
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		Character[] othersInteracting = player.getOthersIneracting();
		if (othersInteracting != null) {
			for (Character interacting : othersInteracting) {
				if (interacting instanceof Npc) {
					Npc npc = (Npc) interacting;
					NpcDefinition def = npc.getNpcDefinition();
					if (def != null && monkOfZamorakIds.contains(def.getId()))
						return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * If under attack, run to monk safe zone and wait a few seconds
	 * @return
	 */
	public boolean runToMonkSafeZoneIfUnderAttack() {
		if (monkSafeZone.getDistanceToNearest() < 25 || capturedTempleArea.getDistanceToNearest() < 25) {
			if (isMonkAttackingMyPlayer()) {
				if (Client.isUsableComponentSelected()) {
					Spell.cancelUsableComponentSelection();
					Time.sleepQuick();
				}
				
				Player player = Players.getMyPlayer();
				if (player == null)
					return false;
				
				if (monkSafeZone.contains(player.getTile()) || monkSafeZone.loopWalkTo(30000)) {
					Time.sleep(2000, 5000);
					return true;
				}
			}
		}
		
		return false;
	}
	
	// lose 2 magic levels when take wine directly off table
	public boolean shouldCastSpell() {
		return Skill.MAGIC.getCurrentLevel() >= 35 ? !areMonksDead() : true;
	}
	
	public boolean collect() {
		
		int minZoom = 30;
		int maxZoom = 50;
		
		if (Camera.getZoomPercent() < minZoom || Camera.getZoomPercent() > maxZoom) {
			Camera.setZoomPercent(Random.nextInt(minZoom, maxZoom));
			Time.sleepQuick();
		}
		
		if (Stats.HEALTH.getPercentage() < 60) {
			ActionSlot slot = ActionSlot.getSlot(Ability.REJUVENATE);
			if (slot != null) {
				// Activate
				if (!slot.isQueued()) {
					slot.interact(true, "Activate", Ability.REJUVENATE.getName());
					Time.sleepQuick();
				}
			}
		} else if (Stats.HEALTH.getPercentage() >= 65) {
			ActionSlot slot = ActionSlot.getSlot(Ability.REJUVENATE);
			if (slot != null) {
				// Deactivate
				if (slot.isQueued()) {
					slot.interact(true, "Activate", Ability.REJUVENATE.getName());
					Time.sleepQuick();
				}
			}
		}
		
		if (Stats.PRAYER.getPercentage() < 30) {
			GroundObject altar = GroundObjects.getNearest(Type.All, chaosAltarId);
			if (altar != null) {
				if (!altar.getTile().isOnScreen()) {
					Camera.turnTo(altar);
					Time.sleepQuick();
				}
				
				if (!altar.getTile().isOnScreen()) {
					altar.loopWalkTo(5000);
					Time.sleepQuick();
				}
				
				if (!altar.loopInteract(null, "Pray-at")) {
					Time.sleepQuick();
					return false;
				}
				
				Player player = Players.getMyPlayer();
				if (player == null)
					return false;
				
				player.waitForMoving(5000, true, (Condition) null);
				player.waitForStopMoving(5000, true, (Condition) null);
			}
		}
		
		try {
			Component.interactionAntiban = false;
			
			if (!Prayer.PROTECT_FROM_MAGIC.activate(true))
				return false;
			
			if (!Prayer.RAPID_HEAL.activate(true))
				return false;
			
			if (!Prayer.RAPID_RESTORE.activate(true))
				return false;
		} finally {
			Component.interactionAntiban = true;
		}
		
		Tab.closeAll();
		
		// TODO: attack monks within temple
		
		if (Skill.MAGIC.getCurrentLevel() < 33) {
			System.out.println("Magic level below 33, waiting...");
			return false;
		}
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		//if (runToMonkSafeZoneIfUnderAttack())
		//	return false;
		
		if (!standingOnGoodTile()) {
			if (Camera.getPitch() < 65) {
				Camera.setPitch(Random.nextInt(65, 74));
				Time.sleepVeryQuick();
			}
			
			Tile standTile = winePlayerStandingTiles[Random.nextInt(0, winePlayerStandingTiles.length)];
			
			if (capturedTempleArea.contains(player.getTile()) || standTile.loopWalkTo(capturedTempleArea, 10000, null)) {
				Time.sleepQuick();
				
				if (!standingOnGoodTile())
					standTile.loopInteract(null, "Walk here");
			}
			Time.sleepVeryQuick();
			if (player.waitForMoving(3000, true, (Condition[])null)) {
				player.waitForStopMoving(10000, true, (Condition[])null);
				Time.sleepVeryQuick();
			}
		}
		
		if (!standingOnGoodTile())
			return false;
		
		if (wineModel == null) {
			Timer timer = new Timer(30000);
			while (timer.isRunning()) {
				if (Stats.HEALTH.getCurrent() < MIN_HEALTH)
					return false;
				
				GroundItem wine = getWine();
				if (wine != null) {
					wineModel = wine.getModel();
					if (wineModel != null) {
						wineModel.heightOff = -350;
						break;
					}
				}
				
				Time.sleep(50);
			}
		}
		
		if (wineModel != null) {
			if (Camera.getZoomPercent() < minZoom || Camera.getZoomPercent() > maxZoom) {
				Camera.setZoomPercent(Random.nextInt(minZoom, maxZoom));
				Time.sleepQuick();
			}
			
			long toWait = 0;
			// wait to save cpu
			if (lastWineGrabTime != 0 && durationLastGrab > 0) {
				toWait = (long) (lastWineGrabTime + (durationLastGrab - durationLastGrab*0.25) - System.currentTimeMillis());
				if (toWait > 0) {
					toWait -= Random.nextInt((int) (toWait*0.2), (int) (toWait*0.3));
					
					if (toWait >= 12000)
						toWait = Random.nextInt(10000, 12000);
					
					Timer timer = new Timer(toWait);
					while (timer.isRunning()) {
						if (Stats.HEALTH.getCurrent() < MIN_HEALTH)
							return false;
						
						if (timer.getRemaining() >= 3000) {
							if (Antiban.doAntiban())
								Time.sleepVeryQuick();
						}
						
						Time.sleep(50);
					}
				}
			}
			
			int loopSleepTime = Random.nextInt(40, 60);
			Array1D<Player> players = Client.getPlayers();
			if (players != null) {
				for (Player p : players.getAll()) {
					if (p != null) {
						if (p.getObject() == player.getObject())
							continue;
						
						Tile playerTile = p.getTile();
						if (playerTile != null && capturedTempleArea.contains(playerTile)) {
							System.out.println("Player detected in captured temple, thread will not sleep.");
							loopSleepTime = 0;
							break;
						}
					}
				}
			}
			
			BaseInfo base = Client.getGameInfo().getBaseInfo();
			winePreciseTile = new PreciseTile((wineTile.getX() - base.getX() + 0.5f) * 512f, (wineTile.getY() - base.getY() + 0.5f) * 512f, 0, wineTile.plane);
			winePreciseTile.height = Calculations.tileHeight((int)winePreciseTile.x, (int)winePreciseTile.y, winePreciseTile.plane);
			
			if (Camera.getYaw() >= 166 && Camera.getYaw() <= 265) {
				// ok
			} else {
				Camera.setAngle(Random.nextInt(166, 265));
				Time.sleepVeryQuick();
			}
			
			if (Camera.getPitch() < 60) {
				Camera.setPitch(Random.nextInt(60, 74));
				Time.sleepVeryQuick();
			}
			
			Time.sleepQuickest();
			
			if (HoveredRibbonTabs.get.isOpen()) {
				Antiban.randomMoveMouse();
				Time.sleepQuickest();
			}
			
			if (!shouldCastSpell() || Spell.TELEKINETIC_GRAB.select(true)) {
				try {
					Component.interactionAntiban = false;
					
					if (wineModel.hover(winePreciseTile.getX(), winePreciseTile.getY(), 0)) {
						Timer timer = new Timer(30000 - toWait);
						takeWineInvoked = false;
						waitForMenuOption = true;
						while (timer.isRunning() || takeWineThreadRunning) {
							if (Stats.HEALTH.getCurrent() < MIN_HEALTH)
								return false;
							
							if (!takeWineThreadRunning) {
								boolean shouldCastSpell = shouldCastSpell();
								
								if (!takeWineInvoked) {
									if (shouldCastSpell && !Client.isUsableComponentSelected()) {
										Time.sleepMedium();
										
										if (takeWineThreadRunning)
											continue;
										
										if (!takeWineInvoked) // double check
											break;
									}
									
									if (!shouldCastSpell && Client.isUsableComponentSelected()) {
										waitForMenuOption = false;
										Spell.cancelUsableComponentSelection();
										Time.sleepVeryQuick();
										break;
									}
								}
								
								if (takeWineInvoked) {
									waitForMenuOption = false;
									
									failsafeMinedRock.reset();
									
									if (lastWineGrabTime != 0)
										durationLastGrab = System.currentTimeMillis() - lastWineGrabTime;
									lastWineGrabTime = System.currentTimeMillis();
									
									
									player = Players.getMyPlayer();
									if (player == null)
										return false;
									
									player.waitForAnimation(new ArrayList<Integer>(24452), 5000, false, (Condition[]) null); // telekinetic grab animation id
									player.waitForIdleAnimation(new ArrayList<Integer>(24452), 10000, true, (Condition[]) null);
									break;
								}
								
								/*String menuAction = Menu.getFirstAction();
								String menuOption = Menu.getFirstOption();
								
								if (menuAction != null && menuOption != null) {
									if (shouldCastSpell && menuAction.equals("Cast")
											&& menuOption.equals(Spell.TELEKINETIC_GRAB.getName() + " -> Wine of Zamorak")) {
										if (Mouse.fastClick(Mouse.LEFT) != null) {
											failsafeMinedRock.reset();
											
											if (lastWineGrabTime != 0)
												durationLastGrab = System.currentTimeMillis() - lastWineGrabTime;
											lastWineGrabTime = System.currentTimeMillis();
											
											
											Player player = Players.getMyPlayer();
											if (player == null)
												return false;
			
											player.waitForAnimation(new ArrayList<Integer>(24452), 5000, false, (Condition[]) null); // telekinetic grab animation id
											player.waitForIdleAnimation(new ArrayList<Integer>(24452), 10000, true, (Condition[]) null);
											break;
										}
									} else if (!shouldCastSpell && menuAction.equals("Take")
											&& menuOption.equals("Wine of Zamorak")) {
										if (Mouse.fastClick(Mouse.LEFT) != null) {
											failsafeMinedRock.reset();
											
											if (lastWineGrabTime != 0)
												durationLastGrab = System.currentTimeMillis() - lastWineGrabTime;
											lastWineGrabTime = System.currentTimeMillis();
											
											
											Player player = Players.getMyPlayer();
											if (player == null)
												return false;
			
											player.waitForAnimation(new ArrayList<Integer>(24452), 5000, false, (Condition[]) null); // telekinetic grab animation id
											player.waitForIdleAnimation(new ArrayList<Integer>(24452), 10000, true, (Condition[]) null);
											break;
										}
									} else {
										if (shouldCastSpell) {
											//Time.sleepVeryQuick();
											//Antiban.randomCameraAngle();
											//Time.sleepVeryQuick();
											wineModel.hover(winePreciseTile.getX(), winePreciseTile.getY(), 0);
											//Time.sleepQuickest();
											Time.sleepVeryQuick();
										}
									}
								}*/
							}
							
							//Time.sleep(5);
							//if (loopSleepTime != 0)
							//	Time.sleep(loopSleepTime);
							Time.sleep(50);
						}
						
						waitForMenuOption = false;
						takeWineInvoked = false;
					}
				} finally {
					Component.interactionAntiban = true;
				}
			}
		}
		
		return false;
	}
	
	boolean checkedCollectionBox = false;
	
	public boolean bank() {
		if ((Bank.get.getNearest() != null || Daemonheim.walkToDaemonheimEntrance())
				&& Bank.get.open()) {
			
			Time.sleepQuick();
			
			if (Bank.get.depositAllInvItems(ArrayUtils.combine(new int[] {/*staffOfAirId,*/ lawRuneId, airRuneId}, allEquipmentIds))) {
				Time.sleepQuick();
				
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					Bank.get.withdrawAll(995, false); // coins
					Time.sleepQuick();
				}
				
				boolean shouldCheckCollectionBox = false;
				
				if (!ItemTable.INVENTORY.containsAny(lawRuneId)
						&& !ItemTable.BANK.containsAny(lawRuneId)) {
					/*System.out.println("Out of law runes! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Out of law runes. Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(airRuneId)
						&& !ItemTable.BANK.containsAny(airRuneId)) {
					/*System.out.println("Out of air runes! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Out of air runes. Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				/*if (!ItemTable.INVENTORY.containsAny(staffOfAirId)
						&& EquipmentSlot.MAINHAND.getItemId() != staffOfAirId
						&& !ItemTable.BANK.containsAny(staffOfAirId)) {
					//System.out.println("Missing staff! Stopping script...");
					//interruptStop();
					//return false;
					System.out.println("Missing staff! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}*/
				
				if (!ItemTable.INVENTORY.containsAny(greenDhideCoifId)
						&& EquipmentSlot.HEAD.getItemId() != greenDhideCoifId
						&& !ItemTable.BANK.containsAny(greenDhideCoifId)) {
					/*System.out.println("Missing coif! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing coif! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(magicShieldbowId)
						&& EquipmentSlot.MAINHAND.getItemId() != magicShieldbowId
						&& !ItemTable.BANK.containsAny(magicShieldbowId)) {
					/*System.out.println("Missing bow! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing bow! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(greenDhideBodyId)
						&& EquipmentSlot.TORSO.getItemId() != greenDhideBodyId
						&& !ItemTable.BANK.containsAny(greenDhideBodyId)) {
					/*System.out.println("Missing body! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing body! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(blueDhideChapsId)
						&& EquipmentSlot.LEGS.getItemId() != blueDhideChapsId
						&& !ItemTable.BANK.containsAny(blueDhideChapsId)) {
					/*System.out.println("Missing chaps! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing chaps! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(blueDhideVambracesId)
						&& EquipmentSlot.HANDS.getItemId() != blueDhideVambracesId
						&& !ItemTable.BANK.containsAny(blueDhideVambracesId)) {
					/*System.out.println("Missing vambraces! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing vambraces! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(amuletOfPowerId)
						&& EquipmentSlot.NECK.getItemId() != amuletOfPowerId
						&& !ItemTable.BANK.containsAny(amuletOfPowerId)) {
					/*System.out.println("Missing amulet! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing amulet! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(runeArrowId)
						&& EquipmentSlot.AMMUNITION.getItemId() != runeArrowId
						&& !ItemTable.BANK.containsAny(runeArrowId)) {
					/*System.out.println("Missing arrows! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing arrows! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (shouldCheckCollectionBox) {
					if (Bank.get.close(true)) {
						Time.sleepMedium();
						
						if (!checkedCollectionBox && CollectionBox.get.open(true)) {
							Time.sleepMedium();
							
							if (ItemTable.GESLOT1.containsAny(lawRuneId) 
									|| ItemTable.GESLOT2.containsAny(lawRuneId)
									|| ItemTable.GESLOT3.containsAny(lawRuneId)
									|| ItemTable.GESLOT4.containsAny(lawRuneId)
									|| ItemTable.GESLOT5.containsAny(lawRuneId)
									|| ItemTable.GESLOT6.containsAny(lawRuneId)
									
									|| ItemTable.GESLOT1.containsAny(airRuneId) 
									|| ItemTable.GESLOT2.containsAny(airRuneId)
									|| ItemTable.GESLOT3.containsAny(airRuneId)
									|| ItemTable.GESLOT4.containsAny(airRuneId)
									|| ItemTable.GESLOT5.containsAny(airRuneId)
									|| ItemTable.GESLOT6.containsAny(airRuneId)
									
									|| ItemTable.GESLOT1.containsAny(allEquipmentIds) 
									|| ItemTable.GESLOT2.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT3.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT4.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT5.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT6.containsAny(allEquipmentIds)) {
								
								if (CollectionBox.get.collectAllToBank())
									checkedCollectionBox = true;
								Time.sleepQuick();
								CollectionBox.get.close();
								return false;
							} else {
								if (CollectionBox.get.collectAllToBank())
									checkedCollectionBox = true;
								Time.sleepQuick();
								CollectionBox.get.close();
							}
						}
					}
					
					
					if (checkedCollectionBox) {
						exchange = true;
						return true;
					}
				}
				
				if (!ItemTable.INVENTORY.containsAny(Daemonheim.ringOfKinshipId) && EquipmentSlot.RING.getItemId() != Daemonheim.ringOfKinshipId && !ItemTable.BANK.containsAny(Daemonheim.ringOfKinshipId)) {
					/*System.out.println("Missing ring! Stopping script...");
					interruptStop();
					return false;*/
					System.out.println("Missing ring! Retrieving from Daemonheim...");
					Bank.get.close(true);
					Time.sleepQuick();
					Daemonheim.retrieveRingOfKinship();
					return false;
				}
				
				try {
					Component.interactionAntiban = false;
					if (ItemTable.BANK.containsAny(lawRuneId)) {
						if (!Bank.get.withdrawAll(lawRuneId, false)) {
							Time.sleepQuick();
							return false;
						}
					}
					
					if (ItemTable.BANK.containsAny(airRuneId)) {
						if (!Bank.get.withdrawAll(airRuneId, false)) {
							Time.sleepQuick();
							return false;
						}
					}
					
					if (ItemTable.BANK.containsAny(runeArrowId)) {
						if (!Bank.get.withdrawAll(runeArrowId, false)) {
							Time.sleepQuick();
							return false;
						}
					}
				} finally {
					Component.interactionAntiban = true;
				}
				
				if (!retrieveEquipment()) {
					Time.sleepQuick();
					return false;
				}
				
				Bank.get.close();
				
				return true;
			}
		}/* else if (DepositBox.get.open()) {
			Time.sleepQuick();
			
			if (DepositBox.get.depositAllItems(staffOfAirId, lawRuneId, Daemonheim.ringOfKinshipId)) {
				Time.sleepVeryQuick();
				DepositBox.get.close();
				
				return true;
			}
		}*/

		
		return false;
	}
	
	boolean boughtEquipment = false;
	boolean retrievedEquipment = false;
	
	public boolean retrieveEquipment() {
		if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds)) {
			boughtEquipment = true;
			retrievedEquipment = true;
			return true;
		}
		
		if (!GrandExchangeWidget.get.close(true))
			return false;
		
		if (equip())
			return true;
		
		System.out.println("Retrieving equipment from bank...");
		
		if (Bank.get.open(true)) {
			Time.sleepMedium();
			
			if (ItemTable.BANK.containsAny(allEquipmentIds)) {
				try {
					Component.interactionAntiban = false;
					
					/*if (!ItemTable.INVENTORY.containsAny(staffOfAirId)
							&& EquipmentSlot.MAINHAND.getItemId() != staffOfAirId) {
						
						if (ItemTable.BANK.containsAny(staffOfAirId)) {
							if (!Bank.get.withdraw(staffOfAirId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}*/
					
					if (!ItemTable.INVENTORY.containsAny(greenDhideCoifId)
							&& EquipmentSlot.HEAD.getItemId() != greenDhideCoifId) {
						
						if (ItemTable.BANK.containsAny(greenDhideCoifId)) {
							if (!Bank.get.withdraw(greenDhideCoifId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(magicShieldbowId)
							&& EquipmentSlot.MAINHAND.getItemId() != magicShieldbowId) {
						
						if (ItemTable.BANK.containsAny(magicShieldbowId)) {
							if (!Bank.get.withdraw(magicShieldbowId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(greenDhideBodyId)
							&& EquipmentSlot.TORSO.getItemId() != greenDhideBodyId) {
						
						if (ItemTable.BANK.containsAny(greenDhideBodyId)) {
							if (!Bank.get.withdraw(greenDhideBodyId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(blueDhideChapsId)
							&& EquipmentSlot.LEGS.getItemId() != blueDhideChapsId) {
						
						if (ItemTable.BANK.containsAny(blueDhideChapsId)) {
							if (!Bank.get.withdraw(blueDhideChapsId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(blueDhideVambracesId)
							&& EquipmentSlot.HANDS.getItemId() != blueDhideVambracesId) {
						
						if (ItemTable.BANK.containsAny(blueDhideVambracesId)) {
							if (!Bank.get.withdraw(blueDhideVambracesId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(amuletOfPowerId)
							&& EquipmentSlot.NECK.getItemId() != amuletOfPowerId) {
						
						if (ItemTable.BANK.containsAny(amuletOfPowerId)) {
							if (!Bank.get.withdraw(amuletOfPowerId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (ItemTable.BANK.containsAny(runeArrowId)) {
						if (!Bank.get.withdrawAll(runeArrowId, false)) {
							Time.sleepQuick();
							return false;
						}
						Time.sleepMedium();
					}
					
					if (!ItemTable.INVENTORY.containsAny(Daemonheim.ringOfKinshipId)
							&& EquipmentSlot.RING.getItemId() != Daemonheim.ringOfKinshipId) {
						
						if (ItemTable.BANK.containsAny(Daemonheim.ringOfKinshipId)) {
							if (!Bank.get.withdraw(Daemonheim.ringOfKinshipId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
				} finally {
					Component.interactionAntiban = true;
				}
				
				Time.sleepMedium();
				
				if (ItemTable.INVENTORY.containsAny(allEquipmentIds)) {
					if (Bank.get.close(true)) {
						Time.sleepQuick();
						if (equip()) {
							boughtEquipment = true;
							retrievedEquipment = true;
							return true;
						}
					}
				}
			} else
				boughtEquipment = false;
		}
		
		return false;
	}

	public boolean equip() {
		if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds)) {
			boughtEquipment = true;
			retrievedEquipment = true;
			return true;
		}
		
		// Equip specified equipment in inventory
		if (ItemTable.INVENTORY.containsAny(allEquipmentIds)) {
			if (!Bank.get.close(true))
				return false;
			
			if (Ribbon.Tab.BACKPACK.open(true)) {
				try {
					Component.interactionAntiban = false;
					
					/*Component staff = InventoryWidget.get.getItem(staffOfAirId);
					if (staff != null && !staff.interact("Wield")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();*/
					
					Component coif = InventoryWidget.get.getItem(greenDhideCoifId);
					if (coif != null && !coif.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component bow = InventoryWidget.get.getItem(magicShieldbowId);
					if (bow != null && !bow.interact("Wield")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component body = InventoryWidget.get.getItem(greenDhideBodyId);
					if (body != null && !body.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component chaps = InventoryWidget.get.getItem(blueDhideChapsId);
					if (chaps != null && !chaps.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component vambraces = InventoryWidget.get.getItem(blueDhideVambracesId);
					if (vambraces != null && !vambraces.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component amulet = InventoryWidget.get.getItem(amuletOfPowerId);
					if (amulet != null && !amulet.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component arrows = InventoryWidget.get.getItem(runeArrowId);
					if (arrows != null && !arrows.interact("Wield")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component ring = InventoryWidget.get.getItem(Daemonheim.ringOfKinshipId);
					if (ring != null && !ring.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
				} finally {
					Component.interactionAntiban = true;
				}
				
				Time.sleepMedium();
				if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds))
					return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		/*failsafeWalkedToMine = new Failsafe("Walked to Mine", 1800000); // 30 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 1800000); // 30 mins.
		failsafeMinedRock = new Failsafe("Mine", 1800000); // 30 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 1800000); // 30 mins.*/
		failsafeWalkedToMine = new Failsafe("Walked to Mine", 3600000); // 1 hr.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 3600000); // 1 hr.
		failsafeMinedRock = new Failsafe("Mine", 3600000); // 1 hr.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 3600000); // 1 hr.
		failsafes.add(failsafeWalkedToMine);
		failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		// custom script breaking is used instead (ran 1 account with custom breaking and it survived, the account with regular breaking was banned)
		breakHandlerParam = new BreakHandlerParam(failsafes);
		params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		boolean success = false;
		/*if (AccountInfo.cur != null) {
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Map<String, String> params = row.getScriptParams_toMap();
				String oreType = params.get("OreType");
				if (oreType != null) {
					success = true;
					
					oreType = oreType.toLowerCase();
					if (oreType.contains("copper"))
						OreType.setCurrent(OreType.COPPER);
					else if (oreType.contains("iron"))
						OreType.setCurrent(OreType.IRON);
					else if (oreType.contains("auto"))
						autoMode = true;
					else
						success = false;
				}
				
				if (success) {
					String mineLocation = params.get("MineLocation");
					if (mineLocation != null) {
						success = true;
						
						mineLocation = mineLocation.toLowerCase();
						if (mineLocation.contains("rimmington"))
							MineLocation.setCurrent(MineLocation.RIMMINGTON);
						else if (mineLocation.contains("auto"))
							autoMode = true;
						else
							success = false;
					}
				}
			}
		}*/
		
		success = true;
		
		if (!success) {
			System.out.println("WineCollector: Error loading params! Loaded default data params.");
			//OreType.setCurrent(OreType.IRON);
			//MineLocation.setCurrent(MineLocation.RIMMINGTON);
			//autoMode = true;
		} else {
			System.out.println("WineCollector: Successfully loaded data params: ");
		}
		
		/*if (!autoMode) {
			System.out.println("\t-OreType: " + OreType.getCurrent().name());
			System.out.println("\t-MineLocation: " + MineLocation.getCurrent().name());
		} else
			System.out.println("\t-Auto mode enabled");*/
	}
	
	public int getWinePerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int totalWine = wineAccumulated;
			int oresPerHour = (int) (totalWine / timeDiffHours);
			return oresPerHour;
		}
		
		return 0;
	}
	
	public int getGpPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int wineGp = wineAccumulated*8800;
			int gpPerHour = (int) (wineGp / timeDiffHours);
			return gpPerHour;
		}
		
		return 0;
	}
	
	@Override
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("-Wine Collector-");
		sb.append(System.lineSeparator());
		
		if (wineAccumulated > 0) {
			sb.append("Wine accumulated: " + wineAccumulated);
			sb.append(System.lineSeparator());
		}
		
		sb.append("Wine/hr: " + getWinePerHour());
		sb.append(System.lineSeparator());
		
		sb.append("GP/hr: " + getGpPerHour() / 1000 + "k");
		sb.append(System.lineSeparator());
		
		return sb.toString();
	}
	
	@Override
	public boolean onStart() {
		
		//wineModel = null;
		
		breakTimer = new Timer(Random.nextInt(600000, 900000));
		
		wineAccumulated = 0;
		lastWineGrabTime = 0;
		durationLastGrab = 0;
		
		ItemTableCallback.addListener(itemTableListener);
		MenuCallback.addListener(menuListener);
		
		loadDataParams();
		
		Antiban.resetTimer();
		
		return true;
	}
	
	
	/*
	 * Script Notes
	 * 
	 */
	
	@Override
	public void run() {
		Antiban.randomMouseSpeed();
		
		/*if (!breakTimer.isRunning()) {
			System.out.println("Taking short 2 to 5 min. break...");
			
			if (Random.nextBoolean())
				Client.logout(true);
			
			Time.sleep(120000, 300000); // 2 to 5 mins
			
			breakTimer = new Timer(Random.nextInt(600000, 900000));
		}*/
		
		if (!Client.loopLogin())
			return;
		
		if (Skill.MAGIC.getRealLevel() == -1)
			return;
		
		// 33 is requirement for spell, but should have 40 so you can at least take a few wines off the table directly (with no spell)
		//if (Skill.MAGIC.getRealLevel() < 33) {
		if (Skill.MAGIC.getRealLevel() < 40) {
			// double check
			Time.sleep(10000);
			if (Skill.MAGIC.getRealLevel() < 40) {
				System.out.println("Magic level is not at least 40! Stopping script...");
				System.out.println("Real magic level: " + Skill.MAGIC.getRealLevel());
				interruptStop();
				return;
			}
		}
		
		if (Client.isUsableComponentSelected()) {
			if (!Spell.cancelUsableComponentSelection())
				return;
			Time.sleepQuick();
		}
		
		if (Daemonheim.isInDungeon()) {
			if (Daemonheim.exitDungeon()) {
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
				
				Time.sleepMedium();
			}
		}
		
		if (!Daemonheim.jumpDownBrokenStairs())
			return;
		
		if (Skill.MAGIC.getCurrentLevel() < 35
				|| (Daemonheim.isNearDaemonheimEntrance() && Skill.MAGIC.getCurrentLevel() < Skill.MAGIC.getRealLevel())) {
			System.out.println("Magic level is not at least 35! Restoring magic level...");
			
			if (Daemonheim.enterDungeon()) {
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
				
				Time.sleepMedium();
			}
			
			return;
		}
		
		/*if (autoMode) {
			if (!autoModeTimer.isRunning() || OreType.getCurrent() == null) {
				if (Skill.MINING.getCurrentLevel() >= 41) // 15 is required but with competition, 41 is better (rune pick)
					OreType.setCurrent(OreType.IRON);
				else
					OreType.setCurrent(OreType.COPPER);
				
				MineLocation.setCurrent(MineLocation.RIMMINGTON);
				
				autoModeTimer.reset();
			}
		}*/
		
		/*if (!exchange && !exchangeLoop.getMainExchangeTimer().isRunning()) {
			exchangeLoop.setMainExchangePeriod(Random.nextInt(10800000, 18000000)); // 3 to 5 hrs
		}*/
		
		if (!DepositBox.get.close(true))
			return;
		
		if (CollectionBox.get.isOpen()) {
			CollectionBox.get.collectAllToBank();
			Time.sleepQuick();
			CollectionBox.get.close();
			return;
		}
		
		if (!exchange) {
			if (!Bank.get.close(true))
				return;
			
			if (!GrandExchangeWidget.get.close(true))
				return;
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		equip();
		
		// TODO: get ring from tutor if inventory, equipment, and bank don't have it
		if (EquipmentSlot.RING.getItemId() != Daemonheim.ringOfKinshipId) {
			if (Daemonheim.equipRing())
				Time.sleepQuick();
		}
		
		/*if (EquipmentSlot.MAINHAND.getItemId() == staffOfAirId
				&& MainWidget.get.isWeaponSheathed()) {
			if (!MainWidget.get.unsheatheWeapon(true))
				return;
			Time.sleepQuick();
		}*/
		
		if (EquipmentSlot.MAINHAND.getItemId() == magicShieldbowId
				&& MainWidget.get.isWeaponSheathed()) {
			if (!MainWidget.get.unsheatheWeapon(true))
				return;
			Time.sleepQuick();
		}
		
		if (!ActionBarWidget.isAutoRetaliateEnabled()) {
			if (!ActionBarWidget.get.setAutoRetaliate(true, true))
				return;
			Time.sleepQuick();
		}
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!Lodestone.FALADOR.isActivated()) {
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			Lodestone.FALADOR.activate();
			Time.sleepMedium();
			return;
		}
		
		if (!CombatMode.REVOLUTION.activate(true))
			return;
		if (!ActionBar.setup(AttackStyle.RANGED.getActionBarIndex(), new Ability[] { Ability.RICOCHET, Ability.FRAGMENTATION_SHOT, Ability.PIERCING_SHOT, Ability.BINDING_SHOT, Ability.ANTICIPATION, Ability.FREEDOM, Ability.REJUVENATE, Ability.EMPTY, Ability.EMPTY }))
			return;
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}
		
		if (!ActionBar.setActionBar(AttackStyle.RANGED.getActionBarIndex()))
			return;
		
		if (!Settings_Gameplay_LootSettingsSubTabWidget.get.disableLootInventory(true))
			return;
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}
		
		ActionSlot spellSlot = ActionSlot.getSlot(Ability.TELEKINETIC_GRAB);
		if (spellSlot == null || spellSlot.getIndex() < 10 || spellSlot.getIndex() >= 13) {
			//spellSlot = ActionSlot.getRandomEmptySlot();
			spellSlot = ActionSlot.getSlot(Random.nextInt(10, 13));
			if (spellSlot == null) {
				//spellSlot = ActionSlot.getRandomSlot();
				spellSlot = ActionSlot.getSlot(Random.nextInt(10, 13));
			}
			if (spellSlot == null) // should never happen
				return;
			
			if (spellSlot != null) {
				if (!spellSlot.setAbility(Ability.TELEKINETIC_GRAB, true))
					return;
				Time.sleepQuick();
			}
		}
		
		if (spellSlot != null) {
			if (!ActionBar.setActionBar(spellSlot.getBarIndex()))
				return;
		}
		
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}
		
		Action action = Action.getNextAction();
		SubAction subAction = action.getSubAction();
		System.out.println(action + ", " + subAction);
		
		switch (action) {
		case COLLECT:
			switch (subAction) {
			case DEFAULT:
				if (collect()) {
					checkedCollectionBox = false;
					failsafeMinedRock.reset();
				}
				
				break;
			case WALK:
				if (faladorLStoCapturedTemplePath.getDistanceToNearest() < 25) {
					if (Random.nextInt(0, 100) <= 75) {
						if (Random.nextBoolean())
							Time.sleepMedium();
						else
							Time.sleepQuick();
					}
					
					TilePath path = faladorLStoCapturedTemplePath.randomize(4, 4);
					if (path.loopWalk(90000, handledBreak, true)) // 90s max
						failsafeWalkedToMine.reset();
				} else {
					if (Lodestone.FALADOR.teleport(true)) {
						
					}
				}
				
				break;
			}
			
			break;
		case BANK:
			switch (action.getSubAction()) {
			case DEFAULT:
				lastWineGrabTime = 0;
				
				if (bank()) {
					failsafeDropOrBank.reset();
					failsafeWalkedToBank.reset();
				} else {
					Time.sleepMedium();
					if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds) && ItemTable.INVENTORY.containsAll(lawRuneId, airRuneId)) {
						failsafeDropOrBank.reset();
						failsafeWalkedToBank.reset();
					}
				}
				
				break;
			case WALK:
				runToMonkSafeZoneIfUnderAttack();
				
				/*if (Lodestone.BURTHORPE.teleport(true)) {
					failsafeWalkedToBank.reset();
				}*/
				
				if (Daemonheim.walkToDaemonheimEntrance()) {
					Time.sleepQuick();
					failsafeWalkedToBank.reset();
				}
				
				break;
			}
			
			break;
		case EXCHANGE:
			switch (action.getSubAction()) {
			case DEFAULT:
				lastWineGrabTime = 0;
				
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
				
				if (exchangeLoop.exchange()) {
					int[] buyEquipmentIdsNoted = new int[buyEquipmentIds.length];
					for (int i = 0; i < buyEquipmentIdsNoted.length; i++) {
						buyEquipmentIdsNoted[i] = buyEquipmentIds[i] + 1;
					}
					
					// break if need to equip inv items
					if (ItemTable.INVENTORY.containsAny(ArrayUtils.combine(buyEquipmentIds, buyEquipmentIdsNoted)))
						break;
					
					if (ExchangeLoop.buyEquipment(65000, new ExchangeItem[] {
							//new ExchangeItem(staffOfAirId, staffOfAirName, false).setPrice(5000),
							new ExchangeItem(greenDhideCoifId, greenDhideCoifName, false).setPrice(10000),
							new ExchangeItem(magicShieldbowId, magicShieldbowName, false).setPrice(10000),
							new ExchangeItem(greenDhideBodyId, greenDhideBodyName, false).setPrice(10000),
							new ExchangeItem(blueDhideChapsId, blueDhideChapsName, false).setPrice(10000),
							new ExchangeItem(blueDhideVambracesId, blueDhideVambracesName, false).setPrice(10000),
							new ExchangeItem(amuletOfPowerId, amuletOfPowerName, false).setPrice(10000),
							})) {
						
						//if (boughtEquipment && !retrieveEquipment())
						//	break;
						
						if (!retrieveEquipment())
							break;
						
						System.out.println("Successfully bought all equipment");
						exchange = false;
					}
				}
				
				break;
			case WALK:
				if (Bank.get.isOpen())
					Bank.get.close(true);
				
				if (GrandExchange.walkToSWBooth()) {
					
				}
				
				break;
			}
			
			break;
		}
		
		//Time.sleep(50);
		
		//System.gc();
	}
	
	@Override
	public boolean onStop() {
		waitForMenuOption = false;
		
		ItemTableCallback.removeListener(itemTableListener);
		MenuCallback.removeListener(menuListener);
		
		System.out.println("Final Wine/hr: " + getWinePerHour());
		System.out.println("Final GP/hr: " + getGpPerHour() + " (" + getGpPerHour() / 1000 + "k)");
		
		return true;
	}
}
