package org.rs3.scripts.combat;

import org.rs3.api.Calculations;
import org.rs3.api.GroundObjects;
import org.rs3.api.Lodestone;
import org.rs3.api.Paths;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Player;
import org.rs3.util.Condition;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public enum CombatLocation {
	BURTHOPE_TROLLS(
			new Area(new Tile(2174, 4417, 0), new Tile(2240, 4355, 0)) // area
	),
	
	AL_KHARID_PALACE(
			new Area(new Tile(3277, 3179, 0), new Tile(3309, 3139, 0)) // area
	),
	
	STRONGHOLD_SECURITY_FLOOR1(
			new Area(new Tile(1852, 5249, 0), new Tile(1921, 5179, 0)) // area
	),
	
	STRONGHOLD_SECURITY_FLOOR2(
			new Area(new Tile(1979, 5251, 0), new Tile(2051, 5181, 0)) // area
	),
	
	STRONGHOLD_SECURITY_FLOOR3(
			new Area(new Tile(2103, 5324, 0), new Tile(2192, 5248, 0)) // area
	),
	
	STRONGHOLD_SECURITY_FLOOR4(
			new Area(new Tile(2300, 5250, 0), new Tile(2369, 5180, 0)) // area TODO: double check all area is covered
	),
	
	ASGARNIAN_ICE_DUNGEON(
			new Area(new Tile(2981, 9604, 0), new Tile(3071, 9534, 0)) // area
	),
	;
	
	private Area area;
	
	private static CombatLocation cur;
	
	public static CombatLocation getCurrent() {
		return cur;
	}
	
	public static void setCurrent(CombatLocation combatLocation) {
		cur = combatLocation;
	}
	
	CombatLocation(Area area) {
		this.area = area;
	}
	
	public Area getArea() {
		return area;
	}
	
	
	public boolean walkToArea(Condition breakCondition, boolean doAntiban) {
		if (area.getDistanceToNearest() < 5)
			return true;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		switch(this) {
		case BURTHOPE_TROLLS:
			if (Paths.burthorpeLStoTrollCave.getDistanceToNearest() <= 25 || Lodestone.BURTHORPE.teleport(true)) {
				if (Paths.burthorpeLStoTrollCave.randomize(4, 4).loopWalk(25000, breakCondition, doAntiban)) {
					Time.sleepQuick();
					
					GroundObject caveEntrance = GroundObjects.getNearest(Type.All, 66533); // cave entrance
					if (caveEntrance != null && caveEntrance.loopInteract(breakCondition, "Enter")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(7000);
						while (timer.isRunning()) {
							if (this.area.contains(player.getTile())) {
								Time.sleepQuick();
								return true;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
			
			return false;
		case AL_KHARID_PALACE:
			if (Lodestone.AL_KHARID.teleport(true)) {
				if (this.area.loopWalkTo(10000)) {
					Time.sleepQuick();
					return true;
				}
			}
			
			return false;
		case STRONGHOLD_SECURITY_FLOOR1:
			if (Paths.edgevilleLStoStrongholdSecurityEntrance.getDistanceToNearest() <= 25 || Lodestone.EDGEVILLE.teleport(true)) {
				if (Paths.edgevilleLStoStrongholdSecurityEntrance.randomize(4, 4).loopWalk(40000, breakCondition, doAntiban)) {
					Time.sleepQuick();
					
					GroundObject entrance = GroundObjects.getNearest(Type.All, 16154); // entrance
					if (entrance != null && entrance.loopInteract(breakCondition, "Climb-down")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(7000);
						while (timer.isRunning()) {
							if (this.area.contains(player.getTile())) {
								Time.sleepQuick();
								return true;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
			
			return false;
		case STRONGHOLD_SECURITY_FLOOR2:
			if (STRONGHOLD_SECURITY_FLOOR1.walkToArea(breakCondition, doAntiban)) {
				Time.sleepQuick();
				
				GroundObject portal = GroundObjects.getNearest(Type.All, 16150); // Portal
				if (portal != null && Calculations.distanceTo(portal) < 15 && portal.loopInteract(breakCondition, "Enter")) {
					Time.sleepQuick();
					
					Timer timer = new Timer(5000);
					while (timer.isRunning()) {
						GroundObject giftOfPeace = GroundObjects.getNearest(Type.All, 16135); // Gift of Peace
						if (giftOfPeace != null && Calculations.distanceTo(giftOfPeace) < 15) {
							Time.sleepQuick();
							break;
						}
						
						Time.sleep(50);
					}
				}
				
				
				GroundObject giftOfPeace = GroundObjects.getNearest(Type.All, 16135); // Gift of Peace
				if (giftOfPeace != null && Calculations.distanceTo(giftOfPeace) < 15) {
					GroundObject ladder = GroundObjects.getNearest(Type.All, 16149); // Ladder
					if (ladder != null && ladder.loopInteract(breakCondition, "Climb-down")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(7000);
						while (timer.isRunning()) {
							if (this.area.contains(player.getTile())) {
								Time.sleepQuick();
								return true;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
			
			return false;
		case STRONGHOLD_SECURITY_FLOOR3:
			if (STRONGHOLD_SECURITY_FLOOR2.walkToArea(breakCondition, doAntiban)) {
				Time.sleepQuick();
				
				GroundObject portal = GroundObjects.getNearest(Type.All, 16082); // Portal
				if (portal != null && Calculations.distanceTo(portal) < 15 && portal.loopInteract(breakCondition, "Enter")) {
					Time.sleepQuick();
					
					Timer timer = new Timer(5000);
					while (timer.isRunning()) {
						GroundObject grainOfPlenty = GroundObjects.getNearest(Type.All, 16077); // Grain of Plenty
						if (grainOfPlenty != null && Calculations.distanceTo(grainOfPlenty) < 15) {
							Time.sleepQuick();
							break;
						}
						
						Time.sleep(50);
					}
				}
				
				
				GroundObject grainOfPlenty = GroundObjects.getNearest(Type.All, 16077); // Grain of Plenty
				if (grainOfPlenty != null && Calculations.distanceTo(grainOfPlenty) < 15) {
					GroundObject ladder = GroundObjects.getNearest(Type.All, 16081); // Ladder
					if (ladder != null && ladder.loopInteract(breakCondition, "Climb-down")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(7000);
						while (timer.isRunning()) {
							if (this.area.contains(player.getTile())) {
								Time.sleepQuick();
								return true;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
			
			return false;
		case STRONGHOLD_SECURITY_FLOOR4:
			if (STRONGHOLD_SECURITY_FLOOR3.walkToArea(breakCondition, doAntiban)) {
				Time.sleepQuick();
				
				GroundObject portal = GroundObjects.getNearest(Type.All, 16116); // Portal
				if (portal != null && Calculations.distanceTo(portal) < 15 && portal.loopInteract(breakCondition, "Enter")) {
					Time.sleepQuick();
					
					Timer timer = new Timer(5000);
					while (timer.isRunning()) {
						GroundObject boxOfHealth = GroundObjects.getNearest(Type.All, 16118); // Box of Health
						if (boxOfHealth != null && Calculations.distanceTo(boxOfHealth) < 15) {
							Time.sleepQuick();
							break;
						}
						
						Time.sleep(50);
					}
				}
				
				
				GroundObject boxOfHealth = GroundObjects.getNearest(Type.All, 16118); // Box of Health
				if (boxOfHealth != null && Calculations.distanceTo(boxOfHealth) < 15) {
					GroundObject drippingVine = GroundObjects.getNearest(Type.All, 16115); // Dripping vine
					if (drippingVine != null && drippingVine.loopInteract(breakCondition, "Climb-down")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(7000);
						while (timer.isRunning()) {
							if (this.area.contains(player.getTile())) {
								Time.sleepQuick();
								return true;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
			
			return false;
		case ASGARNIAN_ICE_DUNGEON:
			if (Paths.portSarimLStoAsgarnianIceDungeonTrapdoor.getDistanceToNearest() <= 25 || Lodestone.PORT_SARIM.teleport(true)) {
				if (Paths.portSarimLStoAsgarnianIceDungeonTrapdoor.randomize(4, 4).loopWalk(25000, breakCondition, doAntiban)) {
					Time.sleepQuick();
					
					GroundObject trapdoor = GroundObjects.getNearest(Type.All, 9472); // cave entrance
					if (trapdoor != null && trapdoor.loopInteract(breakCondition, "Climb-down")) {
						Time.sleepQuick();
						
						Timer timer = new Timer(10000);
						while (timer.isRunning()) {
							if (this.area.contains(player.getTile())) {
								Time.sleepQuick();
								return true;
							}
							
							Time.sleep(50);
						}
					}
				}
			}
			
			return false;
		default:
			return false;
		}
	}
	
	public boolean walkToBank(Condition breakCondition, boolean doAntiban) {
		Interactable bank = Bank.get.getNearest();
		if (bank != null && Calculations.distanceTo(bank) <= 3)
			return true;
		
		switch(this) {
		case BURTHOPE_TROLLS:
			if ((bank != null && Calculations.distanceTo(bank) <= 25)
					|| Lodestone.BURTHORPE.teleport(true)) {
				
				bank = Bank.get.getNearest();
				if (bank == null)
					return false;
				return bank.loopWalkTo(15000);
			}
			return false;
		case AL_KHARID_PALACE:
			if (bank == null)
				return false;
			return bank.loopWalkTo(15000);
		case STRONGHOLD_SECURITY_FLOOR1:
		case STRONGHOLD_SECURITY_FLOOR2:
		case STRONGHOLD_SECURITY_FLOOR3:
		case STRONGHOLD_SECURITY_FLOOR4:
			if ((bank != null && Calculations.distanceTo(bank) <= 25)
					|| Lodestone.BURTHORPE.teleport(true)) {
				
				bank = Bank.get.getNearest();
				if (bank == null)
					return false;
				return bank.loopWalkTo(15000);
			}
			return false;
		case ASGARNIAN_ICE_DUNGEON:
			if ((bank != null && Calculations.distanceTo(bank) <= 25)
					|| Lodestone.BURTHORPE.teleport(true)) {
				
				bank = Bank.get.getNearest();
				if (bank == null)
					return false;
				return bank.loopWalkTo(15000);
			}
			return false;
		default:
			return false;
		}
	}
}
