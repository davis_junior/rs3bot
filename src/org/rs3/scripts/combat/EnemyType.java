package org.rs3.scripts.combat;

import org.rs3.api.Lodestone;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.Tile;

public enum EnemyType {
	TROLL_BRUTE(
			Lodestone.BURTHORPE, // closest lodestone
			new Area( new Tile(2181, 4382, 0), new Tile(2182, 4382, 0), new Tile(2182, 4383, 0), new Tile(2182, 4384, 0), new Tile(2183, 4384, 0), new Tile(2183, 4385, 0), new Tile(2184, 4385, 0), new Tile(2184, 4386, 0), new Tile(2185, 4386, 0), new Tile(2185, 4385, 0), new Tile(2186, 4385, 0), new Tile(2186, 4384, 0), new Tile(2187, 4384, 0), new Tile(2187, 4383, 0), new Tile(2187, 4382, 0), new Tile(2187, 4381, 0), new Tile(2187, 4380, 0), new Tile(2188, 4380, 0), new Tile(2189, 4380, 0), new Tile(2190, 4380, 0), new Tile(2191, 4380, 0), new Tile(2191, 4381, 0), new Tile(2192, 4381, 0), new Tile(2192, 4380, 0), new Tile(2192, 4379, 0), new Tile(2192, 4378, 0), new Tile(2193, 4378, 0), new Tile(2194, 4378, 0), new Tile(2195, 4378, 0), new Tile(2196, 4378, 0), new Tile(2196, 4379, 0), new Tile(2196, 4380, 0), new Tile(2196, 4381, 0), new Tile(2197, 4381, 0), new Tile(2198, 4381, 0), new Tile(2198, 4380, 0), new Tile(2198, 4379, 0), new Tile(2198, 4378, 0), new Tile(2199, 4378, 0), new Tile(2199, 4377, 0), new Tile(2199, 4376, 0), new Tile(2198, 4376, 0), new Tile(2198, 4375, 0), new Tile(2197, 4375, 0), new Tile(2197, 4374, 0), new Tile(2197, 4373, 0), new Tile(2197, 4372, 0), new Tile(2197, 4371, 0), new Tile(2198, 4371, 0), new Tile(2198, 4370, 0), new Tile(2198, 4369, 0), new Tile(2198, 4368, 0), new Tile(2198, 4367, 0), new Tile(2199, 4367, 0), new Tile(2199, 4366, 0), new Tile(2199, 4365, 0), new Tile(2198, 4365, 0), new Tile(2198, 4364, 0), new Tile(2197, 4364, 0), new Tile(2197, 4363, 0), new Tile(2197, 4362, 0), new Tile(2196, 4362, 0), new Tile(2196, 4361, 0), new Tile(2195, 4361, 0), new Tile(2194, 4361, 0), new Tile(2194, 4360, 0), new Tile(2194, 4359, 0), new Tile(2193, 4359, 0), new Tile(2192, 4359, 0), new Tile(2192, 4360, 0), new Tile(2192, 4361, 0), new Tile(2191, 4361, 0), new Tile(2190, 4361, 0), new Tile(2190, 4362, 0), new Tile(2189, 4362, 0), new Tile(2189, 4363, 0), new Tile(2189, 4364, 0), new Tile(2189, 4365, 0), new Tile(2189, 4366, 0), new Tile(2189, 4367, 0), new Tile(2188, 4367, 0), new Tile(2187, 4367, 0), new Tile(2186, 4367, 0), new Tile(2185, 4367, 0), new Tile(2185, 4366, 0), new Tile(2184, 4366, 0), new Tile(2184, 4367, 0), new Tile(2183, 4367, 0), new Tile(2183, 4368, 0), new Tile(2182, 4368, 0), new Tile(2182, 4369, 0), new Tile(2182, 4370, 0), new Tile(2182, 4371, 0), new Tile(2181, 4371, 0), new Tile(2181, 4372, 0), new Tile(2181, 4373, 0), new Tile(2180, 4373, 0), new Tile(2180, 4374, 0), new Tile(2180, 4375, 0), new Tile(2179, 4375, 0), new Tile(2179, 4376, 0), new Tile(2180, 4376, 0), new Tile(2181, 4376, 0), new Tile(2181, 4377, 0), new Tile(2181, 4378, 0), new Tile(2181, 4379, 0), new Tile(2180, 4379, 0), new Tile(2180, 4380, 0), new Tile(2181, 4380, 0), new Tile(2181, 4381, 0) ), // area
			"Troll brute", // name
			new int[] { 14980 }, // npc ids
			13785 // death animation id
	),
	
	TROLL_CHUCKER(
			Lodestone.BURTHORPE, // closest lodestone
			new Area( new Tile(2205, 4410, 0), new Tile(2206, 4410, 0), new Tile(2206, 4411, 0), new Tile(2207, 4411, 0), new Tile(2208, 4411, 0), new Tile(2208, 4410, 0), new Tile(2209, 4410, 0), new Tile(2210, 4410, 0), new Tile(2211, 4410, 0), new Tile(2212, 4410, 0), new Tile(2213, 4410, 0), new Tile(2214, 4410, 0), new Tile(2214, 4409, 0), new Tile(2215, 4409, 0), new Tile(2215, 4408, 0), new Tile(2216, 4408, 0), new Tile(2217, 4408, 0), new Tile(2217, 4407, 0), new Tile(2218, 4407, 0), new Tile(2219, 4407, 0), new Tile(2219, 4406, 0), new Tile(2220, 4406, 0), new Tile(2219, 4405, 0), new Tile(2218, 4405, 0), new Tile(2217, 4405, 0), new Tile(2217, 4404, 0), new Tile(2217, 4403, 0), new Tile(2216, 4403, 0), new Tile(2216, 4402, 0), new Tile(2215, 4402, 0), new Tile(2214, 4402, 0), new Tile(2214, 4401, 0), new Tile(2214, 4400, 0), new Tile(2214, 4399, 0), new Tile(2213, 4399, 0), new Tile(2213, 4398, 0), new Tile(2213, 4397, 0), new Tile(2213, 4396, 0), new Tile(2213, 4395, 0), new Tile(2213, 4394, 0), new Tile(2214, 4394, 0), new Tile(2214, 4393, 0), new Tile(2214, 4392, 0), new Tile(2215, 4392, 0), new Tile(2215, 4391, 0), new Tile(2215, 4390, 0), new Tile(2214, 4390, 0), new Tile(2214, 4389, 0), new Tile(2214, 4388, 0), new Tile(2213, 4388, 0), new Tile(2213, 4387, 0), new Tile(2213, 4386, 0), new Tile(2213, 4385, 0), new Tile(2212, 4385, 0), new Tile(2212, 4384, 0), new Tile(2211, 4384, 0), new Tile(2210, 4384, 0), new Tile(2209, 4384, 0), new Tile(2208, 4384, 0), new Tile(2209, 4385, 0), new Tile(2210, 4385, 0), new Tile(2210, 4386, 0), new Tile(2210, 4387, 0), new Tile(2210, 4388, 0), new Tile(2210, 4389, 0), new Tile(2209, 4389, 0), new Tile(2208, 4389, 0), new Tile(2207, 4389, 0), new Tile(2207, 4390, 0), new Tile(2206, 4390, 0), new Tile(2205, 4390, 0), new Tile(2204, 4390, 0), new Tile(2203, 4390, 0), new Tile(2202, 4390, 0), new Tile(2201, 4390, 0), new Tile(2200, 4390, 0), new Tile(2200, 4391, 0), new Tile(2200, 4392, 0), new Tile(2199, 4392, 0), new Tile(2199, 4393, 0), new Tile(2198, 4393, 0), new Tile(2198, 4394, 0), new Tile(2197, 4394, 0), new Tile(2197, 4395, 0), new Tile(2197, 4396, 0), new Tile(2196, 4396, 0), new Tile(2197, 4397, 0), new Tile(2198, 4397, 0), new Tile(2199, 4397, 0), new Tile(2199, 4398, 0), new Tile(2199, 4399, 0), new Tile(2200, 4399, 0), new Tile(2200, 4400, 0), new Tile(2200, 4401, 0), new Tile(2200, 4402, 0), new Tile(2200, 4403, 0), new Tile(2200, 4404, 0), new Tile(2199, 4404, 0), new Tile(2200, 4405, 0), new Tile(2201, 4405, 0), new Tile(2201, 4406, 0), new Tile(2202, 4406, 0), new Tile(2203, 4406, 0), new Tile(2204, 4406, 0), new Tile(2205, 4406, 0), new Tile(2205, 4407, 0), new Tile(2206, 4407, 0), new Tile(2206, 4408, 0), new Tile(2206, 4409, 0) ), // area
			"Troll chucker", // name
			new int[] { 14981 }, // npc ids
			13785 // death animation id
	),
	
	TROLL_SHAMAN(
			Lodestone.BURTHORPE, // closest lodestone
			new Area( new Tile(2224, 4393, 0), new Tile(2225, 4393, 0), new Tile(2226, 4393, 0), new Tile(2226, 4394, 0), new Tile(2227, 4394, 0), new Tile(2227, 4395, 0), new Tile(2227, 4396, 0), new Tile(2228, 4396, 0), new Tile(2228, 4395, 0), new Tile(2228, 4394, 0), new Tile(2229, 4394, 0), new Tile(2229, 4393, 0), new Tile(2230, 4393, 0), new Tile(2231, 4393, 0), new Tile(2231, 4392, 0), new Tile(2232, 4392, 0), new Tile(2233, 4392, 0), new Tile(2234, 4392, 0), new Tile(2234, 4391, 0), new Tile(2235, 4391, 0), new Tile(2236, 4391, 0), new Tile(2236, 4390, 0), new Tile(2235, 4390, 0), new Tile(2235, 4389, 0), new Tile(2234, 4389, 0), new Tile(2234, 4388, 0), new Tile(2234, 4387, 0), new Tile(2234, 4386, 0), new Tile(2234, 4385, 0), new Tile(2235, 4385, 0), new Tile(2235, 4384, 0), new Tile(2235, 4383, 0), new Tile(2235, 4382, 0), new Tile(2235, 4381, 0), new Tile(2235, 4380, 0), new Tile(2235, 4379, 0), new Tile(2236, 4379, 0), new Tile(2236, 4378, 0), new Tile(2235, 4378, 0), new Tile(2235, 4377, 0), new Tile(2235, 4376, 0), new Tile(2235, 4375, 0), new Tile(2235, 4374, 0), new Tile(2236, 4374, 0), new Tile(2236, 4373, 0), new Tile(2235, 4373, 0), new Tile(2235, 4372, 0), new Tile(2234, 4372, 0), new Tile(2234, 4371, 0), new Tile(2233, 4371, 0), new Tile(2233, 4370, 0), new Tile(2233, 4369, 0), new Tile(2232, 4369, 0), new Tile(2231, 4369, 0), new Tile(2231, 4370, 0), new Tile(2231, 4371, 0), new Tile(2230, 4371, 0), new Tile(2230, 4372, 0), new Tile(2230, 4373, 0), new Tile(2229, 4373, 0), new Tile(2228, 4373, 0), new Tile(2227, 4373, 0), new Tile(2227, 4374, 0), new Tile(2226, 4374, 0), new Tile(2225, 4374, 0), new Tile(2224, 4374, 0), new Tile(2223, 4374, 0), new Tile(2222, 4374, 0), new Tile(2221, 4374, 0), new Tile(2221, 4375, 0), new Tile(2222, 4375, 0), new Tile(2222, 4376, 0), new Tile(2223, 4376, 0), new Tile(2223, 4377, 0), new Tile(2223, 4378, 0), new Tile(2223, 4379, 0), new Tile(2223, 4380, 0), new Tile(2223, 4381, 0), new Tile(2222, 4381, 0), new Tile(2222, 4382, 0), new Tile(2222, 4383, 0), new Tile(2221, 4383, 0), new Tile(2221, 4384, 0), new Tile(2220, 4384, 0), new Tile(2220, 4385, 0), new Tile(2220, 4386, 0), new Tile(2220, 4387, 0), new Tile(2221, 4387, 0), new Tile(2221, 4388, 0), new Tile(2221, 4389, 0), new Tile(2222, 4389, 0), new Tile(2222, 4390, 0), new Tile(2222, 4391, 0), new Tile(2223, 4391, 0), new Tile(2223, 4392, 0), new Tile(2224, 4392, 0) ), // area
			"Troll shaman", // name
			new int[] { 14982 }, // npc ids
			13785 // death animation id
	),
	
	AL_KHARID_WARRIOR(
			Lodestone.AL_KHARID, // closest lodestone
			new Area( new Tile(3290, 3180, 0), new Tile(3291, 3180, 0), new Tile(3292, 3180, 0), new Tile(3293, 3180, 0), new Tile(3294, 3180, 0), new Tile(3295, 3180, 0), new Tile(3295, 3179, 0), new Tile(3295, 3178, 0), new Tile(3295, 3177, 0), new Tile(3295, 3176, 0), new Tile(3295, 3175, 0), new Tile(3295, 3174, 0), new Tile(3296, 3174, 0), new Tile(3297, 3174, 0), new Tile(3298, 3174, 0), new Tile(3298, 3175, 0), new Tile(3299, 3175, 0), new Tile(3300, 3175, 0), new Tile(3301, 3175, 0), new Tile(3301, 3174, 0), new Tile(3302, 3174, 0), new Tile(3303, 3174, 0), new Tile(3303, 3173, 0), new Tile(3303, 3172, 0), new Tile(3303, 3171, 0), new Tile(3303, 3170, 0), new Tile(3303, 3169, 0), new Tile(3303, 3168, 0), new Tile(3303, 3167, 0), new Tile(3303, 3166, 0), new Tile(3303, 3165, 0), new Tile(3303, 3164, 0), new Tile(3302, 3164, 0), new Tile(3301, 3164, 0), new Tile(3300, 3164, 0), new Tile(3299, 3164, 0), new Tile(3298, 3164, 0), new Tile(3297, 3164, 0), new Tile(3296, 3164, 0), new Tile(3295, 3164, 0), new Tile(3294, 3164, 0), new Tile(3293, 3164, 0), new Tile(3292, 3164, 0), new Tile(3291, 3164, 0), new Tile(3290, 3164, 0), new Tile(3289, 3164, 0), new Tile(3288, 3164, 0), new Tile(3287, 3164, 0), new Tile(3286, 3164, 0), new Tile(3285, 3164, 0), new Tile(3284, 3164, 0), new Tile(3283, 3164, 0), new Tile(3282, 3164, 0), new Tile(3282, 3165, 0), new Tile(3282, 3166, 0), new Tile(3282, 3167, 0), new Tile(3282, 3168, 0), new Tile(3282, 3169, 0), new Tile(3282, 3170, 0), new Tile(3282, 3171, 0), new Tile(3282, 3172, 0), new Tile(3282, 3173, 0), new Tile(3282, 3174, 0), new Tile(3283, 3174, 0), new Tile(3284, 3174, 0), new Tile(3284, 3175, 0), new Tile(3285, 3175, 0), new Tile(3286, 3175, 0), new Tile(3287, 3175, 0), new Tile(3287, 3174, 0), new Tile(3288, 3174, 0), new Tile(3289, 3174, 0), new Tile(3290, 3174, 0), new Tile(3290, 3175, 0), new Tile(3290, 3176, 0), new Tile(3290, 3177, 0), new Tile(3290, 3178, 0), new Tile(3290, 3179, 0) ), // area
			"Al Kharid warrior", // name
			new int[] { 18 }, // npc ids
			836 // death animation id
	),
	
	GIANT_SPIDER(
			Lodestone.EDGEVILLE, // closest lodestone
			// SW area
			new Area( new Tile(2118, 5280, 0), new Tile(2119, 5280, 0), new Tile(2120, 5280, 0), new Tile(2121, 5280, 0), new Tile(2121, 5279, 0), new Tile(2122, 5279, 0), new Tile(2122, 5278, 0), new Tile(2123, 5278, 0), new Tile(2123, 5277, 0), new Tile(2124, 5277, 0), new Tile(2125, 5277, 0), new Tile(2125, 5276, 0), new Tile(2126, 5276, 0), new Tile(2126, 5275, 0), new Tile(2127, 5275, 0), new Tile(2128, 5275, 0), new Tile(2129, 5275, 0), new Tile(2130, 5275, 0), new Tile(2131, 5275, 0), new Tile(2131, 5276, 0), new Tile(2132, 5276, 0), new Tile(2132, 5277, 0), new Tile(2132, 5278, 0), new Tile(2133, 5278, 0), new Tile(2133, 5277, 0), new Tile(2134, 5277, 0), new Tile(2134, 5276, 0), new Tile(2135, 5276, 0), new Tile(2135, 5275, 0), new Tile(2135, 5274, 0), new Tile(2134, 5274, 0), new Tile(2134, 5273, 0), new Tile(2134, 5272, 0), new Tile(2135, 5272, 0), new Tile(2135, 5271, 0), new Tile(2134, 5271, 0), new Tile(2133, 5271, 0), new Tile(2132, 5271, 0), new Tile(2132, 5270, 0), new Tile(2131, 5270, 0), new Tile(2131, 5269, 0), new Tile(2131, 5268, 0), new Tile(2132, 5268, 0), new Tile(2132, 5267, 0), new Tile(2133, 5267, 0), new Tile(2133, 5266, 0), new Tile(2134, 5266, 0), new Tile(2134, 5265, 0), new Tile(2135, 5265, 0), new Tile(2136, 5265, 0), new Tile(2136, 5264, 0), new Tile(2136, 5263, 0), new Tile(2137, 5263, 0), new Tile(2137, 5262, 0), new Tile(2136, 5262, 0), new Tile(2135, 5262, 0), new Tile(2134, 5262, 0), new Tile(2134, 5261, 0), new Tile(2133, 5261, 0), new Tile(2133, 5260, 0), new Tile(2132, 5260, 0), new Tile(2132, 5261, 0), new Tile(2131, 5261, 0), new Tile(2131, 5262, 0), new Tile(2131, 5263, 0), new Tile(2131, 5264, 0), new Tile(2130, 5264, 0), new Tile(2130, 5265, 0), new Tile(2129, 5265, 0), new Tile(2128, 5265, 0), new Tile(2127, 5265, 0), new Tile(2127, 5266, 0), new Tile(2126, 5266, 0), new Tile(2126, 5267, 0), new Tile(2125, 5267, 0), new Tile(2124, 5267, 0), new Tile(2123, 5267, 0), new Tile(2122, 5267, 0), new Tile(2121, 5267, 0), new Tile(2121, 5268, 0), new Tile(2121, 5269, 0), new Tile(2121, 5270, 0), new Tile(2120, 5270, 0), new Tile(2120, 5271, 0), new Tile(2119, 5271, 0), new Tile(2119, 5272, 0), new Tile(2118, 5272, 0), new Tile(2118, 5273, 0), new Tile(2117, 5273, 0), new Tile(2117, 5274, 0), new Tile(2116, 5274, 0), new Tile(2116, 5275, 0), new Tile(2117, 5275, 0), new Tile(2117, 5276, 0), new Tile(2117, 5277, 0), new Tile(2117, 5278, 0), new Tile(2117, 5279, 0), new Tile(2117, 5280, 0) ), // area
			"Giant spider", // name
			new int[] { 4400 }, // npc ids
			5329 // death animation id
	),
	
	PIRATE(
			Lodestone.PORT_SARIM, // closest lodestone
			new Area( new Tile(2988, 9585, 0), new Tile(2989, 9585, 0), new Tile(2990, 9585, 0), new Tile(2991, 9585, 0), new Tile(2991, 9586, 0), new Tile(2992, 9586, 0), new Tile(2992, 9587, 0), new Tile(2992, 9588, 0), new Tile(2993, 9588, 0), new Tile(2994, 9588, 0), new Tile(2995, 9588, 0), new Tile(2995, 9587, 0), new Tile(2995, 9586, 0), new Tile(2996, 9586, 0), new Tile(2996, 9585, 0), new Tile(2997, 9585, 0), new Tile(2998, 9585, 0), new Tile(2999, 9585, 0), new Tile(2999, 9584, 0), new Tile(3000, 9584, 0), new Tile(3000, 9583, 0), new Tile(3001, 9583, 0), new Tile(3001, 9582, 0), new Tile(3001, 9581, 0), new Tile(3002, 9581, 0), new Tile(3002, 9580, 0), new Tile(3003, 9580, 0), new Tile(3004, 9580, 0), new Tile(3004, 9579, 0), new Tile(3004, 9578, 0), new Tile(3003, 9578, 0), new Tile(3002, 9578, 0), new Tile(3002, 9577, 0), new Tile(3001, 9577, 0), new Tile(3001, 9576, 0), new Tile(3001, 9575, 0), new Tile(3001, 9574, 0), new Tile(3001, 9573, 0), new Tile(3000, 9573, 0), new Tile(3000, 9572, 0), new Tile(2999, 9572, 0), new Tile(2999, 9571, 0), new Tile(2998, 9571, 0), new Tile(2998, 9570, 0), new Tile(2998, 9569, 0), new Tile(2998, 9568, 0), new Tile(2998, 9567, 0), new Tile(2998, 9566, 0), new Tile(2998, 9565, 0), new Tile(2997, 9565, 0), new Tile(2997, 9564, 0), new Tile(2996, 9564, 0), new Tile(2996, 9563, 0), new Tile(2995, 9563, 0), new Tile(2995, 9562, 0), new Tile(2995, 9561, 0), new Tile(2995, 9560, 0), new Tile(2995, 9559, 0), new Tile(2994, 9559, 0), new Tile(2993, 9559, 0), new Tile(2993, 9560, 0), new Tile(2992, 9560, 0), new Tile(2992, 9561, 0), new Tile(2991, 9561, 0), new Tile(2991, 9562, 0), new Tile(2991, 9563, 0), new Tile(2991, 9564, 0), new Tile(2991, 9565, 0), new Tile(2991, 9566, 0), new Tile(2991, 9567, 0), new Tile(2991, 9568, 0), new Tile(2991, 9569, 0), new Tile(2990, 9569, 0), new Tile(2990, 9570, 0), new Tile(2989, 9570, 0), new Tile(2989, 9571, 0), new Tile(2988, 9571, 0), new Tile(2988, 9572, 0), new Tile(2987, 9572, 0), new Tile(2986, 9572, 0), new Tile(2985, 9572, 0), new Tile(2985, 9573, 0), new Tile(2984, 9573, 0), new Tile(2984, 9574, 0), new Tile(2984, 9575, 0), new Tile(2984, 9576, 0), new Tile(2984, 9577, 0), new Tile(2984, 9578, 0), new Tile(2984, 9579, 0), new Tile(2984, 9580, 0), new Tile(2984, 9581, 0), new Tile(2984, 9582, 0), new Tile(2985, 9582, 0), new Tile(2985, 9583, 0), new Tile(2986, 9583, 0), new Tile(2986, 9584, 0), new Tile(2987, 9584, 0), new Tile(2987, 9585, 0) ), // area
			"Pirate", // name
			new int[] { 184, 6351, 6352, 6353, 6354, 6355, 6356 }, // npc ids
			7185 // death animation id
	);
	
	private Lodestone closestLodestone;
	private Area area;
	private String name; // for menu option also
	private int[] npcIds;
	private int deathAnimationId;
	
	private static EnemyType cur;
	
	public static EnemyType getCurrent() {
		return cur;
	}
	
	public static void setCurrent(EnemyType enemyType) {
		cur = enemyType;
	}
	
	EnemyType(Lodestone closestLodestone, Area area, String name, int[] npcIds, int deathAnimationId) {
		this.closestLodestone = closestLodestone;
		this.area = area;
		this.name = name;
		this.npcIds = npcIds;
		this.deathAnimationId = deathAnimationId;
	}
	
	public Lodestone getClosestLodestone() {
		return closestLodestone;
	}
	
	public Area getArea() {
		return area;
	}
	
	public String getName() {
		return name;
	}
	
	public int[] getNpcIds() {
		return npcIds;
	}
	
	public int getDeathAnimationId() {
		return deathAnimationId;
	}
}