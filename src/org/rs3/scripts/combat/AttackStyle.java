package org.rs3.scripts.combat;

import java.util.HashSet;
import java.util.Set;

import org.rs3.api.Ability;
import org.rs3.api.EquipmentSlot;
import org.rs3.api.ItemTable;
import org.rs3.api.Skill;
import org.rs3.api.Worlds;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.WorldInfo;
import org.rs3.scripts.Combat;
import org.rs3.util.ArrayUtils;

public enum AttackStyle {
	
	MELEE(1), RANGED(3), MAGIC(2);
	
	private static AttackStyle cur;
	
	int actionBarIndex;
	
	private AttackStyle(int actionBarIndex) {
		this.actionBarIndex = actionBarIndex;
	}
	
	
	public int getActionBarIndex() {
		return actionBarIndex;
	}
	
	public Ability[] getPreferredAbilities() {
		boolean members = true;
		WorldInfo worldInfo = Client.getWorldInfo();
		if (worldInfo == null || !Worlds.isMembers(worldInfo.getWorld()))
			members = false;
		
		switch (this) {
		case MAGIC:
			if (members)
				// one-handed
				return new Ability[] { Ability.WRACK, Ability.COMBUST, Ability.DRAGON_BREATH, Ability.IMPACT, Ability.CHAIN, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
			else
				// one-handed
				return new Ability[] { Ability.CHAIN, Ability.COMBUST, Ability.WRACK, Ability.IMPACT, Ability.ANTICIPATION, Ability.FREEDOM, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
		case MELEE:
			if (members) {
				// one-handed
				return new Ability[] { Ability.DISMEMBER, Ability.FURY, Ability.SEVER, Ability.SLICE, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
				
				// two-handed
				//return new Ability[] { Ability.CLEAVE, Ability.DISMEMBER, Ability.SEVER, Ability.SMASH, Ability.SLICE, Ability.FURY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
			} else {
				// one-handed
				return new Ability[] { Ability.SEVER, Ability.FURY, Ability.SLICE, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
				
				// two-handed
				//return new Ability[] { Ability.CLEAVE, Ability.SEVER, Ability.SMASH, Ability.FURY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
			}
		case RANGED:
			if (members)
				// one-handed
				return new Ability[] { Ability.SNIPE, Ability.PIERCING_SHOT, Ability.FRAGMENTATION_SHOT, Ability.RICOCHET, Ability.BINDING_SHOT, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
			else
				// one-handed
				return new Ability[] { Ability.RICOCHET, Ability.FRAGMENTATION_SHOT, Ability.PIERCING_SHOT, Ability.BINDING_SHOT, Ability.ANTICIPATION, Ability.FREEDOM, Ability.EMPTY, Ability.EMPTY, Ability.EMPTY };
		default:
			break;
		}
		
		return null;
	}
	
	public int[] getEquipmentIds() {
		switch (AttackStyle.getCurrent()) {
		case MAGIC:
			return new int[] { Combat.staffOfAirId };
		case MELEE:
			return new int[] { Combat.bronzeSwordId };
			
			// TODO: buy from ge
			/*Set<Integer> ids = new HashSet<>();
			
			if (Skill.ATTACK.getRealLevel() < 50)
				ids.add(Combat.bronzeSwordId);
			else if (Skill.ATTACK.getRealLevel() <= 99)
				ids.add(Combat.rune2hSwordId);
			
			if (Skill.DEFENSE.getRealLevel() < 50) {
				
			} else if (Skill.DEFENSE.getRealLevel() < 99) {
				ids.add(Combat.runeFullHelmId);
				ids.add(Combat.runeChainbodyId);
				ids.add(Combat.runePlatelegsId);
			}
			
			return ArrayUtils.toPrimitiveIntArray(ids.toArray(new Integer[0]));*/
		case RANGED:
			return new int[] { Combat.chargebowId };
		default:
			break;
		}
		
		return null;
	}
	
	public boolean isEquipmentSetup() {
		for (int equipmentId : getEquipmentIds()) {
			if (!ItemTable.EQUIPMENT.containsAny(equipmentId))
				return false;
		}
		
		return true;
		
		/*switch (this) {
		case MAGIC:
			if (EquipmentSlot.MAINHAND.getItemId() == Combat.staffOfAirId)
				return true;
			break;
		case MELEE:
			if (EquipmentSlot.MAINHAND.getItemId() == Combat.bronzeSwordId)
				return true;
			break;
		case RANGED:
			if (EquipmentSlot.MAINHAND.getItemId() == Combat.chargebowId)
				return true;
			break;
		default:
			break;
		}
		
		return false;*/
	}
	
	
	public static AttackStyle getCurrent() {
		return cur;
	}
	
	public static void setCurrent(AttackStyle attackStyle) {
		cur = attackStyle;
	}
}
