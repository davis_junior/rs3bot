package org.rs3.scripts;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.rs3.api.Ability;
import org.rs3.api.ActionBar;
import org.rs3.api.ActionSlot;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.CombatMode;
import org.rs3.api.Constants.Animation;
import org.rs3.api.EquipmentSlot;
import org.rs3.api.GroundItems;
import org.rs3.api.GroundObjects;
import org.rs3.api.Interfaces;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Shop;
import org.rs3.api.Skill;
import org.rs3.api.Spell;
import org.rs3.api.Stats;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.interfaces.ContinueMessage;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.ChatOptionPane;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Settings_Gameplay_LootSettingsSubTabWidget;
import org.rs3.api.interfaces.widgets.ShopWidget;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.BasicInteractable;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.objects.TilePath;
import org.rs3.api.places.WizardsTower;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Character;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.EntityNode;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.NpcDefinition;
import org.rs3.api.wrappers.ObjectDefinition;
import org.rs3.api.wrappers.Player;
import org.rs3.callbacks.MenuCallback;
import org.rs3.callbacks.capture.ModelCapture;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.debug.Paint;
import org.rs3.debug.PaintListenter;
import org.rs3.scripts.combat.AttackStyle;
import org.rs3.scripts.combat.CombatLocation;
import org.rs3.scripts.combat.EnemyType;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.runespan.Creature;
import org.rs3.scripts.runespan.FindAllPaths;
import org.rs3.scripts.runespan.FloorLevel;
import org.rs3.scripts.runespan.Island;
import org.rs3.scripts.runespan.Node;
import org.rs3.scripts.runespan.PlatformBridge;
import org.rs3.scripts.runespan.Rune;
import org.rs3.scripts.tanning.HideType;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import com.sun.corba.se.spi.legacy.connection.GetEndPointInfoAgainException;

public class Runespan extends Script implements PaintListenter {
	
	public static final int floatingEssenceId = 15402; // Collect
	public static final int vineLadderId = 70508; // Climb up
	
	public static final String floatingEssenceName = "Floating essence";
	public static final String vineLadderName = "Vine ladder";
	
	public static final Tile vineLadderTile = new Tile(3957, 6107, 1);
	
	private static boolean siphonEssenceMode = false;
	private static boolean siphonRunesMode = false;
	
	private static int minRunes = 100;
	private static int maxRunes = 150;
	
	private enum Action {
		
		COLLECT_ESSENCE,
		SIPHON_ESSENCE,
		SIPHON_XP,
		SIPHON_RUNES,
		WALK,
		;
		
		public static Action getNextAction() {
			boolean contains = FloorLevel.getCurrent() != null;
			
			if (contains) {
				if (Rune.RUNE_ESSENCE.count() < 10)
					return COLLECT_ESSENCE;
				
				if (Rune.RUNE_ESSENCE.count() < 1000) {
					siphonEssenceMode = true;
					return SIPHON_ESSENCE;
				}
				
				if (siphonEssenceMode) {
					if (Rune.RUNE_ESSENCE.count() < 1500)
						return SIPHON_ESSENCE;
					else
						siphonEssenceMode = false;
				}
				
				for (Rune rune : Rune.values()) {
					if (rune.equals(Rune.RUNE_ESSENCE) || rune.equals(Rune.ELEMENTAL))
						continue;
					
					if (!rune.isObtainable())
						continue;
					
					boolean anyUnderLargeThreshold = false;
					for (Creature creature : FloorLevel.getCurrent().getCreatures()) {
						if (creature.getRune().equals(rune)) {
							if (rune.count() < minRunes) {
								siphonRunesMode = true;
								return SIPHON_RUNES;
							}
							
							if (siphonRunesMode) {
								if (rune.count() < maxRunes) {
									anyUnderLargeThreshold = true;
									return SIPHON_RUNES;
								}
							}
						}
					}
					
					if (siphonRunesMode) {
						if (!anyUnderLargeThreshold)
							siphonRunesMode = false;
					}
				}
				
				return SIPHON_XP;
			}
			
			return WALK;
		}
	}
	
	// saves cpu by defining out of scope curIsland
	Filter<Npc> getCreaturesAttackingPlayerAreaFilter() {
		final Island curIsland = Island.getCurrentIsland();
		
		return new Filter<Npc>() {
			@Override
			public boolean accept(Npc npc) {
				if (npc == null)
					return false;
				
				if (curIsland == null)
					return false;
				
				NpcDefinition def = npc.getNpcDefinition();
				if (def == null)
					return false;
				
				int npcId = def.getId();
				int npcAnimationId = npc.getAnimationId();
				for (Creature creature : Creature.values()) {
					if (creature.getId() == npcId) {
						if (npcAnimationId == creature.getDeathAnimationId())
							return false;
						
						Character interacting = npc.getInteracting();
						Player player = Players.getMyPlayer();
						if (player == null)
							return false;
						
						if (interacting != null && interacting.getObject() == player.getObject()) {
							System.out.println("creature is already attacking my player");
							if (curIsland.getArea().contains(npc.getTile()))
								return true;
						}
					}
				}
				
				if (npc.getInteractingIndex() != -1) {
					Character interacting = npc.getInteracting();
					System.out.println("inter with: [" + npc.getInteractingIndex() + "] " + interacting);
				}
				
				return false;
			}
		};
	}
	
	// saves cpu by defining out of scope curIsland
	Filter<Npc> getCreaturesAreaFilter() {
		final Island curIsland = Island.getCurrentIsland();
		
		return new Filter<Npc>() {
			@Override
			public boolean accept(Npc npc) {
				if (npc == null)
					return false;
				
				if (curIsland == null)
					return false;
				
				NpcDefinition def = npc.getNpcDefinition();
				if (def == null)
					return false;
				
				int npcId = def.getId();
				int npcAnimationId = npc.getAnimationId();
				for (Creature creature : Creature.values()) {
					if (creature.getId() == npcId) {
						if (npcAnimationId == creature.getDeathAnimationId())
							return false;
						
						Character interacting = npc.getInteracting();
						Player player = Players.getMyPlayer();
						if (player == null)
							return false;
						
						if (interacting != null && interacting.getObject() == player.getObject()) {
							System.out.println("creature is already attacking my player");
							if (curIsland.getArea().contains(npc.getTile()))
								return true;
						}
					}
				}
				
				//if (npc.getInteractingIndex() != -1)
				//	return false;
				
				if (curIsland.getArea().contains(npc.getTile()))
					return true;
				
				return false;
			}
		};
	}
	
	Filter<GroundObject> nodesAreaFilter = new Filter<GroundObject>() {
		@Override
		public boolean accept(GroundObject go) {
			if (go == null)
				return false;
			
			Island curIsland = Island.getCurrentIsland();
			if (curIsland == null)
				return false;
			
			if (curIsland.getArea().contains(go.getTile()))
				return true;
			
			return false;
		}
	};
	
	Condition npcIsDead = new Condition() {
		@Override
		public boolean evaluate() {
			if (curNpc != null) {
				//System.out.println(curNpc.getAnimationId());
				
				NpcDefinition def = curNpc.getNpcDefinition();
				if (def == null)
					return false;
				
				int npcId = def.getId();
				int npcAnimationId = curNpc.getAnimationId();
				for (Creature creature : Creature.values()) {
					if (creature.getId() == npcId) {
						if (npcAnimationId == creature.getDeathAnimationId())
							return true;
					}
				}
				
				return false;
			} else
				return true;
		}
	};
	
	Condition interactingWithNpc = new Condition() {
		@Override
		public boolean evaluate() {
			if (curNpc != null) {
				Character interacting = curNpc.getInteracting();
				Player player = Players.getMyPlayer();
				if (player == null)
					return false;
				
				if (interacting != null && interacting.getObject() == player.getObject())
					return true;
			}
			
			return false;
		}
	};
	
	Condition nodeIsLikelyDisposed = new Condition() {
		@Override
		public boolean evaluate() {
			if (curGo != null) {
				if (curGo instanceof EntityNode) {
					EntityNode eNode = (EntityNode) curGo;
					return eNode.isDisposed();
				}
			}
			
			return true;
		}
	};
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public int[] getCreatureIds(Rune rune) {
		List<Integer> creatureIds = new ArrayList<>();
		for (Creature creature : Creature.values()) {
			if (creature.getRune().equals(rune) && creature.isInteractable())
				creatureIds.add(creature.getId());
		}
		
		if (creatureIds.size() == 0)
			return null;
		
		return ArrayUtils.toPrimitiveIntArray(creatureIds.toArray(new Integer[0]));
	}
	
	public int[] getNodeIds(Rune rune) {
		List<Integer> nodeIds = new ArrayList<>();
		for (Node node : Node.values()) {
			if (node.isInteractable()) {
				for (Rune r : node.getRunes()) {
					if (r.equals(rune)) {
						nodeIds.add(node.getId());
						break;
					}
				}
			}
		}
		
		if (nodeIds.size() == 0)
			return null;
		
		return ArrayUtils.toPrimitiveIntArray(nodeIds.toArray(new Integer[0]));
	}
	
	public Npc getCurrentIslandBestXPCreature() {
		// if already siphoning creature, keep siphoning from that creature
		Npc npc = Npcs.getNearest(getCreaturesAttackingPlayerAreaFilter());
		if (npc != null)
			return npc;
		
		Creature curCreature = null;
		for (Npc n : Npcs.getAll()) {
			if (getCreaturesAreaFilter().accept(n)) {
				NpcDefinition def = n.getNpcDefinition();
				if (def == null)
					continue;
				
				for (Creature creature : Creature.values()) {
					if (creature.isInteractable() && def.getId() == creature.getId()) {
						if (curCreature == null || creature.getXp() >= curCreature.getXp()) {
							if (n != null) {
								curCreature = creature;
								npc = n;
							}
						}
					}
				}
			}
		}
		
		return npc;
	}
	
	public GroundObject getCurrentIslandBestXPNode() {
		GroundObject go = null;
		Node curNode = null;
		for (GroundObject g : GroundObjects.getAll(Type.All, 25, false)) {
			if (nodesAreaFilter.accept(g)) {
				for (Node node : Node.values()) {
					if (node.isInteractable() && node.getId() == g.getId()) {
						if (curNode == null || node.getXp() >= curNode.getXp()) {
							if (g != null) {
								curNode = node;
								go = g;
							}
						}
					}
				}
			}
		}
		
		return go;
	}
	
	public BasicInteractable getCurrentIslandBestXPCreatureOrNode() {
		
		// if already siphoning creature, keep siphoning from that creature
		Npc npc = Npcs.getNearest(getCreaturesAttackingPlayerAreaFilter());
		if (npc != null)
			return npc;
		
		npc = getCurrentIslandBestXPCreature();
		GroundObject go = getCurrentIslandBestXPNode();
		
		if (npc == null)
			return go;
		if (go == null)
			return npc;
		
		double creatureXp = 0;
		double nodeXp = 0;
		
		NpcDefinition def = npc.getNpcDefinition();
		if (def != null) {
			for (Creature creature : Creature.values()) {
				if (creature.getId() == def.getId()) {
					creatureXp = creature.getXp();
					break;
				}
			}
		}
		
		for (Node node : Node.values()) {
			if (node.getId() == go.getId()) {
				nodeXp = node.getXp();
				break;
			}
		}
		
		if (nodeXp >= creatureXp)
			return go;
		else
			return npc;
	}
	
	public BasicInteractable getNearestCreatureOrNode(Rune rune) {
		
		// if already siphoning creature, keep siphoning from that creature
		Npc npc = Npcs.getNearest(getCreaturesAttackingPlayerAreaFilter());
		if (npc != null)
			return npc;
		
		npc = Npcs.getNearest(getCreaturesAreaFilter(), getCreatureIds(rune));
		GroundObject go = GroundObjects.getNearest(Type.All, nodesAreaFilter, getNodeIds(rune));
		
		if (npc == null)
			return go;
		if (go == null)
			return npc;
		
		if (npc.getTile().distanceTo() < go.getTile().distanceTo())
			return npc;
		else
			return go;
	}
	
	public Npc getNearestCreature(int... ids) {
		
		Timer timer = new Timer(2000);
		while (timer.isRunning()) {
			Npc npc = Npcs.getNearest(getCreaturesAttackingPlayerAreaFilter());
			if (npc != null)
				return npc;
			
			npc = Npcs.getNearest(getCreaturesAreaFilter(), ids);
			if (npc != null)
				return npc;
			
			Time.sleep(50);
		}
		
		return null;
	}
	
	public GroundObject getNearestNode(int... ids) {
		
		Timer timer = new Timer(2000);
		while (timer.isRunning()) {
			GroundObject go = GroundObjects.getNearest(Type.All, nodesAreaFilter, ids);
			if (go != null)
				return go;
			
			Time.sleep(50);
		}
		
		return null;
	}
	
	public boolean waitUntilStartSiphoning(boolean doAntiban) {
		double dist;
		if (curGo != null)
			dist = Calculations.distanceTo(curGo.getTile());
		else if (curNpc != null)
			dist = Calculations.distanceTo(curNpc.getTile());
		else
			dist = 5;
		
		int maxPeriod;
		
		if (dist >= 5)
			maxPeriod = 5000;
		else if (dist > 2.5)
			maxPeriod = 4000;
		else if (dist > 1.5)
			maxPeriod = 3000;
		else
			maxPeriod = 2500;
		
		Condition breakCondition = new Condition() {
			@Override
			public boolean evaluate() {
				return ChatOptionPane.get.isOpen() || ContinueMessage.isOpen();
			}
		};
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		return player.waitForAnimation(Animation.RUNESPAN_SIPHONING.getIds(), maxPeriod, doAntiban, breakCondition);
	}
	
	public boolean waitUntilStopSiphoning(boolean doAntiban) {
		//Set<Integer> ids = new HashSet<>(Animation.RUNESPAN_SIPHON_END.getIds());
		//ids.add(-1);
		
		Set<Integer> ids = new HashSet<>();
		ids.addAll(Animation.RUNESPAN_SIPHON_END.getIds());
		ids.addAll(Animation.RUNESPAN_SIPHONING.getIds());
		
		Condition breakCondition = new Condition() {
			@Override
			public boolean evaluate() {
				if (curNpc != null && npcIsDead.evaluate())
					return true;
				
				if (curGo != null && nodeIsLikelyDisposed.evaluate())
					return true;
				
				return ChatOptionPane.get.isOpen() || ContinueMessage.isOpen();
			}
		};
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		//return player.waitForAnimation(ids, 60000, doAntiban, breakCondition);
		return player.waitForIdleAnimation(ids, 60000, doAntiban, breakCondition);
	}
	
	public boolean siphon(BasicInteractable interactable) {
		Tab.closeAll();
		
		if (interactable == null)
			return false;
		
		if (interactable instanceof Npc)
			return siphonCreature((Npc) interactable);
		else if (interactable instanceof GroundObject)
			return siphonNode((GroundObject) interactable);
		
		return false;
	}
	
	public boolean siphonCreature(Npc npc) {
		
		if (npc == null)
			return false;
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		synchronized (ModelCapture.npcs) {
			ModelCapture.npcs.remove(curNpc);
			curNpc = npc;
			ModelCapture.npcs.add(curNpc);
		}
		
		if (Calculations.distanceTo(npc) < 15) {
			Camera.turnTo(npc);
			Time.sleepVeryQuick();
			if (!npc.getTile().isOnScreen()) {
				Camera.setDefaultPitch();
				Time.sleepVeryQuick();
			}
		} else if (!npc.loopWalkTo(Island.getCurrentIsland().getArea(), 10000, npcIsDead))
			return false;
		
		String name = null;
		NpcDefinition def = npc.getNpcDefinition();
		if (def != null)
			name = def.getName();
		
		if (Calculations.distanceTo(npc) < 15 && npc.loopInteract(npcIsDead, "Siphon", name, false))
			return waitUntilStartSiphoning(true);
		
		return false;
	}
	
	public boolean siphonNode(GroundObject go) {
		
		if (go == null)
			return false;
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		curGo = go;
		
		if (Calculations.distanceTo(go) < 15) {
			Camera.turnTo(go);
			Time.sleepVeryQuick();
			if (!go.getTile().isOnScreen()) {
				Camera.setDefaultPitch();
				Time.sleepVeryQuick();
			}
		} else if (!go.loopWalkTo(10000, nodeIsLikelyDisposed))
			return false;
		
		String name = null;
		ObjectDefinition def = go.getDefinition();
		if (def != null)
			name = def.getName();
		
		if (Calculations.distanceTo(go) < 15 && go.getTile().loopInteract(nodeIsLikelyDisposed, "Siphon", name, false)) {
			waitUntilStartSiphoning(true);
			return true;
		}
		
		return false;
	}
	
	public boolean collectFloatingEssence() {
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		Npc npc = Npcs.getNearest(new Filter<Npc>() {
			@Override
			public boolean accept(Npc npc) {
				Island curIsland = Island.getCurrentIsland();
				if (curIsland == null)
					return false;
				
				if (curIsland.getArea().contains(npc.getTile()))
					return true;
				
				return false;
			}
		}, floatingEssenceId);
		
		if (npc == null)
			return false;
		
		if (Calculations.distanceTo(npc) < 15) {
			Camera.turnTo(npc);
			Time.sleepVeryQuick();
			if (!npc.getTile().isOnScreen()) {
				Camera.setDefaultPitch();
				Time.sleepVeryQuick();
			}
		} else if (!npc.loopWalkTo(Island.getCurrentIsland().getArea(), 10000, null))
			return false;
		
		if (Calculations.distanceTo(npc) < 15 && npc.loopInteract(null, "Collect"))
			return true;
		
		return false;
	}
	
	public Creature getFloorBestXPCreature() {
		Creature best = null;
		for (Creature creature : FloorLevel.getCurrent().getCreatures()) {
			if (creature.isInteractable()) {
				if (best == null)
					best = creature;
				else if (creature.getXp() >= best.getXp())
					best = creature;
			}
		}
		
		return best;
	}
	
	public boolean bank() {
		
		if (Bank.get.open()) {
			Time.sleepQuick();
			
			if (Bank.get.depositAllInvItems()) {
				Time.sleepQuick();
				
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					Bank.get.withdrawAll(995, false); // coins
					Time.sleepQuick();
				}
				
				Bank.get.close();
				
				return true;
			}
		} else if (DepositBox.get.open()) {
			Time.sleepQuick();
			
			if (DepositBox.get.depositAllItems()) {
				Time.sleepQuick();
				DepositBox.get.close();
				
				return true;
			}
		}

		
		return false;
	}
	
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		failsafeWalkedToMine = new Failsafe("Walked to Mine", 900000); // 15 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 900000); // 15 mins.
		failsafeMinedRock = new Failsafe("Mine", 900000); // 15 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 900000); // 15 mins.
		failsafes.add(failsafeWalkedToMine);
		failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		//params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		//breakHandlerParam = new BreakHandlerParam(failsafes);
		//params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		/*boolean success = false;
		if (AccountInfo.cur != null) {
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Map<String, String> params = row.getScriptParams_toMap();
				String enemyType = params.get("EnemyType");
				if (enemyType != null) {
					success = true;
					
					enemyType = enemyType.toLowerCase();
					enemyType = enemyType.replace("_", " ");
					if (enemyType.contains("al kharid warrior"))
						EnemyType.setCurrent(EnemyType.AL_KHARID_WARRIOR);
					else if (enemyType.contains("giant spider"))
						EnemyType.setCurrent(EnemyType.GIANT_SPIDER);
					else if (enemyType.contains("pirate"))
						EnemyType.setCurrent(EnemyType.PIRATE);
					else if (enemyType.contains("troll brute"))
						EnemyType.setCurrent(EnemyType.TROLL_BRUTE);
					else if (enemyType.contains("troll chucker"))
						EnemyType.setCurrent(EnemyType.TROLL_CHUCKER);
					else if (enemyType.contains("troll shaman"))
						EnemyType.setCurrent(EnemyType.TROLL_SHAMAN);
					else
						success = false;
				}
				
				if (success) {
					String combatLocation = params.get("CombatLocation");
					if (combatLocation != null) {
						success = true;
						
						combatLocation = combatLocation.toLowerCase();
						combatLocation = combatLocation.replace("_", " ");
						if (combatLocation.contains("al kharid palace"))
							CombatLocation.setCurrent(CombatLocation.AL_KHARID_PALACE);
						else if (combatLocation.contains("asgarnian ice dungeon"))
							CombatLocation.setCurrent(CombatLocation.ASGARNIAN_ICE_DUNGEON);
						else if (combatLocation.contains("burthorpe trolls"))
							CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
						else if (combatLocation.contains("stronghold security floor1"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR1);
						else if (combatLocation.contains("stronghold security floor2"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR2);
						else if (combatLocation.contains("stronghold security floor3"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR3);
						else if (combatLocation.contains("stronghold security floor4"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR4);
						else
							success = false;
						
						if (success) {
							String attackStyle = params.get("AttackStyle");
							if (attackStyle != null) {
								success = true;
								
								attackStyle = attackStyle.toLowerCase();
								if (attackStyle.contains("magic"))
									AttackStyle.setCurrent(AttackStyle.MAGIC);
								else if (attackStyle.contains("melee"))
									AttackStyle.setCurrent(AttackStyle.MELEE);
								else if (attackStyle.contains("ranged"))
									AttackStyle.setCurrent(AttackStyle.RANGED);
								else
									success = false;
							}
						}
					}
				}
			}
		}
		
		if (!success) {
			System.out.println("Combat: Error loading params! Loaded default data params.");
			EnemyType.setCurrent(EnemyType.TROLL_BRUTE);
			CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
			AttackStyle.setCurrent(AttackStyle.MELEE);
		} else {
			System.out.println("Combat: Successfully loaded data params: ");
		}
		
		System.out.println("\t-EnemyType: " + EnemyType.getCurrent().name());
		System.out.println("\t-CombatLocation: " + CombatLocation.getCurrent().name());
		System.out.println("\t-AttackStyle: " + AttackStyle.getCurrent().name());*/
	}
	
	@Override
	public String getInfo() {
		return "";
	}
	
	@Override
	public boolean onStart() {
		
		loadDataParams();
		
		Antiban.resetTimer();
		
		destIsland = null;
		
		return true;
	}
	
	Npc curNpc = null;
	GroundObject curGo = null;
	
	Island destIsland = null;
	
	/*
	 * Script Notes
	 * 
	 */
	
	@Override
	public void run() {
		
		Island currentIsland = Island.getCurrentIsland();
		if (currentIsland != null)
			paint_currentIslandTiles = currentIsland.getArea().getBoundingTiles();
		
		Antiban.randomMouseSpeed();
		
		if (!Client.loopLogin())
			return;
		
		if (curNpc != null) {
			//waitUntilStopSiphoningCreature(true, true);
			waitUntilStopSiphoning(true);
			synchronized (ModelCapture.npcs) {
				ModelCapture.npcs.remove(curNpc);
				curNpc = null;
			}
		}
		
		if (curGo != null) {
			waitUntilStopSiphoning(true);
			synchronized (ModelCapture.npcs) {
				ModelCapture.npcs.remove(curNpc);
				curGo = null;
			}
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!Lodestone.DRAYNOR_VILLAGE.isActivated()) {
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			if (!Lodestone.DRAYNOR_VILLAGE.isActivated()) {
				Lodestone.DRAYNOR_VILLAGE.activate();
				Time.sleepMedium();
				return;
			}
		}
		
		
		if (ContinueMessage.isOpen() && ContinueMessage.getText(true).contains("You need to be on a member"))
			ContinueMessage.close(true);
		if (ChatOptionPane.get.isOpen() && ChatOptionPane.get.getHeader().toLowerCase().contains("would you like to subscribe"))
			ChatOptionPane.get.chooseNo(true);
		
		// if in runespan
		if (FloorLevel.getCurrent() != null) {
			// go to middle level if level is high enough (set dest island to 51 where vine ladder is located)
			if (Action.getNextAction().equals(Action.SIPHON_XP) && FloorLevel.getCurrent().equals(FloorLevel.LOWER)) {
				if (Skill.RUNECRAFTING.getRealLevel() >= 40) {
					destIsland = Island.LOWER_51;
				}
			}
			
			// walk to destination island
			if (destIsland != null && Island.getCurrentIsland().equals(destIsland))
				destIsland = null;
			
			if (destIsland != null && !Island.getCurrentIsland().equals(destIsland)) {
				Island[] islands = PlatformBridge.getShortestRoute(Island.getCurrentIsland(), destIsland);
				PlatformBridge[] bridges = PlatformBridge.toBridges(islands);
				
				Timer timer = new Timer(120000);
				while (timer.isRunning()) {
					Island curIsland = Island.getCurrentIsland();
					if (curIsland != null) {
						if (destIsland != null && curIsland.equals(destIsland)) {
							destIsland = null;
							break;
						}
						
						// get next island
						PlatformBridge nextBridge = null;
						for (PlatformBridge bridge : bridges) {
							if (bridge.getMainIsland().equals(curIsland)) {
								nextBridge = bridge;
								break;
							}
						}
						
						if (nextBridge == null)
							break; // TODO: wait for bridge animation to end and don't break
						else {
							Tile platformTile = nextBridge.getPlatformTile();
							GroundObject[] gos = GroundObjects.getAllAt(Type.All, platformTile.getX(), platformTile.getY());
							if (gos != null) {
								GroundObject goPlatform = null;
								for (GroundObject go : gos) {
									for (int id : nextBridge.getPlatform().getIds()) {
										if (go.getId() == id) {
											goPlatform = go;
											break;
										}
									}
									
									if (goPlatform != null)
										break;
								}
								
								if (goPlatform != null) {
									if (goPlatform.loopWalkTo(15000)) {
										Time.sleepQuick();
										
										goPlatform.loopInteract(null, "Use", nextBridge.getPlatform().getName(), false);
										// TODO: wait for bridge animation and end
										Time.sleepQuick();
									}
								}
							}
						}
					}
					
					Time.sleep(50);
				}
				
				return;
			}
			
			
			if (Action.getNextAction().equals(Action.SIPHON_XP) && FloorLevel.getCurrent().equals(FloorLevel.LOWER)) {
				// climb vine ladder if level is high enough
				boolean success = Skill.RUNECRAFTING.getRealLevel() < 40;
				
				Island curIsland = Island.getCurrentIsland();
				if (curIsland != null) {
					if (Skill.RUNECRAFTING.getRealLevel() >= 40 && Island.getCurrentIsland().equals(Island.LOWER_51)) {
						if (vineLadderTile.distanceTo() >= 10) {
							vineLadderTile.loopWalkTo(15000);
							Time.sleepQuick();
						}
						
						GroundObject vineLadder = GroundObjects.getNearest(Type.All, vineLadderId);
						if (vineLadder != null) {
							Tile tile = vineLadder.getTile();
							if (!tile.isOnScreen()) {
								Camera.turnTo(tile);
								Time.sleepQuick();
							}
							
							if (vineLadder.loopInteract(null, "Climb up", vineLadderName, false)) {
								success = true;
								Time.sleep(5000, 7000);
							}
						}
					}
				}
				
				if (!success)
					return;
			}
		}
		
		Island curIsland = Island.getCurrentIsland();
		
		Action action = Action.getNextAction();
		System.out.println(action);
		
		switch (action) {
		case COLLECT_ESSENCE:
			if (curIsland == null)
				break;
			
			if (collectFloatingEssence()) {
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
			}
			
			break;
			
		case SIPHON_ESSENCE:
			if (curIsland == null)
				break;
			
			if (siphonCreature(getNearestCreature(Creature.getAllInteractableIds()))) {
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
			}
			
			break;
			
		case SIPHON_RUNES:
			if (curIsland == null)
				break;
			
			int runeThreshold = siphonRunesMode ? maxRunes : minRunes;
			
			boolean curIslandComplete = true;
			for (Creature creature : curIsland.getCreatures()) {
				Rune rune = creature.getRune();
				if (rune.isObtainable() && !rune.equals(Rune.RUNE_ESSENCE) && !rune.equals(Rune.ELEMENTAL)) {
					if (ItemTable.INVENTORY.countItems(true, rune.getId()) < runeThreshold)
						curIslandComplete = false;
				}
			}
			
			if (!curIslandComplete) {
				for (Creature creature : curIsland.getCreatures()) {
					Rune rune = creature.getRune();
					if (rune.isObtainable() && !rune.equals(Rune.RUNE_ESSENCE) && !rune.equals(Rune.ELEMENTAL)) {
						if (ItemTable.INVENTORY.countItems(true, rune.getId()) < runeThreshold) {
							if (!siphon(getNearestCreatureOrNode(rune))) {
								synchronized (ModelCapture.npcs) {
									ModelCapture.npcs.remove(curNpc);
									curNpc = null;
								}
								curGo = null;
							} else {
								failsafeDropOrBank.reset();
								failsafeMinedRock.reset();
								failsafeWalkedToBank.reset();
								failsafeWalkedToMine.reset();
								break;
							}
						}
					}
				}
			} else { // switch islands since we have enough runes from current island
				for (Rune rune : Rune.values()) {
					if (rune.isObtainable() && !rune.equals(Rune.RUNE_ESSENCE) && !rune.equals(Rune.ELEMENTAL)) {
						if (ItemTable.INVENTORY.countItems(true, rune.getId()) < runeThreshold) {
							destIsland = null;
							for (Creature creature : FloorLevel.getCurrent().getCreatures()) {
								if (creature.getRune().equals(rune)) {
									FindAllPaths algo = new FindAllPaths(curIsland, creature);
									Island[] islands = algo.getShortestPath().toArray(new Island[0]);
									destIsland = islands[islands.length - 1];
									break;
								}
							}
							
							if (destIsland != null)
								break;
						}
					}
				}
			}
			
			break;
			
		case SIPHON_XP:
			if (curIsland == null)
				break;
			
			Creature best = getFloorBestXPCreature();
			boolean containsBestCreature = false;
			for (Creature creature : curIsland.getCreatures()) {
				if (creature.equals(best)) {
					containsBestCreature = true;
					break;
				}
			}
			
			if (!containsBestCreature) {
				List<Island> allIslands = Arrays.asList(Island.values());
				Collections.shuffle(allIslands);
				destIsland = null;
				for (Island island : allIslands) {
					for (Creature creature : island.getCreatures()) {
						if (creature.equals(best)) {
							destIsland = island;
							break;
						}
					}
					
					if (destIsland != null)
						break;
				}
				
				break;
			}
			
			if (!siphon(getCurrentIslandBestXPCreatureOrNode())) {
				synchronized (ModelCapture.npcs) {
					ModelCapture.npcs.remove(curNpc);
					curNpc = null;
				}
				curGo = null;
			} else {
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
			}
			
			break;
			
		case WALK:
			synchronized (ModelCapture.npcs) {
				ModelCapture.npcs.remove(curNpc);
				curNpc = null;
			}
			curGo = null;
			
			if (ItemTable.INVENTORY.countAllItems(false) > 0) {
				Interactable bank = Bank.get.getNearest();
				if ((bank != null && bank.getTile().distanceTo() < 25) || Lodestone.BURTHORPE.teleport(true)) {
					Time.sleepQuick();
					
					bank = Bank.get.getNearest();
					if (bank != null) {
						Tile tile = bank.getTile();
						if (tile.distanceTo() <= 3 && !tile.isOnScreen()) {
							Camera.turnTo(tile);
							Time.sleepQuick();
						}
						
						if (tile.distanceTo() <= 3 || bank.loopWalkTo(10000)) {
							Time.sleepQuick();
							bank();
							Time.sleepQuick();
						}
					}
				}
				
				break;
			}
			
			if (Random.nextInt(0, 100) <= 75) {
				if (Random.nextBoolean())
					Time.sleepMedium();
				else
					Time.sleepQuick();
			}
			
			//if (Skill.RUNECRAFTING.getRealLevel() < 40)
			//	WizardsTower.enterLowLevelRunespanPortal();
			//else
			//	WizardsTower.enterMidLevelRunespanPortal(); // members only
			
			if (WizardsTower.enterLowLevelRunespanPortal()) {
				Time.sleepQuick();
				
				Player player = Players.getMyPlayer();
				if (player == null)
					break;
				
				player.waitForMoving(5000, true, (Condition) null);
				player.waitForStopMoving(10000, true, (Condition) null);
				Timer timer = new Timer(10000);
				while (timer.isRunning()) {
					if (FloorLevel.getCurrent() != null) {
						Time.sleepMedium();
						break;
					}
					
					Time.sleep(50);
				}
			}
			
			break;
		}
	}
	
	@Override
	public boolean onStop() {
		return true;
	}

	
	private Tile[] paint_currentIslandTiles = null;
	
	@Override
	public void draw(Graphics g) {
		if (!Client.isLoggedIn())
			return;
		
		/*g.setColor(Color.green);
		if (paint_currentIslandTiles != null) {
			for (Tile tile : paint_currentIslandTiles) {
				tile.drawOnMap(g);
			}
		}*/
		
		g.setColor(Color.green);
		if (curGo != null) {
			curGo.getTile().draw(g);
		}
		
		g.setColor(Color.green);
		synchronized (ModelCapture.npcs) {
			if (curNpc != null) {
				curNpc.draw(g);
				curNpc.getTile().drawOnMap(g);
			}
		}
	}
}
