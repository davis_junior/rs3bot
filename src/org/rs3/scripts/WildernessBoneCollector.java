package org.rs3.scripts;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.rs3.api.Ability;
import org.rs3.api.ActionBar;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.CombatMode;
import org.rs3.api.EquipmentSlot;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundItems;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Npcs;
import org.rs3.api.Paths;
import org.rs3.api.Players;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.exchange.ExchangeItem;
import org.rs3.api.exchange.ExchangeLoop;
import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.CollectionBox;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Settings_Gameplay_LootSettingsSubTabWidget;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.places.Daemonheim;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.database.AccountInfo;
import org.rs3.scripts.combat.AttackStyle;
import org.rs3.scripts.combat.EnemyType;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.FailsafesParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.tanning.HideType;
import org.rs3.util.ArrayUtils;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class WildernessBoneCollector extends Script {
	
	private static final int adamantFullHelmId = 1161;
	private static final int adamantPlatebodyId = 1123;
	private static final int adamantKiteshieldId = 1199;
	private static final int adamantPlatelegsId = 1073;
	
	private static final String adamantFullHelmName = "Adamant full helm";
	private static final String adamantPlatebodyName = "Adamant platebody";
	private static final String adamantKiteshieldName = "Adamant kiteshield";
	private static final String adamantPlatelegsName = "Adamant platelegs";
	
	private static final int[] buyEquipmentIds = new int[] { /*adamantFullHelmId,*/ adamantPlatebodyId, adamantKiteshieldId, adamantPlatelegsId};
	private static final int[] allEquipmentIds = buyEquipmentIds;
	
	private static boolean exchange = false;
	
	private enum Action {
		
		PICKUP, 
		BURY, 
		FIGHT, 
		WALK,
		BANK,
		EXCHANGE
		;
		
		public static Action getNextAction() {
			if (exchange)
				return EXCHANGE;
			
			//if (!ItemTable.INVENTORY.containsAny(adamantFullHelmId) && EquipmentSlot.HEAD.getItemId() != adamantFullHelmId)
			//	return BANK;
			if (!ItemTable.INVENTORY.containsAny(adamantPlatebodyId) && EquipmentSlot.TORSO.getItemId() != adamantPlatebodyId)
				return BANK;
			else if (!ItemTable.INVENTORY.containsAny(adamantKiteshieldId) && EquipmentSlot.OFFHAND.getItemId() != adamantKiteshieldId)
				return BANK;
			else if (!ItemTable.INVENTORY.containsAny(adamantPlatelegsId) && EquipmentSlot.LEGS.getItemId() != adamantPlatelegsId)
				return BANK;
			
			Player player = Players.getMyPlayer();
			if (player == null)
				return null;
			
			if (boneyardArea.contains(player.getTile())
					|| boneyardArea.getDistanceToNearest() < 25) {
			
				if (ItemTable.INVENTORY.countAllItems(false) >= 28)
					return BURY;
				
				GroundItem[] gis = GroundItems.getAll();
				if (gis != null) {
					for (GroundItem gi : gis) {
						if (gi.getId() == 532) { // big bone
							return PICKUP;
						}
					}
				}
				
				if (ItemTable.INVENTORY.containsAny(532)) { // big bone
					if (Random.nextBoolean())
						return BURY;
					else
						return FIGHT;
				}
				
				return FIGHT;
			} else
				return WALK;
		}
	}
	
	/**
	 * South to North
	 */
	static final Tile[] bigBoneTiles = new Tile[] { new Tile(3273, 3676, 0), new Tile(3267, 3678, 0), new Tile(3273, 3686, 0), new Tile(3268, 3689, 0) };
	
	static final Area boneyardArea = new Area( new Tile(3259, 3693, 0), new Tile(3281, 3668, 0) );
	
	Condition firstBoneSpawned = new Condition() {
		@Override
		public boolean evaluate() {
			return isBoneSpawned(bigBoneTiles[0]);
		}
	};
	
	Filter<Npc> skeletonsAreaFilter = new Filter<Npc>() {
		@Override
		public boolean accept(Npc npc) {
			if (npc.getAnimationId() == 17932) // skeleton death animation
				return false;
			
			if (npc.getInteractingIndex() != -1)
				return false;
			
			if (boneyardArea.contains(npc.getTile()))
				return true;
			
			return false;
		}
	};
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public boolean isBoneSpawned(Tile tile) {
		GroundItem[] gis = GroundItems.getItemsAt(tile.getX(), tile.getY());
		if (gis != null) {
			for (GroundItem gi : gis) {
				if (gi.getId() == 532) { // big bone
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean pickup(Tile tile) {
		GroundItem[] gis = GroundItems.getItemsAt(tile.getX(), tile.getY());
		if (gis != null) {
			for (GroundItem gi : gis) {
				if (gi.getId() == 532) { // big bone
					if (gi.loopInteract(null, "Take", "Big bones", false)) {
						Time.sleepQuick();
						
						Timer timer = new Timer(10000);
						while (timer.isRunning()) {
							if (shouldStop())
								return false;
							
							if (!isBoneSpawned(tile))
								break;
							
							Time.sleep(50);
						}
						
						return true;
					} else
						Time.sleepQuick();
				}
			}
		}
		
		return false;
	}
	
	public boolean walkToTile(Tile tile, boolean randomize) {
		if (Calculations.distanceTo(tile) < 4)
			return true;
		
		if (randomize)
			tile = tile.randomize(2, 2);
		
		Timer timer = new Timer(7000);
		while (timer.isRunning()) {
			if (shouldStop())
				return false;
			
			if (Calculations.distanceTo(tile) < 4)
				return true;
			
			Tile curTile = Walking.getClosestOnMap(tile);
			Walking.walk(curTile);
			Time.sleepMedium();
			Timer curTileTimer = new Timer(5000);
			while (curTileTimer.isRunning()) {
				if (shouldStop())
					return false;
				
				if (Calculations.distanceTo(curTile) < 4)
					break;
				
				Time.sleep(50);
				
				if (Antiban.doAntiban())
					Time.sleepQuickest();
			}
			
			Time.sleep(50);
			
			if (Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		return false;
	}
	
	public boolean pickup() {
		
		if (Camera.getZoomPercent() > 20) {
			Camera.setZoomPercent(Random.nextInt(10, 20));
		}
		
		Tab.closeAll(Tab.BACKPACK);
		
		Timer timer = new Timer(5000);
		
		if (!isBoneSpawned(bigBoneTiles[0]))
			walkToTile(bigBoneTiles[0], false);
		
		while (timer.isRunning()) {
			if (shouldStop())
				return false;
			
			if (isBoneSpawned(bigBoneTiles[0]))
				break;
			
			Time.sleep(50);
			
			if (Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		if (!pickup(bigBoneTiles[0])) {
			Time.sleepQuick();
			if (walkToTile(bigBoneTiles[0], false)) {
				//waitUntilStopMoving();
				pickup(bigBoneTiles[0]);
			}
		}
		Time.sleepQuick();
		
		
		if (!isBoneSpawned(bigBoneTiles[1]))
			walkToTile(bigBoneTiles[1], false);
		
		timer.reset();
		while (timer.isRunning()) {
			if (shouldStop())
				return false;
			
			if (isBoneSpawned(bigBoneTiles[1]))
				break;
			
			Time.sleep(50);
			
			if (Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		if (!pickup(bigBoneTiles[1])) {
			Time.sleepQuick();
			if (walkToTile(bigBoneTiles[1], false)) {
				//waitUntilStopMoving();
				pickup(bigBoneTiles[1]);
			}
		}
		Time.sleepQuick();
		
		
		if (!isBoneSpawned(bigBoneTiles[2]))
			walkToTile(bigBoneTiles[2], false);
		
		timer.reset();
		while (timer.isRunning()) {
			if (shouldStop())
				return false;
			
			if (isBoneSpawned(bigBoneTiles[2]))
				break;
			
			Time.sleep(50);
			
			if (Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		if (!pickup(bigBoneTiles[2])) {
			Time.sleepQuick();
			if (walkToTile(bigBoneTiles[2], false)) {
				//waitUntilStopMoving();
				pickup(bigBoneTiles[2]);
			}
		}
		Time.sleepQuick();
		
		
		if (!isBoneSpawned(bigBoneTiles[3]))
			walkToTile(bigBoneTiles[3], false);
		
		timer.reset();
		while (timer.isRunning()) {
			if (shouldStop())
				return false;
			
			if (isBoneSpawned(bigBoneTiles[3]))
				break;
			
			Time.sleep(50);
			
			if (Antiban.doAntiban())
				Time.sleepQuickest();
		}
		
		if (!pickup(bigBoneTiles[3])) {
			Time.sleepQuick();
			if (walkToTile(bigBoneTiles[3], false)) {
				//waitUntilStopMoving();
				pickup(bigBoneTiles[3]);
			}
		}
		Time.sleepQuick();
		
		return true;
	}
	
	public boolean bury() {
		boolean success = false;
		
		Component.interactionAntiban = false; // TODO: orig set back
		if (ItemTable.INVENTORY.containsAny(532)) { // big bones
			if (InventoryWidget.get.open(true)) {
				System.out.println("open");
				Component[] items = InventoryWidget.get.getItems();
				for (Component item : items) {
					if (item.getComponentId() == 532) { // big bones
						item.interact("Bury");
						Time.sleepQuick();
						success = true;
					}
				}
			}
		}
		Component.interactionAntiban = true; // TODO: orig set back
		
		return success;
	}
	
	boolean checkedCollectionBox = false;
	
	public boolean bank() {
		if (Bank.get.getNearest() != null && Bank.get.open()) {
			
			Time.sleepQuick();
			
			if (Bank.get.depositAllInvItems(allEquipmentIds)) {
				Time.sleepQuick();
				
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					Bank.get.withdrawAll(995, false); // coins
					Time.sleepQuick();
				}
				
				boolean shouldCheckCollectionBox = false;
				
				/*if (!ItemTable.INVENTORY.containsAny(adamantFullHelmId)
						&& EquipmentSlot.HEAD.getItemId() != adamantFullHelmId
						&& !ItemTable.BANK.containsAny(adamantFullHelmId)) {
					
					System.out.println("Missing helmet! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}*/
				
				if (!ItemTable.INVENTORY.containsAny(adamantPlatebodyId)
						&& EquipmentSlot.TORSO.getItemId() != adamantPlatebodyId
						&& !ItemTable.BANK.containsAny(adamantPlatebodyId)) {
					
					System.out.println("Missing platebody! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(adamantKiteshieldId)
						&& EquipmentSlot.OFFHAND.getItemId() != adamantKiteshieldId
						&& !ItemTable.BANK.containsAny(adamantKiteshieldId)) {
					
					System.out.println("Missing shield! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (!ItemTable.INVENTORY.containsAny(adamantPlatelegsId)
						&& EquipmentSlot.LEGS.getItemId() != adamantPlatelegsId
						&& !ItemTable.BANK.containsAny(adamantPlatelegsId)) {
					
					System.out.println("Missing platelegs! Checking colleciton box and going to Grand Exchange...");
					shouldCheckCollectionBox = true;
				}
				
				if (shouldCheckCollectionBox) {
					if (Bank.get.close(true)) {
						Time.sleepMedium();
						
						if (!checkedCollectionBox && CollectionBox.get.open(true)) {
							Time.sleepMedium();
							
							if (ItemTable.GESLOT1.containsAny(allEquipmentIds) 
									|| ItemTable.GESLOT2.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT3.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT4.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT5.containsAny(allEquipmentIds)
									|| ItemTable.GESLOT6.containsAny(allEquipmentIds)) {
								
								if (CollectionBox.get.collectAllToBank())
									checkedCollectionBox = true;
								Time.sleepQuick();
								CollectionBox.get.close();
								return false;
							} else {
								if (CollectionBox.get.collectAllToBank())
									checkedCollectionBox = true;
								Time.sleepQuick();
								CollectionBox.get.close();
							}
						}
					}
					
					
					if (checkedCollectionBox) {
						exchange = true;
						return true;
					}
				}
				
				if (!retrieveEquipment()) {
					Time.sleepQuick();
					return false;
				}
				
				Bank.get.close();
				
				return true;
			}
		}/* else if (DepositBox.get.open()) {
			Time.sleepQuick();
			
			if (DepositBox.get.depositAllItems(staffOfAirId, lawRuneId, Daemonheim.ringOfKinshipId)) {
				Time.sleepVeryQuick();
				DepositBox.get.close();
				
				return true;
			}
		}*/

		
		return false;
	}
	
	boolean boughtEquipment = false;
	boolean retrievedEquipment = false;
	
	public boolean retrieveEquipment() {
		if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds)) {
			boughtEquipment = true;
			retrievedEquipment = true;
			return true;
		}
		
		if (!GrandExchangeWidget.get.close(true))
			return false;
		
		if (equip())
			return true;
		
		System.out.println("Retrieving equipment from bank...");
		
		if (Bank.get.open(true)) {
			Time.sleepMedium();
			
			if (ItemTable.BANK.containsAny(allEquipmentIds)) {
				try {
					Component.interactionAntiban = false;
					
					/*if (!ItemTable.INVENTORY.containsAny(adamantFullHelmId)
							&& EquipmentSlot.HEAD.getItemId() != adamantFullHelmId) {
						
						if (ItemTable.BANK.containsAny(adamantFullHelmId)) {
							if (!Bank.get.withdraw(adamantFullHelmId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}*/
					
					if (!ItemTable.INVENTORY.containsAny(adamantPlatebodyId)
							&& EquipmentSlot.TORSO.getItemId() != adamantPlatebodyId) {
						
						if (ItemTable.BANK.containsAny(adamantPlatebodyId)) {
							if (!Bank.get.withdraw(adamantPlatebodyId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(adamantKiteshieldId)
							&& EquipmentSlot.OFFHAND.getItemId() != adamantKiteshieldId) {
						
						if (ItemTable.BANK.containsAny(adamantKiteshieldId)) {
							if (!Bank.get.withdraw(adamantKiteshieldId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
					if (!ItemTable.INVENTORY.containsAny(adamantPlatelegsId)
							&& EquipmentSlot.LEGS.getItemId() != adamantPlatelegsId) {
						
						if (ItemTable.BANK.containsAny(adamantPlatelegsId)) {
							if (!Bank.get.withdraw(adamantPlatelegsId, 1, false)) {
								Time.sleepQuick();
								return false;
							}
							Time.sleepMedium();
						}
					}
					
				} finally {
					Component.interactionAntiban = true;
				}
				
				Time.sleepMedium();
				
				if (ItemTable.INVENTORY.containsAny(allEquipmentIds)) {
					if (Bank.get.close(true)) {
						Time.sleepQuick();
						if (equip()) {
							boughtEquipment = true;
							retrievedEquipment = true;
							return true;
						}
					}
				}
			} else
				boughtEquipment = false;
		}
		
		return false;
	}

	public boolean equip() {
		if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds)) {
			boughtEquipment = true;
			retrievedEquipment = true;
			return true;
		}
		
		// Equip specified equipment in inventory
		if (ItemTable.INVENTORY.containsAny(allEquipmentIds)) {
			if (!Bank.get.close(true))
				return false;
			
			if (Ribbon.Tab.BACKPACK.open(true)) {
				try {
					Component.interactionAntiban = false;
					
					/*Component helmet = InventoryWidget.get.getItem(adamantFullHelmId);
					if (helmet != null && !helmet.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();*/
					
					Component platebody = InventoryWidget.get.getItem(adamantPlatebodyId);
					if (platebody != null && !platebody.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component shield = InventoryWidget.get.getItem(adamantKiteshieldId);
					if (shield != null && !shield.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
					Component platelegs = InventoryWidget.get.getItem(adamantPlatelegsId);
					if (platelegs != null && !platelegs.interact("Wear")) {
						Time.sleepMedium();
						return false;
					}
					Time.sleepMedium();
					
				} finally {
					Component.interactionAntiban = true;
				}
				
				Time.sleepMedium();
				if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds))
					return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		failsafeWalkedToMine = new Failsafe("Walked to Tree Area", 900000); // 15 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 900000); // 15 mins.
		failsafeMinedRock = new Failsafe("Chop", 900000); // 15 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 900000); // 15 mins.
		//failsafes.add(failsafeWalkedToMine);
		//failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		//breakHandlerParam = new BreakHandlerParam(failsafes);
		//params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		
	}
	
	@Override
	public String getInfo() {
		return "";
	}
	
	@Override
	public boolean onStart() {
		
		loadDataParams();
		
		Antiban.resetTimer();
		
		return true;
	}
	
	Action curAction = null;
	Action lastAction = null;
	
	
	/*
	 * Script Notes
	 * 
	 * - 
	 */
	@Override
	public void run() {
		Antiban.randomMouseSpeed();
		
		if (!Client.loopLogin())
			return;
		
		
		if (!exchange) {
			if (!Bank.get.close(true))
				return;
			
			if (!GrandExchangeWidget.get.close(true))
				return;
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		equip();
		
		// TODO: uncomment after add weapon
		/*if (EquipmentSlot.MAINHAND.getItemId() == magicShieldbowId
				&& MainWidget.get.isWeaponSheathed()) {
			if (!MainWidget.get.unsheatheWeapon(true))
				return;
			Time.sleepQuick();
		}*/
		
		if (ActionBarWidget.isAutoRetaliateEnabled()) {
			if (!ActionBarWidget.get.setAutoRetaliate(false, true))
				return;
			Time.sleepQuick();
		}
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!Lodestone.WILDERNESS_VOLCANO.isActivated()) {
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			Lodestone.WILDERNESS_VOLCANO.activate();
			Time.sleepMedium();
			return;
		}
		
		/*if (!CombatMode.REVOLUTION.activate(true))
			return;
		if (!ActionBar.setup(AttackStyle.RANGED.getActionBarIndex(), new Ability[] { Ability.RICOCHET, Ability.FRAGMENTATION_SHOT, Ability.PIERCING_SHOT, Ability.BINDING_SHOT, Ability.ANTICIPATION, Ability.FREEDOM, Ability.REJUVENATE, Ability.EMPTY, Ability.EMPTY }))
			return;
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}*/
		
		//if (!ActionBar.setActionBar(AttackStyle.RANGED.getActionBarIndex()))
		//	return;
		
		if (!Settings_Gameplay_LootSettingsSubTabWidget.get.disableLootInventory(true))
			return;
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}
		
		Action action = Action.getNextAction();
		
		
		if (curAction != null)
			lastAction = curAction;
		else
			lastAction = action;
		
		curAction = action;
		
		System.out.println(action);
		
		switch (action) {
		case PICKUP:
			if (pickup())
				failsafeMinedRock.reset();
			
			break;
			
		case BURY:
			if (bury())
				failsafeDropOrBank.reset();
			
			//Time.sleepVeryQuick();
			break;
			
		case FIGHT:
			Player player = Players.getMyPlayer();
			if (player == null)
				break;
			
			// if close to southern bone tile
			if (Calculations.distance(player.getTile(), bigBoneTiles[0]) <= 5) {
				if (player.getInteracting() == null) {
					Npc skeleton = Npcs.getNearest(skeletonsAreaFilter, 92, 5338, 5339, 5340); // skeletons
					if (skeleton != null && Calculations.distanceTo(skeleton) <= 5) {
						skeleton.loopInteract(firstBoneSpawned, "Attack");
						Time.sleepQuick();
					}
				}
				
				if (Antiban.doAntiban())
					Time.sleepQuickest();
			} else {
				if (Random.nextInt(0, 100) <= 75) {
					if (Random.nextBoolean())
						Time.sleepMedium();
					else
						Time.sleepQuick();
				}
				
				if (walkToTile(bigBoneTiles[0], true))
					failsafeWalkedToMine.reset();
			}
			
			//Time.sleepQuick();
			break;
			
		case WALK:
			if (Random.nextInt(0, 100) <= 75) {
				if (Random.nextBoolean())
					Time.sleepMedium();
				else
					Time.sleepQuick();
			}
			
			if (Paths.wildernessVolcanoLStoBoneyard.getDistanceToNearest() <= 25 || Lodestone.WILDERNESS_VOLCANO.teleport(true)) {
				if (Paths.wildernessVolcanoLStoBoneyard.randomize(4, 4).loopWalk(60000, null, true)) {
					Time.sleepQuick();
						
					player = Players.getMyPlayer();
					if (player == null)
						break;
					
					Timer timer = new Timer(10000);
					while (timer.isRunning()) {
						if (boneyardArea.contains(player.getTile())) {
							Time.sleepQuick();
							failsafeWalkedToMine.reset();
							break;
						}
						
						walkToTile(bigBoneTiles[0], true);
						Time.sleepQuick();
						
						//Time.sleep(50);
					}
				}
			}
			
			//if (walkToTile(bigBoneTiles[0], true))
			//	failsafeWalkedToMine.reset();
			
			break;
			
		case BANK:
			Interactable bank = Bank.get.getNearest();
			if (bank != null && Calculations.distanceTo(bank) < 25) {
				if (bank()) {
					failsafeDropOrBank.reset();
					failsafeWalkedToBank.reset();
				} else {
					Time.sleepMedium();
					if (ItemTable.EQUIPMENT.containsAll(allEquipmentIds)) {
						failsafeDropOrBank.reset();
						failsafeWalkedToBank.reset();
					}
				}
			} else {
				//runToMonkSafeZoneIfUnderAttack();
				
				if (Lodestone.BURTHORPE.teleport(true)) {
					failsafeWalkedToBank.reset();
				}
				
				break;
			}
			
			break;
			
		case EXCHANGE:
			Npc npc = GrandExchangeWidget.get.getNearest();
			if (npc != null && Calculations.distanceTo(npc) <= 10) {
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
				
				//if (exchangeLoop.exchange()) {
					int[] buyEquipmentIdsNoted = new int[buyEquipmentIds.length];
					for (int i = 0; i < buyEquipmentIdsNoted.length; i++) {
						buyEquipmentIdsNoted[i] = buyEquipmentIds[i] + 1;
					}
					
					// break if need to equip inv items
					if (ItemTable.INVENTORY.containsAny(ArrayUtils.combine(buyEquipmentIds, buyEquipmentIdsNoted)))
						break;
					
					if (ExchangeLoop.buyEquipment(50000, new ExchangeItem[] {
							//new ExchangeItem(adamantFullHelmId, adamantFullHelmName, false).setPrice(10000),
							new ExchangeItem(adamantPlatebodyId, adamantPlatebodyName, false).setPrice(20000),
							new ExchangeItem(adamantKiteshieldId, adamantKiteshieldName, false).setPrice(10000),
							new ExchangeItem(adamantPlatelegsId, adamantPlatelegsName, false).setPrice(10000),
							})) {
						
						//if (boughtEquipment && !retrieveEquipment())
						//	break;
						
						if (!retrieveEquipment())
							break;
						
						System.out.println("Successfully bought all equipment");
						exchange = false;
					}
				//}
				
			} else {
				if (Bank.get.isOpen())
					Bank.get.close(true);
				
				if (GrandExchange.walkToSWBooth()) {
					
				}
			}
			
			break;
		}
	}
	
	@Override
	public boolean onStop() {
		return true;
	}
}
