package org.rs3.scripts;

import java.awt.AWTEvent;
import java.awt.Component;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import org.rs3.api.ItemTable;
import org.rs3.api.Skill;
import org.rs3.api.input.HardwareInput;
import org.rs3.api.input.Input;
import org.rs3.api.input.Keyboard;
import org.rs3.api.input.Mouse;
import org.rs3.api.interfaces.widgets.LoginScreen;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.OpenGLRender;
import org.rs3.api.wrappers.Render;
import org.rs3.api.wrappers.SDRender;
import org.rs3.api.wrappers.SkillData;
import org.rs3.boot.BootType;
import org.rs3.boot.Frame;
import org.rs3.boot.OfficialClientInject;
import org.rs3.callbacks.proxy.ProxySetup;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.Database;
import org.rs3.database.LevelsRow;
import org.rs3.database.StatsRow;
import org.rs3.debug.Paint;
import org.rs3.eventqueue.BlockingEventQueue;
import org.rs3.eventqueue.EventNazi;
import org.rs3.eventqueue.EventRedirect;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class ScriptHandler {
	
	public static Script currentScript = null;
	private static Script nextScript = null;
	private static List<Script> multiScripts = null;
	private static int multiScriptCurIndex = -1;
	private static boolean multiScriptMode = false;
	private static boolean changeScriptInterrupt = false;
	
	private static Timer multiScriptNextChange = null;
	
	public static long scriptStartTime = 0L;
	
	public static EventNazi nazi = null;
	public static EventRedirect redirect = null;
	
	public static void setupSingleScriptMode(Script script) {
		currentScript = script;
		multiScriptMode = false;
	}
	
	public static void setupMultiScriptMode(List<Script> scripts) {
		multiScripts = scripts;
		
		if (scripts != null) {
			if (scripts.size() > 1) {
				multiScriptMode = true;
				multiScriptNextChange = new Timer(getMultiScriptNextChange());
			} else
				setupSingleScriptMode(multiScripts.get(0));
		} else
			multiScriptMode = false;
	}
	
	public static void changeScript(Script script) {
		nextScript = script;
		changeScriptInterrupt = true;
	}
	
	private static int getMultiScriptNextChange() {
		return Random.nextInt(0.75F, 2700000, 5400000, 900000, 7200000); // 45 min, 1.5 hr, 15 min, 2 hr
	}
	
	private static boolean checkMultiScriptChange() {
		if (!multiScriptNextChange.isRunning()) {
			multiScriptCurIndex++;
			if (multiScriptCurIndex >= multiScripts.size()) {
				do {
					Collections.shuffle(multiScripts);
				} while (multiScripts.get(0) == currentScript);
				
				multiScriptCurIndex = 0;
			}
			
			changeScript(multiScripts.get(multiScriptCurIndex));
			
			multiScriptNextChange.setEndIn(getMultiScriptNextChange());
			
			return true;
		}
		
		return false;
	}
	
	public static Thread getNewScriptThread(final boolean waitForUserLogon, final boolean waitForRunTime) {
		Thread thread = new Thread() {
			@Override
			public void run() {
				if (ProxySetup.getProxy() == null) {
					System.out.println("Proxy not setup! Stopping script...");
					return;
				}
				
				if (multiScriptMode) {
					Collections.shuffle(multiScripts);
					multiScriptCurIndex = 0;
					currentScript = multiScripts.get(multiScriptCurIndex);
				}
				
				scriptStartTime = System.currentTimeMillis();
				
				try {
					if (currentScript == null) {
						System.out.println("Script not selected!");
						return;
					}
					
					if (AccountInfo.cur == null) {
						System.out.println("Account not selected!");
						return;
					}
					
					currentScript.stopInterrupt = false;
					currentScript.stopped = false;
					
					boolean loaded = false;
					Timer rsLoadingTimer = new Timer(120000); // 2 min.
					while (rsLoadingTimer.isRunning()) {
						if (Client.isRSDoneLoading()) {
							loaded = true;
							break;
						}
						
						Time.sleep(50);
					}
					
					if (!loaded) {
						System.out.println("Unable to start script! RS didn't load!");
						return;
					}
					
					if (waitForUserLogon) {
						boolean loggedIn = false;
						rsLoadingTimer.reset();
						while (rsLoadingTimer.isRunning()) {
							if (Client.isLoggedIn()) {
								System.out.println("Log in detected! Waiting 10 seconds...");
								//Time.sleep(10000, 15000);
								Timer waitTimer = new Timer(Random.nextInt(10000, 15000));
								while (waitTimer.isRunning()) {
									if (currentScript != null && currentScript.shouldStop()) {
										System.out.println("Waiting interrupted. Script stopped.");
										return;
									}
									
									Time.sleep(500);
								}
								
								if (!Client.isLoggedIn())
									continue;
								
								loggedIn = true;
								break;
							}
							
							Time.sleep(50);
						}
						
						if (!loggedIn) {
							System.out.println("Unable to start script! Not logged in!");
							return;
						}
					}
					
					if (waitForRunTime) {
						RunTimeParam runTimeParam = new RunTimeParam();
						while (!runTimeParam.onStart()) {
							System.out.println("Waiting for current time to be within time constraint...");
							Time.sleep(60000);
						}
						System.out.println("Current time is within time constraint.");
					}
					
					if (Input.useHardWareInput()) {
						// TODO
					} else
						redirect.setupListeners(currentScript);
					
					scriptStartTime = System.currentTimeMillis();
					currentScript.startTime = scriptStartTime;
					System.out.println("Script started.");
					
					Render render = Client.getRender();
					
					if (render instanceof SDRender)
						System.out.println("Render: Safe mode detected.");
					else if (render instanceof OpenGLRender)
						System.out.println("Render: OpenGL detected.");
					else
						System.out.println("Render: Unknown!");
					
					
					List<ScriptParameter> params = setupParams();
					
					if (!doOnStart(params))
						return;
					
					if (!doScriptOnStart())
						return;
					
					Timer databaseUpdateTimer = null;
					Timer levelsUpdateTimer = null;
					Timer statsUpdateTimer = null;
					Timer scriptChangeTimer = null;
					
					//Timer timer = new Timer(120000); // 2 min.
					//Timer timer = new Timer(900000); // 15 min.
					//while (timer.isRunning()) {
					while (!currentScript.shouldStop()) {
						Paint.DebugString.ScriptElapsed.setPaint(true);
						Paint.DebugString.ScriptInfo.setPaint(true);
						
						if (databaseUpdateTimer == null || !databaseUpdateTimer.isRunning()) {
							AccountRow row = AccountInfo.cur.getRow();
							if (row != null) {
								databaseUpdateTimer = new Timer(30000);
								
								row.setLastScriptUpdate(new Timestamp(System.currentTimeMillis()));
								row.executeUpdate();
							}
						}
						
						if (levelsUpdateTimer == null || !levelsUpdateTimer.isRunning()) {
							if (Client.isLoggedIn() && Database.isConnectionValid(2000)) {
								// TODO: test, try to test skill data using a more efficient method	
								if (Skill.ATTACK.getRealLevel() != -1) {
									levelsUpdateTimer = new Timer(300000); // 5 mins
									
									LevelsRow row = new LevelsRow();
									row.setAccountId(AccountInfo.cur.getId());
									row.setAttack(Skill.ATTACK.getRealLevel());
									row.setStrength(Skill.STRENGTH.getRealLevel());
									row.setDefence(Skill.DEFENSE.getRealLevel());
									row.setRanged(Skill.RANGED.getRealLevel());
									row.setPrayer(Skill.PRAYER.getRealLevel());
									row.setMagic(Skill.MAGIC.getRealLevel());
									row.setConstitution(Skill.CONSTITUTION.getRealLevel());
									row.setCrafting(Skill.CRAFTING.getRealLevel());
									row.setMining(Skill.MINING.getRealLevel());
									row.setSmithing(Skill.SMITHING.getRealLevel());
									row.setFishing(Skill.FISHING.getRealLevel());
									row.setCooking(Skill.COOKING.getRealLevel());
									row.setFiremaking(Skill.FIREMAKING.getRealLevel());
									row.setWoodcutting(Skill.WOODCUTTING.getRealLevel());
									row.setRunecrafting(Skill.RUNECRAFTING.getRealLevel());
									row.setDungeoneering(Skill.DUNGEONEERING.getRealLevel());
									row.setAgility(Skill.AGILITY.getRealLevel());
									row.setHerblore(Skill.HERBLORE.getRealLevel());
									row.setThieving(Skill.THIEVING.getRealLevel());
									row.setFletching(Skill.FLETCHING.getRealLevel());
									row.setSlayer(Skill.SLAYER.getRealLevel());
									row.setFarming(Skill.FARMING.getRealLevel());
									row.setConstruction(Skill.CONSTRUCTION.getRealLevel());
									row.setHunter(Skill.HUNTER.getRealLevel());
									row.setSummoning(Skill.SUMMONING.getRealLevel());
									row.setDivination(Skill.DIVINATION.getRealLevel());
									row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
									
									if (LevelsRow.select(AccountInfo.cur.getId()) != null)
										row.executeUpdate();
									else
										row.executeInsert();
								}
							}
						}
						
						if (statsUpdateTimer == null || !statsUpdateTimer.isRunning()) {
							if (Client.isLoggedIn() && Database.isConnectionValid(2000)) {
								statsUpdateTimer = new Timer(300000); // 5 mins
								
								boolean isAlreadyRow = true;
								StatsRow row = StatsRow.select(AccountInfo.cur.getId());
								if (row == null) {
									isAlreadyRow = false;
									row = new StatsRow();
									row.setAccountId(AccountInfo.cur.getId());
								}
								
								row.setMoneyPouch(ItemTable.MONEYPOUCH.getCoinCount());
								row.setLastUpdate(new Timestamp(System.currentTimeMillis()));
								
								if (isAlreadyRow)
									row.executeUpdate();
								else
									row.executeInsert();
							}
						}
						
						if (!BootType.get().equals(BootType.NORMAL)) {
							if (scriptChangeTimer == null || !scriptChangeTimer.isRunning()) {
								scriptChangeTimer = new Timer(30000);
								
								Script script = OfficialClientInject.loadCurrentScript();
								if (script != null) {
									if (!script.getClass().equals(currentScript.getClass())) {
										System.out.println("Detected database script change");
										nextScript = script;
										changeScriptInterrupt = true;
									}
								}
							}
						}
						
						if (multiScriptMode) {
							if (checkMultiScriptChange())
								System.out.println("Multi script mode: running next script...");
						}
						
						if (changeScriptInterrupt) {
							changeScriptInterrupt = false;
							
							if (nextScript != null) {
								currentScript.stopInterrupt = true;
								
								if (!doOnStop(params))
									return;
								
								if (!doScriptOnStop())
									return;
								
								System.out.println("Changing script...");
								
								currentScript = nextScript;
								currentScript.stopInterrupt = false;
								currentScript.stopped = false;
								
								params = setupParams();
								
								if (!doOnStart(params))
									return;
								
								if (!doScriptOnStart())
									return;
							}
						}
						
						if (Input.useHardWareInput()) {
							if (BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
								// TODO
							} else {
								if (!Frame.window.activated) {
									Time.sleep(50);
									continue;
								}
							}
						}
						
						boolean success = true;
						if (params != null) {
							for (ScriptParameter param : params) {
								switch (param.run()) {
								case OK:
									break;
								case FAIL:
									System.out.println("Failed Script Param: " + param.getClass().getName());
									success = false;
									break;
								case STOPSCRIPT:
									success = false;
									System.out.println("Stopped Script Param: " + param.getClass().getName());
									System.out.println("Error: Script param run() failed!");
									
									if (param instanceof RunTimeParam && !AccountInfo.cur.getRow().isWithinTimeConstraint()) {
										System.out.println("Time is outside of time contstraint. Searching for a new account...");
										if (BootType.get().equals(BootType.NORMAL)) {
											Frame.window.loadAccountInfo();
											//Frame.window.loadScriptInfo();
											OfficialClientInject.loadScriptInfo();
										} else if (BootType.get().equals(BootType.OFFICIAL_CLIENT)) {
											OfficialClientInject.loadAccountInfo();
											OfficialClientInject.loadScriptInfo();
										}
										
										if (!LoginScreen.get.isOpen()) {
											Timer timer = new Timer(120000);
											while (timer.isRunning()) {
												if (LoginScreen.get.isOpen())
													break;
												
												Client.logout(false);
												Time.sleepMedium();
												
												Time.sleep(50);
											}
										}
										
										if (LoginScreen.get.isOpen())
											ScriptHandler.runScriptThreaded(false, true);
										else
											System.out.println("Unable to logout!");
									}
									
									return;
								}
								
								if (!success)
									break;
							}
						}
						
						if (!success)
							continue;
						
						currentScript.run();
						Time.sleep(50, 100);
					}
					
					currentScript.interruptStop();
					
					
					if (!doOnStop(params))
						return;
					
					if (!doScriptOnStop())
						return;
				} catch (Throwable t) {
					System.out.println("Script exception!");
					t.printStackTrace();
				} finally {
					currentScript.stopped = true;
					currentScript.stopInterrupt = false;
					
					if (Input.useHardWareInput()) {
						// TODO
					} else
						redirect.resetListeners();
					
					System.out.println("Script stopped.");
					System.out.println("Script ran for: " + Time.format(System.currentTimeMillis() - scriptStartTime));
					Paint.DebugString.ScriptElapsed.setPaint(false);
					Paint.DebugString.ScriptInfo.setPaint(false);
				}
			}
		};
		
		return thread;
	}
	
	private static List<ScriptParameter> setupParams() {
		if (currentScript == null)
			return null;
		
		List<ScriptParameter> params;
		
		if (!currentScript.useConstructorParams) {
			params = currentScript.setupParams();
			currentScript.params = params;
		} else
			params = currentScript.params;
		
		return params;
	}
	
	private static boolean doOnStart(List<ScriptParameter> params) {
		boolean success = true;
		
		if (params != null) {
			for (ScriptParameter param : params) {
				if (!param.onStart()) {
					System.out.println("Failed Script Param onStart(): " + param.getClass().getName());
					success = false;
					break;
				}
			}
		}
		
		if (!success) {
			System.out.println("Error: Script parameter onStart() failed!");
			//return;
		}
		
		return success;
	}
	
	private static boolean doOnStop(List<ScriptParameter> params) {
		boolean success = true;
		
		if (params != null) {
			for (ScriptParameter param : params) {
				if (!param.onStop()) {
					System.out.println("Failed Script Param onStop(): " + param.getClass().getName());
					success = false;
					break;
				}
			}
		}
		
		if (!success) {
			System.out.println("Error: Script parameter onStop() failed!");
			//return;
		}
		
		return success;
	}
	
	private static boolean doScriptOnStart() {
		boolean success = false;
		
		Timer scriptStartTimer = new Timer(100000);
		while (scriptStartTimer.isRunning()) {
			if (currentScript.onStart()) {
				success = true;
				Time.sleep(500, 600);
				break;
			}
			
			Time.sleep(500);
		}
		
		if (!success) {
			System.out.println("Error: Script onStart() failed!");
			//return;
		}
		
		return success;
	}
	
	private static boolean doScriptOnStop() {
		boolean success = false;
		
		Timer scriptStopTimer = new Timer(100000);
		while (scriptStopTimer.isRunning()) {
			if (currentScript.onStop()) {
				success = true;
				Time.sleep(500, 600);
				break;
			}
			
			Time.sleep(500);
		}
		
		if (!success) {
			System.out.println("Error: Script onStop() failed!");
			//return;
		}
		
		return success;
	}
	
	public static Thread runScriptThreaded(boolean waitForUserLogon, boolean waitForRunTime) {
		Thread thread = getNewScriptThread(waitForUserLogon, waitForRunTime);
		thread.start();
		return thread;
	}
	
	public static void setupInput(final Component topLevelComp, String initseq) {
		if (Input.useHardWareInput()) {
			HardwareInput hardwareInput = new HardwareInput();
	        Mouse.setup(hardwareInput);
	        Keyboard.setup(hardwareInput);
	        
	        if (initseq != null)
	        	hardwareInput.sendKeys(initseq, 90, 60);
		} else {
			setupEventQueue(topLevelComp, initseq);
		}
	}
	
	// TODO: not sure if topLevelComp (frame) should request focus or the runescape comp
	private static void setupEventQueue(final Component topLevelComp, String initseq) {	
		BlockingEventQueue.removeAllComponents();
		
		java.awt.Component c = (java.awt.Component) Client.getCanvas().getObject();
		
		ScriptHandler.redirect = new EventRedirect() {
            @Override
            public void dispatched(AWTEvent e) {
            	topLevelComp.requestFocusInWindow(); //any event should cause a refocus
            }
        };
        
        redirect.setupListeners(currentScript);
        
        BlockingEventQueue.addComponent(c, ScriptHandler.redirect);
        BlockingEventQueue.ensureBlocking();
        BlockingEventQueue.setBlocking(c, true);
        
        topLevelComp.requestFocusInWindow();
        
        BlockingEventQueue.setBlocking(c, true);
        BlockingEventQueue.ensureBlocking();
        
        nazi = EventNazi.getNazi(c);
        
        if (initseq != null)
        	nazi.sendKeys(initseq, 90, 60);
        
        Mouse.setup(nazi);
        Keyboard.setup(nazi);
	}
	

}
