package org.rs3.scripts;

import org.rs3.util.Timer;

public class Failsafe {

	Timer timer;
	long period;
	
	String name;
	
	public Failsafe(String name, long period) {
		this.timer = new Timer(period);
		this.period = period;
		
		this.name = name;
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	public long getPeriod() {
		return period;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isRunning() {
		return timer.isRunning();
	}
	
	public void reset() {
		timer.reset();
	}
}
