package org.rs3.scripts;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.Constants;
import org.rs3.api.GESlot;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Players;
import org.rs3.api.Skill;
import org.rs3.api.Toolbelt;
import org.rs3.api.Walking;
import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.exchange.ExchangeItem;
import org.rs3.api.exchange.ExchangeLoop;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.objects.AreaPath;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.callbacks.ItemTableCallback;
import org.rs3.callbacks.ItemTableListener;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.GrandExchangeRow;
import org.rs3.debug.Paint;
import org.rs3.scripts.Miner.SubAction;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.FailsafesParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.tanning.HideType;
import org.rs3.scripts.woodcutting.ChopLocation;
import org.rs3.scripts.woodcutting.LogType;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Woodcutter extends Script {
	
	private static final int runeHatchetId = 1359;
	private static final String runeHatchetName = "Rune hatchet";
	
	private boolean autoMode = false;
	private Timer autoModeTimer = new Timer(60000);
	
	private static boolean exchange = false;
	private static ExchangeLoop exchangeLoop = new ExchangeLoop(-1, new ExchangeItem[] {
			new ExchangeItem(LogType.NORMAL.getItemId(), LogType.NORMAL.getItemName(), true),
			new ExchangeItem(LogType.OAK.getItemId(), LogType.OAK.getItemName(), true)
	});
	
	private int logsAccumulated;
	private int oakLogsAccumulated;
	
	private ItemTableListener itemTableListener = new ItemTableListener() {
		
		@Override
		public void replace(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			//System.out.println("replace[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize + " with old = " + originalItemId + ", " + originalItemStackSize);
		}
		
		@Override
		public void remove(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("remove[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			
		}
		
		@Override
		public void increase(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("increase[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void decrease(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("decrease[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void add(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("add[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
			
			if (itemTableId == ItemTable.INVENTORY.getId()) {
				if (itemId == LogType.NORMAL.getItemId())
					logsAccumulated++;
				else if (itemId == LogType.OAK.getItemId())
					oakLogsAccumulated++;
			}
		}
	};
	
	private enum Action {
		
		CHOP(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Player player = Players.getMyPlayer();
				if (player == null)
					return null;
				
				if (ChopLocation.getCurrent().getMainChopArea().contains(player.getData().getCenterLocation().getTile(false)))
					return SubAction.DEFAULT;
				else if (ChopLocation.getCurrent().getMainChopArea().getDistanceToNearest() < 25)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		DROP(EnumSet.of(SubAction.DEFAULT)) {
			@Override
			public SubAction getSubAction() {
				return SubAction.DEFAULT;
			}
		},
		
		BANK(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Player player = Players.getMyPlayer();
				if (player == null)
					return null;
				
				Interactable bank = Bank.get.getNearest();
				if (bank != null && Calculations.distanceTo(bank) < 25)
					return SubAction.DEFAULT;
				else if (Calculations.distance(player.getData().getCenterLocation().getTile(false), ChopLocation.getCurrent().getDepositBoxTile()) <= 25)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		EXCHANGE(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Npc npc = GrandExchangeWidget.get.getNearest();
				if (npc != null && Calculations.distanceTo(npc) <= 10)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		};
		
		EnumSet<SubAction> possibleSubs;
		
		Action(EnumSet<SubAction> possibleSubs) {
			this.possibleSubs = possibleSubs;
		}
		
		public static Action getNextAction() {
			if (!exchange) {
				if (exchangeLoop.getMainExchangeTimer() != null && !exchangeLoop.getMainExchangeTimer().isRunning()) {
					exchange = true;
					return EXCHANGE;
				}
					
				if (ItemTable.INVENTORY.countAllItems(false) >= 28)
					//return DROP;
					return BANK;
				else
					return CHOP;
			} else
				return EXCHANGE;
		}
		
		public abstract SubAction getSubAction();
	}
	
	public enum SubAction {
		DEFAULT, WALK
	};
	
	Filter<GroundObject> treesAreaFilter = new Filter<GroundObject>() {
		@Override
		public boolean accept(GroundObject animable) {
			if (LogType.getCurrent().getArea().contains(animable.getTile()))
				return true;
			
			return false;
		}
	};
	
	Condition treeIsLikelyDisposed = new Condition() {
		@Override
		public boolean evaluate() {
			return curAo == null || curAo.isDisposed();
		}
	};
	
	Condition treeIsDisposedDefinite = new Condition() {
		@Override
		public boolean evaluate() {
			if (curAo == null)
				return true;
			
			GroundObject[] objects = GroundObjects.getAllAtLocal(GroundObject.Type.Animable, curAaoRelativeTile.x, curAaoRelativeTile.y);
			if (objects != null) {
				if (objects[0] instanceof AnimableObject)
					return false;
			} else
				return true;
			
			return false;
		}
	};
	
	Condition treeIsDisposed_bothChecks = new Condition() {
		@Override
		public boolean evaluate() {
			if (treeIsLikelyDisposed.evaluate() || treeIsDisposedDefinite.evaluate())
				return true;
			
			return false;
		}
	};
	
	Condition treeIsDisposed_clearCur = new Condition() {
		@Override
		public boolean evaluate() {
			if (treeIsDisposed_bothChecks.evaluate()) {
				curAo = null;
				return true;
			}
			
			return false;
		}
	};
	
	Condition notNextToTree = new Condition() {
		@Override
		public boolean evaluate() {
			if (curAaoAbsoluteTile == null || Calculations.distanceTo(curAaoAbsoluteTile) > 2)
				return true;
			
			return false;
		}
	};
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public boolean walkToTreeArea() {
		return LogType.getCurrent().getArea().loopWalkTo(10000);
	}
	
	public AnimableObject getNearestTree() {
		
		RSAnimable animable = null;
		
		Timer timer = new Timer(2000);
		while (timer.isRunning()) {
			animable = (RSAnimable) GroundObjects.getNearest(GroundObject.Type.Animable, treesAreaFilter, LogType.getCurrent().getObjectIds());
			
			if (animable != null) {
				if (animable instanceof AnimableObject)
					return (AnimableObject) animable;
			}
			
			Time.sleep(50);
		}
		
		return null;
	}
	
	public boolean waitUntilStartChopping(boolean breakOnTreeChopped, boolean doAntiban) {
		
		double dist = Calculations.distanceTo(curAaoAbsoluteTile);
		int maxPeriod;
		
		if (dist >= 5)
			maxPeriod = 5000;
		else if (dist > 2.5)
			maxPeriod = 4000;
		else if (dist > 1.5)
			maxPeriod = 3000;
		else
			maxPeriod = 2500;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		return player.waitForAnimation(Constants.Animation.WOODCUTTING.getIds(), maxPeriod, doAntiban, breakOnTreeChopped ? treeIsDisposed_clearCur: null);
	}
	
	public boolean waitUntilStopChopping(boolean breakOnTreeChopped, boolean doAntiban) {
		
		Condition[] breakConditions;
		if (breakOnTreeChopped)
			breakConditions = new Condition[] {treeIsDisposed_bothChecks, notNextToTree};
		else
			breakConditions = new Condition[] {notNextToTree};
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		return player.waitForIdleAnimation(Constants.Animation.WOODCUTTING.getIds(), 60000, doAntiban, breakConditions);
	}
	
	public boolean chop() {
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		Tab.closeAll();
		
		if (!walkToTreeArea())
			return false;
		
		AnimableObject ao = getNearestTree();
		
		if (ao == null)
			return false;
		
		curAo = ao;
		curAaoLoc = ao.getData().getCenterLocation();
		curAaoAbsoluteTile = curAaoLoc.getGlobalTile(true);
		curAaoRelativeTile = curAaoLoc.getLocalTile(true);
		
		Paint.updatePaintObject(ao);
		
		Model model = ao.getModel();
		
		if (model == null || !curAaoAbsoluteTile.isOnScreen()) {
			Tile walkTile = null;
			
			Timer walkTimer = new Timer(5000);
			while (walkTimer.isRunning() && (walkTile == null || !LogType.getCurrent().getArea().contains(walkTile))) {
				walkTile = curAaoAbsoluteTile.randomize(2, 2);
			}
			
			walkTimer.reset();
			while (walkTimer.isRunning() && !walkTile.isOnScreen()) {
				//System.out.println("walking");
				
				Player player = Players.getMyPlayer();
				if (player == null)
					return false;
				
				if (!player.isMoving() || Calculations.distanceTo(Walking.getDestination()) <= 7)
					Walking.walk(Walking.getClosestOnMap(walkTile));
				
				Time.sleepMedium();
			}
			
			if (curAo.isDisposed())
				return false;
		}
		
		if (Camera.getPitch() < 40) {
			Camera.setPitch(Random.nextInt(40, 74));
			Time.sleepVeryQuick();
		}
		
		if (ao.loopInteract(treeIsLikelyDisposed, "Chop down", LogType.getCurrent().getMenuOption(), true))
			return waitUntilStartChopping(true, true);
		
		return false;
	}
	
	public boolean bank() {
		
		if (DepositBox.get.open()) {
			Time.sleepQuick();
			
			if (DepositBox.get.depositAllItems(runeHatchetId)) {
				Time.sleepQuick();
				DepositBox.get.close();
				
				return true;
			}
		}
		
		return false;
	}
	
	boolean boughtRuneHatchet = true;
	boolean retrievedRuneHatchet = false;
	
	public boolean retrieveRuneHatchet() {
		if (Toolbelt.Hatchet.RUNE.equipped())
			return true;
		
		if (retrievedRuneHatchet)
			return true;
		
		if (!GrandExchangeWidget.get.close(true))
			return false;
		
		if (equipRuneHatchet())
			return true;
		
		System.out.println("Retrieving rune hatchet from bank...");
		
		if (Bank.get.open(true)) {
			Time.sleepQuick();
			
			if (ItemTable.BANK.containsAny(runeHatchetId)) {
				if (!Bank.get.withdraw(runeHatchetId, 1, false))
					return false;
				
				Time.sleepMedium();
				
				if (ItemTable.INVENTORY.containsAny(runeHatchetId)) {
					if (Bank.get.close(true)) {
						Time.sleepQuick();
						if (equipRuneHatchet()) {
							retrievedRuneHatchet = true;
							return true;
						}
					}
				}
			} else
				boughtRuneHatchet = false;
		}
		
		return false;
	}

	public boolean equipRuneHatchet() {
		if (Toolbelt.Hatchet.RUNE.equipped())
			return true;
		
		// Add rune hatchet to toolbelt if it's in inventory
		if (ItemTable.INVENTORY.containsAny(runeHatchetId)) {
			if (!Bank.get.close(true))
				return false;
			
			if (Ribbon.Tab.BACKPACK.open(true)) {
				Component hatchet = InventoryWidget.get.getItem(runeHatchetId);
				if (!hatchet.interact("Add to tool belt")) {
					Time.sleepMedium();
					return false;
				}
				Time.sleepMedium();
				if (Toolbelt.Hatchet.RUNE.equipped())
					return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		failsafeWalkedToMine = new Failsafe("Walked to Tree Area", 900000); // 15 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 900000); // 15 mins.
		failsafeMinedRock = new Failsafe("Chop", 900000); // 15 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 900000); // 15 mins.
		failsafes.add(failsafeWalkedToMine);
		failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		breakHandlerParam = new BreakHandlerParam(failsafes);
		params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		boolean success = false;
		if (AccountInfo.cur != null) {
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Map<String, String> params = row.getScriptParams_toMap();
				String logType = params.get("LogType");
				if (logType != null) {
					success = true;
					
					logType = logType.toLowerCase();
					if (logType.contains("normal"))
						LogType.setCurrent(LogType.NORMAL);
					else if (logType.contains("auto"))
						autoMode = true;
					else
						success = false;
				}
				
				if (success) {
					String chopLocation = params.get("ChopLocation");
					if (chopLocation != null) {
						success = true;
						
						chopLocation = chopLocation.toLowerCase();
						if (chopLocation.contains("rimmington"))
							ChopLocation.setCurrent(ChopLocation.RIMMINGTON);
						else if (chopLocation.contains("auto"))
							autoMode = true;
						else
							success = false;
					}
				}
			}
		}
		
		if (!success) {
			System.out.println("Woodcutter: Error loading params! Loaded default data params.");
			//LogType.setCurrent(LogType.Normal);
			//ChopLocation.setCurrent(ChopLocation.RIMMINGTON);
			autoMode = true;
		} else {
			System.out.println("Woodcutter: Successfully loaded data params: ");
		}
		
		if (!autoMode) {
			System.out.println("\t-LogType: " + LogType.getCurrent().name());
			System.out.println("\t-ChopLocation: " + ChopLocation.getCurrent().name());
		} else
			System.out.println("\t-Auto mode enabled");
	}
	
	public int getLogsPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int totalLogs = logsAccumulated + oakLogsAccumulated;
			int logsPerHour = (int) (totalLogs / timeDiffHours);
			return logsPerHour;
		}
		
		return 0;
	}
	
	public int getGpPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int logsGp = logsAccumulated*178;
			int oakGp = oakLogsAccumulated*71;
			int totalGp = logsGp + oakGp;
			int gpPerHour = (int) (totalGp / timeDiffHours);
			return gpPerHour;
		}
		
		return 0;
	}
	
	@Override
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("-Woodcutter-");
		sb.append(System.lineSeparator());
		
		if (logsAccumulated > 0) {
			sb.append("Logs accumulated: " + logsAccumulated);
			sb.append(System.lineSeparator());
		}
		if (oakLogsAccumulated > 0) {
			sb.append("Oak logs accumulated: " + oakLogsAccumulated);
			sb.append(System.lineSeparator());
		}
		
		sb.append("Logs/hr: " + getLogsPerHour());
		sb.append(System.lineSeparator());
		
		sb.append("GP/hr: " + getGpPerHour() / 1000 + "k");
		sb.append(System.lineSeparator());
		
		return sb.toString();
	}
	
	@Override
	public boolean onStart() {
		
		logsAccumulated = 0;
		oakLogsAccumulated = 0;
		
		ItemTableCallback.addListener(itemTableListener);
		
		loadDataParams();
		
		Antiban.resetTimer();
		
		return true;
	}
	
	AnimableObject curAo = null;
	Location curAaoLoc = null;
	Tile curAaoAbsoluteTile = null;
	Tile curAaoRelativeTile = null;
	
	Action curAction = null;
	Action lastAction = null;
	
	
	/*
	 * Script Notes
	 * 
	 * - 
	 */
	@Override
	public void run() {
		
		Antiban.randomMouseSpeed();
		
		if (!Client.loopLogin())
			return;
		
		if (autoMode) {
			if (!autoModeTimer.isRunning() || LogType.getCurrent() == null) {
				//if (Skill.WOODCUTTING.getCurrentLevel() >= 15) // oak logs
				//	LogType.setCurrent(LogType.Oak);
				//else
				LogType.setCurrent(LogType.NORMAL);
				ChopLocation.setCurrent(ChopLocation.RIMMINGTON);
				
				autoModeTimer.reset();
			}
		}
		
		if (!exchange && !exchangeLoop.getMainExchangeTimer().isRunning()) {
			if (!Toolbelt.Hatchet.RUNE.equipped())
				exchangeLoop.setMainExchangePeriod(Random.nextInt(3600000, 5400000)); // 1 to 1.5 hrs
			else
				exchangeLoop.setMainExchangePeriod(Random.nextInt(10800000, 18000000)); // 3 to 5 hrs
		}
		
		if (!DepositBox.get.close(true))
			return;
		
		if (!exchange) {
			if (!Bank.get.close(true))
				return;
			
			if (!GrandExchangeWidget.get.close(true))
				return;
		}
		
		if (curAo != null) {
			waitUntilStopChopping(true, true);
			curAo = null;
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		equipRuneHatchet();
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!Lodestone.PORT_SARIM.isActivated()) {
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			Lodestone.PORT_SARIM.activate();
			Time.sleepMedium();
			return;
		}
		
		Action action = Action.getNextAction();
		SubAction subAction = action.getSubAction();
		
		
		if (curAction != null)
			lastAction = curAction;
		else
			lastAction = action;
		
		curAction = action;
		
		if (curAction.equals(Action.CHOP) && lastAction.equals(Action.BANK)) {
			subAction = SubAction.WALK;
		}
		
		System.out.println(action + ", " + subAction);
		
		switch (action) {
		case CHOP:
			switch (subAction) {
			case DEFAULT:
				if (!chop())
					curAo = null;
				else
					failsafeMinedRock.reset();
				
				break;
			case WALK:
				curAo = null;
				
				Player player = Players.getMyPlayer();
				if (player == null)
					break;
				
				if (ChopLocation.getCurrent().getMainChopArea().getDistanceToNearest() < 100) {
					if (Random.nextInt(0, 100) <= 75) {
						if (Random.nextBoolean())
							Time.sleepMedium();
						else
							Time.sleepQuick();
					}
					
					TilePath path = new TilePath(AreaPath.getLine(player.getData().getCenterLocation().getTile(true), ChopLocation.getCurrent().getMainChopArea().getRandomTile()).toArray(new Tile[0]));
					path.randomize(10, 10);
					
					if (path.loopWalk(90000, handledBreak, true)) // 90s max
						failsafeWalkedToMine.reset();
				} else {
					if (Lodestone.PORT_SARIM.teleport(true)) {
						
					}
				}
				
				break;
			}
			
			break;
		case DROP:
			curAo = null;
			
			InventoryWidget.get.dropAllItems(runeHatchetId); // runeHatchetId = rune hatchet
			failsafeDropOrBank.reset();
			
			failsafeWalkedToMine.reset();
			failsafeWalkedToBank.reset();
			
			Time.sleepVeryQuick();
			break;
		case BANK:
			curAo = null;
			
			switch (action.getSubAction()) {
			case DEFAULT:
				if (bank()) {
					failsafeDropOrBank.reset();
					failsafeWalkedToBank.reset();
				}
				
				break;
			case WALK:
				Player player = Players.getMyPlayer();
				if (player == null)
					break;
				
				if (Calculations.distance(player.getData().getCenterLocation().getTile(false), ChopLocation.getCurrent().getDepositBoxTile()) < 100) {
					if (Random.nextInt(0, 100) <= 75) {
						if (Random.nextBoolean())
							Time.sleepMedium();
						else
							Time.sleepQuick();
					}
					
					TilePath path = ChopLocation.getCurrent().getRandomPathToBank();
					
					if (path.loopWalk(90000, handledBreak, true)) // 90s max
						failsafeWalkedToBank.reset();
				} else {
					if (Lodestone.PORT_SARIM.teleport(true)) {
						
					}
				}
				
				break;
			}
			
			break;
		case EXCHANGE:
			switch (action.getSubAction()) {
			case DEFAULT:
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();

				if (exchangeLoop.exchange()) {
					// equip hatchet if bought
					if (!Toolbelt.Hatchet.RUNE.equipped() && GrandExchangeWidget.get.open(true)) {
						Time.sleepQuick();
						
						// buy hatchet
						if (!boughtRuneHatchet && !retrievedRuneHatchet && !Toolbelt.Hatchet.RUNE.equipped() && ItemTable.MONEYPOUCH.getCoinCount() >= 20000 && !ItemTable.INVENTORY.containsAny(runeHatchetId) && !ItemTable.INVENTORY.containsAny(runeHatchetId + 1)) {
							if (!GESlot.anyContainItem(runeHatchetId)) {
								System.out.println("Buying hatchet...");
								if (GESlot.getRandomEmptySlot().buyByMarketPrice(runeHatchetId, runeHatchetName, 1, Random.nextInt(3, 5)))
									boughtRuneHatchet = true;
								else
									break;
								
								Time.sleepMedium();
							}
						}
						
						// Wait to see if it sells, then collect
						if (boughtRuneHatchet && !retrievedRuneHatchet && !Toolbelt.Hatchet.RUNE.equipped()) {
							// make sure there is an offer
							boolean offerExists = false;
							Timer timer = new Timer(5000);
							while (timer.isRunning()) {
								if (GESlot.anyContainItem(runeHatchetId)) {
									offerExists = true;
									break;
								}
								
								Time.sleep(50);
							}
							
							if (offerExists) {
								timer = new Timer(300000); // 5 mins
								while (timer.isRunning()) {
									GESlot hatchetSlot = GESlot.findByItemId(runeHatchetId);
									if (hatchetSlot != null && hatchetSlot.getStatus().isFinalized()) {
										Time.sleepQuick();
										break;
									}
									
									Time.sleep(5000);
								}
							}
						}
						
						// Retrieve rune hatchet if in GE (regardless if it's been bought during this script)
						GESlot hatchetSlot = GESlot.findByItemId(runeHatchetId);
						if (hatchetSlot != null && hatchetSlot.getStatus().isFinalized()) {
							boughtRuneHatchet = true;
							
							if (!hatchetSlot.collect(false))
								break;
							
							Time.sleepMedium();
							Time.sleepQuick();
						}
						
						if (boughtRuneHatchet && !retrieveRuneHatchet())
							break;
					} else if (!Toolbelt.Hatchet.RUNE.equipped() && ItemTable.MONEYPOUCH.getCoinCount() >= 20000)
						break;
					
					exchange = false;
				}
				
				break;
			case WALK:
				if (Bank.get.isOpen())
					Bank.get.close(true);
				
				if (GrandExchange.walkToSWBooth()) {
					
				}
				
				break;
			}
			
			break;
		}
		
		//Time.sleep(50);
		
		//System.gc();
	}
	
	@Override
	public boolean onStop() {
		ItemTableCallback.removeListener(itemTableListener);
		
		System.out.println("Final Logs/hr: " + getLogsPerHour());
		System.out.println("Final GP/hr: " + getGpPerHour() + " (" + getGpPerHour() / 1000 + "k)");
		
		return true;
	}
}
