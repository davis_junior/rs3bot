package org.rs3.scripts;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import org.rs3.api.Ability;
import org.rs3.api.ActionBar;
import org.rs3.api.ActionSlot;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.CombatMode;
import org.rs3.api.EquipmentSlot;
import org.rs3.api.GroundItems;
import org.rs3.api.GroundObjects;
import org.rs3.api.Interfaces;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Npcs;
import org.rs3.api.Players;
import org.rs3.api.Shop;
import org.rs3.api.Skill;
import org.rs3.api.Spell;
import org.rs3.api.Stats;
import org.rs3.api.Walking;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.interfaces.widgets.ActionBarWidget;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MainWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Settings_Gameplay_LootSettingsSubTabWidget;
import org.rs3.api.interfaces.widgets.ShopWidget;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.objects.Area;
import org.rs3.api.objects.GroundItem;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.GroundObject.Type;
import org.rs3.api.wrappers.BaseInfo;
import org.rs3.api.wrappers.Character;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.debug.Paint;
import org.rs3.scripts.combat.AttackStyle;
import org.rs3.scripts.combat.CombatLocation;
import org.rs3.scripts.combat.EnemyType;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.tanning.HideType;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

import com.sun.corba.se.spi.legacy.connection.GetEndPointInfoAgainException;

public class Combat extends Script {
	
	public static final int staffOfAirId = 1381;
	public static final int bronzeSwordId = 1277;
	public static final int rune2hSwordId = 1319;
	public static final int chargebowId = 19830;
	//public static final int runeArmorSetId = 11838;
	public static final int runeFullHelmId = 1163;
	public static final int runeChainbodyId = 1113;
	public static final int runePlatelegsId = 1079;
	
	public static final String staffOfAirName = "Staff of air";
	public static final String bronzeSwordName = "Bronze sword";
	public static final String rune2hSwordName = "Rune 2h sword";
	public static final String chargebowName = "Chargebow";
	//public static final String runeArmorSetName = "Rune armour set (lg)";
	public static final String runeFullHelmName = "Rune full helm";
	public static final String runeChainbodyName = "Rune chainbody";
	public static final String runePlatelegsName = "Rune platelegs";
	
	private boolean tempOverrideParams = false;
	private EnemyType origEnemyType = null;
	private CombatLocation origCombatLocation = null;
	private AttackStyle origAttackStyle = null;
	
	private boolean buyGnomeEquipment = false;
	
	private boolean autoMode = false;
	private Timer autoModeTimer = new Timer(60000);
	
	private static boolean exchange = false;
	
	private enum Action {
		
		FIGHT, 
		BANK, 
		WALK, 
		;
		
		public static Action getNextAction() {
			if (!AttackStyle.getCurrent().isEquipmentSetup())
				return BANK;
			
			Player player = Players.getMyPlayer();
			if (player == null)
				return null;
			
			boolean contains = EnemyType.getCurrent().getArea().contains(player.getTile());
			
			if (contains || EnemyType.getCurrent().getArea().getDistanceToNearest() < 25) {
				if (Stats.HEALTH.getPercentage() < 20) {
					// TODO: eat food if in backpack
					
					return BANK;
				}
				
				if (EnemyType.getCurrent().equals(EnemyType.GIANT_SPIDER)) {
					if (!contains) // possibly not in area though distance is under 25
						return WALK;
				}
				
				if (ItemTable.INVENTORY.countAllItems(false) >= 28)
					return BANK;
				else
					return FIGHT;
			} else {
				if (Bank.get.getNearest() != null && Calculations.distanceTo(Bank.get.getNearest()) < 25 && Stats.HEALTH.getPercentage() < 100)
					return BANK;
				else if (ItemTable.INVENTORY.countAllItems(false) >= 28)
					return BANK;
				else
					return WALK;
			}
		}
	}
	
	Area strongholdFloor3SWladder = new Area( new Tile(2117, 5259, 0), new Tile(2118, 5259, 0), new Tile(2118, 5260, 0), new Tile(2119, 5260, 0), new Tile(2120, 5260, 0), new Tile(2121, 5260, 0), new Tile(2121, 5259, 0), new Tile(2122, 5259, 0), new Tile(2123, 5259, 0), new Tile(2123, 5258, 0), new Tile(2124, 5258, 0), new Tile(2125, 5258, 0), new Tile(2126, 5258, 0), new Tile(2126, 5259, 0), new Tile(2127, 5259, 0), new Tile(2128, 5259, 0), new Tile(2128, 5258, 0), new Tile(2128, 5257, 0), new Tile(2129, 5257, 0), new Tile(2129, 5256, 0), new Tile(2130, 5256, 0), new Tile(2131, 5256, 0), new Tile(2132, 5256, 0), new Tile(2133, 5256, 0), new Tile(2134, 5256, 0), new Tile(2134, 5255, 0), new Tile(2134, 5254, 0), new Tile(2133, 5254, 0), new Tile(2133, 5253, 0), new Tile(2132, 5253, 0), new Tile(2131, 5253, 0), new Tile(2131, 5252, 0), new Tile(2130, 5252, 0), new Tile(2130, 5251, 0), new Tile(2129, 5251, 0), new Tile(2129, 5250, 0), new Tile(2128, 5250, 0), new Tile(2127, 5250, 0), new Tile(2126, 5250, 0), new Tile(2125, 5250, 0), new Tile(2124, 5250, 0), new Tile(2123, 5250, 0), new Tile(2122, 5250, 0), new Tile(2121, 5250, 0), new Tile(2121, 5251, 0), new Tile(2120, 5251, 0), new Tile(2120, 5252, 0), new Tile(2119, 5252, 0), new Tile(2118, 5252, 0), new Tile(2118, 5253, 0), new Tile(2117, 5253, 0), new Tile(2116, 5253, 0), new Tile(2116, 5254, 0), new Tile(2116, 5255, 0), new Tile(2116, 5256, 0), new Tile(2116, 5257, 0), new Tile(2116, 5258, 0), new Tile(2117, 5258, 0) );
	Area strongholdFloor3SWdoors = new Area( new Tile(2132, 5259, 0), new Tile(2133, 5259, 0), new Tile(2133, 5258, 0), new Tile(2133, 5257, 0), new Tile(2132, 5257, 0), new Tile(2132, 5258, 0) );
	
	Filter<Npc> enemiesAttackingPlayerAreaFilter = new Filter<Npc>() {
		@Override
		public boolean accept(Npc npc) {
			if (npc.getAnimationId() == EnemyType.getCurrent().getDeathAnimationId())
				return false;
			
			Character interacting = npc.getInteracting();
			if (npc.getInteractingIndex() != -1) {
				System.out.println("inter with: [" + npc.getInteractingIndex() + "] " + interacting);
			}
			
			Player player = Players.getMyPlayer();
			if (player == null)
				return false;
			
			if (interacting != null && interacting.getObject() == player.getObject()) {
				System.out.println("enemy is already attacking my player");
				if (EnemyType.getCurrent().getArea().contains(npc.getTile()))
					return true;
			}
			
			return false;
		}
	};
	
	Filter<Npc> enemiesAreaFilter = new Filter<Npc>() {
		@Override
		public boolean accept(Npc npc) {
			if (npc.getAnimationId() == EnemyType.getCurrent().getDeathAnimationId())
				return false;
			
			Character interacting = npc.getInteracting();
			Player player = Players.getMyPlayer();
			if (player == null)
				return false;
			
			if (interacting != null && interacting.getObject() == player.getObject()) {
				System.out.println("enemy is already attacking my player");
				return true;
			}
			
			if (npc.getInteractingIndex() != -1)
				return false;
			
			if (EnemyType.getCurrent().getArea().contains(npc.getTile()))
				return true;
			
			return false;
		}
	};
	
	Condition npcIsDead = new Condition() {
		@Override
		public boolean evaluate() {
			if (curNpc != null) {
				//System.out.println(curNpc.getAnimationId());
				
				if (curNpc.getAnimationId() == EnemyType.getCurrent().getDeathAnimationId())
					return true;
				
				return false;
			} else
				return true;
		}
	};
	
	Condition interactingWithNpc = new Condition() {
		@Override
		public boolean evaluate() {
			if (curNpc != null) {
				Character interacting = curNpc.getInteracting();
				Player player = Players.getMyPlayer();
				if (player == null)
					return false;
				
				if (interacting != null && interacting.getObject() == player.getObject())
					return true;
			}
			
			return false;
		}
	};
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public Npc getNearestEnemy() {
		
		Timer timer = new Timer(2000);
		while (timer.isRunning()) {
			Npc npc = Npcs.getNearest(enemiesAttackingPlayerAreaFilter, EnemyType.getCurrent().getNpcIds());
			if (npc != null)
				return npc;
			
			npc = Npcs.getNearest(enemiesAreaFilter, EnemyType.getCurrent().getNpcIds());
			if (npc != null)
				return npc;
			
			Time.sleep(50);
		}
		
		return null;
	}
	
	public boolean waitUntilStartFighting(boolean breakOnNpcDead, boolean doAntiban) {
		if (curNpc == null)
			return false;
		
		double dist = Calculations.distanceTo(curNpc.getData().getCenterLocation().getTile(false));
		int maxPeriod;
		
		if (dist >= 5)
			maxPeriod = 5000;
		else if (dist > 2.5)
			maxPeriod = 4000;
		else if (dist > 1.5)
			maxPeriod = 3000;
		else
			maxPeriod = 2500;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		// TODO: do not allow interaction with other player (follow)
		return player.waitForInteracting(maxPeriod, doAntiban, breakOnNpcDead ? npcIsDead : null);
	}
	
	public boolean waitUntilStopFighting(boolean breakOnNpcDead, boolean doAntiban) {
		if (curNpc == null)
			return false;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		// TODO: do not allow interaction with other player (follow)
		boolean playerInteracting = player.waitForIdleInteracting(60000, doAntiban, breakOnNpcDead ? npcIsDead : null);
		boolean npcInteracting = curNpc.waitForStopInteracting(player, 60000, doAntiban, breakOnNpcDead ? npcIsDead : null);
		
		return playerInteracting && npcInteracting;
	}
	
	public boolean fight() {
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		Tab.closeAll(Tab.BACKPACK);
		
		Npc npc = getNearestEnemy();
		if (npc == null) {
			Player player = Players.getMyPlayer();
			if (player == null)
				return false;
			
			if (!EnemyType.getCurrent().getArea().contains(player.getTile())) {
				if (!EnemyType.getCurrent().getArea().loopWalkTo(10000))
					return false;
			}
			
			npc = getNearestEnemy();
			if (npc == null)
				return false;
		}
		
		curNpc = npc;
		
		//Paint.updatePaintObject(npc);
		
		if (Calculations.distanceTo(npc) >= 15) {
			if (!npc.loopWalkTo(EnemyType.getCurrent().getArea(), 10000, npcIsDead))
				return false;
			Time.sleepQuick();
		}
		
		if (Camera.getPitch() < 50) {
			Camera.setPitch(Random.nextInt(50, 74));
			Time.sleepVeryQuick();
		}
		
		if (Calculations.distanceTo(npc) < 15 && npc.loopInteract(npcIsDead, "Attack"))
			return waitUntilStartFighting(true, true);
		
		return false;
	}
	
	public boolean bank() {
		
		if (Bank.get.open()) {
			Time.sleepQuick();
			
			int[] equipmentIds = AttackStyle.getCurrent().getEquipmentIds();
			
			if (Bank.get.depositAllInvItems(equipmentIds)) {
				Time.sleepQuick();
				
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					Bank.get.withdrawAll(995, false); // coins
					Time.sleepQuick();
				}
				
				if (equipmentIds != null && AttackStyle.getCurrent().equals(AttackStyle.MAGIC)) {
					for (int equipmentId : equipmentIds) {
						if (!ItemTable.INVENTORY.containsAny(equipmentId) && !ItemTable.EQUIPMENT.containsAny(equipmentId)) {
							if (!ItemTable.BANK.containsAny(equipmentId)) {
								System.out.println("Missing equipment!");
								if (equipmentId == staffOfAirId) {
									System.out.println("Missing staff, switching to Melee to get a staff...");
									tempOverrideParams = true;
									EnemyType.setCurrent(EnemyType.TROLL_SHAMAN);
									CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
									AttackStyle.setCurrent(Random.nextBoolean() ? AttackStyle.MELEE : AttackStyle.RANGED);
									equipmentIds = AttackStyle.getCurrent().getEquipmentIds();
								}
							}
						}
					}
				}
				
				if (equipmentIds != null && AttackStyle.getCurrent().equals(AttackStyle.MELEE)) {
					for (int equipmentId : equipmentIds) {
						if (!ItemTable.INVENTORY.containsAny(equipmentId) && !ItemTable.EQUIPMENT.containsAny(equipmentId)) {
							if (!ItemTable.BANK.containsAny(equipmentId)) {
								System.out.println("Missing equipment!");
								if (equipmentId == bronzeSwordId) {
									System.out.println("Missing bronze sword, getting one from store...");
									buyGnomeEquipment = true;
								}
							}
						}
					}
				}
				
				if (equipmentIds != null && AttackStyle.getCurrent().equals(AttackStyle.RANGED)) {
					for (int equipmentId : equipmentIds) {
						if (!ItemTable.INVENTORY.containsAny(equipmentId) && !ItemTable.EQUIPMENT.containsAny(equipmentId)) {
							if (!ItemTable.BANK.containsAny(equipmentId)) {
								System.out.println("Missing equipment!");
								if (equipmentId == chargebowId) {
									System.out.println("Missing chargebow, getting one from store...");
									buyGnomeEquipment = true;
								}
							}
						}
					}
				}
				
				if (equipmentIds != null) {
					for (int equipmentId : equipmentIds) {
						if (!ItemTable.INVENTORY.containsAny(equipmentId) && !ItemTable.EQUIPMENT.containsAny(equipmentId)) {
							if (ItemTable.BANK.containsAny(equipmentId)) {
								if (!Bank.get.withdraw(equipmentId, 1, false)) {
									Time.sleepQuick();
									return false;
								}
								Time.sleepQuick();
							}
						}
					}
				}
				
				Bank.get.close();
				
				return true;
			}
		}
		
		return false;
	}
	
	public Spell getBestSpell() {
		int magicLevel = Skill.MAGIC.getCurrentLevel();
		
		if (magicLevel >= Spell.AIR_BLAST.getLevelReq())
			return Spell.AIR_BLAST;
		else if (magicLevel >= Spell.AIR_BOLT.getLevelReq())
			return Spell.AIR_BOLT;
		else if (magicLevel >= Spell.AIR_STRIKE.getLevelReq())
			return Spell.AIR_STRIKE;
		
		return null;
	}
	
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		failsafeWalkedToMine = new Failsafe("Walked to Mine", 900000); // 15 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 900000); // 15 mins.
		failsafeMinedRock = new Failsafe("Mine", 900000); // 15 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 900000); // 15 mins.
		failsafes.add(failsafeWalkedToMine);
		failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		//params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		//breakHandlerParam = new BreakHandlerParam(failsafes);
		//params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		boolean success = false;
		if (AccountInfo.cur != null) {
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Map<String, String> params = row.getScriptParams_toMap();
				String enemyType = params.get("EnemyType");
				if (enemyType != null) {
					success = true;
					
					enemyType = enemyType.toLowerCase();
					enemyType = enemyType.replace("_", " ");
					if (enemyType.contains("al kharid warrior"))
						EnemyType.setCurrent(EnemyType.AL_KHARID_WARRIOR);
					else if (enemyType.contains("giant spider"))
						EnemyType.setCurrent(EnemyType.GIANT_SPIDER);
					else if (enemyType.contains("pirate"))
						EnemyType.setCurrent(EnemyType.PIRATE);
					else if (enemyType.contains("troll brute"))
						EnemyType.setCurrent(EnemyType.TROLL_BRUTE);
					else if (enemyType.contains("troll chucker"))
						EnemyType.setCurrent(EnemyType.TROLL_CHUCKER);
					else if (enemyType.contains("troll shaman"))
						EnemyType.setCurrent(EnemyType.TROLL_SHAMAN);
					else if (enemyType.contains("auto"))
						autoMode = true;
					else
						success = false;
				}
				
				if (success) {
					String combatLocation = params.get("CombatLocation");
					if (combatLocation != null) {
						success = true;
						
						combatLocation = combatLocation.toLowerCase();
						combatLocation = combatLocation.replace("_", " ");
						if (combatLocation.contains("al kharid palace"))
							CombatLocation.setCurrent(CombatLocation.AL_KHARID_PALACE);
						else if (combatLocation.contains("asgarnian ice dungeon"))
							CombatLocation.setCurrent(CombatLocation.ASGARNIAN_ICE_DUNGEON);
						else if (combatLocation.contains("burthorpe trolls"))
							CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
						else if (combatLocation.contains("stronghold security floor1"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR1);
						else if (combatLocation.contains("stronghold security floor2"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR2);
						else if (combatLocation.contains("stronghold security floor3"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR3);
						else if (combatLocation.contains("stronghold security floor4"))
							CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR4);
						else if (combatLocation.contains("auto"))
							autoMode = true;
						else
							success = false;
						
						if (success) {
							String attackStyle = params.get("AttackStyle");
							if (attackStyle != null) {
								success = true;
								
								attackStyle = attackStyle.toLowerCase();
								if (attackStyle.contains("magic"))
									AttackStyle.setCurrent(AttackStyle.MAGIC);
								else if (attackStyle.contains("melee"))
									AttackStyle.setCurrent(AttackStyle.MELEE);
								else if (attackStyle.contains("ranged"))
									AttackStyle.setCurrent(AttackStyle.RANGED);
								else
									success = false;
							}
						}
					}
				}
			}
		}
		
		if (!success) {
			System.out.println("Combat: Error loading params! Loaded default data params.");
			EnemyType.setCurrent(EnemyType.TROLL_BRUTE);
			CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
			AttackStyle.setCurrent(AttackStyle.MELEE);
		} else {
			System.out.println("Combat: Successfully loaded data params: ");
		}
		
		System.out.println("\t-AttackStyle: " + AttackStyle.getCurrent().name());
		if (!autoMode) {
			System.out.println("\t-EnemyType: " + EnemyType.getCurrent().name());
			System.out.println("\t-CombatLocation: " + CombatLocation.getCurrent().name());
		} else
			System.out.println("\t-Auto mode enabled");
	}
	
	@Override
	public String getInfo() {
		return "";
	}
	
	@Override
	public boolean onStart() {
		
		loadDataParams();
		
		tempOverrideParams = false;
		
		//Paint.tiles = CombatLocation.getCurrent().getArea().getBoundingTiles();
		//Paint.tiles = EnemyType.getCurrent().getArea().getBoundingTiles();
		
		Antiban.resetTimer();
		
		buyGnomeEquipment = false;
		
		return true;
	}
	
	Npc curNpc = null;
	
	
	/*
	 * Script Notes
	 * 
	 */
	
	@Override
	public void run() {
		
		Antiban.randomMouseSpeed();
		
		if (!Client.loopLogin())
			return;
		
		if (autoMode && !tempOverrideParams) {
			if (!autoModeTimer.isRunning() || EnemyType.getCurrent() == null || CombatLocation.getCurrent() == null) {
				switch (AttackStyle.getCurrent()) {
				case MAGIC:
					EnemyType.setCurrent(EnemyType.TROLL_BRUTE);
					CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
					break;
				case MELEE:
					//if (Skill.ATTACK.getRealLevel() < 41) {
						EnemyType.setCurrent(EnemyType.TROLL_CHUCKER);
						CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
					// TODO: check if have weapon or enough money for weapon
					/*} else if (Skill.ATTACK.getRealLevel() < 70) {
						EnemyType.setCurrent(EnemyType.AL_KHARID_WARRIOR);
						CombatLocation.setCurrent(CombatLocation.AL_KHARID_PALACE);
					} else if (Skill.ATTACK.getRealLevel() <= 99) {
						EnemyType.setCurrent(EnemyType.GIANT_SPIDER);
						CombatLocation.setCurrent(CombatLocation.STRONGHOLD_SECURITY_FLOOR3);
					}*/
					break;
				case RANGED:
					EnemyType.setCurrent(EnemyType.TROLL_SHAMAN);
					CombatLocation.setCurrent(CombatLocation.BURTHOPE_TROLLS);
					break;
				default:
					break;
				}
				
				origEnemyType = EnemyType.getCurrent();
				origCombatLocation = CombatLocation.getCurrent();
				origAttackStyle = AttackStyle.getCurrent();
				
				Paint.tiles = EnemyType.getCurrent().getArea().getBoundingTiles();
				
				autoModeTimer.reset();
			}
		}
		
		if (!Bank.get.close(true))
			return;
		
		if (!buyGnomeEquipment && ShopWidget.get.isOpen()) {
			if (!ShopWidget.get.close(true))
				return;
		}
		
		if (curNpc != null) {
			waitUntilStopFighting(true, true);
			curNpc = null;
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!CombatMode.REVOLUTION.activate(true))
			return;
		if (!ActionBar.setup(AttackStyle.getCurrent().getActionBarIndex(), AttackStyle.getCurrent().getPreferredAbilities()))
			return;
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}
		
		if (!ActionBar.setActionBar(AttackStyle.getCurrent().getActionBarIndex()))
			return;
		
		if (!Settings_Gameplay_LootSettingsSubTabWidget.get.disableLootInventory(true))
			return;
		if (MainWidget.get.isSubInterfaceOpen()) {
			if (!(Ribbon.SubInterface.GRAND_EXCHANGE.isOpen() && exchange)) {
				if (!MainWidget.get.closeSubInterface(true))
					return;
			}
		}
		
		if (!Lodestone.AL_KHARID.isActivated()
				|| !Lodestone.EDGEVILLE.isActivated()
				|| !Lodestone.PORT_SARIM.isActivated()) {
			
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			if (!Lodestone.AL_KHARID.isActivated()) {
				Lodestone.AL_KHARID.activate();
				Time.sleepMedium();
				return;
			}
			
			if (!Lodestone.EDGEVILLE.isActivated()) {
				Lodestone.EDGEVILLE.activate();
				Time.sleepMedium();
				return;
			}
			
			if (!Lodestone.PORT_SARIM.isActivated()) {
				Lodestone.PORT_SARIM.activate();
				Time.sleepMedium();
				return;
			}
		}
		
		
		if (tempOverrideParams) {
			if (!origAttackStyle.equals(AttackStyle.getCurrent())) {
				if (origAttackStyle.equals(AttackStyle.MAGIC)) {
					if (ItemTable.INVENTORY.containsAny(staffOfAirId) || ItemTable.EQUIPMENT.containsAny(staffOfAirId)) {
						System.out.println("We have staff, switching back to magic style...");
						tempOverrideParams = false;
						AttackStyle.setCurrent(origAttackStyle);
						EnemyType.setCurrent(origEnemyType);
						CombatLocation.setCurrent(origCombatLocation);
					}
				}
			}
		}
		
		int[] equipmentIds = AttackStyle.getCurrent().getEquipmentIds();
		if (equipmentIds != null) {
			for (int equipmentId : equipmentIds) {
				if (!ItemTable.EQUIPMENT.containsAny(equipmentId)) {
					if (ItemTable.INVENTORY.containsAny(equipmentId)) {
						if (!Ribbon.Tab.BACKPACK.open(true))
							return;
						
						Time.sleepQuick();
						
						Component item = InventoryWidget.get.getItem(equipmentId);
						if (item == null)
							return;
						
						if (!item.interact("Wield")) {
							Time.sleepQuick();
							return;
						}
						
						Time.sleepQuick();
					}
				}
			}
		}
		
		if (EquipmentSlot.MAINHAND.getItemId() != -1
				&& MainWidget.get.isWeaponSheathed()) {
			if (!MainWidget.get.unsheatheWeapon(true))
				return;
			Time.sleepQuick();
		}
		
		if (!ActionBarWidget.isAutoRetaliateEnabled()) {
			if (!ActionBarWidget.get.setAutoRetaliate(true, true))
				return;
			Time.sleepQuick();
		}
		
		if (AttackStyle.getCurrent().equals(AttackStyle.MAGIC)) {
			if (EquipmentSlot.MAINHAND.getItemId() == staffOfAirId) {
				Spell bestSpell = getBestSpell();
				if (bestSpell != null) {
					if (!getBestSpell().autocast(true))
						return;
				} else
					return;
			}
		}
		
		Action action = Action.getNextAction();
		System.out.println(action);
		
		switch (action) {
		case FIGHT:	
			// pickup
			GroundItem[] gis = GroundItems.getAllOnScreen();
			if (gis != null) {
				for (GroundItem gi : gis) {
					int itemId = gi.getId();
					
					if (/*itemId == 526 ||*/ itemId == 12158 || itemId == staffOfAirId) { // bone, gold charm, staff of air
						String option = null;
						
						if (itemId == 526)
							option = "Bones";
						else if (itemId == 12158)
							option = "Gold charm";
						else if (itemId == staffOfAirId)
							option = staffOfAirName;
						
						if (gi.loopInteract(null, "Take", option, false)) {
							Time.sleepQuick();
							
							//if (Calculations.distanceTo(gi.getTile()) >= 2) {
								Timer timerStartMoving = new Timer(3000);
								while (timerStartMoving.isRunning()) {
									Player player = Players.getMyPlayer();
									if (player == null)
										break;
									
									if (player.isMoving())
										break;
									
									Time.sleep(50);
								}
								
								Timer timerStopMoving = new Timer(10000);
								while (timerStopMoving.isRunning()) {
									Player player = Players.getMyPlayer();
									if (player == null)
										break;
									
									if (!player.isMoving())
										break;
									
									Time.sleep(50);
								}
							//}
						} else
							Time.sleepQuick();
					}
				}
			}
			
			// bury bones
			Component.interactionAntiban = false; // TODO: orig set back
			if (ItemTable.INVENTORY.containsAny(526)) { // bones
				if (Ribbon.Tab.BACKPACK.open(true)) {
					Component[] items = InventoryWidget.get.getItems();
					for (Component item : items) {
						if (item.getComponentId() == 526) { // bones
							item.interact("Bury");
							Time.sleepQuick();
						}
					}
				}
			}
			Component.interactionAntiban = true; // TODO: orig set back
			
			if (!fight())
				curNpc = null;
			else
				failsafeMinedRock.reset();
			
			break;
			
		case BANK:
			curNpc = null;
			
			if (buyGnomeEquipment) {
				if (Shop.BURTHORPE_GNOME_SHOP.open(true)) {
					Time.sleepQuick();
					
					if (ShopWidget.get.isOpen()) {
						if (AttackStyle.getCurrent().equals(AttackStyle.MELEE)) {
							if (ShopWidget.get.buy1(bronzeSwordId)) {
								ShopWidget.get.close(true);
							}
							Time.sleepMedium();
						} else if (AttackStyle.getCurrent().equals(AttackStyle.RANGED)) {
							if (ShopWidget.get.buy1(chargebowId)) {
								ShopWidget.get.close(true);
							}
							Time.sleepMedium();
						}
					}
				}
				
				if (AttackStyle.getCurrent().equals(AttackStyle.MELEE)) {
					if (ItemTable.INVENTORY.containsAny(bronzeSwordId)
							|| ItemTable.EQUIPMENT.containsAny(bronzeSwordId))
						buyGnomeEquipment = false;
				} else if (AttackStyle.getCurrent().equals(AttackStyle.RANGED)) {
					if (ItemTable.INVENTORY.containsAny(chargebowId)
							|| ItemTable.EQUIPMENT.containsAny(chargebowId))
						buyGnomeEquipment = false;
				}
				
				break;
			}
			
			Interactable bank = Bank.get.getNearest();
			if (bank != null && Calculations.distanceTo(bank) <= 25) {
				if (AttackStyle.getCurrent().isEquipmentSetup() && ItemTable.INVENTORY.countAllItems(false) <= 0) { // heal likely
					if (Calculations.distanceTo(bank) > 1) {
						if (bank.loopWalkTo(10000, null)) {
							Time.sleepMedium();
							
							if (Calculations.distanceTo(bank) > 1) {
								bank.getTile().randomize(1, 1).loopInteract(null, "Walk here");
								Time.sleepMedium();
							}
						}
					}
				} else if (bank()) {
					failsafeDropOrBank.reset();
					failsafeWalkedToBank.reset();
				}
			} else { // walk
				if (Random.nextInt(0, 100) <= 75) {
					if (Random.nextBoolean())
						Time.sleepMedium();
					else
						Time.sleepQuick();
				}
				
				if (CombatLocation.getCurrent().walkToBank(null/*handledBreak*/, true))
					failsafeWalkedToBank.reset();
				
				/*TilePath path = MineLocation.getCurrent().getRandomPathToBank();
				if (path.loopWalk(90000, handledBreak, true)) // 90s max
					failsafeWalkedToBank.reset();*/
			}
			
			break;
			
		case WALK:
			Player player = Players.getMyPlayer();
			if (player == null)
				break;
			
			//if (CombatLocation.getCurrent().getArea().contains(player.getTile())
			//		|| CombatLocation.getCurrent().getArea().distanceToNearest() < 25) {
				
				curNpc = null;
				
				if (Random.nextInt(0, 100) <= 75) {
					if (Random.nextBoolean())
						Time.sleepMedium();
					else
						Time.sleepQuick();
				}
				
				
				if (CombatLocation.getCurrent().walkToArea(null/*handledBreak*/, true)) {
					if (EnemyType.getCurrent().equals(EnemyType.GIANT_SPIDER)) {
						if (strongholdFloor3SWladder.contains(player.getTile())) {
							GroundObject door = GroundObjects.getNearest(Type.Boundary, new Filter<GroundObject>() {
								@Override
								public boolean accept(GroundObject go) {
									Tile tile = go.getTile();
									
									if ((tile.getX() == 2132 && tile.getY() == 5257)
											|| (tile.getX() == 2133 && tile.getY() == 5257))
										return true;
									
									return false;
								}
							}, 16089, 16090); // Oozing barrier
							
							if (door != null && door.loopInteract(/*breakCondition*/null, "Open")) {
								Time.sleepQuick();
								
								Timer timer = new Timer(5000);
								while (timer.isRunning()) {
									if (strongholdFloor3SWdoors.contains(player.getTile())) {
										Time.sleepQuick();
										break;
									}
									
									Time.sleep(50);
								}
							}
						}
						
						if (strongholdFloor3SWdoors.contains(player.getTile())) {
							GroundObject door = GroundObjects.getNearest(Type.Boundary, new Filter<GroundObject>() {
								@Override
								public boolean accept(GroundObject go) {
									Tile tile = go.getTile();
									
									if ((tile.getX() == 2132 && tile.getY() == 5260)
											|| (tile.getX() == 2133 && tile.getY() == 5260))
										return true;
									
									return false;
								}
							}, 16089, 16090); // Oozing barrier
							
							if (door != null && door.loopInteract(/*breakCondition*/null, "Open")) {
								Time.sleepQuick();
								
								Timer timer = new Timer(5000);
								while (timer.isRunning()) {
									if (EnemyType.GIANT_SPIDER.getArea().contains(player.getTile())) {
										Time.sleepQuick();
										break;
									}
									
									Time.sleep(50);
								}
							}
						}
					} else if (!EnemyType.getCurrent().getArea().contains(player.getTile())) {
						if (EnemyType.getCurrent().getArea().loopWalkTo(30000)) {
							
						}
					}
					
					failsafeWalkedToMine.reset();
				}
				
				/*TilePath path = EnemyType.getCurrent().getRandomPathTo();
				if (path.loopWalk(90000, handledBreak, true)) // 90s max
					failsafeWalkedToMine.reset();*/
			//}
			
			break;
		}
	}
	
	@Override
	public boolean onStop() {
		return true;
	}
}
