package org.rs3.scripts.tanning;

import org.rs3.scripts.combat.CombatLocation;

public enum HideType {

	COWHIDE("Cowhide", 1739, "Leather", 1741), 
	GREEN_DRAGONHIDE("Green dragonhide", 1753, "Green dragon leather",  1745), 
	BLUE_DRAGONHIDE("Blue dragonhide", 1751, "Blue dragon leather",  2505), 
	RED_DRAGONHIDE("Red dragonhide", 1749, "Red dragon leather",  2507), 
	BLACK_DRAGONHIDE("Black dragonhide", 1747, "Black dragon leather",  2509), 
	;
	
	private String hideName;
	private int hideId;
	private String leatherName;
	private int leatherId;
	
	private static HideType cur;
	
	public static HideType getCurrent() {
		return cur;
	}
	
	public static void setCurrent(HideType hideType) {
		cur = hideType;
	}
	
	private HideType(String hideName, int hideId, String leatherName, int leatherId) {
		this.hideName = hideName;
		this.hideId = hideId;
		this.leatherName = leatherName;
		this.leatherId = leatherId;
	}
	
	public String getHideName() {
		return hideName;
	}
	
	public int getHideId() {
		return hideId;
	}
	
	public String getLeatherName() {
		return leatherName;
	}
	
	public int getLeatherId() {
		return leatherId;
	}
}
