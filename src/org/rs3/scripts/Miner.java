package org.rs3.scripts;

import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.rs3.api.ActionBar;
import org.rs3.api.ActionSlot;
import org.rs3.api.Calculations;
import org.rs3.api.Camera;
import org.rs3.api.Constants;
import org.rs3.api.GESlot;
import org.rs3.api.GrandExchange;
import org.rs3.api.GroundObjects;
import org.rs3.api.ItemTable;
import org.rs3.api.Lodestone;
import org.rs3.api.Players;
import org.rs3.api.Skill;
import org.rs3.api.Toolbelt;
import org.rs3.api.Walking;
import org.rs3.api.GESlot.GEOfferStatus;
import org.rs3.api.antiban.Antiban;
import org.rs3.api.exchange.ExchangeItem;
import org.rs3.api.exchange.ExchangeLoop;
import org.rs3.api.input.Keyboard;
import org.rs3.api.interfaces.widgets.Bank;
import org.rs3.api.interfaces.widgets.ChatBox;
import org.rs3.api.interfaces.widgets.DepositBox;
import org.rs3.api.interfaces.widgets.GrandExchangeWidget;
import org.rs3.api.interfaces.widgets.InventoryWidget;
import org.rs3.api.interfaces.widgets.MakeItemsWidget;
import org.rs3.api.interfaces.widgets.ProcessingItemsWidget;
import org.rs3.api.interfaces.widgets.Ribbon;
import org.rs3.api.interfaces.widgets.Ribbon.Tab;
import org.rs3.api.objects.GroundObject;
import org.rs3.api.objects.Tile;
import org.rs3.api.objects.TilePath;
import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.AnimatedAnimableObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.Component;
import org.rs3.api.wrappers.Interactable;
import org.rs3.api.wrappers.Location;
import org.rs3.api.wrappers.Model;
import org.rs3.api.wrappers.Npc;
import org.rs3.api.wrappers.Player;
import org.rs3.api.wrappers.RSAnimable;
import org.rs3.callbacks.ItemTableCallback;
import org.rs3.callbacks.ItemTableListener;
import org.rs3.database.AccountInfo;
import org.rs3.database.AccountRow;
import org.rs3.database.GrandExchangeRow;
import org.rs3.debug.Paint;
import org.rs3.scripts.WineCollector.SubAction;
import org.rs3.scripts.mining.MineLocation;
import org.rs3.scripts.mining.OreType;
import org.rs3.scripts.parameters.BreakHandlerParam;
import org.rs3.scripts.parameters.CloseUselessInterfacesParam;
import org.rs3.scripts.parameters.DeathHandlerParam;
import org.rs3.scripts.parameters.DefaultParam;
import org.rs3.scripts.parameters.FailsafesParam;
import org.rs3.scripts.parameters.LoginParam;
import org.rs3.scripts.parameters.RandomReseedParam;
import org.rs3.scripts.parameters.RunTimeParam;
import org.rs3.scripts.parameters.SaveScreenshotsParam;
import org.rs3.scripts.parameters.ScriptParameter;
import org.rs3.scripts.tanning.HideType;
import org.rs3.util.Condition;
import org.rs3.util.Filter;
import org.rs3.util.Random;
import org.rs3.util.Time;
import org.rs3.util.Timer;

public class Miner extends Script {
	
	private static final int runePickId = 1275;
	public static final int softClayId = 1761;
	private static final String runePickName = "Rune pickaxe";
	public static final String softClayName = "Soft clay";
	
	
	private static final Tile[] rimmingtonWellTiles = new Tile[] { new Tile(2956, 3206, 0), new Tile(2957, 3206, 0), new Tile(2957, 3205, 0), new Tile(2956, 3205, 0) };
	
	// AreaPath - rimmington
	//static final Tile[] miningToBankStartTiles = new Tile[] { new Tile(2968, 3238, 0), new Tile(2968, 3239, 0), new Tile(2968, 3240, 0), new Tile(2969, 3240, 0), new Tile(2969, 3241, 0), new Tile(2970, 3241, 0), new Tile(2970, 3242, 0), new Tile(2970, 3243, 0), new Tile(2970, 3244, 0), new Tile(2970, 3245, 0), new Tile(2970, 3246, 0), new Tile(2970, 3247, 0), new Tile(2970, 3248, 0), new Tile(2969, 3248, 0), new Tile(2968, 3248, 0), new Tile(2968, 3249, 0), new Tile(2968, 3250, 0), new Tile(2968, 3251, 0), new Tile(2969, 3251, 0), new Tile(2969, 3252, 0), new Tile(2969, 3253, 0), new Tile(2969, 3254, 0), new Tile(2969, 3255, 0), new Tile(2969, 3256, 0) };
	//static final Tile[] miningToBankEndTiles = new Tile[] { new Tile(3043, 3258, 0), new Tile(3043, 3257, 0), new Tile(3043, 3256, 0), new Tile(3043, 3255, 0), new Tile(3043, 3254, 0), new Tile(3044, 3254, 0), new Tile(3044, 3253, 0), new Tile(3044, 3252, 0), new Tile(3044, 3251, 0), new Tile(3043, 3251, 0), new Tile(3043, 3250, 0), new Tile(3043, 3249, 0), new Tile(3044, 3249, 0), new Tile(3044, 3248, 0), new Tile(3044, 3247, 0), new Tile(3044, 3246, 0), new Tile(3044, 3245, 0), new Tile(3043, 3245, 0), new Tile(3043, 3244, 0), new Tile(3043, 3243, 0), new Tile(3043, 3242, 0), new Tile(3043, 3241, 0), new Tile(3043, 3240, 0), new Tile(3043, 3239, 0), new Tile(3043, 3238, 0), new Tile(3043, 3237, 0), new Tile(3044, 3237, 0), new Tile(3045, 3237, 0), new Tile(3046, 3237, 0), new Tile(3047, 3237, 0), new Tile(3048, 3237, 0), new Tile(3049, 3237, 0), new Tile(3049, 3236, 0), new Tile(3049, 3235, 0), new Tile(3049, 3234, 0) };
	//static final Area miningAreaToBank = new Area( new Tile(2974, 3238, 0), new Tile(2973, 3238, 0), new Tile(2972, 3238, 0), new Tile(2971, 3238, 0), new Tile(2970, 3238, 0), new Tile(2969, 3238, 0), new Tile(2968, 3238, 0), new Tile(2968, 3239, 0), new Tile(2968, 3240, 0), new Tile(2969, 3240, 0), new Tile(2969, 3241, 0), new Tile(2970, 3241, 0), new Tile(2970, 3242, 0), new Tile(2970, 3243, 0), new Tile(2970, 3244, 0), new Tile(2970, 3245, 0), new Tile(2970, 3246, 0), new Tile(2970, 3247, 0), new Tile(2970, 3248, 0), new Tile(2969, 3248, 0), new Tile(2968, 3248, 0), new Tile(2968, 3249, 0), new Tile(2968, 3250, 0), new Tile(2968, 3251, 0), new Tile(2969, 3251, 0), new Tile(2969, 3252, 0), new Tile(2969, 3253, 0), new Tile(2969, 3254, 0), new Tile(2969, 3255, 0), new Tile(2969, 3256, 0), new Tile(2970, 3256, 0), new Tile(2971, 3256, 0), new Tile(2971, 3257, 0), new Tile(2972, 3257, 0), new Tile(2973, 3257, 0), new Tile(2974, 3257, 0), new Tile(2974, 3258, 0), new Tile(2975, 3258, 0), new Tile(2975, 3259, 0), new Tile(2976, 3259, 0), new Tile(2977, 3259, 0), new Tile(2978, 3259, 0), new Tile(2979, 3259, 0), new Tile(2980, 3259, 0), new Tile(2981, 3259, 0), new Tile(2982, 3259, 0), new Tile(2983, 3259, 0), new Tile(2983, 3258, 0), new Tile(2984, 3258, 0), new Tile(2984, 3257, 0), new Tile(2985, 3257, 0), new Tile(2986, 3257, 0), new Tile(2987, 3257, 0), new Tile(2988, 3257, 0), new Tile(2989, 3257, 0), new Tile(2990, 3257, 0), new Tile(2991, 3257, 0), new Tile(2992, 3257, 0), new Tile(2993, 3257, 0), new Tile(2994, 3257, 0), new Tile(2994, 3256, 0), new Tile(2995, 3256, 0), new Tile(2996, 3256, 0), new Tile(2997, 3256, 0), new Tile(2998, 3256, 0), new Tile(2999, 3256, 0), new Tile(3000, 3256, 0), new Tile(3001, 3256, 0), new Tile(3002, 3256, 0), new Tile(3003, 3256, 0), new Tile(3004, 3256, 0), new Tile(3005, 3256, 0), new Tile(3006, 3256, 0), new Tile(3007, 3256, 0), new Tile(3008, 3256, 0), new Tile(3009, 3256, 0), new Tile(3010, 3256, 0), new Tile(3010, 3255, 0), new Tile(3011, 3255, 0), new Tile(3012, 3255, 0), new Tile(3013, 3255, 0), new Tile(3014, 3255, 0), new Tile(3015, 3255, 0), new Tile(3016, 3255, 0), new Tile(3017, 3255, 0), new Tile(3017, 3256, 0), new Tile(3017, 3257, 0), new Tile(3017, 3258, 0), new Tile(3018, 3258, 0), new Tile(3019, 3258, 0), new Tile(3020, 3258, 0), new Tile(3021, 3258, 0), new Tile(3022, 3258, 0), new Tile(3023, 3258, 0), new Tile(3024, 3258, 0), new Tile(3025, 3258, 0), new Tile(3026, 3258, 0), new Tile(3027, 3258, 0), new Tile(3028, 3258, 0), new Tile(3029, 3258, 0), new Tile(3030, 3258, 0), new Tile(3031, 3258, 0), new Tile(3031, 3259, 0), new Tile(3032, 3259, 0), new Tile(3033, 3259, 0), new Tile(3034, 3259, 0), new Tile(3035, 3259, 0), new Tile(3036, 3259, 0), new Tile(3037, 3259, 0), new Tile(3038, 3259, 0), new Tile(3038, 3258, 0), new Tile(3039, 3258, 0), new Tile(3040, 3258, 0), new Tile(3041, 3258, 0), new Tile(3042, 3258, 0), new Tile(3043, 3258, 0), new Tile(3043, 3257, 0), new Tile(3043, 3256, 0), new Tile(3043, 3255, 0), new Tile(3043, 3254, 0), new Tile(3044, 3254, 0), new Tile(3044, 3253, 0), new Tile(3044, 3252, 0), new Tile(3044, 3251, 0), new Tile(3043, 3251, 0), new Tile(3043, 3250, 0), new Tile(3043, 3249, 0), new Tile(3044, 3249, 0), new Tile(3044, 3248, 0), new Tile(3044, 3247, 0), new Tile(3044, 3246, 0), new Tile(3044, 3245, 0), new Tile(3043, 3245, 0), new Tile(3043, 3244, 0), new Tile(3043, 3243, 0), new Tile(3043, 3242, 0), new Tile(3043, 3241, 0), new Tile(3043, 3240, 0), new Tile(3043, 3239, 0), new Tile(3043, 3238, 0), new Tile(3043, 3237, 0), new Tile(3044, 3237, 0), new Tile(3045, 3237, 0), new Tile(3046, 3237, 0), new Tile(3047, 3237, 0), new Tile(3048, 3237, 0), new Tile(3049, 3237, 0), new Tile(3049, 3236, 0), new Tile(3049, 3235, 0), new Tile(3049, 3234, 0), new Tile(3048, 3234, 0), new Tile(3047, 3234, 0), new Tile(3046, 3234, 0), new Tile(3045, 3234, 0), new Tile(3044, 3234, 0), new Tile(3043, 3234, 0), new Tile(3042, 3234, 0), new Tile(3041, 3234, 0), new Tile(3040, 3234, 0), new Tile(3039, 3234, 0), new Tile(3038, 3234, 0), new Tile(3037, 3234, 0), new Tile(3036, 3234, 0), new Tile(3035, 3234, 0), new Tile(3034, 3234, 0), new Tile(3033, 3234, 0), new Tile(3032, 3234, 0), new Tile(3031, 3234, 0), new Tile(3030, 3234, 0), new Tile(3029, 3234, 0), new Tile(3029, 3233, 0), new Tile(3028, 3233, 0), new Tile(3027, 3233, 0), new Tile(3026, 3233, 0), new Tile(3026, 3234, 0), new Tile(3026, 3235, 0), new Tile(3026, 3236, 0), new Tile(3026, 3237, 0), new Tile(3026, 3238, 0), new Tile(3026, 3239, 0), new Tile(3026, 3240, 0), new Tile(3025, 3240, 0), new Tile(3024, 3240, 0), new Tile(3023, 3240, 0), new Tile(3022, 3240, 0), new Tile(3021, 3240, 0), new Tile(3020, 3240, 0), new Tile(3020, 3241, 0), new Tile(3019, 3241, 0), new Tile(3018, 3241, 0), new Tile(3017, 3241, 0), new Tile(3016, 3241, 0), new Tile(3015, 3241, 0), new Tile(3014, 3241, 0), new Tile(3014, 3240, 0), new Tile(3014, 3239, 0), new Tile(3013, 3239, 0), new Tile(3012, 3239, 0), new Tile(3011, 3239, 0), new Tile(3010, 3239, 0), new Tile(3009, 3239, 0), new Tile(3009, 3238, 0), new Tile(3008, 3238, 0), new Tile(3007, 3238, 0), new Tile(3007, 3239, 0), new Tile(3006, 3239, 0), new Tile(3006, 3240, 0), new Tile(3005, 3240, 0), new Tile(3005, 3241, 0), new Tile(3004, 3241, 0), new Tile(3004, 3242, 0), new Tile(3003, 3242, 0), new Tile(3002, 3242, 0), new Tile(3002, 3243, 0), new Tile(3001, 3243, 0), new Tile(3001, 3244, 0), new Tile(3001, 3245, 0), new Tile(3000, 3245, 0), new Tile(2999, 3245, 0), new Tile(2998, 3245, 0), new Tile(2997, 3245, 0), new Tile(2996, 3245, 0), new Tile(2995, 3245, 0), new Tile(2994, 3245, 0), new Tile(2994, 3246, 0), new Tile(2993, 3246, 0), new Tile(2992, 3246, 0), new Tile(2991, 3246, 0), new Tile(2990, 3246, 0), new Tile(2989, 3246, 0), new Tile(2988, 3246, 0), new Tile(2988, 3245, 0), new Tile(2987, 3245, 0), new Tile(2986, 3245, 0), new Tile(2986, 3244, 0), new Tile(2985, 3244, 0), new Tile(2984, 3244, 0), new Tile(2983, 3244, 0), new Tile(2982, 3244, 0), new Tile(2981, 3244, 0), new Tile(2980, 3244, 0), new Tile(2980, 3243, 0), new Tile(2979, 3243, 0), new Tile(2979, 3242, 0), new Tile(2978, 3242, 0), new Tile(2978, 3241, 0), new Tile(2977, 3241, 0), new Tile(2977, 3240, 0), new Tile(2976, 3240, 0), new Tile(2976, 3239, 0), new Tile(2975, 3239, 0), new Tile(2974, 3239, 0) );
	//static final AreaPath areaPath_MiningToBank = new AreaPath(miningAreaToBank, miningToBankStartTiles, miningToBankEndTiles);
	
	private static boolean dropMode = false;
	
	private boolean autoMode = false;
	private Timer autoModeTimer = new Timer(60000);
	
	private static boolean exchange = false;
	private static ExchangeLoop exchangeLoop = new ExchangeLoop(-1, new ExchangeItem[] {
			new ExchangeItem(OreType.CLAY.getItemId(), OreType.CLAY.getItemName(), true),
			new ExchangeItem(softClayId, softClayName, true),
			new ExchangeItem(OreType.COPPER.getItemId(), OreType.COPPER.getItemName(), true),
			new ExchangeItem(OreType.IRON.getItemId(), OreType.IRON.getItemName(), true)
	});
	
	private int clayAccumulated;
	private int softClayAccumulated;
	private int copperOreAccumulated;
	private int ironOreAccumulated;
	
	private ItemTableListener itemTableListener = new ItemTableListener() {
		
		@Override
		public void replace(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			//System.out.println("replace[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize + " with old = " + originalItemId + ", " + originalItemStackSize);
			
			if (itemTableId == ItemTable.INVENTORY.getId()) {
				if (itemId == softClayId) {
					clayAccumulated--;
					softClayAccumulated++;
				}
			}
		}
		
		@Override
		public void remove(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("remove[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize,
				int originalItemId, int originalItemStackSize) {
			
		}
		
		@Override
		public void put(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			
		}
		
		@Override
		public void increase(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("increase[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void decrease(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("decrease[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
		}
		
		@Override
		public void add(long itemTableId, int itemTablePosition, int itemId, int itemStackSize) {
			//System.out.println("add[Table " + itemTableId + "] [" + itemTablePosition + "] = " + itemId + ", " + itemStackSize);
			
			if (itemTableId == ItemTable.INVENTORY.getId()) {
				if (itemId == OreType.CLAY.getItemId())
					clayAccumulated++;
				else if (itemId == OreType.COPPER.getItemId())
					copperOreAccumulated++;
				else if (itemId == OreType.IRON.getItemId())
					ironOreAccumulated++;
			}
		}
	};
	
	private enum Action {
		
		MINE(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Player player = Players.getMyPlayer();
				if (player == null)
					return null;
				
				if (MineLocation.getCurrent().getMainMineArea().contains(player.getData().getCenterLocation().getTile(false)))
					return SubAction.DEFAULT;
				else if (MineLocation.getCurrent().getMainMineArea().getDistanceToNearest() < 25)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		DROP(EnumSet.of(SubAction.DEFAULT)) {
			@Override
			public SubAction getSubAction() {
				return SubAction.DEFAULT;
			}
		},
		
		BANK(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Player player = Players.getMyPlayer();
				if (player == null)
					return null;
				
				Interactable bank = Bank.get.getNearest();
				Interactable depositBox = DepositBox.get.getNearest();
				if (bank != null && Calculations.distanceTo(bank) < 25)
					return SubAction.DEFAULT;
				else if (depositBox != null && Calculations.distanceTo(depositBox) < 25)
					return SubAction.DEFAULT;
				else if (Calculations.distance(player.getData().getCenterLocation().getTile(false), MineLocation.getCurrent().getBankTile()) <= 25)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		SOFTEN(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				if (Calculations.distanceTo(rimmingtonWellTiles[0]) <= 10)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		},
		
		EXCHANGE(EnumSet.of(SubAction.DEFAULT, SubAction.WALK)) {
			@Override
			public SubAction getSubAction() {
				Npc npc = GrandExchangeWidget.get.getNearest();
				if (npc != null && Calculations.distanceTo(npc) <= 10)
					return SubAction.DEFAULT;
				else
					return SubAction.WALK;
			}
		};
		
		EnumSet<SubAction> possibleSubs;
		
		Action(EnumSet<SubAction> possibleSubs) {
			this.possibleSubs = possibleSubs;
		}
		
		public static Action getNextAction() {
			if (!exchange) {
				if (exchangeLoop.getMainExchangeTimer() != null && !exchangeLoop.getMainExchangeTimer().isRunning()) {
					exchange = true;
					return EXCHANGE;
				}
				
				if (ItemTable.INVENTORY.countAllItems(false) >= 28) {
					if (ItemTable.INVENTORY.containsAny(OreType.CLAY.getItemId()))
						return SOFTEN;
					else {
						if (dropMode) {
							if (ItemTable.INVENTORY.containsAny(OreType.getCurrent().getItemId()))
								return DROP;
							else
								return BANK; // TODO: drop if should not bank what's in inventory
						} else
							return BANK;
					}
				} else
					return MINE;
			} else
				return EXCHANGE;
		}
		
		public abstract SubAction getSubAction();
	}
	
	public enum SubAction {
		DEFAULT, WALK
	};
	
	Filter<GroundObject> oresAreaFilter = new Filter<GroundObject>() {
		@Override
		public boolean accept(GroundObject animable) {
			if (OreType.getCurrent().getArea().contains(animable.getTile()))
				return true;
			
			return false;
		}
	};
	
	Condition rockIsLikelyDisposed = new Condition() {
		@Override
		public boolean evaluate() {
			return curAao == null || curAao.isDisposed();
		}
	};
	
	Condition rockIsDisposedDefinite = new Condition() {
		@Override
		public boolean evaluate() {
			if (curAao == null)
				return true;
			
			GroundObject[] objects = GroundObjects.getAllAtLocal(GroundObject.Type.Animable, curAaoRelativeTile.x, curAaoRelativeTile.y);
			if (objects != null) {
				if (objects[0] instanceof AnimableObject)
					return true;
			} else
				return true;
			
			return false;
		}
	};
	
	Condition rockIsDisposed_bothChecks = new Condition() {
		@Override
		public boolean evaluate() {
			if (rockIsLikelyDisposed.evaluate() || rockIsDisposedDefinite.evaluate())
				return true;
			
			return false;
		}
	};
	
	Condition rockIsDisposed_clearCur = new Condition() {
		@Override
		public boolean evaluate() {
			if (rockIsDisposed_bothChecks.evaluate()) {
				curAao = null;
				return true;
			}
			
			return false;
		}
	};
	
	Condition notNextToRock = new Condition() {
		@Override
		public boolean evaluate() {
			if (curAaoAbsoluteTile == null || Calculations.distanceTo(curAaoAbsoluteTile) > 2)
				return true;
			
			return false;
		}
	};
	
	Condition handledBreak = new Condition() {
		@Override
		public boolean evaluate() {
			if (breakHandlerParam != null)
				return breakHandlerParam.handleBreaks();
			
			return false;
		}
	};
	
	public boolean walkToOreArea() {
		return OreType.getCurrent().getArea().loopWalkTo(10000);
	}
	
	public AnimatedAnimableObject getNearestRock() {
		
		RSAnimable animable = null;
		
		Timer timer = new Timer(2000);
		while (timer.isRunning()) {
			animable = (RSAnimable) GroundObjects.getNearest(GroundObject.Type.Animable, oresAreaFilter, OreType.getCurrent().getObjectIds());
			
			if (animable != null) {
				if (animable instanceof AnimatedAnimableObject)
					return (AnimatedAnimableObject) animable;
			}
			
			Time.sleep(50);
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return Next rock that isn't currently being mined
	 */
	public AnimatedAnimableObject getNextRock() {
		RSAnimable animable = null;
		
		Timer timer = new Timer(2000);
		while (timer.isRunning()) {
			Filter<GroundObject> filter = new Filter<GroundObject>() {
				@Override
				public boolean accept(GroundObject go) {
					if (oresAreaFilter.accept(go)) {
						if (curAao != null && go.getObject() == curAao.getObject())
							return false;
						else
							return true;
					}
					
					return false;
				}
			};
			
			animable = (RSAnimable) GroundObjects.getNearest(GroundObject.Type.Animable, filter, OreType.getCurrent().getObjectIds());
			
			if (animable != null) {
				if (animable instanceof AnimatedAnimableObject)
					return (AnimatedAnimableObject) animable;
			}
			
			Time.sleep(50);
		}
		
		return null;
	}
	
	public boolean waitUntilStartMining(boolean breakOnRockMined, boolean doAntiban) {
		
		double dist = Calculations.distanceTo(curAaoAbsoluteTile);
		int maxPeriod;
		
		if (dist >= 5)
			maxPeriod = 5000;
		else if (dist > 2.5)
			maxPeriod = 4000;
		else if (dist > 1.5)
			maxPeriod = 3000;
		else
			maxPeriod = 2500;
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		return player.waitForAnimation(Constants.Animation.MINING.getIds(), maxPeriod, doAntiban, breakOnRockMined ? rockIsDisposed_clearCur: null);
	}
	
	public boolean waitUntilStopMining(boolean breakOnRockMined, boolean doAntiban) {
		
		Condition[] breakConditions;
		if (breakOnRockMined)
			breakConditions = new Condition[] {rockIsDisposed_bothChecks, notNextToRock};
		else
			breakConditions = new Condition[] {notNextToRock};
		
		Player player = Players.getMyPlayer();
		if (player == null)
			return false;
		
		return player.waitForIdleAnimation(Constants.Animation.MINING.getIds(), 60000, doAntiban, breakConditions);
	}
	
	public boolean mine() {
		
		if (Camera.getZoomPercent() > 10) {
			Camera.setZoomPercent(Random.nextInt(0, 10));
		}
		
		Tab.closeAll();
		
		if (!walkToOreArea())
			return false;
		
		AnimatedAnimableObject aao = getNearestRock();
		if (aao == null)
			return false;
		
		curAao = aao;
		curAaoLoc = aao.getData().getCenterLocation();
		curAaoAbsoluteTile = curAaoLoc.getGlobalTile(true);
		curAaoRelativeTile = curAaoLoc.getLocalTile(true);
		
		Paint.updatePaintObject(aao);
		
		Model model = aao.getModel();
		
		if (model == null || !curAaoAbsoluteTile.isOnScreen()) {
			Tile walkTile = null;
			
			Timer walkTimer = new Timer(5000);
			while (walkTimer.isRunning() && (walkTile == null || !OreType.getCurrent().getArea().contains(walkTile))) {
				walkTile = curAaoAbsoluteTile.randomize(2, 2);
			}
			
			walkTimer.reset();
			while (walkTimer.isRunning() && !walkTile.isOnScreen()) {
				//System.out.println("walking");
				
				Player player = Players.getMyPlayer();
				if (player == null)
					return false;
				
				if (!player.isMoving() || Calculations.distanceTo(Walking.getDestination()) <= 7)
					Walking.walk(Walking.getClosestOnMap(walkTile));
				
				Time.sleepMedium();
			}
			
			if (curAao.isDisposed())
				return false;
		}
		
		if (!curAao.getTile().isOnScreen() && Camera.getPitch() < 50) {
			Camera.setPitch(Random.nextInt(50, 74));
			Time.sleepVeryQuick();
		}
		
		if (aao.loopInteract(rockIsLikelyDisposed, "Mine", OreType.getCurrent().getMenuOption(), true)) {
			boolean result = waitUntilStartMining(true, true);
			if (result) {
				// hover next rock
				Time.sleepQuick();
				
				if (Camera.getPitch() < 50) {
					Camera.setPitch(Random.nextInt(50, 74));
					Time.sleepVeryQuick();
				}
				
				aao = getNextRock();
				if (aao != null) {
					model = aao.getModel();
					if (model != null) {
						model.hover(aao.getInternalTile());
						Time.sleepQuickest();
					}
				}
			}
			
			return result;
		}
		
		return false;
	}
	
	public boolean soften() {
		if (ProcessingItemsWidget.get.isClosed() && MakeItemsWidget.get.isClosed()) {
			if (!Client.isUsableComponentSelected()) {
				if (Tab.BACKPACK.open(true)) {
					Component clay = InventoryWidget.get.getItem(OreType.CLAY.getItemId());
					if (clay != null) {
						if (clay.interact("Use", OreType.CLAY.getItemName())) {
							Time.sleepQuick();
						}
					}
				}
			}
			
			if (Client.isUsableComponentSelected()) {
				Tile wellTile = rimmingtonWellTiles[Random.nextInt(0, rimmingtonWellTiles.length)];
				if (!wellTile.isOnScreen()) {
					Camera.turnTo(wellTile);
					Time.sleepQuick();
				}
				
				if (wellTile.loopInteract(null, "Use")) {
					Timer timer = new Timer(5000);
					while (timer.isRunning()) {
						if (MakeItemsWidget.get.isOpen())
							break;
						
						if (ProcessingItemsWidget.get.isOpen())
							break;
						
						Time.sleep(50);
					}
				}
			}
		}
		
		
		if (MakeItemsWidget.get.isOpen()) {
			if (MakeItemsWidget.get.clickMakeButton(true)) {
				Time.sleepQuick();
				Timer timer = new Timer(3000);
				while (timer.isRunning()) {
					if (ProcessingItemsWidget.get.isOpen())
						break;
					
					Time.sleep(50);
				}
			}
		}
		
		if (ProcessingItemsWidget.get.isOpen()) {
			Timer timer = new Timer(40000);
			while (timer.isRunning()) {
				if (ProcessingItemsWidget.get.isClosed())
					return true;
				
				if (Antiban.doAntiban())
					Time.sleepVeryQuick();
				
				Time.sleep(50);
			}
		}
		
		return false;
	}
	
	public boolean bank() {
		if (Bank.get.open()) {
			Time.sleepQuick();
			
			if (Bank.get.depositAllInvItems(runePickId)) {
				Time.sleepQuick();
				
				if (ItemTable.BANK.getCoinCount() > 0) { // if bank contains coins, add them to pouch
					Bank.get.withdrawAll(995, false); // coins
					Time.sleepQuick();
				}
				
				Bank.get.close();
				
				return true;
			}
		} else if (DepositBox.get.open()) {
			Time.sleepQuick();
			
			if (DepositBox.get.depositAllItems(runePickId)) {
				Time.sleepQuick();
				DepositBox.get.close();
				
				return true;
			}
		}
		
		return false;
	}
	
	public boolean drop() {
		if (!ItemTable.INVENTORY.containsAny(OreType.getCurrent().getItemId()))
			return true;
		
		ActionSlot slot10 = ActionSlot.getSlot(10);
		ActionSlot slot11 = ActionSlot.getSlot(11);
		ActionSlot slot12 = ActionSlot.getSlot(12);
		
		if (slot10.getItemId() != OreType.getCurrent().getItemId()
				&& slot11.getItemId() != OreType.getCurrent().getItemId()
				&& slot12.getItemId() != OreType.getCurrent().getItemId()) {
			
			ActionSlot itemSlot = null;
			switch (Random.nextInt(0, 3)) {
			case 0:
				itemSlot = slot10;
				break;
			case 1:
				itemSlot = slot11;
				break;
			case 2:
				itemSlot = slot12;
				break;
			}
			
			if (!itemSlot.setItem(OreType.getCurrent().getItemId()))
				return false;
			Time.sleepMedium();
			Time.sleepMedium();
		}
		
		ActionSlot itemSlot = null;
		if (slot10.getItemId() == OreType.getCurrent().getItemId())
			itemSlot = slot10;
		else if (slot11.getItemId() == OreType.getCurrent().getItemId())
			itemSlot = slot11;
		else if (slot12.getItemId() == OreType.getCurrent().getItemId())
			itemSlot = slot12;
		
		if (itemSlot != null) {
			Keyboard.pressKey(itemSlot.getHotkeyCode());
			Time.sleepMedium();
			Timer timer = new Timer(10000);
			while (timer.isRunning()) {
				if (ItemTable.INVENTORY.countItems(false, OreType.getCurrent().getItemId()) == 0) {
					Time.sleepVeryQuick();
					break;
				}
				
				Time.sleep(50);
			}
			Keyboard.releaseKey(itemSlot.getHotkeyCode());
			
			if (ItemTable.INVENTORY.countAllItems(false, runePickId) != 0) {
				Time.sleepQuick();
				
				// drop rest of inventory
				return InventoryWidget.get.dropAllItems(runePickId);
			} else
				return true;
		}
		
		return false;
	}
	
	boolean boughtRunePick = true;
	boolean retrievedRunePick = false;
	
	public boolean retrieveRunePick() {
		if (Toolbelt.Pickaxe.RUNE.equipped())
			return true;
		
		if (retrievedRunePick)
			return true;
		
		if (!GrandExchangeWidget.get.close(true))
			return false;
		
		if (equipRunePick())
			return true;
		
		System.out.println("Retrieving rune pick from bank...");
		
		if (Bank.get.open(true)) {
			Time.sleepQuick();
			
			if (ItemTable.BANK.containsAny(runePickId)) {
				if (!Bank.get.withdraw(runePickId, 1, false))
					return false;
				
				Time.sleepMedium();
				
				if (ItemTable.INVENTORY.containsAny(runePickId)) {
					if (Bank.get.close(true)) {
						Time.sleepQuick();
						if (equipRunePick()) {
							retrievedRunePick = true;
							return true;
						}
					}
				}
			} else
				boughtRunePick = false;
		}
		
		return false;
	}

	public boolean equipRunePick() {
		if (Toolbelt.Pickaxe.RUNE.equipped())
			return true;
		
		// Add rune pick to toolbelt if it's in inventory
		if (ItemTable.INVENTORY.containsAny(runePickId)) {
			if (!Bank.get.close(true))
				return false;
			
			if (Ribbon.Tab.BACKPACK.open(true)) {
				Component pick = InventoryWidget.get.getItem(runePickId);
				if (!pick.interact("Add to tool belt")) {
					Time.sleepMedium();
					return false;
				}
				Time.sleepMedium();
				if (Toolbelt.Pickaxe.RUNE.equipped())
					return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	Failsafe failsafeWalkedToMine;
	Failsafe failsafeWalkedToBank;
	Failsafe failsafeMinedRock;
	Failsafe failsafeDropOrBank;
	
	BreakHandlerParam breakHandlerParam = null;
	
	@Override
	public List<ScriptParameter> setupParams() {
		
		List<ScriptParameter> params = new ArrayList<>();
		params.add(new DefaultParam());
		params.add(new RandomReseedParam());
		params.add(new RunTimeParam());
		
		List<Failsafe> failsafes = new ArrayList<>();
		failsafeWalkedToMine = new Failsafe("Walked to Mine", 900000); // 15 mins.
		failsafeWalkedToBank = new Failsafe("Walked to Bank", 900000); // 15 mins.
		failsafeMinedRock = new Failsafe("Mine", 900000); // 15 mins.
		failsafeDropOrBank = new Failsafe("Drop or Bank", 900000); // 15 mins.
		failsafes.add(failsafeWalkedToMine);
		failsafes.add(failsafeWalkedToBank);
		failsafes.add(failsafeMinedRock);
		failsafes.add(failsafeDropOrBank);
		params.add(new FailsafesParam(failsafes));
		
		params.add(new LoginParam());
		
		breakHandlerParam = new BreakHandlerParam(failsafes);
		params.add(breakHandlerParam);
		
		//int period = 10000;
		//int maxScreenshots = Math.round((failsafeWalkedToMine.getPeriod() + failsafeWalkedToBank.getPeriod()) / period) + 1;
		//params.add(new SaveScreenshotsParam(period, maxScreenshots));
		params.add(new SaveScreenshotsParam());
		
		params.add(new CloseUselessInterfacesParam(false));
		params.add(new CloseUselessInterfacesParam(true));
		
		params.add(new DeathHandlerParam());
		
		return params;
	}
	
	@Override
	public void loadDataParams() {
		boolean success = false;
		if (AccountInfo.cur != null) {
			AccountRow row = AccountInfo.cur.getRow();
			if (row != null) {
				Map<String, String> params = row.getScriptParams_toMap();
				String oreType = params.get("OreType");
				if (oreType != null) {
					success = true;
					
					oreType = oreType.toLowerCase();
					if (oreType.contains("copper"))
						OreType.setCurrent(OreType.COPPER);
					else if (oreType.contains("iron"))
						OreType.setCurrent(OreType.IRON);
					else if (oreType.contains("auto"))
						autoMode = true;
					else
						success = false;
				}
				
				if (success) {
					String mineLocation = params.get("MineLocation");
					if (mineLocation != null) {
						success = true;
						
						mineLocation = mineLocation.toLowerCase();
						if (mineLocation.contains("rimmington"))
							MineLocation.setCurrent(MineLocation.RIMMINGTON);
						else if (mineLocation.contains("auto"))
							autoMode = true;
						else
							success = false;
					}
				}
			}
		}
		
		if (!success) {
			System.out.println("Miner: Error loading params! Loaded default data params.");
			//OreType.setCurrent(OreType.IRON);
			//MineLocation.setCurrent(MineLocation.RIMMINGTON);
			autoMode = true;
		} else {
			System.out.println("Miner: Successfully loaded data params: ");
		}
		
		if (!autoMode) {
			System.out.println("\t-OreType: " + OreType.getCurrent().name());
			System.out.println("\t-MineLocation: " + MineLocation.getCurrent().name());
		} else
			System.out.println("\t-Auto mode enabled");
	}
	
	public int getOresPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int totalOres = softClayAccumulated + clayAccumulated + copperOreAccumulated + ironOreAccumulated;
			int oresPerHour = (int) (totalOres / timeDiffHours);
			return oresPerHour;
		}
		
		return 0;
	}
	
	public int getGpPerHour() {
		long timeDiff = System.currentTimeMillis() - getStartTime();
		double timeDiffHours = timeDiff / 3600000D;
		if (timeDiffHours > 0) { // to prevent divide by zero
			int clayGp = clayAccumulated*250;
			int softClayGp = softClayAccumulated*318;
			int copperGp = copperOreAccumulated*90;
			int ironGp = ironOreAccumulated*315;
			int totalGp = clayGp + softClayGp + copperGp + ironGp;
			int gpPerHour = (int) (totalGp / timeDiffHours);
			return gpPerHour;
		}
		
		return 0;
	}
	
	@Override
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("-Miner-");
		sb.append(System.lineSeparator());
		
		if (clayAccumulated > 0) {
			sb.append("Clay accumulated: " + clayAccumulated);
			sb.append(System.lineSeparator());
		}
		if (softClayAccumulated > 0) {
			sb.append("Soft clay accumulated: " + softClayAccumulated);
			sb.append(System.lineSeparator());
		}
		if (copperOreAccumulated > 0) {
			sb.append("Copper ore accumulated: " + copperOreAccumulated);
			sb.append(System.lineSeparator());
		}
		if (ironOreAccumulated > 0) {
			sb.append("Iron ore accumulated: " + ironOreAccumulated);
			sb.append(System.lineSeparator());
		}
		
		sb.append("Ores/hr: " + getOresPerHour());
		sb.append(System.lineSeparator());
		
		sb.append("GP/hr: " + getGpPerHour() / 1000 + "k");
		sb.append(System.lineSeparator());
		
		return sb.toString();
	}
	
	@Override
	public boolean onStart() {
		
		clayAccumulated = 0;
		softClayAccumulated = 0;
		copperOreAccumulated = 0;
		ironOreAccumulated = 0;
		
		dropMode = false;
		
		ItemTableCallback.addListener(itemTableListener);
		
		loadDataParams();
		
		Antiban.resetTimer();
		
		return true;
	}
	
	AnimatedAnimableObject curAao = null;
	Location curAaoLoc = null;
	Tile curAaoAbsoluteTile = null;
	Tile curAaoRelativeTile = null;
	
	
	/*
	 * Script Notes
	 * 
	 * - While mining, the player's animation id changes to the mining animation id. But after some time, the animation id changes to -1 (default or no animation id). And then after some time it changes back to the mining animation. So, some kind of interaction hook would be best, otherwise checking if the default/no animation id is sustained for some time is required.
	 */
	
	// TODO: task complete on level 6 mining
	// TODO: randomize items in drop action
	// TODO: check interface at level 10 mining to see if it's same as wc and get text message, etc.
	@Override
	public void run() {
		
		Antiban.randomMouseSpeed();
		
		if (!Client.loopLogin())
			return;
		
		if (autoMode) {
			if (!autoModeTimer.isRunning() || OreType.getCurrent() == null) {
				if (Skill.CRAFTING.getRealLevel() < 6) {
					OreType.setCurrent(OreType.CLAY);
					dropMode = false;
				} else if (Skill.MINING.getCurrentLevel() >= 41) { // 15 is required but with competition, 41 is better (rune pick)
					OreType.setCurrent(OreType.IRON);
					dropMode = false;
				} else {
					OreType.setCurrent(OreType.COPPER);
					dropMode = true;
				}
				
				MineLocation.setCurrent(MineLocation.RIMMINGTON);
				
				autoModeTimer.reset();
			}
		}
		
		if (!exchange && !exchangeLoop.getMainExchangeTimer().isRunning()) {
			if (!Toolbelt.Pickaxe.RUNE.equipped())
				exchangeLoop.setMainExchangePeriod(Random.nextInt(3600000, 5400000)); // 1 to 1.5 hrs
			else
				exchangeLoop.setMainExchangePeriod(Random.nextInt(10800000, 18000000)); // 3 to 5 hrs
		}
		
		if (!DepositBox.get.close(true))
			return;
		
		if (!exchange) {
			if (!Bank.get.close(true))
				return;
			
			if (!GrandExchangeWidget.get.close(true))
				return;
		}
		
		if (curAao != null) {
			waitUntilStopMining(true, true);
			curAao = null;
		}
		
		if (ItemTable.INVENTORY.containsUselessItems()) {
			if (Ribbon.Tab.BACKPACK.open(true)) {
				InventoryWidget.get.destroyUselessItems();
				Time.sleepVeryQuick();
			}
			
			return;
		}
		
		equipRunePick();
		
		ChatBox.get.setAlwaysOnMode(false, true);
		
		if (!Lodestone.PORT_SARIM.isActivated()) {
			failsafeDropOrBank.reset();
			failsafeMinedRock.reset();
			failsafeWalkedToBank.reset();
			failsafeWalkedToMine.reset();
			
			Lodestone.PORT_SARIM.activate();
			Time.sleepMedium();
			return;
		}
		
		Action action = Action.getNextAction();
		SubAction subAction = action.getSubAction();
		System.out.println(action + ", " + subAction);
		
		switch (action) {
		case MINE:
			switch (subAction) {
			case DEFAULT:
				if (!mine())
					curAao = null;
				else
					failsafeMinedRock.reset();
				
				break;
			case WALK:
				curAao = null;
				
				if (MineLocation.getCurrent().getMainMineArea().getDistanceToNearest() < 100) {
					if (Random.nextInt(0, 100) <= 75) {
						if (Random.nextBoolean())
							Time.sleepMedium();
						else
							Time.sleepQuick();
					}
					
					TilePath path;
					TilePath path1 = MineLocation.getCurrent().getRandomPathToBank().reverse();
					TilePath path2 = MineLocation.getCurrent().getRandomPathToMine_fromLodestone();
					
					if (path1.getDistanceToNearest() <= path2.getDistanceToNearest())
						path = path1;
					else
						path = path2;
					
					if (path.loopWalk(90000, handledBreak, true)) // 90s max
						failsafeWalkedToMine.reset();
				} else {
					if (Lodestone.PORT_SARIM.teleport(true)) {
						
					}
				}
				
				break;
			}
			
			break;
		case DROP:
			curAao = null;
			
			drop();
			failsafeDropOrBank.reset();
			
			failsafeWalkedToMine.reset();
			failsafeWalkedToBank.reset();
			
			Time.sleepVeryQuick();
			break;
		case SOFTEN:
			curAao = null;
			
			switch (action.getSubAction()) {
			case DEFAULT:
				if (soften()) {
					failsafeDropOrBank.reset();
				}
				
				break;
			case WALK:
				if (rimmingtonWellTiles[Random.nextInt(0, rimmingtonWellTiles.length)].randomize(4, 4).loopWalkTo(30000)) {
					
				}
				
				break;
			}
			break;
		case BANK:
			curAao = null;
			
			switch (action.getSubAction()) {
			case DEFAULT:
				if (bank()) {
					failsafeDropOrBank.reset();
					failsafeWalkedToBank.reset();
				}
				
				break;
			case WALK:
				if (Calculations.distanceTo(MineLocation.getCurrent().getBankTile()) < 120) {
					if (Random.nextInt(0, 100) <= 75) {
						if (Random.nextBoolean())
							Time.sleepMedium();
						else
							Time.sleepQuick();
					}
					
					TilePath path;
					
					if (Calculations.distanceTo(MineLocation.getCurrent().getBankTile()) < 80 || Calculations.distanceTo(rimmingtonWellTiles[0]) < 40)
						path = MineLocation.getCurrent().getRandomPathToBank();
					else
						path = MineLocation.getCurrent().getRandomPathToMine_fromLodestone().reverse();
						
					if (path.loopWalk(90000, handledBreak, true)) // 90s max
						failsafeWalkedToBank.reset();
				} else {
					if (Lodestone.PORT_SARIM.teleport(true)) {
						
					}
				}
				
				break;
			}
			
			break;
		case EXCHANGE:
			switch (action.getSubAction()) {
			case DEFAULT:
				failsafeDropOrBank.reset();
				failsafeMinedRock.reset();
				failsafeWalkedToBank.reset();
				failsafeWalkedToMine.reset();
				
				if (exchangeLoop.exchange()) {
					// equip pick if bought
					if (!Toolbelt.Pickaxe.RUNE.equipped() && GrandExchangeWidget.get.open(true)) {
						Time.sleepQuick();
						
						// buy pick
						if (!boughtRunePick && !retrievedRunePick && !Toolbelt.Pickaxe.RUNE.equipped() && ItemTable.MONEYPOUCH.getCoinCount() >= 25000 && !ItemTable.INVENTORY.containsAny(runePickId) && !ItemTable.INVENTORY.containsAny(runePickId + 1)) {
							if (!GESlot.anyContainItem(runePickId)) {
								System.out.println("Buying pick...");
								if (GESlot.getRandomEmptySlot().buyByMarketPrice(runePickId, runePickName, 1, Random.nextInt(3, 5)))
									boughtRunePick = true;
								else
									break;
								
								Time.sleepMedium();
							}
						}
						
						// Wait to see if it sells, then collect
						if (boughtRunePick && !retrievedRunePick && !Toolbelt.Pickaxe.RUNE.equipped()) {
							// make sure there is an offer
							boolean offerExists = false;
							Timer timer = new Timer(5000);
							while (timer.isRunning()) {
								if (GESlot.anyContainItem(runePickId)) {
									offerExists = true;
									break;
								}
								
								Time.sleep(50);
							}
							
							if (offerExists) {
								timer = new Timer(300000); // 5 mins
								while (timer.isRunning()) {
									GESlot pickSlot = GESlot.findByItemId(runePickId);
									if (pickSlot != null && pickSlot.getStatus().isFinalized()) {
										Time.sleepQuick();
										break;
									}
									
									Time.sleep(5000);
								}
							}
						}
						
						// Retrieve rune pick if in GE (regardless if it's been bought during this script)
						GESlot pickSlot = GESlot.findByItemId(runePickId);
						if (pickSlot != null && pickSlot.getStatus().isFinalized()) {
							boughtRunePick = true;
							
							if (!pickSlot.collect(false))
								break;
							
							Time.sleepMedium();
							Time.sleepQuick();
						}
						
						if (boughtRunePick && !retrieveRunePick())
							break;
					} else if (!Toolbelt.Pickaxe.RUNE.equipped() && ItemTable.MONEYPOUCH.getCoinCount() >= 25000)
						break;
					
					exchange = false;
				}
				
				break;
			case WALK:
				if (Bank.get.isOpen())
					Bank.get.close(true);
				
				if (GrandExchange.walkToSWBooth()) {
					
				}
				
				break;
			}
			
			break;
		}
		
		//Time.sleep(50);
		
		//System.gc();
	}
	
	@Override
	public boolean onStop() {
		ItemTableCallback.removeListener(itemTableListener);
		
		System.out.println("Final Ores/hr: " + getOresPerHour());
		System.out.println("Final GP/hr: " + getGpPerHour() + " (" + getGpPerHour() / 1000 + "k)");
		
		return true;
	}
}
