package org.rs3.gamepack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ASTORE;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.DUP;
import org.apache.bcel.generic.INVOKESPECIAL;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.PUTFIELD;

import searcher.Comparator.CompareType;
import searcher.CompareInstruction;
import searcher.Searcher;
import searcher.pi.PI_CHECKCAST;
import searcher.pi.PI_INVOKESTATIC;
import searcher.pi.PI_NEW;
import searcher.pi.PI_PUTFIELD;
import data.ClassData;
import data.FieldData;

public class GamePackUpdater {
	
	public HashMap<String, ClassGen> classes = new HashMap<String, ClassGen>();
	
	String classLoaderClass = null;
	String classLoaderHolderClass = null;
	FieldData classLoaderMethod = new FieldData("classLoaderMethod", (ClassData) null);
	int classLoaderMethodParameterIndex = -1; // TODO: not actually used to parse method parameter stack
	
	public FieldData classLoaderHolder = new FieldData("classLoaderHolder", (ClassData) null);
	public FieldData classLoader = new FieldData("classLoader", (ClassData) null);
	
	boolean success = false;
	
	public boolean isSuccessful() {
		return success;
	}
	
	public GamePackUpdater(String jarFilePath) {
		ClassGen[] cgs = parseJar(jarFilePath);
		
		if (cgs == null) {
			System.out.println("ERROR: Gamepack Updater: ClassGens is null!");
			return;
		}

		for (ClassGen cg : cgs) {
			classes.put(cg.getClassName(), cg);
		}
	}
	
	public ClassGen[] parseJar(String jarFilePath) {
		ClassGen[] cgs = null;

		File file = new File(jarFilePath);

		JarFile jarFile = null;
		
		try {
			jarFile = new JarFile(file);
			
			Enumeration<JarEntry> en = jarFile.entries();

			cgs = new ClassGen[jarFile.size()];
			int cgi = 0;

			while (en.hasMoreElements()) {
				JarEntry entry = en.nextElement();
				if (entry.getName().endsWith(".class")) {
					ClassParser cp = new ClassParser(jarFile.getInputStream(entry), entry.getName());
					JavaClass jc = cp.parse();
					ClassGen cg = new ClassGen(jc);

					cgs[cgi] = cg;
					cgi++;
				}
				//else
				//	fileNames.add(entry.getName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (jarFile != null) {
				try {
					jarFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (cgs != null) {
			int count = 0;
			for (ClassGen cg : cgs) {
				if (cg != null)
					count++;
			}

			int i = 0;
			ClassGen[] cgsTrimmed = new ClassGen[count];
			for (ClassGen cg : cgs) {
				if (cg != null) {
					cgsTrimmed[i] = cg;
					i++;
				}
			}

			cgs = cgsTrimmed;
		}

		return cgs;
	}
	
	public boolean run() {
		success = false;
		
		if (findClassLoaderClass()) {
			for (ClassGen cg : classes.values()) {
				if (findClassLoaderVariable(cg)) {
					if (findClassLoaderHolderVariable())
						success = true;
				}
			}
		}
		
		System.out.println("GamepackUpdater: Found classLoader class: " + classLoaderClass);
		
		System.out.println("GamepackUpdater: client classloader: " + classLoader.getInternalClassName() + "." + classLoader.getInternalName());
		System.out.println("GamepackUpdater: client classloader holder class: " + classLoaderHolderClass);
		System.out.println("GamepackUpdater: client classloader usage method: " + classLoaderMethod.getInternalClassName() + "." + classLoaderMethod.getInternalName() + classLoaderMethod.getSignature());
		
		System.out.println("GamepackUpdater: client classloader holder: " + classLoaderHolder.getInternalClassName() + "." + classLoaderHolder.getInternalName());
	
		return success;
	}
	
	public boolean findClassLoaderClass() {
		boolean found = false;
		
		for (ClassGen cg : classes.values()) {
			if (cg.getSuperclassName().equals("java.lang.ClassLoader")) {
				classLoaderClass = cg.getClassName();
				found = true;
			}
		}
		
		return found;
	}
	
	/*
	 * paraml2 is Rs2Applet.p passed in Rs2Applet.init()
	 * 
	 static final l a(l paraml1, l paraml2, byte paramByte) {
	 
	 	...
	 	
	  k localk = (k)paraml2;

      a locala = new a(paraml2);
      
      ((e)paraml2).f = paraml1;
      localk.p = locala;
      ((j)paraml2).i = new Hashtable();
      ((g)paraml2).n = new Hashtable();
      
      	...
      	
      }
	 */
	public boolean findClassLoaderVariable(ClassGen cg) {
		ConstantPoolGen cpg = cg.getConstantPool();

		boolean found = false;
		
		for (Method m : cg.getMethods()) {
			MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
			
			InstructionList il = mg.getInstructionList();
			
			if (il == null || il.getLength() == 0)
				continue;
			
			Searcher searcher = new Searcher(il);
			
			//if (mg.getArgumentTypes().length == 0) {
			final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
					new PI_NEW(cpg).adjustTypeString(false, classLoaderClass),
					DUP.class,
					ALOAD.class,
					INVOKESPECIAL.class,
					ASTORE.class
			);
			

			
			List<CompareInstruction[]> patterns1 = new ArrayList<>();
			patterns1.add(pattern1);
			
			InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
			
			if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
					ASTORE astore = (ASTORE) ih.getNext().getNext().getNext().getNext().getInstruction();
					
					final CompareInstruction[] pattern2 = CompareInstruction.Methods.toCompareInstructions(
							ALOAD.class,
							new ALOAD(astore.getIndex()),
							PUTFIELD.class
					);
					
					List<CompareInstruction[]> patterns2 = new ArrayList<>();
					patterns2.add(pattern2);
					
					InstructionHandle[][] matches2 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns2);
					
					if (matches2 != null && matches2.length == patterns2.size()) {
						for (InstructionHandle ih2 : matches2[0]) {
							PUTFIELD putField = (PUTFIELD) ih2.getNext().getNext().getInstruction();
							ALOAD aload = (ALOAD) ih2.getInstruction();
							
							classLoader.set(putField.getReferenceType(cpg).toString(), putField.getFieldName(cpg), putField.getSignature(cpg));
							
							final CompareInstruction[] pattern3 = CompareInstruction.Methods.toCompareInstructions(
									ALOAD.class,
									new PI_CHECKCAST(cpg).adjustTypeString(false, classLoader.getInternalClassName()),
									new ASTORE(aload.getIndex())
							);
							
							List<CompareInstruction[]> patterns3 = new ArrayList<>();
							patterns3.add(pattern3);
							
							InstructionHandle[][] matches3 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns3);
							
							if (matches3 != null && matches3.length == patterns3.size()) {
								for (InstructionHandle ih3 : matches3[0]) {
									ALOAD aload2 = (ALOAD) ih3.getInstruction();
									
									classLoaderMethodParameterIndex = aload2.getIndex();
									classLoaderHolderClass = mg.getArgumentType(classLoaderMethodParameterIndex).toString();
									
									classLoaderMethod.set(cg.getClassName(), mg.getName(), mg.getSignature());
									
									found = true;
								}
							}
						}
					}
				}
			}
		//}
		}
	
		return found;
	}
	
	public boolean findClassLoaderHolderVariable() {
		boolean found = false;
		
		ClassGen cg = classes.get("Rs2Applet");
		
		if (cg != null) {
			ConstantPoolGen cpg = cg.getConstantPool();
			
			for (Method m : cg.getMethods()) {
				MethodGen mg = new MethodGen(m, cg.getClassName(), cpg);
				
				InstructionList il = mg.getInstructionList();
				
				if (il == null || il.getLength() == 0)
					continue;
				
				Searcher searcher = new Searcher(il);
				
				//if (mg.getArgumentTypes().length == 0) {
				final CompareInstruction[] pattern1 = CompareInstruction.Methods.toCompareInstructions(
						new PI_INVOKESTATIC(cpg).adjustClassName(classLoaderMethod.getInternalClassName()).adjustMethodName(classLoaderMethod.getInternalName()).adjustSignature(classLoaderMethod.getSignature())
				);
				
				List<CompareInstruction[]> patterns1 = new ArrayList<>();
				patterns1.add(pattern1);
				
				InstructionHandle[][] matches1 = searcher.searchAllFromStart(true, false, CompareType.Opcode, patterns1);
				
				if (matches1 != null && matches1.length == patterns1.size()) {
					for (InstructionHandle ih : matches1[0]) {
						searcher.gotoHandle(ih);
						/*InstructionHandle getFieldIh = searcher.searchPrev(false, CompareType.Opcode, new PI_GETFIELD(cpg).adjustClassName("Rs2Applet").adjustTypeString(false, classLoaderHolderClass));
						
						if (getFieldIh != null) {
							GETFIELD getField = (GETFIELD) getFieldIh.getInstruction();
							
							classLoaderHolder.set(getField.getReferenceType(cpg).toString(), getField.getFieldName(cpg), getField.getSignature(cpg));
							found = true;
						}*/
						
						InstructionHandle putFieldIh = searcher.searchNext(false, CompareType.Opcode, new PI_PUTFIELD(cpg).adjustClassName("Rs2Applet").adjustTypeString(false, classLoaderHolderClass));
						
						if (putFieldIh != null) {
							PUTFIELD putField = (PUTFIELD) putFieldIh.getInstruction();
							
							classLoaderHolder.set(putField.getReferenceType(cpg).toString(), putField.getFieldName(cpg), putField.getSignature(cpg));
							found = true;
						}
					}
				}
			}
		}
		
		return found;
	}
}
