package org.rs3.gamepack;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import data.ClassData;
import data.FieldData;

public class OldGamePackUpdater {
	
	/*public HashMap<String, ClassNode> classes = null;
	
	public FieldData classLoaderHolder = new FieldData("classLoaderHolder", (ClassData) null);
	public FieldData classLoader = new FieldData("classLoader", (ClassData) null);
	
	boolean success = false;
	
	public boolean isSuccessful() {
		return success;
	}
	
	public OldGamePackUpdater(String jarFilePath) {
		try {
			parseJar(new JarFile(jarFilePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void parseJar(JarFile jarFile) {
		classes = new HashMap<String, ClassNode>();
		
		Enumeration<JarEntry> enumeration = jarFile.entries();
		
		try {
			while (enumeration.hasMoreElements()) {
				JarEntry entry = enumeration.nextElement();
				
				if (entry.getName().endsWith(".class")) {
					ClassReader classReader = new ClassReader(jarFile.getInputStream(entry));
					ClassNode classNode = new ClassNode();
					classReader.accept(classNode, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
					classes.put(classNode.name, classNode);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				jarFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	String classLoaderClass = null;
	
	String classLoaderStaticCreationMethod_class = null;
	String classLoaderStaticCreationMethod_method = null;
	String classLoaderStaticCreationMethod_desc = null;
	
	String classLoaderStaticCreationMethodUsageMethod_class = null;
	String classLoaderStaticCreationMethodUsageMethod_method = null;
	String classLoaderStaticCreationMethodUsageMethod_desc = null;
	
	String classLoaderOriginalInvocationMethod_class = null;
	String classLoaderOriginalInvocationMethod_method = null;
	String classLoaderOriginalInvocationMethod_desc = null;
	
	public boolean run() {
		success = false;
		
		if (findClassLoaderClass())
			if (findClassLoaderStaticCreationMethod())
				if (findClassLoaderStaticCreationMethodUsageMethod())
					if (findClassLoaderOriginalInvocationMethod())
						success = true;
		
		// print details
		System.out.println("Gamepack Updater Results:");
		if (classLoaderClass != null)
			System.out.println("[Gamepack Updater] Found client classloader class: " + classLoaderClass);
		else
			System.out.println("[Gamepack Updater] Unable to find classloader class!");
		
		if (classLoaderStaticCreationMethod_method != null)
			System.out.println("[Gamepack Updater] Found client classloader static creation method: " + classLoaderStaticCreationMethod_class + "." + classLoaderStaticCreationMethod_method);
		else
			System.out.println("[Gamepack Updater] Unable to find client classloader static creation method!");
		
		if (classLoaderStaticCreationMethodUsageMethod_method != null)
			System.out.println("[Gamepack Updater] Found client classloader static creation method usage method: " + classLoaderStaticCreationMethodUsageMethod_class + "." + classLoaderStaticCreationMethodUsageMethod_method);
		else
			System.out.println("[Gamepack Updater] Unable to find client classloader static creation method usage method!");
		
		if (classLoaderOriginalInvocationMethod_method != null)
			System.out.println("[Gamepack Updater] Found client classloader original invocation method: " + classLoaderOriginalInvocationMethod_class + "." + classLoaderOriginalInvocationMethod_method);
		else
			System.out.println("[Gamepack Updater] Unable to find client classloader original invocation method!");
		
		// alternative
		for (ClassNode cn : classes.values()) {
			if (analyze_classLoader1(cn))
				success = true;
			
			if (analyze_classLoader2(cn))
				success = true;
		}
		
		return success;
	}
	
	public boolean findClassLoaderClass() {
		boolean found = false;
		
		for (ClassNode cn : classes.values()) {
			if (cn.superName.equals("java/lang/ClassLoader")) {
				classLoaderClass = cn.name;
				System.out.println("Found client classloader class: " + classLoaderClass);
				
				found = true;
			}
		}
		
		return found;
	}
	
	// find static method that creates classLoader instance
	public boolean findClassLoaderStaticCreationMethod() {
		boolean found = false;
		
		for (ClassNode cn : classes.values()) {
			@SuppressWarnings("unchecked")
			ListIterator<MethodNode> mli = cn.methods.listIterator();
			
	    	while (mli.hasNext()) {
	    		MethodNode mn = mli.next();
	    		
	    		if (classLoaderClass.equals(Type.getReturnType(mn.desc).getClassName())) {
	    			classLoaderStaticCreationMethod_class = cn.name;
	    			classLoaderStaticCreationMethod_method = mn.name;
	    			classLoaderStaticCreationMethod_desc = mn.desc;
	    			
	    			System.out.println("[Gamepack Updater] Found client classloader static creation method: " + classLoaderStaticCreationMethod_class + "." + classLoaderStaticCreationMethod_method);
	    		
	    			found = true;
	    		}
	    	}
		}
		
		return found;
	}
	
	// find method that invokes classLoader creation method
	public boolean findClassLoaderStaticCreationMethodUsageMethod() {
		boolean found = false;
		
		for (ClassNode cn : classes.values()) {
			@SuppressWarnings("unchecked")
			ListIterator<MethodNode> mli = cn.methods.listIterator();
			
	    	while (mli.hasNext()) {
	    		MethodNode mn = mli.next();
	    		
	    		@SuppressWarnings("unchecked")
				ListIterator<AbstractInsnNode> ili = mn.instructions.iterator();
	    		
	    		while (ili.hasNext()) {
	    			AbstractInsnNode ain = ili.next();
	    			
	    			if (ain == null)
	    				break;
	    			
	    			if (ain.getOpcode() == Opcodes.INVOKESTATIC) {
	    				MethodInsnNode min = (MethodInsnNode) ain;
	    				
			    		if (min.owner.equals(classLoaderStaticCreationMethod_class) 
			    				&& min.name.equals(classLoaderStaticCreationMethod_method)
			    				&& min.desc.equals(classLoaderStaticCreationMethod_desc)) {
			    			
			    			classLoaderStaticCreationMethodUsageMethod_class = cn.name;
			    			classLoaderStaticCreationMethodUsageMethod_method = mn.name;
			    			classLoaderStaticCreationMethodUsageMethod_desc = mn.desc;
			    			
			    			System.out.println("[Gamepack Updater] Found client classloader static creation method usage method: " + classLoaderStaticCreationMethodUsageMethod_class + "." + classLoaderStaticCreationMethodUsageMethod_method);
			    			
			    			
			    			if (ili.hasNext()) {
			    				ain = ili.next();
			    				
			    				if (ain.getOpcode() == Opcodes.PUTFIELD) {
			    					FieldInsnNode fin = (FieldInsnNode) ain;
			    					classLoader.set(fin.owner, fin.name, fin.desc);
			    				} else
			    					ain = ili.previous();
			    			}
			    			
			    			found = true;
			    		}
	    			}
	    		}
	    	}
		}
		
		return found;
	}
	
	// find method that invokes ClassLoaderStaticCreationMethodUsageMethod
	public boolean findClassLoaderOriginalInvocationMethod() {
		boolean found = false;
		
		for (ClassNode cn : classes.values()) {
			@SuppressWarnings("unchecked")
			ListIterator<MethodNode> mli = cn.methods.listIterator();
			
	    	while (mli.hasNext()) {
	    		MethodNode mn = mli.next();
	    		
	    		@SuppressWarnings("unchecked")
				ListIterator<AbstractInsnNode> ili = mn.instructions.iterator();
	    		
	    		while (ili.hasNext()) {
	    			AbstractInsnNode ain = ili.next();
	    			
	    			if (ain == null)
	    				break;
	    			
	    			if (ain.getOpcode() == Opcodes.INVOKESTATIC) {
	    				MethodInsnNode min = (MethodInsnNode) ain;
	    				
			    		if (min.owner.equals(classLoaderStaticCreationMethodUsageMethod_class) 
			    				&& min.name.equals(classLoaderStaticCreationMethodUsageMethod_method)
			    				&& min.desc.equals(classLoaderStaticCreationMethodUsageMethod_desc)) {
			    			
			    			classLoaderOriginalInvocationMethod_class = cn.name;
			    			classLoaderOriginalInvocationMethod_method = mn.name;
			    			classLoaderOriginalInvocationMethod_desc = mn.desc;
			    			
			    			System.out.println("[Gamepack Updater] Found client classloader original invocation method: " + classLoaderOriginalInvocationMethod_class + "." + classLoaderOriginalInvocationMethod_method);
			    			
			    			
			    			if (ili.hasNext()) {
			    				ain = ili.next();
			    				
			    				if (ain.getOpcode() == Opcodes.PUTFIELD) {
			    					FieldInsnNode fin = (FieldInsnNode) ain;
			    					classLoaderHolder.set(fin.owner, fin.name, fin.desc);
			    				} else
			    					ain = ili.previous();
			    			}
			    			
			    			found = true;
			    		}
	    			}
	    		}
	    	}
		}
		
		return found;
	}
	
	public boolean analyze_classLoader1(ClassNode cn) {
		boolean found = false;
		
		@SuppressWarnings("unchecked")
		ListIterator<MethodNode> mli = cn.methods.listIterator();
		
    	while (mli.hasNext()) {
    		MethodNode mn = mli.next();
    		
    		@SuppressWarnings("unchecked")
			ListIterator<AbstractInsnNode> ili = mn.instructions.iterator();
    		
    		while (ili.hasNext()) {
    			AbstractInsnNode ain = ili.next();
    			
				if (ain == null || ain.getNext() == null || ain.getNext().getNext() == null || ain.getNext().getNext().getNext() == null || ain.getNext().getNext().getNext().getNext() == null)
					break;
				
    	        if (ain.getOpcode() == Opcodes.ALOAD
    	        		&& ain.getNext().getOpcode() == Opcodes.GETFIELD
    	        		&& ain.getNext().getNext().getOpcode() == Opcodes.CHECKCAST
    	        		&& ain.getNext().getNext().getNext().getOpcode() == Opcodes.GETFIELD
    	        		&& ain.getNext().getNext().getNext().getNext().getOpcode() == Opcodes.CHECKCAST) {
    	        	
    	        	VarInsnNode aload = (VarInsnNode) ain;
    	        	FieldInsnNode getField1 = (FieldInsnNode) ain.getNext();
    	        	FieldInsnNode getField2 = (FieldInsnNode) ain.getNext().getNext().getNext();
    	        	TypeInsnNode checkCast2 = (TypeInsnNode) ain.getNext().getNext().getNext().getNext();
    	        	
    	        	if (aload.var == 0
    	        			&& checkCast2.desc.equals("java/lang/ClassLoader")) {
    	        		
    	        		classLoaderHolder.set(getField1.owner, getField1.name, getField1.desc);
    	        		classLoader.set(getField2.owner, getField2.name, getField2.desc);
    	        		
        	        	found = true;
    	        	}
    	        }
    		}
    	}
		
		return found;
	}
	
	public boolean analyze_classLoader2(ClassNode cn) {
		boolean found = false;
		
		@SuppressWarnings("unchecked")
		ListIterator<MethodNode> mli = cn.methods.listIterator();
		
    	while (mli.hasNext()) {
    		MethodNode mn = mli.next();
    		
    		@SuppressWarnings("unchecked")
			ListIterator<AbstractInsnNode> ili = mn.instructions.iterator();
    		
    		while (ili.hasNext()) {
    			AbstractInsnNode ain = ili.next();
    			
				if (ain == null || ain.getNext() == null || ain.getNext().getNext() == null || ain.getNext().getNext().getNext() == null || ain.getNext().getNext().getNext().getNext() == null)
					break;
				
    	        if (ain.getOpcode() == Opcodes.ALOAD
    	        		&& ain.getNext().getOpcode() == Opcodes.GETFIELD
    	        		&& ain.getNext().getNext().getOpcode() == Opcodes.CHECKCAST
    	        		&& ain.getNext().getNext().getNext().getOpcode() == Opcodes.GETFIELD
    	        		&& ain.getNext().getNext().getNext().getNext().getOpcode() == Opcodes.GOTO) {
    	        	
    	        	VarInsnNode aload = (VarInsnNode) ain;
    	        	FieldInsnNode getField1 = (FieldInsnNode) ain.getNext();
    	        	FieldInsnNode getField2 = (FieldInsnNode) ain.getNext().getNext().getNext();
    	        	JumpInsnNode goto1 = (JumpInsnNode) ain.getNext().getNext().getNext().getNext();
    	        	
    	        	//System.out.println(goto1.label.getNext().getOpcode());
    	        	
    	        	if (goto1.label != null && goto1.label.getNext() != null) {
	    	        	TypeInsnNode checkCast2 = (TypeInsnNode) goto1.label.getNext();
	    	        	
	    	        	if (aload.var == 0
	    	        			&& checkCast2.desc.equals("java/lang/ClassLoader")) {
	    	        		
	    	        		classLoaderHolder.set(getField1.owner, getField1.name, getField1.desc);
	    	        		classLoader.set(getField2.owner, getField2.name, getField2.desc);
	    	        		
	        	        	found = true;
	    	        	}
    	        	}
    	        }
    		}
    	}
		
		return found;
	}*/
}
