package org.rs3.reverse;

import java.util.HashSet;
import java.util.Set;

public class PutstaticFieldRecorder {
	
	private static boolean filterAll = true;
	
	private static Set<String> filters = new HashSet<>();
	
	public static void setFilterStatus(boolean filterAll) {
		PutstaticFieldRecorder.filterAll = filterAll;
	}
	
	public static void clearFilters() {
		filters.clear();
	}
	
	// own - 823
	// ! static vn.ve (fc) holds a list of qu (fc.e) which adds a qu when interacting with a rock
	//
	//
	// ! static ajy.j is also first menu item (or hovered menu item)
	// ! static client.mb * 1884185817 is crosshairPoint.x (only on screen, not interfaces, etc.)
	// ! static client.mc * -1648706485 is crosshairPoint.y (only on screen, not interfaces, etc.)
	// ! static client.mv * 1660507583 is crosshairType, reset to 0 when used, so a callback is required; otherwise: 1 (yellow) for walk, 2 (red) for object/character click
	// ! static client.mz * 1866292669 is crasshairElapsedTime, 0 for begin animation, 400 for complete animation
	// ! static fn.ah (boolean) is true when window is focused and mouse is ready
	// ! static ys.l * 79330357 is some kind of mouse bot detection method -- starts at 99 and goes down to 0 except restarts at 99 probably when movements are ok
	// !	static ul.ar * -1408426421 is similar but value depends on hardware mouse; i.e., without applet, actaul mouse coords. These two values are probably used for comparison; ul.ar reverts to the same value if the applet is not focused; !!! ul.ar is not changed with bot, so it is a actual mouse coord hook, use this for testing simulated mouse
	// !	apr fields are associated with ys.l and apy fields are associated with ul.ar
	//
	// static ma.dn (aa) holds aa.c (vh[]) length 25; vh.c, vh.y, vh.m of index 8 are all changed after cutting down a tree (it's not chat and not items)
	//
	// Chat
	//
	// ! static c.y(...) is the chat message handler (for callback)
	// ! static client.ro * 39150423 is the last chat message loop cycle
	// ! static client.qb * -1203897903 * 39150423 is the current loop cycle
	// ! static hv.c has to do with last chat message (maybe loop cycle or pos)
	// ! apg is chat item node
	// ! apg.i * 247466087 is size
	// ! hz is chat type (0 has Welcome to RuneScape.) (109 has You manage to mine some copper., etc.) (125 has <img=7><col=ff0000>News: Qlimax has just achieved at least level 99 in all skills!)
	// ! static hv.g is an hz map
	// ! hz.p is an apg array
	// ! current apg is apgArray[0]
	// ! apg.j is text
	//
	// How to get current chat message:
	/*Map hzMap = (Map) Reflection.getStaticDeclaredField("hv", "g");
	System.out.println(hzMap);
	
	Object hz = hzMap.get(125);
	Object[] apgArray = (Object[]) Reflection.getDeclaredField("hz", hz, "p");
	Object apg = apgArray[0];
	Object obj = Reflection.getDeclaredField("apg", apg, "j");
	
	System.out.println(obj);*/
	
	/*
	 * own - 830
	 * static
	 * 
	 * ags.p * -775188053 - 0-99 unknown client mouse activity
	 *   - starts at 99 and decreases to 0 as mouse is used
	 *   - changed with any mouse event -- some events react differently
	 *   - resets to 99 after a few seconds of no movement
	 *   
	 * abu.ar * -957495107 - 59-62 unknown hardware mouse activity
	 *   - similar to ags.p - "unknown client mouse activity" except values are only 59-62
	 *   
	 */
	public static void putstaticCalled(Object val, String field, String method) {
		//if (field.equals("xj.o"))
		//	return;
		
		if (val != null) {
			if (val instanceof Number) {
				Number num = (Number) val;
				if (num.intValue() == 99999999)
					System.out.println("PUTSTATIC: " + field + " = " + val);
			}
		}
		
		//if (val != null && val instanceof String && ((String) val).toLowerCase().contains("wa;;;lk"))
		//	System.out.println("PUTSTATIC: " + field + " = " + val);
	   //     System.out.println("PUTSTATIC: " + field + " = " + (int)val * -1408426421);
			//System.out.println("PUTSTATIC: " + field + " = " + ((int)val >> 9));
			
		if (field.startsWith("ags.p;;"))
			System.out.println("PUTSTATIC: " + field + " = " + val);
		
		if (field.startsWith("abu.ar;;")) {
			//System.out.println("PUTSTATIC: " + field + " = " + val);
			
			Number number = (Number) val;
			int num = number.intValue();
			System.out.println("PUTSTATIC: " + field + " = " + num * -957495107);
			
			/*Number number = (Number) val;
			long num = number.longValue();
			System.out.println("PUTSTATIC: " + field + " = " + num * 1476260281L);*/
		}
			
		if (!filters.contains(field)) {
			if (!filterAll) {
				//System.out.println("PUTSTATIC: " + field);
				//if (val != null) {
				//	if (val instanceof Number) {
				System.out.println("PUTSTATIC: " + field + " = " + val);
				//	}
				//}
			} else {
				filters.add(field);
			}
		}
	}
	
}
