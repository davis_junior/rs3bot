package org.rs3.reverse;

import java.util.HashSet;
import java.util.Set;

public class GetstaticFieldRecorder {
	
	private static boolean filterAll = true;
	
	private static Set<String> filters = new HashSet<>();
	
	public static void setFilterStatus(boolean filterAll) {
		GetstaticFieldRecorder.filterAll = filterAll;
	}
	
	public static void clearFilters() {
		filters.clear();
	}
	
	
	public static void getstaticCalled(Object val, String field, String method) {
		if (!filters.contains(field)) {
			if (!filterAll)
				//System.out.println("GETSTATIC: " + field);
				System.out.println("GETSTATIC: " + field + " = " + val);
			else {
				filters.add(field);
			}
		}
	}
	
}
