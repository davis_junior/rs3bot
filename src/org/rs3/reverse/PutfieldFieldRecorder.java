package org.rs3.reverse;

import java.util.HashSet;
import java.util.Set;

public class PutfieldFieldRecorder {
	
	private static boolean filterAll = true;
	
	private static Set<String> filters = new HashSet<>();
	
	private static Object obj = null;
	private static Object val = null;
	
	
	public static void setFilterStatus(boolean filterAll) {
		PutfieldFieldRecorder.filterAll = filterAll;
	}
	
	public static void clearFilters() {
		filters.clear();
	}
	
	public static Object getObject() {
		return obj;
	}
	
	public static Object getValue() {
		return val;
	}
	
	public static float modify(float val) {
		if (val != 1.0 && val != 1.0039216F) {
			//System.out.println(val);
			//return 1.117647F;
			//return val;
			//return (float)Random.nextDouble(1.4, 1.5);
			//val = (float)Random.nextDouble(99.4, 99.9);
			//val = (float)Random.nextDouble(1.14, 1.16);
			System.out.println(val);
			//return val;
			return 1.0F;
		}
		
		return val;
	}
	
	/*
	 * own - 825
	 * SDRender -> adv
	 *	* clientViewport (oi) -> adv.bi
	 */
	
	/*
	 * own - 830
	 * 
	 * apr - HardwareMouse - values DO NOT change with AWT events (running script)
	 * apr.aa * 1095212521 - hardware mouse x (< 0 to 1920)
	 * apr.x * -1476260281 - hardware mouse y (65535 to 1080)
	 * apr.ah * -810829955 - hardware mouse lastEvent
	 *   - 512 - mouse move
	 *   - 513 - mouse left press
	 *   - 514 - mouse left release
	 *   - 516 - mouse right press
	 *   - 517 - mouse right release
	 *   - 519 - mouse middle press
	 *   - 520 - mouse middle release
	 *   - 522 - mouse wheel move (any direction)
	 *   - double click - nothing different
	 * apr.aq * -820102599 - hardware mouse unknown activity
	 *   - always 0, set with any mouse event (move, click, mouse wheel move/click)
	 * apr.am * 6778757040785710661L - hardware mouse eventTime?
	 *   - keeps increasing
	 *     - mouse move +8
	 *     - mouse (left?) press +11269
	 *     - mouse (left?) release +5712
	 *     - etc.
	 * 
	 * apf - ClientMouse - values DO change with script running
	 * apf.k * -700798007 - client mouse x
	 *   - also accounts for outside of client if mouse is moved fast enough -- up to 1920 on my screen with client width at 800
	 * apf.n * 176184079 - client mouse y
	 *   - similar to "client mouse x"
	 * etc. - TODO
	 */
	public static void putfieldCalled(Object obj, Object val, String field, String method) {
		// own2 - 824
		// any.ai (Player) is current npc interaction index
		
		/*if (method.equals("")) {
			//System.out.println("PUTFIELD (" + obj + "): " + field + " = " + val);
		}*/
		
		/*if (val != null) {
			if (val instanceof Number) {
				Number num = (Number) val;
				if (num.intValue() == 9999999)
					System.out.println("PUTFIELD: " + field + " = " + val);
					//System.out.println("PUTFIELD (" + obj + "): " + field + " = " + val);
			}
		}*/
		
		/*if (field.startsWith("apf.k")) {
			//System.out.println("PUTFIELD: " + field + " = " + val);
			//System.out.println("PUTFIELD (" + obj + "): " + field + " = " + val);
			
			Number number = (Number) val;
			int num = number.intValue();
			System.out.println("PUTFIELD (" + obj + "): " + field + " = " + num * -700798007);
		}*/
		
		if (!filters.contains(field)) {
			if (!filterAll) {
				//System.out.println("PUTFIELD: " + field);
				//if (val != null) {
				//	if (val instanceof Number) {
				System.out.println("PUTFIELD: " + field + " = " + val);
				//	}
				//}
				
				/*if (field.equals("aiz.c")) {
					if (PutfieldFieldRecorder.obj != obj) {
						System.out.println("PUTFIELD obj CHANGED: " + PutfieldFieldRecorder.obj + " -> " + obj);
					}
					
					if (PutfieldFieldRecorder.val != val) {
						System.out.println("PUTFIELD val CHANGED: " + PutfieldFieldRecorder.val + " -> " + val);
					}
					
					PutfieldFieldRecorder.obj = obj;
					PutfieldFieldRecorder.val = val;
				}*/
			} else {
				filters.add(field);
			}
		}
	}
	
}
