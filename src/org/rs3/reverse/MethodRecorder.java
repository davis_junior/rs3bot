package org.rs3.reverse;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.rs3.api.wrappers.AnimableObject;
import org.rs3.api.wrappers.Client;
import org.rs3.api.wrappers.MenuItemNode;
import org.rs3.api.wrappers.Model;
import org.rs3.callbacks.MouseCallback;
import org.rs3.callbacks.ViewportCallback;
import org.rs3.debug.Paint;
import org.rs3.util.ArrayUtils;

public class MethodRecorder {
	
	private static boolean filterAll = true;
	
	private static Set<String> filters = new HashSet<>();
	private static Set<String> exitFilters = new HashSet<>();
	
	private static Set<String> ownMethodFilters = new HashSet<>();
	private static String lastOwnMethodCalled = "";
	private static int lastOwnMethodCalledCount = 0;
	
	public static void setFilterStatus(boolean filterAll) {
		MethodRecorder.filterAll = filterAll;
	}
	
	public static void clearFilters() {
		filters.clear();
	}
	
	public static void addToOwnFilter(String... methods) {
		ownMethodFilters.addAll(Arrays.asList(methods));
	}
	
	public static void clearOwnFilters() {
		ownMethodFilters.clear();
	}
	
	
	/**
	 * 
	 * @param obj object of "virtual" method, null if static
	 * @param args method arguments
	 * @param method <classname>.<methodname>
	 */
	public static void methodCalled(Object obj, Object[] args, String method) {
		
		if (method.toLowerCase().contains("jaclib")) {
			System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
		}
		
		// aei.bp check (login screen)
		// aei.bj, aei.bk, aei.bt
		
		/*String[] skipMethods = new String[] {
				// login screen
				"aei.qb", "aei.qt", "aei.bz", "aei.ap", "aei.c", "aei.y", "aei.n", "aei.bb", "aei.be", "aei.bp", "aei.bq",
				
				// lobby screen
				"aei.bj", "aei.bk", "aei.bt",
				
				// logged in
				"aei.p"
			};
		for (String skipMethod : skipMethods) {
			if (method.equals(skipMethod))
				return;
		}*/
		
		/*if (method.equals("pg.u")) {
			if (args != null) {
				MenuItemNode item = MenuItemNode.create(args[0]);
				if (item != null) {
					System.out.println("added menu item: " + item.getAction() + " " + item.getOption());
				}
			}
		} else
			return;*/
		
		if (method.toLowerCase().startsWith("aei.;;")) {
			System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
		}
		
		if (method.equals("aco.d;;")) {
			System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
		}
		
		// own - 850
		// static av.s, ac.w on lobby login -- called to create SDRender object
		// static dz.w -- called to initialize render object (sd and gl and dx)
		//		-- 1st param int
		//			- 0 = safe mode, 1 = gl, 3 = dx, 5 = gl?
		
		//if (method.equals("dz.w") || method.equals("dz.nt") || method.equals("dz.np")) {
		/*if (method.equals("ct.s") || method.equals("ct.w")) {
			System.out.println(method);
			//System.out.println(args[0]);
		}
		
		if (args != null) {
			for (Object arg : args) {
				if (arg != null && arg instanceof String) {
					String str = (String) arg;
					if (str.equals("d")) {
						System.out.println(method);
					}
				}
			}
		}*/
		
		// 
		// 
		// 
		/*if (method.equals("ct.s") || method.equals("ct.w")) {
			System.out.println(method);
			//System.out.println(args[0]);
		}
		
		if (true)
			return;*/
		
		/*if (obj != null && obj.getClass().getName().equals("adj")) {
			if (args != null && args.length >= 6) {
				int intCount = 0;
				for (Object arg : args) {
					if (arg != null && arg.getClass().equals(Integer.class))
						intCount++;
				}
				if (intCount >= 6)
					System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
			}
		}*/
		
		/*if (method.equals("ahb.bf")) {
			AnimableObject ao = AnimableObject.create(obj);
			if (ao != null) {
				//System.out.println(ao.getId());
				
				if (ao.getId() == 38785) {
					System.out.println("original ref = " + obj);
					Model modelField = ao.getModel_field();
					System.out.println("original model ref = " + modelField.getObject());
					System.out.println("original model: " + modelField);
					Paint.model = modelField;
					Paint.animableObj = ao;
				}
			}
		}*/
		
		// own2 - 824
		// z.y(int, int) holds chat index -- eg (0 - INFINITE)
		//		-- arg1 is chat index
		//		-- (unverified) this disregards chat type (RS, player, interaction, etc.)
		
		// ga.jv(int, int, int) holds local destination changed -- eg (49, 41, DECOY)
		//		-- arg1 is x, arg2 is y
		//		-- only called once after click, does not reset to -1, -1
		
		// rd.bf(int, int, int, int) holds destination changed if walking only -- eg (50, 39, 0, DECOY)
		// 		-- arg1 is x, arg2 is y
		//		-- arg3 is clickType -- 0 for screen, 1 for map
		//		-- only called once after click, does not reset to -1, -1
		
		// ti.an(int, int, int) holds destination flag coords - eg (27, 81, DECOY)
		//		-- arg1 is x, arg2 is y
		//		-- called whenever flag is changed; -1, -1 if flag disappears
		
		// hp.bv(apg, int, int, int) holds click coords -- eg (apg@32ec0c29, 411, 262, DECOY)
		//		-- arg1 is menu item clicked (MenuItemNode)
		
		// Field apg.y (long) -- apg is MenuItemNode - is objectUniqueId
		//		-- Calculation:
		//		long objectUniqueId = (long)apg.y * 6270385172188079733L;
		//		int objectDefId = (int)(objectUniqueId >>> 32) & 0x7FFFFFFF;
		
		// Field apg.z (int) -- apg is MenuItemNode -- is actionType
		//		-- Caclulation:
		//		int m = (int) apg * 1870076377;
		//		if (m >= 2000) {
		//			m -= 2000;
		//		}
		//
		//		-- After calculation:
		/*
		 * 2 - use inventory item on tree
		 * 3 - chop tree / mine ore (whether spawned or not)
		 * 4 - pick wheat
		 * 9 - talk to npc
		 * 10 - attack goblin
		 * 20 - pickup ground item
		 * 23 - walk
		 * 25 - use
		 * 46 - follow player
		 * 58 - use inventory item on inventory item
		 * 1002 - examine tree / wheat / signpost
		 * 1003 - examine goblin
		 * 1006 - cancel
		 * 1007 - drop item / examine inventory item
		 */
		
		// amg.x(int) holds interaction type on click -- eg (-673675581)
		//		-- arg1 is interaction type -- -673675581 for drop item, 1599030869 for tree interaction, 738122621 for walk
		
		// Field aeb.at (int[]) is animation ids, all 4 the same; this is no different than Animator.Animation.id -- null when there is no animation but usually -1 when no animation
		
		// agf.fo(ada, int, int, int) is called when hovering something that has an interactable first menu option -- eg (ada@54c215bb, 364, 289, DECOY)
		//		-- arg1 is ?
		//		-- arg2 is mouse x, arg3 is mouse y
		//		-- eg -- if a rock is spawned, this will be called if hovering the rock, otherwise if the rock is gone and you are hovering, this will not be called (even though there is still a menu option)
		
		// ahh.fo(ada, int, int, int) is called when an AnimableObject -- ahh is AnimableObject -- is hovered -- eg (ada@54c215bb, 799, 450, DECOY)
		//		-- arg1 is render
		//		-- arg2 is mouse x, arg3 is mouse y
		
		// tb.an(...) holds a byte[][][] of tiles [plane][x][y] where it is 1 if it is a building tile
		
		// aft.s(...) holds an int[][][] of tiles [plane][x][y] where 0 is underlying water; probably not useful except for maybe dungeoneering location detection?
		
		//qp.ax [[B@3c6b4fcc
		//qp.am [[B@2943f911
		
		/*
		 * 	2125170108
			545606294s
		 */
		
		// own - 830
		//
		// Npc models
		//
		// - NpcDefinition (iu) method iu.v(...) performs grabbing model from modelCache and animating.
		// The model grabbed from the modelCache is unanimated and not rotated
		// The last Animator (yc) performs the animation -- paramyc2.k(localdk2, 0, (short)-13303);
		// Everything else in the npcdef method is likely not needed
		
		//if (obj != null && method.equals("oi.d")) {
		if (obj != null && method.equals("ol.q;;;;")) {
			//System.out.println(obj.hashCode());
			
			//System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
			
			//if (obj != null && obj.hashCode() == 545606294) { // 800 x 600
			if (obj != null && obj.hashCode() == -657354786) { // resized (depends on size)
				ViewportCallback.debugFloats2 = Client.getRender().getClientViewport().getFloats().clone();
				
				//ViewportCallback.floats = Client.getRender().getClientViewport().getFloats().clone();
				//ViewportCallback.floats = null;
			}
			
			//if (obj != null && obj.hashCode() == 2125170108) { // 800 x 600
			if (obj != null && obj.hashCode() == 845476874) { // resized (depends on size)
				ViewportCallback.debugFloats1 = Client.getRender().getClientViewport().getFloats().clone();
				
				//ViewportCallback.floats = ViewportCallback.debugFloats1;
				//ViewportCallback.floats = null;
			}
			
		}
		
		//if (method.equals("zq.p") || method.equals("aqh.p") || method.equals("zb.p") || method.equals("pg.u") || method.equals("go.ay") || method.equals("zb.r") || method.equals("yf.ax") || method.equals("afl.i") || method.equals("qn.ak") || method.equals("m.aw") || method.equals("ame.at") || method.equals("c.bl") || method.equals("vv.w"))
		//	return;
		
		/*if (args != null) {
			for (Object arg : args) {
				if (arg != null) {
					//if (arg instanceof Image)
					//if (arg instanceof String)
					if (data.MenuItemNode.get.getInternalName().equals(arg.getClass().getName()))
						System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
				}
			}
		}*/
		
		/*if (args != null) {
			for (Object arg : args) {
				//if (arg != null && arg instanceof Long && (long)arg == 309599502768437L) {
				if (arg != null && arg instanceof Long && (long)arg == 309586617866549L) {
					System.out.println(method + " " + arg);
					
					if (method.startsWith("")) {
						//System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
					}
				}
			}
		}*/
		
		/*if (args != null) {
			for (Object arg : args) {
				if (arg != null && arg.getClass().getName().equals("ey")) {
					System.out.println(method + " " + arg);
					
					if (method.startsWith("")) {
						//System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
					}
				}
			}
		}*/
		
		/*if (args != null) {
			for (Object arg : args) {
				if (arg != null && arg.getClass().getName().equals("[[[B")) {
					System.out.println(method + " " + arg);
					
					if (method.startsWith(";;;aft.s")) {
						//System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
						
						int[][][] array = (int[][][]) arg;
						if (array != null) {
							int[][] colFlags = array[0];
							if (colFlags != null) {
								
								List<Tile> list = new ArrayList<>();
								BaseInfo base = Client.getGameInfo().getBaseInfo();
								int baseX = base.getX();
								int baseY = base.getY();
								
								for (int x = 0; x < 104; x++) {
									for (int y = 0; y < 104; y++) {
										int val = colFlags[x][y];
										
										if (val == 0) {
											list.add(new Tile(baseX + x, baseY + y, 0));
											//System.out.println(b + ": " + x + ", " + y);
										} //else
										//	System.out.println(val);
									}
								}
								
								Paint.updatePaintObject(list.toArray(new Tile[0]));
								
								//System.out.println(colFlags[50][50]);
								//System.out.println(colFlags.length);
								//System.out.println(ArrayUtils.arrayToString(colFlags));
							}
						}
					}
				}
			}
		}*/
		
		if (!filters.contains(method)) {
			if (!filterAll)
				System.out.println("CALLED: " + method + "(" + ArrayUtils.arrayToString(args) + ")");
			else {
				filters.add(method);
			}
		}
	}
	
	public static void methodExit(Object returnObject, Object methodCallingObj, String method) {
		if (returnObject != null && returnObject instanceof Number) {
			Number num = (Number) returnObject;
			if (num.intValue() == 1621)
				System.out.println("EXITED: " + method + " = " + returnObject);
		}
		
		//if (method.equals("akv.ap"))
		//	System.out.println("EXITED: " + method + " = " + returnObject);
		
		if (!exitFilters.contains(method)) {
			if (!filterAll)
				System.out.println("EXITED: " + method + " = " + returnObject);
			else {
				exitFilters.add(method);
			}
		}
	}
	
	public static void ownMethodCalled(String method) {
		if (method.contains("paint"))
			System.out.println("CALLED: " + method);
		
		if (!ownMethodFilters.contains(method)) {
			if (!filterAll)
				System.out.println("CALLED: " + method);
			else {
				ownMethodFilters.add(method);
			}
			
			/*if (!lastOwnMethodCalled.equals(method)) {
				System.out.println("CALLED: " + method);
				lastOwnMethodCalled = method;
				lastOwnMethodCalledCount = 0;
			} else if (lastOwnMethodCalledCount == 0) {
				System.out.println("CALLED multi: " + method);
				lastOwnMethodCalledCount++;
			}*/
		}
	}
}
