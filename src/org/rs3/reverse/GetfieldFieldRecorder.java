package org.rs3.reverse;

import java.util.HashSet;
import java.util.Set;

public class GetfieldFieldRecorder {
	
	private static boolean filterAll = true;
	
	private static Set<String> filters = new HashSet<>();
	
	private static Object obj = null;
	private static Object val = null;
	
	
	public static void setFilterStatus(boolean filterAll) {
		GetfieldFieldRecorder.filterAll = filterAll;
	}
	
	public static void clearFilters() {
		filters.clear();
	}
	
	public static Object getObject() {
		return obj;
	}
	
	public static Object getValue() {
		return val;
	}
	
	/*
	 * own - 825
	 * SDRender -> adv
	 *	* clientViewport (oi) -> adv.bi
	 */
	public static void getfieldCalled(Object obj, Object val, String field, String method) {
		//if (true)
		//	return;
		
		// own2 - 824
		// any.ai (Player) is current npc interaction index
		
		/*if (method.equals("")) {
			//System.out.println("GETFIELD (" + obj + "): " + field + " = " + val);
		}*/
		
		//if (field.startsWith("aqe.;;")) {
		/*if (val != null && val.getClass().getName().toLowerCase().contains("graphics;;")) {
			
			System.out.println("GETFIELD method " + method);
			//System.out.println("GETFIELD (" + obj + "): " + field + " = " + val);
		}*/
		
		if (!filters.contains(field)) {
			if (!filterAll) {
				//System.out.println("GETFIELD: " + field);
				System.out.println("GETFIELD: " + field + " = " + val);
				
				/*if (field.equals("aiz.c")) {
					if (GetfieldFieldRecorder.obj != obj) {
						System.out.println("GETFIELD obj CHANGED: " + GetfieldFieldRecorder.obj + " -> " + obj);
					}
					
					if (GetfieldFieldRecorder.val != val) {
						System.out.println("GETFIELD val CHANGED: " + GetfieldFieldRecorder.val + " -> " + val);
					}
					
					GetfieldFieldRecorder.obj = obj;
					GetfieldFieldRecorder.val = val;
				}*/
			} else {
				filters.add(field);
			}
		}
	}
	
}
