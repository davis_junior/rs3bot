package org.rs3.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.rs3.ftp.Ftp;

public class FileCleaner {

	public static void deleteRunescapeFiles() {
		File userHomeDir = new File(System.getProperty("user.home"));
		
		File jagex_cl_runescape_LIVE_dat = new File(userHomeDir, "jagex_cl_runescape_LIVE.dat");
		File random_dat = new File(userHomeDir, "random.dat");
		File jagexappletviewer_preferences = new File(userHomeDir, "jagexappletviewer.preferences");
		
		jagex_cl_runescape_LIVE_dat.delete();
		random_dat.delete();
		jagexappletviewer_preferences.delete();
	}
	
	public static void loadNetworkRunescapeCache() {
		File runescapeCacheDir = new File(System.getProperty("user.home") + "\\jagexcache\\runescape");
		long bytes = runescapeCacheDir.exists() ? FileUtils.sizeOfDirectory(runescapeCacheDir) : 0L;
		int mbs = (int) (bytes / 1024 / 1024);
		if (mbs < 1000) {
			System.out.println("Loading cache from network...");
			
			/*File networkShare = new File("\\\\PC-DAVE\\updates\\runescape");
			try {
				FileUtils.copyDirectory(networkShare, runescapeCacheDir);
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
			Ftp client = new Ftp();
			client.login();
			try {
				client.downloadDirectory("/updates/runescape/", runescapeCacheDir.getAbsolutePath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			client.logout();
			
			// check size again
			bytes = runescapeCacheDir.exists() ? FileUtils.sizeOfDirectory(runescapeCacheDir) : 0L;
			mbs = (int) (bytes / 1024 / 1024);
			if (mbs < 1000) {
				System.out.println("Unable to load cache from network! Stopping bot...");
				Time.sleep(5000);
				System.exit(0);
			}
		}
		System.out.println("RuneScape cache dir size: " + mbs + "mb");
	}
	
}
