package org.rs3.util;

/**
 * Provides custom routines.
 */
public interface Routine {
	
	/**
	 * Performs the defined action.
	 *
	 * @return <tt>true</tt> if the action is successful; <tt>false</tt> otherwise.
	 */
	public boolean doAction();
}