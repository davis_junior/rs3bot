package org.rs3.util;

/**
 * Provides custom conditionals.
 */
public abstract class Condition {
	
	final Condition reversed;
	
	public Condition() {
		final Condition cur = this;
		
		reversed = new Condition(cur) {
			@Override
			public boolean evaluate() {
				return !cur.evaluate();
			}
		};
	}
	
	/**
	 * Sometimes the default reversed Condition may not be useful; if so, supply your own here.
	 * 
	 * @param reversed The new reversed Condition.
	 */
	public Condition(Condition reversed) {
		this.reversed = reversed;
	}
	
	/**
	 * Gets the Condition with the return value of evaluate() reversed.
	 * 
	 * @return The Condition with the return value of evaluate() reversed.
	 */
	public Condition getReversed() {
		return reversed;
	}
	
	/**
	 * Evaluates a condition.
	 *
	 * @return <tt>true</tt> if the condition is met; <tt>false</tt> otherwise.
	 */
	public abstract boolean evaluate();
}
