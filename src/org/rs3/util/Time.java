package org.rs3.util;

/**
 * A utility for manipulating time.
 *
 * @author Timer
 */
public class Time {
	/**
	 * @param time The number of milliseconds to ensure sleeping for.
	 */
	public static void sleep(final int time) {
		sleep((long) time);
	}
	
	/**
	 * @param time The number of milliseconds to ensure sleeping for.
	 */
	public static void sleep(final long time) {
		try {
			long start = System.currentTimeMillis();
			
			Thread.sleep(time);
			
			long now;
			while (start + time > (now = System.currentTimeMillis())) {
				Thread.sleep(start + time - now);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sleeps for a random number of milliseconds.
	 *
	 * @param min the minimum sleep time.
	 * @param max the maximum sleep time.
	 */
	public static void sleep(final int min, final int max) {
		sleep(Random.nextInt(min, max));
	}
	
	public static void sleep(float percent1, final int min1, final int max1, final int min2, final int max2) {
		sleep(Random.nextInt(percent1, min1, max1, min2, max2));
	}
	
	/**
	 * 90% 60-120ms else 60-150ms
	 */
	public static void sleepQuickest() {
		Time.sleep(0.90F, 60, 120, 60, 150);
	}
	
	/**
	 * 90% 125-150ms else 150-250ms
	 */
	public static void sleepVeryQuick() {
		//Time.sleep(0.75F, 300, 600, 600, 1200);
		//Time.sleep(0.90F, 200, 400, 400, 600);
		//Time.sleep(0.90F, 150, 250, 250, 350);
		Time.sleep(0.90F, 125, 150, 150, 250);
	}
	
	/**
	 * 90% 200-300ms else 300-400ms
	 */
	public static void sleepQuick() {
		//Time.sleep(0.75F, 500, 800, 1200, 1500);
		//Time.sleep(0.90F, 300, 600, 600, 800);
		//Time.sleep(0.90F, 300, 400, 400, 500);
		Time.sleep(0.90F, 200, 300, 300, 400);
	}
	
	/**
	 * 90% 300-500ms else 500-600ms
	 */
	public static void sleepMedium() {
		//Time.sleep(0.90F, 500, 800, 800, 1200);
		//Time.sleep(0.90F, 400, 600, 600, 800);
		Time.sleep(0.90F, 300, 500, 500, 600);
	}
	
	/**
	 * Converts milliseconds to a String in the format
	 * hh:mm:ss.
	 *
	 * @param time The number of milliseconds.
	 * @return The formatted String.
	 */
	public static String format(final long time) {
		final StringBuilder t = new StringBuilder();
		
		final long total_secs = time / 1000;
		final long total_mins = total_secs / 60;
		final long total_hrs = total_mins / 60;
		
		final int secs = (int) total_secs % 60;
		final int mins = (int) total_mins % 60;
		final int hrs = (int) total_hrs % 24;
		
		if (hrs < 10)
			t.append("0");
		
		t.append(hrs);
		t.append(":");
		
		if (mins < 10)
			t.append("0");
		
		t.append(mins);
		t.append(":");
		
		if (secs < 10)
			t.append("0");
		
		t.append(secs);
		
		return t.toString();
	}
}
