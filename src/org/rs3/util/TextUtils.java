package org.rs3.util;

import java.util.regex.Pattern;

public class TextUtils {
	
	private static final Pattern HTML_TAG = Pattern.compile("(^[^<]+>|<[^>]+>|<[^>]+$)");
	
	/**
	 * Removes html formatting from input. E.g., "<col=ffff>Iron ore rocks" to "Iron ore rocks."
	 * 
	 * @param input text with html
	 * @return input with html removed
	 */
	public static String removeHtml(String input) {
		return HTML_TAG.matcher(input).replaceAll("");
	}
}
