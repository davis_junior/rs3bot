package org.rs3.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.rs3.callbacks.proxy.ProxySetup;

/**
 * A utility that generates random numbers.
 *
 * @author Timer
 */
public class Random {
	private static java.util.Random random;

	static {
		randomOrgReseed();
	}

	/**
	 * Note: the official client (jagexappletloader) sets the system https protocol variable to SSLv3 only, so we must manually set it to TLS.
	 * This is done in the offical client inject method.
	 */
	public static void randomOrgReseed() {
		boolean success = false;
		BufferedReader in = null;
		
		try {
			URL url = new URL(
					//"http://www.random.org/strings/?num=10&len=10&digits=on&unique=on&format=plain&rnd=new");
					"https://www.random.org/cgi-bin/randbyte?nbytes=20&format=d");
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection(ProxySetup.getProxy());
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(10000);
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			StringBuilder stringBuilder = new StringBuilder();

			String line;
			while ((line = in.readLine()) != null) {
				stringBuilder.append(line);
			}
			//in.close();

			String html = stringBuilder.toString();
			html = html.trim().replaceAll(" +", " ");

			System.out.println(html);
			
			String[] parts = html.split(" ");
			if (parts.length == 20) {
				byte[] seed = new byte[parts.length];
				for (int i = 0; i < parts.length; i++) {
					seed[i] = (byte) Integer.parseInt(parts[i]);
				}

				random = new SecureRandom(seed);
				success = true;
				System.out.println("Succeeded in retrieving new random seed from random.org: " + ArrayUtils.arrayToString(seed));
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} finally {
			if (!success) {
				System.out.println("ERROR: Default secure random is used.");
				random = new SecureRandom();
			}
			
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					//e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @return The next random boolean value.
	 */
	public static boolean nextBoolean() {
		return random.nextBoolean();
	}

	/**
	 * Generates a pseudo-random number between two given values.
	 *
	 * @param min The minimum value (inclusive).
	 * @param max The maximum value (exclusive).
	 * @return The generated pseudo-random integer.
	 */
	public static int nextInt(final int min, final int max) {
		if (max < min) {
			return max + random.nextInt(min - max);
		}
		return min + (max == min ? 0 : random.nextInt(max - min));
	}

	/**
	 * Generates a pseudo-random number. Uses min1 and max1 if a random percent falls in percent1, otherwise it uses min2, max2.
	 * @param percentage1 The percent chance of using min1 and max1. Should be between 0.0 and 1.0
	 * @param min1
	 * @param max1
	 * @param min2
	 * @param max2
	 * @return
	 */
	public static int nextInt(float percent1, final int min1, final int max1, final int min2, final int max2) {
		int randPercentage1 = nextInt(0, 100);

		int percentage1 = Math.round(percent1 * 100.0F);

		if (randPercentage1 <= percentage1)
			return nextInt(min1, max1);
		else
			return nextInt(min2, max2);
	}

	/**
	 * Generates a pseudo-random number between two given values.
	 *
	 * @param min The minimum value (inclusive).
	 * @param max The maximum value (exclusive).
	 * @return The generated pseudo-random double.
	 */
	public static double nextDouble(final double min, final double max) {
		return min + random.nextDouble() * (max - min);
	}

	/**
	 * @return A generated pseudo-random double between 0.0 (inclusive) and 1.0 (exclusive).
	 */
	public static double nextDouble() {
		return random.nextDouble();
	}

	/**
	 * Generates a pseudo-random number between the two given values with standard deviation.
	 *
	 * @param min The minimum value (inclusive).
	 * @param max The maximum value (exclusive).
	 * @param sd  Standard deviation.
	 * @return The generated pseudo-random integer.
	 */
	public static int nextGaussian(final int min, final int max, final int sd) {
		return nextGaussian(min, max, min + (max - min) / 2, sd);
	}

	/**
	 * Generates a pseudo-random number between the two given values with standard deviation about a provided mean.
	 *
	 * @param min  The minimum value (inclusive).
	 * @param max  The maximum value (exclusive).
	 * @param mean The mean (>= min & < max).
	 * @param sd   Standard deviation.
	 * @return The generated pseudo-random integer.
	 */
	public static int nextGaussian(final int min, final int max, final int mean, final int sd) {
		if (min == max) {
			return min;
		}
		int rand;
		do {
			rand = (int) (random.nextGaussian() * sd + mean);
		} while (rand < min || rand >= max);
		return rand;
	}
}